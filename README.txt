This is a rigging tool for maya. 
=====================================================

PackageUI lets you link python functions to objects in maya.

For example: You can link the script thats builds a leg rig to the initial joints 
that you need to define the pivot points. 
This way you can save a Maya file with a template leg that just consists of the 
joints and the function linked to them. 
You can import this template for a new character, reposition the joints and build 
it when you are finished.

Requires maya v.2015 or higher. Tested on windows.

Please read the docs: ``docs\html\install.html``
