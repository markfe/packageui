//Maya ASCII 2009 scene
//Name: eye.ma
//Last modified: Thu, Jan 27, 2011 10:33:53 AM
//Codeset: 1252
requires maya "2009";
requires "stereoCamera" "10.0";
currentUnit -l centimeter -a degree -t pal;
fileInfo "application" "maya";
fileInfo "product" "Maya Unlimited 2009";
fileInfo "version" "2009 Service Pack 1a x64";
fileInfo "cutIdentifier" "200904080020-749524";
fileInfo "osv" "Microsoft Windows XP x64 Service Pack 2 (Build 3790)\n";
createNode transform -n "temp_CHECK_cam";
	addAttr -ci true -sn "start_frame" -ln "start_frame" -min 0 -at "long";
	addAttr -ci true -sn "end_frame" -ln "end_frame" -dv 1 -min 1 -at "long";
	setAttr ".t" -type "double3" 14.322252924499953 19.16142523017524 11.895892721028195 ;
	setAttr ".r" -type "double3" -2.3999153349560829 55.600000000000271 -3.5185181857772314e-016 ;
	setAttr -k on ".start_frame";
	setAttr -k on ".end_frame" 24;
createNode camera -n "temp_CHECK_camShape" -p "temp_CHECK_cam";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".cap" -type "double2" 1.4173 0.9449 ;
	setAttr ".ff" 0;
	setAttr ".ncp" 1;
	setAttr ".fcp" 100000;
	setAttr ".fd" 500;
	setAttr ".coi" 17.363801069348284;
	setAttr ".ow" 3000;
	setAttr ".imn" -type "string" "camera1";
	setAttr ".den" -type "string" "camera1_depth";
	setAttr ".man" -type "string" "camera1_mask";
	setAttr ".tp" -type "double3" 0.084529884233654595 18.233332166148834 8.0338293567417285 ;
select -ne :time1;
	setAttr ".o" 1;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 3 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :lightList1;
select -ne :initialShadingGroup;
	setAttr -s 7 ".dsm";
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".fs" 1;
	setAttr ".ef" 10;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
// End of eye.ma
