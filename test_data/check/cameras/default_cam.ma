//Maya ASCII 2009 scene
//Name: default_cam.ma
//Last modified: Wed, Mar 10, 2010 06:36:01 PM
//Codeset: 1252
requires maya "2009";
requires "stereoCamera" "10.0";
currentUnit -l meter -a degree -t pal;
fileInfo "application" "maya";
fileInfo "product" "Maya Unlimited 2009";
fileInfo "version" "2009 Service Pack 1a";
fileInfo "cutIdentifier" "200904080023-749524";
fileInfo "osv" "Microsoft Windows Vista Service Pack 1 (Build 6001)\n";
createNode transform -n "CHECK_cam";
	addAttr -ci true -sn "start_frame" -ln "start_frame" -min 0 -at "long";
	addAttr -ci true -sn "end_frame" -ln "end_frame" -dv 1 -min 1 -at "long";
	setAttr ".t" -type "double3" 0.35865971994658169 19.453520793568195 39.382173507773921 ;
	setAttr ".r" -type "double3" -23.999153349560913 -1.5999999999999923 0 ;
	setAttr -k on ".start_frame";
	setAttr -k on ".end_frame" 24;
createNode camera -n "CHECK_camShape" -p "CHECK_cam";
	setAttr -k off ".v";
	setAttr ".rnd" no;
	setAttr ".cap" -type "double2" 1.4173 0.9449 ;
	setAttr ".ff" 0;
	setAttr ".ncp" 0.01;
	setAttr ".coi" 36.39346314103323;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "camera1";
	setAttr ".den" -type "string" "camera1_depth";
	setAttr ".man" -type "string" "camera1_mask";
	setAttr ".tp" -type "double3" 1368.9432207031252 874.33189050676151 735.32883421778581 ;
select -ne :time1;
	setAttr ".o" 1;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :lightList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".fs" 1;
	setAttr ".ef" 10;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
// End of default_cam.ma
