//Maya ASCII 2009 scene
//Name: rot_startMid_02.ma
//Last modified: Sat, Mar 06, 2010 04:39:47 PM
//Codeset: 1252
requires maya "2009";
requires "stereoCamera" "10.0";
currentUnit -l meter -a degree -t pal;
fileInfo "application" "maya";
fileInfo "product" "Maya Unlimited 2009";
fileInfo "version" "2009 Service Pack 1a";
fileInfo "cutIdentifier" "200904080023-749524";
fileInfo "osv" "Microsoft Windows Vista Service Pack 1 (Build 6001)\n";
createNode joint -n "joint1";
	addAttr -ci true -sn "Vlist_9" -ln "Vlist_9" -at "message";
	addAttr -ci true -sn "Vlist_8" -ln "Vlist_8" -at "message";
	addAttr -ci true -sn "Vlist_7" -ln "Vlist_7" -at "message";
	addAttr -ci true -sn "Vlist_6" -ln "Vlist_6" -at "message";
	addAttr -ci true -sn "Vlist_5" -ln "Vlist_5" -at "message";
	addAttr -ci true -sn "Vlist_4" -ln "Vlist_4" -at "message";
	addAttr -ci true -sn "Vlist_3" -ln "Vlist_3" -at "message";
	addAttr -ci true -sn "Vlist_2" -ln "Vlist_2" -at "message";
	addAttr -ci true -sn "Vlist_1" -ln "Vlist_1" -at "message";
	addAttr -ci true -sn "options" -ln "options" -dt "string";
	addAttr -ci true -sn "builder" -ln "builder" -dt "string";
	addAttr -ci true -sn "package" -ln "package" -dt "string";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
	setAttr ".options" -type "string" "attrName='rot_interpolate', limbSegment=1";
	setAttr ".builder" -type "string" "mf.tools.rot.startMid";
	setAttr ".package" -type "string" "_suffix";
createNode joint -n "split6_JNT" -p "joint1";
	addAttr -ci true -sn "Vlist_goal_0" -ln "Vlist_goal_0" -at "message";
	setAttr ".t" -type "double3" 0.6442011539994712 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
createNode joint -n "split5_JNT" -p "split6_JNT";
	addAttr -ci true -sn "Vlist_goal_0" -ln "Vlist_goal_0" -at "message";
	setAttr ".t" -type "double3" 0.64420115399947064 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
createNode joint -n "split4_JNT" -p "split5_JNT";
	addAttr -ci true -sn "Vlist_goal_0" -ln "Vlist_goal_0" -at "message";
	setAttr ".t" -type "double3" 0.6442011539994712 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
createNode joint -n "split3_JNT" -p "split4_JNT";
	addAttr -ci true -sn "Vlist_goal_0" -ln "Vlist_goal_0" -at "message";
	setAttr ".t" -type "double3" 0.64420115399947064 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
createNode joint -n "split2_JNT" -p "split3_JNT";
	addAttr -ci true -sn "Vlist_goal_0" -ln "Vlist_goal_0" -at "message";
	setAttr ".t" -type "double3" 0.6442011539994712 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
createNode joint -n "split1_JNT" -p "split2_JNT";
	addAttr -ci true -sn "Vlist_goal_0" -ln "Vlist_goal_0" -at "message";
	setAttr ".t" -type "double3" 0.6442011539994712 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
createNode joint -n "joint2" -p "split1_JNT";
	addAttr -ci true -sn "Vlist_goal_0" -ln "Vlist_goal_0" -at "message";
	setAttr ".t" -type "double3" 0.64420115399947064 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "___";
	setAttr ".radi" 2;
createNode joint -n "null_joint1";
	addAttr -ci true -sn "Vlist_goal_0" -ln "Vlist_goal_0" -at "message";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 5.5;
createNode transform -n "nurbsCircle1";
	addAttr -ci true -sn "Vlist_goal_0" -ln "Vlist_goal_0" -at "message";
	setAttr ".t" -type "double3" 0 2.3659866802742258 0 ;
createNode nurbsCurve -n "nurbsCircleShape1" -p "nurbsCircle1";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.78361162489122504 4.7982373409884682e-017 -0.78361162489122382
		-1.2643170607829326e-016 6.7857323231109134e-017 -1.1081941875543879
		-0.78361162489122427 4.7982373409884707e-017 -0.78361162489122427
		-1.1081941875543879 1.9663354616187859e-032 -3.2112695072372299e-016
		-0.78361162489122449 -4.7982373409884701e-017 0.78361162489122405
		-3.3392053635905195e-016 -6.7857323231109146e-017 1.1081941875543881
		0.78361162489122382 -4.7982373409884713e-017 0.78361162489122438
		1.1081941875543879 -3.6446300679047921e-032 5.9521325992805852e-016
		0.78361162489122504 4.7982373409884682e-017 -0.78361162489122382
		-1.2643170607829326e-016 6.7857323231109134e-017 -1.1081941875543879
		-0.78361162489122427 4.7982373409884707e-017 -0.78361162489122427
		;
select -ne :time1;
	setAttr ".o" 1;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :lightList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".fs" 1;
	setAttr ".ef" 10;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
connectAttr "joint1.s" "split6_JNT.is";
connectAttr "joint1.Vlist_1" "split6_JNT.Vlist_goal_0";
connectAttr "split6_JNT.s" "split5_JNT.is";
connectAttr "joint1.Vlist_2" "split5_JNT.Vlist_goal_0";
connectAttr "split5_JNT.s" "split4_JNT.is";
connectAttr "joint1.Vlist_3" "split4_JNT.Vlist_goal_0";
connectAttr "split4_JNT.s" "split3_JNT.is";
connectAttr "joint1.Vlist_4" "split3_JNT.Vlist_goal_0";
connectAttr "split3_JNT.s" "split2_JNT.is";
connectAttr "joint1.Vlist_5" "split2_JNT.Vlist_goal_0";
connectAttr "split2_JNT.s" "split1_JNT.is";
connectAttr "joint1.Vlist_6" "split1_JNT.Vlist_goal_0";
connectAttr "split1_JNT.s" "joint2.is";
connectAttr "joint1.Vlist_7" "joint2.Vlist_goal_0";
connectAttr "joint1.Vlist_8" "null_joint1.Vlist_goal_0";
connectAttr "joint1.Vlist_9" "nurbsCircle1.Vlist_goal_0";
// End of rot_startMid_02.ma
