//Maya ASCII 2009 scene
//Name: rot_startEnd_01.ma
//Last modified: Sat, Mar 06, 2010 06:10:56 PM
//Codeset: 1252
requires maya "2009";
requires "stereoCamera" "10.0";
currentUnit -l meter -a degree -t pal;
fileInfo "application" "maya";
fileInfo "product" "Maya Unlimited 2009";
fileInfo "version" "2009 Service Pack 1a";
fileInfo "cutIdentifier" "200904080023-749524";
fileInfo "osv" "Microsoft Windows Vista Service Pack 1 (Build 6001)\n";
createNode joint -n "joint1";
	addAttr -ci true -sn "Vlist_goal_0" -ln "Vlist_goal_0" -at "message";
	setAttr ".t" -type "double3" -0.049364037839737654 0 -1.9385880436407459 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 1.2722218725854064e-014 89.999999999999986 0 ;
	setAttr ".radi" 2;
createNode joint -n "split12_JNT" -p "joint1";
	setAttr ".t" -type "double3" 0.90829190406938753 -2.0168131699569159e-016 -2.0428103653102881e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
createNode joint -n "split11_JNT" -p "split12_JNT";
	setAttr ".t" -type "double3" 0.90829190406938898 -2.0168131699569191e-016 -2.0428103653102881e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
createNode joint -n "split10_JNT" -p "split11_JNT";
	setAttr ".t" -type "double3" 0.9082919040693872 -2.0168131699569159e-016 -2.1316282072803005e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
createNode joint -n "split9_JNT" -p "split10_JNT";
	setAttr ".t" -type "double3" 0.90829190406938953 -2.0168131699569191e-016 -1.9539925233402755e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
createNode joint -n "split8_JNT" -p "split9_JNT";
	setAttr ".t" -type "double3" 0.90829190406938665 -2.0168131699569152e-016 -2.0428103653102881e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
createNode joint -n "split7_JNT" -p "split8_JNT";
	addAttr -ci true -sn "Vlist_goal_0" -ln "Vlist_goal_0" -at "message";
	setAttr ".t" -type "double3" 0.90829190406938898 -2.0168131699569191e-016 -2.0428103653102881e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
createNode joint -n "split6_JNT" -p "split7_JNT";
	setAttr ".t" -type "double3" 0.90829190406938776 -2.0168131699569164e-016 -1.9539925233402755e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
createNode joint -n "split5_JNT" -p "split6_JNT";
	setAttr ".t" -type "double3" 0.90829190406938898 -2.0168131699569191e-016 -2.1316282072803005e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
createNode joint -n "split4_JNT" -p "split5_JNT";
	setAttr ".t" -type "double3" 0.90829190406938776 -2.0168131699569164e-016 -1.9539925233402755e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
createNode joint -n "split3_JNT" -p "split4_JNT";
	setAttr ".t" -type "double3" 0.90829190406938665 -2.0168131699569139e-016 -2.0428103653102881e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
createNode joint -n "split2_JNT" -p "split3_JNT";
	setAttr ".t" -type "double3" 0.90829190406938776 -2.0168131699569191e-016 -2.0428103653102881e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
createNode joint -n "split1_JNT" -p "split2_JNT";
	setAttr ".t" -type "double3" 0.90829190406939009 -2.0168131699569191e-016 -2.0428103653102881e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".radi" 2;
createNode joint -n "joint2" -p "split1_JNT";
	setAttr ".t" -type "double3" 0.90829190406938776 -2.0168131699569164e-016 -2.3092638912203257e-016 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jot" -type "string" "___";
	setAttr ".radi" 2;
createNode joint -n "joint1_drv";
	addAttr -ci true -sn "Vlist_4" -ln "Vlist_4" -at "message";
	addAttr -ci true -sn "Vlist_3" -ln "Vlist_3" -at "message";
	addAttr -ci true -sn "Vlist_2" -ln "Vlist_2" -at "message";
	addAttr -ci true -sn "Vlist_1" -ln "Vlist_1" -at "message";
	addAttr -ci true -sn "options" -ln "options" -dt "string";
	addAttr -ci true -sn "builder" -ln "builder" -dt "string";
	addAttr -ci true -sn "package" -ln "package" -dt "string";
	setAttr ".t" -type "double3" -0.049364037839737654 0 -1.9385880436407459 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".radi" 6.9;
	setAttr ".options" -type "string" "attrName='soft_interpolate'";
	setAttr ".builder" -type "string" "mf.tools.rot.startEnd";
	setAttr ".package" -type "string" "_suffix";
createNode joint -n "joint2_drv";
	addAttr -ci true -sn "Vlist_goal_0" -ln "Vlist_goal_0" -at "message";
	setAttr ".t" -type "double3" -0.049364037839737675 -2.6218571209439925e-015 -13.746382796542791 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".jo" -type "double3" 0 89.999999999999986 0 ;
	setAttr ".radi" 6.9;
createNode transform -n "nurbsCircle1";
	addAttr -ci true -sn "Vlist_goal_0" -ln "Vlist_goal_0" -at "message";
	setAttr ".t" -type "double3" -2.2702516623282287 2.0551748407581751 -8.0022694615372014 ;
createNode nurbsCurve -n "nurbsCircleShape1" -p "nurbsCircle1";
	setAttr -k off ".v";
	setAttr ".cc" -type "nurbsCurve" 
		3 8 2 no 3
		13 -2 -1 0 1 2 3 4 5 6 7 8 9 10
		11
		0.78361162489122504 4.7982373409884682e-017 -0.78361162489122382
		-1.2643170607829326e-016 6.7857323231109134e-017 -1.1081941875543879
		-0.78361162489122427 4.7982373409884707e-017 -0.78361162489122427
		-1.1081941875543879 1.9663354616187859e-032 -3.2112695072372299e-016
		-0.78361162489122449 -4.7982373409884701e-017 0.78361162489122405
		-3.3392053635905195e-016 -6.7857323231109146e-017 1.1081941875543881
		0.78361162489122382 -4.7982373409884713e-017 0.78361162489122438
		1.1081941875543879 -3.6446300679047921e-032 5.9521325992805852e-016
		0.78361162489122504 4.7982373409884682e-017 -0.78361162489122382
		-1.2643170607829326e-016 6.7857323231109134e-017 -1.1081941875543879
		-0.78361162489122427 4.7982373409884707e-017 -0.78361162489122427
		;
select -ne :time1;
	setAttr ".o" 1;
select -ne :renderPartition;
	setAttr -s 2 ".st";
select -ne :renderGlobalsList1;
select -ne :defaultShaderList1;
	setAttr -s 2 ".s";
select -ne :postProcessList1;
	setAttr -s 2 ".p";
select -ne :lightList1;
select -ne :initialShadingGroup;
	setAttr ".ro" yes;
select -ne :initialParticleSE;
	setAttr ".ro" yes;
select -ne :defaultRenderGlobals;
	setAttr ".fs" 1;
	setAttr ".ef" 10;
select -ne :hardwareRenderGlobals;
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
select -ne :defaultHardwareRenderGlobals;
	setAttr ".fn" -type "string" "im";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
connectAttr "joint1_drv.Vlist_2" "joint1.Vlist_goal_0";
connectAttr "joint1.s" "split12_JNT.is";
connectAttr "split12_JNT.s" "split11_JNT.is";
connectAttr "split11_JNT.s" "split10_JNT.is";
connectAttr "split10_JNT.s" "split9_JNT.is";
connectAttr "split9_JNT.s" "split8_JNT.is";
connectAttr "split8_JNT.s" "split7_JNT.is";
connectAttr "joint1_drv.Vlist_3" "split7_JNT.Vlist_goal_0";
connectAttr "split7_JNT.s" "split6_JNT.is";
connectAttr "split6_JNT.s" "split5_JNT.is";
connectAttr "split5_JNT.s" "split4_JNT.is";
connectAttr "split4_JNT.s" "split3_JNT.is";
connectAttr "split3_JNT.s" "split2_JNT.is";
connectAttr "split2_JNT.s" "split1_JNT.is";
connectAttr "split1_JNT.s" "joint2.is";
connectAttr "joint1_drv.Vlist_1" "joint2_drv.Vlist_goal_0";
connectAttr "joint1_drv.Vlist_4" "nurbsCircle1.Vlist_goal_0";
// End of rot_startEnd_01.ma
