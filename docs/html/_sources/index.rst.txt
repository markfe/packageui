.. packageUI documentation master file, created by
   sphinx-quickstart on Mon Apr 22 08:31:03 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=====================================
packageUI
=====================================

PackageUI lets you link python functions to objects in maya.

For example: You can link the script thats builds a leg rig to the initial joints that you need to define the pivot points.
This way you can save a Maya file with a *template* leg that just consists of the joints and the function linked to them.
You can import this *template* for a new character, reposition the joints and *build* it when you are finished. 

General
=======

.. toctree::
   :maxdepth: 2

   install
   gettingStarted
   howItWorks
   
packageUI Menus
===============

.. toctree::
   :maxdepth: 2   
   
   packagesMenu
   functionPicker

=====   
Tools
=====

.. toctree::
   :maxdepth: 2   
   
   mirrorPose
   playblast


Indices and tables
==================

* :ref:`genindex`    
* :ref:`modindex`
* :ref:`search`

