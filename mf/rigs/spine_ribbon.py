'''
S U M M A R Y
Create cartoony ribbon spine (nurbs plane)

H I S T O R Y 
2012-08-06 proethlein start


import sys
sys.path.append(r'P:\AT\05_RIGGING\rig-tools\setupProject\src')
import mf.builder.component
reload (mf.builder.component)
mf.builder.component.UI()

import mf.rigs
reload (mf.rigs)
help(mf.rigs)
'''
import pymel.core as pm
import mf.tools.proxyGeo as pg

def spine_ribbon( Vlist=[], Vsuffix='', side=0, bindJoints=3, stretchy=1 ):
    ''' 
    Vlist: controlls, joints
    Vsuffix: package name
    bindJoints: number of skinning joints on nurbs plane
    stretchy: 0/1 (False/True)
    '''
    # store selection
    sel = pm.ls(sl=1)
    
    # check Vlist (cog, hip, fk_spine_btm, fk_spine_top, ik_spine_btm, ik_spine_mid, ik_spine_top)
    if( len(Vlist) != 7 ):
        raise NameError( 'Invalid Vlist length: %d' % (len(Vlist) ) )
    cog, hip, fk_spine_btm, fk_spine_mid, ik_spine_btm, ik_spine_mid, ik_spine_top = Vlist
    jn_spine_btm = ik_spine_btm.getChildren(type='joint')[0]
    jn_spine_mid = ik_spine_mid.getChildren(type='joint')[0]
    jn_spine_top = ik_spine_top.getChildren(type='joint')[0]
    
    # no transformation group
    gp_noTrans = pm.group( n='gp_noTrans'+Vsuffix, em=1 )
    
    # null groups and parenting of controlls
    for x, each in enumerate( Vlist ):
        gp=pm.group(em=1, n="gp_"+each)
        pm.delete(pm.parentConstraint( each, gp ))
        gp.setParent( each.getParent() )
        each.setParent( gp )
        if( x > 0 ):
            gp.setParent( cog )
    fk_spine_mid.getParent().setParent( fk_spine_btm )
    hip.getParent().setParent( ik_spine_btm )
    
    # create nurbs
    #    infos
    btmVec = ik_spine_btm.getTranslation( space='world' )
    topVec = ik_spine_top.getTranslation( space='world' )
    ribVec = btmVec - topVec
    ribLength = ( ribVec[0]*ribVec[0] + ribVec[1]*ribVec[1] + ribVec[2]*ribVec[2]  ) ** 0.5
    #    create
    ribNurbs = pm.nurbsPlane( w=ribLength, lr=ribLength/3, d=3, u=4, v=1, ch=0 )[0]
    ribNurbs.rename( 'nurbs_ribbon_'+Vsuffix )
    pm.rebuildSurface( ribNurbs, su=4, sv=1, du=3, dv=1 )
    pm.delete( pm.pointConstraint( [ik_spine_btm, ik_spine_top], ribNurbs ) )
    pm.delete( pm.aimConstraint( ik_spine_top, ribNurbs, aimVector=[0,0,1], upVector=[-1,0,0], wu=[0,0,-1] ) ) #worldUpType='scene' ))
    ribNurbs.inheritsTransform.set(0)
    ribNurbs.t.setLocked(1)
    ribNurbs.r.setLocked(1)
    ribNurbs.s.setLocked(1)
    ribNurbs.setParent( gp_noTrans )
    
    # follicles
    follicles = []
    for x in range(bindJoints):
        eachFol = pm.createNode( 'follicle' )
        eachFolTrans = eachFol.getParent()
        eachFolTrans.rename( 'fol_'+str(x)+'_nurbs'+Vsuffix )
        # connections
        ribNurbs.getShape().local >> eachFol.inputSurface
        ribNurbs.getShape().worldMatrix >> eachFol.inputWorldMatrix
        eachFol.outTranslate >> eachFolTrans.translate
        eachFol.outRotate >> eachFolTrans.rotate
        # uv pos
        eachFol.parameterU.set(0.5)
        uVal = (float(x)+0.5)/bindJoints
        eachFol.parameterU.set( uVal )
        eachFol.parameterV.set( 0.5 )
        # set attr
        eachFolTrans.inheritsTransform.set(0)
        eachFolTrans.t.setLocked(1)
        eachFolTrans.r.setLocked(1)
        eachFolTrans.s.setLocked(1)
        eachFolTrans.setParent( gp_noTrans )
        # add to list
        follicles.append( eachFolTrans )
    
    # bindjoints, orient from ctls
    skinJoints = []
    for x, eachFol in enumerate( follicles ):
        pm.select(cl=1)
        # create
        eachJoint = pm.joint( p=eachFol.getTranslation(space='world') )
        eachJoint.rename( 'jn_'+str(x)+'_spine'+Vsuffix )
        eachJoint.jointOrient.set( ik_spine_btm.getRotation(space='world' ) )
        eachJoint.setParent( eachFol )
        # attributes
        eachJoint.radius.set( 0.05 )
        eachJoint.side.set( side )
        eachJoint.attr('type').set( 18 )
        eachJoint.otherType.set( eachJoint.replace( Vsuffix, '' ) )
        # add to list
        skinJoints.append( eachJoint )
    pg.proxyGeo(Vlist=skinJoints+[jn_spine_top, jn_spine_btm], Vsuffix='_bnd', Vorient=1, 
                xScale=1, yzScale=0, yzScaleGlobal=1, Vsides=6, 
                VcenterPivot=1, chain=0, side=3, label='spine')
    
    # skin nurbs plane
    ribbon_skin = pm.skinCluster( [jn_spine_btm, jn_spine_mid, jn_spine_top], ribNurbs, dr=1, toSelectedBones=True, ignoreHierarchy=True, mi=2 )
    weights = [ [1,0,0], [.85,.15,0], [.3,.7,0], [0,1,0], [0,.7,.3], [0,.15,.85], [0,0,1] ]
    cvLength = len( ribNurbs.cv )/2 #always 7 in this script
    for x in range( cvLength ):
        pm.skinPercent( ribbon_skin, [ribNurbs.cv[x][0], ribNurbs.cv[x][1]], transformValue=[(jn_spine_top, weights[x][0]), (jn_spine_mid, weights[x][1]), (jn_spine_btm, weights[x][2]) ] )
    
    # point-constraint: ik_spine_btm + ik_spine_top >> mid_ctl
    #   group
    cgp_ik_mid = pm.group( em=1, n='cgp_topBtm_'+ik_spine_mid+Vsuffix )
    pm.delete( pm.parentConstraint( ik_spine_mid, cgp_ik_mid ) )
    cgp_ik_mid.setParent( ik_spine_mid.getParent() )
    ik_spine_mid.setParent( cgp_ik_mid )
    #   constraint
    pm.pointConstraint( [ik_spine_btm, ik_spine_top], cgp_ik_mid, mo=1 )
    
    # aim-constraint: ik_spine_top >> mid_ctl
    #   blend-aim-group for ik_spine_mid
    gp_mid_aimBlend = pm.group( em=1, n='cgp_aimBlend_'+ik_spine_mid )
    pm.delete( pm.parentConstraint( ik_spine_mid, gp_mid_aimBlend ) )
    gp_mid_aimBlend.setParent( ik_spine_mid.getParent() )
    #   aim-group
    gp_mid_aim = pm.group( em=1, n='cgp_aim_'+ik_spine_mid )
    pm.delete( pm.parentConstraint( ik_spine_mid.getParent(), gp_mid_aim ) )
    gp_mid_aim.setParent( ik_spine_mid.getParent() )
    #   up-object
    gp_aim_upObject = pm.duplicate( gp_mid_aim ).pop()
    gp_aim_upObject.rename( 'gp_upOrient_'+gp_mid_aim )
    #   group for up-object positioned at spine
    hgp_aim_upObject = pm.duplicate(gp_mid_aim).pop()
    hgp_aim_upObject.rename( 'gp_'+gp_aim_upObject )
    #       parent, move and hide
    gp_aim_upObject.setParent( hgp_aim_upObject )
    gp_aim_upObject.tx.set(5)
    gp_aim_upObject.v.set(0)
    gp_aim_upObject.v.setLocked(1)
    #   aim-constraint
    pm.aimConstraint( ik_spine_top, gp_mid_aim, wut='object', worldUpObject=gp_aim_upObject, aim=[0,1,0], wu=[0,1,0], u=[1,0,0] )
    #   create blend
    blend_ik_spine_mid_aim = pm.createNode( 'blendColors' )
    blend_ik_spine_mid_aim.rename( 'bl_aim_'+ik_spine_mid )
    blend_ik_spine_mid_aim.color2B.set( 0 )
    #       connections
    gp_mid_aim.r >> blend_ik_spine_mid_aim.color1
    blend_ik_spine_mid_aim.output >> gp_mid_aimBlend.r
    #   blend attribute
    attr_blend_string = 'aimOnTopCtl'
    ik_spine_mid.addAttr( attr_blend_string, min=0, max=1, dv=1, k=1 )
    attr_blend = ik_spine_mid.attr( attr_blend_string )
    attr_blend >> blend_ik_spine_mid_aim.blender
    #   distribute rotation from top and bottom-controller
    pm.orientConstraint( [ik_spine_top, ik_spine_btm], hgp_aim_upObject, mo=1, skip=['x', 'z'] )
    #   finalize
    ik_spine_mid.setParent( gp_mid_aimBlend )
    
    # FK on top
    fk_spine_btm.getParent().setParent( cog )
    # fk -> ribbon ctl constraints
    pm.parentConstraint( fk_spine_mid, ik_spine_top.getParent(), mo=True )
    # point-constraint on mid
    #    group
    gp_mid_pcon_YZ = pm.group( em=True, n='cgp_point_mid_XZ'+Vsuffix )
    pm.delete( pm.parentConstraint( ik_spine_mid, gp_mid_pcon_YZ ) )
    gp_mid_pcon_YZ.setParent( ik_spine_mid.getParent() )
    ik_spine_mid.setParent( gp_mid_pcon_YZ )
    #    constrain
    pm.pointConstraint( fk_spine_mid, ik_spine_mid.getParent(), skip=['y'] )
    pm.pointConstraint( ik_spine_mid.getParent().getParent(), ik_spine_mid.getParent(), skip=['y'], w=0 )
    
    
    for e in Vlist:
        e.s.setLocked(1)
        #pm.setAttr(e.s, cb=0, k=0)
    
    
    # stretchy
    if( stretchy ):
        # get objects to work with
        mds_spine = []
        # scale rig
        scaleRigMd = pm.createNode( 'multiplyDivide' )
        scaleRigMd.rename( 'md_scaleRig'+Vsuffix )
        scaleRigMd.operation.set(1)
        scaleRigMd.input1.set(1,1,1)
        scaleRigAttr = scaleRigMd.outputY
        scaleRigAttrComp = scaleRigMd.output
        print( 'Scale rig package attribute: %s.input1X' % (scaleRigMd) )
        for eachJoint in skinJoints:
            #    multiplydivide for each joint
            eachMd = pm.createNode( 'multiplyDivide', n='md_'+eachJoint )
            eachMd.output >> eachJoint.s
            # connections ctl_main.s >> multiply-divide
            scaleRigAttrComp >> eachMd.input1
            # finalize
            mds_spine.append( eachMd )
        # end for eachJoint
        # distribute multiplydivides into groups: upper, lower, mid
        mds_scale_lower = []
        mds_scale_upper = []
        mds_scale_mid = []
        mds_half_length = float( len(mds_spine) ) /2
        for x, md_each in enumerate( mds_spine ):
            if( x == (mds_half_length - 0.5) ):
                mds_scale_mid.append( md_each )
            elif( x > mds_half_length or x == mds_half_length ):
                mds_scale_lower.append( md_each )
            elif( x < mds_half_length ):
                mds_scale_upper.append( md_each )
        # reverse lower
        mds_scale_lower.reverse()
        #
        # keep volume
        stretch_lower = Stretch( ik_spine_btm, ik_spine_mid, mds_scale_lower, "input2Y", scaleRigAttr )
        stretch_upper = Stretch( ik_spine_mid, ik_spine_top, mds_scale_upper, "input2Y", scaleRigAttr )
        #    finalize nodes
        stretch_lower.get_dist_transform().setParent( gp_noTrans )
        stretch_upper.get_dist_transform().setParent( gp_noTrans )
        # volume
        volume_btm_mid = KeepVolume( cog, ik_spine_btm, stretch_lower.get_md_stretch().outputY, mds_scale_lower, "input2X", "input2Z", "btm_spine"+Vsuffix )
        volume_mid_top = KeepVolume( cog, ik_spine_top, stretch_upper.get_md_stretch().outputY, mds_scale_upper, "input2X", "input2Z", "top_spine"+Vsuffix )
        # MID ############
        # stretch
        pm_mid_stretch = pm.createNode( 'plusMinusAverage', n='pma_avg_mid_stretch'+Vsuffix )
        pm_mid_stretch.operation.set(3)
        #    connections
        stretch_lower.get_md_stretch().outputY >> pm_mid_stretch.input1D[0]
        stretch_upper.get_md_stretch().outputY >> pm_mid_stretch.input1D[1]
        # volume
        #    plus-minus
        pm_mid_volume = pm.createNode( 'plusMinusAverage' )
        pm_mid_volume.rename( 'pma_avg_mid_volume'+Vsuffix)
        pm_mid_volume.operation.set(3)
        #        connections
        volume_btm_mid.get_bc_volumeBlend().outputR >> pm_mid_volume.input2D[0].input2Dx
        volume_btm_mid.get_bc_volumeBlend().outputB >> pm_mid_volume.input2D[0].input2Dy
        volume_mid_top.get_bc_volumeBlend().outputR >> pm_mid_volume.input2D[1].input2Dx
        volume_mid_top.get_bc_volumeBlend().outputB >> pm_mid_volume.input2D[1].input2Dy
        #    multiply-divide
        md_mid_volume = pm.createNode( 'multiplyDivide' )
        md_mid_volume.rename( 'md_mid_volume'+Vsuffix)
        md_mid_volume.input1.set(1,1,1)
        #        connections
        pm_mid_volume.output2Dx >> md_mid_volume.input2X
        pm_mid_volume.output2Dy >> md_mid_volume.input2Z
        #    manual scale
        ik_spine_mid.s >> md_mid_volume.input1
        #ik_spine_mid.sy >> md_mid_volume.input1Y
        #ik_spine_mid.sz >> md_mid_volume.input1Z
        # fix volume
        if volume_btm_mid.get_attr_nw_x():
            md_mid_volume.outputX >> volume_btm_mid.get_attr_nw_x()
            md_mid_volume.outputZ >> volume_btm_mid.get_attr_nw_y()
            md_mid_volume.outputX >> volume_mid_top.get_attr_nw_x()
            md_mid_volume.outputZ >> volume_mid_top.get_attr_nw_y()
        # fix mid joint
        if mds_scale_mid:
            #pm_mid_stretch.output1D >> mds_scale_mid[0].input2Y
            #
            md_mid_volume.outputX >> mds_scale_mid[0].input2X
            md_mid_volume.outputZ >> mds_scale_mid[0].input2Z
        #
    # end stretch
    hip.t.setLocked(1)
    #hip.r.setLocked(1)
    hip.s.setLocked(1)
    
    # recreate initial selection
    pm.select( sel )
#

class Stretch:
    ''' 
    create stretchyness between given start and end transform and connect to inputObjects.
    while working with manual scaling of local controls
    '''
    # variables
    __md_stretch = None
    __md_scale = None
    __dist_transform = None
    # getter
    def get_md_stretch(self):
        return self.__md_stretch
    def get_md_scale(self):
        return self.__md_scale
    def get_dist_transform(self):
        return self.__dist_transform
    def __init__(self, transform_start, transform_end, input_nodes, input_attr, scalable_attr ):
        '''
        Creates distance node between (and use as local scale-objects):
        - transform_start 
        - transform_end
        connects stretch value to:
        - input_nodes [] (should start "at" transform_start and "end" at transform_end ) 
        - input_attr
        and keeps rig scalable if
        - scalable_attr is given
        '''
        # make distanceNode
        myDist = pm.createNode( 'distanceDimShape' )
        myDist.getParent().rename( 'dst_'+transform_start+'_to_'+transform_end )
        self.__dist_transform = myDist.getParent()
        #    locStart
        locStart = pm.spaceLocator()
        locStart.rename( 'loc_start_'+myDist.getParent() )
        locStart.setTranslation( transform_start.getTranslation(space='world'), space='world' )
        locStart.setParent( transform_start )
        locStart = locStart.getShape()
        locStart.localScale.set(0.1, 0.1, 0.1)
        #        connect
        locStart.worldPosition[0] >> myDist.startPoint
        #    locEnd
        locEnd = pm.spaceLocator()
        locEnd.rename( 'loc_end_'+myDist.getParent() )
        locEnd.setTranslation(transform_end.getTranslation(space='world'), space='world')
        locEnd.setParent( transform_end )
        locEnd = locEnd.getShape()
        locEnd.localScale.set(0.1, 0.1, 0.1)
        #        connect
        locEnd.worldPosition[0] >> myDist.endPoint
        # create mdScale
        mdScale = pm.createNode( 'multiplyDivide' )
        mdScale.rename( 'md_scale_'+myDist.getParent() )
        #mdScale.input1.set( 1,1,1 )
        #mdScale.input2Y.set( scalable_attr.get() )
        mdScale.input2Y.set( myDist.distance.get() )
        #    connections
        if scalable_attr:
            scalable_attr >> mdScale.input1Y
        #    finalize
        self.__md_scale = mdScale
        # create mdStretch
        mdStretch = pm.createNode( 'multiplyDivide' )
        mdStretch.rename( 'md_stretch_'+myDist.getParent() )
        mdStretch.operation.set( 2 )
        myDist.distance >> mdStretch.input1Y
        mdScale.outputY >> mdStretch.input2Y
        #     connect to input_transforms
        for eachNode in input_nodes:
            #mdStretch.outputY >> eachNode.attr( input_attr )
            pass
        #     finalize
        self.__md_stretch = mdStretch
    # end def __init__
# end Stretch
#
class KeepVolume:
    ''' 
    When given transform is stretched, this class adds a blendable attribute,
    which simulates a constant volume.
    also a manual scaling will be made.
    '''
    # variables
    __name = 'keepVolume'
    __bc_volumeBlend = None
    __attr_blend = None
    __attr_nw_x = None
    __attr_nw_y = None
    # getter
    def getName(self):
        return self.__name
    def get_bc_volumeBlend(self):
        return self.__bc_volumeBlend
    def getBlendAttr(self):
        return self.__attr_blend
    def get_attr_nw_x(self):
        return self.__attr_nw_x
    def get_attr_nw_y(self):
        return self.__attr_nw_y
    
    # methods
    def __init__(self, node_attr, ctl_manualScale, attr_driver, nodes_driven, attr_drivenX, attr_drivenZ, suffix_arg, limitKeepVolume_arg=1 ):
        '''
        creates node-network for keep-volume functionality
        - node_attr (will receive keepVolume attribute)
        - ctl_manualScale (for manual scaling)
        - attr_driver (usually stretch-multiplyidivde.outputX)
        - nodes_driven [] (usually joints)
        - attr_drivenX ("input1X" or "scaleX")
        - attr_drivenZ ("input1Z" or "scaleZ")
        - suffix_arg (suffix string for utilitynodes)
        - limitKeepVolume_arg (Only keep volume for a limited range)
        '''
        # add attribute
        attr_volume_string = 'keepVolume'
        try:
            node_attr.attr( attr_volume_string ).get()
        except:
            node_attr.addAttr( attr_volume_string, min=0, max=1, dv=0, k=False )
        attr_blendVolume = node_attr.attr( attr_volume_string )
        self.__attr_blend = attr_blendVolume
        # blend-colors for blending keepVolume with attribute
        bc_volume = pm.createNode( 'blendColors' )
        bc_volume.rename( 'bl_'+self.getName()+suffix_arg )
        bc_volume.color2.set(1,1,1)
        attr_blendVolume >> bc_volume.blender
        #    finalize
        self.__bc_volumeBlend = bc_volume
        # keep volume (limited SDK setup OR unlimited multiplyDivide setup)
        if limitKeepVolume_arg:
            # limited SDK setup
            for eachDrivenAttr in ( bc_volume.color1R, bc_volume.color1B ):
                pm.setDrivenKeyframe( eachDrivenAttr, dv=1, v=1, itt='spline', ott='spline', cd=attr_driver )
                pm.setDrivenKeyframe( eachDrivenAttr, dv=1.3, v=0.93, itt='flat', ott='flat', cd=attr_driver )
                pm.setDrivenKeyframe( eachDrivenAttr, dv=0.7, v=1.07, itt='flat', ott='flat', cd=attr_driver )
                #
                pm.setDrivenKeyframe( eachDrivenAttr, dv=1.075, v=0.96, itt='spline', ott='spline', cd=attr_driver )
                pm.setDrivenKeyframe( eachDrivenAttr, dv=0.925, v=1.04, itt='spline', ott='spline', cd=attr_driver )
        else:
            # unlimited multiplyDivide setup
            md_volumeCalc = pm.createNode( 'multiplyDivide' )
            md_volumeCalc.rename( 'md_'+self.getName()+'_calc_'+suffix_arg )
            md_volumeCalc.input1.set(1,1,1)
            md_volumeCalc.operation.set(2)
            #    connections
            attr_driver >> md_volumeCalc.input2Y
            attr_driver >> md_volumeCalc.input2Z
            #
            md_volumeCalc.outputY >> bc_volume.color1R
            md_volumeCalc.outputZ >> bc_volume.color1B
        # manual volume scale setup on top
        md_volumeManual = pm.createNode( 'multiplyDivide' )
        md_volumeManual.rename( 'md_'+self.getName()+'_manual_'+suffix_arg )
        #    connections
        bc_volume.outputR >> md_volumeManual.input2X
        bc_volume.outputB >> md_volumeManual.input2Z
        ctl_manualScale.sx >> md_volumeManual.input1X
        ctl_manualScale.sz >> md_volumeManual.input1Z
        # final connection to driven-nodes
        #    first driven can be connected directly
        md_volumeManual.outputX >> nodes_driven[0].attr( attr_drivenX )
        md_volumeManual.outputZ >> nodes_driven[0].attr( attr_drivenZ )
        if( len(nodes_driven) > 1 ):
            # multiple driven-nodes
            #    create placeholder node, which will be connected later from outside of the class
            nw_placeholder = pm.createNode( 'network' )
            nw_placeholder.rename( 'nw_'+self.getName()+suffix_arg )
            attr_nw_x_string = 'driverX'
            attr_nw_y_string = 'driverY'
            nw_placeholder.addAttr( attr_nw_x_string, dv=1 )
            nw_placeholder.addAttr( attr_nw_y_string, dv=1 )
            attr_nw_x = nw_placeholder.attr( attr_nw_x_string )
            attr_nw_y = nw_placeholder.attr( attr_nw_y_string )
            #        finalize
            self.__attr_nw_x = attr_nw_x
            self.__attr_nw_y = attr_nw_y
            # plus-minus node --- 50 / 50
            pm_ctl_nw = pm.createNode( 'plusMinusAverage' )
            pm_ctl_nw.rename( 'pma_'+'0_'+self.getName()+suffix_arg )
            pm_ctl_nw.operation.set(3)
            #    connections
            md_volumeManual.outputX >> pm_ctl_nw.input2D[0].input2Dx
            md_volumeManual.outputZ >> pm_ctl_nw.input2D[0].input2Dy
            attr_nw_x >> pm_ctl_nw.input2D[1].input2Dx
            attr_nw_y >> pm_ctl_nw.input2D[1].input2Dy
            # create further plus-minus nodes, depending on driven-nodes
            if( len(nodes_driven) > 2 ):
                # pm with network input and "half-half" pma
                pm_nw_nwctl = pm.createNode( 'plusMinusAverage' )
                pm_nw_nwctl.rename( 'pma_'+'1_'+self.getName()+suffix_arg )
                pm_nw_nwctl.operation.set(3)
                #    inputs
                attr_nw_x >> pm_nw_nwctl.input2D[0].input2Dx
                attr_nw_y >> pm_nw_nwctl.input2D[0].input2Dy
                pm_ctl_nw.output2Dx >> pm_nw_nwctl.input2D[1].input2Dx
                pm_ctl_nw.output2Dy >> pm_nw_nwctl.input2D[1].input2Dy
                if( len(nodes_driven) > 3):
                    # pm with ctl input and pm with average of both ctl input
                    pm_ctl_ctlnw = pm.createNode( 'plusMinusAverage' )
                    pm_ctl_ctlnw.rename( 'pma_'+'2_'+self.getName()+suffix_arg )
                    pm_ctl_ctlnw.operation.set(3)
                    #    inputs
                    md_volumeManual.outputX >> pm_ctl_ctlnw.input2D[0].input2Dx
                    md_volumeManual.outputZ >> pm_ctl_ctlnw.input2D[0].input2Dy
                    pm_ctl_nw.output2Dx >> pm_ctl_ctlnw.input2D[1].input2Dx
                    pm_ctl_nw.output2Dy >> pm_ctl_ctlnw.input2D[1].input2Dy
            # create connections from plus-minus nodes to input_nodes
            if( len(nodes_driven) == 2):
                pm_driver_ls = [ pm_ctl_nw ]
            elif( len(nodes_driven) == 3 ):
                pm_driver_ls = [ pm_ctl_nw, pm_nw_nwctl ]
            elif( len(nodes_driven) >= 4 ):
                pm_driver_ls = [ pm_ctl_ctlnw, pm_ctl_nw, pm_nw_nwctl ]
            # connections
            for x, eachInput in enumerate( nodes_driven[1:] ):
                # find correct plus-minus for input-node
                if( x < len(pm_driver_ls)-1 ):
                    inputDriver = pm_driver_ls[x]
                else:
                    inputDriver = pm_driver_ls[-1]
                # connect
                inputDriver.output2Dx >> eachInput.attr( attr_drivenX )
                inputDriver.output2Dy >> eachInput.attr( attr_drivenZ )
        #
    # end def __init__
# end class KeepVolume
