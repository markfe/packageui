
import pymel.core as pm
import mf.builder.nodes as bn
import mf.tools.lsSl as lsSl
import mf.tools.parent as tp
import mf.tools.mesh as tm
import mf.tools.snap as snap


class make_ctls():
    def __init__(self, Vlist=[], Vsuffix=''):
        """
        make driver, make driven
        """
        Vlist = lsSl.lsSl(Vlist, 2, 2)
        self.suffix = Vsuffix
        drv = Vlist[0]
        #self.edit = Vlist[0]
        drv.rename('drv' + self.suffix)
        
        self.ctl = tp.nullGroup(Vlist=[drv], Vhierarchy=1, Vtype=2, 
                           Vname='ctl' + self.suffix)[0]
        self.edit = pm.duplicate(drv, n='edit' + self.suffix)[0]
        inv = tp.nullGroup(Vlist=[self.ctl], Vhierarchy=1, Vtype=2, 
                           Vname='inv' + self.suffix)[0]
        
        self.null = tp.nullGroup(Vlist=[self.ctl, drv, inv], Vhierarchy=3, 
                            Vtype=2, freez=0, Vprefix='null_')    
        inv|self.null[0]
        multInv = pm.createNode('multiplyDivide', 
                                      name='invScaleCtrl'+Vsuffix)
        multInv.operation.set (1)
        multInv.input1.set([-1, -1, -1])
        self.ctl.t >> multInv.input2
        multInv.output >> inv.t
        self.ctl.t >> drv.t 
        self.fC = tm.follicleOnPoly([self.edit,Vlist[1]])
        
        pm.delete(drv.getShapes())
        for e in pm.listRelatives(self.edit, children=1, shapes=0):
            if not e in pm.listRelatives(self.edit, shapes=1):
                pm.delete(e)

     
    def finish(self):
        """
        Cleanup and after positioning the ctl shapes.
        """
        pm.delete(self.fC.vector_product, self.fC.cpom)
        pm.pointConstraint(self.fC.foll.getParent(), self.null[2], mo=1)
        em = pm.group(em=1, w=1, n='em')
        snap.snap([em, self.edit])
        emB = pm.group(em=1, w=1, n='emB')
        tp.parentShapeFreez([self.edit, em])
        snap.snap([emB, em])
        tp.parentShapeFreez([em, self.ctl])
        pm.delete(self.edit, em, emB)
        
class wire_ctls():
    """
    Select some joints bound to a geometry. These joints should have the 
    desired controller shape. Last, select a mesh that will transport the 
    controllers. Initialize el setup:
    wireCls = mf.rigs.face_wire.wire_ctls()
    Place the controllers. Than finish the setup:
    wireCls.finsh()

    ========== ===================================================
    Arguments  Description
    ========== ===================================================
    Vlist      [bind joints with controller shape, mesh]
    ========== ===================================================
    """
    def __init__(self, Vlist=[], Vsuffix=''):
        Vlist = lsSl.lsSl(Vlist, 2, 0)
        self.suffix = Vsuffix
        self.mesh = Vlist[-1]
        self.ctlClas = []
        
        ctls = Vlist[:-1]
        for i in range(len(ctls)):
            self.ctlClas.append(make_ctls([ctls[i], self.mesh], 
                                          Vsuffix=self.suffix + '_' + str(i)))
    
    def finish(self):
        noTrans = tp.noTrans(em=1, w=1, n='no_trans' + self.suffix).group
        grpCtls = pm.group(em=1, w=1, n='grp_ctls' + self.suffix)
        grpDrv = pm.group(em=1, w=1, n='grp_drv' + self.suffix)
        for e in self.ctlClas:
            e.finish()
            noTrans|e.fC.foll.getParent()
            grpCtls|e.null[2]
            grpDrv|e.null[1]
            
        
        
        
          
        
        