"""
Eye setup ik/fk. Deformable.  
"""
import pymel.core as pm
import mf.builder.nodes
from mf.tools import lsSl 
from mf.tools import snap
from mf.tools import parent as tp
from mf.tools import cons as tc
from mf.tools import curve as tcv
from mf.tools import proxyGeo as pg


class eye():
    """
    Eye setup ik/fk. with lattice to deform the eyeball, lid automatic and 
    manual rotation. 
    
    Setup will be oriented to the fk ctrl. the pivot of this ctrl must be in
    the middle of the eyeball.
    the aim and the up Vector should be matching the orientation of the 
    fk control.  
 
    Returns:
        The main group and the ik_ctrl.s
    
    ========== ======================================================
    Arguments  Description
    ========== ======================================================
    Vlist[0]   ik_ctrl
    Vlist[1]   fk_ctrl
    Vlist[2]   eye geometry (same pivot as fk_ctrl)
    Vlist[3]   lid control (same pivot as fk_ctrl)
    Vlist[4]   ctrl to receive attributes
    VaimVector aim vector
    VupVector  up vector (has no effect if worldUpType is 'none')
    Vsuffix    suffix 
    ========== ======================================================      
    """ 
    def __init__(self, Vlist=[], VaimVector=[0, 0, 1], VupVector=[1, 0, 0], 
                 Vsuffix='', side=3, LatticeDivisions=[2,2,2]):    
    
        Vlist = lsSl.lsSl(Vlist, 6, 6)   
                            
        self.VikCtrl = pm.rename(Vlist[0], 'ik_ctrl' + Vsuffix)
        VfkCtrl = pm.rename(Vlist[1], 'fk_ctrl' + Vsuffix)
        eyeGeo = pm.rename(Vlist[2], 'eyeball' + Vsuffix)
        VlidCtl = pm.rename(Vlist[3], 'lid_ctrl' + Vsuffix)
        socket = pm.rename(Vlist[4], 'main_ctrl' + Vsuffix)
        VattrObj = pm.rename(Vlist[5], str(Vlist[5]) + Vsuffix)
        
        mf.builder.nodes.set([socket, self.VikCtrl, VfkCtrl, VlidCtl], 
                             'transform_ctrl')
        
        self.nullMain = tp.nullGroup(Vlist=[socket], 
                                     Vprefix='null_', Vhierarchy=3)[0]
        
        self.Vroot = pm.group(w=1, empty=1, name='root' + Vsuffix)   
        VaimSource = pm.group(w=1, empty=1, name='aimSource' + Vsuffix)
        VaimGoal = pm.group(w=1, empty=1, name='aimGoal' + Vsuffix)   
        lidFollow = pm.group(w=1, empty=1, name='lid_follow' + Vsuffix)
        
        VlidSpace = pm.group(w=1, empty=1, name='lidSpace' + Vsuffix)
               
        pm.select(cl=1) 
        
        VattrObj.addAttr('socket_ctrl' + Vsuffix, defaultValue=0, min=0, max=1, 
                         keyable=1, at='long')
        ao_socket_ctrl = pm.ls(str(VattrObj) + '.socket_ctrl' + Vsuffix)[0]
        ao_socket_ctrl >> socket.getShapes()[0].v
        
        
        VattrObj.addAttr('lid_ctrl' + Vsuffix, defaultValue=0, min=0, max=1, 
                         keyable=1, at='long')
        ao_lid_ctrl = pm.ls(str(VattrObj) + '.lid_ctrl' + Vsuffix)[0]
        ao_lid_ctrl >> VlidCtl.v
        
        VattrObj.addAttr('fk_ctrl' + Vsuffix, defaultValue=1, min=0, max=1, 
                         keyable=1, at='long')
        ao_fk_ctrl = pm.ls(str(VattrObj) + '.fk_ctrl' + Vsuffix)[0]
        ao_fk_ctrl  >> VfkCtrl.getShape().v
        
        snap.snap ([VaimSource, lidFollow, self.Vroot, VlidCtl, 
                    VlidSpace, VfkCtrl])
        snap.snap ([VaimGoal, self.VikCtrl])
        
        pg.proxyGeo(Vlist=[VlidCtl, socket, VfkCtrl], Vsuffix='_geo', Vorient=1, 
                        xScale=1, yzScale=1, customGeo=0, 
                        Vsides=4, VcenterPivot=1, chain=0
                        ,side=side)
        
        
        eyeBallPar = tp.nullGroup(Vlist=[VaimSource], 
                                  Vhierarchy=4, Vtype=1, 
                                  Vname='eyeballParent' + Vsuffix)[0]
                                  
        lidPar = tp.nullGroup(Vlist=[VlidSpace], 
                                  Vhierarchy=4, Vtype=1, 
                                  Vname='lidParent' + Vsuffix)[0]
        
        self.nullFk = tp.nullGroup(Vlist=[VfkCtrl], 
                                   Vprefix='null_', Vhierarchy=3)[0]
        
        
        self.VikCtrl|VaimGoal
        self.Vroot|self.nullMain
        socket|VaimSource|self.nullFk
        eyeBallPar|eyeGeo
        VfkCtrl |lidFollow
        socket|VlidSpace|VlidCtl
        
        VfkCtrl.r >> eyeBallPar.r
        VlidCtl.t >> lidPar.t
        VlidCtl.r >> lidPar.r
        VlidCtl.s >> lidPar.s
        
        if 0:
            pm.aimConstraint([VaimSource, VfkCtrl], offset=[0, 0, 0], 
                             sk=['x','z','none'], weight=1)
        
        pm.aimConstraint (VaimGoal, VaimSource, 
                          #worldUpType='none', 
                          worldUpType='objectrotation',
                          worldUpObject=self.nullMain,
                          mo=1, 
                          weight=1, 
                          aimVector=VaimVector, 
                          upVector=VupVector,
                          worldUpVector=VupVector)
        
        lidFix = tp.nullGroup(Vlist=[VfkCtrl], Vhierarchy=1, 
                                     Vtype=1, Vsuffix='lid_fix_')[0]
                                     
        socket|lidFix
        
        tc.constraintSwitch(Vlist=[lidFollow, lidFix, VlidSpace, VattrObj], 
            VattrName='lid_auto_rot' + Vsuffix, Vsuffix='', Vtype=2, Voffset=0)
        aO_lid_auto_rot = pm.ls(str(VattrObj) + '.lid_auto_rot' + Vsuffix)[0]
        
        Vlatt = pm.lattice(eyeGeo, divisions=LatticeDivisions, cp=0, objectCentered=1, 
            ldv=[2,2,2], ol=1, n='lattice' + Vsuffix)
        
        socket|Vlatt[1]
        socket|Vlatt[2]
        
        self.VikGroup = tp.nullGroup(Vlist=[self.VikCtrl], Vhierarchy=2, 
                                     Vtype=1, Vprefix='null_')[0]
        Vline = tcv.aimLine([VaimGoal, self.Vroot], Vsuffix=('aim' + Vsuffix))
     
        # cleanup ------------------------------------------------------------- 
        self.Vgroup = pm.group(em=1, n='grp' + Vsuffix) 
        self.Vgroup|Vline[0] 
        self.Vgroup|self.VikGroup
        self.Vgroup|self.Vroot
        
        aO_lid_auto_rot.set(0.1)
        
        self.VikCtrl.sx.set (lock=1, keyable=0, channelBox=0)
        self.VikCtrl.sy.set (lock=1, keyable=0, channelBox=0)
        self.VikCtrl.sz.set (lock=1, keyable=0, channelBox=0)
        self.VikCtrl.rx.set (lock=1, keyable=0, channelBox=0)
        self.VikCtrl.ry.set (lock=1, keyable=0, channelBox=0)
        self.VikCtrl.rz.set (lock=1, keyable=0, channelBox=0)   
        self.VikCtrl.v.set (lock=1, keyable=0, channelBox=0)
          
        VfkCtrl.sx.set (lock=1, keyable=0, channelBox=1)
        VfkCtrl.sy.set (lock=1, keyable=0, channelBox=1)
        VfkCtrl.sz.set (lock=1, keyable=0, channelBox=1)
        VfkCtrl.tx.set (lock=1, keyable=0, channelBox=1)
        VfkCtrl.ty.set (lock=1, keyable=0, channelBox=1)
        VfkCtrl.tz.set (lock=1, keyable=0, channelBox=1)
        VfkCtrl.ry.set (lock=1, keyable=0, channelBox=1)    
        VfkCtrl.v.set (lock=1, keyable=0, channelBox=0)
        
        VlidCtl.v.set (lock=1, keyable=0, channelBox=0)


class aimAt():
    """
    Aim at for two ik ctrls.

    ========= ==========================
    Arguments Description
    ========= ==========================
    Vlist[0]  ctrl
    Vlist[1]  aiming object (curve)
    Vlist[2]  parent (e.g. head)
    ========= ==========================
    """    
    def __init__(self, Vlist=[], Vsuffix='', VaimVector=[1, 0, 0], 
                 VupVector=[0, 1, 0]):
    
        Vlist = lsSl.lsSl(Vlist, 3, 3)
        
        self.ctrl = pm.rename(Vlist[0], 'aim_ctrl' + Vsuffix)
        self.aimObj = pm.rename(Vlist[1], 'aimObj' + Vsuffix)
        
        mf.builder.nodes.set([self.ctrl], 'transform_ctrl')
        
        self.par = Vlist[2]  
        
        self.nullCtrl, nullAimObj = tp.nullGroup(Vlist=[self.ctrl, self.aimObj], 
                                                  Vprefix='null_', Vhierarchy=3)
        aim = tp.nullGroup(Vlist=[nullAimObj], Vprefix='aim_', Vhierarchy=4)[0] 
        nullAimObj.setParent(self.par)
        
        tc.cons(Vlist=[self.ctrl, nullAimObj], Vtype=1, Voffset=1, Vsuffix='')
        
        pm.aimConstraint (self.par, aim, 
                          #worldUpType='none', 
                          mo=1, weight=1, 
                          aimVector=VaimVector, upVector=VupVector,
                          worldUpType='objectrotation',
                          worldUpObject=self.par
                          )
        
        tc.constraintSwitch(Vlist=[aim, self.ctrl, self.aimObj, self.ctrl], 
                            VattrName='auto_rot', Vsuffix='', Vtype=3, 
                            Voffset=0)
       
        self.ctrl.sy.set (lock=1, keyable=0, channelBox=0)
        self.ctrl.sz.set (lock=1, keyable=0, channelBox=0)   
        self.ctrl.v.set (lock=1, keyable=0, channelBox=0)
        
        self.ctrl.sx >> self.aimObj.sx

        
class eyes():
    """
    Two eye setup.
    
    ========= ==========================
    Arguments Description
    ========= ==========================
    Vlist     eyeR, eyeL, aimAt
    ========= ==========================
    """
    def __init__(self, Vlist=[], Vsuffix='',
             VaimVectorR=[1, 0, 0], VupVectorR=[0, 1, 0], 
             VaimVectorL=[1, 0, 0], VupVectorL=[0, 1, 0],
             VaimVectorA=[1, 0, 0], VupVectorA=[0, 1, 0],
             LatticeDivisions=[2,2,2]):
    
        Vlist = lsSl.lsSl(Vlist, 13, 13)
        
        Vlist[-4].addAttr('eye_r', defaultValue=0, min=0, keyable=1)
        Vlist[-4].eye_r.setLocked(1)
        fk = Vlist[-1].rename('fk' + Vsuffix)
        
        nullFk = tp.nullGroup(Vlist=[fk], Vhierarchy=3, Vsuffix='_null', Vtype=1)[0]
        nullNullFk = tp.nullGroup(Vlist=[nullFk], Vhierarchy=3, Vsuffix='_null', Vtype=1)[0]
        pm.aimConstraint (Vlist[-4], nullFk, 
                          #worldUpType='none', 
                          worldUpType='objectrotation',
                          worldUpObject=nullNullFk,
                          mo=1, 
                          weight=1, 
                          aimVector=VaimVectorR, 
                          upVector=VupVectorR,
                          worldUpVector=VupVectorR)
        
        
        nullR = eye(Vlist=Vlist[0:5]+[Vlist[-4]], VaimVector=VaimVectorR, 
                    VupVector=VupVectorR, Vsuffix=Vsuffix+'R', side=2, 
                    LatticeDivisions=LatticeDivisions)
        
        Vlist[-4].addAttr('eye_l', defaultValue=0, min=0, keyable=1)
        Vlist[-4].eye_l.setLocked(1)
         
        nullL = eye(Vlist=Vlist[5:10]+[Vlist[-4]], VaimVector=VaimVectorL, 
                    VupVector=VupVectorL, Vsuffix=Vsuffix+'L', side=1,
                    LatticeDivisions=LatticeDivisions)
        
        allFkR = tp.nullGroup(Vlist=[nullR.nullFk], Vhierarchy=1, 
                              Vsuffix='_aim', Vtype=1)[0]
        snap.snapRot([allFkR, Vlist[-1]])
        nullAllFkR = tp.nullGroup(Vlist=[allFkR], Vhierarchy=1, 
                              Vsuffix='_aim', Vtype=1)[0]
        nullR.nullFk.getParent()|nullAllFkR|allFkR|nullR.nullFk
        fk.r >> allFkR.r
        
        allFkL = tp.nullGroup(Vlist=[nullL.nullFk], Vhierarchy=1, 
                              Vsuffix='_aim', Vtype=1)[0]
        snap.snapRot([allFkL, Vlist[-1]])
        nullAllFkL = tp.nullGroup(Vlist=[allFkL], Vhierarchy=1, 
                              Vsuffix='_aim', Vtype=1)[0]
        nullL.nullFk.getParent()|nullAllFkL|allFkL|nullL.nullFk
        fk.r >> allFkL.r
        
        
   
        
        Vlist[-4].addAttr('aim_at', defaultValue=0, min=0, keyable=1)
        Vlist[-4].aim_at.setLocked(1)
        
        aim = aimAt(Vlist=Vlist[10:13], Vsuffix=Vsuffix+'A', 
                    VaimVector=VaimVectorA, VupVector=VupVectorA)
       
        nullR.Vroot.setParent(Vlist[12])
        nullL.Vroot.setParent(Vlist[12])
        
        nullR.VikGroup.setParent(Vlist[11])
        nullL.VikGroup.setParent(Vlist[11])
        
        invScaleA = pm.createNode('multiplyDivide', 
                                  name='invScaleCtrl'+Vsuffix)
        invScaleA.operation.set (2)
        invScaleA.input1.set([1, 1, 1])
        aim.ctrl.sx >> invScaleA.input2X
        invScaleA.outputX >> nullR.VikGroup.sx
        invScaleA.outputX >> nullL.VikGroup.sx
        
        
        Vlist[-3].getShapes()[0].template.set(1)
        
        pm.rename(Vlist[-2], 'trans' + Vsuffix) 
        
        noTrans = tp.noTrans(em=1, w=1, n='no_trans' + Vsuffix).group
        noTrans | nullR.Vgroup 
        noTrans | nullL.Vgroup
        
        mainTrans = tp.mainTrans(em=1, w=1, n='main_trans' + Vsuffix).group
        
        glMainTrans = pm.group(em=1, w=1, n='goal_main_trans' + Vsuffix)
        mainTrans | glMainTrans
        mainTrans | aim.nullCtrl
        #print Vlist[-3]
        tc.constraintSwitch(Vlist=[aim.par, glMainTrans, aim.nullCtrl, Vlist[-4]], 
                            VattrName='fix', Vname='', Vsuffix='', Vtype=3, Voffset=1)
        Vlist[-4].fix.set(1)
        
        
        aim.ctrl.addAttr('ik_vis', defaultValue=0, min=0, max=1, 
                         keyable=1, at='long')
        
        aim.ctrl.ik_vis >> nullR.Vgroup.v  
        aim.ctrl.ik_vis >> nullL.Vgroup.v
        aim.ctrl.ik_vis >> aim.aimObj.v
        
        
        
        Vlist[-1].tx.set (lock=1, keyable=0, channelBox=0)
        Vlist[-1].ty.set (lock=1, keyable=0, channelBox=0)
        Vlist[-1].tz.set (lock=1, keyable=0, channelBox=0)
        Vlist[-1].sx.set (lock=1, keyable=0, channelBox=0)
        Vlist[-1].sy.set (lock=1, keyable=0, channelBox=0)
        Vlist[-1].sz.set (lock=1, keyable=0, channelBox=0)
        #Vlist[-1].rx.set (lock=1, keyable=0, channelBox=0)
        Vlist[-1].ry.set (lock=1, keyable=0, channelBox=0)
        #Vlist[-1].rz.set (lock=1, keyable=0, channelBox=0)   
        Vlist[-1].v.set (lock=1, keyable=0, channelBox=0)
        
        Vlist[-2]|nullNullFk
        
        
        
        
        
        
           