import pymel.core as pm

import mf.builder.nodes
from mf.tools import lsSl
from mf.tools import snap
from mf.tools import distance as td
from mf.tools import key as tk
from mf.tools import curve as tcv
from mf.tools import parent as tp
from mf.tools import joint as tj
from mf.tools import cons as tc
from mf.tools import proxyGeo as pg
from mf.tools import rot as rt
from mf.tools import bend as tb
from mf.tools import general as tg
import mf.tools.ribbon as rb
from mf.rigs import appendage as ra
import limb_lug as ll

class tail():
    """
    Simple spline ik tail setup.
    
    ========== ===================================================
    Arguments  Description
    ========== ===================================================
    Vlist[0:1] Two joint chain for the stretchy spine
    Vlist[2:]  Controllers parented as a chain.
    ========== ===================================================
    
    TODO:
    Space switch world to chest.
    """
    def __init__(self, Vlist=[], Vsuffix='', numJoints=20, numCtls=7, 
                 ctlRadius=1, side=1, parentSpace=1):
        
        self.suffix = Vsuffix
        self.ctls = pm.listRelatives(Vlist[2], ad=1, type='joint')
        
        self.ctls.append(Vlist[2])
        self.ctls.reverse()
        
        for i in range(len(self.ctls)):
            self.ctls[i].rename('tail_'+str(i))
            
        numCtls=len(self.ctls)
        
        splits = tj.split(Vlist[0:2], numJoints=numJoints)
        splineC = tb.stretchySpline(Vlist[0:2], Vsuffix)
        splineC.rebuildCurve(numCtls-1)
        clusterC = tb.clusterComponent(Vlist=[splineC.curve], Vsuffix=Vsuffix, 
                                   ctrl_radius=ctlRadius, normal=[0,0,1])
        
        for i in range(len(clusterC.ctrls)):
            
            pm.rename(clusterC.ctrls[i], 'bend_' + str(i) + Vsuffix)
            snap.snap([clusterC.ctrls[i].getParent(), self.ctls[i]])
            self.ctls[i]|clusterC.ctrls[i].getParent()
            
            
        clusterC.clusterLoc[0]|Vlist[0]
        
        
        self.gT = tp.trans(em=1, w=1, n='trans' + Vsuffix).group
        self.gNT = tp.noTrans(em=1, w=1, n='no_trans' + Vsuffix).group
        self.gMT = tp.mainTrans(em=1, w=1, n='main_trans' + Vsuffix).group
        
        self.gST = pm.group(em=1, w=1, n='scale_trans' + Vsuffix)
        self.gSNT = pm.group(em=1, w=1, n='scale_no_trans' + Vsuffix)
        pm.scaleConstraint(self.gST, self.gSNT, skip=['none', 'none', 'none'])
        
        pm.select(cl=1)
        inbGoal = pm.joint()
        snap.snap([inbGoal,  Vlist[0]])
        clusterC.clusterLoc[0]|inbGoal
        
        tj.jointInbCons(Vlist=[Vlist[0], inbGoal], Vsuffix='',
                        Vproxy=1, interpolateScale=1, 
                        label='',
                        proxyScale=(splineC.crvInf.arcLength.get()/18), 
                        side=3, type=2,)
        
        splits[-1].s >> Vlist[1].inverseScale
        
        handBnd = tp.nullGroup(Vlist=[Vlist[1]], Vhierarchy=4, Vtype=2, 
                               Vsuffix='_noScale', connectInverseScale=1)[0]
                               
                               
        self.gSNT.setParent(self.gNT)
        self.gST.setParent(self.gMT)
        
        splineC.connectScale(self.gSNT.sx)
        self.gNT|clusterC.clusterGrp
        self.gNT|splineC.curve
        self.gNT|splineC.handle
        
        self.gMT|clusterC.ctrlGrp
        
        self.gT|clusterC.null_clusterLoc[0]
        
        mf.builder.nodes.set(self.ctls, 'transform_ctrl')
        pg.proxyGeo(Vlist=[Vlist[0]] + splits + [Vlist[1]], yzScaleGlobal=0.05)
        
        
        for c in clusterC.ctrls:
            attrList = [c.sx, c.sy, c.sz,c.v]
            
            for attr in attrList:
                attr.set (lock=1, channelBox=0, keyable=0)
                
        pg.proxyGeo(Vlist=[handBnd], Vprefix='', Vname='', 
                    Vsuffix='_bnd', Vorient=1, xScale=1.1, 
                    yzScale=0, yzScaleGlobal=0.1, customGeo=0, 
                    Vsides=6, VcenterPivot=1,
                    chain=0, shader=1, Vcolor=[0.2, 0.4, 0.4], 
                    shaderName='shoulder_shader',
                    side=side, label='non_scale')
        
        clusterC.ctrls[-1].addAttr('twist')
        clusterC.ctrls[-1].twist.setKeyable(1)
        clusterC.ctrls[-1].twist >> splineC.handle.twist
        
        ctlsNull = tp.nullGroup(Vlist=self.ctls, Vhierarchy=3, Vtype=2, 
                            Vsuffix='_null', connectInverseScale=1)
        self.gT|ctlsNull[0]
        
        # space switches
        spaceSwitches = False
        if spaceSwitches:
            for i in range(len(clusterC.null_clusterLoc)-1):
                    space1 = tp.nullGroup(Vlist=[clusterC.null_clusterLoc[i+1]], Vhierarchy=1, Vtype=2, 
                                       Vsuffix='_space', connectInverseScale=1)[0]
                    space2 = tp.nullGroup(Vlist=[clusterC.null_clusterLoc[i+1]], Vhierarchy=1, Vtype=2, 
                                       Vsuffix='_space', connectInverseScale=1)[0]
                    tc.constraintSwitch(Vlist=[space1, space2, clusterC.null_clusterLoc[i+1], clusterC.clusterLoc[i+1]], 
                                        VattrName='space', Vsuffix='', Vtype=3, Voffset=1)
                    
                    if parentSpace:
                        clusterC.clusterLoc[i]|space2
        self.spaceSwitches()
    
    def spaceSwitches(self):
        """space switches for all controllers to local and world"""
        

        dummyGoalCOG = tp.nullGroup(Vlist=[self.ctls[0]], 
                                  Vhierarchy=1, Vtype=2, 
                                  Vname='dumyGl_COG', 
                                  Vsuffix=self.suffix)[0]
            
        self.gNT|dummyGoalCOG
        
        zero = tc.zeroOutConstraint(Vlist=[self.gMT, self.gT, dummyGoalCOG,
                                    self.ctls[0].getParent(), self.ctls[0]],
                                    VlastObj=1)

        tc.spaceSwitch(headlineAttr=1,
                       Vlist=zero, 
                       VattrName=['world', 'hip', 'cog'], 
                       Vtype=2, VlastObj=1, Voffset=1, skipTranslate=['x','y','z'])
        


            
            
         
