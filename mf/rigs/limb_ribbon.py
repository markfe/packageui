"""
two joint setup for limbs (arms or legs) 
"""
import pymel.core as pm

import mf.builder.nodes
from mf.tools import lsSl
from mf.tools import snap
from mf.tools import distance as td
from mf.tools import key as tk
from mf.tools import parent as tp
from mf.tools import proxyGeo as pg
from mf.tools import cons as tc
from mf.tools import rot as rt
import mf.tools.ribbon as tr
import mf.tools.joint as tj
from mf.rigs import appendage as ra
import mf.rigs.limb as rl
import limb_lug as ll

class limb_ribbon():
    """
    ============= =====================================================
    Arguments     Description
    ============= =====================================================
    proxy         Add proxy geometry.
    side          1=left, 2=right
    twistOffset   Offset for inverse pol vector e.g. for quadrupeds front 
                  legs             
    foot          Add foot setup
    foot_ik       Obsolete 
    ikSwitchCtl   Obsolete
    softKeys      2= softer, 3=harder
    numBindJoints Number of bind joints for each ribbon section. 
    autoRoll      Foot autoroll setup.ss
    ============= =====================================================
    """
    def __init__(self, Vlist=[], Vsuffix='', proxy=1, side=2, twistOffset=0,
                 foot=0, foot_ik=1, ikSwitchCtl='Ik_switches_CON',softKeys=3,
                 numBindJoints=8, autoRoll=1, ball_ik=0):
        
        Vlist = lsSl.lsSl(Vlist, 13, 13)
        self.Vlist = Vlist
        
        self.suffix = Vsuffix   
        self.side = side
        self.twistOffset = twistOffset
        self.softKeys = softKeys
        self.numBindJoints = numBindJoints
        self.jointA = Vlist[0].rename('jointA' + self.suffix) 
        self.jointB = Vlist[1].rename('jointB' + self.suffix)
        self.jointC = Vlist[2].rename('jointC' + self.suffix)
        self.ctrlEnd = Vlist[3].rename('ctrlEnd' + self.suffix)
        self.ctrlPolv = Vlist[4].rename('ctrlPolv' + self.suffix)
        self.ctrlFkB = Vlist[5].rename('ctrlFkB' + self.suffix)
        self.midA = Vlist[6].rename('midA' + self.suffix)
        self.midB = Vlist[7].rename('midB' + self.suffix)
        self.ribbonMid = Vlist[8].rename('ribbonMid' + self.suffix)
        self.footList = Vlist[9:16]
        self.attrObj = self.jointA
        self.autoRoll = autoRoll
        self.wristExtra = Vlist[16].rename('wristExtra' + self.suffix) 
        self.midC = Vlist[17].rename('midC' + self.suffix)
        
        
        mf.builder.nodes.set([self.jointA,
                              self.jointB,
                              self.ctrlEnd,
                              self.ctrlPolv,
                              self.midA,
                              self.midB,
                              self.ribbonMid,
                              self.wristExtra,
                              self.midC], 
                             'transform_ctrl')
        
        if self.ctrlFkB.getShape():
            mf.builder.nodes.set([self.ctrlFkB], 
                             'transform_ctrl')
        
        self.swName = '_____'
        self.attrObj.addAttr(self.swName, k=1)
        self.attrObj.attr(self.swName).setLocked(1)

        self.swName = ''
        
        self.handlePar = tp.nullGroup(Vlist=[self.ctrlEnd], Vhierarchy=4, Vtype=1, 
                     Vname = 'handlePar' + self.suffix)[0]
        snap.snap([self.handlePar, self.footList[0]])
        
        self.gT = tp.trans(em=1, w=1, n='trans' + self.suffix).group
        self.gNT = tp.noTrans(em=1, w=1, n='no_trans' + self.suffix).group
        self.gMT = tp.mainTrans(em=1, w=1, n='main_trans' + self.suffix).group
        
        self.gST = pm.group(em=1, w=1, n='scale_trans' + Vsuffix)
        self.gSNT = pm.group(em=1, w=1, n='scale_no_trans' + Vsuffix)
        pm.scaleConstraint(self.gST, self.gSNT, skip=['none', 'none', 'none'])
        self.gSNT.setParent(self.gNT)
        self.gST.setParent(self.gMT)
        
        self.jointASource = tp.nullGroup(Vlist=[self.jointA], 
                            Vhierarchy=1, 
                            Vtype=2, 
                            Vprefix='sc_', Vsuffix='')[0]
        
        self.stretch()
        self.stretchDistribution()
        
        self.wrist()
        self.ribbon()
        
        if foot:
            self.foot_ik()
        
        if proxy:
            pg.proxyGeo(Vlist=Vlist[0:3], Vsuffix='_geo', Vorient=1, 
                        xScale=1, yzScale=0, yzScaleGlobal=0.06, customGeo=0, 
                        Vsides=8, side=self.side)
            
        null = tp.nullGroup(Vlist=[self.jointA, self.ctrlEnd, self.ctrlPolv], 
                            Vhierarchy=3, 
                            Vtype=2, 
                            Vprefix='null_')
        
        self.jointA.jointOrient.set(null[0].jointOrient.get())
        null[0].jointOrient.set(0,0,0)
        
        # This joint preserves the original orientation of jointA
        self.jointA|self.jointASource
        
        try:
            # This fails if the transform is not a joint (no jointOrient) 
            self.ctrlEnd.jointOrient.set(null[1].jointOrient.get())
            null[1].jointOrient.set(0,0,0)
        except:
            pass
        
        self.polVector()
        
        self.spaceSwitches()
        self.cleanupAttributes()
        self.gT|self.VobStart
        
        self.gT|null[0].getParent()
        self.gMT|null[1]
        self.gMT|null[2]
        
        if ball_ik:
            self.ballIK()
    
    def ballIK(self):
        """
        Setup to use the "ball" of a quadruped leg for the Ik pivot.
        """
        self.jointBIA = self.Vlist[18].rename('BIA' + self.suffix) 
        self.jointBIB = self.Vlist[19].rename('BIB' + self.suffix) 
        self.jointBIC = self.Vlist[20].rename('BIC' + self.suffix) 
        self.jointBIPv = self.Vlist[21].rename('BIPv' + self.suffix) 
        
        bAO = pm.spaceLocator(n='ballIKAttr' + self.suffix)
        strCls = rl.stretchScale([self.jointBIA, self.jointBIB, self.jointBIC, bAO], Vsuffix='_bAO'+self.suffix)
        VstretchOut = strCls.strchCls()
     
        self.jointA.length_A >> bAO.lengthAB
        self.jointA.soft_ik >> bAO.soft_stretch
           
        self.rollTip|strCls.strchCls.VobEnd
        self.jointBIC|self.null_rollBall
        
        # pol vector 
        pm.poleVectorConstraint(self.jointBIPv, VstretchOut[2][0])
        self.nullPv = tp.nullGroup(Vlist=[self.jointBIPv], Vhierarchy=3, 
                                            Vtype=2, Vprefix='null_')[0]
        #distance for the shift wrist
        self.wristDist = td.dist([self.shiftWrist, self.inv_rollBall])
        
        # Wrist and lower arm length
        self.wristDist
        
        AB = self.wristDist.distance()
        bc = td.dist([self.jointB, self.jointC])
        
        BC = bc.distance()
        AC = AB+BC
        
        blend = pm.createNode('blendColors', name='blendMid' + self.suffix)
        
        mid = AB/AC
        
        blend.blender.set(mid)
   
        #self.wristDist.connect([blend.color1G])
        self.ctrlEnd.length_foot >> blend.color1G
        self.jointA.length_B >> blend.color2G
        
        blend.outputG >> bAO.lengthBC
        
        self.wristDist.calibrate()
        self.wristDist.delete()
        bc.delete()
        
        #Parenting 
        self.gT|strCls.strchCls.VobStart
        self.gT|bAO
        self.gT|self.jointBIA
        self.gT|strCls.scaleCls.distGrp
        self.ctrlPolv.getParent()|self.nullPv
        self.jointBIPv|self.ctrlPolv
        strCls.strchCls.dC.parent([self.gT])
        

    def wrist(self):
        """
        Wrist to fk constraint blend. 
        """
        snap.snap([self.ctrlFkB, self.jointC])
        self.jointC|self.ctrlFkB
        
        nullWrist = tp.nullGroup(Vlist=[self.ctrlFkB], Vhierarchy=3, Vtype=2, 
                               Vsuffix='_cons')[0]
        
        goalWrist = tp.nullGroup(Vlist=[self.ctrlFkB], Vhierarchy=1, 
                                      Vtype=2, 
                                      Vsuffix='_goal')[0]
        self.ctrlEnd|goalWrist
        
        tc.constraintSwitch(Vlist=[self.jointC, goalWrist, nullWrist, 
                                   self.ctrlFkB], VattrName='wrist_to_fk', 
                            Vsuffix='', Vtype=2, Voffset=0)
        self.ctrlFkB.wrist_to_fk.set(1)
        
    def stretch(self):
        """
        Stretch and soft_ik setup for IK. Length scaling for FK.
        """
        
        VdistA= td.get([self.jointA, self.jointB])
        VdistB = td.get([self.jointB, self.jointC])
        VdistAB = VdistA+VdistB
        self.VdistAB = VdistAB
        
        self.VobStart = pm.spaceLocator (name='obStart' + self.suffix)
        self.VobEnd = pm.spaceLocator (name='obEnd' + self.suffix)
        
        self.attrObj.addAttr(self.swName+'ik_fk', defaultValue=1, min=0, max=1,keyable=1)
        self.attrObj.addAttr(self.swName+'stretch', defaultValue=0, min=0, max=1, 
                        keyable=1)
        self.attrObj.addAttr(self.swName+'soft_ik', defaultValue=0, min=0, max=1, 
                        keyable=1)
        self.ctrlEnd.addAttr('twist', defaultValue=0, keyable=1)
        
        self.attrObj.addAttr(self.swName+'length_A', defaultValue=1, keyable=1)
        self.attrObj.addAttr(self.swName+'length_B', defaultValue=1, keyable=1)
    
        self.VadjLenDiv = pm.createNode('multiplyDivide', 
                                name='adjLengthDiv' + self.suffix)
        self.VadjLenDiv.operation.set (2)
    
        VblendSoftKeys = pm.createNode('blendColors', 
                                       name='blendSoftStretchKeys' + self.suffix)
        VblendSoftKeys.blender.set (1)
        VblendSoftKeys.color1R.set (2)
        VblendSoftKeys.color2R.set (2)
        
        blendStretch = pm.createNode('blendColors', 
                                     name='blendStretch' + self.suffix)
        blendStretch.color2R.set (1)
 
        VblendSoftKeys.outputR>>blendStretch.color1R
    
        self.VadjLenMult = pm.createNode('multiplyDivide', 
                                         name='adjLengthMult' + self.suffix)
        self.VadjLenMult.operation.set (1)
    
        snap.snap ([self.VobStart, self.jointA])
        snap.snap ([self.VobEnd, self.jointC])
        
        self.Vhandle = pm.ikHandle(startJoint=self.jointA,
                                   endEffector=self.jointC, 
                                   solver='ikRPsolver',
                                   name='handle' + self.suffix)
        pm.rename(self.Vhandle[1], 'effector' + self.suffix)
        
        self.Vhandle[0].setParent(self.VobEnd)
        
        
        
        pm.pointConstraint(self.handlePar, self.Vhandle[0])
        
        reverse_ik = pm.createNode('reverse', name='revIK' + self.suffix)
        self.attrObj.attr(self.swName+'ik_fk') >> reverse_ik.inputX
        reverse_ik.outputX >> self.Vhandle[0].ikBlend
    
        self.dC = td.dist([self.VobStart, self.VobEnd])
        self.dC.parent([self.gT])
        
        if self.twistOffset:
            twistOffsetAdd = pm.createNode('plusMinusAverage', 
                                           name='twistOffsetAdd' + self.suffix)
            twistOffsetAdd.input2D[1].input2Dx.set(self.twistOffset)
            self.ctrlEnd.twist >> twistOffsetAdd.input2D[0].input2Dx
            twistOffsetAdd.output2Dx >> self.Vhandle[0].twist
        else:    
            self.ctrlEnd.twist >> self.Vhandle[0].twist
        
        
        self.blendStretchOff = pm.createNode('blendColors', 
                                    name='blend_StretchOff' + self.suffix)
        
        
        self.attrObj.attr(self.swName+'ik_fk') >> self.blendStretchOff.blender
        self.blendStretchOff.color1R.set(0)
        self.blendStretchOff.color1G.set(0)
        
        self.attrObj.attr(self.swName+'stretch') >> self.blendStretchOff.color2R
        self.attrObj.attr(self.swName+'soft_ik') >> self.blendStretchOff.color2G
        
        self.dC.connect([self.VadjLenDiv.input1X]) 
        blendStretch.outputR >>  self.VadjLenMult.input1X
        blendStretch.outputR >>  self.VadjLenMult.input1Y
        self.blendStretchOff.outputR >> blendStretch.blender
        self.blendStretchOff.outputG >> VblendSoftKeys.blender
        
        # hard keys -----------------------------------------------------------
        # linear
        tk.drvKeySet(valList=[[1.00*VdistAB, 1], 
                              [2.00*VdistAB, 2]], 
                              VattrDriver=self.VadjLenDiv.outputX, 
                              VattrDriven=VblendSoftKeys.color2R)
        # soft keys -----------------------------------------------------------
        if self.softKeys == 2:
            # gets long (1.13) but is very soft
            tk.drvKeySet(valList=[[0.57*VdistAB, 1], 
                                  [0.65*VdistAB, 0.993], 
                                  [0.74*VdistAB, 0.978], 
                                  [0.88*VdistAB, 0.977], 
                                  [0.94*VdistAB, 0.99323], 
                                  [1.00*VdistAB, 1.022775], 
                                  [1.05*VdistAB, 1.056885],
                                  [1.09*VdistAB, 1.091235],  
                                  [1.13*VdistAB, 1.13],
                                  # this must be set as a linear key
                                  [1.2*VdistAB, 1.2], 
                                  [2.00*VdistAB, 2]], 
                                  VattrDriver=self.VadjLenDiv.outputX, 
                                  VattrDriven=VblendSoftKeys.color1R)
            
        elif self.softKeys == 3:
            # very good blending (longest 1.06) 
            tk.drvKeySet(valList=[[0.57*VdistAB, 1], 
                                  [0.65*VdistAB, 0.993229], 
                                  [0.74*VdistAB, 0.978269], 
                                  [0.88*VdistAB, 0.976143], 
                                  [0.94*VdistAB, 0.983745], 
                                  [1.00*VdistAB, 1.010038], 
                                  [1.04*VdistAB, 1.04101], 
                                  [1.07*VdistAB, 1.07], 
                                  [2.00*VdistAB, 2]], 
                                  VattrDriver=self.VadjLenDiv.outputX, 
                                  VattrDriven=VblendSoftKeys.color1R)
     
        pm.keyTangent (VblendSoftKeys.color1R, itt='spline', ott='spline')
        pm.keyTangent (VblendSoftKeys.color2R, itt='spline', ott='spline')
        pm.keyTangent (VblendSoftKeys.color1R, itt='spline', ott='linear', 
                    f=[(1.07*VdistAB)])
    #    keyTangent (VblendSoftKeys.color2R, itt='spline', ott='linear', f=[(VlinKeyB)])
        pm.setInfinity (VblendSoftKeys.color1R, poi='linear')
        pm.setInfinity (VblendSoftKeys.color2R, poi='linear')
    
        
        # CLEANUP
        pm.pointConstraint(self.jointA, self.VobStart)
        self.handlePar|self.VobEnd
        # Connect if setup finishes here don't, connect if there are other 
        # setups that affect the joint-length.
        Vconnect = 1
        if Vconnect==1:
            self.VadjLenMult.outputX >> self.jointA.sx
            self.VadjLenMult.outputX >> self.jointB.sx
            
        # this is the elbow or knee inb.
        proxyScale = 1
        self.midInb =  tj.jointInbCons([self.jointASource, self.jointB],
                                  proxyScale=proxyScale, side=self.side,
                                  label='midInb', Vproxy=0)
        

    def polVector(self):
        """
        Pol vector setup is different for a foot or arm setup.
        """
        pm.poleVectorConstraint(self.ctrlPolv, self.Vhandle[0])    
            
    def stretchDistribution(self):
        """
        Distributes the stretch to the lower and upper limb.
        """
        distA= td.get([self.jointA, self.jointB])
        distB = td.get([self.jointB, self.jointC])
        distAB = distA+distB
        
        blendSectionLen = pm.createNode('blendColors', 
                                    name='blend_section_len' + self.suffix)
        blendSectionLen.blender.set((1/distAB)*distA)
        self.attrObj.attr(self.swName+'length_A') >> blendSectionLen.color1R
        self.attrObj.attr(self.swName+'length_B') >> blendSectionLen.color2R
        blendSectionLen.outputR >> self.VadjLenDiv.input2X
        
        
        self.attrObj.attr(self.swName+'length_A') >> self.VadjLenMult.input2X
        self.attrObj.attr(self.swName+'length_B') >> self.VadjLenMult.input2Y
        
        self.VadjLenMult.outputX >> self.jointA.sx
        self.VadjLenMult.outputY >> self.jointB.sx

    def cleanupAttributes(self):
        """
        Lock and hide attributes
        """
        for jnt in [self.jointA]:
            attrList = [jnt.tx, jnt.ty, jnt.tz,
                        jnt.sx, jnt.sy, jnt.sz, 
                        jnt.v, 
                        jnt.radius]
            for attr in attrList:
                attr.set (lock=1, keyable=0, channelBox=0)
                
        for jnt in [self.jointB]:
            attrList = [jnt.tx, jnt.ty, jnt.tz,
                        jnt.sx, jnt.sy, jnt.sz, 
                        jnt.v, 
                        jnt.radius]
            for attr in attrList:
                attr.set (lock=1, keyable=0, channelBox=0)
        
        for e in [self.ctrlFkB]:
            attrList = [e.tx, e.ty, e.tz,
                        e.sx, e.sy, e.sz, 
                        e.v]
            for attr in attrList:
                attr.set (lock=1, keyable=0, channelBox=0)
              
        
        for jnt in [self.ctrlEnd, self.ctrlPolv]:
            attrList = [jnt.sx, jnt.sy, jnt.sz, 
                        jnt.v,]
            for attr in attrList:
                attr.set (lock=1, keyable=0, channelBox=0)
        
        attrList = [self.ctrlPolv.rx, self.ctrlPolv.ry, self.ctrlPolv.rz,]
        for attr in attrList:
            attr.set (lock=1, channelBox=0, keyable=0)
        
        for attr in attrList:
            attr.set (lock=1, channelBox=0, keyable=0)
            
        for e in [self.jointA, self.jointB, self.ctrlFkB, 
                  self.ctrlEnd, self.ctrlPolv]:
            e.rotateOrder.setKeyable(1)
            
        attrList = [self.wristExtra.rx, self.wristExtra.ry, self.wristExtra.rz,
                    self.wristExtra.sx, self.wristExtra.sy, self.wristExtra.sz,
                    self.wristExtra.v]
        for attr in attrList:
            attr.set (lock=1, channelBox=0, keyable=0)
        
            
    def ribbon(self):
        """
        Add the ribbon.
        """
        #upper ribbon
        midA = pm.spaceLocator(n='midALoc'+self.suffix)
        midB = pm.spaceLocator(n='midBLoc'+self.suffix)
        #midC = pm.spaceLocator(n='midC'+self.suffix)
        pm.delete(pm.orientConstraint(self.jointA, self.jointB, self.ribbonMid))
        snap.snapTrans([midA, midB, self.ribbonMid, self.jointB])
        
        snap.snapRot([midA, self.jointA])
        snap.snapRot([midB, self.jointB])
        #snap.snapRot([midC, self.jointC])
        
        self.ribbonMid|midA
        self.ribbonMid|midB
        null_mid = tp.nullGroup(Vlist=[self.ribbonMid], Vhierarchy=3, Vtype=1, 
                     Vname = 'null_mid' + self.suffix)[0]
        
        aStart = tp.nullGroup(Vlist=[self.jointA], Vhierarchy=1, Vtype=1, 
                     Vname = 'ribbonAStart')[0]
        bEnd = tp.nullGroup(Vlist=[self.ctrlFkB], Vhierarchy=1, Vtype=1, 
                     Vname = 'ribbonBEnd')[0]
        cStart = tp.nullGroup(Vlist=[bEnd], Vhierarchy=1, Vtype=1, 
                     Vname = 'ribbonBEnd')[0]
        cEnd = tp.nullGroup(Vlist=[self.footList[1]], Vhierarchy=1, Vtype=1, 
                     Vname = 'ribbonBEnd')[0]
                     
        snap.snapRot([cStart, cEnd, self.footList[0]])
        
        self.rA = tr.ribbon([aStart, self.midA, midA], Vsuffix=self.suffix + '_up', 
                       side=self.side, numBindJoints=self.numBindJoints)
        self.rB = tr.ribbon([midB, self.midB, bEnd], Vsuffix=self.suffix + '_lw',
                       side=self.side, numBindJoints=self.numBindJoints)
        
        self.rC = tr.ribbon([cStart, self.midC, cEnd], Vsuffix=self.suffix + '_ft',
                       side=self.side, numBindJoints=self.numBindJoints)
        
        self.ribbonMid|self.rA.par_end
        self.ribbonMid|self.rB.par_start
        self.jointA|self.rA.gT
        self.jointB|self.rB.gT
        self.jointC|self.rC.gT
   
        
        
        self.midInb[0]|null_mid
        self.gT|self.rA.par_start
        self.ctrlFkB|self.rB.par_end
        
        self.footList[0]|self.rC.par_start
        self.footList[0]|self.rC.par_end
        
        self.jointA.s >> self.rA.null_null_start.inverseScale
        self.jointA.s >> self.rA.null_null_mid.inverseScale
        self.jointA.s >> self.rA.null_null_end.inverseScale
        
        self.jointB.s >> self.rB.null_null_start.inverseScale
        self.jointB.s >> self.rB.null_null_mid.inverseScale
        self.jointB.s >> self.rB.null_null_end.inverseScale
        
        self.footList[0].s >> self.rC.null_null_start.inverseScale
        self.footList[0].s >> self.rC.null_null_mid.inverseScale
        self.footList[0].s >> self.rC.null_null_end.inverseScale
        
        self.jointA|self.rA.null_null_start
        self.jointA|self.rA.null_null_mid
        self.jointA|self.rA.null_null_end
        
        self.jointB|self.rB.null_null_start
        self.jointB|self.rB.null_null_mid
        self.jointB|self.rB.null_null_end
        
        self.footList[0]|self.rC.null_null_start
        self.footList[0]|self.rC.null_null_mid
        self.footList[0]|self.rC.null_null_end
        #self.footList[1]|self.rC.null_null_end
        
        pm.select(self.rB.null_null_mid)
        
        endBnd = tp.nullGroup(Vlist=[self.rA.sl.start], Vhierarchy=4, Vtype=2, 
                               Vsuffix='_bnd')[0]                   
        startBnd = tp.nullGroup(Vlist=[self.rB.sl.end], Vhierarchy=4, Vtype=2, 
                               Vsuffix='_bnd')[0]
        
        scaleProx = 1
        if self.side == 2: 
            scaleProx = -1                      
        
        px = pg.proxyGeo(Vlist=[endBnd], Vsuffix='_px', 
                              xScale=scaleProx, 
                              yzScale=0, yzScaleGlobal=self.VdistAB * 0.05, 
                              Vsides=6, chain=0,
                              side=self.side,
                              label='endBind')        
        
        px = pg.proxyGeo(Vlist=[startBnd], Vsuffix='_px', 
                              xScale=scaleProx, 
                              yzScale=0, yzScaleGlobal=self.VdistAB * 0.05, 
                              Vsides=6, chain=0,
                              side=self.side,
                              label='startBind') 
        
        #scale mid (elbow/knee)
        midScale = tp.nullGroup(Vlist=[self.ribbonMid], Vhierarchy=4, Vtype=2, 
                               Vsuffix='_scale')[0]
        pxMid = pg.proxyGeo(Vlist=[midScale], Vsuffix='_px', 
                              xScale=scaleProx, VcenterPivot=1,
                              yzScale=0, yzScaleGlobal=0.03 * self.VdistAB, 
                              Vsides=6, chain=0,
                              side=self.side,
                              label='midBind') 
        
        midScaleBlend = pm.createNode('blendColors', 
                                    name='midScaleBlend' + self.suffix)
        
        self.rA.dist.connect([midScaleBlend.color1R])
        self.rB.dist.connect([midScaleBlend.color2R])
        
        self.rA.sl.mid.thickness >> midScaleBlend.color1G
        self.rB.sl.mid.thickness >> midScaleBlend.color2G
        
        self.rA.sl.mid.thickness >> midScaleBlend.color1B
        self.rB.sl.mid.thickness >> midScaleBlend.color2B
        
        midScaleBlend.outputR >> midScale.sx
        midScaleBlend.outputG >> midScale.sy
        midScaleBlend.outputB >> midScale.sz

        # scale all
        self.gSNT.s >> self.rA.scaleAll.input1
        self.gSNT.s >> self.rB.scaleAll.input1
        self.gSNT.s >> self.rC.scaleAll.input1
        self.rA.dist.parent([self.gT])
        self.rB.dist.parent([self.gT])
        self.rC.dist.parent([self.gT])
        
        self.gNT|self.rB.gNT
        self.gNT|self.rA.gNT
        self.gNT|self.rC.gNT
        
    def foot_ik(self):
        """
        Foot setup using maya ik.
        """
     
        wrist = self.footList[0].rename('wrist' + self.suffix) 
        ball = self.footList[1].rename('ball' + self.suffix) 
        tip = self.footList[2].rename('tip' + self.suffix) 
        heel = self.footList[3].rename('heel' + self.suffix) 
        sideA = self.footList[4].rename('sideA' + self.suffix) 
        sideB = self.footList[5].rename('sideB' + self.suffix) 
        self.rollTip = self.footList[6].rename('rollTip' + self.suffix) 
        
        mf.builder.nodes.set([wrist, ball], 'transform_ctrl')
        
        heel.setParent(w=1)
        sideA.setParent(w=1)
        sideB.setParent(w=1)
        self.rollTip.setParent(w=1)
        
        rollBall = tp.nullGroup(Vlist=[ball], Vhierarchy=1, Vtype=2, 
                                Vsuffix='_roll')[0]
                                
        rollTip_dist = tp.nullGroup(Vlist=[self.rollTip], Vhierarchy=4, Vtype=1, 
                                    Vprefix='dist_')[0]
        snap.snap([rollTip_dist, tip])
                                
                                
        null_rollTip = tp.nullGroup(Vlist=[self.rollTip], Vhierarchy=3, Vtype=1, 
                                Vprefix='null_')[0]
                                
        inv_rollTip = tp.nullGroup(Vlist=[self.rollTip], Vhierarchy=4, Vtype=1, 
                                Vprefix='inv_')[0]
        
        rollTipB = tp.nullGroup(Vlist=[self.rollTip], Vhierarchy=3, Vtype=1, 
                                Vprefix='B_')[0]
                                
        inv_rollTipB = tp.nullGroup(Vlist=[inv_rollTip], Vhierarchy=3, Vtype=1, 
                                Vprefix='B_')[0]
                                
                                
        self.null_rollBall = tp.nullGroup(Vlist=[rollBall], Vhierarchy=3, Vtype=1, 
                                Vprefix='null_')[0]
                                
        self.inv_rollBall = tp.nullGroup(Vlist=[rollBall], Vhierarchy=5, Vtype=1, 
                                Vprefix='inv_')[0]
        
        swivleBall = tp.nullGroup(Vlist=[ball], Vhierarchy=1, Vtype=2, 
                                Vsuffix='_swivle')[0]
                                
        null_swivleBall = tp.nullGroup(Vlist=[swivleBall], Vhierarchy=3, Vtype=2, 
                                Vprefix='null_')[0]
                                
        self.ctrlEnd|sideA
        sideA|sideB
        sideB|heel                        
        heel|null_rollTip
        inv_rollTip|self.null_rollBall
        self.ctrlFkB|wrist
        
        inv_rollTip|null_swivleBall
        
        handle_fa = pm.ikHandle(startJoint=wrist,
                                endEffector=ball, 
                                solver='ikRPsolver',
                                name='handle_fa' + self.suffix)
        pm.rename(handle_fa[1], 'effector_fa' + self.suffix)
        
        handle_fb = pm.ikHandle(startJoint=ball,
                                endEffector=tip, 
                                solver='ikRPsolver',
                                name='handle_fb' + self.suffix)
        pm.rename(handle_fb[1], 'effector_fb' + self.suffix)
        
        nullHandle_fa = tp.nullGroup([handle_fa[0]], Vhierarchy=3, 
                                     Vprefix='null_')[0]
        pm.pointConstraint(nullHandle_fa, handle_fa[0])
        
        nullHandle_fb = tp.nullGroup([handle_fb[0]], Vhierarchy=3, 
                                     Vprefix='null_')[0]
        pm.pointConstraint(nullHandle_fb, handle_fb[0])
             
        nullHandle_fa.setParent(rollBall)
        nullHandle_fb.setParent(swivleBall)
      
        reverse_ik_f = pm.createNode('reverse', name='revIKF' + self.suffix)
        self.attrObj.attr(self.swName+'ik_fk') >> reverse_ik_f.inputX
        reverse_ik_f.outputX >> handle_fa[0].ikBlend
        reverse_ik_f.outputX >> handle_fb[0].ikBlend
        
        #self.handlePar.setParent(inv_rollBall)
        snap.snap([self.wristExtra, self.handlePar])
        self.inv_rollBall|self.wristExtra|self.handlePar
        self.shiftWrist = tp.nullGroup(Vlist=[self.wristExtra], Vhierarchy=3, 
                                       Vname='shiftWrist' + self.suffix)[0]
        nullShiftWrist = tp.nullGroup(Vlist=[self.shiftWrist], Vhierarchy=1, Vprefix='null')[0]
        snap.snapTrans([nullShiftWrist, self.inv_rollBall])
        self.inv_rollBall|nullShiftWrist|self.shiftWrist
        
        
        footAttr = ['foot','heel_roll', 'ball_roll', 'tip_roll', 'heel_swivle',
                    'ball_swivle','tip_swivle','side_roll', 
                    'tip_sides', 'tip_up', 'tip_twist', 
                    'roll','roll_limit', 'length', 'lengthA', 'lengthB', 'length_foot'] 
    
        for a in footAttr:
            self.ctrlEnd.addAttr(a, k=1)
            
        self.ctrlEnd.heel_roll >> heel.ry
        self.ctrlEnd.ball_roll >> rollBall.ry
        self.ctrlEnd.tip_roll >> self.rollTip.ry
        self.ctrlEnd.heel_swivle >> heel.rz
        self.ctrlEnd.ball_swivle >> rollBall.rz
        self.ctrlEnd.tip_swivle >> self.rollTip.rz
        self.ctrlEnd.foot.setLocked(1)
        self.ctrlEnd.tip_sides >> swivleBall.rz
        self.ctrlEnd.tip_up >> swivleBall.ry
        self.ctrlEnd.tip_twist >> swivleBall.rx
        self.ctrlEnd.length_foot.set(1)
        self.ctrlEnd.length_foot >> nullShiftWrist.sx
        self.ctrlEnd.length.setLocked(1)
        
        # shift pivots ball/tip -----------------------------------------------
  
        multBallShiftInvA = pm.shadingNode('multiplyDivide', asUtility=1, 
                                        name='ball_shift' + self.suffix)
        multBallShiftInvB = pm.shadingNode('multiplyDivide', asUtility=1, 
                                        name='ball_shift' + self.suffix)
        
        multSide = 1
        if self.side == 2:
            multSide = -1
            
        multBallShiftInvA.input2.set([-1*multSide,-1*multSide,-1*multSide]) 
        multBallShiftInvB.input2.set([1*multSide,1*multSide,1*multSide])
          
        self.ctrlEnd.lengthA >> multBallShiftInvA.input1X
        self.ctrlEnd.lengthB >> multBallShiftInvA.input1Y
        
        multBallShiftInvA.outputX >> self.inv_rollBall.tx
        
        
        self.ctrlEnd.lengthA >> multBallShiftInvB.input1X
        self.ctrlEnd.lengthB >> multBallShiftInvB.input1Y
        
        
        multBallShiftInvB.outputX >> rollBall.tx
        multBallShiftInvB.outputX >> swivleBall.tx
        
        dWrist = td.dist(Vlist=[self.VobEnd, swivleBall], Vsuffix=self.suffix)
        dWrist.calibrate(1)
        
        
        dBall = td.dist(Vlist=[nullHandle_fa, rollTip_dist], Vsuffix=self.suffix)
        dBall.calibrate(1)
        
        
        multBallShiftInvA.outputX >> inv_rollTip.tx
        multBallShiftInvB.outputX >> self.rollTip.tx
        
        multBallShiftInvA.outputY >>  inv_rollTipB.tx
        multBallShiftInvB.outputY >> rollTipB.tx
        
        wrist.addAttr('length', k=1, min=0.0001)
        ball.addAttr('length', k=1, min=0.0001)
        wrist.length.set(1)
        ball.length.set(1)
        
        blendScale = pm.createNode('blendColors', 
                                    name='blendScale' + self.suffix)
        self.attrObj.ik_fk >> blendScale.blender
        
        wrist.length >> blendScale.color1R
        dWrist.connect([blendScale.color2R])
        
        ball.length >> blendScale.color1G
        dBall.connect([blendScale.color2G])
        
        blendScale.outputR >> wrist.sx
        blendScale.outputG >> ball.sx
            
        wrist.sx.setLocked(1)
        ball.sx.setLocked(1)
        
        dWrist.parent([self.gT])
        dBall.parent([self.gT])
        
        #END shift pivots ball/tip --------------------------------------------
        
        rangeRoll = pm.createNode('setRange', name='rangeRoll%s' % (self.suffix))
        rangeRoll.minX.set (0)
        rangeRoll.maxX.set (360)
        rangeRoll.oldMinX.set (0)
        rangeRoll.oldMaxX.set (360)
        
        rangeRoll.minY.set (-360)
        rangeRoll.maxY.set (0)
        rangeRoll.oldMinY.set (-360)
        rangeRoll.oldMaxY.set (0)
        
        self.ctrlEnd.side_roll >> rangeRoll.valueX
        self.ctrlEnd.side_roll >> rangeRoll.valueY
        
        rangeRoll.outValueX >> sideA.rx
        rangeRoll.outValueY >> sideB.rx
        
        if self.autoRoll:
            rangeRoll = pm.createNode('setRange', name='rangeRollLimit'+self.suffix)
            rangeAdd = pm.createNode('plusMinusAverage', name='range_add'+self.suffix)
            multRollInv = pm.shadingNode('multiplyDivide', asUtility=1, 
                                        name='mult' + self.suffix)
            
            rangeRoll.outValue >> multRollInv.input1
            multRollInv.input2.set([-1,-1,-1])
            
            multRollInv.outputX >> self.ctrlEnd.heel_roll
            multRollInv.outputY >> self.ctrlEnd.ball_roll
            multRollInv.outputZ >> self.ctrlEnd.tip_roll
            
            self.ctrlEnd.roll >> rangeRoll.valueX
            self.ctrlEnd.roll >> rangeRoll.valueY
            self.ctrlEnd.roll >> rangeRoll.valueZ
            
            rangeRoll.minX.set (180)
            rangeRoll.minY.set (0)
            rangeRoll.minZ.set (0)
            
            rangeRoll.maxX.set (0)
            self.ctrlEnd.roll_limit >> rangeRoll.maxY
            rangeRoll.maxZ.set (180)
            
            rangeRoll.oldMinX.set (-180)
            rangeRoll.oldMinY.set (0)
            self.ctrlEnd.roll_limit >> rangeRoll.oldMinZ
            
            rangeRoll.oldMaxX.set (0)
            self.ctrlEnd.roll_limit >> rangeRoll.oldMaxY
            rangeAdd.output3D.output3Dx >> rangeRoll.oldMaxZ
            
            self.ctrlEnd.roll_limit >> rangeAdd.input3D[0].input3Dx
            rangeAdd.input3D[1].input3Dx.set(180)
            
            self.ctrlEnd.heel_roll.setLocked(1)
            self.ctrlEnd.ball_roll.setLocked(1)
            self.ctrlEnd.tip_roll.setLocked(1)
            
            self.ctrlEnd.heel_roll.set(lock=1, keyable=0, channelBox=0)
            self.ctrlEnd.ball_roll.set(lock=1, keyable=0, channelBox=0)
            self.ctrlEnd.tip_roll.set(lock=1, keyable=0, channelBox=0)
            
            self.ctrlEnd.roll_limit.set(30)
        
        else:
            self.ctrlEnd.roll.set(lock=1, keyable=0, channelBox=0)
            self.ctrlEnd.roll_limit.set(lock=1, keyable=0, channelBox=0)
            
        
        footLen = td.get([wrist, ball]) + td.get([ball, tip])    
        prxList = [ball, tip]
        pg.proxyGeo(prxList, 
                    yzScaleGlobal=0.7, 
                    side=self.side, 
                    label='toe_')
        
        sideMult = 1
        if self.side == 2: 
            sideMult = -1 
        tj.jointInbCons(Vlist=[wrist, ball], Vproxy=1, interpolateScale=1,
                        proxyScale=0.13*footLen, side=self.side, label='toe_inb',
                        autoSlide=0.2*footLen*sideMult)
        
        wristInv = tp.nullGroup([wrist], Vhierarchy=4, Vtype=2, Vprefix='inv_',
                     connectInverseScale=1)[0]
        pg.proxyGeo([wristInv], 
                    side=self.side, 
                    label='inv_wrist',
                    VcenterPivot=1, chain=0,
                    yzScaleGlobal=0.13*footLen)
        
        wristInv|self.rB.par_end
        wristInv|self.rC.par_start 
        
        for jnt in [ball, wrist]:
            attrList = [jnt.tx, jnt.ty, jnt.tz,
                        jnt.sy, jnt.sz]
            for attr in attrList:
                attr.set (lock=1, keyable=0, channelBox=0)


    def spaceSwitches(self):
        """space switches for all controllers to local and world"""
        

        dummyGoalA = tp.nullGroup(Vlist=[self.jointA], 
                                  Vhierarchy=1, Vtype=2, 
                                  Vname='dumyGl_Chest', 
                                  Vsuffix=self.suffix)[0]
        self.dummyGoalPV = tp.nullGroup(Vlist=[self.jointA], 
                                  Vhierarchy=1, Vtype=2, 
                                  Vname='dumyGl_PV', 
                                  Vsuffix=self.suffix)[0]
        dummyGoalCOG = tp.nullGroup(Vlist=[self.jointA], 
                                  Vhierarchy=1, Vtype=2, 
                                  Vname='dumyGl_COG', 
                                  Vsuffix=self.suffix)[0]
                                  
        self.dummyGoalGT= tp.nullGroup(Vlist=[self.jointA], 
                                  Vhierarchy=1, Vtype=2, 
                                  Vname='dumyGl_GT', 
                                  Vsuffix=self.suffix)[0]
                  
        self.gNT|dummyGoalA
        self.gNT|dummyGoalCOG
        self.gT|self.dummyGoalPV
        self.gT|self.dummyGoalGT
        
        zero = tc.zeroOutConstraint(Vlist=[self.gMT, self.dummyGoalGT, dummyGoalA,
                                    self.jointA.getParent(), self.jointA],
                                    VlastObj=1)

        tc.spaceSwitch(headlineAttr=1,
                       Vlist=zero, 
                       VattrName=['world', 'limb_root', 'chest'], 
                       Vtype=2, VlastObj=1, Voffset=1, skipTranslate=['x','y','z'])
        
        tc.spaceSwitch(headlineAttr=1,
                       Vlist=[self.gMT,  self.dummyGoalGT, self.dummyGoalPV, self.ctrlEnd, 
                              self.ctrlPolv.getParent(), self.ctrlPolv], 
                       VattrName=['world', 'body', 'limb_root', 'limb_end'], 
                       VlastObj=1, Voffset=1)
        self.ctrlPolv.limb_root.set(0.5)
        self.ctrlPolv.limb_end.set(0.5)
        
        tc.spaceSwitch(headlineAttr=1,
                       Vlist=[self.gMT, dummyGoalCOG, dummyGoalA, self.dummyGoalGT, 
                              self.ctrlEnd.getParent(), self.ctrlEnd], 
                       VattrName=['world', 'cog', 'body', 'limb_root'], 
                       VlastObj=1, Voffset=1)
        
class limbBendHip():
    """
    Limb bend connected to hip.
    
    ============ ==============================================================
    Arguments    Description
    ============ ==============================================================
    Vlist[0:18]  limb list
    Clist[18:25] hip list
    ============ ==============================================================
    
    TODO: Add pol vector objects to template or add placement values for pvs
          as option.
    """
    
    def __init__(self, Vlist=[], Vsuffix='', proxy=1, side=2, twistOffset=0,
                 foot=0, foot_ik=1, ikSwitchCtl='Ik_switches_CON',softKeys=3,
                 numInterpolate=3, drvKeys=0, handleTwist=0, numBindJoints=8, 
                 twistOffsetHip=0):
        
        self.sl = lsSl.sl(Vlist, 11, 0,
                          slDict={
                                  'limbList':'0:16',
                                  'hipList':'16:23'
                                  })
        
        limbBendCls = limb_ribbon(Vlist=self.sl.limbList, Vsuffix=Vsuffix, 
                                  proxy=proxy, side=side, 
                                  twistOffset=twistOffset,
                                  foot=foot, foot_ik=foot_ik, 
                                  ikSwitchCtl=ikSwitchCtl,
                                  softKeys=softKeys,
                                  numBindJoints=numBindJoints)
        
        hipCls = ll.hip(Vlist=self.sl.hipList, Vsuffix=Vsuffix + '_hip',  
                        proxy=proxy, numInterpolate=numInterpolate, side=side,
                        drvKeys=drvKeys, handleTwist=twistOffsetHip)
        
        hipCls.connectFoot(limbBendCls.handlePar)
        hipCls.lugCls.invCtrl|limbBendCls.gT 
        
        #connect ik length
        limLen = limbBendCls.VdistAB
        hipLen =  hipCls.str.VdistAB
        multLen = pm.createNode('multiplyDivide', 
                             name='multLen' + Vsuffix)
        
        addDif = pm.createNode('plusMinusAverage', 
                            name='addDif' + Vsuffix)
        
        addDif.addAttr('num', defaultValue=1-(limLen/hipLen))
        addDif.num >> addDif.input1D[0]
        
        
        addLen = pm.createNode('plusMinusAverage', name='addLen' + Vsuffix)
        limbBendCls.attrObj.length_A >> addDif.input1D[0]
        limbBendCls.attrObj.length_B >> addDif.input1D[1]
        
        
        addLen.output1D >> multLen.input1X
        
        multLen.input2X.set(limLen/hipLen)
        multLen.outputX >> addDif.input1D[1]
        
        addDif.output1D >> hipCls.sl.ctrl.length 
        hipCls.sl.ctrl.length.set(lock=1, channelBox=1, keyable=0)
        
        # connect soft_stretch
        limbBendCls.attrObj.soft_ik >> hipCls.sl.ctrl.soft_stretch
        hipCls.sl.ctrl.soft_stretch.set(lock=1, channelBox=0, keyable=0)
        
        #ik swith >> auto hip
        hipCls.connectIkSwitch(limbBendCls.attrObj.ik_fk)
        
        hipCls.gT|limbBendCls.dummyGoalGT
        hipCls.gT|limbBendCls.dummyGoalPV

        # auto rot attr on foot ik ctrl
        ikCtrl = limbBendCls.ctrlEnd
        hipCtrl = hipCls.lugCls.sl.ctrl
        
        ikCtrl.addAttr('hip', defaultValue=0, min=0, keyable=1)
        ikCtrl.hip.setLocked(1)
        
        ikCtrl.addAttr('hip_auto_rot', at='double', defaultValue=0, 
                       max=1, min=0, keyable=1)
        
        ikCtrl.hip_auto_rot >> hipCtrl.auto_rot
        hipCtrl.auto_rot.setLocked(1)
        hipCtrl.auto_rot.setKeyable(0) 
        

     
     

     
     
    