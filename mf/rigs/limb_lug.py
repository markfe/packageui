"""
limb junctions (leg-to-hip, arm-to-shoulder) 
"""
import pymel.core as pm
import mf.builder.nodes
from mf.tools import lsSl
from mf.tools import snap
from mf.tools import distance as td
from mf.tools import parent as tp
from mf.tools import joint as tj
from mf.tools import cons as tc
from mf.tools import proxyGeo as pg
from mf.tools import key as tk
import limb


class lug():
    """
    Stretchy lug.
    """
    def __init__(self, Vlist=[], Vsuffix='',  proxy=1, numInterpolate=3, side=3):
        """
        Duplicates the initial joint chain for interpolation.
        """
        self.sl = lsSl.sl(Vlist, 3, 3,
                  slDict={
                          'jointA':'0',
                          'jointB':'1',
                          'ctrl':'2'
                          })
        
        mf.builder.nodes.set([self.sl.ctrl],  
                             'transform_ctrl')
        
        self.suffix = Vsuffix
        self.side = side
        
        pm.rename(self.sl.jointA, 'jntMovA' + self.suffix)
        pm.rename(self.sl.jointB, 'jntMovB' + self.suffix)
        
        self.chainFix = tj.duplicateChain([self.sl.jointA], 
                                   ['jntFixA' + self.suffix, 
                                    'jntFixB' + self.suffix])
        
        self.nullJointA = tp.nullGroup([self.sl.jointA], Vtype=2, 
                             Vname='null_jntMovA' + self.suffix,
                             Vhierarchy=3)[0]
        
        self.start = tp.nullGroup([self.sl.jointA], Vtype=3, 
                             Vname='start' + self.suffix)[0]
        self.end = tp.nullGroup([self.sl.jointB], Vtype=3, 
                           Vname='end' + self.suffix)[0]
        self.dist = td.dist([self.start, self.end])
        self.stretchyIk()
        self.interpolate(numInterpolate)    
        self.ctrl()
        #self.stretchyIk()
        
        self.cleanup()
        
    def stretchyIk(self):
        """
        Stretchy IK with distance tool.
        """
        self.handle, effector = pm.ikHandle(startJoint=self.sl.jointA, 
                                       endEffector=self.sl.jointB, 
                                       solver='ikRPsolver',
                                       name='handle' + self.suffix)
        
        pm.rename(effector, 'effector' + self.suffix)
        self.end|self.handle
        
        self.dist.calibrate(1)
        self.dist.connect([self.sl.jointA.sx])
        
        
        self.ctrlPolv = tp.nullGroup([self.sl.jointB],
                                     Vtype=3, Vhierarchy=4,
                                     Vname='pv' + self.suffix)[0]
        self.sl.jointA|self.ctrlPolv
        if self.side==1:                             
            self.ctrlPolv.ty.set(-10)
          
        elif self.side==2:                             
            self.ctrlPolv.ty.set(10)
        self.ctrlPolv.setParent(w=1)
            
        pm.poleVectorConstraint(self.ctrlPolv, self.handle)
         
        
    def ctrl(self):
        """
        Ctrl and auto/manual switch.
        """
        #snap.snapTrans([self.sl.ctrl, self.sl.jointB])
        #snap hip_ctrl to the hip origin
        snap.snapTrans([self.sl.ctrl, self.sl.jointA])
        
        #create a new ctrl on the leg origin.
        self.invCtrlEnd = tp.nullGroup([self.sl.ctrl], Vtype=2, Vhierarchy=5,
                                Vname='end_ctrl' + self.suffix)[0]
        
        snap.snapTrans([self.invCtrlEnd, self.sl.jointB])
        
        
        self.invCtrl = tp.nullGroup([self.sl.ctrl], Vtype=2, Vhierarchy=5,
                                Vname='leg_par' + self.suffix)[0]
                            
        self.sl.ctrl.s >> self.invCtrl.inverseScale
        
        #self.invCtrl|self.invCtrlEnd
        
        self.nullCtrl = tp.nullGroup([self.sl.ctrl], Vtype=2, Vhierarchy=3,
                                Vname='null_ctrl' + self.suffix, freez=0)[0]
        
        self.ctrlPar = tp.nullGroup([self.sl.jointA], Vtype=2, 
                                    Vname='ctrl_par' + self.suffix)[0]
                                    
        self.ctrlInb = tp.nullGroup([self.sl.jointA], Vtype=2, 
                                    Vname='ctrl_inb' + self.suffix)[0]
                                    
        self.ctrlFix = tp.nullGroup([self.sl.jointA], Vtype=2, 
                                    Vname='ctrl_fix' + self.suffix)[0]
        
        self.blend = pm.createNode('blendColors', 
                                   name='blend_ik' + self.suffix)
        
        self.gT = pm.group(w=1, em=1, n='trans' + self.suffix)
        self.gT|self.ctrlInb
        
        zero = tc.zeroOutConstraint([self.ctrlPar, 
                                     self.ctrlFix, 
                                     self.ctrlInb, 
                                     self.sl.ctrl],
                                     VlastObj=1)
        
        switch = tc.constraintSwitch(Vlist=zero, 
                                            VattrName='auto_rot', Vname='', 
                                            Vsuffix='', 
                                            Vtype=3, Voffset=0)
        self.blend.color1R.set(0) 
        self.sl.ctrl.auto_rot >> self.blend.color2R
        self.blend.outputR >> switch.blender
        
        pg.proxyGeo([self.invCtrlEnd], VcenterPivot=1, chain=0, 
                    side=self.side,
                    yzScaleGlobal=0.2 * self.dist.distance())   
   
    def interpolate(self, num=3):
        """
        Interpolation.
        """
        self.inbA = []
        inbB = []
        for i in range(num):
            newChain = tj.duplicateChain([self.sl.jointA], 
                        ['jntInbA_' + str(i) + self.suffix, 
                         'jntInbB_' + str(i) + self.suffix])
            
            tp.nullGroup([newChain[1]], Vhierarchy=3, Vtype=2, Vprefix='null_', 
                         connectInverseScale=1)
            self.inbA.append(newChain[0])
            inbB.append(newChain[1])
        
        tc.distribute(self.inbA + [self.sl.jointA, self.chainFix[0]], Vtype=2)
        tc.distribute(self.inbA + [self.sl.ctrl, self.chainFix[0]], Vtype=4,
                      skipScale=[0, 1, 1])
        tc.distribute(inbB + [self.end, self.chainFix[1]], Vtype=2)
        pg.proxyGeo(inbB, VcenterPivot=1, chain=0, side=self.side, 
                    label='lg', 
                    yzScaleGlobal=0.2 * self.dist.distance())
    
    def cleanup(self):
        """
        Hierarchy.
        """
        self.ctrlInb|self.nullCtrl
        self.sl.ctrl|self.end
         
        for o in self.inbA:
            self.start|o
            
        self.start|self.nullJointA
        
        self.gT|self.chainFix[0]
        self.gT|self.ctrlFix
        self.gT|self.start
        self.dist.parent([self.gT])
        
    
class hip():
    """
    Limb lug auto slide on a virtual sphere.
    """
    def __init__(self, Vlist=[], Vsuffix='',  proxy=1, numInterpolate=3, 
                 side=3, drvKeys=0, handleTwist=0):
        
        self.sl = lsSl.sl(Vlist, 6, 6,
                  slDict={'jointA':'0',
                          'jointB':'1',
                          'jointC':'2',
                          'ctrl':'3',
                          'lugA':'4',
                          'lugB':'5',}, Vrename=1, Vsuffix=Vsuffix)
        
        mf.builder.nodes.set([self.sl.ctrl],  
                             'transform_ctrl')
        
        self.suffix = Vsuffix
        self.side = side
        self.proxy = proxy
        self.numInterpolate = numInterpolate
            
        self.makeDriverLimb()
        self.lug()
        self.polVector()
        
        if drvKeys:
            self.drivenKeyAttr()
    
        self.lugCls.handle.twist.set(handleTwist)
        self.cleanup()
        
        
    def makeDriverLimb(self): 
        """
        Stretchy limb with locked angle on lug.
        """
        #from mf.rigs import limb as rl
        self.str = limb.stretch([self.sl.jointA, self.sl.jointB, 
                                   self.sl.jointC, self.sl.ctrl], 
                                   hardKeys=3, softKeys=4,
                                   Vsuffix=self.suffix)
        
        
        self.footPar = tp.nullGroup(Vlist=[self.str.VobEnd], Vhierarchy=3, 
                                     Vtype=1, jointRadius=3, 
                                     Vname='foot_par' + self.suffix)[0]
        
        self.null_obStart = tp.nullGroup(Vlist=[self.str.VobStart], 
                                         Vhierarchy=3, 
                                         Vtype=1, jointRadius=3, 
                                         Vprefix='null_')[0]
                                         
    def polVector(self):
        """
        Pole vector auto Xrotation.
        """
        self.ctrlPolv = tp.nullGroup([self.sl.jointB],
                                     Vtype=3, 
                                     Vname='pv' + self.suffix)[0]
        pm.poleVectorConstraint(self.ctrlPolv, self.str.Vhandle[0])
        
        self.grpPv = tp.nullGroup([self.ctrlPolv], 
                                  Vtype=1,
                                  Vhierarchy=1,
                                  #pivot=0, rot=0,
                                  Vname='grp_pv' + self.suffix)[0]
        
        pvFix = tp.nullGroup([self.ctrlPolv], 
                             Vtype=3,
                             Vhierarchy=1,
                             Vname='pvFix' + self.suffix)[0]
                             
        pvMov = tp.nullGroup([self.ctrlPolv], 
                             Vtype=3,
                             Vhierarchy=1,
                             Vname='pvMov' + self.suffix)[0]
        
        self.grpPv|self.ctrlPolv
        self.grpPv|pvFix
        self.grpPv|pvMov
        
        tc.spaceSwitch(headlineAttr=1,
                       Vlist=[pvFix, pvMov, self.ctrlPolv], 
                       VattrName=['pv', 'limb_root'], 
                       Vtype=1, Voffset=1)
        
        self.ctrlPolv.limb_root.set(1)
        #----------------------------------------------------------------------
        self.grpPvLg = tp.nullGroup([self.lugCls.ctrlPolv], 
                                    Vtype=1,
                                    Vhierarchy=1,
                                    #pivot=0, rot=0,
                                    Vname='grp_pvLg' + self.suffix)[0]
        
        pvLgFix = tp.nullGroup([self.lugCls.ctrlPolv], 
                             Vtype=3,
                             Vhierarchy=1,
                             Vname='pvLgFix' + self.suffix)[0]
                             
        pvLgMov = tp.nullGroup([self.lugCls.ctrlPolv], 
                             Vtype=3,
                             Vhierarchy=1,
                             Vname='pvLgMov' + self.suffix)[0]
        
        self.grpPvLg|self.lugCls.ctrlPolv
        self.grpPvLg|pvLgFix
        self.grpPvLg|pvLgMov
        
        
        tc.spaceSwitch(headlineAttr=1,
                       Vlist=[pvLgFix, pvLgMov, self.lugCls.ctrlPolv], 
                       VattrName=['pv', 'limb_root'], 
                       Vtype=1, Voffset=1)
        
        self.lugCls.ctrlPolv.limb_root.set(1)
        
        
    def lug(self):
        """
        Creates the lug class and connects the lug to the hip.
        """
        self.lugCls = lug(Vlist=[self.sl.lugA, self.sl.lugB, self.sl.ctrl], 
                     Vsuffix='_lug' + self.suffix, proxy=self.proxy, 
                     numInterpolate=self.numInterpolate, side=self.side)
        
        self.sl.jointA|self.lugCls.ctrlPar
    
    def drivenKeyAttr(self):
        tk.drivenKeyBs(Vlist=[self.lugCls.sl.jointA])
    
    def cleanup(self):
        
        self.gT = tp.trans(em=1, w=1, n='trans' + self.suffix).group
        #self.gNT = tp.noTrans(em=1, w=1, n='no_trans' + self.suffix).group
        self.gMT = tp.mainTrans(em=1, w=1, n='main_trans' + self.suffix).group
        
        self.gWT = pm.group(em=1, w=1, n='world_trans' + self.suffix)
        self.gST = pm.group(em=1, w=1, n='scale_trans' + self.suffix)
        
        self.gMT|self.gST
        self.gMT|self.gWT
        
        self.str.dC.parent([self.gST])
        
        self.gT|self.null_obStart
        self.gT|self.sl.jointA
        
        self.gT|self.lugCls.gT
        
        self.gT|self.grpPv
        self.sl.ctrl|self.grpPvLg
        

    def connectFoot(self, footIK):
        footIK|self.footPar
    
    def connectIkSwitch(self, ikFkAttr):
        ikFkAttr >> self.lugCls.blend.blender
        
        
class shoulder():
    """
    shoulder setup.
    
    ========== ===================================================
    Arguments  Description
    ========== ===================================================
    Vlist[0]   root of a joint chain
    Vlist[1]   root ctrl
    ========== ===================================================
    """
    def __init__(self, Vlist=[], Vsuffix='', side=0, numRootInb=2):
        self.sl = lsSl.sl(Vlist, 9, 9,
                  slDict={
                          'root':'0',
                          'ctrl':'1'
                          }, 
                          Vrename=1, Vsuffix=Vsuffix)
        
        mf.builder.nodes.set([self.sl.ctrl],  
                             'transform_ctrl')
        
        self.suffix = Vsuffix
        self.side = side
        
        snap.snapRot([self.sl.ctrl, self.sl.root])
        
        orient = self.sl.ctrl.jointOrient.get()
        self.nullCtrl = tp.nullGroup([self.sl.ctrl], Vtype=2, Vhierarchy=3,
                                     Vname='null_ctrl' + self.suffix)[0]
        self.sl.ctrl.jointOrient.set(orient)
        self.nullCtrl.jointOrient.set(0,0,0)
        
        
                                     
        self.nullRoot = tp.nullGroup([self.sl.root], Vtype=1, Vhierarchy=3,
                                     Vname='null_root' + self.suffix)[0]
                                     
        self.gT = tp.trans(em=1, w=1, n='trans' + Vsuffix).group
        snap.snap([self.gT, self.sl.ctrl])
        
        self.gT|self.nullCtrl
        self.sl.ctrl|self.nullRoot
        
        
        self.dist = td.get([self.sl.root, self.sl.root.getChildren()[0]])
        
        self.rootInb(numRootInb)
        
        pg.proxyGeo([self.sl.ctrl], yzScaleGlobal=0.1*self.dist, chain=0, 
                    VcenterPivot=1, side=self.side, label='ctrl')
        
        self.gMT = tp.mainTrans(em=1, w=1, n='main_trans' + Vsuffix).group

        self.gWT = pm.group(em=1, w=1, n='world_trans' + Vsuffix)
        self.gWT.setParent(self.gMT)
        
        zero = tc.zeroOutConstraint([self.gWT, self.gT,
                                    self.nullCtrl, self.sl.ctrl],
                                    VlastObj=1)
        tc.spaceSwitch(headlineAttr=1,
                       Vlist=zero, 
                       VattrName=['world', 'limb_root'], 
                       Vtype=2, VlastObj=1, Voffset=1)
        
    def rootInb(self, num):
        goal = tp.nullGroup([self.sl.ctrl], Vhierarchy=4, Vtype=2, 
                            Vprefix='goal_', 
                            connectInverseScale=1)[0]
                            
        
        goalFix = tp.nullGroup([self.sl.ctrl], Vhierarchy=1, Vtype=2, 
                               Vprefix='goalFix_', 
                               connectInverseScale=1)[0]
                               
        self.gChestPar = tp.nullGroup([goalFix], Vtype=1, Vhierarchy=3,
                                Vname='chestPar' + self.suffix)[0]                       
                               
        #self.gT|goalFix
        
        inb = tp.nullGroup([self.sl.ctrl], Vhierarchy=4, Vtype=2, 
                           Vprefix='inb_', 
                            connectInverseScale=1, Vnum=num)
        
        tc.distribute(inb + [goal, goalFix], Vtype=3)
        
        prx = pg.proxyGeo(inb, VcenterPivot=1, chain=0, side=self.side,
                          yzScaleGlobal=0.1*self.dist)
          
    