"""
two joint setup for limbs (arms or legs) 
"""
import pymel.core as pm

import mf.builder.nodes
from mf.tools import lsSl
from mf.tools import snap
from mf.tools import distance as td
from mf.tools import key as tk
from mf.tools import curve as tcv
from mf.tools import parent as tp
from mf.tools import joint as tj
from mf.tools import cons as tc
from mf.tools import proxyGeo as pg
from mf.tools import rot as rt
from mf.tools import bend as tb
from mf.tools import general as tg
import mf.tools.ribbon as rb
from mf.rigs import appendage as ra
import limb_lug as ll


class limb():
    """
    main method, complete limb setup
    calls the separate modules and the final connections for each module 
    are made
    
    returns: nothing
    """
    def __init__(self, Vlist=[], Vsuffix='', endType=1, proxy=1, stretchDistr=1,
                 spaceswitch=1, pin=1, inverseJunction=0, side=2):
        
        Vlist = lsSl.lsSl(Vlist, 9, 9, ['joint', 'joint', 'joint', 'transform'], 
                          """\nselect three joints of a joint chain (from start to end)""")
        
        self.suffix = Vsuffix   
        self.jointA = Vlist[0] 
        self.jointB = Vlist[1]
        self.jointC = Vlist[2]
        self.VctrlStart = Vlist[3]
        self.VctrlEnd = Vlist[4]
        self.VctrlPolv = Vlist[5]
        self.attrObj = Vlist[6]
        ctrlFkA = Vlist[7]
        ctrlFkB = Vlist[8]
        
        self.inverseJunction = inverseJunction
        mf.builder.nodes.set([self.VctrlStart,
                              self.VctrlEnd,
                              self.VctrlPolv, 
                              self.attrObj], 
                             'transform_ctrl')
        
        self.attrObj.addAttr('limb', defaultValue=0, min=0, keyable=1)
        self.attrObj.limb.setLocked(1)
        
        self.VctrlEnd.addAttr('limb', defaultValue=0, min=0, keyable=1)
        self.VctrlEnd.limb.setLocked(1)
        
        pm.rename (self.jointA, ('jnt_ik1%s' % (Vsuffix)))
        pm.rename (self.jointB, ('jnt_ik2%s' % (Vsuffix)))
        pm.rename (self.jointC, ('jnt_ik3%s' % (Vsuffix)))
        pm.rename (self.VctrlStart, ('ctrlA%s' % (Vsuffix)))
        pm.rename (self.VctrlEnd, ('ctrlB%s' % (Vsuffix)))
        pm.rename (self.VctrlPolv, ('polv%s' % (Vsuffix)))
        pm.rename (self.attrObj, ('attrObj%s' % (Vsuffix)))
        pm.rename (ctrlFkA, ('ctrlFkA%s' % (Vsuffix)))
        pm.rename (ctrlFkB, ('ctrlFkB%s' % (Vsuffix)))
        
        # ctrl visibility
        self.attrObj.addAttr('ik_fk', defaultValue=1, min=0, max=1, 
                        keyable=1, at='double')
        invIk_fk = tg.invAttr(self.attrObj.ik_fk, Vsuffix)
        invIk_fk.connect([self.VctrlEnd.getShape().v,self.VctrlPolv.getShape().v])
        self.attrObj.ik_fk >> ctrlFkA.getShape().v
        self.attrObj.ik_fk >> ctrlFkB.getShape().v
        # Both ctrls will be visible when ik_fk is set to 0.51
        invIk_fk.setRange.maxX.set(0.1)
        
        # GET START VALUES
        self.VdistA= td.get([self.jointA, self.jointB])
        self.VdistB = td.get([self.jointB, self.jointC])
        self.VdistAB = self.VdistA + self.VdistB
        
        ctSc = (self.VdistAB*0.15)
        
        self.fkList = tj.duplicateChain([self.jointA], 
                                   Vnamestring=['jnt_fk1%s' % (Vsuffix), 
                                                'jnt_fk2%s' % (Vsuffix), 
                                                'jnt_fk3%s' % (Vsuffix)])
        tp.parentShape(Vlist=[ctrlFkA, self.fkList[1]])
        tp.parentShape(Vlist=[ctrlFkB, self.fkList[2]])
        
        mf.builder.nodes.set([self.fkList[1], self.fkList[2]], 
                             'transform_ctrl')
        # the empty transform nodes will remain in the scene for reference.
        ctrlFkA.setParent(self.fkList[1])
        ctrlFkB.setParent(self.fkList[2])
        
        if pin:
            #pin setup

            pinCls = pinJunction(Vlist=[self.jointA, self.jointB, self.jointC,
                                self.VctrlEnd, self.VctrlPolv], 
                                Vsuffix=Vsuffix, Vconnect=0)
        
        self.drvList = tj.duplicateChain([self.jointA], 
                                    Vnamestring=['jnt_drv1%s' % (Vsuffix), 
                                                 'jnt_drv2%s' % (Vsuffix), 
                                                 'jnt_drv3%s' % (Vsuffix)])
        if proxy:
            pg.proxyGeo(Vlist=self.drvList, Vsuffix='_geo', Vorient=1, 
                        xScale=1, yzScale=0, yzScaleGlobal=0.06, customGeo=0, 
                        Vsides=8)
            
                  
        slideCls = slide([Vlist[0], Vlist[1], Vlist[2], self.VctrlEnd], Vsuffix, 0)
        VslideOut = slideCls()
        
        if stretchDistr:
            distrCls = stretchDistribution([Vlist[0], Vlist[1], Vlist[2], self.VctrlEnd])
        
        # objects for ik/fk snap
        fk1_snap = tp.nullGroup(Vlist=[self.VctrlStart], Vhierarchy=4, Vtype=1, 
                                Vname='fk1_snap' + self.suffix)[0]
        snap.snapRot([fk1_snap, self.fkList[0]])
        
        fk3_snap = tp.nullGroup(Vlist=[self.fkList[2]], Vhierarchy=4, Vtype=1, 
                                Vname='fk3_snap' + self.suffix)[0]
        snap.snap([fk3_snap, self.VctrlEnd])
        
        ik1_snap = tp.nullGroup(Vlist=[ self.jointA], Vhierarchy=4, Vtype=1, 
                                Vname='ik1_snap' + self.suffix)[0]
        snap.snap([ik1_snap, self.VctrlStart])
        
        ik3_snap = tp.nullGroup(Vlist=[self.VctrlEnd], Vhierarchy=4, Vtype=1, 
                                Vname='ik3_snap' + self.suffix)[0]
        snap.snap([ik3_snap, self.fkList[2]])
        # END objects for ik/fk snap
        
        
        stretchCls = stretch(Vlist=[Vlist[0], Vlist[1], Vlist[2], self.VctrlEnd], 
                             Vsuffix=Vsuffix, Vconnect=0)
        VstretchOut = stretchCls()
        #TODO: make global
        self.endObj = VstretchOut[4]

        #limbStart([Vlist[0], self.fkList[0], self.VctrlStart[0]])
        
        consObj = tp.nullGroup(Vlist=[self.fkList[-1]], Vhierarchy=5, Vtype=1, 
                               Vsuffix='_consObj')[0]
        
        endCls = limbEnd(Vlist=[self.jointA, consObj, 
                                VstretchOut[4], self.drvList[2], 
                                self.attrObj], Vsuffix=Vsuffix, endType=endType,
                                proxyScale=self.VdistAB,
                                side=side)
        
        self.VhandPar = endCls()
           
        
        if stretchDistr:
            distrCls.connectLength(VstretchOut[0])
            distrCls.connectSections(VslideOut[0], VslideOut[1])
            distrCls.connectOut(self.jointA.sx, self.jointB.sx)   
        else: 
            VmultStretchSlide = pm.shadingNode('multiplyDivide', asUtility=1, 
                                        name='multStretchSlide%s' % (Vsuffix))
    
            VstretchOut[0] >> VmultStretchSlide.input1X
            VstretchOut[0] >> VmultStretchSlide.input1Y
            VslideOut[0] >> VmultStretchSlide.input2X
            VslideOut[1] >> VmultStretchSlide.input2Y
        
            VmultStretchSlide.outputX >> self.jointA.sx
            VmultStretchSlide.outputY >> self.jointB.sx
            

        if pin:
            if stretchDistr: 
                distrCls.connectOut(pinCls.blend.color2R,
                                    pinCls.blend.color2G)

            else:
                pinCls.connectLength(VmultStretchSlide.outputX,
                                     VmultStretchSlide.outputY)
            
            pinCls.connectOutput(self.jointA.sx, self.jointB.sx)
            
                
        # fk setup 
        tc.constraintIkFk(VikList=[self.jointA,self.jointB,self.jointC], 
                          VfkList=self.fkList, 
                          VdrvList=self.drvList, Vattr=self.attrObj.ik_fk, 
                          Vsuffix='_ikFk', Voffset=1, Vctrl=self.attrObj, 
                          blendScale=1)
        
        # pol vector 
        pm.poleVectorConstraint(self.VctrlPolv, VstretchOut[2][0])
                 
        pm.makeIdentity(self.VctrlStart, apply=1, t=0, r=0, s=1, n=0, jo=0) 
        pm.makeIdentity(self.VctrlEnd, apply=1, t=0, r=0, s=1, n=0, jo=0)
        pm.makeIdentity(self.VctrlPolv, apply=1, t=0, r=0, s=1, n=0, jo=0)
        self.nullCtrl = tp.nullGroup(Vlist=[self.VctrlStart, self.VctrlEnd, 
                                            self.VctrlPolv], Vhierarchy=3, 
                                            Vtype=2, 
                                            Vprefix='null_', Vsuffix='')
        

        if self.VctrlEnd.nodeType() == 'joint':
            self.VctrlEnd.jointOrient.set(self.nullCtrl[1].jointOrient.get())
            self.nullCtrl[1].jointOrient.set(0,0,0)
        
        if self.VctrlStart.nodeType() == 'joint':
            self.VctrlStart.jointOrient.set(self.nullCtrl[0].jointOrient.get())
            self.nullCtrl[0].jointOrient.set(0,0,0)


        
        
        
        try:
            aimLine = tcv.aimLine(Vlist=[self.VctrlPolv, self.drvList[1]], 
                                  Vsuffix=Vsuffix)
            invIk_fk.connect([aimLine[0].getShape().v])
        except:
            pass
        pm.parent(VstretchOut[3], self.VctrlStart)

        nullEnd = tp.nullGroup(Vlist=[stretchCls.VobEnd], Vhierarchy=2, Vtype=1, 
                     Vprefix='null_')[0]
        
        pm.parent(nullEnd, self.VctrlEnd)
        print nullEnd
        
        try:
            pm.parent(self.attrObj, self.VctrlStart)
        except:
            pass 
        
        self.grpjnt = pm.group(em=1, w=1, n='grp_jnt'+ Vsuffix)
        
        self.gConnect = tp.nullGroup(Vlist=[self.jointA], Vhierarchy=1, 
                                     Vtype=2, jointRadius=3, 
                                     Vname='grp_connect', Vsuffix=Vsuffix)[0]
        self.gNConnect = tp.nullGroup(Vlist=[self.gConnect], Vhierarchy=3, 
                                     Vtype=1, jointRadius=3, 
                                     Vname='null_grp_connect', Vsuffix=Vsuffix)[0]
        
        snap.snap([self.grpjnt, self.jointA])
        
        self.jointA.setParent(self.grpjnt)
        self.drvList[0].setParent(self.grpjnt)
        self.fkList[0].setParent(self.grpjnt)
        
        self.gT = tp.trans(em=1, w=1, n='trans' + Vsuffix).group
        self.gNT = tp.noTrans(em=1, w=1, n='no_trans' + Vsuffix).group
        self.gMT = tp.mainTrans(em=1, w=1, n='main_trans' + Vsuffix).group
        
        self.gWT = pm.group(em=1, w=1, n='world_trans' + Vsuffix)
        self.gST = pm.group(em=1, w=1, n='scale_trans' + Vsuffix)
        
        if pin:
            self.gT|pinCls.distGrp
            
        self.nullCtrl[0].setParent(self.gT)
        self.VhandPar.setParent(self.jointC)
        self.grpjnt.setParent([self.VctrlStart])
        self.gNConnect.setParent(self.gT)
        self.gST.setParent(self.gMT)
        self.gWT.setParent(self.gMT)
        
        self.nullCtrl[1].setParent(self.gST)
        self.nullCtrl[2].setParent(self.gST)
        stretchCls.dC.parent([self.gST])
        
        aimLine[0].setParent(self.gNT)
        
        self.invScaleStart = tp.nullGroup(Vlist=[self.VctrlStart], 
                                          Vhierarchy=5, Vtype=2, 
                                          Vname='ctrlA_sInv', 
                                          Vsuffix=Vsuffix)[0]

        
        self.VctrlStart.s >> self.invScaleStart.inverseScale
        self.VctrlStart.sx >> self.fkList[0].sx
        self.VctrlStart.sx >> self.VctrlStart.sy
        self.VctrlStart.sx >> self.VctrlStart.sz
        
        #self.VctrlStart.addAttr('fk_scaleX', defaultValue=1, min=0, keyable=1)
        #self.VctrlStart.fk_scaleX >> self.fkList[0].sx
        
        # space switches 
        if spaceswitch:
            self.spaceSwitches()
        
        # cleanup attributes 
        self.cleanupAttributes()
        
    def spaceSwitches(self):
        """space switches for all controllers to local and world"""
        dummyGoalA = tp.nullGroup(Vlist=[self.nullCtrl[1]], 
                                  Vhierarchy=1, Vtype=2, 
                                  Vname='dumyGl_A', 
                                  Vsuffix=self.suffix)[0]
        
        VctrlEndGoal = tp.nullGroup(Vlist=[self.VctrlEnd], 
                                  Vhierarchy=4, Vtype=1, 
                                  Vname='endGl', 
                                  Vsuffix=self.suffix)[0]                          
        
        self.gNT|dummyGoalA
        
        tc.spaceSwitch(headlineAttr=1,
                       Vlist=[self.gWT, self.VctrlStart, dummyGoalA, 
                              self.nullCtrl[1], self.VctrlEnd], 
                       VattrName=['world', 'limb_root', 'body'], 
                       VlastObj=1, Voffset=1)
        
        tc.spaceSwitch(headlineAttr=1,
                       Vlist=[self.gWT, self.invScaleStart, VctrlEndGoal, 
                              self.nullCtrl[2], self.VctrlPolv], 
                       VattrName=['world', 'limb_root', 'limb_end'], 
                       VlastObj=1, Voffset=1)
        
        zero = tc.zeroOutConstraint([self.gWT, self.gT,
                                    self.nullCtrl[0], self.VctrlStart],
                                    VlastObj=1)
        tc.spaceSwitch(headlineAttr=1,
                       Vlist=zero, 
                       VattrName=['world', 'limb_root', 'limb_end'], 
                       Vtype=2, VlastObj=1, Voffset=1)
        # defaults
        self.VctrlEnd.world.set(1)
        #self.VctrlEnd.dummy_a.setLocked(1)
        
        self.VctrlPolv.limb_root.set(0.5)
        self.VctrlPolv.limb_end.set(0.5)
        
        self.VctrlStart.limb_root.set(1)
        
        
    def cleanupAttributes(self):
        """lock and hide attributes"""
        for jnt in [self.fkList[1]]:
            attrList = [jnt.tx, jnt.ty, jnt.tz,
                        jnt.rx, jnt.ry, 
                        jnt.sy, jnt.sz, 
                        jnt.v, 
                        jnt.radius]
            for attr in attrList:
                attr.set (lock=1, keyable=0, channelBox=0)
                
        for jnt in [self.fkList[2]]:
            attrList = [jnt.tx, jnt.ty, jnt.tz,
                        jnt.sy, jnt.sz, 
                        jnt.v, 
                        jnt.radius]
            for attr in attrList:
                attr.set (lock=1, keyable=0, channelBox=0)
        
        for jnt in [self.VctrlEnd, self.VctrlPolv]:
            attrList = [jnt.sx, jnt.sy, jnt.sz, 
                        jnt.v,]
            for attr in attrList:
                attr.set (lock=1, keyable=0, channelBox=0)
        
        attrList = [self.VctrlPolv.rx, self.VctrlPolv.ry, self.VctrlPolv.rz,]
        for attr in attrList:
            attr.set (lock=1, channelBox=0, keyable=0)
        
        attrList = [self.VctrlStart.v, self.VctrlStart.sy, self.VctrlStart.sz,
                    self.attrObj.tx, self.attrObj.ty, self.attrObj.tz,
                    self.attrObj.rx, self.attrObj.ry, self.attrObj.rz,
                    self.attrObj.sx, self.attrObj.sy, self.attrObj.sz,
                    self.attrObj.v]
        for attr in attrList:
            attr.set (lock=1, channelBox=0, keyable=0)
        
            
        self.jointB.preferredAngleX.set(0)
        self.jointB.preferredAngleY.set(0)
        
        if self.inverseJunction:
            self.jointB.preferredAngleZ.set(-90)
        else:    
            self.jointB.preferredAngleZ.set(90)
    
    def __call__(self):
        return


class stretchDistribution():
    """
    distributes the stretch and overall scale between the first and 
    second segment
    """
    def __init__(self, Vlist=[], Vsuffix=''):
        
        Vlist = lsSl.lsSl(Vlist, 4, 4, ['joint', 'joint', 'joint', 'any'], 
                          """\nselect three joints and a ctrl to receive the attributes""")
        
        jointA = Vlist[0] 
        jointB = Vlist[1]
        jointC = Vlist[2]
        attrObj = Vlist[3]
        
        VdistA= td.get([jointA, jointB])
        VdistB = td.get([jointB, jointC])
        VdistAB = VdistA+VdistB
        
        attrObj.addAttr('stretch_distribution', defaultValue=0, min=(-(VdistB/VdistAB)),
                        max=(VdistA/VdistAB), keyable=1)
        
        
        minusMidOrig = pm.createNode('plusMinusAverage', 
                                   name='minusMidOrig%s' % (Vsuffix))
        minusMidOrig.input2D[1].input2Dx.set (VdistB/VdistAB)
    
        attrObj.stretch_distribution >> minusMidOrig.input2D[0].input2Dx
        
           
        # ((((AB + BC) * baseLen) - AB) / BC ) <blend> baseLen >> segment1
        # ((((AB + BC) * baseLen) - BC) / AB ) <blend> baseLen >> segment2 
       
        
        # get orig
        self.sectionOrigDist = pm.createNode('multiplyDivide', 
                                  name='section_orig_dist' + Vsuffix)
        self.sectionOrigDist.input2X.set(VdistA)
        self.sectionOrigDist.input2Y.set(VdistB)
        
        
        # (AB + BC) * baseLen
        self.multOrigBase = pm.createNode('multiplyDivide', 
                                  name='mult_orig_baseLen' + Vsuffix)
        self.multOrigBase.input1X.set(VdistAB)
        self.multOrigBase.input1Y.set(VdistAB)
        # baseLen
        self.multOrigBase.input2X.set(1)
        self.multOrigBase.input2Y.set(1)
        
        # mult_orig_baseLen - AB)
        self.subSection = pm.createNode('plusMinusAverage', 
                                   name='sub_section' + Vsuffix)
        self.subSection.operation.set(2)
        self.multOrigBase.outputX >> self.subSection.input3D[0].input3Dx
        self.multOrigBase.outputY >> self.subSection.input3D[0].input3Dy
        self.subSection.input3D[1].set([VdistA, VdistB, 1])
        
        # sub_section / AB
        self.divSection = pm.createNode('multiplyDivide', 
                                  name='div_section' + Vsuffix)
        self.divSection.operation.set(2)
        self.subSection.output3Dx >> self.divSection.input1X
        self.subSection.output3Dy >> self.divSection.input1Y
        self.divSection.input2X.set(VdistB)
        self.divSection.input2Y.set(VdistA)
        
        # divSection <blend> 1
        self.blendSectionDistr = pm.createNode('blendColors', 
                                    name='blend_sectionDistr' + Vsuffix)
        self.blendSectionDistr.color1G.set(1)
        self.blendSectionDistr.color2R.set(1)
        self.divSection.outputX >> self.blendSectionDistr.color1R
        self.divSection.outputY >> self.blendSectionDistr.color2G
        
        minusMidOrig.output2Dx >> self.blendSectionDistr.blender
        
        # multiply
        self.multDisMid = pm.createNode('multiplyDivide', 
                                  name='mult_distr_Mid' + Vsuffix)
        self.multDisMid.input1X.set(1)
        self.multDisMid.input1Y.set(1)
        
        self.blendSectionDistr.outputG >> self.multDisMid.input2X
        self.blendSectionDistr.outputR >> self.multDisMid.input2Y
        
        
        
        self.sectionOrigDist.outputX >> self.subSection.input3D[1].input3Dx
        self.sectionOrigDist.outputY >> self.subSection.input3D[1].input3Dy    
        
        self.sectionOrigDist.outputX>> self.divSection.input2Y
        self.sectionOrigDist.outputY >> self.divSection.input2X
        
        #self.connectOut(Vlist[0].sx, Vlist[1].sx) 
    
    def connectLength(self, attr):
        """Connects the overall scale."""
        attr >> self.multOrigBase.input2X
        attr >> self.multOrigBase.input2Y
        
        
    def connectSections(self, attrA, attrB):
        """Connects the baseLength (origLen*scale) of section A and B."""
        attrA >> self.sectionOrigDist.input1X
        attrB >> self.sectionOrigDist.input1Y
        
        attrA >> self.multDisMid.input1X
        attrB >> self.multDisMid.input1Y 
    
    def connectOut(self, attrA, attrB):
        """Connects the scale output to the rig (joints)."""
        self.multDisMid.outputX >> attrA
        self.multDisMid.outputY >> attrB


class stretch():
    """
    Basic Ik setup with base-length and soft stretch.

    Returns: 
        - attribute to connect to scale of jointA and JointB
        - main group
        - ik handle
        - start object
  
    ========= =================================================================
    Arguments Description
    ========= =================================================================
    Vlist[0]  jointA
    Vlist[1]  jointB
    Vlist[2]  JointC
    Vlist[3]  object to receive the Attributes
    Vsuffix   suffix (default '')
    Vconnect  connect to the joint scale (default 1)
    hardKeys  stretching with soft stretch off (1=linear, 2=soft)
    softKeys  stretching with soft stretch on (1=obsolete, 2=soft, 3=default)
    ========= =================================================================           
    """    
    def __init__(self, Vlist=[], Vsuffix='', Vconnect=1, hardKeys=2, 
                 softKeys=2):

        Vlist = lsSl.lsSl(Vlist, 4, 4, ['joint', 'joint', 'joint', 'any'], 
                          """\nselect three joints and a ctrl to receive the attributes""")
        
        jointA = Vlist[0] 
        jointB = Vlist[1]
        jointC = Vlist[2]
        attrObj = Vlist[3]
        
        VdistA= td.get([jointA, jointB])
        VdistB = td.get([jointB, jointC])
        VdistAB = VdistA+VdistB
        self.VdistAB = VdistAB
        
        self.VobStart = pm.spaceLocator (name='obStart%s' % (Vsuffix))
        self.VobEnd = pm.spaceLocator (name='obEnd%s' % (Vsuffix))
        # this group is no longer used till the parenting of the shoulder
        self.Vgrp = ''
        
        attrObj.addAttr('twist', defaultValue=0, keyable=1)
        attrObj.addAttr('length', defaultValue=1, min=.0001, keyable=1)
        attrObj.addAttr('soft_stretch', defaultValue=0, min=0, max=1, 
                        keyable=1)
    
        self.VadjLenDiv = pm.createNode('multiplyDivide', 
                                name='adjLengthDiv%s' % (Vsuffix))
        self.VadjLenDiv.operation.set (2)
    
        VblendSoftKeys = pm.createNode('blendColors', 
                                    name='blendSoftStretchKeys%s' % (Vsuffix))
        VblendSoftKeys.blender.set (1)
        VblendSoftKeys.color1R.set (2)
        VblendSoftKeys.color2R.set (2)
    
        self.VadjLenMult = pm.createNode('multiplyDivide', 
                                      name='adjLengthMult%s' % (Vsuffix))
        self.VadjLenMult.operation.set (1)
    
        snap.snap ([self.VobStart, jointA])
        snap.snap ([self.VobEnd, jointC])
        
        self.Vhandle = pm.ikHandle(startJoint=jointA,endEffector=jointC, 
                                solver='ikRPsolver',
                                name='handle%s' % (Vsuffix))
        pm.rename(self.Vhandle[1], 'effector' + Vsuffix)
        self.Vhandle[0].setParent(self.VobEnd)
    
        self.dC = td.dist([self.VobStart, self.VobEnd])
        
        attrObj.twist >> self.Vhandle[0].twist
        attrObj.length >> self.VadjLenMult.input2X
        attrObj.length >> self.VadjLenDiv.input2X
         
        self.dC.connect([self.VadjLenDiv.input1X]) 
        VblendSoftKeys.outputR >>  self.VadjLenMult.input1X
        VblendSoftKeys.outputR >>  self.VadjLenMult.input1Y
     
        # hard keys -----------------------------------------------------------
        if hardKeys == 1:
            # values with soft stretch off (for straight joints)
            tk.drvKeySet(valList=[[0.60*VdistAB, 1], 
                                  [0.95*VdistAB, 0.975], 
                                  [1.00*VdistAB, 1], 
                                  [2.00*VdistAB, 2]], 
                                  VattrDriver=self.VadjLenDiv.outputX, 
                                  VattrDriven=VblendSoftKeys.color2R)
        
        if hardKeys == 2:
            # linear (for testing with bended setups)
            tk.drvKeySet(valList=[[1.00*VdistAB, 1], 
                                  [2.00*VdistAB, 2]], 
                                  VattrDriver=self.VadjLenDiv.outputX, 
                                  VattrDriven=VblendSoftKeys.color2R)
        
        if hardKeys == 3:
            # leg does not straighten (for limb_lug)
            tk.drvKeySet(valList=[[1.00*self.dC.distance(), 1], 
                                  [2.00*self.dC.distance(), 2]], 
                                  VattrDriver=self.VadjLenDiv.outputX, 
                                  VattrDriven=VblendSoftKeys.color2R)
        
        # soft keys -----------------------------------------------------------
        if softKeys == 1:
            # old keys 
            # only gets a little bit longer the end (1.03), but it gets 
            # shorter at the beginning
            tk.drvKeySet(valList=[[0.57*VdistAB, 1], 
                                  [0.65*VdistAB, 0.986], 
                                  [0.74*VdistAB, 0.967], 
                                  [0.88*VdistAB, 0.953], 
                                  [0.94*VdistAB, 0.967], 
                                  [1.00*VdistAB, 1.004], 
                                  [1.03*VdistAB, 1.03],
                                  [1.06*VdistAB, 1.06],  
                                  [2.00*VdistAB, 2]],
                                  VattrDriver=self.VadjLenDiv.outputX, 
                                  VattrDriven=VblendSoftKeys.color1R)
        if softKeys == 2:
            # gets long (1.13) but is very soft
            tk.drvKeySet(valList=[[0.57*VdistAB, 1], 
                                  [0.65*VdistAB, 0.993], 
                                  [0.74*VdistAB, 0.978], 
                                  [0.88*VdistAB, 0.977], 
                                  [0.94*VdistAB, 0.99323], 
                                  [1.00*VdistAB, 1.022775], 
                                  [1.05*VdistAB, 1.056885],
                                  [1.09*VdistAB, 1.091235],  
                                  [1.13*VdistAB, 1.13],
                                  # this must be set as a linear key
                                  [1.2*VdistAB, 1.2], 
                                  [2.00*VdistAB, 2]], 
                                  VattrDriver=self.VadjLenDiv.outputX, 
                                  VattrDriven=VblendSoftKeys.color1R)
     
        if softKeys == 3:
            # very good blending (longest 1.06) 
            tk.drvKeySet(valList=[[0.57*VdistAB, 1], 
                                  [0.65*VdistAB, 0.993229], 
                                  [0.74*VdistAB, 0.978269], 
                                  [0.88*VdistAB, 0.976143], 
                                  [0.94*VdistAB, 0.983745], 
                                  [1.00*VdistAB, 1.010038], 
                                  [1.04*VdistAB, 1.04101], 
                                  [1.07*VdistAB, 1.07], 
                                  [2.00*VdistAB, 2]], 
                                  VattrDriver=self.VadjLenDiv.outputX, 
                                  VattrDriven=VblendSoftKeys.color1R)
            
        if softKeys == 4:
            # leg does not straighten (for limb_lug)
            tk.drvKeySet(valList=[[0.57*self.dC.distance(), 1], 
                                  [0.65*self.dC.distance(), 0.993], 
                                  [0.74*self.dC.distance(), 0.978], 
                                  [0.88*self.dC.distance(), 0.977], 
                                  [0.94*self.dC.distance(), 0.99323], 
                                  [1.00*self.dC.distance(), 1.022775], 
                                  [1.05*self.dC.distance(), 1.056885],
                                  [1.09*self.dC.distance(), 1.091235],  
                                  [1.13*self.dC.distance(), 1.13],
                                  # this must be set as a linear key
                                  [1.2*self.dC.distance(), 1.2], 
                                  [2.00*self.dC.distance(), 2]], 
                                  VattrDriver=self.VadjLenDiv.outputX, 
                                  VattrDriven=VblendSoftKeys.color1R)
    
        pm.keyTangent (VblendSoftKeys.color1R, itt='spline', ott='spline')
        pm.keyTangent (VblendSoftKeys.color2R, itt='spline', ott='spline')
        pm.keyTangent (VblendSoftKeys.color1R, itt='spline', ott='linear', 
                    f=[(1.07*VdistAB)])
    #    keyTangent (VblendSoftKeys.color2R, itt='spline', ott='linear', f=[(VlinKeyB)])
        pm.setInfinity (VblendSoftKeys.color1R, poi='linear')
        pm.setInfinity (VblendSoftKeys.color2R, poi='linear')
    
        attrObj.soft_stretch >> VblendSoftKeys.blender
        
        # CLEANUP
        pm.parentConstraint(self.VobStart, jointA, mo=1)
       
        # Connect if setup finishes here don't, connect if there are other 
        # setups that affect the joint-length.
        if Vconnect==1:
            self.VadjLenMult.outputX >> jointA.sx
            self.VadjLenMult.outputX >> jointB.sx
            
    def __call__(self):
        """returns objects to be connected"""
        return [self.VadjLenMult.outputX, self.Vgrp, self.Vhandle, 
                self.VobStart, self.VobEnd]


class slide():
    """ 
    mid slide setup
 
    returns: 
    attributes to connect to scale of jointA and JointB [AttrA, AttrB]
  
    ========= =================================================================
    Arguments Description
    ========= ================================================================= 
    Vlist[0]  jointA
    Vlist[1]  jointB
    Vlist[2]  JointC
    Vlist[3]  object to receive the Attributes
    Vsuffix   suffix
    Vconnect  connect to the joint scale (default 1) 
    ========= ================================================================= 
    """       
    def __init__(self, Vlist=[], Vsuffix='', Vconnect=1):

        Vlist = lsSl.lsSl(Vlist, 4, 4, ['joint', 'joint', 'joint', 'any'], 
                          """\nselect three joints and a ctrl to receive the attributes""")
        
        jointA = Vlist[0] 
        jointB = Vlist[1]
        jointC = Vlist[2]
        attrObj = Vlist[3]
           
        VdistA= td.get([jointA, jointB])
        VdistB = td.get([jointB, jointC])
        VdistAB = VdistA+VdistB
        
        attrObj.addAttr('slide', defaultValue=0, min=(-(VdistB/VdistAB)),
                        max=(VdistA/VdistAB), keyable=1)
        
        VminusMidOrig = pm.createNode('plusMinusAverage', 
                                   name='minusMidOrig%s' % (Vsuffix))
        VminusMidOrig.input1D[1].set (VdistB/VdistAB)
    
        self.VblendMid = pm.createNode('blendColors', 
                                    name='blendMid%s' % (Vsuffix))
        self.VblendMid.color1R.set (0)
        self.VblendMid.color2R.set (VdistB/VdistA+1)
        self.VblendMid.color1G.set (VdistA/VdistB+1)
        self.VblendMid.color2G.set (0)
        
        attrObj.slide >> VminusMidOrig.input1D[0]
        VminusMidOrig.output1D >> self.VblendMid.blender
    
        if Vconnect==1:
            self.VblendMid.outputR >> jointA.sx
            self.VblendMid.outputG >> jointB.sx
             
        # CLEANUP
    def __call__(self): 
        """returns objects to be connected"""   
        return [self.VblendMid.outputR, self.VblendMid.outputG]

class scale():
    """ 
    Setup to set the length of joints A/B and B/C independently with an.
    ``lengthA`` and ``lengthB`` attribute.
  
    ========= =================================================================
    Arguments Description
    ========= ================================================================= 
    Vlist[0]  jointA
    Vlist[1]  jointB
    Vlist[2]  JointC
    Vlist[3]  object to receive the Attributes
    Vsuffix   suffix
    ========= ================================================================= 
    """       
    def __init__(self, Vlist=[], Vsuffix=''):

        Vlist = lsSl.lsSl(Vlist, 4, 4, ['joint', 'joint', 'joint', 'any'], 
                          """\nselect three joints and a ctrl to receive the attributes""")
        
        jointA = Vlist[0]
        jointB = Vlist[1]
        jointC = Vlist[2]
        attrObj = Vlist[3]
        
        ab = td.dist([jointA, jointB])
        bc = td.dist([jointB, jointC])

        AB = ab.distance()
        BC = bc.distance()
        AC = AB+BC
        
        blend = pm.createNode('blendColors', name='blendMid' + Vsuffix)
        
        mid = AB/AC
        blend.blender.set(mid)
        
        attrObj.addAttr('lengthAB', keyable=1, dv=1)
        attrObj.addAttr('lengthBC', keyable=1, dv=1)
        attrObj.addAttr('lengthAC', keyable=1)
        
        attrObj.lengthAB >> blend.color1G
        attrObj.lengthBC >> blend.color2G
        
        blend.outputG >> attrObj.lengthAC
        
        ab.calibrate()
        bc.calibrate()
        
        self.distGrp = pm.group(em=1, w=1, n='dist'+Vsuffix)
        ab.parent([self.distGrp])
        bc.parent([self.distGrp])
        
        self.out = attrObj.lengthAC
        self.inAB = blend.color1G
        self.inBC = blend.color2G 

class stretchScale():
       
    def __init__(self, Vlist=[], Vsuffix=''):

        Vlist = lsSl.lsSl(Vlist, 4, 4, ['joint', 'joint', 'joint', 'any'], 
                          """\nselect three joints and a ctrl to receive the attributes""")
        jointA = Vlist[0]
        jointB = Vlist[1]
        jointC = Vlist[2]
        attrObj = Vlist[3]
        
        self.scaleCls = scale(Vlist, Vsuffix=Vsuffix + '_sc')
        self.strchCls = stretch(Vlist, Vsuffix=Vsuffix + '_st')
        
        
        attrObj.lengthAC >> self.strchCls.VadjLenDiv.input2X
        attrObj.lengthAB >> self.strchCls.VadjLenMult.input2X;      
        attrObj.lengthBC >> self.strchCls.VadjLenMult.input2Y;
        
        self.strchCls.VadjLenMult.outputX >> jointA.scaleX;
        self.strchCls.VadjLenMult.outputY >> jointB.scaleX;
        
        self.strchCls.VobStart     
        
class pinJunction():
    """
    Creates an attribute that makes the limb junction stick to the pol vector.
    """
    def __init__(self, Vlist=[], Vsuffix='', Vconnect=1):
        Vlist = lsSl.lsSl(Vlist, 5, 5, ['joint', 'joint', 'any'])
        
        jointA = Vlist[0] 
        jointB = Vlist[1]
        jointC = Vlist[2]
        ikCtrl = Vlist[3]
        
        polVector = Vlist[4]
        
        pvLoc = pm.group(n='aim_at_pv'+Vsuffix, em=1, w=1)
        snap.snapTrans([pvLoc, jointB])
        
        self.distA = td.dist([jointA, pvLoc], VdimensionLine=0)
        self.distA.calibrate(1)
        
        snap_jc = pm.group(em=1, w=1, n='snapToJointC' + Vsuffix)
        snap.snapTrans([snap_jc, jointC])
        ikCtrl|snap_jc
        self.distB = td.dist([snap_jc, pvLoc], VdimensionLine=0)
        self.distB.calibrate(1)
        
        snap.snapTrans([pvLoc, polVector])
        polVector|pvLoc
        
        self.blend = pm.createNode('blendColors', name='blendLen' + Vsuffix)
        self.blend.blender.set(0)
        self.blend.color2R.set(1)
        self.blend.color2G.set(1)
        
        self.distA.connect([self.blend.color1R])
        self.distB.connect([self.blend.color1G])
        
        ikCtrl.addAttr('pin_to_pol_vector', defaultValue=0, min=0, max=1, keyable=1)
        ikCtrl.pin_to_pol_vector >> self.blend.blender
        
        if Vconnect:
            self.connectOutput(jointA.sx, jointB.sx)
        
        self.distGrp = pm.group(n='grp_dist'+Vsuffix, em=1, w=1)
        self.distA.parent([self.distGrp])
        self.distB.parent([self.distGrp])
            
    
    def connectOutput(self, attrA, attrB):
        self.blend.outputR >> attrA
        self.blend.outputG >> attrB
    
    def connectLength(self, attrA, attrB):
        attrA >> self.blend.color2R
        attrB >> self.blend.color2G
    
    
class limbStart():
    """
    object that will be parented to the shoulder or hip rig.
    it should make it possible to connect the armrig by just parenting.
    the shoulder should be oriented to the first joint.
    all new options for the limb connection should be added here
    """    
    def __init__(self, Vlist):

        print '\n----------\nlimbStart setup'
        print Vlist
        print '----------\n'
          
        return


class limbEnd():
    """
    Connections for different types of appendages..
   
    Arguments:
     - ik end 
     - fk end 
     - ctrl for attribute
     - drv end
     - attrObj
    """
    def __init__(self, endType, Vproxy=1, proxyScale=1, side=2, **kwdArgs):
        
        self.Vproxy = Vproxy
        self.proxyScale = proxyScale
        self.side = side
        
        if endType == 1:
            self.hand(**kwdArgs)
        
        elif endType ==2:
            self.foot(**kwdArgs)
    
    def hand(self, Vlist=[], Vsuffix=''):
        """
        Connection for hand.
        """
        Vlist = lsSl.lsSl(Vlist, 3, 4)
         
        jointA = Vlist[0] 
        fkEnd = Vlist[1]
        obEnd = Vlist[2]
        drvEnd = Vlist[3]
        attrObject = Vlist[-1]
        
        self.VhandPar = pm.spaceLocator(n='handPar%s' % (Vsuffix))
        
        bldIkFk = tc.constraintSwitch(Vlist=[fkEnd, obEnd, self.VhandPar, 
                                             attrObject], VattrName='ik_fk', 
                                             Vsuffix='', Vtype=2, Voffset=0)
        tc.cons(Vlist=[drvEnd, self.VhandPar], Vtype=1)
        
        #fix wrist ------------------------------------------------------------
        consRoFix = tc.cons(Vlist=[drvEnd, self.VhandPar], Vtype=2)
        
        blendFix = pm.createNode('blendColors', name='blendFix%s' % (Vsuffix))
        blendFix.blender.set (1)
        blendFix.color1R.set (0)
        blendFix.color2R.set (1)
        blendFix.color1G.set (1)
        blendFix.color2G.set (0)
    
        blendFix.outputR >> bldIkFk.color1R
        blendFix.outputR >> bldIkFk.color2G
        weight =  tc.filterConsWeights(Vlist=consRoFix,  Vtype=2)
        
        attrObject.addAttr('fix_wrist', defaultValue=0, min=0, max=1, 
                         keyable=1, at='double')
        
        attrObject.fix_wrist >> weight[-1]
        attrObject.fix_wrist >> blendFix.blender
        
        
        handBnd = tp.nullGroup(Vlist=[self.VhandPar], Vhierarchy=4, Vtype=2, 
                               Vsuffix='_bnd')[0]
        scaleProx = 1
        if self.side == 2: 
            scaleProx = -1                      
        if self.Vproxy:
            self.px = pg.proxyGeo(Vlist=[handBnd], Vsuffix='_px', 
                                  xScale=scaleProx, 
                                  yzScale=0, yzScaleGlobal=self.proxyScale * 0.08, 
                                  Vsides=6, chain=0) 
    
    def foot(self, Vlist=[], Vsuffix=''):
        """
        Connection for foot.
        """
        Vlist = lsSl.lsSl(Vlist, 3, 4)
         
        jointA = Vlist[0] 
        fkEnd = Vlist[1]
        obEnd = Vlist[2]
        drvEnd = Vlist[3]
        attrObject = Vlist[-1]
        
        self.VhandPar = pm.spaceLocator(n='handPar%s' % (Vsuffix))
        bldIkFk = tc.constraintSwitch(Vlist=[fkEnd, obEnd, self.VhandPar, 
                                             attrObject], VattrName='ik_fk', 
                                             Vsuffix='', Vtype=2, Voffset=0)
        tc.cons(Vlist=[drvEnd, self.VhandPar], Vtype=1)
        
        # parent a shape to the limb end
        #tcv.controlShape([obEnd])
    
    def __call__(self):
        return self.VhandPar

class limbBendPosition():
    """
    """
    
    def __init__(self, Vlist=[], Vsuffix=''):
    
        self.sl = lsSl.sl(Vlist, 11, 0,
                          slDict={
                                  'jointA':'0',
                                  'jointB':'1',
                                  'jointC':'2',
                                  
                                  'ctrlA':'3',
                                  'ctrlB':'4',
                                  'pv':'5',
                                  
                                  'attrObj':'6',
                                  'fkB':'7',
                                  'fkC':'8',
                                  'bendCtrlA':'9',
                                  'bendCtrlB':'10',
                                  'footList':'11:18'
                                  })
    
    def connect(self):
                #unlock attributes
        print self.sl.Vlist
        for i in range(len(self.sl.Vlist)):
            Vattr= pm.listAttr(self.sl.Vlist[i], keyable=1, locked=1) 
            print   self.sl.Vlist[i] 
            self.sl.Vlist[i].t.setLocked(0)
            self.sl.Vlist[i].r.setLocked(0)
            self.sl.Vlist[i].s.setLocked(0)
       
            for j in range(len(Vattr)):    
                VattrObj = pm.ls ('%s.%s' % (self.sl.Vlist[i], Vattr[j]))
                VattrObj[0].set (lock=0, keyable=1, channelBox=1)
                VattrObj[0].set (keyable=1)
                print Vattr
        
        
        self.sl.jointA|self.sl.jointC
        
        self.sl.jointB|self.sl.pv
        self.sl.jointB|self.sl.fkB
        self.sl.jointC|self.sl.fkC
        
        pm.pointConstraint(self.sl.ctrlA, self.sl.jointC, self.sl.jointB)
        
        pm.pointConstraint(self.sl.jointA, self.sl.jointB, self.sl.bendCtrlA)
        pm.pointConstraint(self.sl.jointB, self.sl.jointC, self.sl.bendCtrlB)
        
        pm.pointConstraint(self.sl.jointB, self.sl.jointC, self.sl.bendCtrlB)
        
        pm.pointConstraint(self.sl.ctrlA, self.sl.jointA)
        pm.pointConstraint(self.sl.ctrlB, self.sl.jointC)
        
        pm.orientConstraint(self.sl.jointA, self.sl.ctrlA)
        

        
        self.sl.jointA.t.set(0,0,0)
        self.sl.jointA.r.set(0,0,0)
        
        self.sl.jointB.t.set(0,0,0)
        self.sl.jointB.r.set(0,0,0)
        
        self.sl.jointC.t.set(0,0,0)
        self.sl.jointC.r.set(0,0,0)
        
        self.sl.pv.t.set(0,0,0)
        self.sl.pv.r.set(0,0,0)
        
        self.sl.bendCtrlB.t.setLocked(1)
        self.sl.bendCtrlB.r.setLocked(1)
        
        self.sl.bendCtrlA.t.setLocked(1)
        self.sl.bendCtrlA.r.setLocked(1)
        
        self.sl.fkB.t.setLocked(1)
        self.sl.fkB.r.setLocked(1)
        
        self.sl.fkC.t.setLocked(1)
        self.sl.fkC.r.setLocked(1)
        
        self.sl.pv.tx.setLocked(1)
        self.sl.pv.tz.setLocked(1)
        
        self.sl.pv.r.setLocked(1)
        
        
        
    
    def clean (self):
        #unlock attributes
        for i in range(len(self.sl.Vlist)):
            self.sl.Vlist[i].t.setLocked(0)
            self.sl.Vlist[i].r.setLocked(0)
            self.sl.Vlist[i].s.setLocked(0)
            Vattr= pm.listAttr(self.sl.Vlist[i], keyable=1, locked=1)        
            for j in range(len(Vattr)):    
                VattrObj = pm.ls ('%s.%s' % (self.sl.Vlist[i], Vattr[j]))
                VattrObj[0].set (lock=0, keyable=1, channelBox=1)
                VattrObj[0].set (keyable=1)
        
        #delete children that are constraints
        for o in self.sl.Vlist:
          
            cons = tc.filterCons(o.getChildren())
            pm.delete(cons)
        
        #try parent to world transforms  
        tp.unParent(self.sl.Vlist[3:])
        
        #freeze scale
        for o in self.sl.Vlist:
            pm.makeIdentity(o, apply=True, t=0, r=0, s=1, n=0)
        
        #try parent to world joints 
        tp.unParent(self.sl.Vlist[0:2])        
        
        
        #create hierarchy
        self.sl.jointA|self.sl.jointB|self.sl.jointC
         
        #rot jo orient
            
            

class limbBend():
    """
    Creates a complete limb setup with ik/fk and bend controllers.
                
    =========== ===============================================================
    Arguments   Description
    =========== ===============================================================
    Vlist[0]    first joint (of a tree joint chain)
    Vlist[1]    second joint
    Vlist[2]    third joint
                
    Vlist[3]    ctrl Start
    Vlist[4]    ctrl End
    Vlist[5]    ctrl Polvector
                
    Vlist[6]    attribute object
                
    Vlist[7]    Fk ctrl 1
    Vlist[8]    Fk ctrl 2
                
    Vlist[9]    bend ctrl 1
    Vlist[10]   bend ctrl 2
                
    Vsuffix     suffix
    endType     1 = hand
    proxy       proxy geometry
    midPosition position of elbow or knee
    sharpBend   add sharp bend setup
    side        1 = left, 2 = right
    =========== ===============================================================
    """
    def __init__(self, Vlist=[], Vsuffix='', endType=1, proxy=1, 
                 numJoints=10, midPosition=5, sharpBend=1, rollJunction=1,
                 side=2, spaceswitch=1, ribbon=0, pin=1, easyRoll=1,
                 inverseJunction=0):
        
        self.sl = lsSl.sl(Vlist, 11, 0,
                          slDict={
                                  'limbList':'0:9',
                                  'attrObj':'6',
                                  'bendCtrlA':'9',
                                  'bendCtrlB':'10',
                                  'footList':'11:18'
                                  })
        
        print self.sl.limbList[1]
        midOrient = self.sl.limbList[1].jointOrient.get()
        print midOrient
        
        if not midOrient == [0,0,0]:
            print 'mid orient offset found.'
            tempLoc = pm.spaceLocator()
        
            snap.snap([tempLoc, self.sl.limbList[4]])
                
            for f in self.sl.footList:
                self.sl.limbList[1]|f   
            
            self.sl.limbList[1]|self.sl.limbList[4] 
            self.sl.limbList[1].jointOrient.set([0,0,0])
            
            for f in self.sl.footList:
                pm.parent(f, w=1)    
            pm.parent(self.sl.limbList[4] , w=1) 
            
        rot = Vlist[0].rz.get()
        Vlist[0].rz.set(0)
                
        self.limbCls = limb(Vlist=self.sl.limbList, Vsuffix=Vsuffix, 
                       endType=endType, proxy=0, spaceswitch=spaceswitch, 
                       pin=pin, inverseJunction=inverseJunction,
                       side=side)
        
        if ribbon:
            # TODO: ribbon setup 
            print 'For ribbon limb setup, please use the limb_ribbon class.'
            pass
        
        else:
            # bend setup 
            bendCls = tb.bend(Vlist=[self.limbCls.drvList[0], self.limbCls.drvList[1], 
                                     self.limbCls.drvList[2], self.sl.bendCtrlA, 
                                     self.sl.bendCtrlB, self.sl.limbList[4], 
                                     self.sl.attrObj], 
                              Vsuffix='_bnd'+Vsuffix, numJoints=numJoints, 
                              midPosition=midPosition, sharpBend=sharpBend,
                              proxy=0, side=side)
            
            bendCls.gNT.setParent(self.limbCls.gNT)
            bendCls.gST.setParent(self.limbCls.gST)
            
            l = bendCls.drvChain + [self.limbCls.gConnect] + [self.limbCls.VhandPar] + [bendCls.AttrObj]
        
            # TODO: 'side' should be xUp, xDown and labelSide  
            rtCls = rt.startEndB (Vlist=l, Vsuffix='_rt'+Vsuffix, 
                                  midPosition=midPosition + 1, attrName='',
                                  side=side, rollJunction=rollJunction)
       
        
        
        Vlist[0].rz.set(rot)
        # this can move the setup slightly and should come at the very end.
        self.limbCls.jointB.preferredAngleX.set(0)
        self.limbCls.jointB.preferredAngleY.set(0)
        if  inverseJunction:
            self.limbCls.jointB.preferredAngleZ.set(-90)
        else:
            self.limbCls.jointB.preferredAngleZ.set(90)
        #
        
        if endType == 2:
            self.footCls = ra.foot(self.sl.footList + [self.limbCls.attrObj, 
                                                       self.limbCls.VctrlEnd], 
                                   Vsuffix, side, easyRoll=easyRoll)
            
            self.footCls.nullFk.setParent(self.limbCls.fkList[2])
            self.limbCls.endObj.setParent(self.footCls.sl.wrist)
            self.footCls.ikGrp.setParent(self.limbCls.VctrlEnd)
        
        if not midOrient == [0,0,0]:
            self.limbCls.fkList[1]    
            self.limbCls.fkList[1].rx.setLocked(0)
            self.limbCls.fkList[1].ry.setLocked(0)
            self.limbCls.fkList[1].rx.set(midOrient[0])
            self.limbCls.fkList[1].ry.set(midOrient[1])
            self.limbCls.fkList[1].rz.set(midOrient[2])
            self.limbCls.fkList[1].rx.setLocked(1)
            self.limbCls.fkList[1].ry.setLocked(1)
            
            #null = tp.nullGroup([self.sl.limbList[4]], Vhierarchy=1, 
            #                    Vprefix='straight')[0]
            
            #snap.snap([null, tempLoc])
            
            
            pm.delete(tempLoc)
            self.sl.limbList[6].soft_bend.set(0)
           

class limbBendHip():
    """
    Limb bend connected to hip.
 
    ============ ==============================================================
    Arguments    Description
    ============ ==============================================================
    Vlist[0:18]  limb list
    Clist[18:25] hip list
    ============ ==============================================================
 
    TODO: Add pol vector objects to template or add placement values for pvs
          as option.
    """
    
    def __init__(self, Vlist=[], Vsuffix='', endType=1, proxy=1, 
                 numJoints=10, midPosition=5, sharpBend=1, rollJunction=1,
                 side=2, numInterpolate=3, drvKeys=0, handleTwist=0, ribbon=0, 
                 pin=0, easyRoll=1, inverseJunction=0):
        
        self.sl = lsSl.sl(Vlist, 11, 0,
                          slDict={
                                  'limbList':'0:18',
                                  'hipList':'18:25'
                                  })
        
        limbBendCls = limbBend(Vlist=self.sl.limbList, Vsuffix=Vsuffix, 
                               endType=endType, proxy=proxy, 
                               numJoints=numJoints, midPosition=midPosition, 
                               sharpBend=sharpBend, rollJunction=rollJunction,
                               side=side, spaceswitch=0, ribbon=ribbon, 
                               pin=pin, easyRoll=easyRoll, 
                               inverseJunction=inverseJunction)
        
        hipCls = ll.hip(Vlist=self.sl.hipList, Vsuffix=Vsuffix + '_hip',  
                        proxy=proxy, numInterpolate=numInterpolate, side=side,
                        drvKeys=drvKeys, handleTwist=handleTwist)
        
        #footWrist|footPar
        #limbBendCls.footCls.sl.wrist|hipCls.footPar
        hipCls.connectFoot(limbBendCls.footCls.sl.wrist)
        
        #hipCtrl|limbTrans
        hipCls.lugCls.invCtrl|limbBendCls.limbCls.gT 
        
        #connect ik length
        limLen = limbBendCls.limbCls.VdistAB
        hipLen =  hipCls.str.VdistAB
        multLen = pm.createNode('multiplyDivide', 
                             name='multLen' + Vsuffix)
        
        addDif = pm.createNode('plusMinusAverage', 
                            name='addDif' + Vsuffix)
        
        addDif.addAttr('num', defaultValue=1-(limLen/hipLen))
        addDif.num >> addDif.input1D[0]
        
        limbBendCls.footCls.sl.keysObj.length >> multLen.input1X
        multLen.input2X.set(limLen/hipLen)
        multLen.outputX >> addDif.input1D[1]
        
        addDif.output1D >> hipCls.sl.ctrl.length 
        hipCls.sl.ctrl.length.set(lock=1, channelBox=1, keyable=0)
        
        # connect soft_stretch
        limbBendCls.footCls.sl.keysObj.soft_stretch >> hipCls.sl.ctrl.soft_stretch
        hipCls.sl.ctrl.soft_stretch.set(lock=1, channelBox=0, keyable=0)
        
        #ik swith >> auto hip
        hipCls.connectIkSwitch(limbBendCls.footCls.sl.attrObj.ik_fk)
        
        zero = tc.zeroOutConstraint([hipCls.gWT, 
                                     hipCls.gT, 
                                     limbBendCls.limbCls.nullCtrl[2],
                                     limbBendCls.limbCls.VctrlPolv],
                                    VlastObj=1)
        tc.spaceSwitch(headlineAttr=1, 
                       Vlist=zero, 
                       VattrName=['world', 'limb_root'], 
                       VlastObj=1, 
                       Voffset=1) 
        
        # auto rot attr on foot ik ctrl
        ikCtrl = limbBendCls.limbCls.VctrlEnd
        hipCtrl = hipCls.lugCls.sl.ctrl
        
        ikCtrl.addAttr('hip', defaultValue=0, min=0, keyable=1)
        ikCtrl.hip.setLocked(1)
        
        ikCtrl.addAttr('hip_auto_rot', at='double', defaultValue=0, 
                       max=1, min=0, keyable=1)
        
        ikCtrl.hip_auto_rot >> hipCtrl.auto_rot
        hipCtrl.auto_rot.setLocked(1)
        hipCtrl.auto_rot.setKeyable(0) 
        
#        ikCtrl.addAttr('hip_ctrl', defaultValue=0, min=0, max=1, 
#                         keyable=1, at='long')
#        
#        ikCtrl.hip_ctrl >> hipCtrl.getShape().v


class rubberLimb():
    """
    Simple rubber limb with ik ctls that can be connected to the ik setup.
    Inbetween joint for wrist and non scaling joint for limblug.
   
    ============ ==============================================================
    Arguments    Description
    ============ ==============================================================
    Vlist[0:2]   joints
    ============ ==============================================================
 
    TODO:
    Space switch world to chest.
    """
    def __init__(self, Vlist=[], Vsuffix='', numJoints=20, numCtls=7, 
                 ctlRadius=1, side=1, parentSpace=1):
        
        splits = tj.split(Vlist, numJoints=numJoints)
        splineC = tb.stretchySpline(Vlist, Vsuffix)
        splineC.rebuildCurve(numCtls-1)
        clusterC = tb.clusterComponent(Vlist=[splineC.curve], Vsuffix=Vsuffix, 
                                   ctrl_radius=ctlRadius, normal=[0,1,0])
        
        for i in range(len(clusterC.ctrls)):
            
            pm.rename(clusterC.ctrls[i], 'bend_' + str(i) + Vsuffix)
            
        clusterC.clusterLoc[0]|Vlist[0]
        
        
        self.gT = tp.trans(em=1, w=1, n='trans' + Vsuffix).group
        self.gNT = tp.noTrans(em=1, w=1, n='no_trans' + Vsuffix).group
        self.gMT = tp.mainTrans(em=1, w=1, n='main_trans' + Vsuffix).group
        
        self.gST = pm.group(em=1, w=1, n='scale_trans' + Vsuffix)
        self.gSNT = pm.group(em=1, w=1, n='scale_no_trans' + Vsuffix)
        pm.scaleConstraint(self.gST, self.gSNT, skip=['none', 'none', 'none'])
        
        pm.select(cl=1)
        inbGoal = pm.joint()
        snap.snap([inbGoal,  Vlist[0]])
        clusterC.clusterLoc[0]|inbGoal
        
        tj.jointInbCons(Vlist=[Vlist[0], inbGoal], Vsuffix='',
                        Vproxy=1, interpolateScale=1, 
                        label='',
                        proxyScale=(splineC.crvInf.arcLength.get()/18), 
                        side=3, type=2,)
        
        splits[-1].s >> Vlist[-1].inverseScale
        
        handBnd = tp.nullGroup(Vlist=[Vlist[1]], Vhierarchy=4, Vtype=2, 
                               Vsuffix='_noScale', connectInverseScale=1)[0]
                               
        #
                               
        self.gSNT.setParent(self.gNT)
        self.gST.setParent(self.gMT)
        
        splineC.connectScale(self.gSNT.sx)
        self.gNT|clusterC.clusterGrp
        self.gNT|splineC.curve
        self.gNT|splineC.handle
        
        self.gMT|clusterC.ctrlGrp
        
        self.gT|clusterC.null_clusterLoc[0]
        
        mf.builder.nodes.set(clusterC.ctrls, 'transform_ctrl')
        pg.proxyGeo(Vlist=[Vlist[0]] + splits + [Vlist[1]], yzScaleGlobal=0.05)
        
        
        for c in clusterC.ctrls:
            attrList = [c.sx, c.sy, c.sz,c.v]
            
            for attr in attrList:
                attr.set (lock=1, channelBox=0, keyable=0)
                
        pg.proxyGeo(Vlist=[handBnd], Vprefix='', Vname='', 
                    Vsuffix='_bnd', Vorient=1, xScale=1.1, 
                    yzScale=0, yzScaleGlobal=0.1, customGeo=0, 
                    Vsides=6, VcenterPivot=1,
                    chain=0, shader=1, Vcolor=[0.2, 0.4, 0.4], 
                    shaderName='shoulder_shader',
                    side=side, label='non_scale')
        
        clusterC.ctrls[-1].addAttr('twist')
        clusterC.ctrls[-1].twist.setKeyable(1)
        clusterC.ctrls[-1].twist >> splineC.handle.twist
        
        # space switches
        for i in range(len(clusterC.null_clusterLoc)-1):
            space1 = tp.nullGroup(Vlist=[clusterC.null_clusterLoc[i+1]], Vhierarchy=1, Vtype=2, 
                               Vsuffix='_space', connectInverseScale=1)[0]
            space2 = tp.nullGroup(Vlist=[clusterC.null_clusterLoc[i+1]], Vhierarchy=1, Vtype=2, 
                               Vsuffix='_space', connectInverseScale=1)[0]
            tc.constraintSwitch(Vlist=[space1, space2, clusterC.null_clusterLoc[i+1], clusterC.clusterLoc[i+1]], 
                                VattrName='space', Vsuffix='', Vtype=3, Voffset=1)
            
            if parentSpace:
                clusterC.clusterLoc[i]|space2
        
        
        
        
       
        