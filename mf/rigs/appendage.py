"""
hand, feet, claws, etc. 
"""
import pymel.core as pm
import mf.builder.nodes
from mf.tools import lsSl
from mf.tools import snap
from mf.tools import distance as td
from mf.tools import curve as tcv
from mf.tools import parent as tp
from mf.tools import joint as tj
from mf.tools import cons as tc
from mf.tools import proxyGeo as pg


class hand():
    """
    Hand setup.
    
    =============== ===================================================
    Arguments       Description
    =============== ===================================================
    Vlist[0]        wrist
    Vlist[1]        attrObj
    Vlist[0]        cup_root -- [cup_joints]+[finger_root_joints] 
    numCupJoints    pivots for the cup attribute (min value is 2)
    inbetweenJoints Rotating in between joints.
    fist            Extra groups for a custom "fist" shape.
    =============== ===================================================
    """
    def __init__(self, Vlist=[], Vsuffix='', numCupJoints=4, side=1, 
                 ctrlScale=1, inbetweenJoints=1, easyOrient=0, fist=0,
                 ctlRotateOrder=3):
        """TODO: Add attrObj for each finger."""
        self.sl = lsSl.sl(Vlist, 1, 2,
          slDict={
                  'wrist':'0',
                  'attrObj':'1',
                  'cup_root':'2:' 
                  }, Vsuffix=Vsuffix)
        
        mf.builder.nodes.set([self.sl.attrObj], 'transform_ctrl')
        
        self.side = side
        self.suffix = Vsuffix
        self.ctrlScale =ctrlScale
        self.inbetweenJoints = inbetweenJoints
        self.cup = self.sl.cup_root[:numCupJoints]
        self.root = self.sl.cup_root[numCupJoints:]
        self.easyOrient = easyOrient
        self.ctlRotateOrder = ctlRotateOrder
        
        fingerRoot = []
        for o in self.root:
            fingerRoot.append(self.getFinger(o)[1])
                
        self.makeHierarchy()
        
        self.setFingerLength()
        
        pm.rename(self.sl.wrist, 'hand' + self.suffix)
        
        pm.rename(self.sl.attrObj, 'finger' + self.suffix)
        
        self.makeFinger()
      
        self.sl.attrObj.addAttr('hand', defaultValue=0, keyable=1, at='bool')
        self.sl.attrObj.hand.set(1)
        self.sl.attrObj.hand.setLocked(1)
        
        if fist:
            self.fist(self.fingerLength*0.8)
        
        self.makeCup('cup', 'rx')
        
        self.spread(self.root[1:], 'spread_a')
        self.spread(fingerRoot[1:], 'spread_b')
        
        self.sl.attrObj.addAttr('spread_thumb', defaultValue=0, 
                         keyable=1, at='double')
        self.sl.attrObj.spread_thumb >> self.root[0].ry
        
        self.nice()
              
        self.sl.attrObj.addAttr('size', defaultValue=1, 
                                keyable=1, at='double')
        self.sl.attrObj.size >> self.scale.sx
        self.sl.attrObj.size >> self.scale.sy
        self.sl.attrObj.size >> self.scale.sz
         
        self.cleanup()
        
    def spread(self, joints, attrName):
        numJoints = len(joints)
        if numJoints == 4:
            # four finger spread
            midInd = (len(joints)/2)
            pinkySide = joints[midInd:]
            thumbSide = joints[0]
        
            self.sl.attrObj.addAttr(attrName, defaultValue=0, 
                             keyable=1, at='double')
            
            attr = pm.ls(str(self.sl.attrObj) + '.' + attrName)[0]
            
            for o in pinkySide:
                attr >> o.ry
                
            multPinky = pm.shadingNode('multiplyDivide', asUtility=1, 
                                        name='mult' + attrName + self.suffix)
            attr >> multPinky.input1X
            multPinky.input2X.set(-2)
            
            rev = pm.createNode('multiplyDivide', name='rev' + attrName + self.suffix)
            rev.input2X.set(-1)
            attr >> rev.input1X
            
            rev.outputX >> pinkySide[0].ry
            multPinky.outputX >> pinkySide[1].ry
            
            attr >> thumbSide.ry
            
        if numJoints == 3:
            # tree finger spread
            self.sl.attrObj.addAttr(attrName, defaultValue=0, 
                             keyable=1, at='double')
            attr = pm.ls(str(self.sl.attrObj) + '.' + attrName)[0]
            attr >> joints[0].ry
            
            multPinky = pm.shadingNode('multiplyDivide', asUtility=1, 
                                        name='mult' + attrName + self.suffix)
            attr >> multPinky.input1X
            multPinky.input2X.set(-0.4)
            multPinky.outputX >> joints[1].ry
                
            rev = pm.shadingNode('multiplyDivide', asUtility=1, 
                                        name='mult' + attrName + self.suffix)
            
            attr >> rev.input1X
            rev.input2X.set(-1.4)
            rev.outputX >> joints[2].ry
            
        if numJoints == 2:
            # tree finger spread
            self.sl.attrObj.addAttr(attrName, defaultValue=0, 
                             keyable=1, at='double')
            attr = pm.ls(str(self.sl.attrObj) + '.' + attrName)[0]
            attr >> joints[0].ry
            
            multPinky = pm.shadingNode('multiplyDivide', asUtility=1, 
                                        name='mult' + attrName + self.suffix)
            attr >> multPinky.input1X
            multPinky.input2X.set(-0.8)
            multPinky.outputX >> joints[1].ry
                
            rev = pm.shadingNode('multiplyDivide', asUtility=1, 
                                        name='mult' + attrName + self.suffix)
            

    def makeHierarchy(self):
        root = self.root[1:]
        cup = self.cup[1:]
        root.reverse()
        cup.reverse()
        
        self.cup[0]|self.root[0]
        self.sl.wrist|self.cup[0]
        self.sl.wrist|self.cup[1]
        
        for i in range(len(root)):
            try:
                cup[i]|root[i]
            except:
                self.sl.wrist|root[i]
                    
        for i in range(len(cup)-1):
            cup[i+1]|cup[i]
    
    def makeCup(self, attrSrc, attrDest):
        for i in range(len(self.cup)): 
            pm.rename(self.cup[i], 'cup_' + str(i) + self.suffix)  
            self.sl.attrObj.addAttr(attrSrc + '_' + str(i+1), defaultValue=0, 
                         keyable=1, at='double')
            attr = pm.ls(str(self.sl.attrObj) + '.' + attrSrc + '_' + str(i+1))[0]
            attrJoint = pm.ls(str(self.cup[i]) + '.' + attrDest)[0]
            attr >> attrJoint
            
            # no ctls needed for the cup joints
            self.makeFingerCtrls(self.cup[i], self.fingerLength, 
                                'finger_ctrls' )    
        pg.proxyGeo(self.cup, yzScaleGlobal=0.1*self.dist, chain=0, 
                    VcenterPivot=1, side=self.side, label= attrSrc + str(i))
    
    def setFingerLength(self):
            chain = self.getFinger(self.root[0])
            self.fingerLength = 0
            for j in range(len(chain)-1):
                self.fingerLength = self.fingerLength + td.get([chain[j], 
                                                                chain[j+1]])   
    
    def headline(self, attrName):
        self.sl.attrObj.addAttr(attrName, defaultValue=0, 
                         keyable=1, at='double')
        attr = pm.ls(str(self.sl.attrObj) + '.' + attrName)[0]
        attr.setLocked(1)
    
    def nice(self):
        self.sl.attrObj.addAttr('nice_a', defaultValue=0, keyable=1, at='double')
        self.sl.attrObj.addAttr('nice_b', defaultValue=0, keyable=1, at='double')
        
        deg_a = -0.5
        deg_b = 0
        for f in self.fingerAll[1:]:
            deg_a = deg_a + 0.2
            deg_b = deg_b + 0.3
            multNice = pm.shadingNode('multiplyDivide', asUtility=1, 
                                    name='mult' + self.suffix)
            self.sl.attrObj.nice_a >> multNice.input1X
            self.sl.attrObj.nice_b >> multNice.input1Y
            
            multNice.input2X.set(deg_a)
            multNice.input2Y.set(deg_b)
            
            addKeysA = pm.createNode('plusMinusAverage', 
                                      name='add' + self.suffix)
            
            multNice.outputX >> addKeysA.input3D[0].input3Dx
            multNice.outputY >> addKeysA.input3D[1].input3Dx
            
            self.sl.attrObj.nice_a >> multNice.input1Z
            multNice.input2Z.set(deg_a)
            
            null = tp.nullGroup(Vlist=f, Vhierarchy=5, Vtype=2, Vsuffix='_nice')
            
            multNice.outputX >> null[0].rz
            addKeysA.output3D.output3Dx >> null[1].rz
            
            for n in null[2:]:
                multNice.outputY >> n.rz
        
    def makeFinger(self):
        fingerRoot = []
        
        for i in range(len(self.root)):
            fingerRoot.append([self.root[i]])
        
        for i in range(len(fingerRoot)):
            pm.rename(fingerRoot[i][0], 'hand_' + str(i+1) + self.suffix)
            self.rename(fingerRoot[i][0], 'finger_' + str(i+1) + self.suffix)
        
        self.fingerAll = []
        for i in range(len(fingerRoot)):    
            self.fingerAll.append(self.getFinger(fingerRoot[i][0]))
        
        for i in range(len(fingerRoot)):    
            finger = self.getFinger(fingerRoot[i][0])
            
            if not self.easyOrient:
                for f in finger:
                    pass
                    #f.rotateOrder.set(3)
        
        ctrls = []
        ctrlsRoot = []
        for i in range(len(self.fingerAll)):
            chain = self.fingerAll[i]
            
            ctrlsFinger = []
            
            ctrl = self.makeFingerCtrls(chain[0], self.fingerLength,
                                                  'finger_ctrls')
            ctrlsFinger.append(ctrl)
            
            ctrl = self.makeFingerCtrls(chain[1], self.fingerLength,
                                                'finger_ctrls')
            ctrlsFinger.append(ctrl)
            
            for o in chain[2:-1]:
                ctrl = self.makeFingerCtrls(o, self.fingerLength,
                                                  'finger_ctrls')
                ctrlsFinger.append(ctrl)
              
            ctrls.append(ctrlsFinger) 
            ctrlsRoot.append(ctrlsFinger[1])
        
        if self.easyOrient:
            for i in range(len(fingerRoot)):    
                for f in finger:
                    pass
                    #f.rotateOrder.set(3)
            
#        for i in range(len(self.fingerAll)):
#            self.makeAttr(self.fingerAll[i][0:2], 
#                          'root', 'rz', ctrlsRoot[i])
#        
#        for i in range(len(self.fingerAll)):
#            self.makeAttr(self.fingerAll[i][2:], 
#                          'curl', 'rz', ctrlsRoot[i])
        
        for i in range(len(self.fingerAll)):
            junctions = self.fingerAll[i][0:-1]
            fingerName = 'finger_'+str(i)
            if i == 0:
                fingerName = 'thumb'
            if i == 1:
                fingerName = 'index'
            if i == 2:
                fingerName = 'middle'
            
            if len(self.fingerAll)== 4:
                if i == 3:
                    fingerName = 'pinky'
            else:
                if i == 3:
                    fingerName = 'ring'
                if i == 4:
                    fingerName = 'pinky'
                
            self.sl.attrObj.addAttr(fingerName, k=1, at='bool')
            self.sl.attrObj.attr(fingerName).set(1)
            self.sl.attrObj.attr(fingerName).setLocked(1)
            
            
            for j in range(len(junctions)):
                self.makeAttr([junctions[j]], 
                              fingerName +'_curl_'+str(j), 
                              'rz', self.sl.attrObj)
        
        for i in range(len(fingerRoot)):
            pg.proxyGeo(ctrls[i] + [self.fingerAll[i][-1]], yzScaleGlobal=0.07, 
                        side=self.side, 
                        label='finger_'+str(i+1) + '_' + str(10+1))
            
            self.dist = 0
            for j in range(len(ctrls[i])-1):
                self.dist = self.dist + td.get([ctrls[i][j], ctrls[i][j+1]])
            
            if self.inbetweenJoints:
                for j in range(len(ctrls[i])-1):
                    tj.jointInbCons(Vlist=[ctrls[i][j], ctrls[i][j+1]], Vproxy=1, 
                                    proxyScale=0.12*self.dist, side=self.side,
                                    label='inb_'+ str(i+1) + '_' +str(j+1))
        
        self.scale = tp.trans(em=1, w=1, n='scale' + self.suffix).group
        snap.snap([self.scale, self.sl.wrist])
        
        self.trans = tp.trans(em=1, w=1, n='trans' + self.suffix).group
        snap.snap([self.trans, self.sl.wrist])
        
        self.trans|self.scale|self.sl.wrist
        self.scale|self.sl.attrObj
            
    def getFinger(self, root):
        chain = [root]
        child = root.listRelatives(allDescendents=1)
        child.reverse()
        chain = chain + child
        return chain
        
    def rename(self, root, name):
        chain = self.getFinger(root)[1:]
        for i in range(len(chain)):
            pm.rename(chain[i], name + '_' + str(i))
       
    def makeAttr(self, finger, attrSource, attrDest, attrObj):               
        attrObj.addAttr(attrSource, defaultValue=0, 
                         keyable=1, at='double')
        attr = pm.ls(str(attrObj) + '.' + attrSource)[0]
        
        for i in range(len(finger)):    
            attrJoint = pm.ls(str(finger[i]) + '.' + attrDest)[0]
            attr >> attrJoint
    
    def fist(self, scale):
        self.sl.attrObj.addAttr('fist', defaultValue=0, 
                         keyable=1, at='double')
        
        fist_ctls = []
        centerMult = 1
        if self.side == 1:
            centerMult = -1
        
        for i in range(len(self.fingerAll)):
            for e in self.fingerAll[i]:
 
                fst = tp.nullGroup([e], Vhierarchy=5, Vtype=2,
                               connectInverseScale=1, Vsuffix='_fist')[0]  
                tcv.controlShape([fst],  radius=(self.ctrlScale*scale)*0.015, 
                         center=[0,(centerMult * scale)*0.2,0])
                                
        for e in self.cup: 
            fst = tp.nullGroup([e], Vhierarchy=5, Vtype=2,
                               connectInverseScale=1, Vsuffix='_fist')[0]
            
            tcv.controlShape([fst],  radius=(self.ctrlScale*scale)*0.015, 
                         center=[0,(centerMult * scale)*0.2,0])
       
          
    def makeFingerCtrls(self, joint, scale, attrName):
        ctrl= tp.nullGroup([joint], Vhierarchy=5, Vtype=2,
                            connectInverseScale=1, Vsuffix='_ctrl')[0]
        ctrl.rotateOrder.set(self.ctlRotateOrder)
        centerMult = 1
        if self.side == 1:
            centerMult = -1
        tcv.controlShape([ctrl],  radius=(self.ctrlScale*scale)*0.015, 
                         center=[0,(centerMult * scale)*0.2,0])
        
        if not self.sl.attrObj.hasAttr(attrName): 
            self.sl.attrObj.addAttr(attrName, defaultValue=0, max=1, min=0, 
                                    keyable=1, at='long')
        
        visAttr = pm.ls(str(self.sl.attrObj) + '.' + attrName)[0]
        
        visAttr >> ctrl.getShape().v
        
        mf.builder.nodes.set([ctrl], 'transform_ctrl_extra')
        
        ctrl.v.setLocked(1)
        ctrl.radius.setLocked(1)
        
        ctrl.v.setKeyable(0)
        ctrl.radius.setKeyable(0)
        return ctrl
    
    def cleanup(self):
        attrList = [self.sl.attrObj.tx, self.sl.attrObj.ty, self.sl.attrObj.tz,
                    self.sl.attrObj.rx, self.sl.attrObj.rz, self.sl.attrObj.ry,
                    self.sl.attrObj.sx, self.sl.attrObj.sy, self.sl.attrObj.sz,
                    self.sl.attrObj.v]
        for attr in attrList:
            attr.set (lock=1, channelBox=0, keyable=0)
            
        pg.proxyGeo([self.sl.wrist], yzScaleGlobal=0.1*self.dist, chain=0, 
                    VcenterPivot=1, side=self.side, label='wrist')
        
            
class foot():
    """
    Foot setup ik and fk.
    """
    def __init__(self, Vlist=[], Vsuffix='', side=3, easyRoll=0):
        
        self.sl = lsSl.sl(Vlist, 9, 9,
                  slDict={
                          'wrist':'0',
                          'ball':'1',
                          'tip':'2',
                          'heel':'3',
                          'sidePivA':'4',
                          'sidePivB':'5',
                          'ctrlBall':'6',
                          'attrObj':'7',
                          'keysObj':'8'
                          })
        self.suffix = Vsuffix
        pm.rename (self.sl.wrist, ('wrist' + Vsuffix))
        pm.rename (self.sl.ball, ('ball' + Vsuffix))
        pm.rename (self.sl.tip, ('tip' + Vsuffix))
        pm.rename (self.sl.heel, ('heelA' + Vsuffix))
        pm.rename (self.sl.ctrlBall, ('ctrlBall' + Vsuffix))
        
        pm.rename (self.sl.sidePivA, ('sidePivA' + Vsuffix))
        pm.rename (self.sl.sidePivB, ('sidePivB' + Vsuffix))
        
        pm.rename (self.sl.attrObj, ('attrObj' + Vsuffix))
        pm.rename (self.sl.keysObj, ('foot' + Vsuffix))
        
        mf.builder.nodes.set([self.sl.attrObj], 'transform_ctrl')
        
        if not self.sl.attrObj.hasAttr('ik_fk'):
            self.sl.attrObj.addAttr('ik_fk', defaultValue=1, min=0, max=1,
                                        keyable=1, at='double')
        
        self.sl.keysObj.addAttr('foot', defaultValue=0, min=0, keyable=1)
        self.sl.keysObj.foot.setLocked(1)
        
        attr = ['heel_roll', 'ball_roll', 'tip_roll', 'heel_swivle',
                 'ball_swivle', 'tip_swivle', 'side_roll', 
                 'toe_rot_up', 'toe_rot_side']
        
        for a in attr:
            self.sl.keysObj.addAttr(a, defaultValue=0, 
                                    keyable=1, at='double')
            
        self.wristB = tp.nullGroup(Vlist=[self.sl.wrist], Vtype=2, 
                                   Vname='heelB', 
                                   Vsuffix=Vsuffix)[0]
        
        self.ballB = tp.nullGroup(Vlist=[self.sl.ball], Vtype=2, 
                                  Vname='heelC', 
                                  Vsuffix=Vsuffix)[0]
                                    
        ballCtrl = tp.nullGroup(Vlist=[self.sl.ball], Vtype=2, 
                                Vname='ball_fk_ctrl', 
                                Vsuffix=Vsuffix)[0]
                                
        ballKeys = tp.nullGroup(Vlist=[self.sl.ball], Vtype=2, 
                                Vname='ball_fk_keys', 
                                Vsuffix=Vsuffix)[0]
        
        rangeRoll = pm.createNode('setRange', name='rangeRoll%s' % (Vsuffix))
        rangeRoll.minX.set (0)
        rangeRoll.maxX.set (360)
        rangeRoll.oldMinX.set (0)
        rangeRoll.oldMaxX.set (360)
        
        rangeRoll.minY.set (-360)
        rangeRoll.maxY.set (0)
        rangeRoll.oldMinY.set (-360)
        rangeRoll.oldMaxY.set (0)
        
        self.sl.keysObj.toe_rot_up >> ballKeys.rz
        self.sl.keysObj.toe_rot_side >> ballKeys.ry
        
        ballKeys.rotateOrder.set(2)
        ballCtrl.rotateOrder.set(2)
        
        self.sl.keysObj.side_roll >> rangeRoll.valueX
        self.sl.keysObj.side_roll >> rangeRoll.valueY
        
        rangeRoll.outValueX >> self.sl.sidePivA.rx
        rangeRoll.outValueY >> self.sl.sidePivB.rx
        
        self.sl.keysObj.heel_roll >> self.sl.heel.rz
        
        self.sl.keysObj.ball_roll >> self.sl.ball.rz
        self.sl.keysObj.tip_roll >> self.sl.tip.rz
        
        self.sl.keysObj.heel_swivle >> self.sl.heel.ry
        self.sl.keysObj.ball_swivle >> self.sl.ball.ry
        
        self.sl.keysObj.tip_swivle >> self.sl.tip.ry
        
        self.sl.sidePivA|self.sl.sidePivB|self.sl.heel|self.sl.tip
        self.sl.tip|self.sl.ball|self.sl.wrist
        self.sl.ball|self.wristB
        self.sl.tip|self.ballB
        
        jointsIk = [self.wristB, self.ballB, self.sl.tip]
        self.jointsFk = tp.nullGroup(Vlist=jointsIk,
                                Vtype=2, Vname='fk1', Vsuffix=Vsuffix)
        
        self.nullFk = tp.nullGroup([self.jointsFk[0]], Vtype=2, Vhierarchy=1,
                                     Vprefix='null_')[0]
        
        self.nullFk|self.jointsFk[0]|self.jointsFk[1]|self.jointsFk[2]
        
    
        self.jointsDrv = tj.duplicateChain(self.jointsFk, ['drv1' + Vsuffix, 
                                                 'drv2' + Vsuffix, 
                                                 'drv3' + Vsuffix])
    
        jointsDrvNull = tp.nullGroup([self.jointsDrv[0]], Vtype=2, Vhierarchy=1,
                                     Vname='null_')[0]
        
        tc.constraintIkFk(VikList=jointsIk, VfkList=self.jointsFk, 
                          VdrvList=self.jointsDrv, 
                          Vattr=self.sl.attrObj.ik_fk, Vsuffix='', Voffset=1, 
                          Vctrl=self.sl.attrObj, VskippLast=1, blendScale=1, 
                          useConstraints=1)
        
        snap.snap([self.sl.ctrlBall, ballCtrl])
        
        tp.parentShape(Vlist=[self.sl.ctrlBall, ballCtrl])
        
        mf.builder.nodes.set([ballCtrl], 'transform_ctrl')
        
        self.jointsDrv[1]|ballKeys|ballCtrl|self.sl.ctrlBall
        
        footLen = td.get([self.sl.wrist, self.sl.ball]) + td.get([self.sl.ball, 
                                                                  self.sl.tip])
        prxList = [self.jointsDrv[0], ballCtrl, self.jointsDrv[2]]
        pg.proxyGeo(prxList, 
                    yzScaleGlobal=0.08, 
                    side=side, 
                    label='toe_')
        
        self.ikGrp = pm.group(em=1, w=1, n='grp_foot_ik'+ Vsuffix)
        jointsDrvNull.setParent(self.ikGrp)
        self.sl.sidePivA.setParent(self.ikGrp)
        
        tj.jointInbCons(Vlist=[self.jointsDrv[0], ballCtrl], Vproxy=1, 
                        proxyScale=0.13*footLen, side=side, label='toe_inb')
        
        if easyRoll:
            self.rollLimit()
        
    def rollLimit(self):
        self.sl.keysObj.addAttr('roll', defaultValue=0, keyable=1)
        self.sl.keysObj.addAttr('roll_limit', defaultValue=30, min=0.1, keyable=1)
        
        rangeRoll = pm.createNode('setRange', name='rangeRollLimit'+self.suffix)
        rangeAdd = pm.createNode('plusMinusAverage', name='range_add'+self.suffix)
        
        rangeRoll.outValueX >> self.sl.keysObj.heel_roll
        rangeRoll.outValueY >> self.sl.keysObj.ball_roll
        rangeRoll.outValueZ >> self.sl.keysObj.tip_roll
        
        self.sl.keysObj.roll >> rangeRoll.valueX
        self.sl.keysObj.roll >> rangeRoll.valueY
        self.sl.keysObj.roll >> rangeRoll.valueZ
        
        rangeRoll.minX.set (180)
        rangeRoll.minY.set (0)
        rangeRoll.minZ.set (0)
        
        rangeRoll.maxX.set (0)
        self.sl.keysObj.roll_limit >> rangeRoll.maxY
        rangeRoll.maxZ.set (180)
        
        rangeRoll.oldMinX.set (-180)
        rangeRoll.oldMinY.set (0)
        self.sl.keysObj.roll_limit >> rangeRoll.oldMinZ
        
        rangeRoll.oldMaxX.set (0)
        self.sl.keysObj.roll_limit >> rangeRoll.oldMaxY
        rangeAdd.output3D.output3Dx >> rangeRoll.oldMaxZ
        
        self.sl.keysObj.roll_limit >> rangeAdd.input3D[0].input3Dx
        rangeAdd.input3D[1].input3Dx.set(180)
        
        self.sl.keysObj.heel_roll.setLocked(1)
        self.sl.keysObj.ball_roll.setLocked(1)
        self.sl.keysObj.tip_roll.setLocked(1)
        
        self.sl.keysObj.heel_roll.set(lock=1, keyable=0, channelBox=0)
        self.sl.keysObj.ball_roll.set(lock=1, keyable=0, channelBox=0)
        self.sl.keysObj.tip_roll.set(lock=1, keyable=0, channelBox=0)
        
        

        
class toes():
    """
    Simple Toe setup with spread and curl.
   
    ========== ===================================================
    Arguments  Description
    ========== ===================================================
    Vlist[:]   list of root joints
    ========== ===================================================
    """    
    def __init__(self, Vlist=[], Vsuffix='', side=2, extraCtrls=0):
        self.sl = lsSl.sl(Vlist, 8, 9,
                          slDict={
                                  'root':':-1',
                                  'attrObj':'-1'
                                  }, Vrename=1, Vsuffix=Vsuffix)
        self.suffix = Vsuffix
        self.side = side
        self.sl.attrObj.rename('roll' + self.suffix)
        
        self.gT = tp.trans(em=1, w=1, n='trans' + self.suffix).group
        mf.builder.nodes.set([self.sl.attrObj], 'transform_ctrl')
        
        for o in self.sl.root:  
            print o  
            self.sl.attrObj.ry >> o.rz
            self.gT|o
            cl = o.getChildren(allDescendents=1)
            cl.reverse()
            chain = [o] + cl
            for i in range(len(chain)):
                pm.rename(chain[i], str(o) + '_' + str(i))
            
            for c in cl[:-1]:
                self.sl.attrObj.ry >> c.rz
            
            
            pg.proxyGeo(chain, yzScaleGlobal=0.3, chain=1, 
                        VcenterPivot=0, side=side, label='ctrl')
            
            if extraCtrls:
                for o in chain[0:-1]:
                    ctrl = self.makeFingerCtrls(o, 4, 'toe_ctrls')
                
        self.spread(self.sl.root)
        self.nullCtrl = tp.nullGroup([self.sl.attrObj], Vtype=1, Vhierarchy=3,
                                     Vname='null_ctrl' + self.suffix)[0]
        self.gT|self.nullCtrl    
        
        attrList = [self.sl.attrObj.tx, self.sl.attrObj.ty, 
                    self.sl.attrObj.tz, 
                    self.sl.attrObj.sy, self.sl.attrObj.sz, self.sl.attrObj.sx, 
                    self.sl.attrObj.v]
        
        for attr in attrList:
            attr.set (lock=1, keyable=0, channelBox=0)
        pm.select(self.sl.attrObj)
        
    def spread(self, joints):
        """Spread for four toes."""
        if len(joints) == 4:
            driverAttr = self.sl.attrObj.rz
            multSpread = pm.shadingNode('multiplyDivide', asUtility=1, 
                                        name='mult' + self.suffix)
            driverAttr >> multSpread.input1X
            driverAttr >> multSpread.input1Y
            driverAttr >> multSpread.input1Z
            
            multSpread.input2X.set(-2)
            multSpread.input2Y.set(-1)
            multSpread.input2Z.set(2)
            
            multSpread.outputX >> joints[0].ry
            multSpread.outputY >> joints[1].ry
            driverAttr >> joints[2].ry
            multSpread.outputZ >> joints[3].ry
            
            #on x
            multRoll = pm.shadingNode('multiplyDivide', asUtility=1, 
                                        name='mult' + self.suffix)
            self.sl.attrObj.rx >> multRoll.input1X
            self.sl.attrObj.rx >> multRoll.input1Y
            self.sl.attrObj.rx >> multRoll.input1Z
            
            self.sl.attrObj.rx >> multRoll.input1X
            self.sl.attrObj.rx >> multRoll.input1Y
            self.sl.attrObj.rx >> multRoll.input1Z
            
            multRoll.input2X.set(-2)
            multRoll.input2Y.set(-1)
            multRoll.input2Z.set(2)
            
            multRoll.outputX >> joints[0].rx
            multRoll.outputY >> joints[1].rx
            self.sl.attrObj.rx >> joints[2].rx
            multRoll.outputZ >> joints[3].rx
        
        elif  len(joints) == 3:
            driverAttr = self.sl.attrObj.rz
            multSpread = pm.shadingNode('multiplyDivide', asUtility=1, 
                                        name='mult' + self.suffix)
            driverAttr >> multSpread.input1X
            driverAttr >> multSpread.input1Y
            driverAttr >> multSpread.input1Z
            
            multSpread.input2X.set(-2)
            multSpread.input2Y.set(-1)
            multSpread.input2Z.set(2)
            
            multSpread.outputX >> joints[0].ry
            multSpread.outputZ >> joints[2].ry
            
            #on x
            multRoll = pm.shadingNode('multiplyDivide', asUtility=1, 
                                        name='mult' + self.suffix)
            self.sl.attrObj.rx >> multRoll.input1X
            self.sl.attrObj.rx >> multRoll.input1Y
            self.sl.attrObj.rx >> multRoll.input1Z
            
            multRoll.input2X.set(-2)
            multRoll.input2Y.set(-1)
            multRoll.input2Z.set(2)
            
            multRoll.outputX >> joints[0].rx
            multRoll.outputZ >> joints[2].rx
        
    def makeFingerCtrls(self, joint, scale, attrName):
        
        ctrl= tp.nullGroup([joint], Vhierarchy=5, Vtype=2,
                            connectInverseScale=1, Vsuffix='_ctrl')[0]
        ctrl.rotateOrder.set(3)
        centerMult = 1
        if self.side == 1:
            centerMult = -1
        tcv.controlShape([ctrl],  radius=(1*scale)*0.015, 
                         center=[0,(centerMult * scale)*0.2,0])
        
        if not self.sl.attrObj.hasAttr(attrName): 
            self.sl.attrObj.addAttr(attrName, defaultValue=0, max=1, min=0, 
                                    keyable=1, at='long')
        
        visAttr = pm.ls(str(self.sl.attrObj) + '.' + attrName)[0]
        
        visAttr >> ctrl.getShape().v
        
        mf.builder.nodes.set([ctrl], 'transform_ctrl_extra')
        
        ctrl.v.setLocked(1)
        ctrl.radius.setLocked(1)
        
        ctrl.v.setKeyable(0)
        ctrl.radius.setKeyable(0)
        
        return ctrl


class fkAppendage():
    """
    Simple Fk appendage for tails ears etc.

    ========== ===================================================
    Arguments  Description
    ========== ===================================================
    Vlist[0]   root of a joint chain
    Vlist[1]   root ctrl curve
    Vlist[2]   joint for the ctrl curve
    ctlSize    size for fk ctrls.
    ========== ===================================================
    
    Note: the 'trans' group and also its parent need to be joints, so 
    inverse scale works properly. 
    """
    def __init__(self, Vlist=[], Vsuffix='', ctrlSize=1, numFirstSection=3,
                 inbetweenJoints=1, side=0, bindJoints=1, numRootInb=2, 
                 interpolateScale=0, rootCtrl=1):
        
        self.sl = lsSl.sl(Vlist, 3, 3,
                  slDict={
                          'root':'0',
                          'ctrl':'1',
                          'ctrlJnt':'2',
                          
                          }, 
                          Vrename=1, Vsuffix=Vsuffix)
        
        mf.builder.nodes.set([self.sl.ctrl], 'transform_ctrl')
        
        self.suffix = Vsuffix
        self.ctrlSize = ctrlSize
        self.inbetweenJoints = inbetweenJoints
        self.side = side
        
        snap.snap([self.sl.ctrl, self.sl.ctrlJnt])
        self.sl.ctrl|self.sl.ctrlJnt
        self.rootCtrl = rootCtrl
        
        alljoints = self.getChain(self.sl.root)
        chain = alljoints[:-1]
        for i in range(len(chain)):
            pm.rename(chain[i], 'j_' + str(i) + self.suffix)
        
             
        self.nullCtrl = tp.nullGroup([self.sl.ctrl], Vtype=1, Vhierarchy=3,
                                     Vname='null_ctrl' + self.suffix)[0]
                                     
        self.nullRoot = tp.nullGroup([self.sl.root], Vtype=1, Vhierarchy=3,
                                     Vname='null_root' + self.suffix)[0]
                                
                                     
        self.gSG = tp.nullGroup([self.sl.root], Vtype=1, Vhierarchy=1,
                                     Vname='scale_goal' + self.suffix)[0]
                                     
        self.gSRC = tp.nullGroup([self.sl.root], Vtype=2, Vhierarchy=1,
                                     Vname='parent_to_trans' + self.suffix)[0]
                                     
                                     
        self.gT = tp.trans(em=1, w=1, n='trans' + self.suffix).group
        snap.snap([self.gT, self.sl.ctrl])
                                     
        tc.cons([self.gSRC, self.gSG], Vtype=3)
        
        self.gT|self.gSRC
        self.gT|self.gSG|self.nullCtrl
        self.sl.ctrl|self.nullRoot
        
               
        self.gMT = tp.mainTrans(em=1, w=1, n='main_trans' + Vsuffix).group
        
        self.gWT = pm.group(em=1, w=1, n='world_trans' + Vsuffix)
        self.gWT.setParent(self.gMT)
                                     
        keys = tp.nullGroup(chain[1:], Vtype=2, Vhierarchy=3,
                            Vprefix='keys_')
            
        
        ctrls = []
        self.startCtrl = self.makeStartCtrl(chain[0])
        ctrls.append(self.startCtrl)
        
        self.startCtrl.addAttr('extra_ctrls', defaultValue=0, max=1, min=0, 
                                keyable=1, at='long')
        
        if not self.rootCtrl:
            firstKeys = tp.nullGroup([chain[0]], Vtype=2, Vhierarchy=3,
                                Vprefix='keys_')
            
            self.startCtrl.addAttr('rot_root', defaultValue=0, min=0, keyable=1)
            self.startCtrl.rot_root.setLocked(1)
            
            self.makeKeys(firstKeys[:numFirstSection], 'rot_x_0', 'rx', 0)
            self.makeKeys(firstKeys[:numFirstSection], 'rot_y_0', 'ry', 0)
            self.makeKeys(firstKeys[:numFirstSection], 'rot_z_0', 'rz', 0)
            
        
        self.startCtrl.addAttr('first_section', defaultValue=0, min=0, keyable=1)
        self.startCtrl.first_section.setLocked(1) 
        
        self.makeKeys(keys[:numFirstSection], 'rot_x_1', 'rx', 0)
        self.makeKeys(keys[:numFirstSection], 'rot_y_1', 'ry', 0)
        self.makeKeys(keys[:numFirstSection], 'rot_z_1', 'rz', 0)
        
        self.startCtrl.addAttr('second_section', defaultValue=0, min=0, keyable=1)
        self.startCtrl.second_section.setLocked(1)
         
        self.makeKeys(keys[numFirstSection:], 'rot_x_2', 'rx', 0) 
        self.makeKeys(keys[numFirstSection:], 'rot_y_2', 'ry', 0)
        self.makeKeys(keys[numFirstSection:], 'rot_z_2', 'rz', 0)
            
        for o in chain[1:]:
            ctrls.append(self.makeCtrls(o))
            
        for i in range(len(ctrls)):
            pm.rename(ctrls[i], 'ctrl_' + str(i) + self.suffix)
        
        self.dist = 0    
        for j in range(len(alljoints)-1):
                self.dist = self.dist + td.get([alljoints[j], alljoints[j+1]])
        
        if self.inbetweenJoints:
            for j in range(len(ctrls)-1):
                tj.jointInbCons(Vlist=[ctrls[j], ctrls[j+1]], Vproxy=1, 
                                proxyScale=0.12*self.dist, side=self.side,
                                label='inb_'+ str(i+1) + '_' +str(j+1),
                                interpolateScale=0)
        
        if len(ctrls) > 1:
            pg.proxyGeo(ctrls[0:2], yzScaleGlobal=0.5, 
                            side=self.side, 
                            label='fk_start_')
        else:
            pg.proxyGeo([ctrls[0], alljoints[1]], yzScaleGlobal=0.5, 
                            side=self.side, 
                            label='fk_start_')
        
        if bindJoints:    
            pg.proxyGeo(ctrls[1:] + [alljoints[-1]], yzScaleGlobal=0.05, 
                        side=self.side, 
                        label='fk_jnts_')
        
        pg.proxyGeo([self.sl.ctrl], yzScaleGlobal=0.1*self.dist, chain=0, 
                    VcenterPivot=1, side=self.side, label='ctrl')
        
        zero = tc.zeroOutConstraint([self.gWT, self.sl.ctrl,
                                    self.nullRoot, self.sl.ctrl],
                                    VlastObj=1)
        
        tc.spaceSwitch(headlineAttr=1,
                       Vlist=zero, 
                       VattrName=['world', 'body'], 
                       Vtype=2, VlastObj=1, Voffset=1)
        
        attrList = [self.sl.ctrl.sx, self.sl.ctrl.sy, self.sl.ctrl.sz, 
                    self.sl.ctrl.v]
        for attr in attrList:
            attr.set (lock=1, keyable=0, channelBox=0)
            
        self.sl.ctrl.body.set(1)
        
        self.rootInb(numRootInb, interpolateScale)
    
    def rootInb(self, num, interpolateScale):
        goal = tp.nullGroup([self.sl.ctrl], Vhierarchy=4, Vtype=2, 
                            Vprefix='goal_', 
                            connectInverseScale=1)[0]
        goalFix = tp.nullGroup([self.sl.ctrl], Vhierarchy=1, Vtype=2, 
                               Vprefix='goalFix_', 
                               connectInverseScale=1)[0]
        
        self.gSG|goalFix
        
        inb = tp.nullGroup([self.sl.ctrl], Vhierarchy=4, Vtype=2, 
                           Vprefix='inb_', 
                            connectInverseScale=1, Vnum=num)
        
        tc.distribute(inb + [goal, goalFix], Vtype=3)
        
        prx = pg.proxyGeo(inb, VcenterPivot=1, chain=0, side=self.side, 
                          label='ikRtInb')
        
        if interpolateScale:
            # scale source to connect scale for interpolation                       
            scaleSource = tp.nullGroup([self.sl.ctrl], Vhierarchy=1, Vtype=2, 
                                       Vprefix='scaleSource_', 
                                       connectInverseScale=1)[0]
                                       
            tc.distribute(inb + [goal, goalFix], Vtype=4)
        
        
                                      
    def getChain(self, root):
        chain = [root]
        child = root.listRelatives(allDescendents=1)
        child.reverse()
        chain = chain + child
        return chain
    
    def makeKeys(self, chain, attrSource, attrDest, defVal):
        self.startCtrl.addAttr(attrSource, defaultValue=defVal, 
                         keyable=1, at='double')
        
        attr = pm.ls(str(self.startCtrl) + '.' + attrSource)[0]
        
        for i in range(len(chain)):    
            attrJoint = pm.ls(str(chain[i]) + '.' + attrDest)[0]
            attr >> attrJoint
    
    def makeStartCtrl(self, joint):
        if not self.rootCtrl:
            ctrl = self.sl.ctrl
        
        else:
            ctrl = tp.nullGroup([joint], Vtype=2, Vhierarchy=5,
                                         connectInverseScale=1,
                                         Vprefix='ctrl_')[0]
                                             
            tcv.controlShape([ctrl],  radius=self.ctrlSize, center=[0,0,0])
            #self.startCtrl.extra_ctrls >> ctrl.getShape().v
            
            attrList = [ctrl.tx, ctrl.ty, ctrl.tz,
                        ctrl.sy, ctrl.sz, 
                        ctrl.v, ctrl.radius]
            for attr in attrList:
                attr.set (lock=1, keyable=0, channelBox=0)
            
            mf.builder.nodes.set([ctrl], 'transform_ctrl')
        
        return ctrl
    
    def makeCtrls(self, joint):
        ctrl = tp.nullGroup([joint], Vtype=2, Vhierarchy=5,
                                     connectInverseScale=1,
                                     Vprefix='ctrl_')[0]
                                     
        tcv.controlShape([ctrl],  radius=self.ctrlSize, center=[0,0,0])
        self.startCtrl.extra_ctrls >> ctrl.getShape().v
        
        attrList = [ctrl.tx, ctrl.ty, ctrl.tz,
                    ctrl.sy, ctrl.sz, 
                    ctrl.v, ctrl.radius]
        for attr in attrList:
            attr.set (lock=1, keyable=0, channelBox=0)
        
        mf.builder.nodes.set([ctrl], 'transform_ctrl_extra')
        return ctrl
    

class fkAddon():
    """
    Simple addon object with inverse scale connected.
    """
    def __init__(self):
        pass
     
                                     

            

        
      
