"""
This is a patch to add a dynamic setup to a limb.
Add this patch in bind pose.
"""
import maya.cmds as cm
import pymel.core as pm
from pymel.core import PyNode as PN
import mf.tools.dynamic as td
import mf.tools.lsSl as lsSl
import mf.tools.distance as dist


reload(td)


class patch():
    def __init__(self, Vsuffix='_armL'):
        """
        find all objects in scene
        TODO:
        1. Add third setup for an extra dynamic curve.
           One controller on start, middle and end of limb.
        2. Make setup generic for arms and legs.


p = patch('_legL')
p.addDynamics()

p = patch('_legL')
p.addDynamics()

p = patch('_armR')
p.addDynamics()
p.setDefaults()

p = patch('_armL')
p.addDynamics()
p.setDefaults()
        """
        self.suffix = Vsuffix
        if pm.objExists('attrObj_end_rt' + self.suffix):
            self.attrObj = PN('attrObj_end_rt' + self.suffix)
        else:
            self.attrObj = PN('attrObj' + self.suffix)

        self.curve_bnd = PN('curve_bnd' + self.suffix)
        self.Cluster_bnd = PN('Cluster_bnd' + self.suffix)
        self.Cluster_bnd1 = PN('Cluster_bnd' + self.suffix + '1')
        self.Cluster_bnd2 = PN('Cluster_bnd' + self.suffix + '2')
        self.Cluster_bnd3 = PN('Cluster_bnd' + self.suffix + '3')

        self.Cluster_bndSet = PN('Cluster_bnd' + self.suffix + 'Set')
        self.Cluster_bnd1Set = PN('Cluster_bnd' + self.suffix + '1Set')
        self.Cluster_bnd2Set = PN('Cluster_bnd' + self.suffix + '2Set')
        self.Cluster_bnd3Set = PN('Cluster_bnd' + self.suffix + '3Set')

        self.handPar = PN('handPar' + self.suffix)
        #self.jnt_drv14_bnd =  PN('jnt_drv14_bnd' + self.suffix) #14 is last in ths case
        for e in reversed(range(24)):
            objName = ('jnt_drv' + str(e) + '_bnd' + Vsuffix)
            if pm.objExists(objName):
                self.jnt_drv_bnd = PN(objName)
                break

        #self.cons_handPar_orient  = PN('cons_handPar' + Vsuffix)
        #self.cons_handPar_point   = PN('cons_handPar' + Vsuffix + '1')


    def addDynamics(self):
        ctrl = pm.spaceLocator()

        pm.sets(self.Cluster_bndSet, remove=self.curve_bnd.cv[0])
        pm.sets(self.Cluster_bnd1Set, remove=self.curve_bnd.cv[1])
        pm.sets(self.Cluster_bnd2Set, remove=self.curve_bnd.cv[2])
        pm.sets(self.Cluster_bnd3Set, remove=self.curve_bnd.cv[3])

        self.h = td.hairCurve([self.curve_bnd, ctrl], Vsuffix=self.suffix + '_dyn')


        pm.sets(self.Cluster_bndSet, add=self.h.goalCurve.cv[0])
        pm.sets(self.Cluster_bnd1Set, add=self.h.goalCurve.cv[1])
        pm.sets(self.Cluster_bnd2Set, add=self.h.goalCurve.cv[2])
        pm.sets(self.Cluster_bnd3Set, add=self.h.goalCurve.cv[3])

        pm.sets(self.Cluster_bndSet,  add=self.h.ctrlCurve.cv[0])
        pm.sets(self.Cluster_bnd1Set, add=self.h.ctrlCurve.cv[1])
        pm.sets(self.Cluster_bnd2Set, add=self.h.ctrlCurve.cv[2])
        pm.sets(self.Cluster_bnd3Set, add=self.h.ctrlCurve.cv[3])




        orientCons = pm.orientConstraint([self.jnt_drv_bnd, self.handPar])
        pointCons = pm.pointConstraint([self.jnt_drv_bnd, self.handPar])

        self.attrObj.addAttr('dynamic', defaultValue=0, min=0, max=1, keyable=1, at='double')
        self.attrObj.addAttr('dynamic_translate', defaultValue=0, min=0, max=1, keyable=1, at='double')
        self.attrObj.addAttr('dynamic_rotate', defaultValue=0, min=0, max=1, keyable=1, at='double')
        self.attrObj.addAttr("dynamic_attach" ,at="enum", en="Off:Base:Tip:BothEnds:",
                             dv=1, keyable=1)

        self.attrObj.dynamic_translate >> pointCons.w1
        reverse = pm.createNode('reverse')
        self.attrObj.dynamic_translate >> reverse.inputX
        reverse.outputX >> pointCons.w0

        self.attrObj.dynamic_rotate >> orientCons.w3
        switch(orientCons.w0, self.attrObj.dynamic_rotate, 0)
        switch(orientCons.w1, self.attrObj.dynamic_rotate, 0)
        switch(orientCons.w2, self.attrObj.dynamic_rotate, 0)

        self.attrObj.dynamic >> self.h.ctrl.dynamic_blend

        self.attrObj.dynamic_attach >> self.h.fol.pointLock

        wireProxy([self.h.dynCurve, self.curve_bnd])

    def setDefaults(self):
        attrVal = [['stiffness', 0.03],
                   ['damping', 0.1],
                   ['drag', 0.1],
                   ['curveAttract', 0],
                   ['lengthFlex', 0],
                   ['friction', 0.1],
                   ['gravity', 10],
                   ['strength', 0],
                   ['frequency', 1],
                   ['speed', 1],
                   ['startCurveAttract', 0.5]]

        for a in attrVal:
            getattr(self.h.ctrl, a[0]).set(a[1])


def switch(oldAttr, newAttr, val):
    """
    Build an overwrite for an existing attribute and its original input.

switch(PN('pCube1.v'), PN('nurbsCircle1.all_but_first_on'), 1)
switch(PN('pCube2.v'), PN('nurbsCircle1.all_but_first_on'), 0)
switch(PN('pCube3.v'), PN('nurbsCircle1.all_but_first_on'), 0)
    """
    inp = oldAttr.inputs(plugs=1)
    if inp:
        print inp[0]

    blend = pm.createNode('blendColors', name='blend')
    inp[0] >> blend.color2R
    blend.color1R.set(val)
    blend.outputR >> oldAttr
    newAttr >> blend.blender


class wireProxy():
    def __init__(self, Vlist=[], Vsuffix=''):
        """
        Creates a poly cylinder along a curve

wireProxy()
        """
        Vlist = lsSl.lsSl(Vlist, 2, 2)
        curve = Vlist[0]
        print '##########'
        print Vlist[1]
        boundCurve = pm.duplicate(Vlist[1])[0]
        startLoc = self.makeLocOnCV(Vlist[0].cv[0])
        endLoc = self.makeLocOnCV(Vlist[0].cv[-1])

        d = dist.dist([startLoc, endLoc])

        cylinder = pm.polyCylinder(r=1, h=1, sx=6, sy=20, ax=[1,0,0],
                                    rcp=0, cuv=3, ch=0)[0]
        cylinder.rotatePivotX.set(-0.5)
        cylinder.scalePivotX.set(-0.5)

        d.align([cylinder], parent=1)
        cylinder.sx.set(d.distance())
        cylinder.sy.set(d.distance()*0.03)
        cylinder.sz.set(d.distance()*0.03)

        blendSh = pm.blendShape(boundCurve, o="world", n='temp_BS')
        pm.blendShape(blendSh[0], e=1, t=[boundCurve, 0, Vlist[0].getParent(), 1])
        cm.setAttr(blendSh[0].name() + '.weight[0]', 1)

        wire([boundCurve, cylinder])

    def makeLocOnCV(self, cv):
        t = cv.getPosition(space='world')
        loc = pm.spaceLocator()
        loc.t.set([t[0], t[1], t[2]])
        return loc


class wire():
    """
wire()
    """
    def __init__(self, Vlist=[], Vsuffix=''):
        Vlist = lsSl.lsSl(Vlist, 1, 0)
        curve = Vlist[0]
        wire = pm.wire([curve], ignoreSelected=1)[0]
        wireSet = wire.message.outputs()[0]
        baseCurve = pm.duplicate(curve)[0]
        baseCurve.v.set(0)

        baseCurve.getShape().worldSpace[0] >> wire.baseWire[0]
        curve.getShape().worldSpace[0] >> wire.deformedWire[0]

        pm.sets(wireSet, add=Vlist[1:])

        wire.dropoffDistance[0].set(1000)


















