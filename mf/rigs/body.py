"""
body setup trunk, neck, head.  
"""
import pymel.core as pm
import mf.builder.nodes
from mf.tools import lsSl 
from mf.tools import snap
from mf.tools import parent as tp
from mf.tools import cons as tc
from mf.tools import proxyGeo as pg
from mf.tools import distance as td
from mf.tools import joint as tj
import mf.rigs.limb_lug as ll
from mf.tools import key as tk

class spineLug():
    """
    !This is just a test class.
    Spine setup for characters with round hips.
    Test with the limb_lug.
    """
    def __init__(self, Vlist=[], Vsuffix='', numInterpolate=3):
        
        self.sl = lsSl.sl(Vlist, 4, 4,
                  slDict={
                          'jointA':'0',
                          'jointB':'1',
                          'chest':'2',
                          'hip':'3'
                          },
                          Vrename=1, Vsuffix=Vsuffix)
        
        self.suffix = Vsuffix
        lugCls = ll.lug(Vlist=[self.sl.jointA, self.sl.jointB, self.sl.chest],
                        Vsuffix=self.suffix + '_lug',
                        numInterpolate=numInterpolate)
        
        lugCls.sl.ctrl|lugCls.ctrlPolv
        
        self.nullHip = tp.nullGroup([self.sl.hip], Vtype=1, Vhierarchy=3,
                                Vname='null_ctrl' + self.suffix)[0]
        self.sl.hip|lugCls.ctrlFix
        self.sl.hip|lugCls.ctrlInb
        
        self.sl.hip|lugCls.ctrlPar
        
               
class spine():
    """
    Spine setup for characters with round hips.
    """
    def __init__(self, Vlist=[], Vsuffix='', numInterpolateHip=3, 
                 numInterpolateChest=3, numInterpolateShoulder=3, side=3, 
                 drvKeys=0, midCtrl=0):
        
        self.midCtrl = midCtrl
        if self.midCtrl:
            self.sl = lsSl.sl(Vlist, 7, 7,
                      slDict={
                              'jointA':'0',
                              'jointB':'1',
                              'chestA':'2',
                              'chestB':'3',
                              'hip':'4',
                              'body':'5',
                              'mid':'6'
                              },
                              Vrename=1, Vsuffix=Vsuffix)
        
            mf.builder.nodes.set([self.sl.chestA, 
                                  self.sl.chestB, 
                                  self.sl.hip,
                                  self.sl.body,
                                  self.sl.mid], 
                                 'transform_ctrl')
            
        
        else:
            self.sl = lsSl.sl(Vlist, 6, 6,
                      slDict={
                              'jointA':'0',
                              'jointB':'1',
                              'chestA':'2',
                              'chestB':'3',
                              'hip':'4',
                              'body':'5'
                              },
                              Vrename=1, Vsuffix=Vsuffix)
        
            mf.builder.nodes.set([self.sl.chestA, 
                                  self.sl.chestB, 
                                  self.sl.hip,
                                  self.sl.body], 
                                 'transform_ctrl')

        
        self.suffix = Vsuffix
        self.side = side
        
        self.start = tp.nullGroup([self.sl.jointA], Vtype=3, 
                             Vname='start' + self.suffix)[0]
        self.end = tp.nullGroup([self.sl.jointB], Vtype=3, 
                           Vname='end' + self.suffix)[0]
                           
        self.end_chestA = tp.nullGroup([self.sl.jointB], Vtype=1, 
                                       Vname='end_chestA' + self.suffix)[0]
        self.sl.chestA|self.end_chestA
        
        self.ctrl()
        self.stretchyIk()
        self.interpolate(numInterpolateHip, 
                         numInterpolateChest, 
                         numInterpolateShoulder)
        if drvKeys:
            self.drivenKeyAttr()
        
          
              
            
        self.cleanup()
    
    def ctrl(self):
        nullChestA = tp.nullGroup([self.sl.chestA], Vtype=1, Vhierarchy=3,
                                Vname='null_chestA' + self.suffix)[0]
                                
        nullChestB = tp.nullGroup([self.sl.chestB], Vtype=1, Vhierarchy=3,
                                Vname='null_chestB' + self.suffix)[0]
                                
        nullHip = tp.nullGroup([self.sl.hip], Vtype=1, Vhierarchy=3,
                                Vname='null_hip' + self.suffix)[0]
                                
        self.gT = tp.nullGroup([self.sl.body], Vtype=1, Vhierarchy=3,
                                Vname='trans' + self.suffix)[0]
        
        
                                
        self.sl.chestB|nullChestA
        self.sl.body|nullChestB
        self.sl.body|nullHip
        
        prx = (pg.proxyGeo([self.sl.chestA], VcenterPivot=1, chain=0,
                           side=self.side, label='chest'))
        
        if self.midCtrl:
            nullMid = tp.nullGroup([self.sl.mid], Vtype=1, Vhierarchy=3,
                                Vname='null_mid' + self.suffix)[0]
            
            self.sl.chestB|nullMid
            self.sl.mid|nullChestA
    
        
    def stretchyIk(self):
        """
        Stretchy IK with distance tool.
        """
        handle, effector = pm.ikHandle(startJoint=self.sl.jointA, 
                                       endEffector=self.sl.jointB, 
                                       solver='ikRPsolver',
                                       name='handle' + self.suffix)
        
        pm.rename(effector, 'effector' + self.suffix)
        self.end|handle
        
        self.dist = td.dist([self.start, self.end])
        self.dist.calibrate(1)
        self.dist.connect([self.sl.jointA.sx])
        
        ctrlPolv = tp.nullGroup([self.sl.jointB],
                                     Vtype=3, 
                                     Vname='pv' + self.suffix)[0]
                                     
        pm.poleVectorConstraint(ctrlPolv, handle) 
        #self.sl.chestA|ctrlPolv
        
        pvFix = tp.nullGroup([ctrlPolv], 
                             Vtype=3,
                             Vhierarchy=1,
                             Vname='pvFix' + self.suffix)[0]
                             
        pvMov = tp.nullGroup([ctrlPolv], 
                             Vtype=3,
                             Vhierarchy=1,
                             Vname='pvMov' + self.suffix)[0]
                             
        nullPvMov = tp.nullGroup([pvMov], 
                             Vtype=3,
                             Vhierarchy=3,
                             Vname='null_pvMov' + self.suffix)[0]
                             
        tc.spaceSwitch(headlineAttr=1,
                       Vlist=[pvFix, pvMov, ctrlPolv], 
                       VattrName=['pv', 'limb_root'], 
                       Vtype=1, Voffset=1)
        
        ctrlPolv.pv.set(0.5)
        ctrlPolv.limb_root.set(0.5)
        
        self.sl.hip|nullPvMov
        self.end_chestA|ctrlPolv
        self.end_chestA|pvFix
        pvMov.ty.set(-20)
        pvFix.ty.set(-20)
        
        hipTrans = pm.group(w=1, em=1, n='hipTrans' + self.suffix)
        hipTrans|self.start
        self.sl.hip|hipTrans
        self.dist.parent([self.sl.body])
        
        self.sl.chestA|self.end 
        self.start|self.sl.jointA
    
    
    def interpolate(self, numInterpolateHip=3, numInterpolateChest=3, 
                    numInterpolateShoulder=3):
        """
        Interpolation.
        """
        self.xrotB = tp.nullGroup([self.sl.jointB], Vtype=2,
                                  Vhierarchy=4, 
                                  Vname='xRotB' + self.suffix,
                                  connectInverseScale=1)[0]
                                  
        tc.cons(Vlist=[self.end_chestA, self.xrotB], Vtype=2, #
                skipRotate=['none', 'y', 'z'])
        
        self.inbAB = tp.nullGroup([self.sl.jointA], Vtype=2,
                                  Vhierarchy=4, 
                                  Vname='inbAB' + self.suffix,
                                  connectInverseScale=1)[0]
        
        tc.cons(Vlist=[self.sl.jointA, self.inbAB], Vtype=1, 
                skipTranslate=['none', 'y', 'z'],
                weight=0.4)
        tc.cons(Vlist=[self.sl.jointB, self.inbAB], Vtype=1, 
                skipTranslate=['none', 'y', 'z'],
                weight=0.6)
        
        
        self.intIKCyl = tp.nullGroup([self.inbAB], Vtype=2, 
                                     Vhierarchy=4,    
                                     Vname='intIKCyl' + self.suffix,
                                     connectInverseScale=1)[0] 
        
        self.intIK = tp.nullGroup([self.inbAB], Vtype=2, 
                                  Vhierarchy=4,
                                  Vname='intIK' + self.suffix,
                                  connectInverseScale=1)[0]

        snap.snap([self.intIK, self.sl.jointA])
        
        
        self.intFix = tp.nullGroup([self.sl.jointA], Vtype=2, 
                                    Vname='intFix' + self.suffix,
                                    connectInverseScale=1)[0]
        
        inb = []
        for i in range(numInterpolateHip):
            new = tp.nullGroup([self.intIK], Vtype=2, 
                                Vname='inb' + self.suffix + str(i))[0]
                                
            self.sl.jointB|new
            inb.append(new)
         
        tc.distribute(inb + [self.intIK, self.intFix], Vtype=3)
        pg.proxyGeo(inb + [self.intIK, self.intIKCyl], VcenterPivot=1, chain=0,
                    side=self.side, label='cyl')
        
        #----------------------------------------------------------------------
        self.intchest = tp.nullGroup([self.xrotB], Vtype=2, 
                                     Vhierarchy=4,    
                                     Vname='intChest' + self.suffix,
                                     connectInverseScale=1)[0]
        
        inbCyl = []
        for i in range(numInterpolateChest):
            new = tp.nullGroup([self.intIKCyl], Vtype=2, 
                                Vname='inbCyl' + self.suffix + str(i))[0]
                                
            self.sl.jointB|new
            inbCyl.append(new)
        
        # shoulder connection and in between-----------------------------------
        self.connShoulder = tp.nullGroup([inbCyl[-1]], 
                                    Vtype=2, 
                                    Vhierarchy=4,    
                                    Vname ='connShoulder' + self.suffix)[0]
        
        inbChest = []
        for i in range(numInterpolateShoulder):
            
                inbCh = tp.nullGroup([self.end_chestA], Vtype=2, Vhierarchy=4, 
                                Vname='inbChest' + self.suffix + str(i))[0]
                inbChest.append(inbCh)
        
        tc.distribute(inbChest + [self.end_chestA, inbCyl[-1]], Vtype=3)                        
        #----------------------------------------------------------------------                        
                                 
        tc.distribute(inbCyl + [self.intIKCyl, self.intchest], Vtype=3)
        pg.proxyGeo(inbCyl + inbChest, VcenterPivot=1, chain=0, 
                    side=self.side, label='sph')
        
        
        self.sl.hip|self.intFix
        pg.proxyGeo([self.intFix], VcenterPivot=1, chain=0,
                    side=self.side, label='fix')
    
    def drivenKeyAttr(self):
        tk.drivenKeyBs(Vlist=[self.sl.jointA])
        
    def cleanup(self):
        """
        Lock and hide attributes.
        """
        for o in [self.sl.hip, self.sl.chestA, self.sl.chestB]:
            
            
            o.sx.set(lock=1, keyable=0, channelBox=0)
            o.sy.set(lock=1, keyable=0, channelBox=0)
            o.sz.set(lock=1, keyable=0, channelBox=0)
            o.v.set(lock=1, keyable=0, channelBox=0)

        
        self.sl.body.v.set(lock=1, keyable=0, channelBox=0)
        
    
class head():
    """
    Head and jaw setup.
    TODO: upper and lower squash object.
    Different pivots as jaw.
    """
    def __init__(self, Vlist=[], Vsuffix=''):
        self.sl = lsSl.sl(Vlist, 5, 5,
                  slDict={
                          'all':'0',
                          'jawLowA':'1',
                          'jawLowB':'2',
                          'jawUp':'3',
                          'jawUpB':'4',
                          'squashUp':'5',
                          
                          },
                          Vrename=1, Vsuffix=Vsuffix)
        
        mf.builder.nodes.set([self.sl.all, 
                              self.sl.jawLowA,
                              self.sl.jawLowB,
                              self.sl.jawUp,
                              self.sl.jawUpB], 
                             'transform_ctrl')
        
        self.suffix = Vsuffix
        
        self.sl.all|self.sl.jawLowA|self.sl.jawLowB
        self.sl.all|self.sl.jawUp|self.sl.jawUpB
        
        self.nullCtrls = tp.nullGroup(self.sl.Vlist[:-1], 
                                Vtype=1, 
                                Vhierarchy=3,    
                                Vprefix ='null_',
                                connectInverseScale=1)
        
        jawAJnt = tp.nullGroup([self.sl.jawLowA], 
                                Vtype=2, 
                                Vhierarchy=5,    
                                Vprefix ='jnt_',
                                connectInverseScale=1)[0]
        
        jawBJnt = tp.nullGroup([self.sl.jawLowB], 
                                Vtype=2, 
                                Vhierarchy=5,    
                                Vprefix ='jnt_',
                                connectInverseScale=1)[0]
        
        jawUJnt = tp.nullGroup([self.sl.jawUp], 
                                Vtype=2, 
                                Vhierarchy=5,    
                                Vprefix ='jnt_',
                                connectInverseScale=1)[0]
                                
        jawBUJnt = tp.nullGroup([self.sl.jawUpB], 
                                Vtype=2, 
                                Vhierarchy=5,    
                                Vprefix ='jnt_',
                                connectInverseScale=1)[0]
        
        tj.jointInbCons(Vlist=[jawAJnt, jawBJnt], Vproxy=1, proxyScale=1, 
                        side=0, label='inbLow', type=3)
        
        tj.jointInbCons(Vlist=[jawUJnt, jawBUJnt], Vproxy=1, proxyScale=1, 
                        side=0, label='inbUp', type=3)
        
        
        headJnt = tp.nullGroup([self.sl.all], 
                                Vtype=2, 
                                Vhierarchy=5,    
                                Vprefix ='jnt_',
                                connectInverseScale=1)[0]
        
        
        self.gT = tp.trans(em=1, w=1, n='trans' + Vsuffix).group
        self.gMT = tp.mainTrans(em=1, w=1, n='main_trans' + Vsuffix).group
        
        self.gWT = pm.group(em=1, w=1, n='world_trans' + Vsuffix)
        self.gWT.setParent(self.gMT)
        
        self.gT|self.nullCtrls[0]
        
        zero = tc.zeroOutConstraint([self.gWT, self.gT,
                                     self.sl.all],
                                    VlastObj=1)
        tc.spaceSwitch(headlineAttr=1,
                       Vlist=[zero[0], zero[1], self.sl.all.getParent(), zero[2]], 
                       VattrName=['world', 'root'], 
                       Vtype=2, VlastObj=1, Voffset=1)
        
        
        self.px = pg.proxyGeo(Vlist=self.sl.Vlist[:-1], Vsuffix='_px', xScale=1, 
                                  yzScale=0, yzScaleGlobal=1, 
                                  Vsides=4, chain=0, VcenterPivot=1,
                                  side=0, label='head')
        
        self.squash()
        
    def squash(self):
        
        squashLow = tp.nullGroup([self.sl.squashUp], 
                                 Vtype=2, 
                                 Vhierarchy=5,    
                                 Vname ='squashLow' + self.suffix,
                                 connectInverseScale=1)[0]
        
        nullUp, nullLow = tp.nullGroup([self.sl.squashUp, squashLow], 
                                  Vtype=1, 
                                  Vhierarchy=3,    
                                  Vprefix ='null_',
                                  connectInverseScale=1)
        
        self.sl.all|nullUp
        self.sl.squashUp|self.nullCtrls[4]    
        self.sl.all|nullLow
        squashLow|self.nullCtrls[2] 
        
        self.sl.all.addAttr('up_scale_x', defaultValue=1, min=0.1, keyable=1)
        self.sl.all.addAttr('up_scale_y', defaultValue=1, min=0.1, keyable=1)
        self.sl.all.addAttr('up_scale_z', defaultValue=1, min=0.1, keyable=1)
        
        self.sl.all.addAttr('low_scale_x', defaultValue=1, min=0.1, keyable=1)
        self.sl.all.addAttr('low_scale_y', defaultValue=1, min=0.1, keyable=1)
        self.sl.all.addAttr('low_scale_z', defaultValue=1, min=0.1, keyable=1)
        
        self.sl.all.up_scale_x >> self.sl.squashUp.sx  
        self.sl.all.up_scale_y >> self.sl.squashUp.sy
        self.sl.all.up_scale_z >> self.sl.squashUp.sz
        
        self.sl.all.low_scale_x >> squashLow.sx
        self.sl.all.low_scale_y >> squashLow.sy
        self.sl.all.low_scale_z >> squashLow.sz
        
class neck():
    """
    Head and jaw setup.
    TODO: upper and lower squash object.
    Different pivots as jaw.
    """
    def __init__(self, Vlist=[], Vsuffix=''):
        self.sl = lsSl.sl(Vlist, 5, 5,
                  slDict={
                          'A':'0',
                          'B':'1',
                          'headPar':'2',
                          'ctrlA':'3',
                          'ctrlB':'4',
                          'ctrlC':'5',
                          },
                          Vrename=1, Vsuffix=Vsuffix)
        
        mf.builder.nodes.set([self.sl.A, 
                              self.sl.B], 
                             'transform_ctrl')
        
        #self.sl.A|self.sl.B
        newPar = tc.freeTransIk([self.sl.B], Vsuffix=Vsuffix)
        newPar|self.sl.headPar
        
        null = tp.nullGroup([self.sl.A, self.sl.B, self.sl.headPar], 
                    Vtype=1, 
                    Vhierarchy=3,    
                    Vprefix ='null_',
                    connectInverseScale=1)
        
        tj.jointInbCons(Vlist=[self.sl.A, self.sl.B], Vsuffix=Vsuffix,
                        Vproxy=1, proxyScale=1, type=2)
        
        tp.parentShape([self.sl.ctrlA, self.sl.A])
        tp.parentShape([self.sl.ctrlB, self.sl.B])
        #tp.parentShape([self.sl.ctrlC, self.sl.headPar])
        
        self.px = pg.proxyGeo(Vlist=[self.sl.A, 
                                     self.sl.B, 
                                     self.sl.headPar], 
                              Vsuffix='_px', xScale=1, 
                              yzScale=0, yzScaleGlobal=0.1, 
                              Vsides=4, chain=1, VcenterPivot=0,
                              side=0, label='head')
        
        self.px = pg.proxyGeo(Vlist=[self.sl.headPar], 
                              Vsuffix='_px', xScale=1, 
                              yzScale=0, yzScaleGlobal=0.1, 
                              Vsides=4, chain=0, VcenterPivot=1,
                              side=0, label='head')
        self.sl.A|self.sl.ctrlA
        self.sl.B|self.sl.ctrlB
        self.sl.headPar|self.sl.ctrlC
        
        self.gT = tp.trans(em=1, w=1, n='trans' + Vsuffix).group
        self.gT|null[0]
        
        
        
        
        
        
        
        
        
        
        
        
