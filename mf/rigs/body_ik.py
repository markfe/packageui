"""
body setup ik/fk.  
"""
import pymel.core as pm
import mf.builder.nodes as bn
from mf.tools import lsSl 
from mf.tools import parent as tp
from mf.tools import proxyGeo as pg
from mf.tools import general as tg
from mf.tools import distance as td
from mf.tools import cons as tc


class spineRibbon():
    """
    Ribbon spine with 8 bindjoints.
 
    TODO: IK anf FK setup.
    mid ctrl rotation for IK. 
    """
    def __init__(self, Vlist=[], Vsuffix=''):
        self.sl = lsSl.sl(Vlist, 3, 3,
                  slDict={
                          'start':'0',
                          'mid':'1',
                          'end':'2'
                          },
                          Vrename=1, Vsuffix=Vsuffix)
        
        bn.set([self.sl.start, 
                self.sl.mid, 
                self.sl.end], 
                'transform_ctrl')
        
        dist = td.dist([self.sl.start, self.sl.end])
        
        nurbs, nurbsPlane = pm.nurbsPlane(ax=[0, 1, 0], w=dist.distance()*1.12, 
                                          lr=0.1, d=1, u=9,
                                          v=1, ch=1, n='nurbs' +Vsuffix)
        
        cube, polyCube = pm.polyCube (w=dist.distance(), 
                                         h=1, sx=8, sy=1, 
                                         ax=[0, 1, 0], cuv=2, ch=1,
                                         n='cube'+Vsuffix)
        cube.sz.set(0.5)
        
        rot = self.sl.mid.r.get()
        dist.align([cube, nurbs, self.sl.mid], 50)
        self.sl.mid.r.set(rot)
           
        pos = [[0.0556,0.5],
               [0.1667,0.5],
               [0.2778,0.5],
               [0.3889,0.5],
        
               [0.6111,0.5],
               [0.7222,0.5],
               [0.8333,0.5],
               [0.9444,0.5]]
        
        null_ctrl = tp.nullGroup([self.sl.start, self.sl.mid, self.sl.end], 
                                 Vhierarchy=3, Vprefix='null_')
        follicles = []
        for p in pos:
            folShape = pm.createNode( 'follicle' )
            
            nurbs.local >> folShape.inputSurface
            nurbs.worldMatrix >> folShape.inputWorldMatrix
            folShape.outTranslate >> folShape.getParent().translate
            folShape.outRotate >> folShape.getParent().rotate
            
            folShape.parameterU.set(p[0])
            folShape.parameterV.set(p[1])
            
            follicles.append( folShape.getParent() )
        
        bindRiv = []
        for each in follicles:
            cubeBnd = pg.proxyGeo([each], VcenterPivot=1, chain=0, 
                        xScale=2, yzScaleGlobal=dist.distance()*0.04)
            
            bindRiv.append(cubeBnd.pxNodeLs[0].bj)
            
        planeBnd = pg.proxyGeo([self.sl.start, self.sl.mid, self.sl.end], 
                    xScale=2, VcenterPivot=1, chain=0, 
                    yzScaleGlobal=dist.distance()*0.04)
        
        print planeBnd.pxNodeLs
        bind = []
        for each in planeBnd.pxNodeLs:
            bind.append( each.bj)
        
        # skinCluster returns list in maya 2009    
        skin = pm.skinCluster(bind, nurbs, 
                                   dr=3, toSelectedBones=True, 
                                   ignoreHierarchy=True, mi=2 )
                            
                                   
        refSkin = pm.skinCluster(bind + bindRiv, cube, 
                                   dr=1, toSelectedBones=True, 
                                   ignoreHierarchy=True, mi=1 )[0]
                                   
        weight = [[1, 0, 0], 
                  [.9, .1, 0],
                  [.5, .5, 0],
                  [.2, .8, 0],
                  [0, 1, 0],
                  [0, 1, 0],
                  [0, .8, .2],
                  [0, .5, .5],
                  [0, .1, .9],
                  [0, 0, 1]]
        
        for i in range(len(weight)):
            print weight[i]
            pm.skinPercent(skin, nurbs.cv[i][0:1], 
                         transformValue=[(bind[0], weight[i][0]), 
                                         (bind[1], weight[i][1]), 
                                         (bind[2], weight[i][2])
                                         ])
          
        self.gT = tp.trans(em=1, w=1, n='trans' + Vsuffix).group
        self.gMT = tp.mainTrans(em=1, w=1, n='main_trans' + Vsuffix).group
        self.gNT = tp.noTrans(em=1, w=1, n='no_trans' + Vsuffix).group
            
        self.gNT|cube
        self.gNT|nurbs
        
        self.gST = pm.group(em=1, w=1, n='scale_trans' + Vsuffix)
        self.gSNT = pm.group(em=1, w=1, n='scale_no_trans' + Vsuffix)
        
        pm.scaleConstraint(self.gST, self.gSNT, skip=['none', 'none', 'none'])
        self.gSNT.setParent(self.gNT)
        self.gST.setParent(self.gMT)
        
        for each in follicles:
            self.gNT|each
            self.gSNT.sx >> each.sx
            self.gSNT.sx >> each.sy 
            self.gSNT.sx >> each.sz  
            
        for each in null_ctrl:
            self.gT|each
        
        dist.delete()
        tg.reorderDeforms([cube])
        
    def midIK(self):
        """
        MId ctrl orientation.
        """
        pass
    

class spine():
    """
    Spine setup for skinny characters.
    TODO: mid_ik translation does not effect end_ik
    """
    def __init__(self, Vlist=[], Vsuffix=''):
        self.sl = lsSl.sl(Vlist, 7, 7,
              slDict={'start':'0',
                      'mid_ik':'1',
                      'end_ik':'2',
                      'hip_ik':'3',
                      
                      'mid_fk':'4',
                      'end_fk':'5',
                      'hip_fk':'6',},
                      Vrename=1, Vsuffix=Vsuffix)
        
        self.suffix = Vsuffix
        bn.set([self.sl.start, 
                self.sl.mid_ik, self.sl.end_ik, self.sl.hip_ik,
                self.sl.mid_fk, self.sl.end_fk, self.sl.hip_fk], 
                'transform_ctrl') 
        
        self.ik()
        self.fk() 
        self.ik_fk()   
        self.cleanup()
        self.fk_mid()
        
    def ik_fk(self):
        """
        IK/FK constraints and visibility.
        """
        mid_drv = tp.nullGroup([self.sl.mid_fk], Vprefix='drv_', Vtype=3)[0]
        end_drv = tp.nullGroup([self.sl.end_fk], Vprefix='drv_', Vtype=3)[0]
        hip_drv = tp.nullGroup([self.sl.hip_fk], Vprefix='drv_', Vtype=3)[0]
        
        self.sl.start.addAttr('ik_fk', defaultValue=1, min=0, max=1, 
                        keyable=1, at='double')    
        
        tc.constraintIkFk(VikList=[self.sl.mid_ik, 
                                   self.sl.end_ik,
                                   self.sl.hip_ik], 
                          VfkList=[self.sl.mid_fk, 
                                   self.sl.end_fk,
                                   self.sl.hip_fk], 
                          VdrvList=[mid_drv,
                                    end_drv,
                                    hip_drv], 
                          Vattr=self.sl.start.ik_fk, 
                          Vsuffix='_ikFk', 
                          Voffset=1, 
                          Vctrl=self.sl.start, 
                          blendScale=1,
                          useConstraints=1,
                          VskippLast=0)
        
        invIk_fk = tg.invAttr(self.sl.start.ik_fk, self.suffix)
        invIk_fk.connect([self.sl.mid_ik.getShape().v,
                          self.sl.end_ik.getShape().v,
                          self.sl.hip_ik.getShape().v])
        
        self.sl.start.ik_fk >> self.sl.mid_fk.getShape().v
        self.sl.start.ik_fk >> self.sl.hip_fk.getShape().v
        self.sl.start.ik_fk >> self.sl.end_fk.getShape().v
        
        invIk_fk.setRange.maxX.set(0.1)
    
    def fk(self):
        """
        Fk ctrls setup.
        """
        self.sl.start|self.sl.mid_fk|self.sl.end_fk
        self.sl.start|self.sl.hip_fk
        
        newPar = tc.freeTransIk([self.sl.mid_fk], Vsuffix='_free')
        newPar|self.sl.end_fk
        
        tp.nullGroup([self.sl.start, 
                      self.sl.mid_fk, 
                      self.sl.end_fk,
                      self.sl.hip_fk], 
                     Vhierarchy=3, Vprefix='null_')
        
    def fk_mid(self):
        """
        Interpolation for the Fk middle ctrl.
        """
        mid_point = tp.nullGroup([self.sl.mid_fk], 
                                 Vname='mid_point'+self.suffix, Vtype=3)[0]
                                 
        mid_trans = tp.nullGroup([self.sl.mid_fk], 
                               Vname='mid_rot'+self.suffix, Vtype=3)[0]
                               
        mid_aim = tp.nullGroup([self.sl.mid_fk], 
                               Vname='mid_aim'+self.suffix, Vtype=3)[0]
        mid_aim_low = tp.nullGroup([self.sl.mid_fk], 
                               Vname='mid_aim'+self.suffix, Vtype=3)[0]
        mid_aim_inb = tp.nullGroup([self.sl.mid_fk], 
                               Vname='mid_aim'+self.suffix, Vtype=3)[0]
          
                               
        pv_point = tp.nullGroup([self.sl.mid_fk], 
                                Vname='pv_point'+self.suffix, Vtype=3)[0]
        pv_orient = tp.nullGroup([self.sl.mid_fk], 
                                 Vname='pv_orient'+self.suffix, Vtype=3)[0]
        pv_loc = tp.nullGroup([self.sl.mid_fk], 
                              Vname='pv_loc'+self.suffix, Vtype=3)[0]               
        
        self.sl.mid_fk|mid_point|mid_trans|mid_aim|mid_aim_inb
        mid_trans|mid_aim_low
        self.sl.mid_fk|pv_point|pv_orient|pv_loc
        pv_loc.tz.set(-3)
        
        tc.cons([self.sl.hip_fk, self.sl.end_fk, pv_point], Vtype=1, Voffset=1)
        
        tc.cons([self.sl.mid_fk, mid_point, mid_trans], Vtype=1, Voffset=1)
        
        tc.cons([self.sl.hip_fk, self.sl.end_fk, pv_orient], Vtype=2, Voffset=1,
                skipRotate=['x','none','z'])
    
        tc.cons([self.sl.hip_fk, self.sl.end_fk, mid_point], Vtype=1, Voffset=1)
        aimU = pm.aimConstraint([self.sl.end_fk, mid_aim], mo=0)
        aimL = pm.aimConstraint([self.sl.hip_fk, mid_aim_low], mo=0)
        tc.cons([mid_aim, mid_aim_low, mid_aim_inb], Vtype=2, Voffset=0)
        #aimC
        
        px = pg.proxyGeo([mid_aim_inb], VcenterPivot=1, chain=0, 
                         shader=1, Vcolor=[0.6, 0.3, 0.3], xScale=0.1, 
                         shaderName='shader', yzScaleGlobal=1)
         
    def ik(self):
        """
        Ik ctrls setup.
        """
        self.sl.start|self.sl.mid_ik
        self.sl.start|self.sl.end_ik
        self.sl.start|self.sl.hip_ik
        
        tp.nullGroup([self.sl.start, 
                      self.sl.mid_ik, 
                      self.sl.end_ik,
                      self.sl.hip_ik], 
                     Vhierarchy=3,Vprefix='null_')
        
    def ik_mid(self):
        """
        Inerpolation for the Ik middle ctl.
        """
        
        pass
    
    def cleanup(self):
        """
        """
        
        pass

     