"""
Divide a rig in a ctl and a rig hierarchie.
This is not finished!
"""
import maya.cmds as cm
import pymel.core as pm
import mf.builder.nodes as bn
from mf.tools import parent as tp
from mf.tools import lsSl

def split(Vlist=[]):
    allCtls = bn.getAll('transform_ctrl')
    allExtra = bn.getAll('transform_ctrl_extra')
    allCtls = allCtls + allExtra
    allNull = []
    for e in allCtls:
        print e.ctrl   
        children =  e.ctrl.listRelatives(c=1, s=0)
        print children
        cctls = []
        creg = []
        for c in children:
            if c.hasAttr('transform_ctrl') or c.hasAttr('transform_ctrl_extra'):
                cctls.append(c)
            else:
                creg.append(c)
            
        if len(creg) > 0:
            null = tp.nullGroup(Vlist=[e.ctrl ], Vhierarchy=4, Vtype=1, 
                                Vname='DRV_' + str(e.ctrl ))[0]
            allNull.append(null)
            null.addAttr(bn.register('transform_drv')[0])
            
            e.ctrl.addAttr('match', at='message')
            null.addAttr('match', at='message')
            e.ctrl.match >> null.match
                   
            for r in creg:
                if r.nodeType() == 'transform' or r.nodeType() == 'joint':
                    null|r
                    
                    
    for e in allCtls:
        ctlPar([e.ctrl])
        
    for i in range(len(allCtls)):
        pm.parentConstraint(allCtls[i].ctrl, allNull[i])  
        pm.scaleConstraint(allCtls[i].ctrl, allNull[i])   

                
def ctlPar(Vlist=[]):
    """
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    
    par = None
    parent = Vlist[0].getParent()
    if parent.hasAttr('transform_ctrl') or parent.hasAttr('transform_ctrl_extra'):
        # parent is already the right one
        par = None
        Vlist[0].getParent().match.connections(plugs=0)[0]|Vlist[0].match.connections(plugs=0)[0]
        return par
    
    if not par:
        parents = cm.ls(str(Vlist[0]), long=1)[0].split('|')[1:]
        if len(parents) == 1:
            return par
        
        for p in range(len(parents)-1):
            parent = pm.ls('|'.join(parents[:-p-1]))[0]
           
            if parent:
                if parent.hasAttr('transform_ctrl') or parent.hasAttr('transform_ctrl_extra'):
                    par = parent
                    break
    if par:
        hasPar = Vlist[0].getParent()
        if hasPar:
            try:
                
                par|hasPar
            except:
                pass
            par.match.connections(plugs=0)[0]|Vlist[0].match.connections(plugs=0)[0]
    return par
                
        
        
        
        
    

