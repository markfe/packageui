import pymel.core as pm
from pymel.core import PyNode as PN

from mf.tools import lsSl
import mf.tools.snap as sp

def findNames(Vlist=[]):
    limbs = []
    for e in Vlist:
        namespace = e.getNamespace()
        print namespace


def switch(Vlist=[]):
    """
    Matches ik and fk positions.
    Select any controller of an arm or leg and run script.
    
    TODO: Make it work together with the foot roll on.
    TODO: Make it work for the auto hip.
    """
    
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    namespace = ':'.join(str(Vlist[0]).split(':')[:-1])

    ns = ''
    if not namespace == '':
        ns = namespace + ':' 
    
    split = str(Vlist[0]).split('_')
    i=1
    suffix =''
    while 1:
        suffix =  '_' + split[-i] + suffix
        try:
            attrObj = PN(ns + "jnt_fk2" + suffix)
            break
        except:
            pass
        i+=1    
        
    try: 
        attrObj = PN(ns + "attrObj_end_rt" + suffix)
    except:
        attrObj = PN(ns + "attrObj" + suffix)
        
    ctrlA = PN(ns + "ctrlA" + suffix)
    jnt_fk2 = PN(ns + "jnt_fk2" + suffix)
    jnt_fk3 = PN(ns + "jnt_fk3" + suffix)
    
    jnt_ik1 = PN(ns + "jnt_ik1" + suffix)
    jnt_ik2 = PN(ns + "jnt_ik2" + suffix)
    
    fk1_snap = PN(ns + 'fk1_snap' + suffix)
    fk3_snap = PN(ns + 'fk3_snap' + suffix)
    ik1_snap = PN(ns + 'ik1_snap' + suffix)
    ik3_snap = PN(ns + 'ik3_snap' + suffix)
    
    try:
        ctrlB = PN(ns + "ctrlB" + suffix)
    except:
        ctrlB = PN(ns + "foot" + suffix)
        
    polv = PN(ns + "polv" + suffix)
    line = PN(ns + "line" + suffix)
    state = attrObj.ik_fk.get()

    if state:
        attrObj.ik_fk.set(0)
        sp.snap([ctrlB, fk3_snap])
        
        try:
            twist = ctrlB.twist.get()
            ctrlB.twist.set(0)
            crvInf = pm.createNode('curveInfo')
            line.getShape().worldSpace[0] >> crvInf.inputCurve
            curveLen = crvInf.arcLength.get()
            pm.delete(crvInf) 
            if curveLen == 0: 
                curveLen = 0.001
            ctrlB.twist.set(twist)
            
            # get orientation
            locOA = pm.spaceLocator()
            locOB = pm.spaceLocator()
            jnt_ik1|locOA|locOB
            sp.snap([locOA, jnt_ik1])
            sp.snap([locOB, jnt_ik1])
            sp.snapTrans([locOB, jnt_ik2])
            if locOB.tx.get() < 0:
                curveLen = -curveLen
                
            locA = pm.spaceLocator()
            locB = pm.spaceLocator()
            sp.snap([locA, locB, jnt_fk2])
            #jnt_fk1|locA|locB
            fk1_snap|locA|locB
            
            locA.rz.set(locA.rz.get()/2)
            locB.ty.set(-curveLen)
            sp.snapTrans([polv, locB])
            pm.delete(locOA)
            pm.delete(locA)
            
            # calculate twist
            locA = pm.spaceLocator()
            locB = pm.spaceLocator()
            sp.snap([locA, jnt_ik2])
            sp.snap([locB, polv])
    
            jnt_ik1|locA|locB
            ctrlB.twist.set(0)
        
            sp.snapTrans([polv, locB])
            ctrlB.twist.set(twist)
            pm.delete(locA)
        
        except:
            sp.snapTrans([polv, jnt_fk2])
             
        pm.select(ctrlB)
        
    else:
        attrObj.ik_fk.set(1)
        sp.snapRot([ctrlA, ik1_snap])
        ctrlA.sx.set(jnt_ik1.sx.get())
        jnt_fk2.rz.set(jnt_ik2.rz.get())
        try:
            jnt_fk2.rx.set(0)
        except:
            pass
        try:
            jnt_fk2.ry.set(0)
        except:
            pass
        jnt_fk2.sx.set(jnt_ik2.sx.get())
        sp.snapRot([jnt_fk3, ik3_snap])
        
        pm.select(jnt_fk3)
        