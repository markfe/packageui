from __future__ import with_statement

import os
import pymel.core as pm
import maya.cmds as cm
import mf.tools.lsSl as lsSl
import mf.tools.snap as snap
import mf.tools.curve as tc
import mf.tools.parent as tp
import mf.tools.distance as td
import mf.builder.nodes as bn
import mf.builder.main as bm


class UI():
    """    
    **UI for setting up mirroring on a rig.**
       
    There are three steps to quickly set up a rig for mirroring:
    
        1. Select all controllers and hit the ``Make selected a ctrl`` button.
        2. Hit the ``Setup All`` button.
        3. Use the ``Find selected`` Button to tweak single controllers.
    
    If you want to test mirroring, use this command::
    
        import mf.rigs.tools.mirror
        mf.rigs.tools.mirror.mirror_pose()
    
    ===================== ======================================================
    Options               Description
    ===================== ======================================================   
    Center search dist    All ctrls inside this range will be set as ``Center``.
                          Set this to a higher value if your center ctrls are 
                          not exactly in the middle.
    
    Partner search dist   Search range for the partner controller.
    
    Partner match range   If more than one controller is found as a partner,
                          all ctrls that are closer than this value will be
                          matched by name.
    ===================== ======================================================     
    """ 
    def __init__(self, showAdditionalOptions=0):
        relPath = bm.getRelativePath()
                                    
        UIname = 'mirror_setup'
        print UIname
        try:
            pm.deleteUI(UIname)
        except: 
            pass
        pm.window(UIname, t=UIname, h=10, w=100, s=1)
        with pm.menuBarLayout():
            with pm.menu('Help',  tearOff=True):
                pm.menuItem(label='Open help',
                            c="import pymel.core as pm; pm.showHelp('" +
                                        os.path.split(os.path.split(relPath)[0])[0].replace("\\", "/")+
                                        "\\docs\\html\\mirrorPose.html', a=1)")

        with pm.paneLayout('scene', configuration="vertical2", paneSize=(2, 60, 40)) as self.paneVrt:
            with pm.formLayout() as self.mainForm:
                self.scroll = pm.textScrollList(sc=self.TSL_selection_CB, ams=1)
            with pm.formLayout() as self.formR:    
                with pm.columnLayout(adjustableColumn=1):
                    pm.text('List controller', h=20)
                    with pm.rowLayout(numberOfColumns=5, adjustableColumn=1):
                        pm.button(l='Refresh list', c=self.populate)
                        pm.button(l='Guess', ann='Select all curves that could be controllers', c=findControls)
                        #pm.button(l='Help', c="import pymel.core as pm; pm.showHelp('" +
                        #                os.path.split(os.path.split(relPath)[0])[0].replace("\\", "/")+
                        #                "\\docs\\html\\mirrorPose.html', a=1)")
                        
                    pm.button(l='Make selected a ctrl', c=self.makeCtrl_CB)
                    pm.button(l='Make selected NOT a ctrl', c=self.unCtrl_CB)
                
                pm.separator(h=10)
                    
                with pm.columnLayout(adjustableColumn=1):
                    pm.text('Automatic setup', h=20)
                    
                    self.centerFF = pm.floatSliderGrp( label='Center search dist:', field=True, 
                                                    minValue=0.0001, maxValue=0.002, 
                                                    fieldMinValue=0.000000001, fieldMaxValue=1000.0, 
                                                    value=0.001, pre=8,
                                                    ann='Search range for the partner controller.') 
                    
                    self.partnerFF = pm.floatSliderGrp( label='Partner search dist:', field=True, 
                                                        minValue=0.001, maxValue=0.02, 
                                                        fieldMinValue=0.000000001, fieldMaxValue=1000.0, 
                                                        value=0.01, pre=8, cc=self.partnerSliderDrag, 
                                                        ann='Search range for the partner controller.') 
                    
                    self.difFF = pm.floatSliderGrp( label='Partner match range:', field=True, 
                                                    minValue=0.0001, maxValue=0.002, 
                                                    fieldMinValue=0.000000001, fieldMaxValue=1000.0, 
                                                    value=0.001, pre=8,
                                                    ann='''If more than one controller is found as a partner, all ctrls that are closer than this value will be matched by name.''') 
                    
                    pm.floatSliderGrp(self.difFF, q=1, v=1)
                    pm.button(l='Setup all', c=self.setupAll_CB)
                    pm.button(l='Setup selected', c=self.setupSel_CB)
                    
                    pm.separator(h=5, style='in')
                    pm.text('Adjust selected:', h=20)
                    with pm.rowLayout(numberOfColumns=5, adjustableColumn=4):
                        pm.button(l='Side', c=self.setupSide_CB, w=60)
                        pm.button(l='Partner', c=self.setupPartner_CB, w=60)
                        pm.button(l='Rule', c=self.setupRule_CB, w=60)
                        pm.text('', w=10)
                        pm.button(l='Exclude', c=self.exclude_CB, w=60)
                
                pm.separator(h=10)
                    
                with pm.columnLayout(adjustableColumn=1):
                    with pm.rowLayout(numberOfColumns=2, adjustableColumn=2):
                        pm.text('Ctrl:', w=44, al='left')
                        self.currentCtrl = pm.text(label='', h=20, al='left')
                    
                    with pm.optionMenu( label='Side:     ', w=100, 
                                        changeCommand=self.optionMenue_CB) as self.optionM:
                            pm.menuItem( label='Center')
                            pm.menuItem( label='Left')
                            pm.menuItem( label='Right')
                            pm.menuItem( label='None')
                    
                    with pm.rowLayout(numberOfColumns=3, adjustableColumn=2):
                        pm.text('Partner:', w=44, al='left')
                        self.partnerTF = pm.text(label='', h=20, al='left')
                        self.patnerButton = pm.button('Set selected', w=90, c=self.setPartner_CB)
                        
                    with pm.columnLayout():
                        with pm.rowLayout(numberOfColumns=5, cw=[[1,40], [2,60]]):
                            pm.text('Mirror:')
                            pm.text('translate')
                            self.tx = pm.checkBox(label='x', cc=self.CheckB_CB)
                            self.ty = pm.checkBox(label='y', cc=self.CheckB_CB)
                            self.tz = pm.checkBox(label='z', cc=self.CheckB_CB)
                        with pm.rowLayout(numberOfColumns=5, cw=[[1,40], [2,60]]):
                            pm.text(label='')
                            pm.text('rotate')
                            self.rx = pm.checkBox(label='x', cc=self.CheckB_CB)
                            self.ry = pm.checkBox(label='y', cc=self.CheckB_CB)
                            self.rz = pm.checkBox(label='z', cc=self.CheckB_CB)
                        with pm.rowLayout(numberOfColumns=5, cw=[[1,40], [2,60]]):
                            pm.text(label='')
                            pm.text('scale')
                            self.sx = pm.checkBox(label='x', cc=self.CheckB_CB)
                            self.sy = pm.checkBox(label='y', cc=self.CheckB_CB)
                            self.sz = pm.checkBox(label='z', cc=self.CheckB_CB)
                    
                    if showAdditionalOptions:        
                        pm.button('Add custom Attributes', w=90, c=self.setupCustom_CB)
                
                pm.separator(h=10)
                with pm.columnLayout(adjustableColumn=1):
                    pm.text('Find controller', h=20)
                    with pm.rowLayout(numberOfColumns=6, adjustableColumn=6):
                        
                        pm.button(l='Left', c=self.findLeft_CB, w=60)
                        pm.button(l='Center', c=self.findMid_CB, w=60)
                        pm.button(l='Right', c=self.findRight_CB, w=60)
                        pm.button(l='None', c=self.findNone_CB, w=60)
                        pm.separator(w=10, st='none')
                        pm.button(l='Partner', c=self.findPartner_CB, w=50)
                        
                    with pm.rowLayout(numberOfColumns=1, adjustableColumn=1):
                        pm.button(label='From selection in scene', w=90, c=self.findSelInList_CB)
                        
        self.formR.redistribute(1.0, 0.1, 1.6, 0.1, 1.4, 0.1, 0.8)
        self.mainForm.redistribute()
        pm.showWindow(UIname)
        
        self.populate()
    
    def partnerSliderDrag(self, *args):
        val = pm.floatSliderGrp(self.partnerFF, q=1, v=1)
        current = pm.floatSliderGrp(self.difFF, q=1, v=1)
        pm.floatSliderGrp(self.difFF, e=1, maxValue=val)
        
        if current > val:
            pm.floatSliderGrp(self.difFF, e=1, v=val)
    
    def findPartner_CB(self, *args):
        ctrls = []
        for e in self.get_all_selected():
            partner = e.get_partner()
            if partner:
                ctrls.append(partner) 
        if ctrls:
            pm.select(ctrls) 
            self.findSelInList_CB() 
    
    def findLeft_CB(self, *args):
        ctrls = []
        for k, v in self.ctrlDict.items():
            if pm.objExists(k):
                if v.ctrl.side.get() == 1:
                    ctrls.append( v.ctrl) 
        pm.select(ctrls) 
        self.findSelInList_CB() 
        
    def findMid_CB(self, *args):
        ctrls = []
        for k, v in self.ctrlDict.items():
            if pm.objExists(k):
                if v.ctrl.side.get() == 0:
                    ctrls.append( v.ctrl) 
        pm.select(ctrls) 
        self.findSelInList_CB() 
    
    def findRight_CB(self, *args):
        ctrls = []
        for k, v in self.ctrlDict.items():
            if pm.objExists(k):
                if v.ctrl.side.get() == 2:
                    ctrls.append( v.ctrl) 
        pm.select(ctrls) 
        self.findSelInList_CB() 
    
    def findNone_CB(self, *args):
        ctrls = []
        for k, v in self.ctrlDict.items():
            if pm.objExists(k):
                if v.ctrl.side.get() == 3:
                    ctrls.append( v.ctrl) 
        pm.select(ctrls) 
        self.findSelInList_CB() 
    
    def findSelInList_CB(self, *args):
        self.get_selected
        sl = pm.ls(sl=1)
        selLIst = []
        allItems = pm.textScrollList(self.scroll, q=1, ai=1)
        pm.textScrollList(self.scroll, e=1, da=1)
        if sl:
            for e in sl:
                if e.name() in allItems:
                    selLIst.append(e.name())
            if allItems:
                pm.textScrollList(self.scroll, e=1, si=selLIst)
            self.TSL_selection_CB()
        
    def makeCtrl_CB(self, *args):
        Vlist = lsSl.lsSl([], 1, 0)
        for e in Vlist:
            bn.set([e], 'transform_ctrl')
            tc.ctrl(Vlist=[e], edit=1)
        self.populate()
        self.findSelInList_CB()
    
    def unCtrl_CB (self, *args):
        Vlist = lsSl.lsSl([], 1, 0)
        for e in Vlist:
            if e.hasAttr('transform_ctrl'):
                pm.deleteAttr(e.transform_ctrl)
        self.populate()
        self.TSL_selection_CB()
    
    def setupAll_CB(self, *args):
        #ctrls = self.get_selected_ctrls()
        mid_dist = pm.floatSliderGrp(self.centerFF, q=1, v=1)
        partner_dist = pm.floatSliderGrp(self.partnerFF, q=1, v=1)
        dif_dist = pm.floatSliderGrp(self.difFF, q=1, v=1)
        allCtrls = self.get_all_ctrls()
        m = mirror_setup(Vlist=allCtrls)
        m.setup(mid_dist=mid_dist, partner_dist=partner_dist, dif_dist=dif_dist)
        
    def setupSel_CB(self, *args):
        ctrls = lsSl.lsSl([], 1, 0)
        mid_dist = pm.floatSliderGrp(self.centerFF, q=1, v=1)
        partner_dist = pm.floatSliderGrp(self.partnerFF, q=1, v=1)
        dif_dist = pm.floatSliderGrp(self.difFF, q=1, v=1)
        
        m = mirror_setup(Vlist=ctrls)
        m.setup(mid_dist=mid_dist, partner_dist=partner_dist, dif_dist=dif_dist)
        pm.select(ctrls)
        self.findSelInList_CB()
    
    def setupSide_CB(self, *args):
        ctrls = lsSl.lsSl([], 1, 0)
        mid_dist = pm.floatSliderGrp(self.centerFF, q=1, v=1)
        
        m = mirror_setup(Vlist=ctrls)
        m.get_ctrls()
        m.set_side(accuracy=mid_dist)
        pm.select(ctrls)
        self.findSelInList_CB()
        
    def setupPartner_CB(self, *args):
        ctrls = lsSl.lsSl([], 2, 0)
        partner_dist = pm.floatSliderGrp(self.partnerFF, q=1, v=1)
        dif_dist = pm.floatSliderGrp(self.difFF, q=1, v=1)
        
        m = mirror_setup(Vlist=ctrls)
        m.get_ctrls()
        m.find_partner(searchRange=partner_dist, difRange=dif_dist)
        pm.select(ctrls)
        self.findSelInList_CB()
        
    def setupRule_CB(self, *args):
        ctrls = lsSl.lsSl([], 1, 0)
        
        m = mirror_setup(Vlist=ctrls)
        m.get_ctrls()
        m.set_rule(1,1,1)
        pm.select(ctrls)
        self.findSelInList_CB()
        #self.TSL_selection_CB()
        
    def setupCustom_CB(self, *args):
        ctrls = lsSl.lsSl([], 1, 0)
        attr_list = pm.channelBox('mainChannelBox', q=1, selectedMainAttributes=1)
        if attr_list:
            m = mirror_setup(Vlist=ctrls)
            m.get_ctrls()
            m.set_custom(attr_list)
            pm.select(ctrls)
            self.findSelInList_CB()
            #self.TSL_selection_CB()
        
    def exclude_CB(self, *args):
        ctrls = lsSl.lsSl([], 1, 0)
        m = mirror_setup(Vlist=ctrls)
        m.get_ctrls()
        
        for e in m.ctrls:
            e.set_side(3)
            e.remove_partner()
            e.set_rule([[1,1,1], [1,1,1],[1,1,1]])
        self.TSL_selection_CB()    
    
    def setPartner_CB (self, *args):
        Vlist = pm.ls(sl=1)
        if Vlist:
            Vlist[0]
            c = self.get_selected()
            c.set_partner([Vlist[0]])
            pm.text(self.partnerTF, e=1, l=Vlist[0]) 
    
    def optionMenue_CB(self, item):
        side = 3
        if item == 'Center':
            side =  0
        if item == 'Left':
            side =  1
        if item == 'Right':
            side =  2 
        if item == 'None':
            side =  3
        print side
        sel = self.get_selected()
        sel.set_side(side)
        if side == 0 or side == 3:
            sel.remove_partner()
            pm.text(self.partnerTF, e=1, l='')
        if side == 3:
            sel.set_rule([[1,1,1], [1,1,1],[1,1,1]])
        self.TSL_selection_CB() 
            
        
    def CheckB_CB(self, *args):
        rule = [[self.convert_cb(pm.checkBox(self.tx, q=1, v=1)),
                 self.convert_cb(pm.checkBox(self.ty, q=1, v=1)),
                 self.convert_cb(pm.checkBox(self.tz, q=1, v=1))],
                                             
                [self.convert_cb(pm.checkBox(self.rx, q=1, v=1)),
                 self.convert_cb(pm.checkBox(self.ry, q=1, v=1)),
                 self.convert_cb(pm.checkBox(self.rz, q=1, v=1))],
                                            
                [self.convert_cb(pm.checkBox(self.sx, q=1, v=1)),
                 self.convert_cb(pm.checkBox(self.sy, q=1, v=1)),
                 self.convert_cb(pm.checkBox(self.sz, q=1, v=1))]]
        #print rule
        sel = self.get_selected()
        sel.set_rule(rule)
    
    def get_selected(self):
        sel = pm.textScrollList(self.scroll, q=1, si=1)
        if sel:
            # Avoid Maya crashing when a node does not exist anymore.
            if pm.objExists(sel[0]):
                return self.ctrlDict[sel[0]]
        else:
            return None
    
    def get_all_ctrls(self):
        result = []
        items = pm.textScrollList(self.scroll, q=1, ai=1)
        for e in items:
            # Avoid Maya crashing when a node does not exist anymore.
            if pm.objExists(e):
                result.append(self.ctrlDict[e].ctrl)
        return result
    
    def get_selected_ctrls(self):
        result = []
        sel = pm.textScrollList(self.scroll, q=1, si=1)
        for e in sel:
            # Avoid Maya crashing when a node does not exist anymore.
            if pm.objExists(e):
                result.append(self.ctrlDict[e].ctrl)
        return result
    
    def get_all_selected(self):
        result = []
        self.findSelInList_CB()
        sel = pm.textScrollList(self.scroll, q=1, si=1)
        for e in sel:
            # Avoid Maya crashing when a node does not exist anymore.
            if pm.objExists(e):
                result.append(self.ctrlDict[e])
        return result
        
    def convert_cb(self, val):
        if val == 0:
            return 1
        else:
            return -1
    
    def TSL_selection_CB(self):
        c = self.get_selected()
        #print sel
        if c:
            rule = c.get_rule()
            side = c.get_side()
            partner = c.get_partner()
            #print rule
            if rule:
                pm.checkBox(self.tx, e=1, v=rule[0][0]*-1+1)
                pm.checkBox(self.ty, e=1, v=rule[0][1]*-1+1)
                pm.checkBox(self.tz, e=1, v=rule[0][2]*-1+1)
                
                pm.checkBox(self.rx, e=1, v=rule[1][0]*-1+1)
                pm.checkBox(self.ry, e=1, v=rule[1][1]*-1+1)
                pm.checkBox(self.rz, e=1, v=rule[1][2]*-1+1)
                
                pm.checkBox(self.sx, e=1, v=rule[2][0]*-1+1)
                pm.checkBox(self.sy, e=1, v=rule[2][1]*-1+1)
                pm.checkBox(self.sz, e=1, v=rule[2][2]*-1+1)
            
            pm.text(self.currentCtrl, e=1, label=c.ctrl.name()) 
            pm.optionMenu(self.optionM, e=1, sl=side+1)
            pm.select(self.get_selected_ctrls())
            
            if not partner:
                partner = ''
            pm.text(self.partnerTF, e=1, l=partner) 
        
        else:
            pm.checkBox(self.tx, e=1, v=0)
            pm.checkBox(self.ty, e=1, v=0)
            pm.checkBox(self.tz, e=1, v=0)
                                        
            pm.checkBox(self.rx, e=1, v=0)
            pm.checkBox(self.ry, e=1, v=0)
            pm.checkBox(self.rz, e=1, v=0)
                                        
            pm.checkBox(self.sx, e=1, v=0)
            pm.checkBox(self.sy, e=1, v=0)
            pm.checkBox(self.sz, e=1, v=0)
            pm.text(self.currentCtrl, e=1, label='') 
            pm.optionMenu(self.optionM, e=1, sl=4)
            pm.text(self.partnerTF, e=1, l='')
            

    def populate(self, *args):
        self.ctrlDict = {}
        ctrlNames = []
        #ctrlsExtra = bn.getAll(Vlist=[], Vsuffix='',id='transform_ctrl_extra',)
        #for e in ctrlsExtra:
        #    bn.set([e.ctrl], id='transform_ctrl')
        
        ctrls = bn.getAll(Vlist=[], Vsuffix='',id='transform_ctrl',)
        
        for e in ctrls:
            ctrlNames.append(e.ctrl.name())
            self.ctrlDict[e.ctrl.name()] = e
            #print e
        #ctrlNames.sort()
        ctrlNames = sorted(ctrlNames, key=lambda v: (v.upper(), v[0].islower()))
        pm.textScrollList (self.scroll, e=1, removeAll=1)
        pm.textScrollList(self.scroll, e=1, append=ctrlNames)
    


class mirror_setup():
    """
    Setup a new character for mirroring.
    
    TODO: For center ctrls, find out if they are tilted on the z axis. If
    yes, mark their side as "None". Center ctrls tilted on z are asymmetrical 
    and can not be mirrored.
    """
    def __init__(self, Vlist=[], Vsuffix=''):
        Vlist = lsSl.lsSl(Vlist, 0, 0)
        self.Vlist = Vlist
        self.foul = []
        #print Vlist
    
    def setup(self, mid_dist=0.0001, partner_dist=0.01, dif_dist=0.001):
        self.get_ctrls()
        self.set_side(mid_dist)
        self.find_partner(partner_dist, dif_dist)
        self.set_rule()
        
    
    def get_ctrls(self):
        """
        Finds all controllers in scene and refreshes their setup if needed.
        """
        if self.Vlist:
            self.ctrls = bn.get(Vlist=self.Vlist, id='transform_ctrl',)
            #self.ctrls += bn.get(Vlist=self.Vlist, id='transform_ctrl_extra',)
        else:
            self.ctrls = bn.getAll(Vlist=[], id='transform_ctrl',)
            #self.ctrls += bn.getAll(Vlist=[], id='transform_ctrl_extra',)
            
        self.ctrlList = []
        for e in self.ctrls:
            self.ctrlList.append(e.ctrl)
        
        for e in self.ctrls:
            e.addAttributes()
        
    def set_side(self, accuracy=0.0001):
        """
        Finds the side by worldspace.
        If a right or lef ctrl does not have a partner it is assumed to be a
        centric ctrl.
        
        ==========
        0 - Center
        1 - Left
        2 - Right
        3 - None
        ==========
        """
        for e in self.ctrls:
            print e.ctrl
            ws = getWorldspace(e.ctrl)
            if ws[0][0] >= accuracy:
                e.set_side(1)
            elif ws[0][0] <= -accuracy:
                e.set_side(2)
            else:
                e.set_side(0)
    
    def find_partner(self, searchRange=0.01, difRange=0.001):
        """
        Finds the partner of all ctrls on the right side.
        Searches first by worldspace. If no partner is found looks for similar
        names in all ctrls. 
        """
        searchlist = []
        for c in self.ctrls:
            if c.get_side() == 1:
                searchlist.append(c.ctrl)
                
        print 'Searchlist (all controllers on the left side)'
        for e in searchlist:
            print e
        
        for e in self.ctrls:
            
            if e.get_side() == 2:
                result = None
           
                closest = td.findClosest(searchlist + [e.ctrl], searchRange, [-1,1,1], difRange)
                #print '###'
                #print closest.sortedDist
                
                if closest.result:
                    if len(closest.result) == 1:
                        result = closest.result[0][1]
                     
                    else:  
                        searchlistNames = []
                        for c in closest.sortedDist:
                            searchlistNames.append(c[1]) 
 
                        result = compare_string().find_closest(searchlistNames, e)
                
                if result:
                    e.remove_partner()
                    e.set_partner([result])
                else:
                    self.foul.append(e)
                    
                print 'Current Ctrl ----------------------------------'
                print '  ', e.ctrl
                #print closest.sortedDist
                #print closest.result
                print '  ', result
            
        print 'Unfinished: '
        for e in self.foul:
            print e.ctrl
        print '----------'
        

                
    def set_rule(self, center=1, right=1, left=0):
        """Sets the rule to the ctrl partners"""
        for e in self.ctrls:
            if right:
                if e.get_side() == 2:
                    partner = e.get_partner()
                    if partner:
                        rule = get_rule([e.ctrl, e.get_partner()])
                        trs = rule.rule + [[1,1,1]]
                        e.set_rule(trs)
                    else:
                        print 'No partner found for: ', e.ctrl.name() 
            
            if left:
                if e.get_side() == 1:
                    partner = e.get_partner()
                    if partner:
                        rule = get_rule([e.ctrl, e.get_partner()])
                        trs = rule.rule + [[1,1,1]]
                        e.set_rule(trs)
                    else:
                        print 'No partner found for: ', e.ctrl.name()
            
            if center:    
                if e.get_side() == 0:
                    loc = pm.spaceLocator()
                    locPar = pm.spaceLocator() 
                    snap.snapRot(Vlist=[loc, locPar, e.ctrl])
                    loc.setParent(locPar)
                    rule = get_rule([e.ctrl, loc])
                    trs = rule.rule + [[1,1,1]]
                    e.set_rule(trs)
                    pm.delete(locPar)
    
    def set_custom(self, attr_list):
        """Sets all custom attributes that should be inverted"""
        for e in self.ctrls:
            for a in attr_list:    
                if not e.ctrl.hasAttr('mirror_custom'):
                    e.ctrl.addAttr('mirror_custom', dt='string')
                e.ctrl.mirror_custom.set(str(attr_list)[1:-1])
            
class compare_string(): 

    def find_closest(self, stringlist, match):
        """
        Finds the closest string to match in a list of strings.
        """ 
        fuzzyness = []
        for e in stringlist:
            fuzzyness.append([self.fuzzy_substring(e.name(), match.ctrl.name()), e])
        return sorted(fuzzyness)[0][1]
        
    def fuzzy_substring(self, needle, haystack):
        """Calculates the fuzzy match of needle in haystack,
        using a modified version of the Levenshtein distance
        algorithm.
        The function is modified from the levenshtein function
        in the bktree module by Adam Hupp"""
        m, n = len(needle), len(haystack)
    
        # base cases
        if m == 1:
            return not needle in haystack
        if not n:
            return m
    
        row1 = [0] * (n+1)
        for i in range(0,m):
            row2 = [i+1]
            for j in range(0,n):
                cost = ( needle[i] != haystack[j] )
    
                row2.append( min(row1[j+1]+1, # deletion
                                   row2[j]+1, #insertion
                                   row1[j]+cost) #substitution
                               )
            row1 = row2
        
        # add 0.01 if length is not the same
        wrongLen = abs(m-n)/100.0

        return min(row1)+ wrongLen

class get_rule():
    def __init__(self, Vlist=[]):
        """
        Returns the mirro rule for translation and rotation.
        """
        print '--------------------------------'
        print Vlist
        Vlist = lsSl.lsSl(Vlist, 2, 2)
        self.Vlist = Vlist
        scale = self.get_negative_scale()
        if scale == -1:
            self.rule = [[1,1,1],[1,1,1]]
        else:       
            self.rule = []
            rotRule = self.get_lr(use_jointOrient=0)
            self.rule.append(rotRule)
            
            if self.Vlist[0].nodeType() == 'joint':
          
                rot = self.get_lr(use_jointOrient=1)
                self.rule.insert(0, [rot[0]*-1,rot[1]*-1,rot[2]*-1])
                
                print 'Ctrl is a joint.'
                print ''
            else:
                rot = [rotRule[0],rotRule[1],rotRule[2]]
                self.rule.insert(0, [rot[0]*-1,rot[1]*-1,rot[2]*-1])
                print 'Is NO joint. Orientation:'
        print 'Rule:', self.rule
        print ''
        print 'Scale: ', scale
        pm.select(Vlist)
    
    def make_dummy(self, transform, axis=[1,-1,-1], use_jointOrient=0):
        """
        Creates a dummy object for the given controller.
        axis - x=[1,-1,-1]
        """
        typ = 1
        if transform.nodeType() == 'joint':
            typ = 2
        
        dummy = tp.nullGroup(Vlist=[transform], Vtype=typ, Vsuffix='_dummy')[0]
        dummy.setParent(transform.getParent())
        
        pm.select(cl=1)
        locA = pm.joint()
        
        if use_jointOrient:
            try:
                jo = dummy.jointOrient.get()
                locA.jointOrient.set(jo)
                print 'Joint orient set to:', jo
            except:
                pass
       
        locAA = pm.spaceLocator()
        snap.snapTrans([locA, dummy])
        snap.snap([locAA, dummy])
        locA|locAA
        
        locA.r.set(25*axis[0],25*axis[1],25*axis[2]) 
        snap.snapRot([dummy, locAA])
        rot=[dummy.rx.get(), dummy.ry.get(), dummy.rz.get()]
        
        pm.delete(locA, dummy)
        return rot
    
    def get_lr (self, use_jointOrient=0):
        """
        Calculate the rule for a left/right pair.
        """
        rotA = self.make_dummy(self.Vlist[0], [1,1,1], use_jointOrient)
        rotB = self.make_dummy(self.Vlist[1], [1,-1,-1], use_jointOrient)
        
        rot=[rotA[0] - rotB[0], 
             rotA[1] - rotB[1],
             rotA[2] - rotB[2]]
        
        rule=[]
        for e in rot:
            if self.round_pos(e):
                rule.append(-1)
            else:
                rule.append(1)

        return rule
    
    def get_negative_scale(self):
        """
        Returns -1 if one of the controllers is in opposite scaled space.
        
        Using the decomposed matrix it seems impossible to find out which axis 
        is negative scaled.The decomposed matrix always returns the z axis 
        as negative. For example a scale of -1,1,1 will return 1,1-1. 
        A double negative scale like: -1,1,-1 returns 1,1,1. A triple negative
        returns 1,1,-1. Unparenting the controller will give the same result.
        
        World scale if inverse is [1,1,-1], no matter which axis is inversed in 
        the cannel box.
        If inverse: trans*[-1,-1,1], rot[1,1,-1]
        """
        wsA = getWorldspace(self.Vlist[0])[2]
        wsB = getWorldspace(self.Vlist[1])[2]
        ws = [wsA[0]*wsB[0],wsA[1]*wsB[1],wsA[2]*wsB[2]]
        if self.round(ws[2]) < 0:
            return -1
        else:
            return 1
    
    def round(self, val):
        return int(val*100000000)/100000000
     
    def round_pos(self, val):
        """
        Sets the value to positive and rounds it.
        """
        if val < 0:
            val = val*-1
        return int(val*100000000)/100000000

def getWorldspace(transform):
    """
    Returns the worldspace of the given transform node.
    Creates a decomposeMatrix node to get world transform.
    """
    if not pm.pluginInfo('decomposeMatrix', loaded=1, q=1):
        try:
            pm.loadPlugin('decomposeMatrix')
        except:
            pass
    if not pm.pluginInfo('matrixNodes', loaded=1, q=1):
        try:
            pm.loadPlugin('matrixNodes')
        except:
            pass

    dcm = pm.createNode('decomposeMatrix')
    dcmN = dcm.name()
    pm.ls(transform)[0].worldMatrix >> dcm.inputMatrix
    grp = pm.group(em=1, w=1)
    snap.snapTrans(Vlist=[grp, transform])
    t = grp.t.get()
    #t = cm.getAttr(dcmN + '.outputTranslate')[0]
    r = cm.getAttr(dcmN + '.outputRotate')[0]
    s = cm.getAttr(dcmN + '.outputScale')[0]
    h = cm.getAttr(dcmN + '.outputShear')[0]
    
    pm.delete(dcm, grp)
    
    return (t, r, s, h)
                
class mirror_pose():
    """
    Mirrors the values of the selected controller over to the other side.
    
    Use this command for a hotkey or shelf button::
    
        import mf.rigs.tools.mirror
        mf.rigs.tools.mirror.mirror_pose()
    
    This is the fast version of mirror_pose.
    It does not use vector class, pymel, or the ctrl class.
    The previous Version can be used as mirror_pose_OLD.
    """
    def __init__(self):
        sel = cm.ls(sl=1)
        if not sel:
            raise Exception('Nothing selected')
        ctrls = []
        ctrlsM = []
        ctrlsR = []
        ctrlsL = []
        for e in sel:
            try:
                cm.getAttr(e + '.transform_ctrl')
                ctrls.append(e)
            except:
                try:
                    cm.getAttr(e + '.transform_ctrl_extra')
                    ctrls.append(e)
                except:
                    pass
            
        for e in ctrls:
            side = cm.getAttr(e + '.side')
            if side == 0:
                ctrlsM.append(e)
            if side == 1:
                ctrlsL.append(e)
            if side == 2:
                ctrlsR.append(e)
        
        dataR = []
        for e in ctrlsR:
            partner = cm.listConnections(e + '.mirror_partner', plugs=0)
            if partner:
                dataR.append(self.get_values(e, partner[0]))
        
        dataL = []
        for e in ctrlsL:
            partner = cm.listConnections(e + '.mirror_partner', plugs=0)
            if partner:
                dataL.append(self.get_values(e, partner[0]))
        
        dataM = []
        for e in ctrlsM:
            dataM.append(self.get_values(e, e))
        
        for e in dataR:
            self.set_values(e)
            
        for e in dataL:
            self.set_values(e)
            
        for e in dataM:
            self.set_values(e)
    
    def get_rule(self, trans):
        strA = cm.getAttr(trans + '.mirror_rule')
        return eval('['+strA+']')
    
    def get_custom_rule(self, trans):
        try: 
            strA = cm.getAttr(trans + '.mirror_custom')
        except:
            return []
        return eval('['+strA+']')
        
    def set_values(self, rule):
        av = [[rule[0] + '.tx', rule[1][0][0]],
              [rule[0] + '.ty', rule[1][0][1]],
              [rule[0] + '.tz', rule[1][0][2]],
                              
              [rule[0] + '.rx', rule[1][1][0]],
              [rule[0] + '.ry', rule[1][1][1]],
              [rule[0] + '.rz', rule[1][1][2]],
                                  
              [rule[0] + '.sx', rule[1][2][0]],
              [rule[0] + '.sy', rule[1][2][1]],
              [rule[0] + '.sz', rule[1][2][2]]] 
        
        for e in av:
            try:
                cm.setAttr(e[0], e[1])
            except:
                pass
        for e in rule[2]:
            try:
                cm.setAttr(rule[0] + '.' + e[0], e[1])
            except:
                pass
                
    def get_values(self, trans, goal):
        
        rule = self.get_rule(trans)
        custom = self.get_custom_rule(trans)
        
        tx = cm.getAttr(trans + '.tx')*rule[0][0]
        ty = cm.getAttr(trans + '.ty')*rule[0][1]
        tz = cm.getAttr(trans + '.tz')*rule[0][2]
    
        rx = cm.getAttr(trans + '.rx')*rule[1][0]
        ry = cm.getAttr(trans + '.ry')*rule[1][1]
        rz = cm.getAttr(trans + '.rz')*rule[1][2]
        
        sx = cm.getAttr(trans + '.sx')*rule[2][0]
        sy = cm.getAttr(trans + '.sy')*rule[2][1]
        sz = cm.getAttr(trans + '.sz')*rule[2][2]
        
        userDef = cm.listAttr(trans, userDefined=1, k=1)
        userDefKey = []
        
        if userDef:
            for e in userDef:
                val = cm.getAttr(trans + '.' + e)
                if e in custom:
                    #print 'Found custon attribute:', e
                    val= val*-1
                
                userDefKey.append([e, val])
        
        return [goal, [[tx, ty, tz], 
                        [rx, ry, rz],
                        [sx, sy, sz,]], userDefKey]
        
class mirror_pose_OLD():
    """
    This is an obsolete version. Please use 'mirror_pose()' instead.
    
    Class for mirroring and swaping poses.
    TODO: Speed up performance.
    """
    def __init__(self, Vlist=[], axis='x'):
        """
        """
        Vlist = lsSl.lsSl(Vlist, 1, 0)
        self.Vlist = Vlist
        ctls = bn.get(Vlist, 'transform_ctrl')
        ctls += bn.get(Vlist, 'transform_ctrl_extra')
        if ctls:
            leftSide = []
            leftSideCustom = []
            
            for e in ctls:
                t = trans()
                t.set(e.ctrl)
                t.mult(e.get_rule())
                
                #print t.m
                #print e.ctrl
                if e.get_side() == 1:
                    partner = e.get_partner()
                    if partner:
                        leftSide.append([t, partner])     
                    leftSideCustom.append(self.mirror_custom(e.ctrl, partner, apply=0))
                    
            for e in ctls:
                    
                t = trans()
                t.set(e.ctrl)
                t.mult(e.get_rule())
                #print t.m
                #print e.ctrl
                
                if e.get_side() == 2:
                    t = trans()
                    t.set(e.ctrl)
                    t.mult(e.get_rule())
                    
                    partner = e.get_partner()
                    if partner:
                        t.apply(partner)
                    self.mirror_custom(e.ctrl, partner)
                elif e.get_side() == 0:
                    t = trans()
                    t.set(e.ctrl)
                    t.mult(e.get_rule())
                    t.apply(e.ctrl)
            
            for e in leftSide:
                e[0].apply(e[1])
            
            #print leftSideCustom    
            for e in leftSideCustom:
                #print e
                for f in e:
                    f[0].set(f[1])
    
    def mirror_custom(self, source, goal, apply=1):
        """
        Mirrors all custom attributes.
        """
        attributes = pm.listAttr(source, userDefined=1)
        result=[]
        for e in attributes:
            sourceAttr = pm.ls(source.name()+'.'+e)[0]
            val = sourceAttr.get()
            if goal:
                if goal.hasAttr(e):
                    goalAttr = pm.ls(goal.name()+'.'+e)[0]
                    if sourceAttr.isKeyable() and not sourceAttr.isLocked():
                        #print sourceAttr    
                        try:
                            if apply:
                                goalAttr.set(val)
                            else:
                                result.append([goalAttr, val])
                        except:
                            print goalAttr, 'Not set.'
                            pass
        return result
                    
                    
class trans():
    """
    Class for transform calculations.
    """
    def __init__(self):
        
        self.m=(
            (None,None,None),   
            (None,None,None),  
            (None,None,None),  
            (None,None,None))
          
        self.atrrNames=(
            ('tx', 'ty', 'tz'), 
            ('rx', 'ry', 'rz'), 
            ('sx', 'sy', 'sz'),
            ('shxy', 'shxz', 'shyz'))
        
    def set(self, transform):
        """
        Sets the matrix to the current attribute values.
        """
        self.m = []
        for e in self.atrrNames:
            l = []
            for a in e:
                attr = getattr(transform, a)
                l.append(attr.get())
            self.m.append(l)

    def mult(self, val):
        """
        Multiplies the transform matrix with the input values.
        """
        self.m=(
            (self.m[0][0]*val[0][0], self.m[0][1]*val[0][1], self.m[0][2]*val[0][2]),
            (self.m[1][0]*val[1][0], self.m[1][1]*val[1][1], self.m[1][2]*val[1][2]),
            (self.m[2][0]*val[2][0], self.m[2][1]*val[2][1], self.m[2][2]*val[2][2]),
            (self.m[3][0]*val[3][0], self.m[3][1]*val[3][1], self.m[3][2]*val[3][2]))
    
    def apply(self, transform):
        """
        Applie the matrix to a transform.
        """
        for i, e in enumerate(self.atrrNames):
            for j, a in enumerate(e):
                transAttr = getattr(transform, a)
                if not transAttr.isLocked():
                    transAttr.set(self.m[i][j])
                    
def includeExtraCtrls():
    """
    Tags all extra controller with the 'transform_ctrl' id.
    """
    extra = bn.getAll(Vlist=[], id='transform_ctrl_extra')
    for e in extra:
        bn.set([e.ctrl], id='transform_ctrl')
        
        
class findControls():
    """
    Looks at a scene and makes a guess about what could be a controller.
    All curves with non selectable overrided will be skipped. 
    Also all curves that are invisible with locked visibility.
    """
    def __init__(self, *args):
        self.result = []
        allCurveShapes = pm.ls(type='nurbsCurve')
        for e in allCurveShapes:
            if not self.isSelectable(e):
                pass
            else:
                self.result.append(e.getParent())
                e.isVisible
        
        print "These objects could be controllers:"
        for e in self.result:
            print e
        pm.select(self.result)
        
    def isSelectable(self, node, override=2):
        
        if not node.attr('v').get() and node.attr('v').isLocked() and not node.attr('v').inputs():
            return False
                
        if  node.attr('overrideEnabled').get() and node.attr('overrideDisplayType').get() > 0:
            return False
        
        if node.attr('template').get():
            return False
        
        parent = node.getParent()
        if not parent:
            return True
        else:
        
            return self.isSelectable(parent)
        

    
    
    
    





                
                
