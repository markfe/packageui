import pymel.core as pm
import mf.tools.snap as snap
import mf.tools.lsSl as lsSl
import mf.tools.parent as tp

class loc():
    """
    A locator that will replace a controller.
    It will have the same space, so that animation can be copied back and forth.
    """
    def __init__(self, Vlist=[], Vsuffix=''):
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        
        self.ctrl = Vlist[0]
        self.suffix = Vsuffix
        
        self.consP = None
        self.consS = None
        
        #self.create()
        self.edit()
        
    
    def create(self):
        self.locPar = pm.group(em=1, w=1, n=self.ctrl.nodeName() + '_locPar')
        self.loc = pm.spaceLocator(n=self.ctrl.nodeName() + '_loc')
        self.loc.getShape().overrideEnabled.set(1)
        self.loc.getShape().overrideColor.set(14)
        
        self.set_align()
        self.set_constraint()
        
    def edit(self):
        self.ctrlPar = self.ctrl.getParent()
        
        childrn = self.ctrl.getChildren()
        for e in childrn:
            if e.nodeType() == 'parentConstraint':
                self.consP = e
            if e.nodeType() == 'scaleConstraint':
                self.consS = e
        if self.consP:
            self.loc = self.consP.target[0].targetTranslate.connections()[0]
            self.locPar = self.loc.getParent()
        else:   
            self.create()      
        #self.loc = 
        #self.locPar
         
    def set_align(self):
        self.ctrlPar = self.ctrl.getParent()
        if self.ctrlPar:
            self.ctrlPar|self.locPar|self.loc
        self.loc.s.set(self.ctrl.s.get())
        
        if self.ctrlPar:
            snap.snap([self.locPar, self.ctrlPar])
        self.locPar.t.set([0,0,0])  
        self.locPar.r.set([0,0,0]) 
        self.locPar.s.set([1,1,1])     
        snap.snap([self.loc, self.ctrl])

    def set_constraint(self):
        self.consP = pm.parentConstraint([self.loc, self.ctrl], 
                                                  n='pCons' + self.suffix)
        self.consS = pm.scaleConstraint([self.loc, self.ctrl], 
                                                 n='sCons' + self.suffix)
        
    def get_weights(self):
        print pm.parentConstraint(self.consP, q=1, targetList=1)
        weightAliases = pm.parentConstraint(self.consP, q=1, weightAliasList=1)
        
        for w in weightAliases:
            print self.loc.addAttr(w.longName(), k=1)

                
                
class switch():
    """
    Space switch.
    TODO:
    Where to store the offset? (in the constraint or on a separate transform?)
    """
    def __init__(self, Vlist=[], Vsuffix=''):
        Vlist = lsSl.lsSl(Vlist, 2, 0)
        self.suffix = Vsuffix
        
        childrn = Vlist[-1].getChildren()
        print childrn
        self.consP = None
        self.consS = None
    

    def set_constraint(self, Vlist=[]):
        """
        Adds constraints to the given goal object.
        """
        Vlist = lsSl.lsSl(Vlist, 2, 0)
        
        for e in Vlist[:-1]:  
            
            null = tp.nullGroup(Vlist=[e], Vhierarchy=4,  Vsuffix='_goal')[0] 
            self.consP =pm.parentConstraint([null, Vlist[-1]], 
                                                  n='pCons' + self.suffix)
            self.consS = pm.scaleConstraint([null, Vlist[-1]], 
                                                 n='sCons' + self.suffix)
    def set_weights(self, Vlist=[]):
        Vlist = lsSl.lsSl(Vlist, 1, 1)
  
        #print pm.parentConstraint(e, q=1, targetList=1)
        weightAliases = pm.parentConstraint(self.consP, q=1, weightAliasList=1)
        
        for w in weightAliases:
            if not Vlist[0].hasAttr(w.longName()):
                Vlist[0].addAttr(w.longName(), k=1, min=0, max=1)
                newAttr = getattr(Vlist[0], w.longName())
                newAttr >> w
                print newAttr
                
class UI():
    def __init__(self, Vlist=[]):
        
        UIname = 'space_switch'
        print UIname
        try:
            pm.deleteUI(UIname)
        except: 
            pass
        pm.window(UIname, t=UIname, h=10, w=100, s=1)
        self.mainFormA = pm.formLayout()
        
        self.createUI(self.mainFormA)
        #self.editUI(self.mainFormA)
        
        self.mainFormA.redistribute(0,1)
        pm.showWindow(UIname)
    
    def createUI(self, parent=''):
        """
        UI to create camera planes.
        """
        self.mainFormB = pm.formLayout(p=parent)
      
        self.tF = pm.text(l='First select all goal objects and  \n  last the object to be constraint.')

        pm.button(l='add constraint', w=60, h=40, c=self.create)
        pm.separator(h=10, st='none')
        self.mainFormB.redistribute()
        
    def create(self, *args):
        Vlist = lsSl.lsSl([], 1, 0)
        l = loc(Vlist=[Vlist[-1]])
        
        cons = False
        childrn = l.locPar.getChildren()
        for e in childrn:
            if e.nodeType() == 'parentConstraint':
                cons = True
        s = switch(Vlist[:-1]+[l.locPar])
        if not cons:
            s.set_constraint([l.locPar.getParent(), l.locPar])
            s.set_weights([l.loc])
            
        s.set_constraint(Vlist[:-1]+[l.locPar])
        s.set_weights([l.loc])
        
        
        

        
    
        
    
