"""
Functions that can be generally used for more than one character.
"""

import pymel.core as pm
import mf.builder.nodes as bn
import mf.tools.lsSl as lsSl


class allCleanup():
    
    def __init__(self, Vlist=[], Vsuffix='', rootPart='', edit=0):
        """
        Cleanup Asset, after building and assembling the separate parts.
        """
        Vlist[0].rename('loc' + Vsuffix)
        self.asset = bn.getAll('transform_character')[0]
        
        self.suffix = Vsuffix
        self.rootPar = rootPart
        
        self.ctrls()
        if not edit:
            self.hierarchy()
        self.connectFosterObjects()
        self.hideLocShapes()
        self.organizeMeshes()
        self.switches()
        if not edit:
            self.proxy()
        self.leftoverGroup()

    
    def ctrls(self):
        """
        Cleans up the controllers.
        """
        
        ctrls = []
        ctrls_extra = []
        ctrl = bn.getAll('transform_ctrl')
        ctrl_extra = bn.getAll('transform_ctrl_extra')
        
        for c in ctrl:
            c.cleanupChannels()
            ctrls.append(c.ctrl)
            c.cleanupLocked()
            
        for c in ctrl_extra:
            c.cleanupChannels()
            ctrls_extra.append(c.ctrl)
            c.cleanupLocked()
        
        # "All" set
        if not ctrls == []:
                pm.sets(ctrls, n='All')
                
                for o in ctrls:
                    if o.getShape():
                        o.getShape().overrideEnabled.set(1)
                        self.asset.group.controllers_vis >> o.getShape().overrideVisibility
    
    def hierarchy(self):
        """
        Organizes the hierarchy of all loose rig parts.
        TODO: double main ctrl.
        """
        # no_trans ----------------------------------------------------------------
        no_trans = pm.group(em=1, w=1, n='no_trans' + self.suffix)
        no_trans.v.set(0)
        t = bn.getAll('transform_no_trans')
        for o in t:
            if not o.group.getParent():
                no_trans|o.group
          
        # main_trans --------------------------------------------------------------
        main_trans = pm.group(em=1, w=1, n='main_trans' + self.suffix)
        bn.set([main_trans], 'transform_main_trans_grp')
        
        try:
            mt = bn.getAll('transform_main_trans')
            for o in mt:
                if not o.group.getParent():
                    main_trans|o.group
        except:
            print 'transform_main_trans not found' 
        
        if not self.rootPar == '':
            main_trans|pm.ls(self.rootPar)[0]
        
        self.asset.group|no_trans
        
        main = bn.getAll('transform_main')[0]
        main.groupB|main_trans
        
        packageRig = bn.getAll('package_rig')
        for o in packageRig:
            if not o.group.getParent():
                try:
                    self.asset.group|o.group
                except:
                    pass
        
    def connectFosterObjects(self):
        """
        Reconnect all foster objects.
        """
        d = bn.getAll('transform_foster')
        for o in d:
            print o
            o.connect()  
    
    def hideLocShapes(self):
        """
        Sets override visibility to 0 for all locator shapes.
        """
        ls = pm.ls(type='locator')
        for o in ls:
            try:
                o.overrideEnabled.set(1)
                o.overrideVisibility.set(0)
            except:
                pass 
    
    def organizeMeshes(self):
        """
        Creates a mesh group and organizes all meshes without parent under it. 
        """
        mesh_grp = pm.group(w=1, em=1, n='mesh_grp')
        bn.set([mesh_grp], 'transform_no_trans')
        bn.set([mesh_grp], 'transform_mesh_grp')
        
        ass = pm.ls(assemblies=1)
        meshGrp = bn.getAll('transform_mesh_grp')[0]
        for a in ass:
            if a.getShape():
                if a.getShape().nodeType() == 'mesh':
                    meshGrp.group|a
        self.asset.group|meshGrp.group
        
        rendermeshes = bn.getAll('mesh_rendermesh')
        for e in rendermeshes:
          
            meshGrp.group|e.mesh

        for e in pm.ls(type='mesh'):
            par = e.getParent()
            par.overrideEnabled.set(1)
            self.asset.group.meshes_override >> par.overrideDisplayType
            self.asset.group.meshes_vis >> par.v


    def switches(self):
        """
        Switches for visibility, meshes override, controllers. 
        """
        mesh_grp = bn.getAll('transform_mesh_grp')[0]
        
        mesh_grp.group.overrideEnabled.set(1)
        self.asset.group.meshes_override >> mesh_grp.group.overrideDisplayType
        self.asset.group.meshes_vis >> mesh_grp.group.v
    
    def leftoverGroup(self):
        """
        Parents eveything that is in the world to a "assemblies" group under 
        the character.
        """
        leftover = pm.group(n='assemblies', em=1, w=1)
        self.asset.group|leftover
        
        ass = pm.ls(assemblies=1)
        for a in ass:
            print a
            if a == self.asset.group or str(a)=='persp' or str(a)=='front'\
                                   or str(a)=='side' or str(a)=='top'\
                                   or str(a)=='back':
                pass
            else:
                pass
                leftover|a
    
    def proxy(self):
        """
        Display Layer.
        """   
        b = bn.getAll('joint_bind')
        
        for o in b:
            if not o.proxy== '':
                #pm.editDisplayLayerMembers(prx_dl, o.proxy, nr=1)
                o.proxy.overrideEnabled.set(1)
                self.asset.group.proxy_override >> o.proxy.overrideDisplayType
                o.proxy.v.setLocked(0)
                self.asset.group.proxy_vis >> o.proxy.v
                
            o.bj.drawLabel.set(0)
            
def addCtrls(Vlist=[], Vsuffix=''):
    """
    Sets a ctl.
    """ 
    Vlist = lsSl.lsSl(Vlist=Vlist, Vmin=1, Vmax=0)
    ctrl = Vlist
    bn.set(Vlist, 'transform_ctrl')
    asset = bn.getAll('transform_character')[0]
    ctrls = bn.get(Vlist, 'transform_ctrl')
    
    
    for c in ctrls:
        c.cleanupChannels()
        c.cleanupLocked()
 
    pm.sets('All', add=Vlist)
            
    for o in ctrl:
        o.getShape().overrideEnabled.set(1)
        print o.getShape()
        asset.group.controllers_vis >> o.getShape().overrideVisibility
        
        
        
        
        
        
        
        
        
        
    
    
