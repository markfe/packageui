"""
Body rig motion path.
"""


import pymel.core as pm
import maya.mel as mel
import mf.builder.nodes
import mf.tools.lsSl as lsSl
import mf.tools.snap as snap
import mf.tools.proxyGeo as pg
import mf.tools.parent as tp
import mf.tools.cons as tc


class spine():
    def __init__(self, Vlist=[], Vsuffix='', num=10):
               
        """
        Creates joints, distributed on a curve with a motion path. 
        Rotation is set to an up object.

        ========== =====================================================
        Arguments  Description
        ========== =====================================================
        Vlist[0]   curve
        Vlist      up object 
        ========== =====================================================
        """
        Vlist = lsSl.lsSl(Vlist, 2, 2)
        self.suffix = Vsuffix
        self.num = num
        # unit conversion nodes do not work automatically.
        unit = pm.currentUnit( q=1, l=1)
        self.unitMult = 1
        if unit == 'cm':
            self.unitMult = 100
        if unit == 'mm':
            self.unitMult = 1000
        
        pm.select(cl=1)
        self.scaleGrp = pm.group(em=1, w=1, n='scale' + self.suffix)
        self.no_trans = pm.group(em=1, w=1, n='no_trans' + self.suffix)
        j = []
        
        self.no_trans|Vlist[0]
        self.no_trans|Vlist[1]
        
        aim = self.pathAim(Vlist[1])
        bnd = self.path(Vlist[0], aim)
    
    def mpAttach(self, Vlist=[], Vsuffix=''):
        Vlist = lsSl.lsSl(Vlist, 2, 0, ['any'])
        curve = Vlist[0]
        trans = Vlist[1]
        mp = pm.createNode('motionPath')
        mp.rotateOrder >> trans.rotateOrder
        mp.message >> trans.specifiedManipLocation
        mp.rotateZ >> trans.rotate.rotateZ
        mp.rotateY >> trans.rotate.rotateY
        mp.rotateX >> trans.rotate.rotateX
        mp.allCoordinates >> trans.translate
        mp.fractionMode.set(1)
        mp.worldUpType.set(1)
        curve.getShape().worldSpace >> mp.geometryPath
        
        return mp
    
    def pathAim(self, curve):
        j = []
        for i in range(self.num):
            if i == 0:
                uVal = 0.00001*self.unitMult
            elif i == self.num-1:
                uVal = 0.00999*self.unitMult
            else:
                uVal=(0.01/(self.num-1))*(i)*self.unitMult
                   
            pm.select(cl=1)
            jnt = pm.joint(n='p_' + str(i) + self.suffix + '_am')
            j.append(jnt)
            mPath = self.mpAttach([curve, jnt])
#             mPathStr = pm.pathAnimation(jnt, curve,
#                        fractionMode=True, follow=True, followAxis='x',
#                        upAxis='y', worldUpType="object",
#                        worldUpVector=(0, 1, 0), inverseUp=False,
#                        inverseFront=False, bank=False)
#             
#             mPath = pm.ls(mPathStr)[0]
#             pm.disconnectAttr(mPath.uValue)
            mPath.uValue.set(uVal)
            
            self.no_trans|jnt
        return j
        
    def path(self, curve, aim):
        j = []
        for i in range(self.num):
            if i == 0:
                uVal = 0.00001*self.unitMult
            elif i == self.num-1:
                uVal = 0.00999*self.unitMult
            else:
                uVal=(0.01/(self.num-1))*(i)*self.unitMult
                   
            pm.select(cl=1)
            jnt = pm.joint(n='p_' + str(i) + self.suffix + '_bd')
            j.append(jnt)
            mPath = self.mpAttach([curve, jnt])
#             mPathStr = pm.pathAnimation(jnt, curve,
#                        fractionMode=True, follow=True, followAxis='x',
#                        upAxis='y', worldUpType="object",
#                        worldUpVector=(0, 1, 0), inverseUp=False,
#                        inverseFront=False, bank=False)
#             
#             mPath = pm.ls(mPathStr)[0]
#             pm.disconnectAttr(mPath.uValue)
            mPath.uValue.set(uVal)
            
            aim[i].worldMatrix[0] >> mPath.worldUpMatrix
            
            pg.proxyGeo([jnt], VcenterPivot=1, chain=0, xScale=0.5, 
                        yzScaleGlobal=0.04, Vsides=4)
            
            self.scaleGrp.s >> jnt.s
            self.no_trans|jnt
        return j
    
    
class spineCtrls_biped():
    """
    TODO:
    Rebuild curve with 5 CVs.
    Pivot ctrls.
    """
    def __init__(self, Vlist=[], Vsuffix=''):
        
        self.sl = lsSl.sl(Vlist, 3, 0,
                  slDict={
                          'root':'0',
                          'base':'1',
                          'hip':'2',
                          'mid':'3',
                          'top':'4',
                          'chest':'5',
                          'curve_bind':'6',
                          'curve_up':'7',
                          'midBase':'8'
                          },
                          Vrename=1, Vsuffix=Vsuffix)
        
        self.sl.root|self.sl.base
        self.sl.root|self.sl.hip
        #self.sl.base|self.sl.midBase
        self.sl.base|self.sl.midBase|self.sl.top|self.sl.chest
        self.sl.base|self.sl.mid
        
        ctrlNull = tp.nullGroup(Vlist=[self.sl.root, self.sl.base, self.sl.mid, 
                            self.sl.top, self.sl.hip, self.sl.chest, 
                            self.sl.midBase], 
                      Vhierarchy=3, Vprefix='null_')
        
        curveBind = tp.nullGroup(Vlist=[self.sl.base, self.sl.mid, self.sl.chest], 
                      Vhierarchy=4, Vtype=2, Vname='', Vprefix='bind_', Vsuffix='', 
                      Vnum=1, jointRadius=1.1, pivot=1, rot=1, connectInverseScale=0, 
                      freez=1)
        
        tc.spaceSwitch(Vlist=[self.sl.base, self.sl.midBase, self.sl.top, ctrlNull[2], self.sl.mid],
                       VattrName=[], Vtype=3, 
                       Vsuffix='', VlastObj=1, Voffset=1, headlineAttr=1)
        
        skinCurve([self.sl.curve_bind] + curveBind)
        skinCurve([self.sl.curve_up] + curveBind)
        
        mp = spine([self.sl.curve_bind, self.sl.curve_up], Vsuffix + '_pt')
        
        self.gT = tp.trans(em=1, w=1, n='trans' + Vsuffix).group
        self.gNT = tp.noTrans(em=1, w=1, n='no_trans' + Vsuffix).group
        self.gMT = tp.mainTrans(em=1, w=1, n='main_trans' + Vsuffix).group
        
        self.gST = pm.group(em=1, w=1, n='scale_trans' + Vsuffix)
        self.gSNT = pm.group(em=1, w=1, n='scale_no_trans' + Vsuffix)
        pm.scaleConstraint(self.gST, self.gSNT, skip=['none', 'none', 'none'])
        
        self.gST.setParent(self.gMT)
        self.gSNT.setParent(self.gNT)
        
        self.gT|ctrlNull[0]
        self.gSNT.s >> mp.scaleGrp.s
        self.gNT|mp.no_trans
        self.gNT|mp.scaleGrp

class spineCtrls_bird():
    """
    TODO:
    Rebuild curve with 5 CVs.
    Pivot ctrls.
    """
    def __init__(self, Vlist=[], Vsuffix=''):
        
        self.sl = lsSl.sl(Vlist, 3, 0,
                  slDict={
                          'root':'0',
                          'base':'1',
                          'hip':'2',
                          'mid':'3',
                          'top':'4',
                          'chest':'5',
                          'curve_bind':'6',
                          'curve_up':'7'
                          },
                          Vrename=1, Vsuffix=Vsuffix)
        
        self.sl.root|self.sl.base|self.sl.hip
        self.sl.root|self.sl.mid
        self.sl.root|self.sl.top|self.sl.chest
        
        
        ctrlNull = tp.nullGroup(Vlist=[self.sl.root, self.sl.base, self.sl.mid, 
                            self.sl.top, self.sl.hip, self.sl.chest], 
                      Vhierarchy=3, Vprefix='null_')
        
        curveBind = tp.nullGroup(Vlist=[self.sl.base, self.sl.mid, self.sl.chest], 
                      Vhierarchy=4, Vtype=2, Vname='', Vprefix='bind_', Vsuffix='', 
                      Vnum=1, jointRadius=1.1, pivot=1, rot=1, connectInverseScale=0, 
                      freez=1)
        
        tc.spaceSwitch(Vlist=[self.sl.base, self.sl.top, ctrlNull[2], self.sl.mid],
                       VattrName=[], Vtype=3, 
                       Vsuffix='', VlastObj=1, Voffset=1, headlineAttr=1)
        
        skinCurve([self.sl.curve_bind] + curveBind)
        skinCurve([self.sl.curve_up] + curveBind)
        
        mp = spine([self.sl.curve_bind, self.sl.curve_up], Vsuffix + '_pt')
        
        self.gT = tp.trans(em=1, w=1, n='trans' + Vsuffix).group
        self.gNT = tp.noTrans(em=1, w=1, n='no_trans' + Vsuffix).group
        self.gMT = tp.mainTrans(em=1, w=1, n='main_trans' + Vsuffix).group
        
        self.gST = pm.group(em=1, w=1, n='scale_trans' + Vsuffix)
        self.gSNT = pm.group(em=1, w=1, n='scale_no_trans' + Vsuffix)
        pm.scaleConstraint(self.gST, self.gSNT, skip=['none', 'none', 'none'])
        
        self.gST.setParent(self.gMT)
        self.gSNT.setParent(self.gNT)
        
        self.gT|ctrlNull[0]
        self.gSNT.s >> mp.scaleGrp.s
        self.gNT|mp.no_trans
        self.gNT|mp.scaleGrp
   
class neckCtrls():
    """
    TODO:
    Rebuild curve with 5 CVs.
    Pivot ctrls.
    """
    def __init__(self, Vlist=[], Vsuffix=''):
        
        self.sl = lsSl.sl(Vlist, 3, 0,
                  slDict={
                          'base':'0',
                          'mid':'1',
                          'top':'2',
                          'head':'3',
                          'curve_bind':'4',
                          'curve_up':'5'
                          },
                          Vrename=1, Vsuffix=Vsuffix)
        
       
        self.sl.base|self.sl.mid
        self.sl.base|self.sl.top|self.sl.head
        
        
        ctrlNull = tp.nullGroup(Vlist=[self.sl.base, self.sl.mid, 
                            self.sl.top, self.sl.head], 
                      Vhierarchy=3, Vprefix='null_')
        
        curveBind = tp.nullGroup(Vlist=[self.sl.base, self.sl.mid, self.sl.top], 
                      Vhierarchy=4, Vtype=2, Vname='', Vprefix='bind_', Vsuffix='', 
                      Vnum=1, jointRadius=1.1, pivot=1, rot=1, connectInverseScale=0, 
                      freez=1)
        
        tc.spaceSwitch(Vlist=[self.sl.base, self.sl.top, ctrlNull[1], self.sl.mid],
                       VattrName=[], Vtype=3, 
                       Vsuffix='', VlastObj=1, Voffset=1, headlineAttr=1)
        
        #self.sl.mid.space.set(0.5)
        
        skinCurve([self.sl.curve_bind] + curveBind)
        skinCurve([self.sl.curve_up] + curveBind)
        
        mp = spine([self.sl.curve_bind, self.sl.curve_up])
        
        self.gT = tp.trans(em=1, w=1, n='trans' + Vsuffix).group
        self.gNT = tp.noTrans(em=1, w=1, n='no_trans' + Vsuffix).group
        self.gMT = tp.mainTrans(em=1, w=1, n='main_trans' + Vsuffix).group
        
        self.gST = pm.group(em=1, w=1, n='scale_trans' + Vsuffix)
        self.gSNT = pm.group(em=1, w=1, n='scale_no_trans' + Vsuffix)
        pm.scaleConstraint(self.gST, self.gSNT, skip=['none', 'none', 'none'])
        
        self.gST.setParent(self.gMT)
        self.gSNT.setParent(self.gNT)
        
        self.gT|ctrlNull[0]
        self.gSNT.s >> mp.scaleGrp.s
        self.gNT|mp.no_trans
        self.gNT|mp.scaleGrp
        
def skinCurve(Vlist=[], Vsuffix=''):
    sl = lsSl.sl(Vlist, 3, 0,
                  slDict={
                          'curve':'0',
                          'jointA':'1',
                          'jointB':'2',
                          'jointC':'3',
                          },
                          Vrename=0, Vsuffix=Vsuffix)
    cvs = sl.curve.getShape().cv
    print cvs
    for i, cmpon in enumerate(cvs):
        print cmpon
    
    skin = pm.skinCluster([sl.jointA, sl.jointB, sl.jointC], sl.curve, 
                           dr=1.5, toSelectedBones=True, 
                           ignoreHierarchy=True, mi=2)
    
    pm.skinPercent([skin, sl.curve.getShape()], pruneWeights=100, normalize=0)
    
    pm.skinPercent (skin, cvs[4], transformValue=[sl.jointA, 1.0], normalize=0)
    pm.skinPercent (skin, cvs[3], transformValue=[sl.jointA, 0.6], normalize=0)
    pm.skinPercent (skin, cvs[3], transformValue=[sl.jointB, 0.4], normalize=0)
    pm.skinPercent (skin, cvs[2], transformValue=[sl.jointB, 1.0], normalize=0)
    pm.skinPercent (skin, cvs[1], transformValue=[sl.jointB, 0.4], normalize=0)
    pm.skinPercent (skin, cvs[1], transformValue=[sl.jointC, 0.6], normalize=0)
    pm.skinPercent (skin, cvs[0], transformValue=[sl.jointC, 1.0], normalize=0)
    
         
            








        


