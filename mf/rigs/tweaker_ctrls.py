"""
Tweaker controlls for second skin cluster setup.
"""
import pymel.core as pm
import mf.tools.lsSl as lsSl
import mf.tools.joint as tj
import mf.tools.parent as tp
import mf.tools.snap as snp
import mf.tools.parent as tp
import mf.tools.curve

class makeTweakers():
    def __init__(self, Vlist=[], Vsuffix=''):
        Vlist = lsSl.lsSl(Vlist, 1, 0)

        for e in Vlist:
            self.jnts = []
            relatives = pm.listRelatives(e, c=1, ad=1, s=0)

            for r in relatives:
                if r.nodeType() == "joint":
                    self.jnts.append(r)

            dup = tj.duplicateChain([e])
            self.jnts.reverse()
            self.jnts.insert(0, e)
            print self.jnts
            print dup
            for i, j in enumerate(dup):
                j.rename(self.jnts[i].nodeName() + '_drv')
            tp.nullGroup(self.jnts + dup, Vprefix="null_", Vhierarchy=3, Vtype=2)

            for i, j in enumerate(dup):
                self.jnts[i].t >> j.t
                self.jnts[i].r >> j.r
                self.jnts[i].s >> j.s

            mf.tools.curve.controlShape(Vlist=self.jnts, radius=0.2)

    # def cleanup(self):
    #     for e in self.jnts
    #         e.





