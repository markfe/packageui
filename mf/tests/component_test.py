"""
tests for builder.component::

    import unittest   
    import mf.tests.component_test as ct
    reload(ct)
    
    help(unittest._TextTestResult)
    help(unittest.TestLoader)
    
    
    suite = unittest.TestLoader().loadTestsFromModule(ct)
    #suite = unittest.TestLoader().loadTestsFromTestCase(ct.componentWrongArglist)
    x = unittest.TextTestRunner(verbosity=2).run(suite)
        
        
    y =  runTests()
    z = y.failures
    for element in z[0]:
        print element
    z = y.errors
    z = y.wasSuccessful()
"""
import unittest
import pymel.core as pm
from mf.builder import component
import all as at

testPath = at.returnTestpath()

class componentWrongArglist(unittest.TestCase):
    
    def test_B(self):
        """component.UIpackageMake fails because function has no Vsuffix attribute"""
        pm.newFile(f=1)
        pm.select(pm.polyCube())
        component.UI()
        pm.textField ('builderTF', e=1, text='mf.tests.component_test.%s' % ('B'))
        self.assertRaises(component.wrongArgumentsError, component.UIpackageMake)
        pm.newFile(f=1)
        
    def test_C(self):
        """component.UIpackageMake fails because function has no Vlist attribute"""
        pm.newFile(f=1)
        pm.select(pm.polyCube())
        component.UI()
        pm.textField ('builderTF', e=1, text='mf.tests.component_test.%s' % ('C'))
        self.assertRaises(component.wrongArgumentsError, component.UIpackageMake)
        pm.newFile(f=1)
        
    def test_F(self):
        """component.UIpackageMake fails because function has no `*args` """
        pm.newFile(f=1)
        pm.select(pm.polyCube())
        component.UI()
        pm.textField ('builderTF', e=1, text='mf.tests.component_test.%s' % ('F'))
        self.assertRaises(component.wrongArgumentsError, component.UIpackageMake)
        pm.newFile(f=1)
        
    def test_H(self):
        """component.UIpackageMake fails with no input"""
        pm.newFile(f=1)
        pm.select(pm.polyCube())
        component.UI()
        pm.textField ('builderTF', e=1, text='mf.tests.component_test.%s' % ('H'))
        self.assertRaises(component.wrongArgumentsError, component.UIpackageMake)
        #pm.newFile(f=1)
        
    try:
        pm.deleteUI('packageUI')
    except: 
        pass        

def B(Vlist=''): pass

def C(Vsuffix=''): pass

def F(*args): pass

def H(): pass

class componentBuild(unittest.TestCase):
    
    def test_buildWorkingPackage(self):
        """component.packageImport succeeds to import a known package"""
        pm.newFile(f=1)
        self.assertFalse(component.packageImport(Vfolder=testPath + 'templates', Vfilenames=['eye.ma']))
        pm.newFile(f=1)
        
    def test_evalWorkingPackage(self):
        """component.eval succeeds to build a known package"""
        pm.newFile(f=1)
        component.packageImport(Vfolder=testPath + 'templates', Vfilenames=['eye.ma'])
        self.assertFalse(component.packageEvalList(packAttrName=['package']))
        pm.newFile(f=1)
    

