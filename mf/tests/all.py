"""
test suite
"""
import os
import unittest
import mf.builder.main as bm


def returnTestpath():
    """
    Path for all files needed for testing.
    """
    Vpath = bm.getRelativePath()
    for i in range(2):
        Vpath = os.path.split(Vpath)[0]
    Vpath = os.path.abspath(Vpath + '/test_data')
    
    return Vpath + '/'
    

def run(verbosity=2):
    """
    Runs all tests.
  
    Run this from the script editor::
    
        import mf.tests.all
        reload(mf.tests.all)
        mf.tests.all.run(2)
    """
    suite = unittest.TestLoader().loadTestsFromNames([
                                                      'mf.tests.skin_test', 
                                                      'mf.tests.component_test',
                                                      'mf.tests.anim_test',
                                                      'mf.tests.distance_test',
                                                      'mf.tests.bend_test',
                                                      'mf.tests.proxyGeo_test'
                                                      ])
    return unittest.TextTestRunner(verbosity=verbosity).run(suite)