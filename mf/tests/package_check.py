"""
Subjective tests for packages.
works with a drop-in folder structure.
All files needed for checking are in one folder to easily replace whole suite
or relink.
Structure is the same as for building rigs and must always be updated to changes
(all tests work only with maya ascii files (.ma))

Limitations:
    for every check there can be only one mesh, and one cam

TODO:
    Export cam with cam transform selected.
    Try to strip namespace for all animation when reexporting changed anim.
    Simplify namespace handling.
    Option to revert to version in incremental save.
"""
from __future__ import with_statement
import os
import pymel.core as pm
from pymel.core.language import mel
from mf.builder import component as cp
from mf.tools import file as tf
import mf.tools.skin as ts
import mf.tools.anim as ta 
import mf.tools.general as gt
from mf.tools import lsSl


def UI():
    """
    UI for exporting:
        - mesh
        - weights
        - anim
        - cam
    """
    try: pm.deleteUI('exportChckUI')
    except:pass

    with pm.window('exportChckUI', title='check UI', h=10, w=10, 
                   resizeToFitChildren=1, s=0, toolbox=1) as win:
        
        with pm.columnLayout('op_main', adjustableColumn=1, h=1):
            pm.window('exportChckUI', e=1, h=10, w=10)
            
            with pm.menuBarLayout():
                with pm.menu( label='import', tearOff=True ):
                    pm.menuItem( label='mesh', 
                                 c='import mf.tests.package_check as pc; pc.UIimportRun(part="mesh")')
                    pm.menuItem( label='weights', 
                                 c='import mf.tests.package_check as pc; pc.UIimportRun(part="weights")')
                    pm.menuItem( label='anim', 
                                 c='import mf.tests.package_check as pc; pc.UIimportRun(part="anim")')
                    pm.menuItem( label='new cam', 
                                 c='import mf.tests.package_check as pc; pc.importTempCam()')
                    
                with pm.menu( label='export', tearOff=True ):
                    pm.menuItem( label='mesh', 
                                 c='import mf.tests.package_check as pc; pc.UIexportRun(part="mesh")')
                    pm.menuItem( label='weights',
                                 c='import mf.tests.package_check as pc; pc.UIexportRun(part="weights")')
                    pm.menuItem( label='anim',
                                 c='import mf.tests.package_check as pc; pc.UIexportRun(part="anim")')
                    #pm.menuItem( divider=True )
                    pm.menuItem( label='cam',
                                 c='import mf.tests.package_check as pc; pc.UIexportRun(part="cam")') 
    
            pm.separator(h=15, st='none')        
            pm.checkBox ('incrCB', v=1, al='left', l='incremental save')              
            
            pm.checkBox ('meshOnlyCB', v=0, al='left', l='show mesh only')
            pm.button (l='check selected', w=60, en=1, 
                    c='import mf.tests.package_check as pc; pc.UIrunCheck()')
                   
            pm.separator(h=10)
            pm.button (l='show blast', w=60, en=1,
                    c='import mf.tests.package_check as pc; pc.UIshowBlast()')
            
    win.show()
    return
 

def UIimportRun(part=''):
    
    fileName = pm.textScrollList ('templateTSL', q=1, selectItem=1)
    Vpath = pm.textField('pathTF', q=1, text=1)
    
    if fileName == []:
        raise Exception ('no package selected')
    
    Vbasename = os.path.splitext(fileName[0])[0]
    pathes = returnPathes(Vpath)[1]

    if part == 'mesh':
        importMesh(pathes['mesh'], Vbasename)
        
    if part == 'weights':
        importWeights(Vlist=[], Vpath=os.path.join(pathes['weights'], Vbasename + '.txt'))
    
    if part == 'anim':
        importAnim(Vpath=pathes['anim'], Vfile=Vbasename)


def UIexportRun(part=''):
    
    fileName = pm.textScrollList ('templateTSL', q=1, selectItem=1)
    Vpath = pm.textField('pathTF', q=1, text=1)
    #incremental = pm.checkBox('incrCB', q=1, v=1)
    incremental = pm.menuItem('incrCB', cb=1, q=1)
    if fileName == []:
        raise Exception ('no package selected')
    
    Vbasename = os.path.splitext(fileName[0])[0]
    pathes = returnPathes(Vpath)[1]

    if part == 'mesh':
        exportMesh(pathes['mesh'], Vbasename, incremental)
    elif part == 'weights':
        exportWeights(pathes['weights'], Vbasename, incremental)
    elif part == 'anim':
        exportAnim(pathes['anim'], Vbasename, incremental)
    elif part == 'cam':
        exportCam(pathes['cam'], Vbasename, Vlist=[], incremental=incremental)

    pass

def UIshowBlast():
    """
    Opens the playblast for the selected template in the default application.
    """
    Vpath = pm.textField('pathTF', q=1, text=1)
    fileName = pm.textScrollList ('templateTSL', q=1, selectItem=1)[0]
    Vbasename = os.path.splitext(fileName)[0]
    (checkPath, pathes) = returnPathes(Vpath)
    
    os.startfile(pathes['blast']+'/'+Vbasename+'.avi')
    return

def importTempCam():
    """
    Import template camera.
    """
    Vpath = pm.textField('pathTF', q=1, text=1)
    pathes = returnPathes(Vpath)[1]
      
    print os.path.abspath(pathes['cam']+'/default_cam.ma')
    pm.importFile(os.path.abspath(pathes['cam']+'/default_cam.ma'), renameAll=1,
                renamingPrefix=('temp'))

    return


def exportMesh(Vpath, Vbasename, incremental=1):
    """
    plain export
    """
    pm.exportSelected(Vpath+'/'+Vbasename+'.ma', typ="mayaAscii", es=1, force=0,
               constructionHistory=0, constraints=0,
               channels=0, expressions=0,uiConfiguration=0, shader=1)
    print Vpath+'/'+Vbasename+'.ma'
    if incremental == 1:
        try:
            (source, copy) = tf.getVersion(Vpath, Vbasename+'.ma')
            tf.makeVersion(source, copy)
        except tf.noPathError: pass
    return
    
def exportWeights(Vpath, Vbasename, incremental=1):
    """
    plain export
    """
    ts.writeWeights(Vlist=[], Vpath=Vpath+'/' + Vbasename+'.txt', force=1)
    print Vpath+'/'+Vbasename+'.txt'
    if incremental == 1:
        try:
            (source, copy) = tf.getVersion(Vpath, Vbasename+'.txt')
            tf.makeVersion(source, copy)
        except tf.noPathError: pass
    return

def exportAnim(Vpath, Vbasename, incremental=1):
    """
    TODO: exclude the cameras animation if possible
    """
    ta.exportAnim(Vpath=Vpath+'/',
                  Vfile=Vbasename, force=1)
    if incremental == 1:
        try:
            (source, copy) = tf.getVersion(Vpath, Vbasename+'.ma')
            tf.makeVersion(source, copy)
        except tf.noPathError: pass
    
        try:
            (source, copy) = tf.getVersion(Vpath, Vbasename+'.txt')
            tf.makeVersion(source, copy)
        except tf.noPathError: pass
    pass

def exportCam(Vpath, Vbasename, Vlist=[], incremental=1):
    """
    plain export
    """
    Vlist = lsSl.lsSl(Vlist=Vlist, Vmin=1, Vmax=1)
    
    if Vlist[0].nodeType() == 'camera':
        pass
    elif Vlist[0].getShapes()[0].nodeType() == 'camera':
        pass
    else:
        raise Exception('selection is not a camera')

    pm.exportSelected(Vpath+'/'+Vbasename+'.ma', typ="mayaAscii", es=1, force=0,
           constructionHistory=0, constraints=0,
           channels=1, expressions=0,uiConfiguration=0, shader=0)
    if incremental == 1:
        try:
            (source, copy) = tf.getVersion(Vpath, Vbasename+'.ma')
            tf.makeVersion(source, copy)
        except tf.noPathError: pass
    return

def returnPathes(Vpath=''):
    """
    returns pathes for check
    """
    checkPath = os.path.abspath(Vpath + '/check')
    pathes = {'templates':(Vpath + '/templates'),
          'mesh':(checkPath + '/meshes'),
          'weights':(checkPath + '/weights'),
          'anim':(checkPath + '/animation'),
          'cam':(checkPath + '/cameras'),
          'blast':(checkPath + '/playblasts'),
          'built':(checkPath + '/built')}
    
    for c, p in pathes.iteritems():
        pathes[c] = os.path.abspath(p)
        
    return (checkPath, pathes)

def makeFolder(Vpath=''):
    """
    create filestructure for package_check
    """
    tf.checkPath(Vpath)
    (checkPath, pathes) = returnPathes(Vpath)
    try:
        if os.path.exists(checkPath) == 0:
            os.mkdir(checkPath)
            print 'new folder: '+checkPath
    except:
        pass
    for c, p in pathes.iteritems():
        try:
            if os.path.exists(p) == 0:
                os.mkdir(p)
                print 'new folder: '+p
        except:
            pass
    pass

def UIrunCheck():
    """
    passes values from the UI to runCheck
    """
    fileName = pm.textScrollList ('templateTSL', q=1, selectItem=1)
    Vpath = pm.textField('pathTF', q=1, text=1)
    #incremental = pm.checkBox('incrCB', q=1, v=1)
    #meshOnly = pm.checkBox('meshOnlyCB', q=1, v=1)
    incremental = pm.menuItem('incrCB', cb=1, q=1)
    meshOnly = pm.menuItem('meshOnlyCB', cb=1, q=1)
    
    suppress = pm.menuItem('suffixCheckCB', cb=1, q=1) 
    
    for i in range(len(fileName)):
        try:
            pm.newFile(f=1)
            runCheck(Vpath=Vpath, Vfilename=fileName[i], incremental=incremental,
                     meshOnly=meshOnly, suppressSuffixError=suppress)
            
            #pm.newFile(f=1)
        except Exception, detail:
            print type(detail)
            print detail
            print '%s not checked'%fileName[i]
            pass
    pass

def readCheck(package):
    """
    returns the existing files for a check
    """
    pass
    
def runCheck(Vpath='', Vfilename='', incremental=1, meshOnly=0, verbosity=1,
             suppressSuffixError=0):
    """
    runs the whole check resulting in a playblast
    """
    Vbasename = os.path.splitext(Vfilename)[0]

    pathes = returnPathes(Vpath)[1]
    info = ['---------------------------------------------------------------------------',
            'CHECKING: '+str(Vfilename)]
    exeptDetail = []
    mesh=''
    try:
        # build
        cp.packageImport(Vfolder=pathes['templates'], Vfilenames=[Vfilename])
        cp.packageEvalList(packAttrName=cp.returnPackageAttribute(), 
                            suppressSuffixError=suppressSuffixError, delCons=1)
        info.append('built')
        try:
            # import mesh 
            mesh = importMesh(Vpath=pathes['mesh'], Vfile=Vbasename)
            
            try:
                shaderName = 'CHECK_custom_def_material'
                Vshader = pm.shadingNode ('blinn', asShader=1, n=shaderName)
                VshadingGrp = pm.ls('initialShadingGroup')[0]
                Vshader.outColor >> VshadingGrp.surfaceShader
                Vshader.color.set([0.56, 0.50, 0.46])
            except Exception, detail:
                exeptDetail.extend(['exception during mesh:', str(type(detail)), str(detail), ''])
                pass

            info.append('..mesh')
            
            try:
                # import weights 
                importWeights(Vlist=[mesh[0]], 
                              Vpath=pathes['weights']+'/'+Vbasename+'.txt')
                info.append('....weights')
            except Exception, detail:
                # TODO: catch stack trace
                # traceback.print_tb(sys.exc_traceback)
                exeptDetail.extend(['exception during weights:', str(type(detail)), str(detail), ''])
                pass
        except:
            pass
        try:
            # import animation 
            importAnim(Vpath=pathes['anim'], Vfile=Vbasename)
            info.append('..anim')
        except Exception, detail:
            exeptDetail.extend(['exception during anim:', str(type(detail)), str(detail), ''])
            pass
        try:  
            # import camera 
            camOut = importCam(Vpath=pathes['cam'], Vfile=Vbasename)
            #print camOut
            info.append('..cam')
            
            try:
                #make playblast
                exportBlast(cam=camOut[0], Vpath=pathes['blast'],
                            Vfile=Vbasename, startFrame=camOut[1],
                            endFrame=camOut[2], incremental=incremental,
                            meshOnly=meshOnly, mesh=mesh)

                info.append('....blast')
            except Exception, detail:
                exeptDetail.extend(['exception during blast:', str(type(detail)), str(detail), ''])
                pass
        except Exception, detail:
            exeptDetail.extend(['exception during cam:', str(type(detail)), str(detail), ''])
            pass
        # save file
        pm.exportAll(pathes['built']+'/'+Vbasename+'.ma', type='mayaAscii', force=1)
        if incremental == 1:
            try:
                (source, copy) = tf.getVersion(pathes['built'], Vbasename+'.ma')
                tf.makeVersion(source, copy)
            except tf.noPathError: pass
        info.append('saved')

    except Exception, detail:
        exeptDetail.extend(['exception during build:', str(type(detail)), str(detail), ''])
        pass

    if verbosity == 1 and len(exeptDetail) > 0:
        print '---------------------------------------------------------------'
        print 'EXCEPTIONS WHILE CHECKING:'
        for line in exeptDetail:
            print line

    for line in info:
        print line

    return pathes

def importMesh(Vpath='', Vfile=''):
    """
    plain import
    """
    pm.importFile(os.path.abspath(Vpath+'/'+Vfile+'.ma'), renameAll=1, 
                  renamingPrefix=('CHECK_mesh_'+Vfile))
    meshes = pm.ls('CHECK_mesh_'+Vfile+'*')
    mesh = None
    
    for obj in meshes:
    
        if obj.nodeType() == 'transform':
            mesh = [obj]
        
    # layer--------------------------------------------------------------------
    mesh_dl = pm.createDisplayLayer (name="CHECK_mesh_dl" ,number=1 ,empty=1)
    pm.editDisplayLayerMembers(mesh_dl, mesh[0])
    mesh_dl.displayType.set(2)
    pm.select(mesh)
    return mesh

def importWeights(Vlist=[], Vpath=''):
    """
    For every joint in the weightlist thats not in the scene a dummy joint will 
    be created. 
    """
    Vlist = lsSl.lsSl(Vlist=Vlist, Vmin=1, Vmax=2)
    try:
        pm.skinCluster(Vlist[0].getShape(), e=1,  ub=1)
    except:
        pass
    ts.applyWeights(Vlist=Vlist, Vsuffix='CHECK', Vpath=Vpath)

    return

def importAnim(Vpath, Vfile):
    """
    plain
    """ 
    ta.importAnim(Vpath+'/', Vfile, deleteLoose=1, namespace='ANCHCK', 
                  useNamespace=0)

def importCam(Vpath='', Vfile=''):
    """
    Gets start and endframe from the camera.
    If there is no camera, imports the default_cam.
    """
    try:
        pm.importFile(Vpath+'/'+Vfile+'.ma', renameAll=1, 
                      renamingPrefix=('CHECK_cam_'+Vfile))
    except:
        pm.importFile(Vpath+'/'+'default_cam'+'.ma', renameAll=1, 
                      renamingPrefix=('CHECK_cam_'+Vfile))
        
    try:
        cam = pm.ls('CHECK_cam_'+Vfile+'*', type='camera')[0].getParent()
        try:
            startFrame = cam.start_frame.get()
            endFrame = cam.end_frame.get()
            pm.playbackOptions(minTime=startFrame, maxTime=endFrame)  
        except:
            print 'no framerange found'
            Exception('no framerange found')
            
        return [cam, startFrame, endFrame]
    except:
        print 'no camera found'
        Exception('no camera found' + 'CHECK_cam_' + Vfile)


def exportBlast(Vpath='', Vfile='', cam='', startFrame=0, endFrame=1, incremental=1, meshOnly=0, mesh=''):
    """
    incremental::
    
        name.###.avi
        '%04d' % 1
        # Result: 0001 #
    """
    pm.lookThru(cam)
    # I used mel because pmel does not return panel right in maya 2011
    activePanel = mel.eval('getPanel -withFocus')

    if meshOnly == 1 and mesh[0] != '':
        try:
            pm.isolateSelect(activePanel, state=1)
            pm.isolateSelect(activePanel, addDagObject=mesh[0])
        except: pass
    
       
    pm.modelEditor(activePanel, edit=1, 
                   displayAppearance='smoothShaded', 
                   wireframeOnShaded=1, 
                   activeOnly=0, 
                   joints=0)

    pm.playblast(fp=4, clearCache=1, showOrnaments=0, 
                 viewer=0, format='movie', percent=100,
                 filename=Vpath+'/'+Vfile, forceOverwrite=1,
                 widthHeight=(800, 600),compression='none',
                 endTime=endFrame,
                 startTime=startFrame)
    
    if incremental == 1:
        try:
            (source, copy) = tf.getVersion(Vpath, Vfile+'.avi')
            tf.makeVersion(source, copy)
        except tf.noPathError: pass

    pass
