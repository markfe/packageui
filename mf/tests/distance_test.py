""" 
test for tools.distance
"""
import pymel.core as pm
import unittest
from mf.tools import distance as td

class allMethods(unittest.TestCase):
    methods = (
               td.dist.line,
               td.dist.text, 
               td.dist.goals,
               td.dist.distance,
               td.dist.initial,
               #td.dist.parent,
               td.dist.getParent,
               td.dist.calibrate,
               td.dist.distance,
               td.dist.unCalibrate,
               td.dist.lock,
               #td.dist.connect,
               #td.dist.delete,
               )

    def testCreateEditCompare(self): 
        """created and edited instances of dist should have same output"""  
        pm.newFile(f=1)
        cA = pm.polyCube()
        cA[0].tx.set(5)
        cB = pm.polyCube()
        pm.select([cA[0], cB[0]])
        created = td.dist()
        
        pm.select(created()[1])
        edited = td.dist(Vlist=[created()[1]], edit=1)
        
        for method in self.methods:
            self.assertEqual(method(created), method(edited))
        pm.newFile(f=1)
 
    