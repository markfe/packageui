"""
test for tools.skin
"""
import pymel.core as pm
import unittest
from mf.tools import skin as tc

class getSkinBadInput(unittest.TestCase):

    def testNoCluster(self):
        """getSkinfromGeo fails with object that has no skin cluster"""
        Vcube = (pm.polyCube(w=1, h=1,d=1, sx=1, sy=1, sz=1, ch=0))
        pm.select(Vcube)
        result = tc.getSkinfromGeo(Vcube)
        self.assertEqual([], result)
        pm.delete(Vcube)
        #print (str(Vcube))
    
    