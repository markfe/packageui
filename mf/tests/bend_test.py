"""
test for tools.bend
"""
import pymel.core as pm
import unittest
from mf.tools import bend as tb

class allAttributes(unittest.TestCase):
    attributes = (
                  'curve',
                  'curveShape',
                  'handle',
                  'effector',
                  'jointChain'
                  )

    def testCreateEditCompare(self): 
        """created and edited instances of bend should have same output"""  
        pm.newFile(f=1)
        
        JointStart = pm.joint(p=[0,0,0])
        for i in range(10):
            pm.joint(p=[0,0,i+1])
        created = tb.splineIk(Vlist=[JointStart])
        
        handle = pm.ls(type='ikHandle')
        
        edited = tb.splineIk(Vlist=handle, edit=1)
        
        for attr in self.attributes:
            self.assertEqual(getattr(created, attr), getattr(edited, attr))
        pm.newFile(f=1)