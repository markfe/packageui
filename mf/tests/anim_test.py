""" 
test for tools.anim::

    import unittest   
    import mf.tests.anim_test as at
    reload(at)
    
    suite = unittest.TestLoader().loadTestsFromModule(at)
    x = unittest.TextTestRunner(verbosity=2).run(suite)
"""
import os
import pymel.core as pm
import unittest
from mf.tools import anim as ta
from mf.tools import file as ft
import all as at

testPath = at.returnTestpath()

class exportAnimBadInput(unittest.TestCase):
    
    def testBadPath(self):
        """exportAnim fails with a non existing path"""
        self.assertRaises(ft.noPathError, ta.exportAnim, testPath + 'nonExistingPath/', 'badPathTest')
   
    def testNoAnim(self):
        """exportAnim fails when there is nothing to export"""
        pm.newFile(f=1)
        self.assertRaises(ta.noCurvesError, ta.exportAnim, testPath + 'anim/', 'badNoAnimTest')
        
    def testDrivenKeys(self):
        """exportAnim fails with only driven keys"""
        pm.newFile(f=1)
        pm.importFile(testPath + 'testAnimDrvKeys.ma')
        self.assertRaises(ta.noCurvesError, ta.exportAnim,  testPath + 'anim/', 'testAnimDrvKeys')
        pm.newFile(f=1)
        
class importAnimBadInput(unittest.TestCase):
    
    def testBadPath(self):
        """importAnim fails with a non existing path"""
        self.assertRaises(ft.noPathError, ta.importAnim,  testPath + 'nonExistingPath')
       
    def testNoObjects(self):
        """importAnim fails with no objects to connect the animation curves"""
        pm.newFile(f=1)
        result = ta.importAnim(Vpath= testPath + 'anim/', Vfile='pCube1')
        pm.newFile(f=1)
        # result[2] are failed connections
        self.assertNotEqual([], result[2])
        
class sanityCheck(unittest.TestCase):
    
    def testReimportAnim(self):
        """importAnim passes with reimported animation"""
        pm.newFile(f=1)
        pm.importFile( testPath + 'testAnimSanityCheck.ma')
        ta.exportAnim( testPath + 'anim/', 'sanityCheck')
        pm.newFile(f=1)
        pm.importFile( testPath + 'testAnimSanityCheckNoAnim.ma')
        result = ta.importAnim( testPath + 'anim/', 'sanityCheck')
        pm.newFile(f=1)
        os.remove( testPath + 'anim/sanityCheck.ma')
        os.remove( testPath + 'anim/sanityCheck.txt')
        self.assertEqual([], result[2])
        
    def testReimportAnimRef(self):
        """importAnim passes reimporting animation from an object with namespace"""
        pm.newFile(f=1)
        pm.importFile( testPath + 'testAnimSanityCheckRef.ma')
        ta.exportAnim( testPath + 'anim/', 'sanityCheckRef')
        pm.newFile(f=1)
        pm.importFile(testPath + 'testAnimSanityCheckRefNoAnim.ma')
        result = ta.importAnim( testPath + 'anim/', 'sanityCheckRef')
        pm.newFile(f=1)
        os.remove( testPath + 'anim/sanityCheckRef.ma')
        os.remove( testPath + 'anim/sanityCheckRef.txt')
        self.assertEqual([], result[2])
        
        