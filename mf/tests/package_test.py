"""
tests for all packages that can be build with component.packageEval()::

    import unittest   
    import mf.tests.package_test as pt
    reload(pt)
    
    suite = unittest.TestLoader().loadTestsFromModule(pt)
    x = unittest.TextTestRunner(verbosity=2).run(suite)
"""
import unittest
import pymel.core as pm
from mf.builder import component
import all as at

testPath = at.returnTestpath()

class rot(unittest.TestCase):
    
    def test_startMidA(self):
        """building rot_startMid_01.ma"""
        pm.newFile(f=1)
        component.packageImport(Vfolder=testPath + 'templates', 
                                Vfilenames=['rot_startMid_01.ma'])
        
        self.assertFalse(component.packageEvalList(packAttrName=['package'], 
                         suppressSuffixError=0, delCons=1))
        pm.newFile(f=1)
    
    def test_startMidB(self):
        """building rot_startMid_02.ma"""
        pm.newFile(f=1)
        component.packageImport(Vfolder=testPath + 'templates', 
                                Vfilenames=['rot_startMid_02.ma'])
        
        self.assertFalse(component.packageEvalList(packAttrName=['package'], 
                         suppressSuffixError=0, delCons=1))
        pm.newFile(f=1)
        
    def test_startMidC(self):
        """building rot_startMid_03.ma"""
        pm.newFile(f=1)
        component.packageImport(Vfolder=testPath + 'templates',
                                Vfilenames=['rot_startMid_03.ma'])
        
        self.assertFalse(component.packageEvalList(packAttrName=['package'], 
                         suppressSuffixError=0, delCons=1))
        pm.newFile(f=1)
    