"""
list selection             
"""
import sys
import os
import pymel.core as pm

class lsSlError(Exception): pass
class wrongSelectionError(lsSlError): pass
class stopSelectionError(lsSlError): pass

    
def lsSl (Vlist=[], Vmin=0, Vmax=0, Vtype=['any'], VerrorStr='', returnPairs=0, returnString=0):
    """
    Checks if a list is empty. if so, fills it with the selection.
    
    Returns:
    [Vlist]
    
    =========== ================================================================
    Arguments   Description
    =========== ================================================================
    Vlist       List of objects.
    Vmin        Min number of objects allowed to be selected 
                (if it is 0 nothing needs to be selected).
    Vmax        Max number of objects allowed to be selected (0 is infinite).
    Vtype       List of types that have to be selected, 
                if there are more objects selected Vtype[-1] will be compared  
    VerrorStr   Additional information displayed after the error.
    returnPairs 1 = return the selection as pairs like: [[a,b],[c,d],[e]].
    =========== ================================================================
    
    Example:
    
    >>> Vlist = lsSl.lsSl(Vlist, 4, 4, ['joint', 'joint', 'joint', 'any'], 'select three joints and a ctrl to receive the attributes')
    """
    if Vmin > Vmax and Vmax !=0:
        raise wrongSelectionError ('min value is higher than max value')

    if len(Vtype)>Vmax and Vmax !=0:
        raise wrongSelectionError ('node-type-list is larger than max items allowed for selection')

    Vsize = (len(Vlist))
    
    if Vsize == 0:
        Vlist = pm.ls(sl=1)
        VsizeSl = (len(Vlist))
        if VsizeSl < 1 and Vmin !=0:
            raise wrongSelectionError ('Nothing selected. %s' % (VerrorStr))

        elif VsizeSl < Vmin:
            raise wrongSelectionError('Less than %s objects selected. %s' % (Vmin, VerrorStr))

        elif VsizeSl > Vmax and Vmax !=0:
            raise wrongSelectionError('More than %s objects selected. %s' % (Vmax, VerrorStr))
       
        #Test type only if list is from selection.-----------------------------
        if Vtype==[] or Vtype==['any']:
            Vtype = ['any']
        else:
            for i in range(len(Vlist)):
                VtypeOb = Vlist[i].nodeType()
                if i < len(Vtype):
                    if VtypeOb !=  Vtype[i] and Vtype[i] != 'any':
                        raise wrongSelectionError ('Selection %s is not a %s. %s' % ((i+1) , Vtype[i], VerrorStr))
        
                else:
                    if VtypeOb !=  Vtype[-1] and Vtype[-1] != 'any':
                        raise wrongSelectionError ('Selection %s is not a %s. %s' % ((i+1), Vtype[-1], VerrorStr))
            
    if returnPairs == 1:
        Vlist = listToPairs(Vlist)
    
    if returnString == 1:
        listStr = []
        for ob in Vlist:
            listStr.append(str(ob))
        return listStr
            
    return Vlist


def listToPairs(Vlist):
    """
    Given a list, returns a list with pairs like: [[a,b],[c,d],[e]]
    if the given list has a odd range the last item returned will be a single 
    instead of a pair
    """ 
    Vresult=[]
    Vrange=range(len(Vlist))
    
    for i in Vrange:
        Vrange.pop(0)
        Vresult.append(Vlist[i:i+2])
    return Vresult


class sl():
    """
    Class used to call objects from Vlist by their names.
    
    ========== ================================================================
    Arguments  Description
    ========== ================================================================
    slDict     Dictionary of attribute names an indexes for the selection list,
               that are passed as strings e.g.: {'handCtrl':'0', 'shoulderCtrl':'1'}
    printInfo  will print info about the component lists
    rename     all object will be renamed according to the slDict
    exit       will exit after printing the objects and their names.
    ========== ================================================================
    """
    def __init__(self, Vlist=[], Vmin=0, Vmax=0, slDict={}, printInfo=0, 
                 Vrename=0, Vsuffix='', exit=0, **kwdArgs):
        
        l = lsSl(Vlist=Vlist, Vmin=Vmin, Vmax=Vmax, **kwdArgs)
        self.Vlist = l
        
        for c, p in slDict.iteritems():
            
            if c == 'Vlist':
                raise lsSlError('Vlist is a reserved attribute name')
            self.__dict__[c] = eval('l[' + p + ']')
        
        if printInfo:
            self.printInfo(exit)
        
        if Vrename:
            self.rename(Vsuffix)
            
        self.finaly(Vsuffix, exit)
    
    def printInfo(self, exit):
        for c, p in self.__dict__.iteritems():
            print c, p
            if exit:
                print 'exiting python'
                sys.exit()
        
    def rename(self, Vsuffix):
        for c, p in self.__dict__.iteritems():
                if not c == 'Vlist':
                    self.name(c, p, Vsuffix)
                    
    def name(self, c, p, Vsuffix):
        """
        Renames the objects with a suffix using the the slDict names.
        """ 
        if type(p) != type([]):
            pm.rename(p, c + Vsuffix)
        
        if type(p) == type([]):
            
                for i in range(len(p)):
                    pm.rename(p[i], c + '_' + str(i+1) + Vsuffix)
    
    def finaly(self, Vsuffix, exit):
        pass


 
  
        