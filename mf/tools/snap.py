"""
positioning tools            
"""

from __future__ import with_statement
import pymel.core as pm
import lsSl

def snap(Vlist=[], Vmove=1):
    """
    Snaps a list of objects to the last object
  
    Returns: [translation, rotation] [[x, y, z],[x, y, z]]

    ========= =========================================
    Arguments Description
    ========= =========================================
    Vlist[]   list of objects
    Vmove     0 = objects will not be moved (just returning the x form)
    ========= =========================================
    """
    Vlist = lsSl.lsSl(Vlist, 2, 0)
    
    Vtrans = snapTrans(Vlist, Vmove)
    Vrot = snapRot(Vlist, Vmove)
    pm.select (Vlist[:-1])
    
    return [Vtrans,Vrot]

def snapTrans(Vlist=[], Vmove=1):
    """
    Moves a list of objects to the last object.
    Returns: translation [x, y, z]
    """
    Vlist = lsSl.lsSl(Vlist, 2, 0) 
    Vtrans = pm.xform (Vlist[-1], q=1, ws=1, rp=1)
    
    if Vmove == 1:
        for o in Vlist[:-1]:
            pm.move (o, Vtrans, rpr=1)
    pm.select (Vlist[:-1])
    
    return Vtrans

def snapRot(Vlist=[], Vmove=1):
    """
    Rotates a list of objects to the last object.
    Returns: rotation [x, y, z]
    """
    Vlist = lsSl.lsSl(Vlist, 2, 0)
    Vrot = pm.xform(Vlist[-1], q=1, ws=1, rotation=1)
    goalOrder = pm.xform (Vlist[-1], q=1, roo=1)
    
    if Vmove == 1:
        
        for o in Vlist[:-1]:
            sourceOrder = pm.xform(o, q=1, roo=1)
    
            if not goalOrder == sourceOrder:
                pm.xform (o, ws=1, rotation=Vrot, roo=goalOrder, p=1)
                pm.xform (o, ws=1, roo=sourceOrder, p=1)
            
            else:
                pm.xform (o, ws=1, rotation=Vrot)
    
    pm.select (Vlist[:-1])
    
    return Vrot

def unfreeze(Vlist=[]):
    """
    Sets the pivots translate [0, 0, 0] to the world (or parent) origin.
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    
    for obj in Vlist:
        par = obj.getParent()
        
        Vtrans = pm.xform (obj, q=1, ws=1, rp=1)
        
        parTrans = [0, 0, 0]
        if par:
            parTrans =  pm.xform (par, q=1, ws=1, rp=1)
        
        pm.move (obj, parTrans, rpr=1)
        pm.makeIdentity(obj, apply=True, t=1, r=0, s=0, n=0)
        pm.move (obj, Vtrans, rpr=1)
        
def transToPiv(Vlist=[]):
    """
    Pastes the translation values of the last object to the rotate and 
    scale pivot values of all other objects.
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    
    for o in Vlist[:-1]:
        o.rotatePivot.set(Vlist[-1].t.get())
        o.scalePivot.set(Vlist[-1].t.get())
    
def snapStill(Vlist=[]):
    """
    Snaps a list of objects to the last object without rotating the shape.
    """
    Vlist = lsSl.lsSl(Vlist, 2, 0)
    for o in Vlist[:-1]:
        tmp_par = pm.group(w=1, em=1)
        snap([tmp_par, Vlist[-1]])
        par = o.getParent()
        tmp_par|o
        
        pm.makeIdentity(o, apply=True, t=0, r=1, s=0, n=0)
        
        if par != None:
            par|o
        else:
            pm.parent (o, w=1, absolute=1)
        
        pm.delete(tmp_par)

def rotOrderStill(Vlist=[], rotOrder='xyz'):
    """
    Sets the rotation order without rotating the object.
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    for o in Vlist:
        pm.xform (o, ws=1, roo=rotOrder, p=1)
    
def snapUI():
    
    try:
        pm.deleteUI('snapUI')
    except: pass
    
    with pm.window('snapUI', resizeToFitChildren=1, toolbox=1, s=0) as win:
        with pm.menuBarLayout('ererer'):
                with pm.menu( label='rotation Order', tearOff=True ):
                    pm.menuItem( label='xyz',
                                  c='import mf.tools.snap; mf.tools.snap.rotOrderStill([], "xyz")')
                    pm.menuItem( label='yzx',
                                  c='import mf.tools.snap; mf.tools.snap.rotOrderStill([], "yzx")')
                    pm.menuItem( label='zxy',
                                  c='import mf.tools.snap; mf.tools.snap.rotOrderStill([], "zxy")')
                    pm.menuItem( label='xzy',
                                  c='import mf.tools.snap; mf.tools.snap.rotOrderStill([], "xzy")')
                    pm.menuItem( label='yxz',
                                  c='import mf.tools.snap; mf.tools.snap.rotOrderStill([], "yxz")')
                    pm.menuItem( label='zyx',
                                  c='import mf.tools.snap; mf.tools.snap.rotOrderStill([], "zyx")')
        
        with pm.columnLayout():
            pm.button( l='translation', w=110, c= 'import mf.tools.snap; mf.tools.snap.snapTrans()')
            pm.button( l='rotation', w=110, c= 'import mf.tools.snap; mf.tools.snap.snapRot()')
            pm.button( l='scale', w=110, c= 'import mf.tools.snap; mf.tools.snap.snapScale()')
            pm.button( l='both', w=110, c= 'import mf.tools.snap; mf.tools.snap.snap()')
            
            pm.separator(h=5)
            pm.button( l='rotation still ', w=110, c= 'import mf.tools.snap; mf.tools.snap.snapStill()')
            pm.button( l='unfreez trans', w=110, c= 'import mf.tools.snap; mf.tools.snap.unfreeze()')
            
         
    win.show()
    
def snapScale(Vlist=[], calibrate=[1, 1, 1], setSourceAttr=1, attrList=['sx', 'sy', 'sz']):
    """
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    wsg = pm.xform (Vlist[-1], ws=1, s=1, q=1)
    x = wsg[0] * (1/calibrate[0])
    y = wsg[1] * (1/calibrate[1])
    z = wsg[2] * (1/calibrate[2])
    
    for o in Vlist[:-1]: 
        if setSourceAttr: 
            if not attrList:  
                o.s.set(x, y, z)
            else:
                for i, e in enumerate(attrList):
                    getattr(o, e).set([x,y,z][i])
    
    return (x, y, z)    
    

def snapOffset(Vlist=[], attrList=['sx', 'sy', 'sz']):
    """
    Snaps two object with an offset.
    [offsetObject, goal, source]
    """
    Vlist = lsSl.lsSl(Vlist, 3, 3)
    locA = pm.spaceLocator() 
    locB = pm.spaceLocator()
    
    #Set scale to the offsets scaleX, before measuring the offset.
    for e in attrList:
        getattr(Vlist[1], e).set(Vlist[0].sx.get())
    
    
    snap([locA, Vlist[0]])
    snap([locB, Vlist[1]])
    
    #locA.s.set(Vlist[2].s.get())
    locA|locB
    locB.s.set(Vlist[0].s.get())
    
    snap([locA, Vlist[2]])
    snapScale([locA, Vlist[2]])
    
    snap([Vlist[1], locB])
    snapScale([Vlist[1], locB], attrList=attrList)
    pm.delete([locA, locB])
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
