"""
tools for proxy geometry
"""
import pymel.core as pm

import mf.builder.nodes 
import lsSl
import snap
import distance as ds
import shading as ts
import mf.tools.parent as tp

class bindJoint():
    """
    Creates a custom bind joint node.
    """
    def __init__(self, Vlist=[], Vsuffix='', edit=0, **kwdArgs):
        
        if edit:
            self.edit(Vlist=Vlist)   
        else:
            self.create(**kwdArgs)

    def create(self, Vlist=[], **kwdArgs):
        """creates the joint"""
        pm.select(cl=1)
        self.bj = pm.joint(**kwdArgs)
        
        self.bj.addAttr(mf.builder.nodes.register('joint_bind')[0])    
        
        self.bj.segmentScaleCompensate.set(0)
        self.bj.segmentScaleCompensate.setLocked(1)
        
        self.makeProxy()
        self.makeShape()
    
        pm.select(self.bj)
          
    def edit(self, Vlist=[]):
        """finds the joint"""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.bj = Vlist[0]
        self.proxy = ''
        self.shape = ''
        
        children = self.bj.getChildren()
        
        if children != []:
            self.proxy = children[0]
            shape = self.proxy.getShape()
            if shape:
                self.shape = shape
                
    def makeProxy(self):
        """creates the proxy group"""
        self.proxy = tp.nullGroup(Vlist=[self.bj], Vhierarchy=4, Vtype=1, 
                                      Vsuffix='_px')[0]
        self.proxy.addAttr('center_pivot', keyable=0)
        pm.setAttr(self.proxy.center_pivot, keyable=0, channelBox=1)
             
    def makeShape(self, Vlist=[], sides=6, centerPivot=0, custom=0, maintainPivot=1):
        """Creates the shape."""
        if not self.proxy:
            self.makeProxy()             
        
        shape = self.proxy.getShape()
            
        if shape:
            pm.delete(shape)
            if maintainPivot:
                centerPivot = self.proxy.center_pivot.get()
         
        if custom:
            Vlist = lsSl.lsSl(Vlist, 1, 1)
            geo = pm.duplicate(Vlist[0])[0]
        else: 
            if sides == 4:
                geo = pm.polyCube(w=1, h=1, d=1)[0]    
            else: 
                geo = pm.polyCylinder (r=1, h=1, sx=sides, sy=0, sz=0, ax=[0, 1, 0], 
                                    rcp=0, cuv=3, ch=0, createUVs=0)[0]
            geo.rz.set(90)
            
            if centerPivot == 0:
                geo.tx.set(0.5)
                
            pm.makeIdentity (geo, apply=1, t=1, r=1, s=1, n=0)
            
        self.shape = geo.getShape()
        pm.parent(self.shape, self.proxy, shape=1, r=1)
        pm.rename(self.shape, str(self.proxy)+'Shape')
        pm.delete(geo)
        
        self.proxy.center_pivot.set(centerPivot)
        pm.select(self.proxy)
                
    def setLabel(self, side=3, label=''):
        pm.setAttr(str(self.bj)+'.type',18)
        self.bj.otherType.set(label)
        self.bj.side.set(side)      


class proxyGeo():
    """
    Creates:
        a proxy geometry for each object but the last. each proxy can be 
        oriented to the next object.  
   
    Returns: 
        list of the proxy geometry
    
    =============== ===========================================================
    Arguments       Description
    =============== ===========================================================
    Vlist[]         list of objects 
    Vsuffix         suffix  
    Vorient         Aim proxy to the next object in the list, if value 
                    is 0 it will be oriented like the parent.  
    xScale          x scale of the proxy  
    yzScale         yz scale of the proxy
    yzScaleGlobal   value for scaling the geometry in yz multiplied with the 
                    overall length
    VcustomGeo      use the last selected object as custom geometry 
    Vsides          sides of the default geometry 
    VcenterPivot    center pivot, default pivot it at x0
    chain           if false the last object will also receive a geometry.
    side            0=center, 1=left, 2=right, 3=none 
    =============== ===========================================================
    """
    def __init__(self, Vlist=[], Vprefix='', Vname='', Vsuffix='_bnd', Vorient=1, 
                 xScale=1, yzScale=0, yzScaleGlobal=1, customGeo=0, Vsides=6, 
                 VcenterPivot=0, chain=1, shader=0, Vcolor=[0.4, 0.4, 0.6], 
                 shaderName='shader', smoothLevel=0, side=3, label=''):
        
        Vlist = lsSl.lsSl(Vlist, 1+chain+customGeo, 0)
        custGeo = ''
        newCustomGeo = ''
        if customGeo:
            custGeo = Vlist.pop()
        
        self.pxNodeLs = []
        self.length = 0
        
        for i in range(len(Vlist)-chain):
            
            if Vname == '':
                VnewName = '%s' % (Vlist[i])
            else:
                VnewName = '%s' % (Vname)
            
            if custGeo:
                newCustomGeo = pm.duplicate(custGeo)[0]
                
            pm.select(cl=1)           
            self.pxNode = bindJoint()
            pm.rename(self.pxNode.bj, (Vprefix + VnewName + Vsuffix))
            pm.rename(self.pxNode.proxy, (Vprefix + VnewName + Vsuffix + '_px'))
            #self.pxNode.build()
            self.pxNode.makeShape(Vlist=[newCustomGeo], sides=Vsides, 
                          centerPivot=VcenterPivot, custom=customGeo,
                          maintainPivot=0)
            self.pxNode.setLabel(side, label+str(i))
            self.pxNode.proxy
            self.pxNode.proxy.smoothLevel.set(smoothLevel)
            
            # chain -----------------------------------------------------------
            if chain == 1:
                self.length = (self.length + ds.get([Vlist[i], Vlist[i+1]]))
                
                # add all distances
                Vdist = ds.distance(Vlist=[Vlist[i], Vlist[i+1]], Vpermanent=1)
                
                snap.snap([self.pxNode.bj, Vdist[2]])
                pm.parent (self.pxNode.bj, Vlist[i])
                pm.makeIdentity (self.pxNode.bj, apply=1, t=0, r=1, s=0, n=0)
            
                pm.makeIdentity (self.pxNode.proxy, apply=1, t=0, r=0, s=1, n=0)
                self.pxNode.proxy.sx.set(Vdist[0]*xScale)
                
                pm.delete(Vdist[2])
            
            # no chain --------------------------------------------------------
            else:
                self.length = 1
                snap.snap([self.pxNode.bj, Vlist[i]])
                pm.parent (self.pxNode.bj, Vlist[i])
                self.pxNode.proxy.sx.set(xScale)
            
            if xScale == 0:
                self.pxNode.proxy.sx.set(1)
    
            if yzScale == 0:
                self.pxNode.proxy.sy.set(1)
                self.pxNode.proxy.sz.set(1)
            
            if Vorient == 0:
                snap.snapRot([self.pxNode.proxy, Vlist[i]])
            
            #self.pxNode.bj.segmentScaleCompensate.set(0)        
            self.pxNodeLs.append(self.pxNode)
            
        if customGeo == 1:
            pm.delete(custGeo)
         
        if yzScaleGlobal != 0:
            self.calibrate(yzScaleGlobal, skipX=chain)    
            
        self.pxObLs = []
        for c in self.pxNodeLs:
            self.pxObLs.append(c.bj)
            
        if shader == 1:
            ts.proxyShader(Vlist=self.pxObLs, Vcolor=Vcolor, shaderName=shaderName)
                               
        pm.select (self.pxObLs)
        
    def calibrate(self, value=1, skipX=1):
        """calibrates the proxys y and z scale to a value"""
        for px in self.pxNodeLs:    
            px.proxy.sy.set(self.length*value)
            px.proxy.sz.set(self.length*value)
            if skipX == 0:
                px.proxy.sx.set(self.length*value*px.proxy.sx.get())
                        
    def __call__(self):               
        return self.pxNodeLs   
    





    