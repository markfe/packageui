import pymel.core as pm
import mf.tools.lsSl as lsSl


class UI():
    def __init__(self):
        UIname = 'linearise'
        try:
            pm.deleteUI(UIname)
        except:
            pass
        pm.window(UIname, t=UIname, h=200, w=100, s=1)
        self.mainFormA = pm.formLayout(h=160)

        pm.button(l='Linearise All', w=260, c=self.lineariseAll)
        pm.button(l='Linearise Selected', w=60, c=self.lineariseSelected)
        pm.button(l='Add to "LINEARISED_MATERIALS" set', w=60, c=self.makeSet)
        pm.button(l='Remove from "LINEARISED_MATERIALS" set', w=60, c=self.removeFromSet)
        self.mainFormA.redistribute()
        pm.showWindow(UIname)

    def lineariseAll(self, *args):
        sel = pm.ls(sl=1)
        pm.select(cl=1)
        c = Linearise()
        c.getMaterials()
        c.getNonlinearAttributes()
        c.linearise()
        pm.select(sel)

    def lineariseSelected(self, *args):
        sel = pm.ls(sl=1)
        c = Linearise()
        c.getMaterials()
        c.getNonlinearAttributes()
        c.linearise()
        pm.select(sel)

    def makeSet(self, *args):
        c = Linearise()
        c.getMaterials()
        for e in c.materials:
            c.selectionSetAdd(e)

    def removeFromSet(self, *args):
        c = Linearise()
        #c.getMaterials()
        sel = pm.ls(sl=1)
        for e in sel:
            c.selectionSetRemove(e)


class Linearise():
    def __init__(self):
        """
        Not linearising the "additional_color" gives a more
        matching overall color, but adds some glow to the
        dark areas.
        """
        self.attributesCompound = [
                                # lambert / blinn
                                'color',
                                'ambiantColor',
                                'incandescence',
                                'specularColor',
                                'reflectedColor',
                                # surfaceShader
                                'outColor',
                                # sss
                                'overall_color',
                                'diffuse_color',
                                'front_sss_color',
                                'mid_sss_color',
                                'back_sss_color',
                                'specular',
                                # mia material
                                'diffuse',
                                'refl_color',
                                'refr_color',
                                'refr_trans_color',
                                'ao_dark',
                                'ao_ambiant',

                                'additional_color',

                                # unknown
                                #'irradiance',
                                #'irradianceColor',
                                #'ambient'
                                ]

        self.atributesSingle=['reflectivity']

        self.excludedRenderLayers=[
                                   'Z',
                                   'win_nrm',
                                   'Z_win',
                                   'win_Z',
                                   'track'
                                  ]

    def getMaterials(self, Vlist=[]):
        """
        Finds all materials, excluding defaults.
        """
        sl = lsSl.lsSl(Vlist, 0, 0)
        matLin = None

        if pm.objExists('LINEARISED_MATERIALS'):
            linSet = pm.ls('LINEARISED_MATERIALS')[0]
            matLin = pm.sets(linSet, q=1)

        nonDefaultMat = []
        self.materials = []
        materials = []
        allMaterials = pm.ls(materials=1)
        for e in allMaterials:
            if not e.isDefaultNode():
                nonDefaultMat.append(e)

        if not sl:
            materials = nonDefaultMat
        else:
            for e in sl:
                if e in nonDefaultMat:
                    materials.append(e)
            if not materials:
                raise Exception('No materials in selection.')

        if matLin:
            for e in materials:
                if not e in matLin:
                    self.materials.append(e)
        else:
            self.materials = materials

        if not self.materials:
            raise Exception('No materials found to linearise.')
        else:
            for m in self.materials:
                if self.checkExcluded(m):
                    self.materials.remove(m)

    def checkExcluded(self, material):
        """
        Returns true if a shader is excluded.
        """
        excludedRenderLayersUpper = [e.upper() for e in self.excludedRenderLayers]

        if material.hasAttr('outColor'):
            shaderOut = material.outColor.outputs()
        elif material.hasAttr('message'):
            shaderOut = material.message.outputs()
        else:
            return False
        sg = None
        if shaderOut:
            for e in shaderOut:
                if e.nodeType() == 'shadingEngine':
                    sg = e

        if sg:
            sgOutputs = sg.message.outputs(plugs=1)
            for e in sgOutputs:
                    if e.longName() == 'shadingGroupOverride':
                        if e.node().nodeName().upper() in excludedRenderLayersUpper:
                            print material, 'excluded (' + e.node().nodeName() + ')'
                            return True

            sgInputs = sg.inputs(plugs=1)
            for e in sgInputs:
                if e.node().nodeName().upper() in excludedRenderLayersUpper:
                    print material, 'excluded (' + e.node().nodeName() + ')'
                    return True
        return False


    def getNonlinearAttributes(self):
        """
        Attribute names of an compound attribute can clash with names of a
        single slot attribute.
        a compound attribute can be found up to four times. One time as
        compound and up to three times as single attribute e.g.:
        color
        colorR
        colorG
        colorB
        """
        self.attributes = []
        for mat in self.materials:
            if pm.nodeType(mat) == 'rampShader':
                a = pm.ls(mat.name()+'.color')[0]

                for e in a:
                    an = e.name()
                    c = pm.ls(an+".color_Color")
                    if c:
                        self.attributes.append([c[0], False, 1])

            else:
                ratio = 1
                allAttibutes = pm.listAttr(mat)
                for a in allAttibutes:


                    if a =='diffuse':
                        if mat.hasAttr('diffuse_weight'):
                            ratio = mat.getAttr('diffuse_weight')

                    if a =='additional_color':
                        if mat.hasAttr('diffuse_weight'):
                            ratio = 1-mat.getAttr('diffuse_weight')


                    if a in self.attributesCompound:
                        attr = getattr(mat, a)
                        if attr.isCompound():
                            self.attributes.append([attr, False, ratio])

                    if a in self.atributesSingle:
                        attr = getattr(mat, a)
                        if not attr.isCompound():
                            self.attributes.append([attr, True, ratio])



    def linearise(self):
            self.connections = []
            for e in self.attributes:
                self.connections.append(connectionAttr(e[0], e[1], e[2]))
                self.selectionSetAdd(e[0].node())

    def selectionSetAdd(self, node):
        linSet = self.selectionSetGet()
        pm.sets(linSet, e=1, add=node)

    def selectionSetRemove(self, node):
        linSet = self.selectionSetGet()
        pm.sets(linSet, e=1, remove=node)

    def selectionSetGet(self):
        if not pm.objExists('LINEARISED_MATERIALS'):
            linSet = pm.sets(n='LINEARISED_MATERIALS', em=1)
            pm.lockNode(linSet, l=1)
        else:
            linSet = pm.ls('LINEARISED_MATERIALS')[0]
        return linSet



class connectionAttr():
    """
    This class treats a single or compound attribute.
    """
    def __init__(self, attr, isSingle=None, ratio=1):
        """
        """
        print attr
        self.isSingle = isSingle
        self.ratio = ratio
        self.goal = attr
        self.get()
        self.linearise()

    def get(self):
        self.allSingle = []

        if self.goal.isCompound():
            ar = pm.ls(self.goal.name() + 'R')
            ag = pm.ls(self.goal.name() + 'G')
            ab = pm.ls(self.goal.name() + 'B')
            if ar:
                self.allSingle.append(ar[0])
            if ag:
                self.allSingle.append(ag[0])
            if ab:
                self.allSingle.append(ab[0])
        else:
            self.allSingle.append(self.goal)

    def linearise(self):
        plugs = []
        correct = []
        connectedCompund = False
        hasPlugs = False
        for e in self.allSingle:
            if e.inputs(plugs=1):
                plugs.append([e.inputs(plugs=1)[0], e])
                hasPlugs = True
            else:
                plugs.append(None)
                correct.append(e)

        multC = self.goal.inputs(plugs=1)
        if multC:
            connectedCompund = multC[0]

        if connectedCompund:
            gamma = self.createGammaNode()

            if not self.isSingle:
                connectedCompund >> gamma.value
                gamma.outValue >> self.goal
            else:
                connectedCompund >> gamma.valueX
                gamma.outValueX >> self.goal

        else:
            if hasPlugs:
                gamma = self.createGammaNode()

                if plugs[0]:
                    plugs[0][0] >> gamma.valueX
                    gamma.outValueX >> plugs[0][1]
                if plugs[1]:
                    plugs[1][0] >> gamma.valueY
                    gamma.outValueY >> plugs[1][1]
                if plugs[2]:
                    plugs[2][0] >> gamma.valueZ
                    gamma.outValueZ >> plugs[2][1]

        if not connectedCompund:
            for e in correct:
                clr = e.get()
                e.setLocked(0)
                if clr > 0:
                    linclr = pow(clr, 2.2)
                    result = blend(clr, linclr, self.ratio)

                    e.set(result)
                elif clr < 0:
                    pm.warning('Negative could not be gamma corrected: ' + e.name())


    def createGammaNode(self):
        gamma = pm.shadingNode('gammaCorrect', n='linearize_gammaCorrect_0', asUtility=1)

        gammaVal = blend(1, 0.45, self.ratio)

        gamma.gammaX.set(gammaVal)
        gamma.gammaY.set(gammaVal)
        gamma.gammaZ.set(gammaVal)
        return gamma

def blend(a,b,ratio):
    """0 -- 1 """
    y=1-ratio
    return a*y+b*ratio

















