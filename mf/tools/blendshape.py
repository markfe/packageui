"""
general tools for blendshapes
"""
import pymel.core as pm
from maya import mel
from mf.tools import lsSl
from mf.tools import key as tk 
from mf.tools import general as gt
import mf.tools.snap as snap
    

class mirror():
    """
    Class for mirroring shapes.
    """
    def __init__(self, Vlist=[], Vsuffix='',rename=['', '']):
        
        self.sl = lsSl.sl(Vlist, 1, 0, Vtype=['transform'], 
                          slDict={'bs':':-1',
                                  'base':'-1'})
        self.shapes = []
        for e in self.sl.bs:
    
            dub = pm.duplicate(self.sl.base)[0]
            newBase = pm.duplicate(self.sl.base)[0]
            
            blendSh = pm.blendShape (newBase)
            pm.blendShape (blendSh[0], e=1, t=[newBase, 0, e, 1])
            pm.blendShape (newBase, e=1, weight=[0,0])
            
            inv = pm.group(em=1, w=1)
            snap.snap([inv,newBase])
            inv|newBase
            inv.sx.set(-1)
            
            pm.select([dub, newBase])
            #mel.eval('CreateWrap')
            mel.eval('doWrapArgList "2" { "1","0.001","1" }')
            pm.blendShape (newBase, e=1, weight=[0,1])
            
            new = pm.duplicate(dub)[0]
            try:
                new.rename(str(e).replace('_L', '_R'))
            except:
                try:
                    new.rename(str(e).replace('_R', '_L'))
                except:
                    pass
                
            
            if not rename[0] == '':
                new.rename(str(e).replace(rename[0], rename[1]))
            pm.delete(dub, newBase, inv)
            
            snap.snap([new, e])
        
            self.shapes.append(new)
            
        for e in self.shapes:
            gt.deleteUnusedShapes(e.getShapes(), printInfo=0) 

class makeSymmetrc():
    """
    Makes a mesh symmetric.
       
    ========== =================================================
    Arguments  Description
    ========== =================================================
    Vlist[:]   Meshes
    side       1=left, 2=right
    ========== =================================================
    """
    def __init__(self, Vlist=[], Vsuffix='', side=1):
        
        self.sl = lsSl.sl(Vlist, 1, 0, Vtype=['transform'], 
                          slDict={'base':'-1',
                                  'bs':':-1'})
        
        self.result=[]
        self.side=side
        
        for e in self.sl.bs:
            
            newBase = pm.duplicate(self.sl.base, n='newBase')[0] 
            sym = pm.duplicate(self.sl.base, n=str(e + '_sym'))[0]
            self.findMiddle(newBase)
            self.splitMesh(e, newBase)
            pm.delete(newBase, ch=1)
            
            m = mirror([newBase, self.sl.base])
           
            blendSh = pm.blendShape (sym)
            pm.blendShape (blendSh[0], e=1, t=[sym, 0, newBase, 1])
            pm.blendShape (blendSh[0], e=1, t=[sym, 1, m.shapes[0], 1])
            pm.blendShape (newBase, e=1, weight=[0,1])  
            pm.blendShape (newBase, e=1, weight=[1,1])  
            pm.delete(sym, ch=1)
            
            pm.delete(m.shapes[0])
            pm.delete(newBase)
            
            for s in [sym]:
                gt.deleteUnusedShapes(s.getShapes(), printInfo=0) 
            snap.snap([sym, e])
            
            self.result.append(sym)
        pm.select(self.result)
        
    def findMiddle(self, base): 
        ran = 0.001
        self.middleVert = []
        self.sideVert = []
        for i, v in enumerate(base.vtx):
            pos = v.getPosition()[0]
            if pos < ran and pos > -ran:
                self.middleVert.append(v)
            if self.side==1:
                if pos < ran:
                    self.sideVert.append(v)    
            else:
                if pos > ran:
                    self.sideVert.append(v)   
            
    def splitMesh(self, mesh, base):
                    
        blendSh = pm.blendShape (base)
        pm.blendShape (blendSh[0], e=1, t=[base, 0, mesh, 1])
        pm.blendShape (base, e=1, weight=[0,0])        
      

        
        for v in enumerate(self.sideVert):
            index = str(v[1]).split(']')[0].split('[')[1]
            blendSh[0].inputTarget[0].inputTargetGroup[0].targetWeights[index].set(0)
            
        for v in enumerate(self.middleVert):
            index = str(v[1]).split(']')[0].split('[')[1]
            blendSh[0].inputTarget[0].inputTargetGroup[0].targetWeights[index].set(0.5)
    
        pm.blendShape (base, e=1, weight=[0,1]) 
    
class split():
    """Class for splitting shapes."""
    def __init__(self, Vlist=[], Vsuffix=''):
        self.sl = lsSl.sl(Vlist, 1, 2, Vtype=['transform'], 
                          slDict={'bs':'0:-1',
                                  'base':'-1'})
        
        print self.sl.bs
        print self.sl.base



class makeCorr():
    """
    Looks in the blendshape node for corrective shapes and connects them.
    Correctine shapes must be named like this: 'lidAttr__browShape__corr'.
    
    ========== =================================================
    Arguments  Description
    ========== =================================================
    Vlist[0]   blendshape node
    Vlist[1]   object with driver(lid) attributes
    ========== =================================================
    """
    def __init__(self, Vlist=[], Vsuffix='', edit=0, **kwdArgs):
    
        if edit: 
            self.edit(Vlist=Vlist)   
        else: 
            self.create(Vlist=Vlist, Vsuffix=Vsuffix, **kwdArgs)
    
    def create(self, Vlist=[], Vsuffix='', **kwdArgs):   
        self.sl = lsSl.sl(Vlist, 1, 2, Vtype=['blendShape', 'transform'], 
                          slDict={'drv':'1',
                                  'bs':'0'})
        
        w = getWeights([self.sl.bs])
        
        self.corr = []
        for a in w.weights:
            
            s =a[0].split('__')
            
            if s[-1] == 'corr' and len(s) == 3:

                if a[1].inputs() == []:
                    print 'corrective made!!'
                    lidAttr = pm.ls(str(self.sl.drv) + '.' + s[0])[0]
                    
                    corr = corrLid([lidAttr, w.weight[s[1]]])
                    corr.output >> a[1]
                    self.corr.append([[w.weight[s[1]]], a[1]]) 
    
    def edit(self, Vlist=[], Vsuffix=''):
        self.sl = lsSl.sl(Vlist, 1, 1, Vtype=['blendShape'], 
                          slDict={'bs':'0'})
        
        self.getCorr()
        
    def getCorr(self):
        w = getWeights([self.sl.bs])
        
        self.corr = []
        for a in w.weights:
            s =a[0].split('__')
            if s[-1] == 'corr' and len(s) == 3:
               
                if a[1].inputs():
                    self.corr.append([s[1], w.weight[s[1]], a[1]])
                    print a[1].inputs()
                    
        
class corrLid():
    """
    Corrective lid for cluster setup.
    """
    def __init__(self, Vlist=[], Vsuffix=''):
        
        lidAttr = Vlist[0] 
        browAttr = Vlist[1]
        
        blend = pm.createNode('blendColors', name='blend' + Vsuffix)
        blend.color2R.set (0)
    
        browAttr >> blend.blender
        lidAttr >> blend.color1R
        
        self.output = blend.outputR
    

def correctiveLidShape(Vlist, Vsuffix, doublesided=1, driverIn=[-1, 0, 1], outValueUp=[0, 1], outValueDown=[0, 1], VsetRange=0):
    """
    Creates a setup for browshapes that need to deform the lid outline of the 
    open eye. if the lid is open, the brow-shape will deform the line of 
    the lid. when the lid closes this brow-shape will blend with a second one 
    that does not modify the lid line. 
    
    the setup creates a blend node with four attributes to be connected to 
    the blendshapes. optional it creates the same setup with setRange nodes.

    returns:
    the blender and e.g. the setRange nodes
    
    ================ ============================================================
    Arguments        Description
    ================ ============================================================
    Vlist[0]         attribute that indicates where the lid is (0=closed, 1=open)
    Vlist[1]         attribute that drives the brow-shape 
                     (e.g. -1= down, 0=neutral, 1=up) 
    
    doublesided      If 0, there will only be a connection for a positive 
                     attribute value, e.g. only for 'brows_up'.
    
    driverIn[0]      min values of the slider that controls the brow-shapes. 
    driverIn[1]      default values of the slider that controls the brow-shapes. 
    driverIn[2]      max values of the slider that controls the brow-shapes.

    outValueUp[0]    default out up
    outValueUp[1]    max out up
    
    outValueDown[0]  default out down
    outValueDown[1]  max out down
     
    VsetRange        if value is 1, setRange nodes will be used.  
    ================ ============================================================
      
    Example::
    
        import blendshape as bs
        reload(bs)
        help(bs.correctiveLidShape)
    
        Vlid = ls('ctrlLid.ty')
        Vbrowe =  ls('ctrlBrow.ty')
    
        Vresult = bs.correctiveLidShape([Vlid, Vbrowe], Vsuffix='_oh_up_l', doublesided=1, driverIn=[-1, 0, 1],
                                        outValueUp=[0, 1], outValueDown=[0, 1], VsetRange=1)
    
        # to connect to the set range setup (VsetRange=1)
        Vresult[1].outValueX >> ls('up.tz')[0]
        Vresult[1].outValueY >> ls('up_corr.tz')[0]
        Vresult[2].outValueX >> ls('down.tz')[0]
        Vresult[2].outValueY >> ls('down_corr.tz')[0]
        
        # to connect to the keyed setup (VsetRange=0)
        Vresult.open_up >> ls('up.tz')[0]
        Vresult.closed_up >> ls('up_corr.tz')[0]
        Vresult.open_down >> ls('down.tz')[0]
        Vresult.closed_down >> ls('down_corr.tz')[0]
    """ 
     
    # ORGANIZE LIST AND RENAME
    lidAttr = Vlist[0] 
    browAttr = Vlist[1]
    
    # CREATE NODES
    Vblend = pm.createNode('blendColors', name='blend%s' % (Vsuffix))
    Vblend.color1R.set (driverIn[1])
    Vblend.color2G.set (driverIn[1])
    
    # CREATE RIG AND MAKE CONNECTIONS
    lidAttr[0] >> Vblend.blender
    browAttr[0] >> Vblend.color2R
    browAttr[0] >> Vblend.color1G 
    
    # driven keys -------------------------------------------------------------
    if VsetRange==0:
        Vblend.addAttr('open_up', defaultValue=0, min=0, max=1, keyable=1)
        Vblend.addAttr('closed_up', defaultValue=0, min=0, max=1, keyable=1)
    
        if doublesided == 1:
            Vblend.addAttr('open_down', defaultValue=0, min=0, max=1, keyable=1)
            Vblend.addAttr('closed_down', defaultValue=0, min=0, max=1, keyable=1)
 
        tk.drvKeySet(valList=[[driverIn[1], outValueUp[0]], [driverIn[2], outValueUp[1]]], 
                    VattrDriver=Vblend.outputG, VattrDriven=Vblend.open_up)
        tk.drvKeySet(valList=[[driverIn[1], outValueUp[0]], [driverIn[2], outValueUp[1]]], 
                    VattrDriver=Vblend.outputR, VattrDriven=Vblend.closed_up)
        
        if doublesided == 1:
            tk.drvKeySet(valList=[[driverIn[1], outValueDown[0]], [driverIn[0], outValueDown[1]]], 
                        VattrDriver=Vblend.outputG, VattrDriven=Vblend.open_down)
            tk.drvKeySet(valList=[[driverIn[1], outValueDown[0]], [driverIn[0], outValueDown[1]]], 
                        VattrDriver=Vblend.outputR, VattrDriven=Vblend.closed_down)
              
        pm.keyTangent (Vblend.open_up, itt='linear', ott='linear')
        pm.keyTangent (Vblend.closed_up, itt='linear', ott='linear')
        
        if doublesided == 1:
            pm.keyTangent (Vblend.open_down, itt='linear', ott='linear')
            pm.keyTangent (Vblend.closed_down, itt='linear', ott='linear')
            
        return Vblend 
    
    # set range ---------------------------------------------------------------
    elif VsetRange==1:
        
        rangeOpen = pm.createNode('setRange', name='rangeOpen%s' % (Vsuffix))
        
        if doublesided == 1:
            rangeClosed = pm.createNode('setRange', name='rangeClosed%s' % (Vsuffix))
        
        rangeOpen.minX.set (outValueUp[0])
        rangeOpen.maxX.set (outValueUp[1])
        rangeOpen.oldMinX.set (0)
        rangeOpen.oldMaxX.set (driverIn[2])
        Vblend.outputG >> rangeOpen.valueX
        rangeOpen.minY.set (outValueUp[0])
        rangeOpen.maxY.set (outValueUp[1])
        rangeOpen.oldMinY.set (0)
        rangeOpen.oldMaxY.set (driverIn[2])
        Vblend.outputR >> rangeOpen.valueY
        
        if doublesided == 1:
            rangeClosed.minX.set (outValueDown[1])
            rangeClosed.maxX.set (outValueDown[0])
            rangeClosed.oldMinX.set (driverIn[0])
            rangeClosed.oldMaxX.set (0)
            Vblend.outputG >> rangeClosed.valueX
            rangeClosed.minY.set (outValueDown[1])
            rangeClosed.maxY.set (outValueDown[0])
            rangeClosed.oldMinY.set (driverIn[0])
            rangeClosed.oldMaxY.set (0)
            Vblend.outputR >> rangeClosed.valueY
              
            return[Vblend, rangeOpen, rangeClosed]
        
        return[Vblend, rangeOpen]
    
def createComplement(Vlist=[]):
    """
    creates: 
    the complement of a split blendshape

    returns: 
    the new mesh
    
    =========== =================================================
    Arguments   Description
    =========== =================================================
    Vlist[0]    full blendshape
    Vlist[1]    half blendshape
    Vlist[2]    original mesh
    =========== =================================================
    """
    Vlist = lsSl.lsSl(Vlist, 3, 3, ['transform'])

    Vfull = pm.duplicate(Vlist[0])
    Vhalf = pm.duplicate(Vlist[1])
    Vorig = pm.duplicate(Vlist[2])

    VblendSh = pm.blendShape (Vorig)

    pm.blendShape (VblendSh[0], e=1, t=[Vorig[0], 0, Vfull[0], 1])
    pm.blendShape (VblendSh[0], e=1, t=[Vorig[0], 1, Vhalf[0], 1])
    pm.blendShape (VblendSh[0], e=1, weight=[0,1])
    pm.blendShape (VblendSh[0], e=1, weight=[1,-1])

    Vnew = pm.duplicate(Vorig[0])
    Vcons = pm.pointConstraint(Vlist[0], Vlist[1], Vnew)

    pm.delete (Vorig, Vfull, Vhalf, ch=1)
    pm.delete(Vfull, Vhalf, Vorig, Vcons)
    Vshapes = pm.listRelatives(Vnew, shapes=1)

    gt.deleteUnusedShapes(Vshapes, printInfo=1)

    return Vnew

                
class getWeights():
    """
    Returns all weights of a blendshape as attributes.
   
    Example::
    
        import mf.tools.blendshape as tb
        reload(tb) 
        
        W = tb.getWeights()
        
        bsNode = pm.ls('body_kc_blendShape1')[0]
        mesh = pm.ls('body_bs_goal')[0]
        newShape = pm.ls(sl=1)[0] 
        
        i = W.weightList.index('pCube2')
        
        pm.blendShape(bsNode, e=1, t=[mesh, i, newShape, 1])
    
    ================ ==========================================  
    Class attributes Description 
    ================ ==========================================       
    self.weight      Dictionary with weightname/attribute pairs.
    self.weightList  List of weightnames. Indexes match the 
                     blendshape.weights index of the blendshape 
                     node. Empty weights are ``None``.
    self.weights     Nested List of weightname/attribute.
    self.connections List of source/destination attributes.
    ================ ==========================================  
    """
    def __init__(self, Vlist=[], Vsuffix=''):
        Vlist = lsSl.lsSl(Vlist, 1, 1, Vtype=['blendShape'])
        
        self.blendshape = Vlist[0]
        self.weight = {}
        self.weightList = []
        self.weights = []
        weight = []
        
        for i, comp in enumerate(self.blendshape.weight):
            weight.append(comp)
        
        # check for blendshape node without targets
        if( not weight ):
            return
        
        wname = str(weight[-1])
        s = wname.find('[')
        e = wname.find(']')
        alhighest = int(str(wname)[s+1:e])+1    
        
        for i in range(alhighest):
            self.weightList.append(None)
        
        for i, comp in enumerate(self.blendshape.weight):
            alias = pm.aliasAttr(comp, q=1)
            
            #if there is no alias, use the attribute name
            if alias == '':
                alias = comp.name(includeNode=False)
          
            wname = str(comp)
            s = wname.find('[')
            e = wname.find(']')
            index = int(str(wname)[s+1:e])
            
            if not alias == '':
                self.weight[alias] = comp
                self.weights.append([alias,comp])
                self.weightList[index] = alias
                
            else:
                wname = str(comp)
                s = wname.find('.')
                attrName = str(wname)[s+1:]
                self.weights.append([attrName,comp])
                self.weightList[index] = attrName
                
        self.getConnections()
                
    def getConnections(self):
        """
        Stores all connections to the blendshape node in ``self.connections``
        """
        self.connections = []
        for e in self.weights:
            inputs = e[1].inputs(plugs=1)
            value = e[1].get()
            if inputs:
                self.connections.append([inputs[0], e[1], value])
            else:
                self.connections.append([None, e[1], value])
                
    
    def disconnect(self):
        """
        Disconnects all inputs from the blendshape.
        """
        for e in self.connections:
            if e[0]:
                pm.disconnectAttr(e[0], e[1])
            #print e
    
    def reconnect(self):
        """
        Reconnects all inputs to the blendshape.
        """
        for e in self.connections:
            pm.setAttr(e[1], e[2])
            if e[0]:
                pm.connectAttr(e[0], e[1])
                
class replaceShapes(getWeights):
    """
    Example::
    
        import mf.tools.blendshape
        reload (mf.tools.blendshape)
        x = mf.tools.blendshape.replaceShapes(Vlist=[], Vsuffix='',)
        x.getPlugs()
        x.replace(oldShape='R_Press_In')
        
        
        for s in pm.ls(sl=1):
            old = str(s).replace('_n', '')
            try:
                print x.weightList.index(old)
            except:
                print s
            print s
            old = str(s).replace('_n', '')
            pm.select(s)
            x.replace(oldShape=old)
    """
    def getPlugs(self):
        self.plugs=[]
        for i in range(len(self.weightList)):
            plug = pm.ls(str(self.blendshape)+".inputTarget[0].inputTargetGroup[" + str(i) + "].inputTargetItem[6000].inputGeomTarget")[0]
            self.plugs.append(plug)
            
    def replace(self, Vlist=[], oldShape=''):
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.getPlugs()
        plug = self.plugs[self.weightList.index(oldShape)]
        print plug
        print plug.inputs()
        Vlist[0].getShape().worldMesh[0] >> plug

          
class wrapShapes():
    """
    Make copys of a shape by wrapping a new mesh to an old mesh.
    Wrap deformer options must be set in the maya UI.
    
    =========== =================================================
    Arguments   Description
    =========== =================================================
    Vlist[0]    old mesh (with blendshapes)
    Vlist[1]    blendshape node
    Vlist[2]    new mesh
    =========== =================================================
    
    Returns:
    New blendshapes for destmesh.  
    """
    def __init__(self, Vlist=[], Vsuffix='', **kwdargs):

        Vlist = lsSl.lsSl(Vlist, 3, 3)
        
        pm.select(Vlist[2], Vlist[0])
        mel.eval('CreateWrap')
        
        al = pm.aliasAttr(Vlist[1], q=1)
        attrList = []
        shapeNames = []
        for i in range(0, len(al), 2):
            attrList.append(pm.ls(Vlist[1] + '.' + al[i])[0])
            shapeNames.append(al[i])
        # break all connections
        for a in attrList:
            try:
                pm.disconnectAttr(a.connections(p=1)[0], a) 
            except:
                pass
            a.set(0)
        
        self.result = []
        #make copys
        for i in range(len(attrList)):
            attrList[i].set(1)
            self.result.append(pm.duplicate(Vlist[2], n=shapeNames[i] + '_WRAPPED'))
            attrList[i].set(0)
            
        pm.group(self.result, w=1, n='grp_WRAPPED')

def bakeCorrective(Vlist=[], Vsuffix=''):
    """
    Bakes a corrective shape to a list of blendshapes.

    =========== =================================================
    Arguments   Description
    =========== =================================================
    Vlist[0:-2] blendshape meshes
    Vlist[-2]   original mesh
    Vlist[-1]   corrective shape
    =========== =================================================
    """
    Vlist = lsSl.lsSl(Vlist, 3, 0, ['transform'])
    
    shapes = Vlist[0:-2]
    print shapes
    orig = Vlist[-2]
    corr = Vlist[-1]
    
    result = []
    
    for o in shapes:
        performSettings(1)
        dub = pm.duplicate(orig)[0]
        performSettings(0)
        
        VblendSh = pm.blendShape (dub)
        pm.blendShape (VblendSh[0], e=1, t=[dub, 0, corr, 1])
        pm.blendShape (VblendSh[0], e=1, t=[dub, 1, o, 1])
        pm.blendShape (VblendSh[0], e=1, weight=[0,1])
        pm.blendShape (VblendSh[0], e=1, weight=[1,1])
        
        new = pm.duplicate(dub)[0]
        
        new.tx.setLocked(0)
        new.ty.setLocked(0)
        new.tz.setLocked(0)
        
        Vcons = pm.pointConstraint(o, orig, new)
        
        pm.delete (dub, orig, ch=1)
        pm.delete (dub, Vcons)
        
        Vshapes = pm.listRelatives(new, shapes=1)
        gt.deleteUnusedShapes(Vshapes, printInfo=1)
        
        pm.rename(new, str(o) + '_CORR_' + str(corr))
        
        result.append(new)
        
    return result 


def correctiveLinear(Vlist=[]):
    """
    creates: 
    A corrective blendshape for linear deformation

    returns: 
    the new mesh
    
    ========= =================================================
    Arguments Description
    ========= =================================================
    Vlist[0]    shape
    Vlist[1]    shape corrected
    Vlist[2]    original
    ========= =================================================
    """
    Vlist = lsSl.lsSl(Vlist, 3, 3, ['transform'])

    orig = Vlist[0]
    modeled = Vlist[1]
    mesh = Vlist[2]
    
    performSettings(1)
    dub = pm.duplicate(mesh)
    performSettings(0)
    
    VblendSh = pm.blendShape (dub)
    
    pm.blendShape (VblendSh[0], e=1, t=[dub[0], 0, modeled, 1])
    pm.blendShape (VblendSh[0], e=1, t=[dub[0], 1, orig, 1])
    pm.blendShape (VblendSh[0], e=1, weight=[0,1])
    pm.blendShape (VblendSh[0], e=1, weight=[1,-1])

    new = pm.duplicate(dub)[0]
    
    new.tx.setLocked(0)
    new.ty.setLocked(0)
    new.tz.setLocked(0)
    
    Vcons = pm.pointConstraint(modeled, orig, new)
    
    pm.delete (dub, ch=1)
    pm.delete (dub, Vcons)
    
    Vshapes = pm.listRelatives(new, shapes=1)
    gt.deleteUnusedShapes(Vshapes, printInfo=1)
    
    return new

class pipe():
    """
    Pipes a number of meshes trough a painted blendshape. 
    For splitting shapes.
    
    =========== =================================================
    Arguments   Description
    =========== =================================================
    Vlist[0:-2] full modeled shapes
    Vlist[-2]   source shape
    Vlist[-1]   painted goal mesh with Vlist[-2] as blendshape
    =========== =================================================
    """
    def __init__(self, Vlist=[], Vsuffix=''):
        
        self.sl = lsSl.sl(Vlist, 3, 0,
                      slDict={
                              'full':'0:-2',
                              'source':'-2',
                              'split':'-1',
                              },
                              Vrename=0, Vsuffix=Vsuffix)
        result = []
        for o in self.sl.full:
            VblendSh = pm.blendShape (self.sl.source)
            
            pm.blendShape (VblendSh[0], e=1, t=[self.sl.source, 0, o, 1])
            pm.blendShape (VblendSh[0], e=1, weight=[0,1])
            
        
            new = pm.duplicate(self.sl.split)[0]
            pm.rename(new, str(o) + Vsuffix)
            
            new.tx.setLocked(0)
            new.ty.setLocked(0)
            new.tz.setLocked(0)
            
            Vcons = pm.pointConstraint(o, new)
            new.ty.set(new.ty.get()+1)
            
            pm.delete (VblendSh)
            pm.delete (o, ch=1)
            pm.delete (Vcons)
            
            Vshapes = pm.listRelatives(new, shapes=1)
            gt.deleteUnusedShapes(Vshapes, printInfo=1)
            
            result.append(new)
            
        pm.select(result)
    
        
def performSettings(val):
    pm.performanceOptions(
                        clusterResolution=val, 
                        disableStitch=val, 
                        disableTrimDisplay=val, 
                        latticeResolution=val, 
                        passThroughBindSkinAndFlexors=val, 
                        passThroughBlendShape=val, 
                        passThroughCluster=val, 
                        passThroughFlexors=val, 
                        passThroughLattice=val, 
                        passThroughPaintEffects=val, 
                        passThroughSculpt=val, 
                        passThroughWire=val, 
                        useClusterResolution=val, 
                        useLatticeResolution=val
                        )

def getTweak(Vlist=[]):
    """
    Returns the first ``tweak`` node found in the deformer history.
    """
    Vlist = lsSl.lsSl(Vlist, 1, 1)
    hist = Vlist[0].listHistory(leaf=1, historyAttr=1, ag=1)
    
    for o in hist:
 
        if o.type() == 'tweak':
            return o
    
    return None
    
def materialize(Vlist=[], targetNames=[]):
    """
    Materializes a blendshape mesh if it had been deleted.
    
    =========== ==========================================================
    Arguments   Description
    =========== ==========================================================
    Vlist[0]    blendshape node
    targetNames list of names of blendshape weights to be materialized, if
                the list is empty, all weights will be materialized. 
    =========== ==========================================================
    """
    Vlist = lsSl.lsSl(Vlist, 1, 1, Vtype=['blendShape'])

    W = getWeights(Vlist)
    result = []
    
    Vlist[0].useTargetCompWeights.set(0)
    W.disconnect()
    performSettings(1)
    pm.performanceOptions(passThroughBlendShape=0)
    
    outGeo = Vlist[0].outputGeometry[0].outputs()[0]
    getTweak([outGeo]).envelope.set(0)
    
    if targetNames == []:
        targetNames = W.weightList
        
    for name in targetNames:
        if name:
            i = W.weightList.index(name)   
            inputAttr = Vlist[0].inputTarget[0].inputTargetGroup[i].inputTargetItem[6000].inputGeomTarget
            
            if inputAttr.isConnected():
                pm.warning('"' + name +'" skipped. InputAttr is connected')
            
            else:
                # get mesh from blendshape node
                mesh = pm.blendShape(Vlist[0], geometry=1, q=1)[0]
                
                for e in W.weights:
                    e[1].set(0)    
                W.weight[name].set(1)
                
                newMesh = pm.duplicate(mesh)[0]
                gt.deleteUnusedShapes(newMesh.getShapes(), printInfo=0)
                newMesh.getShape().worldMesh[0] >> inputAttr
                newMesh.setParent(w=1)
                newMesh.rename(name)
                result.append(newMesh)
        
    Vlist[0].useTargetCompWeights.set(1)
    W.reconnect()
    performSettings(0)
    getTweak(Vlist).envelope.set(1) 
    
    return result
    
    

    
                          
