"""
general joint tools
"""

import pymel.core as pm
import pymel.core.datatypes as dt
import lsSl
import snap
import parent as tp
import cons as tc
import mf.tools.proxyGeo as pg
from mf.tools import key as tk

def split(Vlist=[], numJoints=1,  Vparent=1, Vtype=2, Vname='', Vprefix='', Vsuffix=''):
    """
    creates: 
    split joints between two objects.   
    the split joints will be oriented like Vlist[0]

    returns: list of the new joints

    ========== ===================================================
    Arguments  Description
    ========== ===================================================
    Vlist[0]   source start
    Vlist[1]   source end
    numJoints  number of split joints
    Vparent    if 1 the new joints will be parented as a chain
    Vtype      1=point, 2=orient, 3=parent, 4=scale(blend nodes)
    ========== ===================================================
    """
    Vlist = lsSl.lsSl(Vlist, 2, 2)
    Vsplit=[]
    
    for i in range(numJoints):
        Vsplit.append((tp.nullGroup(Vlist=[Vlist[0]], Vhierarchy=1, Vtype=Vtype, 
                                    Vname=Vname, Vprefix=Vprefix, Vsuffix=Vsuffix))[0])
    
    Vjoints = Vsplit+Vlist   
    tc.distribute(Vlist=Vjoints, Vtype=1, Voffset=0, permanent=0, Vsuffix='')
    
    if Vparent == 1:
        for i in range((len(Vjoints)-3)):
            pm.parent(Vjoints[i+1], Vjoints[i])
        pm.parent(Vlist[1], Vjoints[-3])
        # Bugfix 24.01.16
        Vjoints[-3].s >> Vlist[-1].inverseScale
        pm.parent(Vjoints[0], Vlist[0])
    
    pm.select (Vlist)
    
    return Vsplit

def duplicateChain(Vlist=[], Vnamestring=[]):
    """
    Duplicates a joint chain and renames with a namestring returns joints.
    """ 
    Vlist = lsSl.lsSl(Vlist, 1, 1, ['joint'], '\nselect a root joint')
    
    jointA = Vlist[0] 
    jointA = pm.duplicate (Vlist[0], rr=1, renameChildren=1);
    VjointList = pm.listRelatives (jointA, allDescendents=1) 
    VjointList.reverse()
    VjointList.insert(0, jointA[0])
    
    for i in range(len(VjointList)): 
        if len(Vnamestring) != 0:
            pm.rename (VjointList[i], Vnamestring[0])
            Vnamestring.pop(0)
    
    return VjointList

def jointInbCons(Vlist=[], Vsuffix='', Vproxy=1, proxyScale=1, side=3, 
                 label='', type=2, interpolateScale=0, autoSlide=0):
    """
    Creates an interpolation joint for skinning, constraint between two joints.
    Vlist -- [start, end] (start is parent)
    """

    Vlist = lsSl.lsSl(Vlist, 2, 2)
    pm.select(cl=1)
    
    Vjoint = tp.nullGroup(Vlist=[Vlist[1]], Vhierarchy=4, Vtype=2, 
                          Vname=str(Vlist[0]), Vsuffix='_inb')[0]
    Vjoint.jointOrient.set([0,0,0])
    
    nullJoint = tp.nullGroup(Vlist=[Vjoint], Vhierarchy=3, Vtype=2, 
                              Vprefix='null_')[0]
    
    snap.snap([Vjoint,Vlist[1]])
    pm.makeIdentity(Vjoint, apply=1, t=1, r=1, s=1, n=0, jo=1)
    
    if type == 2:
        Vorient = pm.orientConstraint(Vlist[1], Vlist[0], Vjoint, mo=0)
        Vorient.interpType.set (2)
    
    elif type == 3:
        Vorient = pm.parentConstraint(Vlist[1], Vlist[0], Vjoint, mo=1)
        Vorient.interpType.set (2)
    
    if interpolateScale:
        tc.distribute([Vjoint, Vlist[1], Vlist[0]], Vtype=4)
    

    proxPar = Vjoint    
    if autoSlide:
        Vlist[1].addAttr('slide', defaultValue=0, 
                                min=0, max=1, keyable=1)
        
        shift = tp.nullGroup(Vlist=[Vjoint], Vhierarchy=4, 
                                        Vtype=2, Vsuffix='_shift')[0]
        
        blendScaleMid = pm.createNode('blendColors', 
                                      name='blendScaleMid' + Vsuffix)
        
        blendScaleMid.output >>  shift.t
        Vlist[1].slide >> blendScaleMid.blender
        Vlist[1].slide.set(1)
        blendScaleMid.color2.set (0,0,0)
        blendScaleMid.color1.set (0,0,0)
        shiftDirection=1
        tk.drvKeySet([[0,0], 
                      [-40, 0.30*shiftDirection*autoSlide],
                      [-90, 0.50*shiftDirection*autoSlide], 
                      [40, -0.30*shiftDirection*autoSlide],
                      [90, -0.50*shiftDirection*autoSlide]], 
                    Vjoint.rz, blendScaleMid.color1G)
        
        tk.drvKeySet([[0,0], 
                      [-40, -0.30*shiftDirection*autoSlide],
                      [-90, -0.50*shiftDirection*autoSlide], 
                      [40, 0.30*shiftDirection*autoSlide],
                      [90, 0.50*shiftDirection*autoSlide]], 
                    Vjoint.ry, blendScaleMid.color1B)
        
        proxPar = shift
        
    if Vproxy:
        pg.proxyGeo(Vlist=[proxPar], VcenterPivot=1, chain=0, 
                    shader=1, Vcolor=[0.6, 0.3, 0.3], xScale=0.1, 
                    shaderName='shader', yzScaleGlobal=proxyScale,
                    side=side, label=label)
    
    return [Vjoint, nullJoint]
    
def jointLabel(Vlist=[], Vsuffix='string', Vlabel='label'):
    """
    Sets joint labels for the selected joints to right and left.
    """ 
    Vlist = lsSl.lsSl(Vlist, 2, 2)

    Vjoints = pm.selected()
    for Vjoint in Vjoints:
        Vjoint.type = 18
        Vjoint.otherType=Vlabel

    Vjoints[0].side.set (1)
    Vjoints[1].side.set (2)

def setJointAttr(Vlist=[], val=1):
    """
    Sets attributes visible in the channelbox.
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0, ['joint'])

    for i in range(len(Vlist)):
        Vlist[i].jointOrientX.set(k=0, cb=val)
        Vlist[i].jointOrientY.set(k=0, cb=val)
        Vlist[i].jointOrientZ.set(k=0, cb=val)
        
        Vlist[i].preferredAngleX.set(k=0, cb=val)
        Vlist[i].preferredAngleY.set(k=0, cb=val)
        Vlist[i].preferredAngleZ.set(k=0, cb=val)
        
        Vlist[i].segmentScaleCompensate.set(k=1, cb=val)

def addRoToJo(Vlist=[]):
    """
    Adds the rotation of a joint to the joint-orientation 
    and sets the joint-orientation to zero.
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0, ['joint'])

    for i in range(len(Vlist)):
        Vlist[i].jointOrient.set ((Vlist[i].r.get()+Vlist[i].jointOrient.get()))
        Vlist[i].r.set ([0, 0, 0])
                
