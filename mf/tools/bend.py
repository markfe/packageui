"""
tools for bendy setups like spline ik and wire deformers
"""
import pymel.core as pm
import mf.builder.nodes
from pymel.core.language import mel
import mf.tools.lsSl as lsSl
import mf.tools.general as gt
import mf.tools.parent as tp
import mf.tools.joint as tj
import mf.tools.distance as td
import mf.tools.proxyGeo as tg
import mf.tools.key as tk
import mf.tools.cons as tc
import mf.tools.snap as snap
import mf.tools.curve as tcv

class splineIk():
    """
    creates:
    simple spline ik with edit option
    
    ========== =====================================================
    Arguments  Description
    ========== =====================================================
    Vlist      first joint in chain or ikHandle(edit=1)
    edit       collecting objects 
    ========== =====================================================
    """
    def __init__(self, Vlist=[], edit=0, numSpans=1, **kwdArgs):
        if edit==0:
            self.create(Vlist=Vlist, numSpans=numSpans, **kwdArgs)
        else:
            self.edit(Vlist=Vlist)        
    
    def create(self, Vlist=[], Vsuffix='', numSpans=20):
        """creates a spline ik, Vlist[0] -- first joint of chain"""
        Vlist = lsSl.lsSl(Vlist=Vlist, Vmin=1, Vmax=1)
        self.Vsuffix = Vsuffix
        
        self.jointChain = pm.listRelatives(Vlist[0], allDescendents=1)
        self.jointChain.append(Vlist[0])
        self.jointChain.reverse()
        
        self.handle, self.effector, self.curve = \
            pm.ikHandle(startJoint=Vlist[0], endEffector=self.jointChain[-1], 
                        sol='ikSplineSolver', numSpans=numSpans)
    
        pm.rename(self.handle, 'ikHandle' + Vsuffix) 
        pm.rename(self.effector, 'effector' + Vsuffix)
        pm.rename(self.curve, 'curve' + Vsuffix)  
        
        self.curveShape = self.curve.getShapes()[0]
        self.curve.template.set(1)
        self.curve.template.set(lock=0, keyable=0, channelBox=1)
        
    def edit(self, Vlist=[], Vsuffix=''):
        """edit a spline ik, Vlist[0] -- ikHandle"""
        Vlist = lsSl.lsSl(Vlist=Vlist, Vmin=1, Vmax=1, Vtype=['ikHandle'])
        self.Vsuffix = Vsuffix
        self.handle = Vlist[0]
        self.curveShape = self.handle.inCurve.inputs(shapes=1)[0]
        self.curve = self.curveShape.getParent()
        self.effector = pm.ikHandle(self.handle, q=1, endEffector=1)
        self.jointChain = pm.ikHandle(self.handle, q=1, jointList=1)
        jointLast = self.effector.translateX.inputs()[0]
        self.jointChain.append(jointLast)
    
    def rebuildCurve(self, numCvs=4):
        """Rebuilds the curve and evenly distributes the cvs"""
        pm.rebuildCurve(self.curve, rt=0, ch=0, end=1, d=1, kr=0,
                        s=numCvs, kcp=0, tol=0, kt=0, rpo=1, kep=1)                
        pm.rebuildCurve(self.curve, rt=0, ch=0, end=1, d=3, kr=0,
                        s=9, kcp=1, tol=0, kt=0, rpo=1, kep=1)
        
    def __call__(self):
        """returns the objects of the splineIK"""
        return [self.handle, self.curve, self.curveShape, 
                self.effector, self.jointChain, self.Vsuffix]    
        
        
class stretchySpline(splineIk):
    """
    creates:
    a stretchy splineIk

    ========== =====================================================
    Arguments  Description
    ========== =====================================================
    Vlist      first joint in chain or ikHandle(add=1)
    add        add stretchy setup to an existing spline ik 
    ========== =====================================================
    """
    def __init__(self, Vlist=[], Vsuffix='', add=0, numSpans=1):
        """create ik handle and stretchy setup"""
        
        if add == 0:
            splineIk.__init__(self, Vlist=Vlist, Vsuffix=Vsuffix, edit=0, 
                              numSpans=numSpans)
        elif add == 1:
            splineIk.__init__(self, Vlist=Vlist, Vsuffix=Vsuffix, edit=1)
        
        self.crvInf = pm.createNode('curveInfo',n=("curveInfoSpine" + Vsuffix))
        self.curveShape.worldSpace[0] >> self.crvInf.inputCurve  
        self.multSpn = pm.createNode('multiplyDivide',
                                     n=("mult_strSp" + Vsuffix))
        self.multSpn.operation.set(2)
        
        self.multScl = pm.createNode('multiplyDivide',
                                     n=("mult_scale" + Vsuffix))
        self.multScl.addAttr ('initial_arclength', keyable=0)
        self.multScl.initial_arclength.set(channelBox=1)
        
        self.multScl.operation.set(1)
        
        arclength = self.crvInf.arcLength.get()
        self.multScl.initial_arclength.set(arclength)
        self.crvInf.arcLength >> self.multSpn.input1X
        
        self.multScl.input1X.set(1)
        self.arclengthSet(arclength)
        
        self.multScl.outputX >> self.multSpn.input2X
        self.connectJoints()
        if add != 1:
            for i in range(len(self.jointChain)):
                pm.rename(self.jointChain[i], 'jnt_' + str(i+1) + self.Vsuffix)
        
        pm.select(cl=1)
        
    def __call__(self):
        """returns the objects of the stretch rig"""
        return [self.curve, self.crvInf, self.multScl]
    
    def arclengthSet(self, value=1):
        """sets the arclength divider"""
        self.multScl.input2X.set(value)
    
    def arclength(self):
        """returns the curves arclength"""
        return self.crvInf.arcLength.get()
    
    def initialArclength(self):
        """returns the curves initial arclength"""
        return self.multScl.initial_arclength.get()
        
    def connectJoints(self):
        """connect to joint list scaleX"""
        for i in range(len(self.jointChain)):
            self.multSpn.outputX >> self.jointChain[i].scaleX
        return self.jointChain
 
    def connectScale(self, attr):
        """connect to scale rig"""
        attr >> self.multScl.input1X
        return self.multScl


class bend(stretchySpline):
    """
    Full bend setup for a three joint limb.
    """
    def __init__(self, Vlist=[], Vsuffix='_XXX', numJoints=25, midPosition=13, 
                 sharpBend=1, proxy=1, side=2):
        """
        Creates the setup.

        ========== =====================================================
        Arguments  Description
        ========== =====================================================
        Vlist[0:3] Three joints chain.
        Vlist[3:5] Bend controls.
        Vlist[5]   IKctrl
        Vlist[-1]  Attribute Object.
        numJoints  Number of split joints.
        ========== =====================================================
        """
        Vlist = lsSl.lsSl(Vlist=Vlist, Vmin=6, Vmax=6)
        pm.select(cl=1)
        self.AttrObj = Vlist.pop()
        self.Vsuffix = Vsuffix
        self.joint1 = pm.rename(Vlist[0], 'base_1'+Vsuffix)
        self.joint2 = pm.rename(Vlist[1], 'base_2'+Vsuffix)
        self.joint3 = pm.rename(Vlist[2], 'base_3'+Vsuffix)
        self.ctrlA = pm.rename(Vlist[3], 'ctrlA'+Vsuffix)
        self.ctrlB = pm.rename(Vlist[4], 'ctrlB'+Vsuffix)
        self.ctrlIK = Vlist[5]
        
        mf.builder.nodes.set([self.ctrlA, self.ctrlB], 'transform_ctrl')
       
        self.cluster = []
        self.clusterLoc = []
        self.makeJointchain(numJoints)
        
        self.AttrObj.addAttr('bend', defaultValue=0, min=0, keyable=1)
        self.AttrObj.bend.setLocked(1)
        
        self.AttrObj.addAttr('ctrls_vis', defaultValue=0, min=0, max=1,
                             keyable=1, at='long')
        
        self.AttrObj.ctrls_vis >> self.ctrlA.getShape().v
        self.AttrObj.ctrls_vis >> self.ctrlB.getShape().v
            
        self.distAB = td.dist(Vlist=[self.joint1, self.joint2],
                              name='distAB'+Vsuffix)
        
        calA = tp.nullGroup([self.distAB.start], Vhierarchy=1, Vtype=1, 
                            Vname='calibrateA', Vsuffix=Vsuffix)[0]
        scaleA = tp.nullGroup([calA], Vhierarchy=4, Vtype=1, 
                              Vname='scaleA', Vsuffix=Vsuffix)[0]
        
        sA = self.distAB.calibrate(1)
        calA.scale.set(sA)
        self.distBC = td.dist([self.joint2, self.joint3], 
                              name='distAC'+Vsuffix)
        
        calB = tp.nullGroup([self.distBC.start], Vhierarchy=1, Vtype=1, 
                            Vname='calibrateB', Vsuffix=Vsuffix)[0]
        scaleB = tp.nullGroup([calB], Vhierarchy=4, Vtype=1, 
                              Vname='scaleB', Vsuffix=Vsuffix)[0]
        
        sB = self.distBC.calibrate(1)
        calB.scale.set(sB)
        
        stretchySpline.__init__(self, Vlist=self.startEnd, Vsuffix=Vsuffix,
                                numSpans=1)
        
        if sharpBend == 1: 
            self.sharpBend(midPosition=midPosition)
        else:
            self.drvChain = self.initialChain[:]
               
        for cmpon in enumerate(self.curveShape.cv):
            cluster = (pm.cluster(cmpon[1], n=('Cluster'+Vsuffix)))
            clusterLoc = tp.nullGroup(Vlist=[cluster[1]], Vhierarchy=1, 
                                      Vtype=1, Vsuffix='_loc')[0]
            self.clusterLoc.append(clusterLoc)
            self.cluster.append(cluster[1])
            pm.parentConstraint([clusterLoc, cluster])
          
        self.distAB.align(Vlist=[self.cluster[1], self.clusterLoc[1], 
                                 self.ctrlA], percent=50)
        snap.snapRot([self.ctrlA, self.joint1])
        self.distBC.align(Vlist=[self.cluster[2], self.clusterLoc[2], 
                                 self.ctrlB], percent=50)
        snap.snapRot([self.ctrlB, self.joint2])
        
        self.ctrlA.setParent(scaleA)
        self.ctrlB.setParent(scaleB)
        
        ctrlNull = tp.nullGroup([self.ctrlA, self.ctrlB], 
                                Vhierarchy=3, Vtype=1, Vprefix='null_')
        
        ctrlKeys = tp.nullGroup([self.ctrlA, self.ctrlB], 
                                Vhierarchy=3, Vtype=1, Vprefix='keys_')
        
        ctrlSide = tp.nullGroup(ctrlKeys, 
                                Vhierarchy=3, Vtype=1, Vprefix='side_')
            
        pm.makeIdentity(ctrlNull, s=1, r=0, t=0, apply=1)
        
        ctrlRot = tp.nullGroup([ctrlNull[1]], 
                                Vhierarchy=3, Vtype=1, Vprefix='rot_')[0] 
        # Rotate one controller group.
        ctrlNull[1].rx.set(180)
        ctrlNull[1].rz.set(180)
        
        if side == 2:
            ctrlSide[0].rz.set(180)
            ctrlSide[1].rz.set(180)
        
        self.clusterLoc[0].setParent(scaleA)
        self.clusterLoc[1].setParent(self.ctrlA)
        self.clusterLoc[2].setParent(self.ctrlB)
        self.clusterLoc[3].setParent(scaleB)
        calA.setParent(self.joint1)
        calB.setParent(self.joint2)
        
        # Scaling of the curve arc and inverse scaling of the ctrls.
        self.joint1.sx >> scaleA.sy
        self.joint1.sx >> scaleA.sz
        self.joint2.sx >> scaleB.sy
        self.joint2.sx >> scaleB.sz
        
        invScaleA = pm.createNode('multiplyDivide', 
                                  name='invScaleCtrlA'+Vsuffix)
        invScaleA.operation.set (2)
        invScaleA.input1.set([1, 1, 1])
        self.joint1.sx >> invScaleA.input2X
        invScaleA.outputX >> self.ctrlA.sx
        invScaleA.outputX >> self.ctrlA.sy
        invScaleA.outputX >> self.ctrlA.sz
        
        invScaleB = pm.createNode('multiplyDivide', 
                                  name='invScaleCtrlA'+Vsuffix)
        invScaleB.operation.set (2)
        invScaleB.operation.set (2)
        invScaleB.input1.set([1, 1, 1])
        self.joint2.sx >> invScaleB.input2X
        invScaleB.outputX >> self.ctrlB.sx
        invScaleB.outputX >> self.ctrlB.sy
        invScaleB.outputX >> self.ctrlB.sz
    
               
        self.addKeysA = pm.createNode('plusMinusAverage', 
                                      name='addKeysA%s' % (Vsuffix))
        self.addKeysB = pm.createNode('plusMinusAverage', 
                                      name='addKeysB%s' % (Vsuffix))
        self.addKeysA.output3D >> ctrlKeys[0].t
        self.addKeysB.output3D >> ctrlKeys[1].t
        
        
        self.AttrObj.addAttr('bend_shapes', defaultValue=0, min=0, keyable=1)
        self.AttrObj.bend_shapes.setLocked(1)
        
        self.bendRound(self.blender(name='round', default=1))
        self.bendInverse(self.blender(name='inverse', default=0))
        self.bendSharp(self.blender(name='straight', default=0))
        
        if proxy:
            tg.proxyGeo(Vlist=self.drvChain, yzScaleGlobal=0.05)  
        
        self.cleanup()
        pm.select(self.joint2)
        return
    
    def blender(self, name, default):
        """creates a blender for each ctrl"""
        VblendA = pm.createNode('blendColors', name='A_'+name)
        VblendB = pm.createNode('blendColors', name='B_'+name)
        VblendA.color1.set([0, 0, 0])
        VblendA.color2.set([0, 0, 0])
        VblendB.color1.set([0, 0, 0])
        VblendB.color2.set([0, 0, 0])
        
        self.AttrObj.addAttr(name, defaultValue=default, min=0, max=1, 
                             keyable=1, at='double')  
        shapeAttr = pm.ls(str(self.AttrObj)+'.'+str(name))[0]
        
        shapeAttr >> VblendA.blender
        shapeAttr >> VblendB.blender
        
        i = 0
        while self.addKeysA.input3D[i].isConnected(): i = i + 1
        VblendA.output >> self.addKeysA.input3D[i]
        i = 0
        while self.addKeysB.input3D[i].isConnected(): i = i + 1
        VblendB.output >> self.addKeysB.input3D[i]
    
        return [VblendA, VblendB]
      
    def bendRound(self, blendPair):
        self.setKeys(blendPair=blendPair,
                     keyValuesX=[
                                [0, 0],
                                [90, 0.3], 
                                [180, 0.5]
                                ],
                    keyValuesY=[
                                [0, 0],
                                [90, 0.3], 
                                [180, 0.9],
                                ]
                                ) 
    
    def bendSharp(self, blendPair):
        self.setKeys(blendPair=blendPair,
                     keyValuesX=[
                                [0, 0],
                                [1, 0.2],
                                [3, 0.4],
                                [22.5, 0.6],
                                [45, 0.7],
                                [90, 0.6], 
                                [180, 0.7]
                                ],
                    keyValuesY=[
                                [0, 0],
                                [45, 0],
                                [90, 0], 
                                [180, 0],
                                ]
                                ) 
        
    def bendInverse(self, blendPair):
        self.setKeys(blendPair=blendPair,
                     keyValuesX=[
                                [0, 0],
                                [90, 0.3], 
                                [180, 0.5]
                                ],
                    keyValuesY=[
                                [0, 0.3],
                                [45, 0.2],
                                [90, 0.3],
                                [180, 0.9],
                                ]
                                )
    
    def setKeys(self, blendPair, keyValuesX, keyValuesY):
        """
        Sets driven keys for a bend shape.

        ========== =====================================================
        Arguments  Description
        ========== =====================================================
        blendPair  Blender for upper and lower controller.
        keyValues  Nested list for translation x and y.
        ========== =====================================================
        """
        keyValuesMirrX = keyValuesX[:]
        keyValuesYinv = keyValuesY[:]
        
        for val in keyValuesY:
            keyValuesYinv.append([val[0],val[1]*-1])
        
        keyValuesMirrY = keyValuesYinv[:]
        
        for val in keyValuesX:
            keyValuesMirrX.append([val[0]*-1,val[1]])
                
        for val in keyValuesYinv:
            keyValuesMirrY.append([val[0]*-1,val[1]*-1])
           
        for blend in blendPair:
            tk.drvKeySet(valList=keyValuesMirrX,
                                  VattrDriver=self.joint2.rz, 
                                  VattrDriven=blend.color1R)
            tk.drvKeySet(valList=keyValuesMirrY,
                                  VattrDriver=self.joint2.rz, 
                                  VattrDriven=blend.color1G)
            pm.keyTangent(blend.color1R, itt='spline', ott='spline')
            pm.keyTangent(blend.color1G, itt='spline', ott='spline')
        
    
    def makeJointchain(self, numJoints):
        """creates the joint chain for the spline ik"""
        self.startEnd = tp.nullGroup(Vlist=[self.joint1, self.joint3], 
                                     Vhierarchy=1, Vtype=2, Vname='', 
                                     Vsuffix='', jointRadius=1)
        splitJoints = tj.split(Vlist=self.startEnd, numJoints=numJoints,  
                                     Vparent=1, Vtype=2, Vname='', Vprefix='', 
                                     Vsuffix='')
        
        splitJoints.append(self.startEnd[1])
        splitJoints.insert(0, self.startEnd[0])
        self.initialChain = splitJoints[:]
        
        return self.initialChain
    
        
    def sharpBend(self, midPosition):
        """
        Sharp bend setup.
     
        =========== =====================================================
        Arguments   Description
        =========== =====================================================
        midPosition Index for the middle joint.
        =========== =====================================================
        """
        self.sharpChain = tj.duplicateChain([self.startEnd[0]])
        self.drvChain = tj.duplicateChain([self.startEnd[0]])
        
        for i in range(len(self.sharpChain)):
            pm.rename(self.sharpChain[i], 'jnt_shrp' + str(i+1) + self.Vsuffix)
            pm.rename(self.drvChain[i], 'jnt_drv' + str(i+1) + self.Vsuffix)
        
        chainA = self.sharpChain[:midPosition]
        chainB = self.sharpChain[midPosition:]
        # align the chains, so the middle Joint matches joint2. 
        multAlign = pm.createNode('multiplyDivide', 
                                name='alignChainA'+self.Vsuffix)
        
        multAlign.operation.set (1)
        multAlign.input1.set([1, 1, 1])
                                    
        chainAscale = (td.get([self.joint1, self.joint2]) / 
                              td.get([chainA[0], chainB[0]]))
        chainBscale = (td.get([self.joint2, self.joint3]) / 
                              td.get([chainB[0], chainB[-1]]))
        
        multAlign.input2X.set(chainAscale)
        multAlign.input2Y.set(chainBscale)
        
        self.AttrObj.addAttr('soft_bend', defaultValue=1, min=0, max=1, 
                             keyable=1, at='double')
         
        tc.constraintIkFk(VikList=self.sharpChain, VfkList=self.initialChain, 
                          VdrvList=self.drvChain, Vattr=self.AttrObj.soft_bend, 
                          Vsuffix=self.Vsuffix, Voffset=1, Vctrl=self.AttrObj, 
                          blendScale=1)
        
        self.joint1.sx >> multAlign.input1X
        self.joint2.sx >> multAlign.input1Y
        
        for jnt in chainA:
            multAlign.outputX >> jnt.sx 
        
        for jnt in chainB:
            multAlign.outputY >> jnt.sx 
        
        pm.orientConstraint([self.joint2, chainB[0]], mo=0)
        
        self.sharpChain[0].setParent(self.joint1)
        self.drvChain[0].setParent(self.joint1)  
        
        # scale soft bend chain.-----------------------------------------------
        
        # (curve - average(scaleA, scaleB)
        # average(scaleA, scaleB)
        blendAB = pm.createNode('blendColors', name='blendAB%s' % (self.Vsuffix))
        self.joint1.sx >> blendAB.color1R
        self.joint2.sx >> blendAB.color2R
        
        # initial length relation
        distAB = td.get([self.joint1, self.joint2])
        distBC = td.get([self.joint2, self.joint3])
        blendAB.blender.set((1 / (distAB + distBC)) * distAB)
        

        # (curve - (averageAB)
        subScaleCrv = pm.createNode('plusMinusAverage', 
                                   name='subScaleCrv%s' % (self.Vsuffix))
        subScaleCrv.operation.set(2)
        subScaleCrv.addAttr('check', defaultValue=1, keyable=1, at='double')
        subScaleCrv.output1D >> subScaleCrv.check
        self.multSpn.outputX >> subScaleCrv.input1D[0]
        blendAB.outputR >> subScaleCrv.input1D[1]
        
        # (scale + overlap) 
        addOverlapA = pm.createNode('plusMinusAverage', 
                                   name='addOverlap%s' % (self.Vsuffix))
        addOverlapA.addAttr('check', defaultValue=1, keyable=1, at='double')
        addOverlapA.output1D >> addOverlapA.check 
        self.joint1.sx >> addOverlapA.input1D[0]
        subScaleCrv.output1D >> addOverlapA.input1D[1]
        
        addOverlapB = pm.createNode('plusMinusAverage', 
                                   name='addOverlap%s' % (self.Vsuffix))
        addOverlapB.addAttr('check', defaultValue=1, keyable=1, at='double')
        addOverlapB.output1D >> addOverlapB.check 
        self.joint2.sx >> addOverlapB.input1D[0]
        subScaleCrv.output1D >> addOverlapB.input1D[1]
        
        multAlignCrv = pm.createNode('multiplyDivide', 
                                name='multAlignCurve'+self.Vsuffix)
        
        # min and max can be unlocked if needed
        self.ctrlIK.addAttr('bend', defaultValue=0, min=0, keyable=1)
        self.ctrlIK.bend.setLocked(1)
        
        self.AttrObj.addAttr('soft_align', defaultValue=0, min=0, max=1, 
                             keyable=1)
        blendBendAlign = pm.createNode('blendColors', 
                                       name='blendBendAlign%s' % (self.Vsuffix))
        self.AttrObj.soft_align >> blendBendAlign.blender
        multAlignCrv.outputX >> blendBendAlign.color1R
        self.multSpn.outputX >> blendBendAlign.color2R
        
        multAlignCrv.outputY >> blendBendAlign.color1G
        self.multSpn.outputX >> blendBendAlign.color2G
        
        # mutiply the start-mid and mid-end length with the length of joint2 and joint3
        multAlignCrv.operation.set (1)
        multAlignCrv.input1.set([1, 1, 1])
    
        multAlignCrv.input2X.set(chainAscale)
        multAlignCrv.input2Y.set(chainBscale)

        chainInA = self.initialChain[:midPosition]
        chainInB = self.initialChain[midPosition:]
        
        addOverlapA.output1D >> multAlignCrv.input1X
        addOverlapB.output1D >> multAlignCrv.input1Y
        
        for jnt in chainInA:
            blendBendAlign.outputR >> jnt.sx 
        
        for jnt in chainInB:
            blendBendAlign.outputG >> jnt.sx 
    
    def cleanup(self):
        """Grouping, locking attributes and deleting unused."""
        self.gNT = pm.group(em=1, w=1, n='no_trans' + self.Vsuffix)
        self.gST = pm.group(em=1, w=1, n='scale_trans' + self.Vsuffix)
        self.gSNT = pm.group(em=1, w=1, n='scale_no_trans' + self.Vsuffix)
        
        pm.scaleConstraint(self.gST, self.gSNT, skip=['none', 'y', 'z'])
        self.connectScale(self.gSNT.sx)
        self.gSNT.setParent(self.gNT)
        
        self.jointChain[0].setParent(self.joint1)
        
        self.curve.setParent(self.gNT)
        self.handle.setParent(self.gNT)
        
        for cluster in self.cluster:
            cluster.setParent(self.gNT)
        
        self.distAB.parent([self.gST])
        self.distBC.parent([self.gST])
        
        self.distAB.delete()
        self.distBC.delete()
        
        for ctrl in [self.ctrlA, self.ctrlB]:
            attrList = [ctrl.sx, ctrl.sy, ctrl.sz,
                        ctrl.rx, ctrl.ry, ctrl.rz, 
                        ctrl.v,]
            for attr in attrList:
                attr.set (lock=1, keyable=0, channelBox=0)   
    

def mfSplineIkOnThreeJoints(Vlist=[], Vsuffix='', splits=8, blends=2):
    """
    MEL: mfSplineIkOnThreeJoints - all arguments passed
 
    creates: stretchy spline Ik with blender for shapes
 
    returns: Vlist

    ========== =====================================================
    Arguments  Description
    ========== =====================================================
    Vlist      three joints
    Vsuffix    suffix
    splits     number of split joints
    blends     number of shapes for the spline ik
    ========== =====================================================
  
    """
    Vlist = lsSl.lsSl(Vlist=Vlist, Vmin=3, Vmax=3, returnString=1)
    
    mel.mfSuffixWin()
    pm.textField('mfSuffixWinField', e=1, tx=Vsuffix)
    
    Vresult = mel.mfSplineIkOnThreeJoints(Vlist, Vsuffix, splits, blends)
    return Vresult


class clusterComponent():
    """
    Clusters all components of a curve, polygon or lattice
     and creates controller.
    """
    def __init__(self, Vlist=[], Vsuffix='', ctrl_radius=1, normal=[1,0,0]):
        
        Vlist = lsSl.lsSl(Vlist, 1, 0)
        
        
        for e in Vlist:
            self.shape = e.getShape()
            self.clusterLoc = []
            self.null_clusterLoc = []
            self.cluster = []
            self.ctrls = []
            components = None
            
            try:
                components = enumerate(self.shape.cv)
            except:
                try:
                    components = enumerate(self.shape.vtx)
                except:
                    try:
                        components = enumerate(self.shape.pt)
                    except:
                        pass
                
            for cmpon in components:
                cluster = (pm.cluster(cmpon[1], n=('Cluster'+Vsuffix)))
                clusterLoc = tp.nullGroup(Vlist=[cluster[1]], Vhierarchy=1, 
                                          Vtype=1, Vsuffix='_ctrl')[0]
                null_clusterLoc = tp.nullGroup(Vlist=[clusterLoc], Vhierarchy=3, 
                                          Vtype=1, Vsuffix='_null')[0]                         
                
                if ctrl_radius:
                    ctrl = tcv.controlShape([clusterLoc], radius=ctrl_radius, 
                                            normalX=normal[0], normalY=normal[1], 
                                            normalZ=normal[2], curveShape='sphere')
                
                self.ctrls.append(clusterLoc)
                self.clusterLoc.append(clusterLoc)
                self.null_clusterLoc.append(null_clusterLoc)
                self.cluster.append(cluster[1])
                
                pm.parentConstraint([clusterLoc, cluster])
                
            self.clusterGrp = pm.group(w=1, em=1, n='cluster' + Vsuffix)
            
            for o in self.cluster:
                self.clusterGrp|o
                
            self.ctrlGrp = pm.group(w=1, em=1, n='ctrl' + Vsuffix)
            
            for o in self.null_clusterLoc:
                self.ctrlGrp|o
            
            mf.builder.nodes.set(self.ctrls, 'transform_ctrl')
                
class clusterBezierCtls():
      
    def __init__(self, Vlist=[], Vsuffix='', ctrl_radius=1):
        """
        Creates controls for a bezier curve.
     
        ========== =====================================================
        Arguments  Description
        ========== =====================================================
        Vlist[0]   curve
        ========== =====================================================     
        """
        
        Vlist = lsSl.lsSl(Vlist, 1, 0)
        
        
        for e in Vlist:
            self.curveShape = e.getShape()
            self.clusterLoc = []
            self.null_clusterLoc = []
            self.cluster = []
            self.ctrls = []
            
            
            for cmpon in enumerate(pm.ls(str(self.curveShape) + '.cv[:]')[0]):
                cluster = (pm.cluster(cmpon[1], n=('Cluster'+Vsuffix)))
                clusterLoc = tp.nullGroup(Vlist=[cluster[1]], Vhierarchy=1, 
                                          Vtype=2, Vsuffix='_ctrl')[0]
                null_clusterLoc = tp.nullGroup(Vlist=[clusterLoc], Vhierarchy=3, 
                                          Vtype=1, Vsuffix='_null')[0]                         
                
                
                ctrl = tcv.ctrlCurve(Vtype='cube', Vname='ctrl')[0]
                ctrl.s.set(ctrl_radius, ctrl_radius, ctrl_radius)
                pm.makeIdentity(apply=True, t=0, r=0, s=1, n=0)
                
                VctrlShape = ctrl.history()[0]
        
                snap.snap([ctrl, clusterLoc])

                pm.parent(VctrlShape, clusterLoc, r=1, shape=1)
                pm.delete(ctrl)

                self.ctrls.append(clusterLoc.getParent())
                self.clusterLoc.append(clusterLoc)
                self.null_clusterLoc.append(null_clusterLoc)
                self.cluster.append(cluster[1])
                
                pm.parentConstraint([clusterLoc, cluster])
                
            self.clusterGrp = pm.group(w=1, em=1, n='cluster' + Vsuffix)
            
            for o in self.cluster:
                self.clusterGrp|o
                
            self.ctrlGrp = pm.group(w=1, em=1, n='ctrl' + Vsuffix)
            
            for o in self.null_clusterLoc:
                self.ctrlGrp|o
                
        firstCv = self.clusterLoc[0:2]
        lastCv = self.clusterLoc[-2:]
        lastCv.reverse()
        cvGps = [firstCv] +  self.chunks(self.clusterLoc[2:-2], 3) + [lastCv]

        for i in range(len(cvGps)):
            if len(cvGps[i]) == 2:
                cv = cvGps[i][0]
                tan = [cvGps[i][1]]
            else:
                cv = cvGps[i][1]
                tan = [cvGps[i][0], cvGps[i][2]]
            print cv, tan
            print '----------'

            cv.addAttr('tangent_ctls', min=0, max=1, k=1)

            cv.rename('cv_' + str(i) + Vsuffix)
            cv.getParent().rename('null_' + cv.nodeName())
            for j in range(len(tan)):
                tan[j].rename('cv_' + str(i) + '_tan_' + str(j+1) + Vsuffix)
                tan[j].getParent().rename('null_' + tan[j].nodeName())

                cv|tan[j].getParent()
                cv.s >> tan[j].inverseScale

                cv.getShape().overrideEnabled.set(1)
                cv.getShape().overrideColor.set(13)

                shapes = pm.ls(tan[j].getChildren(), shapes=1)

                for s in shapes:
                    pm.scale(s.cv , [0.7, 0.7, 0.7], ocp=1)
                    cv.tangent_ctls >> s.v
                
            

    def chunks(self, l, n):
        return [l[i:i+n] for i in range(0, len(l), n)]
    
                
            
                    
    
    
    