"""
general tools

"""
import os

from pymel import versions
import maya.cmds as cm
import pymel.core as pm
from mf.tools import lsSl
import mf.tools.file as ft
    
       
class reorderAttr():
    def __init__(self, Vlist=[], Vsuffix='', **kwdArgs):
        Vlist = lsSl.lsSl(Vlist, 1, 0)
        self.obj = Vlist[0]
        self.attrList = pm.listAttr(self.obj, userDefined=1)
    
    def reorder(self, newOrder):
        locked = []
        
        for name in newOrder:
            attr = pm.ls(str(self.obj) + '.' + name)[0]
            
            if attr.isLocked():
                attr.setLocked(0)
                locked.append(attr)
                
            if versions.current() >= versions.v2011:
                print attr
                pm.deleteAttr(attr)
                pm.undo()
            else:
                pm.renameAttr(attr, 'temp_attr_name')
                pm.renameAttr(attr, name)
        
        for attr in locked:
            attr.setLocked(1)
        pm.select(self.obj)
        
class reorderAttrUI():
    def __init__(self):
        try:
            pm.deleteUI('reorderAttrWin')
        except: pass
        with pm.window('reorderAttrWin', resizeToFitChildren=1, toolbox=1, s=0) as win:
            
            with pm.columnLayout():
                #pm.text(' prefix, name, suffix')
                
                pm.button( l='move up', c=self.moveUp)
                pm.button( l='move down', c=self.moveDown)
                pm.intField('reorderAttrIntField', v=1)
                          
        win.show()
    
    def moveUp(self, *args):
        self.move(-1)
        
    def moveDown(self, *args):
        self.move(1)
        
    def move(self, direction):
        for e in range(pm.intField('reorderAttrIntField', q=1, v=1)):
            attrNames = pm.channelBox('mainChannelBox', q=1, selectedMainAttributes=1)
            r = reorderAttr()
            newList = r.attrList[:]
            
            if direction == 1:
                newList.reverse()
                attrNames.reverse()
                for a in attrNames:
                    i = newList.index(a)
                    newList.insert(i-1, newList.pop(i))
        
                newList.reverse()
                
            else:   
                for a in attrNames:
                    i = newList.index(a)
                    newList.insert(i-1, newList.pop(i))
            print newList
                    
            r.reorder(newList)
        
    
class invAttr():
    """
    Creates a set range node that inverts the range of an attribute.
    """    
    def __init__(self, attr, Vsuffix=''):
        mn = attr.getMin() 
        mx = attr.getMax()
        self.setRange = pm.createNode('setRange', name='rangeOpen%s' % (Vsuffix))
        self.setRange.oldMinX.set(mn)
        self.setRange.oldMaxX.set(mx)
        self.setRange.minX.set(mx)
        self.setRange.maxX.set(mn)
        attr >> self.setRange.valueX
    
    def connect(self, destAttrList):
        for attr in destAttrList:
            self.setRange.outValueX >> attr
    
    def __call__(self):
        return self.setRange
    
    def setOutVal(self, min, max):
        self.setRange.minX.set(min)
        self.setRange.maxX.set(max)
        
        
def matchFosterAttributes():
    """
    Compares all channel box attributes of the foster object and 
    """
       

def filterNodeType(Vlist=[], type=''):
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    result = []
    for obj in Vlist:
        if obj.nodeType() == type:
            result.append(obj)
    return result
        
def listDeformersInHistory(Vlist=[]):
    """
    returns the deformers in the history of Vlist[0]
    """
    Vlist = lsSl.lsSl(Vlist, 1, 1)
    result=[]

    hist = Vlist[0].listHistory(leaf=1, historyAttr=1)

    for obj in hist:
        if filterDeformers([obj]) != '':
            result.append(filterDeformers([obj]))

    return result

def filterDeformers(Vlist=[], deformerList=['tweak', 'nonLinear', 'skinCluster', 'blendShape', 'ffd', 'wire', 'sculpt']):
    """
    returns Vlist[0] if it is a matching deformer, else returns an empty string
    """
    Vlist = lsSl.lsSl(Vlist, 0, 0)
    for deform in deformerList:
        if Vlist[0].nodeType() == deform:
            return Vlist[0]
    return ''

def reorderDeformerList(Vmesh, l, refList, obj):
    """
    reorders two given deformers, regarding to a reference list
    """
    lReverse = l[:]
    lReverse.reverse()
    lcopy = l[:]

    indRef = refList.index(obj.nodeType())
    ind = l.index(obj)
    for i in range(len(lReverse)):

        # get index in ref list:
        indItt = refList.index(lReverse[i].nodeType())
        
        if indRef > indItt:
            objB = lcopy[lcopy.index(lReverse[i])]
            # maya raises a warning than pymel raises an exception
            # the warning gets through and will be printed e.g.:
            # Warning: blendShape2 is not deforming pSphereShape1. #
            try:
                if lcopy.index(obj) == lcopy.index(objB)+1:
                    pass

                else:
                    pm.reorderDeformers(objB, obj, Vmesh)
                    print 'REORDERED: %s  %s>>%s'% (Vmesh, objB, obj)

                lcopy.insert(lcopy.index(lReverse[i])+1, obj)
                lcopy.remove(obj)
                return lcopy
            except:
                pass
    return lcopy

def itterReorderDeformerList(Vmesh, l, refList):
    """
    does reorderDeformerList for all deformers in list
    """
    result = l[:]
    for i in range(len(result)):
        result = reorderDeformerList(Vmesh, result, refList, l[i])
        
    #result = reorderDeformerList(result, refList, l[i])
    return result

def reorderDeforms(Vlist=[], refList=['tweak', 'nonLinear', 'wire', 'ffd', 'skinCluster', 'blendShape', 'sculpt']):
    """
    reorders the deformers on a Vlist
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    result=[]

    for i in range(len(Vlist)):

        defs = listDeformersInHistory([Vlist[i]])
        result.append(itterReorderDeformerList(Vlist[i], defs, refList))

    return result


def getChildren(Vlist=[]):
    """
    returns a sorted list of all childern
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    Vresult = []
    for i in range(len(Vlist)):
        children = pm.listRelatives(Vlist[i], allDescendents=1)
        children.append(Vlist[0])
        children.reverse()
        Vresult = Vresult + children
    
    return Vresult


def hideLockedAttr(Vlist=[]):
    """
    hides all locked attributes and returns them
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    Vresult = []
    
    for i in range(len(Vlist)):
        Vattr= pm.listAttr(Vlist[i], keyable=1, locked=1)        
        for j in range(len(Vattr)):    
            VattrObj = pm.ls ('%s.%s' % (Vlist[i], Vattr[j]))
            VattrObj[0].set (lock=1, keyable=0, channelBox=0)
            #print VattrObj 
            Vresult.append(VattrObj)

    return Vresult

def getShapes(Vlist=[], Vtype='any', printInfo=1):
    """
    gets shapes from a transform, with a type filter
    
    returns: list of transforms or shapes. list can be mixed. 
    
    ========== ================================================================
    Arguments  Description
    ========== ================================================================
    Vlist[]    list of objects
    Vtype      type filter for the shapes
    printInfo  print information about shapes
    ========== ================================================================
    """  
    Vlist = lsSl.lsSl(Vlist, 1, 0)  
    Vhist = []
    Vshapes = []
    VtypeOb = ''
    Vinfo = []
    Vresult = []
    
    for i in range(len(Vlist)):
        Vhist = Vlist[i].history()
        
        for i in range(len(Vhist)):
            Vshape = pm.ls (Vhist[i], shapes=1)
        
            if (len(Vshape)) > 0:
                Vshapes.append(Vshape[0])
                           
    for i in range(len(Vshapes)):
        VtypeOb = Vshapes[i].nodeType()       
       
        if VtypeOb == Vtype or Vtype == 'any':
            Vresult.append(Vshapes[i])
    
    if printInfo == 1:
        print '\n-----------------------------------------------------'              
        print ('\n%s shapes:' % (Vtype))
        for i in range(len(Vresult)):
            print '%s \t %s' % (Vresult[i].nodeType(), Vresult[i])
          
    return Vresult    


def deleteUnusedShapes(Vlist=[], Vtype='any', Vdel=1, checkCustomAttr=1, printInfo=1):
    """
    deletes shape nodes that don't have connections (inputs or outputs)
   
    returns: the deleted shapes
    
    =============== ===========================================================
    Arguments       Description
    =============== ===========================================================
    Vlist[]         list of objects or list of shapes
    Vtype           type filter for the shapes
    Vdel            delete shapes or just give information
    checkCustomAttr filter for custom attributes on the shapes
    printInfo       print information about shapes
    =============== ===========================================================
    
    EXAMPLE::
    
        Is used to delete the '*Orig' shapes that remain after deleting 
        a blendshape node.
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    Vshapes = getShapes(Vlist=Vlist, Vtype=Vtype, printInfo=printInfo)
    
    Vresult = []
    Vinfo = []
    VinfoNonDel = []
    
    for i in range(len(Vshapes)):
        Vinp=Vshapes[i].inputs()
        Voutp=Vshapes[i].outputs()
        Vcon=Vshapes[i].connections()
        VconnSize = ((len(Vinp))+(len(Voutp))+(len(Vcon)))
        
        customAttributes = pm.listAttr(Vshapes[i], userDefined=1) 
        hasCustomAttr = len(customAttributes)
        
        if checkCustomAttr == 1:
            VconnSize = (VconnSize + hasCustomAttr)
        
        if Vdel==0 and printInfo==1:
            print ('\nshape: %s\ninputs: %s\noutputs: %s\nconnections: %s\ncustom attributes: %s' % (Vshapes[i], Vinp, Voutp, Vcon, customAttributes))
            
        if VconnSize == 0:   
            Vresult.append(Vshapes[i])
            Vinfo.append('%s' % Vshapes[i])
        else:
            VinfoNonDel.append('%s' % Vshapes[i])
            
    if Vdel==1:
        pm.delete(Vresult)
        
        if printInfo == 1:
            print 'Delete unused shapes.'
            print '\nNOT DELETED:'
            for i in range(len(VinfoNonDel)):
                print VinfoNonDel[i]
                
            print '\nDELETED:'
            for i in range(len(Vinfo)):
                print Vinfo[i]
        
    return Vresult

def getParents(Vlist=[], Vsuffix=[]):
    
    Vlist = lsSl.lsSl(Vlist, 1, 1)
    
    try:
        parent = pm.listRelatives(Vlist[0], parent=1)[0]
    except:
        return []
    
    parents = [parent]
    
    while 1:
        try:
            parent = pm.listRelatives(parents[-1], parent=1)[0]
            parents.append(parent)

        except:
            break
            
    return parents
    

def isVisible(Vlist=[], Vsuffix=''):
    """
    Returns 0 if any parent or the object itself is invisible.
    
    Example::
    
        import mf.tools.general
        reload(mf.tools.general)
        list = pm.ls(sl=1)
        for o in list:
            print o + ' ' + str(mf.tools.general.isVisible([o]))
    """
    Vlist = lsSl.lsSl(Vlist, 1, 1)
     
    vis = Vlist[0].v.get()
    if vis:
        if Vlist[0].overrideEnabled.get():
            if not Vlist[0].overrideVisibility.get():
                return False
        
        parents = getParents([Vlist[0]])
        if parents == []:
            return vis
        
        for p in parents:
            if not p.v.get():
                vis = 0
                break
            elif p.overrideEnabled.get():
                if not p.overrideVisibility.get():
                    vis = 0
                    break
    return vis

def writeNodes(Vlist=[], Vsuffix='', Vpath=r'', force=0):
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    
    fullPath = os.path.abspath(Vpath)
    path = os.path.split(fullPath)[0]
    
    ft.checkPath(path)
    if force != 1:
        ft.fileExists(fullPath)
        
    f = open (fullPath, 'w')
    f.writelines ('# The first line is reserved for comments.')
        
    Vlist.sort()      
    for o in Vlist:    
        if o[1]:
            f.writelines ("\n" + str(o))

def readNodes(Vlist=[], Vsuffix='', Vpath=r'', namespace=''):
    fullPath = os.path.abspath(Vpath)
    path = os.path.split(fullPath)[0]
    result = []
    missing = []
    
    ft.checkPath(path)
        
    f = open (fullPath, 'r')
    
    lines = f.readlines()
    comment = lines[0].split('\n')[0]
    
    for line in lines[1:]:
        #print line.split('\n')[0]
        try:
            result.append(pm.ls(namespace + line.split('\n')[0])[0])
        except:
            missing.append(namespace + line.split('\n')[0])
    
    if not missing == []:
        print 'Objects not found in scene:'
        for o in missing:
            print o
        raise Exception('objects missing in scene') 
    pm.select(result)
    return result

def copyConnections(Vlist=[], Vsuffix=''):
    """
    Connects the second object to where the first one was connected. 
    """
    Vlist = lsSl.lsSl(Vlist, 2, 0)
    out = Vlist[0].outputs(plugs=1)
    for e in out:
        for p in e.inputs(plugs=1):
            if Vlist[1].hasAttr(p.name(includeNode=False)):
                print p.outputs(plugs=1)
                for a in p.outputs(plugs=1):
                    try:
                        Vlist[1].attr(p.name(includeNode=False)) >> a
                    except:
                        pm.warning(str(Vlist[1].attr(p.name(includeNode=False))) +
                                   ' Could not be connected to ' + a)
    
class unlock():
    def __init__(self, Vlist=[]):
        self.Vlist = lsSl.lsSl(Vlist, 1, 0)
        self.tranformAttributes = ['t', 'r', 's', 'sh',
                                   'tx', 'ty', 'tz',
                                   'rx', 'ry', 'rz',
                                   'sx', 'sy', 'sz',
                                   'shxy', 'shxz', 'shyz',
                                   'v']
        self.unlock()
        self.disconnect()

    def unlock(self):
        foul = []
        for o in self.Vlist:
            for e in self.tranformAttributes:
                attr = getattr(o, e)
                try:
                    attr.setLocked(0)
                except:
                    foul.append(attr)

    def disconnect(self):
        for o in self.Vlist:
            for a in ['t', 'r', 's',
                      'tx', 'ty', 'tz',
                      'rx', 'ry', 'rz',
                      'sx', 'sy', 'sz']:
                connection = cm.listConnections(o + '.' + a, c=1, d=1, p=1)
                if connection:
                    try:
                        cm.disconnectAttr(connection[1], connection[0])
                    except:
                        pass





