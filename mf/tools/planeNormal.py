"""
tools for getting the plane normal from three points 
(for joint orientation and aim constraints)
"""

import pymel.core as pm
import pymel.core.datatypes as dt
import lsSl
from mf.tools import parent as tp
from mf.tools import joint as tj

def getPlaneNormal(Vlist=[]):
    """
    given three objects A, B and C,
    it calculates the vectors AB and BC
    and from those the cross product
    
    returns: 
    the cross product
    """
    Vlist = lsSl.lsSl(Vlist, 3, 3)
    VposA = dt.Vector(pm.xform (Vlist[0], q=1, ws=1, rotatePivot=1))
    VposB = dt.Vector(pm.xform (Vlist[1], q=1, ws=1, rotatePivot=1))
    VposC = dt.Vector(pm.xform (Vlist[2], q=1, ws=1, rotatePivot=1))
    VectorA = VposA - VposB
    VectorB = VposC - VposB
    Vcross = dt.Vector(pm.mel.crossProduct(VectorA, VectorB, 1, 1))
    
    return Vcross
    
def aimCons(Vlist=[], aimVector=[1, 0, 0], upVector=[0, 0, 1], **kwargs):
    """
    sets the rotation for object B in a triangle.
    given three objects it calculates the cross product and uses an 
    aim constraint to set the rotation.
   
    returns: 
    the new rotation
    """
    Vlist = lsSl.lsSl(Vlist, 3, 3)
    Vcross = getPlaneNormal(Vlist)
    pm.delete(pm.aimConstraint (Vlist[2], Vlist[1], worldUpVector=Vcross, 
        aimVector=aimVector, upVector=upVector))
    Vrot = Vlist[1].r.get()
    return Vrot

def orientTriangle(Vlist=[], Vsuffix='', pointDownAxis=[1, 0, 0], 
                   straightenElbowAxis=[0, 0, 1]):
    """
    Orients a three joint chain. 
    
    =================== =======================================================
    Arguments           Description
    =================== =======================================================
    Vlist               Three objects, e.g. three joints in a chain.
    pointDownAxis       Axis that will point to the next object (elbow to hand).
    straightenElbowAxis Axis that will straighten the elbow. 
    =================== =======================================================
    """
    Vlist = lsSl.lsSl(Vlist, 3, 3)
    
    parentDict = tp.getParent(Vlist)
    tp.unParent(Vlist)
    
    childrenA = Vlist[0].getChildren(type='transform')
    childrenB = Vlist[1].getChildren(type='transform')
    childrenC = Vlist[2].getChildren(type='transform')
        
    for c in childrenA:
        c.setParent(w=1)
    for c in childrenB:
        c.setParent(w=1)
    for c in childrenC:
        c.setParent(w=1)
    
    for o in Vlist:
        
        if o.nodeType() == 'joint':
            o.jointOrient.set([0, 0, 0])
        o.r.set([0, 0, 0])
    
    aimCons(Vlist=Vlist, 
            aimVector=pointDownAxis, upVector=straightenElbowAxis)
    aimCons(Vlist=[Vlist[2],Vlist[0],Vlist[1]], 
            aimVector=pointDownAxis, upVector=straightenElbowAxis)
    
    for o in Vlist:
        if o.nodeType() == 'joint':
            o.jointOrient.set([0, 0, 0])
            tj.addRoToJo([o])
            
    tp.reParent(parentDict)
    
    if Vlist[2].nodeType() == 'joint':
        if Vlist[2].getParent() == Vlist[1]:
            Vlist[2].jointOrient.set([0, 0, 0])
        else:
            par = tp.getParent([Vlist[2]])
            print par
            Vlist[1]|Vlist[2]
            Vlist[2].jointOrient.set([0, 0, 0])
            tp.reParent(par)
    else:
        if Vlist[2].getParent() == Vlist[1]:
            Vlist[2].r.set([0, 0, 0])
        else:
            Vlist[2].r.set(Vlist[1].r.get())
            
    for c in childrenA:
        c.setParent(Vlist[0])
    for c in childrenB:
        c.setParent(Vlist[1])
    for c in childrenC:
        c.setParent(Vlist[2])
        
    pm.select(Vlist)
    
    