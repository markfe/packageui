"""
general tools for skinning
"""
from __future__ import with_statement

import os

import maya.OpenMaya
from maya.OpenMayaAnim import MFnSkinCluster
from maya.OpenMaya import MIntArray, MDagPathArray
import maya.mel as mel

from pymel import versions
import pymel.core as pm
import mf.tools.file as ft
import general as gt
import lsSl
import parent as tp
import cons as tc


class skinError(Exception): pass
class noClusterError(skinError): pass

   
def writeWeights(Vlist=[], Vpath=r'D://weights.txt', force=0):
    """
    ========= =========================================
    Arguments Description
    ========= =========================================
    Vlist[0]  mesh    
    or:
    Vlist[0]  mesh shape
    Vlist[1]  skin cluster
    Vpath     complete path
    Vfile     filename (can have any extension)
    ========= =========================================
    """
    Vlist = lsSl.lsSl(Vlist=Vlist, Vmin=1, Vmax=2)
    
    if len(Vlist) == 1:
        Vshape = gt.getShapes(Vlist=Vlist, printInfo=0)[0]

        try: 
            Vcluster = getSkinfromGeo(Vlist)[0]
        except: 
            raise noClusterError('no skin cluster found')
    else:
        Vshape = Vlist[0]
        Vcluster = Vlist[1]
    
    Vinfluences = pm.skinCluster (Vshape, q=1,  influence=1)
    VinfluencesStr = []
    
    for i in range(len(Vinfluences)):
        VinfluencesStr.append(str(Vinfluences[i]))

    Vpath = os.path.abspath(Vpath)
    #print os.path.split(Vpath)    
    ft.checkPath(os.path.split(Vpath)[0])
    if force != 1:
        ft.fileExists(Vpath)   
    f = open (Vpath, 'w')
    f.writelines ('%s\n' % (VinfluencesStr))
    
    # try for mesh-vertex, curve-cv, lattice-point
    try: 
        cmponList = Vshape.vtx
    except: 
        try: cmponList = Vshape.cv
        except:
            try: cmponList = Vshape.pt
            except: 
                raise Exception
    
    for i, cmpon in enumerate(cmponList):
        Vweights=[]
        indexList = []  
        
        weightListFull = pm.skinPercent(Vcluster, cmpon, v=1, q=1)
        
        for j in range(len(Vinfluences)):
            
            if weightListFull[j] != 0:   
                Vweights.append(weightListFull[j])
                indexList.append(Vinfluences.index(Vinfluences[j]))
        
        wi = [Vweights, indexList]
        f.writelines ('%s\n' % (wi))
    f.close()         
    return 

def readInfluences(Vpath=r'D://weights.txt'):
    """
    ========= =========================================
    Arguments Description
    ========= =========================================
    Vpath     complete path
    ========= =========================================
    """
    ft.checkPath(Vpath)
    f = open (Vpath, 'r')
    line = f.readline()
    cleanline = line.strip()
    Vinfluences = eval(cleanline)
    f.close()
    return Vinfluences

def readWeights(Vlist=[], Vpath=r'D://weights.txt',
                namespace='', fast=1):
    """
    ========= =========================================
    Arguments Description
    ========= =========================================
    Vlist[0]  mesh
    or:
    Vlist[0]  mesh shape
    Vlist[1]  skin cluster
    
    Vpath     complete path
    ========= =========================================

    TODO:
        return failed components
    """ 
    Vlist = lsSl.lsSl(Vlist=Vlist, Vmin=1, Vmax=2)
    
    if len(Vlist) == 1:
        Vshape= gt.getShapes(Vlist=Vlist, printInfo=0)[0]
        Vshape = gt.getShapes(Vlist=Vlist, printInfo=0)[0]
        Vcluster = getSkinfromGeo(Vlist)
        try: 
            Vcluster = getSkinfromGeo(Vlist)[0]
        except: 
            raise noClusterError('no skin cluster found')
        
        Vcluster.normalizeWeights.set(2)
        
    if versions.current() >= versions.v2011:
        if not Vcluster.skinningMethod.get() == 0:
            raise skinError('Import only works for classic linear skinning.')
        fast=1

    else:
        Vshape = Vlist[0]
        Vcluster = Vlist[1]
    
    Vpath = os.path.abspath(Vpath)
    ft.checkPath(Vpath)
    f = open (Vpath, 'r')
    
    
    #Vinfluences = eval(f.readline())
    Vinfluences = eval(f.readline().split('\r')[0])
        
    
    if not namespace == '':
        newname = []
        for i in Vinfluences:
            newname.append(namespace + i)
        Vinfluences = newname[:]
        
    #for name in Vinfluences:
    #    print name
        
    # set all weights to zero
    pm.skinPercent([Vcluster, Vshape], pruneWeights=100, normalize=0)
    
    # try for mesh-vertex, curve-cv, lattice-point
    try: 
        cmponList = Vshape.vtx
    except: 
        try: cmponList = Vshape.cv
        except:
            try: cmponList = Vshape.pt
            except: 
                raise Exception
       
    if fast:
        print '-- using fast skin import.'
        strCluster = str(Vcluster)
        jointInd = jointsToIndex(strCluster, Vinfluences)
    
        for i, cmpon in enumerate(cmponList):
            wi = eval(f.readline().split('\r')[0])    
            w = wi[0]
            ii = wi[1]
            tvPairs = []
            
            for j in range(len(w)):
                tvPairs.append((jointInd[ii[j]], w[j]))
            
            for tv in tvPairs:
                attr = strCluster + '.weightList[' + str(i) + '].weights[' + str(tv[0]) + ']'     
                pm.setAttr(attr, tv[1])   
        
    else:
        for i, cmpon in enumerate(cmponList):
            wi = eval(f.readline().split('\r')[0])    
            w = wi[0]
            ii = wi[1]    
            #wi = eval(f.readline())
            
            # make transform/value pairs ------------------------------------------
            tvPairs = []
            
            for j in range(len(w)):
                tvPairs.append((Vinfluences[ii[j]], w[j]))
            
            pm.skinPercent (Vcluster, cmpon, transformValue=tvPairs, normalize=0)

    f.close()       
    return Vinfluences

def applyWeights(Vlist=[], Vsuffix='', Vpath='', worldOrigJoints=0, editCluster=0):
    """
    Skins a mesh and collects all lost influences in a "lostInfluences" group.
    """
    Vlist = lsSl.lsSl(Vlist=Vlist, Vmin=1, Vmax=2)
    realJoints = []
    joints = readInfluences(Vpath)
    
    lstInf = []
    for joint in joints:
        
        lsJoint = pm.ls(joint)
        if lsJoint == []:
            pm.select(cl=1)
            lstInf.append(pm.joint(n=joint))
    
    if lstInf != []:
        lostInfGrp = pm.group(em=1, w=1, a=1, name='lostInfluences' + Vsuffix)
        
        for j in lstInf:
            j.setParent(lostInfGrp)
   
    for joint in joints:
        #print joint
        realJoints.append(pm.ls(joint)[0])
    
    
    namespace=''
    if worldOrigJoints:
        namespace = 'origin_'
        origJoints = []
        skinJoints = []
        for j in realJoints:
            origJoint = tp.nullGroup([j], Vhierarchy=1, 
                                     Vtype=2, Vprefix='skin_')[0]
            skinJoint = tp.nullGroup([j], Vhierarchy=1, 
                                     Vtype=2, Vprefix='origin_')[0]
            origJoints.append(origJoint)
            skinJoints.append(skinJoint)
            
            tc.cons([j, origJoint], Vtype=3)
            tc.cons([j, origJoint], Vtype=5)
            origJoint.t >> skinJoint.t
            origJoint.r >> skinJoint.r
            origJoint.s >> skinJoint.s
            
        realJoints = skinJoints[:]
        group = pm.group(w=1, em=1, n='no_trans_orJnts' + Vsuffix)
        for j in skinJoints:
            group|j
            
        group = pm.group(w=1, em=1, n='trans_orJnts' + Vsuffix)
        for j in origJoints:
            group|j
        
    if editCluster:
        skinCluster = getSkinfromGeo([Vlist[0].getShape()])
        print skinCluster
        if not skinCluster == []:
        
            for j in realJoints:
                try:
                    pm.skinCluster(skinCluster, e=1, dr=3, lw=False, wt=0, ai=j)
                except:
                    pass
        else:
            
            realJoints.append(Vlist[0])
            skinCluster = pm.skinCluster(realJoints, 
                                     ignoreHierarchy=1, 
                                     toSelectedBones=1)
            
            
    else:
        realJoints.append(Vlist[0])
        skinCluster = pm.skinCluster(realJoints, 
                                     ignoreHierarchy=1, 
                                     toSelectedBones=1)
        
    

    pm.select(cl=1)
    pm.select(Vlist)

    readWeights(Vlist, Vpath, namespace)
    try:
        gt.reorderDeforms(Vlist)
        print 'history on %s reordered' % (Vlist[0])
    except:
        pass
    
    pass

def skinMesh(Vlist=[], Vsuffix='', weightPath='', meshPath=''):
    """
    Imports a single mesh and skins it.
    """
    pm.importFile(meshPath, renameAll=1, renamingPrefix=('SKIN_mesh_'))
    meshes = pm.ls('SKIN_mesh_' + '*')
    meshObj = None
    
    for obj in meshes:
        if obj.nodeType() == 'transform':
            meshObj = obj
    
    applyWeights(Vlist=[meshObj], Vsuffix=Vsuffix, Vpath=weightPath)
    pm.rename(meshObj, 'mesh' + Vsuffix)   
    
           
def getSkinfromGeo(Vlist=[]):
    """
    Gets all skin cluster from a geometries history.
    """
    Vlist = lsSl.lsSl(Vlist=Vlist, Vmin=1, Vmax=1)
    try:
        cluster =  pm.ls(pm.mel.findRelatedSkinCluster(str(Vlist[0])))[0]
        return pm.ls(cluster)
    except:
        return [] 
   
def filterHistory(history, typeFilter='skinCluster'):
    """
    Filters a list of objects by a node type.
    """
    Vtype = history.nodeType()
    if Vtype == typeFilter:
        return(history)

def jointsToIndex(skinCluster, jointNames):
    """
    Finds the joint index for each joint name within the skin clusters weight
    attributes.
    """
    sel = maya.OpenMaya.MSelectionList()
    sel.add(skinCluster)     
    skinCluster = maya.OpenMaya.MObject()
    sel.getDependNode(0, skinCluster)
    
    skinFn = MFnSkinCluster( skinCluster )
 
    jApiIndices = {}
    _tmp = MDagPathArray()
    skinFn.influenceObjects( _tmp )
    for n in range( _tmp.length() ):
        jApiIndices[_tmp[n].partialPathName()] = skinFn.indexForInfluenceObject( _tmp[n] )
    
    result = []
    #print jApiIndices
    for j in jointNames:
        result.append(jApiIndices[j])
    return result

def pasteWeights(Vlist=[], Vsuffix=''):
    """
    Pastes from the last selected object to all other objects in the selection.

    =========== =========================================
    Arguments   Description
    =========== =========================================
    Vlist[0:-1] dest meshes
    Vlist[-1]   skinned source mesh
    =========== =========================================

    """
    Vlist = lsSl.lsSl(Vlist=Vlist, Vmin=2, Vmax=0)
    
    for e in Vlist[:-1]:
        oldCluster = getSkinfromGeo([Vlist[-1].getShape()])
        influences = pm.skinCluster(oldCluster, q=1, influence=1)
    
        pm.skinCluster(e, influences, tsb=1, ignoreHierarchy=1)
    
        pm.select([Vlist[-1], e])
        mel.eval('copySkinWeights  -noMirror -surfaceAssociation closestPoint -influenceAssociation name -influenceAssociation label -influenceAssociation label;')
        pm.select(Vlist)

def resetHistory(Vlist=[]):
    """
    Deletes the history and reskinnes the object.
    Note that all other history will be deleted.
    """
    Vlist = lsSl.lsSl(Vlist=Vlist, Vmin=1, Vmax=0)
    userPath = os.path.abspath(os.path.expanduser("~"))
    dirPath = os.path.join(userPath, 'maya', 'assetLoader')
    filePath = os.path.join(dirPath, 'skinWeights.txt')
    for e in Vlist:
        if pm.objExists(e):
            if os.path.exists(filePath):
                os.remove(filePath)
            if getSkinfromGeo([e.getShape()]):
                print 'S'
                writeWeights([e], filePath)
                pm.delete(e, ch=1)
                applyWeights(Vlist=[e], Vpath=filePath)
