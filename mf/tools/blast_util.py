import os
import maya.cmds as cm
import pymel.core as pm

class getNames():
    """
    Functions to return the filenames for the playblasts.
    The returned list of names should be the full path with the filename 
    but without extension.
    """
    def currentFile(self):
        """
        Get the path and filename of the current maya scene the playblast is
        launched in.
        """
        scenePath = pm.sceneName()
        self.filePath, mayaFile = os.path.split(scenePath)
        self.filename = os.path.splitext(mayaFile)[0]
    
    def default(self):
        self.currentFile()
        
        playblastPath = os.path.join(self.filePath, 'playblast')
        print playblastPath
        
        if not os.path.exists(playblastPath):
            os.mkdir(playblastPath)
            
        return [os.path.join(playblastPath, self.filename)]

        
class preScript():
    """
    Functions that will be run before the playblast.
    E.g to set up lighting.
    """
        
    def setShaders(self):
        shader = cm.ls(type='lambert')
        for s in shader:
            inc = cm.getAttr(s + '.incandescence')[0]
            i = inc[0]+inc[1]+inc[2]
            if i > 1.9:
                cm.setAttr(s + '.incandescence', 0, 0, 0,  type='double3')
                cm.setAttr(s + '.ambientColor', 1, 1, 1,  type='double3')
                cm.setAttr(s + '.diffuse', 0)


class postScript():
    """
    Functions that will be run after the playblast.
    E.g to delete lighting.
    """
    def deleteLightSetup(self):
        """
        Deletes the GB3 light setup.
        """
        if pm.objExists('light_playblast_setup'):
            pm.delete(pm.ls('light_playblast_setup'))
            print 'Light setup found and deleted after playblast.'
        else:
            print 'No light setup found to deleted after playblast.'

    
        