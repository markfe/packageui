"""
general file tools
"""
import os
import re
import shutil


class fileError(Exception): pass
class fileExistsError(fileError): pass
class noPathError(fileError): pass

    
def fileExists(Vpath):
    """
    checks if a file exists
    """

    if os.path.isfile(Vpath) !=0:
        raise fileExistsError ('file already exists: %s\n' % (Vpath))
 
    return Vpath
    
def checkPath(Vpath):
    """
    checks if a path does not exist
    """    
    if os.path.exists(Vpath) ==0:
            raise noPathError ('path does not exist: %s\n' % (Vpath))
    
    return (Vpath)

              
def makeFolder(Vpath, Vfolder):
    """
    creates folder under the given path
    """ 
    for i in range(len(Vfolder)): 
        checkPath(Vpath)
        if os.path.exists(Vpath + Vfolder[i]) == 0:
            os.mkdir('%s\%s' % (Vpath, Vfolder[i]))
            
    return

def makeFile(Vstring, Vpath='D:\weights.txt'):
    
    Vpath = fileExists(Vpath)
    f = open (Vpath, 'w')
    f.write(Vstring)
    f.close()

def readFile(Vpath='D:\weights.txt'):
    
    f = open (Vpath,'r')
    read_data = f.read()
    f.close()
    return read_data
    

def getSubdirs(path, allow_blanc_char=0):
    """
    Returns all non-hidden subdirs.
    """
    abspath = os.path.normpath(path)
    files = os.listdir(abspath)
    result = []
    for c in files:
        
        if not re.search(r'^\.', c):
            
            split=os.path.splitext(c)
            if split[1] == '' and os.path.isdir(os.path.join(abspath, split[0])):
                result.append(split[0])
        
        if not allow_blanc_char:
            if not c.find(' ') == -1:
                raise Exception('Folder "'+c+'" contains blank characters. Please use"_" instead.' )
    return result

def sortByExtension(VpathList, Vextension):
    Vresult = []
    for i in range(len(VpathList)):
        ext = os.path.splitext(VpathList[i])
        if ext[-1] == Vextension:
            Vresult.append(VpathList[i])
            
    return Vresult

def getVersion(Vpath, Vfile):
    """
    ========== =====================================================
    Arguments  Description
    ========== =====================================================
    Vpath      path in which to look for versions
    Vfile      filename with extension
    ========== =====================================================

    returns:
    (full path of the input file, new version after the latest)
    """
    checkPath(Vpath+'/'+Vfile)
    incPath = (Vpath+'/incrementalSave')
    
    if os.path.exists(incPath) == 0:
        try:     
            os.mkdir(incPath)
            print 'created folder: %s'%(incPath)
        except: pass
    
    Vversions = []
    VersInd = []
    Vsubdir = os.listdir(incPath)

    sourceBasename = os.path.splitext(Vfile)

    for filename in Vsubdir:

        if os.path.isfile(incPath+'/'+ filename) != 0:
            goalBasename = os.path.splitext(filename)
            num = os.path.splitext(goalBasename[0])

            if sourceBasename[0] == num[0] and sourceBasename[1] == goalBasename[1]:
                try:
                    v = int(num[-1].split('.')[1])
                    Vversions.append(filename)
                    VersInd.append(v)

                except: pass
    if Vversions != []:
        i = VersInd.index(max(VersInd))
        
        newName = '%s.%04d%s' % (sourceBasename[0],(v+1),sourceBasename[1])
    else:
        
        newName = sourceBasename[0]+'.0001'+sourceBasename[1]

    return (os.path.abspath(Vpath+'/'+Vfile), os.path.abspath(incPath+'/'+newName))

def makeVersion(source, copy):

    shutil.copy2(source, copy)
    return

def readInfo(path='', key='ch'):
    """
    Looks for a key in an info file and returns all following lines up to the 
    next blank line. Comments will be skipped.s
    """ 
    f = open(path, 'r')
    lines = []
    k = 0
    x = 1
    
    while x:
        line = f.readline()
        cleanLine = line.replace('\n', '').strip()
        
        if cleanLine == '':
            k = 0
        
        if k:
            #passes comments
            if re.search('^#', cleanLine):
                pass
            else:
                lines.append(cleanLine)
        
        if cleanLine == key:
            k = 1
        
        if not line:
            break
    f.close()

    return lines


def getFileFormat(file):
    """
    Returns the file format names for mayas file command.
    """
    ex = os.path.splitext(file)[1]
    type = None
    
    if ex == '.ma':
        type = "mayaAscii"
    elif ex == '.mb':
        type = "mayaBinary"
    elif ex == '.obj':
        type = "OBJ"
        
    return type
