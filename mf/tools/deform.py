"""
general tools for deformation
"""
import pymel.core as pm
import lsSl
import mf.tools.distance as td
import mf.tools.proxyGeo as pg
from mf.tools import key as tk
import mf.tools.parent as tp


class muscle(td.dist):
    """
    Simple "squashy" object that can be used as a kind of muscle.
    
    Example::
    
        import mf.tools.deform
        
        o = pm.ls(sl=1)#start, end, up, proxy
        c = mf.tools.deform.muscle(Vlist=[o[0], o[1]], Vsuffix='',edit=0)
        c.proxy([o[3]])
        c.calibrate()
        c.scale()
        c.upObject([o[2]])
    """
    
    def upObject(self, Vlist=[]):
        """
        Sets the Up-Object for the aim constraint. 
        """
        Vlist = lsSl.lsSl(Vlist, 1, 1, VerrorStr='- select one transform node')
        self.lock(0)
        pm.delete(self.aim)
        
        self.aim = pm.aimConstraint(self.goalObj[1], self.start,
            offset=[0, 0, 0,],
            weight=1,
            aimVector=[1, 0, 0],
            upVector=[0, 1, 0],
            worldUpType='object',
            worldUpVector=[0, 1, 0])
        
        self.aim.worldUpType.set(1)
        Vlist[0].worldMatrix >> self.aim.worldUpMatrix
        
    def proxy(self, Vlist=[]):
        """
        Creates a bind joint and proxy geometry.
        """
        Vlist = lsSl.lsSl(Vlist)
        prox = pg.proxyGeo([self.end, self.start, Vlist[0]], chain=1, 
                           customGeo=1, VcenterPivot=1, smoothLevel=1)
        self.connect([prox.pxNode.bj.sx])
        prox.pxNode.proxy.s.set([1,0.2,0.2])
        self.bj = prox.pxNode.bj
    
    def scale(self):
        """
        Sets driven keys for the x,y scaling.
        """
        tk.drvKeySet(valList=[
                              [2, 0.7],
                              [1, 1], 
                              [0.1, 2.3]],
                      VattrDriver=self.end.tx, 
                      VattrDriven=self.bj.sy)
        tk.drvKeySet(valList=[[2, 0.7],
                              [1, 1], 
                              [0.1, 2.3]],
                      VattrDriver=self.end.tx, 
                      VattrDriven=self.bj.sz)

class piston(td.dist):
    """
    Stretchy piston that rotates with its parent.
    """
    def freez(self):
        """
        Parents the distance to an empty group.
        """
        self.null = tp.nullGroup(Vlist=[self.start], Vhierarchy=3, 
                                 Vtype=1, Vprefix='null_')[0]
        
    def bindJoint(self):
        """
        Creates a bind joint an proxy.
        """
        self.calibrate(1)
        cubeBnd = pg.proxyGeo([self.start, self.end], VcenterPivot=0, chain=1, 
                              xScale=1, yzScaleGlobal=self.distance()*0.04,
                              side=0, label=str(self.start))
        
        self.connect([cubeBnd.pxNode.bj.sx])
        self.goalObj[0]|self.null

    
class averageVert():
    def __init__(self, Vlist=[], Vsuffix=''):
        """
        ========== =====================================================
        Arguments  Description
        ========== =====================================================
        Vlist[0]   loc
        Vlist[1:]  selection sets for vertices    
        ========== =====================================================
        """
        self.sl = lsSl.sl(Vlist, 9, 9,
                  slDict={
                          'loc':'0',
                          'set':'1:'
                          }, 
                          Vrename=1, Vsuffix=Vsuffix)
        shape = self.sl.loc.getShapes()[0]
        avNodes = []
        
        for set in Vlist[1:]:
            verts = pm.sets(set, q=True)
            avNode = pm.polyAverageVertex(verts, i=4, ch=1)[0]
            avNodes.append(avNode)

        shapes =  self.sl.loc.getShapes()
        shapes.remove(shape)
        pm.rename(shapes, 'poly' + Vsuffix)    
        
        

        
        
        
    