"""
general tools for curves 
"""
import random
import pymel.core as pm
import mf.builder.nodes
import lsSl
import snap
import mf.tools.proxyGeo as pg
import mf.tools.parent as tp

class ctrl():
    def __init__(self, Vlist=[], Vsuffix='', edit=0, **kwdArgs):
        """
        Creates and edits the transform_ctrl custom node.
        """
        if edit: self.edit(Vlist=Vlist)   
        else: self.create(**kwdArgs)
            
    def create(self, Vlist=[], **kwdArgs):
        """
        Creates the nodes.
        """
        self.ctrl = pm.group(**kwdArgs) 
        self.ctrl.addAttr(mf.builder.nodes.register('transform_ctrl')[0])
        self.addAttributes()

    def edit(self, Vlist=[]):
        """
        Finds all nodes.
        """
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.ctrl = Vlist[0]
        self.addAttributes()
    
    def addAttributes(self):
        """
        Adds all custom attributes.
        Rule::
        
            [1,1,1],[1,1,1],[1,1,1]
        """
        if not self.ctrl.hasAttr('side'):
            self.ctrl.addAttr("side" ,at="enum", en="Center:Left:Right:None:",
                              dv=3)
        if not self.ctrl.hasAttr('mirror_partner'):
            self.ctrl.addAttr('mirror_partner', at='message')
        if not self.ctrl.hasAttr('mirror_rule'):
            self.ctrl.addAttr('mirror_rule',  dt='string')
            self.ctrl.mirror_rule.set('[1,1,1],[1,1,1],[1,1,1]')
    
    def set_partner(self, Vlist=[]):
        """
        Set the mirror_partner.
        """
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.ctrl.mirror_partner >> Vlist[0].mirror_partner
    
    def remove_partner(self, Vlist=[]):
        """
        Set the mirror_partner.
        """
        partner = self.get_partner()
        if partner:
            try:
                self.ctrl.mirror_partner // partner.mirror_partner
            except:
                try:
                    partner.mirror_partner // self.ctrl.mirror_partner
                except:
                    pass
    
    def get_partner(self):
        """
        Get the mirror_partner.
        """
        partner = self.ctrl.mirror_partner.connections(plugs=0)
        if partner:
            return partner[0]
    
    def set_side(self, side=3):
        """
        Sets the side of a control (0=center, 1=left, 2=right, 3=none).
        """
        self.ctrl.side.set(side)
    
    def get_side(self):
        return self.ctrl.side.get()
        
    def set_rule(self, rule):
        self.ctrl.mirror_rule.set(str(rule)[1:-1])
        partner = self.get_partner()
        if partner:
            partner.mirror_rule.set(str(rule)[1:-1])
        
    def get_rule(self):
        return eval(self.ctrl.mirror_rule.get()+',[1,1,1]')
    
    def ctls_match(self, Vlist=[]):
        """
        Makes a connection between two ctls to mark them as matching for 
        mirroring.
        """
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.ctrl.mirror_partner >> Vlist[1].mirror_partner
        
    def getKeyable(self):
        """
        Returns a list of all keyable attributes in the channelbox of the
        ctrl.
        """
        attr = []
        attrNames = pm.listAttr(self.ctrl, k=1)
        
        for n in attrNames:
            attr.append(pm.ls(self.ctrl + '.' + n)[0])
        
        return attr
    
    def cleanupChannels(self, verbosity=0):
        """
        Hides undesired attributes types in the channel box.
        """
        attrList = ['jointOrientX', 'jointOrientY', 'jointOrientZ', 
                    'radius', 
                    'visibility']
        
        for a in attrList:
            
            if self.ctrl.hasAttr(a):
                attr = pm.ls(str(self.ctrl) + '.' + a)[0]
                pm.setAttr(attr, cb=0)
                pm.setAttr(attr, k=0)
                
                if verbosity:
                    print 'set keyable(0) on: ' + str(attr)
                                 
    def cleanupLocked(self, verbosity=1):
        """
        Hides locked attributes.
        """
        ud = pm.listAttr(self.ctrl, locked=1, ud=1)
        locked = pm.listAttr(self.ctrl, locked=1)
        
        for a in ud:
            locked.remove(a)
        
        for a in locked:
            attr = pm.ls(str(self.ctrl) + '.' + a)[0]
            attr.set (lock=1, keyable=0, channelBox=0)           
            
    def setScale(self, val):
        """
        Resizes the ctrl curve.
        """      
        shapes = pm.ls(self.ctrl.getChildren(), shapes=1)
        for s in shapes:
            pm.scale(s.cv , val, cp=1)
    
    def getShapes(self):
        """
        Returns a list of all shapes or an empty list if there are none.
        """      
        return pm.ls(self.ctrl.getChildren(), shapes=1)
        
                  
def ctrlCurve(Vtype='cube', Vname='ctrl'):
    """
    creates: different ctrl shapes

    returns: the curve

    ========== =====================================================
    Arguments  Description
    ========== =====================================================
    Vtype      curve type
    Vname      name
    ========== =====================================================
    """   
    Vcurve = []
    cvLayout = []
    
    if Vtype == 'cube':       
        cvLayout =[(0.5, 0.5, 0.5), (0.5, 0.5, -0.5), (-0.5, 0.5, -0.5), 
                   (-0.5, -0.5, -0.5), (0.5, -0.5, -0.5), (0.5, 0.5, -0.5), 
                   (-0.5, 0.5, -0.5), (-0.5, 0.5, 0.5), (0.5, 0.5, 0.5), 
                   (0.5, -0.5, 0.5), (0.5, -0.5, -0.5), (-0.5, -0.5, -0.5),
                   (-0.5, -0.5, 0.5), (0.5, -0.5, 0.5), (-0.5, -0.5, 0.5), 
                   (-0.5, 0.5, 0.5)]
   
    if Vtype == 'sphere':
        cvLayout =[(0, .5, 0), (0, .33, -.33), (0, 0, -.5), (0, -.33, -.33), 
                   (0, -.5, 0), (0, -.33, .33), (0, 0, .5), (0, .33, .33), 
                   (0, .5, 0), (.33, .33, 0), (.5, 0, 0), (.33, -.33, 0), 
                   (0, -.5, 0), (-.33, -.33, 0), (-.5, 0, 0), (-.33, .33, 0), 
                   (0, .5, 0)]

    if Vtype == 'circle':
        cvLayout =[[0.783611624891, 0, -0.783611624891],
                    [-0, 0, -1.10819418755],
                    [-0.783611624891, 0, -0.783611624891],
                    [-1.10819418755, 0, 0],
                    [-0.783611624891, 0, 0.783611624891],
                    [0, 0, 1.10819418755],
                    [0.783611624891, 0, 0.783611624891],
                    [1.10819418755, 0, 0],
                    [0.783611624891, 0, -0.783611624891]]

    if Vtype == 'locator':
        cvLayout =[ [-0.5, 0, 0],
                    [-0, 0, 0],
                    [0.5, 0, 0],
                    [0.5, 0, 0],
                    [0, 0, 0],
                    [0, -0.5, 0],
                    [0, 0, 0],
                    [0, 0.5, 0],
                    [0, 0.5, 0],
                    [0, 0, 0],
                    [0, 0, 0.5],
                    [0, 0, 0],
                    [0, 0, -0.5]]
     
    Vcurve = pm.ls (pm.curve(d=1, p=cvLayout, name='%s' % (Vname)))
                       
    pm.rename (Vcurve[0].history(), "%sShape" % (Vname))
    return Vcurve


def aimLine(Vlist=[], Vsuffix=''):
    """
    creates: a clustered reference curve between two objects

    returns: the curve

    ========== =====================================================
    Arguments  Description
    ========== =====================================================
    Vlist      [start object, end object]
    Vsuffix    name of the curve
    ========== =====================================================
    """  
    Vlist = lsSl.lsSl(Vlist, 2, 3, 
                      VerrorStr='\nselect: \n\tobject start, \n\tobject end')
 
    Vcurve = pm.ls(pm.curve (d=1, p=[(0,0,0), (0,0,-1)], n='line' + Vsuffix))
    pm.rename (Vcurve[0].history(), "line%sShape" % (Vsuffix))
    
    VclusterA = pm.cluster('%s.cv[0]' % Vcurve[0], n='cluster1%s' % (Vsuffix))
    VclusterB = pm.cluster('%s.cv[1]' % Vcurve[0], n='cluster2%s' % (Vsuffix))

    snap.snap([VclusterA[1], Vlist[0]])
    snap.snap([VclusterB[1], Vlist[1]]) 
    
    Vlist[0]|VclusterA[1]
    Vlist[1]|VclusterB[1]
    
    Vcurve[0].template.set(1) 
  
    return Vcurve


def controlShape(Vlist=[], normalX=1, normalY=0, normalZ=0, radius=1.2, 
                 Vhistory=0, center=[0, 0, 0], curveShape=''):
    """
    parents ctrl shapes (nurbs circle) to a given object list
    returns: the shapes
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0, ['any'], 
                      VerrorStr='\nselect: \n\tobject start, \n\tobject end')
    
    Vresult = []
    for i in range(len(Vlist)):
        
        if not curveShape:
            Vctrl = pm.circle(ch=Vhistory, fp=[0,0,0], sw=360, 
                           nr=[normalX,normalY,normalZ], center=center,
                           r=radius, n=('%s_ctrl' % (Vlist[i])))
        else:
            Vctrl = ctrlCurve(Vtype=curveShape, Vname=('%s_ctrl' % (Vlist[i])))
            Vctrl[0].s.set([radius, radius, radius])
            pm.makeIdentity(Vctrl[0], apply=True, t=0, r=0, s=1, n=0, pn=1)
        
        VctrlShape = Vctrl[0].history()[0]
        
        snap.snap([Vctrl[0], Vlist[i]])
        pm.parent(VctrlShape, Vlist[i], r=1, shape=1)
        pm.delete (Vctrl[0])
        
        Vresult.append(VctrlShape)
        
    return Vresult
    

class attach():
    def do(self, Vlist=[], Vsuffix='', rotation=True):
        Vlist = lsSl.lsSl(Vlist, 2, 0, ['any'], 
                      VerrorStr='\nselect: \n\tobject start, \n\tobject end')
        
        curve = Vlist[0]
        trans = Vlist[1]
        mp = pm.createNode('motionPath')
        mp.rotateOrder >> trans.rotateOrder
        mp.message >> trans.specifiedManipLocation
        if rotation:
            mp.rotateZ >> trans.rotate.rotateZ
            mp.rotateY >> trans.rotate.rotateY
            mp.rotateX >> trans.rotate.rotateX
        mp.allCoordinates >> trans.translate
        mp.fractionMode.set(1)
        mp.worldUpType.set(1)
        curve.getShape().worldSpace >> mp.geometryPath
        
        return mp

class attachLight():
    def __init__(self, Vlist=[], Vsuffix='', num=10):
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.j = []
        self.gNT = tp.noTrans(em=1, w=1, n='no_trans' + Vsuffix).group
        unit = pm.currentUnit( q=1, l=1)
        #unitMult = 100
        unitMult = 1
        for i in range(num):
            if i == 0:
                uVal = 0.00001 * unitMult
            elif i == num - 1:
                uVal = 0.00999 * unitMult
            else:
                uVal = (0.01 / (num - 1)) * (i) * unitMult


            print uVal
            pm.select(cl=1)
            jnt = pm.joint(n='p_' + str(i) + Vsuffix)
            self.j.append(jnt)

            mPath = attach().do([Vlist[0], jnt], rotation=False)
            mPath.addAttr('U')
            mPath.U.set(uVal)
            mPath.U >> mPath.uValue
            #mPath.uValue.set(uVal, force=1)
            self.gNT|jnt


class attatchTo():
    
    def __init__(self, Vlist=[], Vsuffix='', num=10, keepSpacing=1, objectRotationUp=0):        
        """
        Creates joints, distributed on a curve with a motion path. 
        Rotation is set to an up object.

        ========== =====================================================
        Arguments  Description
        ========== =====================================================
        Vlist[0]   curve
        Vlist      up object 
        ========== =====================================================
        
        .. note::
            Undo is not supported for the ``pathAnimation`` command.
        """
        # unit conversion nodes do not work automatically.
        unit = pm.currentUnit( q=1, l=1)
        unitMult = 100
#         if unit == 'cm':
#             unitMult = 100
#         if unit == 'mm':
#             unitMult = 1000
        
        Vlist = lsSl.lsSl(Vlist, 2, 2)
        pm.select(cl=1)
        self.scaleGrp = pm.group(em=1, w=1, n='scale'+Vsuffix)
        self.j = []
        crvInf = pm.createNode('curveInfo')
        Vlist[0].getShape().worldSpace[0] >> crvInf.inputCurve
        curveLen = crvInf.arcLength.get()
        multScale = pm.createNode('multiplyDivide',
                                  n=("mult_ScaleAll" + Vsuffix))
        multSpn = pm.createNode('multiplyDivide',
                                n=("mult_strSp" + Vsuffix))        
        multSpn.operation.set(2)
        #crvInf.arcLength >> multSpn.input1X
        
        crvInf.arcLength >> multScale.input1X
        
        multSpn.input2X.set(curveLen)
        
        try:
            Vlist[1].addAttr('trail_center', defaultValue=0.5, min=0, max=1, 
                             keyable=1)
            Vlist[1].addAttr('trail_distribute', defaultValue=1, min=0, max=3, 
                             keyable=1)
        except:
            pass
        
        rangeDist = pm.createNode('setRange', name='rangeDist' + Vsuffix)
        rangeDist.minX.set (0.0001)
        rangeDist.maxX.set (curveLen*3)
        rangeDist.oldMinX.set (0)
        rangeDist.oldMaxX.set (3)
        Vlist[1].trail_distribute >> rangeDist.valueX
        #rangeDist.outValueX >> multSpn.input2X
        
        if keepSpacing:
            rangeDist.outValueX >> multSpn.input2X
            multScale.outputX >> multSpn.input1X
          
        else:
            multSpn.input1X.set(1)
            Vlist[1].trail_distribute >> multSpn.input2X
        
        self.allPos = []
        self.gNT = tp.noTrans(em=1, w=1, n='no_trans' + Vsuffix).group
        for i in range(num):
            if i == 0:
                uVal = 0.00001*unitMult
            elif i == num-1:
                uVal = 0.00999*unitMult
            else:
                uVal=(0.01/(num-1))*(i)*unitMult
                   
            pm.select(cl=1)
            jnt = pm.joint(n='p_' + str(i) + Vsuffix)
            self.j.append(jnt)

            mPath = attach().do([Vlist[0], jnt])
            #mPath.uValue.set(uVal)
            if objectRotationUp:
                mPath.worldUpType.set(2)
            Vlist[1].worldMatrix[0] >> mPath.worldUpMatrix
            
            pg.proxyGeo([jnt], VcenterPivot=1, chain=0, xScale=0.5, 
                        yzScaleGlobal=.2, Vsides=6, shader=1, shaderName='shader'+Vsuffix,
                        Vcolor=[random.random(), random.random(), random.random()])
            
            self.scaleGrp.s >> jnt.s
            
            self.gNT|jnt
            
            self.multU = pm.createNode('multiplyDivide',
                                   n=("mult_U" + Vsuffix))
            self.multU.operation.set(2)
            self.multU.input1X.set(uVal)
            multSpn.outputX >> self.multU.input2X
            self.allPos.append(self.multU.outputX)
            self.multU.outputX >> mPath.uValue
            #fix unit conversion nodes
            inp = mPath.uValue.inputs()[0]
            if inp.type() == "unitConversion":
                inp.conversionFactor.set(1)
            
            addKeysA = pm.createNode('plusMinusAverage', 
                                      name='addKeysA' + Vsuffix)
            addKeysA.addAttr('input_1', defaultValue=0, keyable=1)
            addKeysA.addAttr('input_2', defaultValue=0, keyable=1)
            addKeysA.input_1 >> addKeysA.input1D[0]
            addKeysA.input_2 >> addKeysA.input1D[1]
            addKeysA.input_1.set(uVal-1)
            multSpn.outputX >> addKeysA.input_2
            
            blendA = pm.createNode('blendColors', name='blend_'+ Vsuffix)
            addKeysA.output1D >> blendA.color1R
            blendA.color2R.set(uVal)
            
            blendA.outputR >> self.multU.input1X
            
            Vlist[1].trail_center >> blendA.blender
        self.gNT|self.scaleGrp
            
            
            


