"""
general tools for keyframes and driven keys
"""
import pymel.core as pm 
import mf.tools.lsSl as lsSl


class reconnectAnim():
    
    def __init__(self, connectSelected=0):
        pl = pm.ls(type='reference')
        placeHolderAttr = []
        for e in pl:
            #print e
            try:
                placeHolderAttr.append(e.placeHolderList)
            except:
                pass
        
        if not connectSelected:
            for e in placeHolderAttr:
                #print e
                self.conn(e.connections())
        else:
            self.conn(pm.ls(sl=1))
                
            
    def conn(self, placeHolderAttr):
        c = placeHolderAttr
        #print c
        failList = []
        
        for e in c:
            splits = e.split('_')
            attrNames = []
            for i in range(len(splits)):
                attrNames.append('_'.join(splits[:i]) + '.'  + '_'.join(splits[i:]))
            #print attrNames
            attrNames.reverse()
            success = 0
            
            try:
                print '#', e.output.connections()
            except:
                pass
            
            for attr in attrNames:
                #print attr[:-1]
                try:
                    e.output >> pm.ls(attr)[0]
                    success = 1
                except:
                    try:
                        e.output >> pm.ls(attr[:-1])[0]
                        success = 1
                    except:
                        pass
                
            if not success:
                    failList.append(e)
                
        print '--------------------------------------\nThese keys could not be connected:'    
        print len(failList)
        for f in failList:
            print f
        print '--------------------------------------'

class character():
    def __init__(self):
        pm.character('character1')
        

def drvKeySet(valList, VattrDriver, VattrDriven):
    """
    Sets driven keyframes.

    =========== =========================================
    Arguments   Description
    =========== =========================================
    valList     list of value pairs for driver and driven [driverVal, drivenVal]
    VattrDriver driver attribute
    VattrDriven driven attribute
    =========== =========================================
    """
    for i in range(len(valList)):
        pm.setDrivenKeyframe(VattrDriven, driverValue=valList[i][0], 
                          value=valList[i][1], cd=VattrDriver)
               
def getKeyVal(VkeyedAttr, Vtime=(1,1)):
    """
    Returns a key value, given time and an attribute.
    """
    pm.keyframe
    pm.selectKey(cl=1)
    pm.selectKey(VkeyedAttr, t=Vtime)
    Vresult = pm.keyframe(q=1, sl=1, vc=1)
    pm.selectKey(cl=1)
    
    return Vresult

def drivenKeyBs(Vlist=[], Vsuffix='', angle=60):  
    """
    Creates driven key attributes to connect blendshapes. 
    """
    Vlist[0].addAttr('rotBDrvX', defaultValue=0, keyable=1)
    Vlist[0].addAttr('rotBDrvY', defaultValue=0, keyable=1)
    Vlist[0].addAttr('rotBDrvZ', defaultValue=0, keyable=1)
    
    drvKeySet(valList=[[0, 0], 
                          [angle, 1]], 
                          VattrDriver=Vlist[0].rx, 
                          VattrDriven=Vlist[0].rotBDrvX)
    
    drvKeySet(valList=[[0, 0], 
                          [angle, 1]], 
                          VattrDriver=Vlist[0].ry, 
                          VattrDriven=Vlist[0].rotBDrvY)
    
    drvKeySet(valList=[[0, 0], 
                          [angle, 1]], 
                          VattrDriver=Vlist[0].rz, 
                          VattrDriven=Vlist[0].rotBDrvZ)
    
    Vlist[0].addAttr('rotDrvX', defaultValue=0, keyable=1)
    Vlist[0].addAttr('rotDrvY', defaultValue=0, keyable=1)
    Vlist[0].addAttr('rotDrvZ', defaultValue=0, keyable=1)
    
    drvKeySet(valList=[[0, 0], 
                          [-angle, 1]], 
                          VattrDriver=Vlist[0].rx, 
                          VattrDriven=Vlist[0].rotDrvX)
    
    drvKeySet(valList=[[0, 0], 
                          [-angle, 1]], 
                          VattrDriver=Vlist[0].ry, 
                          VattrDriven=Vlist[0].rotDrvY)
    
    drvKeySet(valList=[[0, 0], 
                          [-angle, 1]], 
                          VattrDriver=Vlist[0].rz, 
                          VattrDriven=Vlist[0].rotDrvZ)
    
    

def setPose(driverAttr='ctrl.tx', VdriverVal=1, Vlist=[], scaleSub=-1):
    """
    Sets a driven key of all Vlist objects with the defined driver attribute 
    and value.
    
    ========= =========================================
    Arguments Description
    ========= =========================================
    scaleSub  Subtract 1 from scale to avoid accumulative scaling.
    ========= =========================================

    .. Note::
        make a dummy object with default values
    """
    Vlist = lsSl.lsSl(Vlist, Vmin=1, Vmax=0)
    driverAttr = pm.ls(driverAttr)
    
    for i in range(len(Vlist)):
        
        attrList = [
                    Vlist[i].tx,
                    Vlist[i].ty,
                    Vlist[i].tz, 
                
                    Vlist[i].rx,
                    Vlist[i].ry,
                    Vlist[i].rz,
                    
                    Vlist[i].sx,
                    Vlist[i].sy,
                    Vlist[i].sz
                    ]
        
        # get the current values and adjust them
        valList = [
                   Vlist[i].tx.get(),
                   Vlist[i].ty.get(),
                   Vlist[i].tz.get(),
            
                   Vlist[i].rx.get(),
                   Vlist[i].ry.get(),
                   Vlist[i].rz.get(),  
                
                   (Vlist[i].sx.get())+scaleSub,
                   (Vlist[i].sy.get())+scaleSub,
                   (Vlist[i].sz.get())+scaleSub
                   ]
        
        for j in range(len(attrList)):
            print 
                         
            drvKeySet([[VdriverVal, valList[j]]], driverAttr[0], attrList[j])
          
def mirrorValues(Vlist=[], tMult=[1,1,1], rMult=[1,1,1], sMult=[1,1,1]):
    """
    Multiplies the values of the first object and sets them on the 
    second object.
    """   
    Vlist = lsSl.lsSl(Vlist, Vmin=2, Vmax=2)
    t = Vlist[0].t.get()
    r = Vlist[0].r.get()
    s = Vlist[0].s.get()
    
    Vlist[1].t.set([t[0]*tMult[0], t[1]*tMult[1], t[2]*tMult[2]])
    Vlist[1].r.set([r[0]*rMult[0], r[1]*rMult[1], r[2]*rMult[2]])
    Vlist[1].s.set([s[0]*sMult[0], s[1]*sMult[1], s[2]*sMult[2]])
    
    
    
    
    