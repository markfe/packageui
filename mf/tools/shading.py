"""
general shading tools
"""

import os
import random
import pymel.core as pm
import lsSl
import mf.tools.snap as snap
import mf.tools.parent as tp


class HSVControlls():
    """
    """
    def __init__(self, Vlist=[], Vsuffix=''):
        Vlist = lsSl.lsSl(Vlist, 1, 0)
        self.suffix = Vsuffix
        Vlist[0].addAttr('color_H', defaultValue=0, min=0, max=360, keyable=1, at='float')
        Vlist[0].addAttr('color_S', defaultValue=0, min=0, max=1, keyable=1, at='float')
        Vlist[0].addAttr('color_V', defaultValue=0, min=0, max=1, keyable=1, at='float')
        Vlist[0].addAttr('specular', defaultValue=0, min=0, max=1, keyable=1, at='float')
        Vlist[0].addAttr('toony', defaultValue=0, min=0, max=1, keyable=1, at='float')
        
        blend = pm.createNode('blendColors', name='blend' + self.suffix)
        blend2 = pm.createNode('blendColors', name='blend2' + self.suffix)
        
        hsvToRgb = pm.createNode('hsvToRgb', n='hsvToRgb1' + self.suffix)
        network = pm.createNode('network', n='network' + self.suffix)
        
        network.addAttr('colorR', defaultValue=0, keyable=1, at='float')
        network.addAttr('colorG', defaultValue=0, keyable=1, at='float')
        network.addAttr('colorB', defaultValue=0, keyable=1, at='float')
        
        network.addAttr('incR', defaultValue=0, keyable=1, at='float')
        network.addAttr('incG', defaultValue=0, keyable=1, at='float')
        network.addAttr('incB', defaultValue=0, keyable=1, at='float')
        
        Vlist[0].toony >> blend.blender 
        Vlist[0].toony >> blend2.blender 
        
        hsvToRgb.outRgbB >> blend.color1R
        hsvToRgb.outRgbG >> blend.color1G
        hsvToRgb.outRgbR >> blend.color1B
        
        hsvToRgb.outRgbB >> blend2.color2R
        hsvToRgb.outRgbG >> blend2.color2G
        hsvToRgb.outRgbR >> blend2.color2B
        
        blend.color2R.set(0)
        blend.color2G.set(0)
        blend.color2B.set(0)
        
        blend2.color1R.set(0)
        blend2.color1G.set(0)
        blend2.color1B.set(0)
        
        blend2.outputR >> network.colorR
        blend2.outputG >> network.colorG
        blend2.outputB >> network.colorB
        
        shaderName = 'shader' + self.suffix
        Vshader = pm.shadingNode ('blinn', asShader=1, n='shaderName')
        VshadingGrp = pm.sets (renderable=1, noSurfaceShader=1, empty=1, 
                               name='%sSG' % (shaderName))
        Vshader.outColor >> VshadingGrp.surfaceShader
        
        network.colorR >> Vshader.colorB
        network.colorG >> Vshader.colorG
        network.colorB >> Vshader.colorR
        
        Vlist[0].specular >> Vshader.specularColorR
        Vlist[0].specular >> Vshader.specularColorG
        Vlist[0].specular >> Vshader.specularColorB
        
        Vlist[0].color_H >> hsvToRgb.inHsvR
        Vlist[0].color_S >> hsvToRgb.inHsvG
        Vlist[0].color_V >> hsvToRgb.inHsvB
        
        blend.outputR >> network.incR 
        blend.outputG >> network.incG 
        blend.outputB >> network.incB 
        
        network.incB >> Vshader.incandescenceR
        network.incG >> Vshader.incandescenceG
        network.incR >> Vshader.incandescenceB
        
        if Vlist>1:
            for e in Vlist[1:]:
                pm.sets (VshadingGrp, e=1, forceElement=e)
                
        Vlist[0].color_H.set(random.randint(1, 360))
        Vlist[0].color_S.set(1)
        Vlist[0].color_V.set(1)
        Vlist[0].toony.set(0.5)
        
        
class textureCtrls():
    """
    Connects a controller to a placeTexture node.
    
    ============== ==============================
    Arguments      Description
    ============== ==============================
    Vlist[0]       Controller
    Vlist[1]       placeTexture node
    ============== ==============================
    """
    
    def __init__(self, Vlist=[], Vsuffix=''):
        Vlist = lsSl.lsSl(Vlist, 1, 0)
        self.suffix = Vsuffix
        self.ctrl = Vlist[0]
        self.placeT = Vlist[1]
        self.image = self.placeT.coverage.outputs()[0]
        
        self.sequence()
        self.coverage()
        self.rotation() 
        self.cleanup()
        
    def sequence(self):
        """Connect image sequence."""
        if not self.ctrl.hasAttr('image'):
            self.ctrl.addAttr('image', min=0, dv=1, at='long', k=1)   
        #self.ctrl.addAttr('image', min=0, dv=1, at='long', k=1)    
        self.ctrl.image >> self.image.frameExtension
        self.image.useFrameExtension.set(1)
        self.image.useHardwareTextureCycling.set(1)
        
    def coverage(self): 
        """Connect translation and scaling."""           
        addTransScene = self.placeT.offsetU.inputs(scn=1)
        
        if not addTransScene == []:
            addTrans = addTransScene[0]
        else:
            addTrans = pm.createNode('plusMinusAverage', 
                                     n=self.placeT.name() + '_add')
            self.ctrl.addAttr('mirror', min=0, max=1, at='long', k=1)
            self.placeT.mirrorU.set(1)
            self.ctrl.mirror >> addTrans.input3D[0].input3Dx
        
            self.ctrl.sx >> self.placeT.coverageU
            self.ctrl.sy >> self.placeT.coverageV
            addTrans.output3Dx >> self.placeT.offsetU
            addTrans.output3Dy >> self.placeT.offsetV
        
        for i in range(20):
            if not addTrans.input3D[i].input3Dx.inputs() == []:
                pass
            else:    
                self.ctrl.tx >> addTrans.input3D[i].input3Dx
                self.ctrl.ty >> addTrans.input3D[i].input3Dy
                break
            
    def rotation(self):
        """Connect rotation."""
        addRotScene = self.placeT.rotateFrame.inputs(scn=1)
        null = tp.nullGroup(Vlist=[self.ctrl], Vhierarchy=3, Vprefix='null')[0]
        mult = pm.createNode('multiplyDivide', name='mult_rot'+self.suffix)
        mult.input2X.set(-1)
       
        self.ctrl.rz >> null.rz
        self.ctrl.rz >> mult.input1X
        self.ctrl.s.set([1,1,1])
        null.s.set([1,1,1])
        
        if not addRotScene == []:
            print '------------------'
            print addRotScene
            addRot = addRotScene[0]
            
            for i in range(20):
                if not addRot.input3D[i].input3Dx.inputs() == []:
                    pass
                else:    
                    mult.outputX >> addRot.input3D[i].input3Dx
                    break
    
        else:
            addRot = pm.createNode('plusMinusAverage', 
                                     n=self.placeT.name() + '_addRot')
            mult.outputX >> addRot.input3D[0].input3Dx
            addRot.output3Dx >> self.placeT.rotateFrame
        
            
        
    def cleanup(self):
        """Cleanup crtls."""
        for e in [self.ctrl]:
            attrList = [e.rx, e.ry, e.v, e.tz]
            for attr in attrList:
                attr.set (lock=1, keyable=0, channelBox=0)
        
        
def proxyShader(Vlist=[], Vcolor=[1, 1, 1], shaderName='shader', randomColor=0, toony=0):
    """
    creates:
        a shader for proxy geometry with a predefined color
 
    returns:
        [shader, shadingGroup]    
    """
    
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    
    try:
        VshadingGrp = pm.ls('%sSG' % (shaderName))[0]
        Vshader = pm.ls(shaderName)[0]     
    except:
        Vshader = pm.shadingNode ('lambert', asShader=1, n=shaderName)
        VshadingGrp = pm.sets (renderable=1, noSurfaceShader=1, empty=1, 
                               name='%sSG' % (shaderName))
        Vshader.outColor >> VshadingGrp.surfaceShader
    
    if randomColor:
        rRan = random.random()
        gRan = random.random()
        bRan = random.random()
        
        Vcolor =[(rRan*randomColor + Vcolor[0]*(1-randomColor)), 
                 (gRan*randomColor + Vcolor[1]*(1-randomColor)), 
                 (bRan*randomColor + Vcolor[2]*(1-randomColor))]   
        
    Vshader.color.set(Vcolor)
    if toony:
        Vshader.incandescence.set([Vcolor[0]/3,Vcolor[1]/3,Vcolor[2]/3])
    
    for i in range(len(Vlist)):
        try:
            pm.sets (VshadingGrp, e=1, forceElement=Vlist[i])
        except:
            pass
        
    return [Vshader, VshadingGrp]


def setTransparent(Vlist=[]):
    """
    Sets up selected shader for rendering image planes with alpha channel.   
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    for each in Vlist:
        each.shadowAttenuation.set(0)
    
        
def imagePlane(Vlist=[], path=r'', sequence=0, polyPlane=0, normalize=1):
    """
    Sets up a picture as a polygon image plane in the correct proportions.   
    """
    shad = pm.shadingNode('lambert', asShader=1)
    setTransparent([shad])
    tex = pm.shadingNode('file', asTexture=1)
    tex.outColor >> shad.color
    tex.outTransparency >> shad.transparency
    name = os.path.splitext(os.path.split(path)[-1])[0]    
    if sequence:
        tex.useFrameExtension.set(1)
    tex.fileTextureName.set(path)
    size = tex.outSize.get()
    
    if normalize:
        width = 1 
        height = (1 /size[0])*size[1]
    else:
        width = size[0]/100  
        height = size[1]/100 
    
    if polyPlane:
        plane = pm.polyPlane(w=width, h=height, ch=0, sx=1, sy=1, n=name)[0]
    else:
        plane = pm.nurbsPlane(w=width, lr=height ,ch=0, n=name)[0]
    
    shadingGrp = pm.sets (renderable=1, noSurfaceShader=1, empty=1, 
                          name='%sSG' % (str(shad)))
    shad.outColor >> shadingGrp.surfaceShader
    pm.sets (shadingGrp, e=1, forceElement=plane)
    if polyPlane:
        plane.rx.set(90)
    else:
        plane.ry.set(-90)
    pm.makeIdentity(plane, apply=1, t=1, r=1, s=1, n=0)
    
    pm.select(tex)
    return plane, tex
    
class cameraPlane():
    """
    Class representing a camera plane.
    """
    def __init__(self, Vlist=[], edit=0, **kwdArgs):
        if edit==0:
            self.create(Vlist=Vlist, **kwdArgs)
        else:
            self.edit(Vlist=Vlist)
        
    def create(self,  Vlist=[], Vsuffix='', path=r'', sequence=0, polyPlane=0):
        """
        Creates the camera plane.
        """
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        
        self.cam = Vlist[0]
        self.camshape = self.cam.getShape()
        
        self.plane, self.file = imagePlane(Vlist=[], path=path, 
                                           sequence=sequence, polyPlane=polyPlane)
        sg = self.plane.getShape().instObjGroups.connections(plugs=0)[0]
        self.shader = sg.surfaceShader.connections(plugs=0)[0]
        
        self.plane.addAttr ('focalLength', defaultValue=0, keyable=0)
        self.plane.addAttr ('depth', defaultValue=1, min=0, keyable=1)
        self.plane.focalLength.set(self.camshape.focalLength.get())
        
        self.group = pm.group(em=1, w=1, n='null_'+self.plane.nodeName())
        snap.snap([self.group, self.plane, self.cam])
        self.cam|self.group|self.plane
        self.group.tz.set(-1)
        
        self.divide = pm.createNode('multiplyDivide', 
                    name='divide'+Vsuffix)
        
        self.mult = pm.createNode('multiplyDivide', 
                    name='mult'+Vsuffix)
        
        self.divide.operation.set(2)
        self.divide.input1X.set(36)
        self.plane.focalLength >> self.divide.input2X
        
        self.divide.outputX >> self.mult.input1X
        self.plane.depth >> self.mult.input2X
        
        self.mult.input1Y.set(-1)
        self.plane.depth >> self.mult.input2Y
        self.mult.outputY >> self.group.tz
        
        self.mult.outputX >> self.group.sx
        self.mult.outputX >> self.group.sy
        self.mult.outputX >> self.group.sz
        pm.select(Vlist)
        
    def edit(self, Vlist=[]):
        """
        Gathers object for editing.
        """
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        
        self.group= Vlist[0]
        self.cam = self.group.getParent()
        self.camshape = self.cam.getShape()
        self.mult = self.group.sx.connections(plugs=0)[0]
        self.plane = self.mult.input2X.connections(plugs=0)[0]
        self.divide = self.plane.focalLength.connections(plugs=0)[0]
        sg = self.plane.getShape().instObjGroups.connections(plugs=0)[0]
        self.shader = sg.surfaceShader.connections(plugs=0)[0]
        self.file = self.shader.color.connections(plugs=0)[0]
        fn = self.file.fileTextureName.get()
        
        if not fn == '':
            self.path = os.path.abspath(fn).replace('\\', '/')
        else:
            self.path = ''
       
    def select(self, *args):
        """Select the plane."""
        pm.select(self.plane)
    
    def fit(self, *args):
        """Fits the plane to the current focal length."""
        self.plane.focalLength.set(self.camshape.focalLength.get())

    def connect(self, *args):
        """Connects the plane to the current focal length."""
        self.camshape.focalLength >> self.plane.focalLength
    
    def parent(self, *args):
        """Parents the plane to its group."""
        if not self.plane.getParent() == self.group:
            try:
                self.group|self.plane
            except:
                pass
        else:
            pm.warning('Plane is already connected to camera.')
    
    def setPath(self, path, *args):
        """Sets the texture path."""
        self.file.fileTextureName.set(path)
        
    def setDepth(self, val, *args):
        """Sets the depth."""
        self.plane.depth.set(val)
    
    def delete(self, *args):
        """Sets the depth."""
        
        for e in [self.plane, self.group,  
                  self.file, self.shader,
                  self.mult, self.divide]:
            try: 
                pm.delete(e)
            except: pass

def cameraPlanesAllCamsUI():
    """
    Opens one interface for each camera in the scene.
    """
    for e in pm.ls(type='camera'):
        if not e in ['perspShape','frontShape', 'sideShape','topShape', 'backShape']:
            try:
                cameraPlaneUI([e.getParent()])
            except Exception, detail:
                print detail
            
class cameraPlaneUI():
    """
    UI for managing camera planes.
    There can be one active UI for each camera in the scene.
    
    ========= ================================
    Arguments Description
    ========= ================================
    Vlist     Camera shape or transform node.
    ========= ================================
    
    .. note:: 
        Images are always horizontally fit.
    """
    def __init__(self, Vlist=[], usePolygon=0):
        Vlist = lsSl.lsSl(Vlist, 1, 1, VerrorStr='Please select a camera.')
        self.usePolygon = usePolygon
        self.cam = ''
        if Vlist[0].type() == 'camera':
            self.cam = Vlist[0].getParent()
        else:
            if not Vlist[0].getShape().type() == 'camera':
                raise Exception(Vlist[0].name() + '  is not a camera!')
            else:
                self.cam = Vlist[0]
        
        UIname = self.cam.name() + '_image_planes'
        print UIname
        try:
            pm.deleteUI(UIname)
        except: 
            pass
        pm.window(UIname, t=UIname, h=10, w=100, s=1)
        self.mainFormA = pm.formLayout()
        
        self.createUI(self.mainFormA)
        self.editUI(self.mainFormA)
        
        self.mainFormA.redistribute(0,1)
        pm.showWindow(UIname)
        
    def createUI(self, parent=''):
        """
        UI to create camera planes.
        """
        self.mainFormB = pm.formLayout(p=parent)
        with pm.rowLayout(nc=4, adjustableColumn=1):
            self.tF = pm.textField(text='')
            pm.button(l='browse', w=60, c=self.setFilename)
            self.polyCB = pm.checkBox(l='Polygon', v=self.usePolygon)
        pm.button(l='create plane', w=60, h=40, c=self.create)
        pm.separator(h=10, st='none')
        self.mainFormB.redistribute()
        
    def editUI(self, parent=''):
        """
        UI to edit camera planes.
        """
        self.groups = self.cam.getChildren()
        try:
            pm.deleteUI(self.scroll)
        except: 
            pass
       
        self.scroll = pm.scrollLayout(childResizable=1, p=parent)
        self.mainFormC = pm.formLayout(p=self.scroll)
        with pm.columnLayout(adjustableColumn=1):
            pCls = []
            
            for e in self.groups:
                if not e.nodeType() == 'camera':
                    try:
                        pCls.append(cameraPlane(Vlist=[e], edit=1))
                    except Exception, detail:
                        print detail
            for c in pCls:
                with pm.columnLayout(adjustableColumn=1) as column:
                    with pm.rowLayout(nc=7, adjustableColumn=1) as mixButtons:
    
                        tF = pm.textField(text=c.path, h=30)
                        pm.button(l='browse', w=60, h=30, 
                                  c=pm.Callback(self.editFilename, c, tF))
                        pm.textField(tF, e=1, ec=pm.Callback(self.setPath, c, tF))
                        pm.button(l='select', w=60,  h=30, c= pm.Callback(c.select))
                        pm.button(l='fit', w=60,  h=30, c=c.fit)
                        pm.button(l='parent', w=60,  h=30, c= pm.Callback(c.parent))
                        pm.button(l='delete', w=60,  h=30, c= pm.Callback(self.deletePlane, c, column))
                        max = 30
                        depth = c.plane.depth.get()
                        if depth >= max:
                            max = depth*2
                    pm.attrFieldSliderGrp( min=0, max=10000, smn=1, smx=max,
                                            at=c.plane.depth)
                    pm.separator(h=15, st='none')
                self.mainFormC.redistribute()
    
    def editFilename(self, cls, ctl, *args):
        defPath = pm.textField(ctl, q=1, text=1) 
        filename = pm.fileDialog2(fileMode=1, caption="Import Image", dir=defPath)
        if filename:
            pm.textField(ctl, e=1, text=filename[0])
            cls.setPath(filename)
    
    def setFilename(self, *args):
        defPath = pm.textField(self.tF, q=1, text=1) 
        filename = pm.fileDialog2(fileMode=1, caption="Import Image", dir=defPath)
        print filename
        pm.textField(self.tF, e=1, text=filename[0])
    
    def create(self, *args):
        path = pm.textField(self.tF, q=1, text=1)
        poly = pm.checkBox(self.polyCB, q=1, v=1)
        print path
        if not os.path.exists(path):
            raise Exception('Path does not exist! ' + path)
        if not os.path.isfile(path):
            subdir = os.listdir(path)
            print subdir
            images = []
            for e in subdir:
                if os.path.isfile(os.path.join(path, e)):
                    images.append(e)
            if images:
                print 'found subdir:'
                for e in images:
                    print e
                self.promtForFolderContent(images, path)
                    
                 
        
        else:    
            cameraPlane(Vlist=[self.cam], Vsuffix='',path=path, polyPlane=poly)
            
            self.editUI(self.mainFormA)
            self.mainFormA.redistribute(0,1)
    
    def promtForFolderContent(self, images, path):
        UIname = ' texturePrompt'
        try:
            pm.deleteUI(UIname)
        except: 
            pass
        self.allCB = []
        win = pm.window(UIname)
        with pm.formLayout() as mainFormA:
            pm.text('Files found:')
            pm.text(path)
            pm.button('Import', c=pm.Callback(self.promtForFolderContent_Ok, path))
            pm.button('Cancel', c=pm.Callback(self.promtForFolderContent_cancel))
            
            with pm.scrollLayout(childResizable=1) as scroll:
                with pm.columnLayout(adjustableColumn=1):
                    
                    for e in images:
                        ext = os.path.splitext(e)[-1].upper()
                        val = 0
                        if ext in ['.PNG', '.JPG', '.GIF', '.IFF', '.EPS', '.TGA',
                                   '.TIF']:
                            val = 1
                        cb = pm.checkBox(e, v=val, ann=e)
                        self.allCB.append(cb)
            
        mainFormA.redistribute(0,0,0,0,1)
        pm.showWindow(win)
    
    def promtForFolderContent_Ok(self, path):
        selectedFiles = []
        for e in self.allCB:
            val = pm.checkBox(e, q=1, v=1)
            nam = pm.checkBox(e, q=1, ann=1)
            selectedFiles.append([val, nam])
        poly = pm.checkBox(self.polyCB, q=1, v=1)   
        for e in selectedFiles:
            if e[0]:
                p = os.path.join(path, e[1])
                cameraPlane(Vlist=[self.cam], Vsuffix='',path=p, polyPlane=poly)
        
        self.editUI(self.mainFormA)
        self.mainFormA.redistribute(0,1)
        UIname = 'texturePrompt'
        try:
            pm.deleteUI(UIname)
        except: 
            pass
    
    def promtForFolderContent_cancel(self):
        UIname = 'texturePrompt'
        try:
            pm.deleteUI(UIname)
        except: 
            pass
        
        
    def setPath(self, cls, tf):
        path = pm.textField(tf, q=1, text=1)
        cls.setPath(path)
        
    def setDepth(self, cls, slid):
        val = pm.floatSliderGrp(slid, q=1, v=1)
        cls.setDepth(val)
        
    def deletePlane(self, cls, uiElement, *args):
        confirm = pm.confirmDialog(title='Confirm', 
                           message=('Delete plane?'), 
                           button=['Delete','Cancel'], 
                           defaultButton='Cancel', 
                           dismissString='Cancel' )                      
        if confirm == 'Delete':
            cls.delete()
            pm.deleteUI(uiElement)
        else: 
            pass

def combineMeshes(Vlist=[]):
    """
    Combines meshes.
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    par = Vlist[0].getParent()
    
    new = pm.polyUnite(Vlist, ch=0)[0]
    par|new
    pass


def controllPlanarProj(Vlist=[], Vsuffix=''):
    """
    select: Ctrl, projection, mesh
    """
    Vlist = lsSl.lsSl(Vlist, Vmin=2, Vmax=3)
    
    if not pm.pluginInfo('decomposeMatrix', loaded=1, q=1):
        try:
            pm.loadPlugin('decomposeMatrix')
        except:
            pass
    

    dcm = pm.createNode('decomposeMatrix')

    
#    Vlist[0].r >> Vlist[1].rotate
#    Vlist[0].t >> Vlist[1].projectionCenter
#    Vlist[0].sx >> Vlist[1].projectionWidth
#    Vlist[0].sy >> Vlist[1].projectionHeight
# 
    Vlist[2].worldMatrix[0]  >> Vlist[1].manipMatrix   
    Vlist[0].worldMatrix >> dcm.inputMatrix
    dcm.outputRotate >> Vlist[1].rotate
    dcm.outputTranslate >> Vlist[1].projectionCenter
    dcm.outputScaleX >> Vlist[1].projectionWidth
    dcm.outputScaleY >> Vlist[1].projectionHeight
 
    Vlist[2].worldMatrix[0]  >> Vlist[1].manipMatrix

class replaceShader():
    """
    replace shader with dummy lambert.
    """
    def __init__(self, Vlist=[], Vsuffix=''):
        Vlist = lsSl.lsSl(Vlist, Vmin=1, Vmax=0)
        self.suffix = Vsuffix

        for e in Vlist:
            print self.findTexture(e.getShape())

    def findTexture(self, mesh):
        print mesh
        shadingGrp = pm.listConnections(mesh, source=False, destination=True, type='shadingEngine')
        #material = pm.listConnections(shadingGrp[0])
        material = pm.listConnections(shadingGrp[0],  source=True, destination=False)
        texture = pm.listConnections(material[0], source=True, destination=True, type='file')
        if not texture:
            linear = pm.listConnections(material[0], source=True, destination=True)
            texture = pm.listConnections(linear, linear,destination=True, type='file')

        print shadingGrp
        print material[0]
        print texture

        shaderName = 'shader' + self.suffix
        Vshader = pm.shadingNode('blinn', asShader=1, n='shaderName')
        VshadingGrp = pm.sets(renderable=1, noSurfaceShader=1, empty=1,
                              name='%sSG' % (shaderName))

        Vshader.incandescence.set(0.5, 0.5, 0.5)
        Vshader.specularColor.set(0.07, 0.07, 0.07)
        Vshader.eccentricity.set(0.05)

        Vshader.outColor >> VshadingGrp.surfaceShader

        texture[0].outColor >> Vshader.color
        #pm.connectAttr(mesh.instObjGroups,VshadingGrp.dagSetMembers[0], f=1, na=1)
        pm.sets(VshadingGrp, e=1, forceElement=mesh)







        

        
