"""
Tools for creating corrective shapes.
"""

from __future__ import with_statement
import os
import inspect

import maya.OpenMaya as om
import pymel.core as pm

import mf.builder.nodes
import mf.builder.main as bm
from mf.tools import file as tf
from mf.tools import general as gt
from mf.tools import blendshape as bt
from mf.tools import mesh as tm
import mf.tools.lsSl as lsSl



def _getTemplateFolder(basePath):
    fullPath = ''
    if os.path.exists(os.path.join(basePath, 'settings_private.txt')):
        fullPath = os.path.join(basePath, 'settings_private.txt')
    elif os.path.exists(os.path.join(basePath, 'settings.txt')):
        fullPath = os.path.join(basePath, 'settings.txt')
    return fullPath


def returnCorrTemp():
    settingsPath = _getTemplateFolder(bm.getRelativePath())
    if not settingsPath and os.getenv('SOI_CONFIG'):
        settingsPath = _getTemplateFolder(os.path.join(os.getenv('SOI_CONFIG'), 'mf'))
    pathlist = []
    if not settingsPath:
        pathlist = []
    try:
        pathlist = tf.readInfo(os.path.abspath(settingsPath), 'corrective template:')
    except:
        pass
    return pathlist

def UI(template='', **args):
    if template == '':
        template = returnCorrTemp()[0]
    
    exec('import ' + template)
    exec('reload( ' + template + ')')
    characterObjects = eval(template + '.characterObjects')
    keys = getkeys(characterObjects)
    keys.sort()
    
    temp = returnCorrTemp()
    temp.sort()
    
    UIname = 'correctUI'
    try:
        pm.deleteUI(UIname)
    except: 
        pass
    
    with pm.window(UIname, resizeToFitChildren=1, toolbox=0, s=1) as win:
        
        with pm.formLayout() as layout:

            with pm.columnLayout(adjustableColumn=1):
                with pm.menuBarLayout('mainMenuBarPro'):
                    with pm.menu( label='Select Project Template',  tearOff=True): 
                        for t in temp:
                            proC = 'import mf.tools.corrective as dt; dt.UIsetProject("' + t + '")'
                            pm.menuItem(label=t, c=proC)
                
                pm.textField('corrShapeTemplateNameTF', 
                             text=template, w=150, en=0)
                
                with pm.menuBarLayout('mainMenuBar'):
                    pm.setParent('..')
                    pm.setParent('..')
                        
                pm.textField('corrShapeCharNameTF', text='', w=150, en=0)  
                pm.separator(h=10, style='none')  
                pm.button(l='Edit', w=150, 
                          c='import mf.tools.corrective as dt; dt.UIedit()')
                pm.button(l='Cancel edit', w=150, 
                          c='import mf.tools.corrective as dt; dt.UIcancelEdit()')
                pm.separator(h=15, style='none')
                
                
                
                pm.textField('corrShapeNameTF', text='shapeName', w=150)
                pm.button(l='Create new', w=150, 
                          c='import mf.tools.corrective as dt; dt.UIapply()')
                
                pm.separator(h=15, style='none')
                pm.button(l='Replace with selected', w=150, 
                          c='import mf.tools.corrective as dt; dt.UIreplace()')
            pm.text('Corrective shapes:') 
             
            pm.textScrollList('corrShapeNameSL', allowMultiSelection=1,
                              dcc='import mf.tools.corrective as dt; dt.UIselectShape()')
            
        
        layout.redistribute(0,0)
        UIsetProject(template) 
        UIlistShapes()      
        pm.showWindow(win)
        
        shapesPopup = pm.popupMenu()
        pm.popupMenu(shapesPopup, parent='corrShapeNameSL' ,e=1, alt=0)
        
        pm.menuItem(label='delete selected shapes',
                    c='import mf.tools.corrective as dt; dt.UIdeleteShapes()')

def UIpopulateCharList(template):
    exec('import ' + template)
    exec('reload( ' + template + ')')
    characterObjects = eval(template + '.characterObjects')
    keys = getkeys(characterObjects)
    keys.sort()
    
    try:
        pm.deleteUI('charSelectMenue')
    except:
        pass
    pm.menu('charSelectMenue', label='Select Character',  
            tearOff=True, p='mainMenuBar')
    for o in keys:
        charC = 'import mf.tools.corrective as dt; dt.UIsetChar("' + o + '")'
        pm.menuItem(label=o, c=charC, p='charSelectMenue')

def UIsetProject(template):
    pm.textField('corrShapeTemplateNameTF', text=template, e=1)
    pm.textField('corrShapeCharNameTF', text='',e=1)
    UIpopulateCharList(template)
    UIlistShapes()
    
def UIlistShapes():
    template = pm.textField('corrShapeTemplateNameTF', text=1, q=1)
    exec('import ' + template)
    tempClass = eval(template + '.characterObjects')
    name = pm.textField('corrShapeCharNameTF', text=1, q=1)
    if name == '':
        return
    pm.textScrollList ('corrShapeNameSL', e=1, removeAll=1)
    pm.textScrollList('corrShapeNameSL', e=1, 
                      append=listShapes(name, tempClass))

def UIsetChar(name):
    print name
    pm.textField('corrShapeCharNameTF', e=1, text=name)
    UIlistShapes()

def UIcancelEdit():
    template = pm.textField('corrShapeTemplateNameTF', text=1, q=1)
    exec('import ' + template)
    tempClass = eval(template + '.characterObjects')
    name = pm.textField('corrShapeCharNameTF', text=1, q=1)
    cancelEdit(name, tempClass)
      
def UIedit():
    template = pm.textField('corrShapeTemplateNameTF', text=1, q=1)
    exec('import ' + template)
    tempClass = eval(template + '.characterObjects')
    name = pm.textField('corrShapeCharNameTF', text=1, q=1)
    edit(name, tempClass)
    
def UIapply():
    template = pm.textField('corrShapeTemplateNameTF', text=1, q=1)
    exec('import ' + template)
    tempClass = eval(template + '.characterObjects')
    name = pm.textField('corrShapeCharNameTF', text=1, q=1)
    shapeName = pm.textField('corrShapeNameTF', q=1, text=1)
    apply(name, shapeName, tempClass)
    UIlistShapes()
    
def UIreplace():
    template = pm.textField('corrShapeTemplateNameTF', text=1, q=1)
    exec('import ' + template)
    tempClass = eval(template + '.characterObjects')
    name = pm.textField('corrShapeCharNameTF', text=1, q=1)
    shapeName = pm.textField('corrShapeNameTF', q=1, text=1)
    meshes = pm.textScrollList('corrShapeNameSL', q=1, selectItem=1)
    
    if not len(meshes) == 1:
        raise Exception('Please select one shape from the list.')
    else:
        replaceShape = meshes[0]
    apply(name, shapeName, tempClass, replaceShape)
    UIlistShapes()

def UIdeleteShapes():
    template = pm.textField('corrShapeTemplateNameTF', text=1, q=1)
    exec('import ' + template)
    tempClass = eval(template + '.characterObjects')
    name = pm.textField('corrShapeCharNameTF', text=1, q=1)
    if name == '':
        return
    meshes = pm.textScrollList('corrShapeNameSL', q=1, selectItem=1)
    deleteShapes(name, tempClass, meshes)
    UIlistShapes()
    
def UIselectShape():
    meshes = pm.textScrollList('corrShapeNameSL', q=1, selectItem=1)
    try:
        pm.select(meshes)
    except:
        pass
    
def listShapes(name, tempClass):
    char = tempClass.__dict__[name]()
    
    x = mf.builder.nodes.getAll('mesh_corrective')
    bsNames = []
    #print pm.blendShape(char.blendshape(),q=1, mesh=1)
    for o in x:
        #print o.blendshape()
        #if o.blendshape() == char.blendshape:
        bsNames.append(o.mesh)
    return bsNames 
        
def deleteShapes(name, tempClass, meshes, deleteAttr=True):
    """
    Deletes corrective shapes by name.
    """
    char = tempClass.__dict__[name]()
    meshList = []
    for m in meshes:
        meshList.append(pm.ls(m)[0].getShape())
    
    x = mf.builder.nodes.get(meshList, 'mesh_corrective')
  
    for o in x:
        
        attrName = o.mesh.replace(char.NS.replace(':', '')+'_', '')
    
        bs = bt.getWeights([o.blendshape()])
        attr = pm.ls(str(char.attrObj)+'.'+attrName)[0]
        
        wname =  str(attr.outputs(plugs=1)[0])
        weightIndex = bs.weightList
        
        s = wname.find('[')
        e = wname.find(']')
        weightIndex = int(str(wname)[s+1:e])
    
        pm.blendShape(o.blendshape(), e=1, remove=1, 
                      t=[char.blendshapeMesh ,weightIndex, str(o.mesh), 1])
        pm.delete(o.mesh)
        
        if deleteAttr:
            pm.deleteAttr(attr)
        
def cancelEdit(name, tempClass):
    """
    Delete all corrective meshes without connections to a blendshape.
    """
    char = tempClass.__dict__[name]()
    modeledMesh = pm.ls('*___CORR_MODEL', r=1)
    if not modeledMesh == []:
        pm.delete(modeledMesh[0])
    
    char.editModeOff()
    

def getkeys(myClass):
    """
    Returns the names of all user defined classes of the given class.
    """
    keys = []
    for c, p in myClass.__dict__.iteritems():
        if inspect.isclass(p):
            keys.append(c)
            
    return keys
               
def edit(name, tempClass):
    """
    Sets a character to editing mode. 
    """
    char = tempClass.__dict__[name]()
    modeledMesh = pm.ls(str(char.skinnedMesh) + '___CORR_MODEL')
    
    if not modeledMesh == []:
        pm.select(modeledMesh[0])
        raise Exception('There is already a modeling mesh in the scene.' + 
                        'Use this for modeling or delete it.')
     
    char.editModeOn()
    
    dup = tm.duplicate([char.skinnedMesh], rr=1)[0]
    dup.v.set(1)
    attrList = [dup.tx, dup.ty, dup.tz,
                dup.rx, dup.ry, dup.rz,
                dup.sx, dup.sy, dup.sz, 
                dup.v]
    for attr in attrList:
        attr.set (lock=0, keyable=0, channelBox=1)

    try:
        pm.disconnectAttr(dup.drawOverride.connections(p=1)[0], 
                          dup.drawOverride)
    except: 
        pass
    dup.overrideEnabled.set(0)
    try:
        pm.disconnectAttr(dup.getParent().drawOverride.connections(p=1)[0], 
                          dup.getParent().drawOverride)
    except: 
        pass
    dup.getParent().overrideEnabled.set(0)
    pm.rename(dup, str(char.skinnedMesh) + '___CORR_MODEL')
    pm.select(dup) 
    
    try:
        pm.mel.SculptGeometryTool()
    except:
        pass 
    
def apply(name, shapeName, tempClass, replaceShape=False):
    """
    Applies the modeled blendshape.
    """
    char = tempClass.__dict__[name]() 
    current = pm.currentTime(q=1)   
    
    modeledMesh = pm.ls(str(char.skinnedMesh) + '___CORR_MODEL')
    if modeledMesh == []:
        raise Exception('No modeled mesh found. Please use edit mode first')
    modeledMesh = modeledMesh[0]
    
    bs = bt.getWeights([char.blendshape])
    
    if not replaceShape:
        if pm.hasAttr(char.attrObj, shapeName):
            raise Exception('Attribute name exists. Please choose a new name.')
    
        if shapeName in bs.weightList:
            raise Exception('Blendshape name exists. Please choose a new name.')
    else:
        shapeName = replaceShape.replace(char.NS.replace(':', '_'), '')
        deleteShapes(name, tempClass, meshes=[replaceShape], deleteAttr=False)
    
    startInt = 0
    try:
        int(shapeName[0])
        startInt = 1
    except:
        pass
    
    if startInt:
        raise Exception('Attribute name can not start with a number.' + 
                        'Please choose a new name.')
    
    created = createMesh([char.skinnedMesh, modeledMesh])
    
    try:
        pm.disconnectAttr(created.corrective.getShape().instObjGroups)
    except:
        pass
    
    if not created.corrective:
        raise Exception('No vertexes to move.')
        char.editModeOff()
        pm.delete(modeledMesh)
        return
    
    mf.builder.nodes.set([created.corrective.getShape() ],  'mesh_corrective')
    
    pm.rename(created.corrective, char.NS.replace(':', '_') + shapeName) 
    
    bs = bt.getWeights([char.blendshape])
    bs.weightList
    newBsIndex = len(bs.weightList)   
    
    if replaceShape:
        print 'replacing'
        print replaceShape
        
    pm.blendShape (char.blendshape, e=1, t=[char.blendshapeMesh, newBsIndex, 
                                                created.corrective, 1])
    char.editModeOff()
    pm.delete(modeledMesh)
    

    if not replaceShape:
        char.attrObj.addAttr(shapeName, defaultValue=1, min=0, max=1, 
                             keyable=1, at='double')
        newAttr = pm.ls(str(char.attrObj) + '.' + shapeName)[0]
        newAttr >> char.blendshape.weight[newBsIndex]
        newAttr.set(1)
        
    else:
        newAttr = pm.ls(str(char.attrObj) + '.' + shapeName)[0]
        newAttr >> char.blendshape.weight[newBsIndex]
        
    created.corrective.v.set(0)
    pm.select(char.attrObj)
    
    
class createMesh():
    """
    Creates a corrective shape for nonlinear deformation.
    
    ========= =====================
    Arguments
    ========= =====================
    Vlist[0]  rigged mesh
    Vlist[1]  modeled correction 
    ========= =====================
    """
    def __init__(self, Vlist=[], Vsuffix='', reorderDeformers=1):
        Vlist = lsSl.lsSl(Vlist, 2, 2)
        
        self.orig = Vlist[0]
        self.modeled = Vlist[1]
        hist = self.orig.listHistory(leaf=1, historyAttr=1)
        
        o = ''
        tweak = ''
        for o in hist:
            if o.type() == 'tweak':
                tweak = o
                break
        if tweak == '':
            pm.warning('No deformer found. Nothing moved.')
            return        
        
        if reorderDeformers:
            gt.reorderDeforms([self.orig], refList=['nonLinear', 'wire', 'ffd', 
                                             'skinCluster', 'tweak', 'blendShape', 
                                             'sculpt'])
        count =0
        self.tweakList = []
        dagOrig = self.getDagByName(str(self.orig))
        itterOrig = om.MItMeshVertex(dagOrig)
        y = om.MItMeshVertex(self.getDagByName(str(self.modeled)))
        su = om.MScriptUtil()
        itterOrig.reset()     

        for i in range (itterOrig.count()):             
            vA = [itterOrig.position().x, itterOrig.position().y, 
                  itterOrig.position().z]
            y.setIndex(i, su.asIntPtr())
            vB = [y.position().x, y.position().y, y.position().z]
            TweakVert = tweak.vlist[0].vertex[i].vertex
            
            count = count + self.setPos(TweakVert, vA, vB, itterOrig, i)
            itterOrig.next()
        
        
        if count == 0:
            self.corrective = None
            pm.warning('Nothing to move.')
            return
        
        for i in range(len(self.tweakList)):
            val = self.tweakList[i][1]
            tweak.vlist[0].vertex[self.tweakList[i][0]].vertex.set(val)
            
        print str(count) + ' vertexes moved.'
        
        bt.performSettings(1)
        self.corrective = tm.duplicate([self.orig])[0]
        
        self.corrective.tx.setLocked(0)
        self.corrective.ty.setLocked(0)
        self.corrective.tz.setLocked(0)
        
        self.corrective.rx.setLocked(0)
        self.corrective.ry.setLocked(0)
        self.corrective.rz.setLocked(0)
        
        self.corrective.sx.setLocked(0)
        self.corrective.sy.setLocked(0)
        self.corrective.sz.setLocked(0)
         
        bt.performSettings(0) 
        
        if reorderDeformers:  
            gt.reorderDeforms([self.orig], refList=['tweak', 'nonLinear', 'ffd', 'skinCluster', 'wrap', 'blendShape', 'wire', 'sculpt'])
        
        # reset rigged mesh
        for i in range(len(self.tweakList)):
            tweak.vlist[0].vertex[self.tweakList[i][0]].vertex.set(0, 0 ,0)
        
    def getDagByName(self, name):
        
        selList = om.MSelectionList()
        om.MGlobal.getSelectionListByName(name,selList)
    
        if selList.length()>0:
            obj = om.MDagPath()
            selList.getDagPath(0,obj)    
            return obj
        else: return -1
           
    def setPos(self, TweakVert, vA, vB, itterOrig, i):
        
        if not vB == vA:
            su = om.MScriptUtil()
            N = [vB[0] - vA[0],
                 vB[1] - vA[1],
                 vB[2] - vA[2]]
             
            TweakVert.set(1,0,0)
            itterOrig = om.MItMeshVertex(self.getDagByName(str(self.orig)))
            itterOrig.setIndex(i, su.asIntPtr())
            X = [itterOrig.position().x - vA[0],
                 itterOrig.position().y - vA[1],
                 itterOrig.position().z - vA[2]]
    
            TweakVert.set(0,1,0)
            itterOrig = om.MItMeshVertex(self.getDagByName(str(self.orig)))
            itterOrig.setIndex(i, su.asIntPtr())
            Y = [itterOrig.position().x - vA[0],
                 itterOrig.position().y - vA[1],
                 itterOrig.position().z - vA[2]]
    
            TweakVert.set(0,0,1)
            itterOrig = om.MItMeshVertex(self.getDagByName(str(self.orig)))
            itterOrig.setIndex(i, su.asIntPtr())
            Z = [itterOrig.position().x - vA[0],
                 itterOrig.position().y - vA[1],
                 itterOrig.position().z - vA[2]]        
            
            f = ((X[0] * ((Y[1]*Z[2]) - (Z[1]*Y[2]))) - 
                 (Y[0] * ((X[1]*Z[2]) - (Z[1]*X[2]))) + 
                 (Z[0] * ((X[1]*Y[2]) - (Y[1]*X[2]))))
            
            if f:
                x = ((N[0] * ((Y[1]*Z[2]) - (Z[1]*Y[2]))) - 
                     (Y[0] * ((N[1]*Z[2]) - (Z[1]*N[2]))) + 
                     (Z[0] * ((N[1]*Y[2]) - (Y[1]*N[2]))))/f
                
                y = ((X[0] * ((N[1]*Z[2]) - (Z[1]*N[2]))) -
                     (N[0] * ((X[1]*Z[2]) - (Z[1]*X[2]))) + 
                     (Z[0] * ((X[1]*N[2]) - (N[1]*X[2]))))/f
                
                z = ((X[0] * ((Y[1]*N[2]) - (N[1]*Y[2]))) - 
                     (Y[0] * ((X[1]*N[2]) - (N[1]*X[2]))) + 
                     (N[0] * ((X[1]*Y[2]) - (Y[1]*X[2]))))/f
                       
                self.tweakList.append([i, [x, y, z]])
                
            return 1
        return 0
    

class angle():
    def __init__(self, Vlist=[], edit=0, **kwdArgs):
        if edit==0:
            self.create(Vlist=Vlist, **kwdArgs)
        
        else:
            self.edit(Vlist=Vlist)
            
    def create(self, Vlist=[], Vsuffix='', name='', VdimensionLine=1, 
                 infoText='', VlockAttr=1):
        """
        Creates the angle tool.
        """
        
        Vlist = lsSl.lsSl(Vlist, 4, 4)
        
        pm.loadPlugin('decomposeMatrix.mll')
        pm.pluginInfo('decomposeMatrix.mll', e=1, autoload=1)
        
        matrixA1 = pm.createNode('decomposeMatrix')
        matrixA2 = pm.createNode('decomposeMatrix')
        
        matrixB1 = pm.createNode('decomposeMatrix')
        matrixB2 = pm.createNode('decomposeMatrix')
        
        minusA = pm.createNode('plusMinusAverage')
        minusB = pm.createNode('plusMinusAverage')
        
        minusA.operation.set(2)
        minusB.operation.set(2)
        
        angle = pm.createNode('angleBetween')
        
        Vlist[0].worldMatrix[0] >> matrixA1.inputMatrix
        Vlist[1].worldMatrix[0] >> matrixA2.inputMatrix
        
        Vlist[2].worldMatrix[0] >> matrixB1.inputMatrix
        Vlist[3].worldMatrix[0] >> matrixB2.inputMatrix
        
        matrixA1.outputTranslate >> minusA.input3D[0]
        matrixA2.outputTranslate >> minusA.input3D[1]
        
        matrixB1.outputTranslate >> minusB.input3D[0]
        matrixB2.outputTranslate >> minusB.input3D[1]
        
        minusA.output3D >> angle.vector1
        minusB.output3D >> angle.vector2
        
        Vlist[0].addAttr('angle', keyable=1, at='double')
        angle.angle >> Vlist[0].angle
        
        print angle.angle.get()
        return angle.angle
     
     
    def edit(self, Vlist=[]):
        """
        Gathers object for editing.
        """

        pass
            

