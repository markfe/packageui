"""
tools for handling animation
"""
import os
import re
import pymel.core as pm
import file as tf
import fileinput
import maya.cmds as cmds


class animError(Exception): pass
class noCurvesError(animError): pass

def exportAnim(Vpath='', Vfile='', force=0):
    """
    Exports animation curves to a .ma file and the information to connect the 
    curves in a corresponding .txt file.

    returns: [animation curve name, connected attribute name]

    ========= ================================
    Arguments Description
    ========= ================================
    Vpath     path
    Vfile     filename (without extension)  
    ========= ================================
    """ 
    tf.checkPath(Vpath)
    if force != 1:
        tf.fileExists(Vpath + Vfile + '.txt')
    
    Vresult = []
    # get curves and connections
    animCurves = pm.ls(type = ['animCurveTL', 'animCurveTA', 
                               'animCurveTT', 'animCurveTU'])
    # ['animCurveTL', 'animCurveTA', 'animCurveTT', 
    # 'animCurveTU', 'animCurveUA', 'animCurveUL', 
    # 'animCurveUT', 'animCurveUU'])
    for i in range(len(animCurves)):
        Vcon = ''
        try:
            Vcon = animCurves[i].connections(plugs=1)[0]
        except:
            pass
        Vresult.append([str(animCurves[i]), str(Vcon)])
    # check curves    
    if len(animCurves) == 0:
        raise noCurvesError ('no animation curves found')
    # write files
    f = open (Vpath + Vfile + '.txt', 'w')
        
    for i in range(len(Vresult)):
            f.writelines (str(Vresult[i])+ '\n')
    f.close()
            
    pm.select(animCurves) 
    try:
        # uiConfiguration=0  
        pm.exportSelected(Vpath + Vfile + '.ma', type="mayaAscii", 
                          constructionHistory=0, constraints=0, channels=0, 
                          expressions=0,uiConfiguration=0, shader=0)
    except:
        # uiConfiguration=0
        pm.exportSelected(Vpath + Vfile + '.mb', type="mayaBinary", 
                          constructionHistory=0, constraints=0, channels=0, 
                          expressions=0,uiConfiguration=0, shader=0)
    
    return Vresult

def importAnim(Vpath='', Vfile='', deleteLoose=1, namespace='ANIM', useNamespace=1):
    """
    Imports animation curves from a .ma file and the information to connect the 
    curves from a corresponding .txt file.
  
    returns: 
    [failed source attributes, failed goal attributes, failed connections]
    
    ============ =====================================================
    Arguments    Description
    ============ =====================================================
    Vpath        path
    Vfile        filename (without extension)
    deleteLoose  delete curves that don't have connection information
    namespace    namespace
    ============ =====================================================
    """
    tf.checkPath(Vpath)  

    Vpairs = []     
    failedSrc = []
    failedGoal = []
    failedCon = []
    
    if not useNamespace:
        namespace = ''
    # read files
    try:
        if useNamespace:
            cmds.file(Vpath + Vfile + '.ma', pr=1,i=1,namespace=namespace, 
                      loadReferenceDepth="all", ra=True, type="mayaAscii", 
                      options="v=0")
        else:
            cmds.file(Vpath + Vfile + '.ma', pr=1,i=1, rpr='clash', 
                      loadReferenceDepth="all", ra=False, type="mayaAscii", 
                      options="v=0")
            
    except:
        if useNamespace:
            cmds.file(Vpath + Vfile + '.mb', pr=1,i=1, namespace=namespace, 
                      loadReferenceDepth="all", ra=True, type="mayaBinary", 
                      options="v=0")
        else:
            cmds.file(Vpath + Vfile + '.mb', pr=1,i=1, rpr='clash', 
                      loadReferenceDepth="all", ra=False, type="mayaBinary", 
                      options="v=0")
        
    for line in fileinput.input([Vpath + Vfile + '.txt']):
        Vpairs.append(eval(line))
    
    # connect animation curves    
    for Vpair in Vpairs:
        
        if Vpair[1] == '':
            failedCon.append(str(Vpair[0]))
            print 'no connection info'
            if deleteLoose == 1:
                try:
                    #print 'delete loose'
                    pm.delete(Vpair[0])
                except:
                    pass
        else:
            try: 
                Vsrc = pm.ls(namespace + ':' + Vpair[0] + '.output')[0]
            except:
                failedSrc.append(namespace + ':' + str(Vpair[0])) 
                pass
            
            try:
                Vgoal = pm.ls(Vpair[1])[0]
            except:
                failedGoal.append(str(Vpair[1]))
                if deleteLoose == 1:
                    #print Vsrc
                    pm.delete(pm.ls(namespace + ':' + Vpair[0])[0])
                pass
                       
            try:    
                Vsrc >> Vgoal
            except:
                failedCon.append(str(Vpair[0])) 
                pass
    
    print failedSrc
    print failedGoal
    print failedCon
    return [failedSrc, failedGoal, failedCon]

def deleteAnim(Vtype=['animCurveTL', 'animCurveTA', 
                      'animCurveTT', 'animCurveTU', 
                      'animCurveUA', 'animCurveUL', 
                      'animCurveUT', 'animCurveUU']):
    """
    Deletes animation curves of the given types.
    """  
    animCurves = pm.ls(type=Vtype)
    pm.delete(animCurves)
    return animCurves



class transfer_edits():
    """
    Transfer animation (Transfer reference edits from layout to animation department.)
    """
    def __init__(self, path="D:/maya/GB5/shot_001/rocky.editMA", referenceNode="ROCRN"):
        self.path = path
        self.referenceNode = referenceNode
    
    def write(self):
        """
        Writes the .editMA
        """
        pm.exportEdits(self.path, f=1, type="editMA", orn=self.referenceNode, 
                       includeNetwork=0, includeAnimation=1, includeSetAttrs=1,
                       )
                       #editCommand = "connectAttr")
                       #editCommand = ["addAttr", "connectAttr", "deleteAttr", "disconnectAttr", "parent", "setAttr", "lock","unlock"])
    
    def read(self, regexStr = [".wl\[.*\]\.w\[.*\]"]):
        """
        Reads the .editMA
        """
        self._cleanup_editMA(lineStart='select ', regexStr=regexStr)
        self._cleanup_editMA(lineStart='disconnectAttr ', regexStr=regexStr)
        try:
            # this is wrapped in a try/except so the script can go on when failing to set single edits.
            r = pm.importFile(self.path, type="editMA", namespace="offlineNS", 
                applyTo=self.referenceNode, returnNewNodes=1)
            #print 'Return value from import:'
            #print r
        except:
            #print "Errors occurred while importing."
            pass
        pm.namespace(removeNamespace='offlineNS', mergeNamespaceWithParent=1)   
        
    def _cleanup_editMA(self, lineStart, regexStr):
        """
        Strip All "disconnectAttr" and "addAttr" from the file.
        The editCommand flag does not seem to have any effect::
        
            disconnectAttr "<main>:aimSource_eyeR_aimConstraint1.crz" "<main>:aimSource_eyeR.rz";
                    
                setAttr "<main>:aimSource_eyeR.rz" 39.255017535672884;
            select -ne "<main>:nurbs_ribbon__rsShape";
                addAttr -ci true -h true -sn "sref" -ln "surfaceReference" -min 0 -max 1 -at "bool";
                setAttr "<main>:nurbs_ribbon__rsShape.sref" yes;
                
        import re
        regexStr = ".wl\[.*\]\.w\[.*\]"
        fullNodePath = ".wl[87].w[13]"
        x = re.search(regexStr, fullNodePath)
        x.group(0)
        
        c.read(regexStr = [".wl\[.*\]\.w\[.*\]", '"<main>:mouth'])
        
        import pip.maya.scene as ps
        reload(ps)
        c = ps.transfer_edits(referenceNode='ROBRN')
        c.read(regexStr = [".wl\[.*\]\.w\[.*\]", '"<main>:mouth', '"<main>:E_tooth_r_lw'])

        """
        result = []
        self.references = []
        self.path = os.path.abspath(self.path) 
        if not os.path.exists(self.path):
            raise Exception ('Path not valid:  '+ self.path )
        if not os.path.splitext(self.path)[-1] == '.editMA':
            pm.warning('File is not maya editMA:  '+ self.path )
        else:
            f = open(self.path, 'r')
            lines = f.readlines()
            f.close()
            reflines = []
            
            lStart = None
            lEnd = None
      
            for e in lines[:]:
                reflines.append(e.strip('\n'))
        
        foul = 0
        for e in reflines:
            
            foulLine = 0
            for r in regexStr:
                x = re.search(r, e)
                if x:
                    foulLine =1
            if foulLine:
                continue
            
            if e[:len(lineStart)] == lineStart: 
                foul = 1
                continue
            
            if not foul:
                result.append(e+'\n')
            
            if foul:
                    #print 'TAP'
                if '\t' in e:
                    pass
                    #print 'OOO'
                else:
                    foul = 0
                    result.append(e+'\n')
                    
            
            #print foul
        f = open(self.path, 'w')
        f.writelines(result)
        f.close()        
        #for e in result:
            #print e
            
    def _listKeys(self):
        """
        Lists all keys from the .editMA
        """
        pass
    

                          

    





    

