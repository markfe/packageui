import os
import shutil
import subprocess
import re

import maya.cmds as cm
import maya.mel
import pymel.core as pm
from mf.tools import blast_util as bu

class readTemplate():
    def __init__(self, path=r'D:\git\rig_tools\rig-tools\shared\playblast.txt'):
        """
        Reads a template and creates the playblast. 
        """
        camList = self.getLines(path, '-camList')
        camOptions = self.makeDict(self.getLines(path, '-camOptions'))
        viewportOptions = self.makeDict(self.getLines(path, '-viewportOptions'))
        conv = self.getLines(path, '-converter')
        extension = self.getLines(path, '-extension')[0]
        exportNameScript = self.getLines(path, '-exportNameScript')
        preBlastScript = self.getLines(path, '-preBlastScript')
        postBlastScript = self.getLines(path, '-postBlastScript')
        fc = self.getLines(path, '-framecounter')
        res = self.getLines(path, '-resolution')
        mayaCodec = self.getLines(path, '-mayaCodec')
        openWhenDoneLine = self.getLines(path, '-openWhenDone')
        
        if conv:
            converter = conv[0]
        else:
            converter = None
        
        if fc:
            framecount = eval(fc[0])
        else:
            framecount=1
        
        if res:
            w, h = res[0].split('x')
            resolution = [eval(w),eval(h)]
        else:
            resolution = [1280, 720]  
            
        if mayaCodec:
            compression = mayaCodec[0]
        else:
            compression = 'none'    
        
        openWhenDone = True
        for l in openWhenDoneLine:
            if 'NO' in l.upper() or 'FALSE' in l.upper():
                openWhenDone = False
    
         
        blast(camList,
              camOptions,
              viewportOptions,
              converter,
              extension,
              exportNameScript,
              preBlastScript,
              postBlastScript,
              framecount,
              resolution,
              compression,
              openWhenDone)
        
    def getLines(self, path, key):
        """
        Looks for a key in an info file and returns all following lines up to the 
        next blank line. Comments will be skipped.
        """ 
        f = open(path, 'r')
        lines = []
        k = 0
        x = 1
        while x:
            line = f.readline()
            cleanLine = line.replace('\n', '').strip()
            #Do not read more of the file if there is a blank line after the values.    
            if k == 1:
                if cleanLine == '':
                    break
            if cleanLine == '':
                k = 0
            if k:
                #Skip comments.
                if cleanLine.replace(' ', '')[0] == '#':
                    pass
                else:
                    lines.append(cleanLine)
            if cleanLine == key:
                k = 1
            if not line:
                break
        f.close()
        return lines
            
    def makeDict(self, l):
        d = {}
        for e in l:
            splits = e.split(',')
            k = splits[0]
            v = ','.join(splits[1:])
            d[k] = eval(v)
        return d

class blast():
    def __init__(self, 
                 camList=[], 
                 camOptions={},
                 viewportOptions={},
                 converter='', 
                 extension='',
                 exportNameScript=[],
                 preBlastScript=[], 
                 postBlastScript=[],
                 framecount = 1,
                 resolution=[1280, 720],
                 compression = 'none',
                 openWhenDone = True):
        """
        ================ ========================================================
        Arguments        Description
        ================ ========================================================
        camList          List of camera suffixes to look for.
                         The first camera found will be used.
        camOptions       Dictionary of Attribute: Values to be set on the camera.
        viewportOptions  Dictionary of viewport settings.
        converter        Path of the batch file to convert the playblast.
        exportNameScript Script that returns new names for the exported files.
        preBlastScript   Script to run befor the playblast (to adjust things 
                         the scene). 
        ================ ========================================================
        """
        
        self.camList = camList
        self.camOptions = camOptions
        self.viewportOptions = viewportOptions
        self.converter = converter 
        self.extension = extension
        self.exportNameScript = exportNameScript
        self.preBlastScript = preBlastScript
        self.postBlastScript = postBlastScript
        self.fc = framecount
        self.resolution = resolution
        self.compression = compression
        self.openWhenDone = openWhenDone
        
        self.makeTempFolder()
        self.getPaths()
        self.pathCheck()
        self.getCam()
        self.camSettings()
        self.runPreScripts()
        self.create()
        self.runPostScripts()
        if self.converter:
            self.convert()
            self.copy()
        else:
            self.copyUnconverted()
        
    
    def pathCheck(self):
        for e in self.names:
            if not os.path.exists(os.path.split(e)[0]):
                raise Exception('Path does not exist: ' + e) 
        if self.converter:
            if not os.path.isfile(self.converter): 
                raise Exception('No converter found: ' + self.converter)   
    
    def makeTempFolder(self):
        filePath = pm.sceneName()
        self.fileName= os.path.splitext(os.path.split(filePath)[-1])[0]
        
        userPath = os.path.abspath(os.path.expanduser("~"))
        #self.tempFolder = os.path.join(userPath,'playblast_temp')
        self.tempFolder = 'C:\\playblast_temp'
         
        try:
            shutil.rmtree(self.tempFolder)
            print 'Removed contents from: ', self.tempFolder
        except:
            pass
        os.makedirs(self.tempFolder)
        
    def getCam(self):
        self.cam = None
        for e in self.camList:
            self.cam = pm.ls('*' + e, transforms=1) 
            if self.cam:
                print 'Used cam: ', self.cam
                break
        if not self.cam:
            raise Exception('No camera found.')
        
    def camSettings(self):
        for k, v in self.camOptions.iteritems():
            each_attr = pm.PyNode( self.cam[0].attr( k ) )
            if( each_attr.get() != v ):
                if( each_attr.isLocked() ):
                    each_attr.setLocked( 0 )
                each_attr.set( v )
        
    
    def create(self, quality = 100):
        """
        Create the playblast in the temp folder.
        """
        if self.viewportOptions:
            window = pm.window()
            pm.paneLayout()
            self.mypanel = pm.modelPanel()
        else:
            currentPanel = pm.getPanel(withFocus=1)

            if not currentPanel.type() == 'modelEditor':
                raise Exception('No viewport selected for playblast.')

            self.mypanel = currentPanel
            #print '##############'
            print self.mypanel

        if self.viewportOptions:
            pm.showWindow( window )
    
            pm.window(window, e=1, w=self.resolution[0]+200, h=self.resolution[1]+200)      
        pm.refresh()
        
        pm.lookThru (self.cam, self.mypanel)
        cm.setFocus(self.mypanel)
        
        if self.fc:
            self.framecounter() 
        
        if self.viewportOptions:
            self.panelSettings()
        
        aPlayBackSliderPython = maya.mel.eval('$tmpVar=$gPlayBackSlider')
        sound = pm.timeControl( aPlayBackSliderPython, q=1, sound=1)
        
        aviPath = os.path.join(self.tempFolder, 'uncompressed.avi')
        
        if self.viewportOptions:
            pm.playblast (format='movie', filename=aviPath, clearCache=1, viewer=0, 
                          showOrnaments=1, fp=4, percent=100, 
                          compression=self.compression,
                          widthHeight=self.resolution, forceOverwrite=1, s=sound)
        
            pm.deleteUI(self.mypanel, panel=True)
            pm.deleteUI(window)
    
        else:
            pm.playblast (format='movie', filename=aviPath, clearCache=1, viewer=0, 
                          showOrnaments=1, fp=4, percent=100, 
                          compression=self.compression,
                          widthHeight=self.resolution, forceOverwrite=1, s=sound,
                          offScreen=1)
            
        
    def framecounter(self):
        """
        Hud framecounter.
        """
        try:
            huds = pm.headsUpDisplay(lh=1)
            for h in huds:
                #print h
                pm.headsUpDisplay(h, rem=1)
        except:
            print "hud error"
        pm.headsUpDisplay(rp=[2,0])
        pm.headsUpDisplay(rp=[7,0])
        pm.headsUpDisplay("HUDCam", block=0, section=2, pre="cameraNames", 
                          labelFontSize='large',)
        pm.headsUpDisplay("HUDFrame",block=0,section=7,blockSize="small",
                          label="Frame",
                          command="import mf.tools.blast; mf.tools.blast.getFrame()",
                          decimalPrecision=1,
                          dataFontSize="large",
                          dataAlignment="right",
                          attachToRefresh=1,vis=1)
        
    def panelSettings(self, settings={}):
        """
        Sets panel settings.
        """
        pm.modelEditor(self.mypanel, e=1, allObjects=0)
        pm.modelEditor(self.mypanel, edit=1, **self.viewportOptions)

    def setShaders(self):
        """
        Modify shader in scene.
        """
        shader = pm.ls(type='lambert')
        for s in shader:
            inc = s.incandescence.get()
            i = inc[0]+inc[1]+inc[2]
            if i > 1.9:
                s.incandescence.set(0, 0, 0)
                s.ambientColor.set(1, 1, 1)
                s.diffuse.set(0)
    
    def convert(self):
        print '------------------------------'
        print '  Converting:'
        print self.tempFolder + '/uncompressed.avi'
        print '  to:'
        print self.tempFolder + '/compressed.' + self.extension 
        old = os.path.abspath(self.tempFolder + '/uncompressed.avi').replace('\\', '/')
        new = os.path.abspath(self.tempFolder + '/compressed.' + self.extension).replace('\\', '/')
        print subprocess.call(self.converter + ' "' + old + '" "' +  new + '"')
    
    def runPreScripts(self):
        for e in self.preBlastScript:
            x = getattr(bu.preScript, e)
            x(bu.preScript())
    
    def runPostScripts(self):
        for e in self.postBlastScript:
            x = getattr(bu.postScript, e)
            x(bu.postScript())
    
    def getPaths(self):
        self.names = []
        for e in self.exportNameScript:
            x = getattr(bu.getNames, e)
            res = x(bu.getNames())
            self.names = self.names + res
        #print self.names
         
    def copy(self):
        """
        """
        fullPaths = []
        for e in self.names:
            fullPaths.append(e + '.' + self.extension)
        print '---------------------------'
        for e in fullPaths:
            tmp = os.path.abspath(self.tempFolder + '/compressed.' + self.extension).replace('\\', '/')
            shutil.copy2(tmp, e)
            print 'File created: ', e
        
        if self.openWhenDone:    
            subprocess.call(('cmd', '/C', 'start', '', fullPaths[-1])) 
    
    def copyUnconverted(self):
        fullPaths = []
        for e in self.names:
            fullPaths.append(e + '.' + 'avi')
        print '---------------------------'
        for e in fullPaths:
            tmp = os.path.abspath(self.tempFolder + '/uncompressed.avi').replace('\\', '/')
            shutil.copy2(tmp, e)
            print 'File created: ', e
        
        if self.openWhenDone:    
            subprocess.call(('cmd', '/C', 'start', '', fullPaths[-1])) 
        
           
                
def getFrame():
    return pm.currentTime(q=1)


def getPanelSettings():
    flags=[]
        
def confirmSound():
    aPlayBackSliderPython = maya.mel.eval('$tmpVar=$gPlayBackSlider')
    sound = pm.timeControl( aPlayBackSliderPython, q=1, sound=1)

    if not sound:
        Vconfirm = cm.confirmDialog(title='Confirm sound', 
                                    message=('Sound is off!' + '\n\n'+
                                             '    '+ sound), 
                                    button=['Cancel', 'Ignore'], 
                                    defaultButton='Cancel', 
                                    dismissString='Cancel',
                                    ma='left',
                                    icon='warning')                      
        if Vconfirm == 'Ignore':
            pass
        else:
            raise Exception('Canceled by user.')

def confirmTimerange():
    startFrame = pm.playbackOptions(q=1, min=1)
    endFrame = pm.playbackOptions(q=1, max=1)

    startAnim = pm.playbackOptions(q=1, animationStartTime=1)
    endAnim = pm.playbackOptions(q=1, animationEndTime=1)


    if not startFrame == startAnim or not endFrame == endAnim:
        Vconfirm = cm.confirmDialog(title='Confirm',
                                    message=('Timerange is not animation range!\n'+
                                             str(startAnim) + '  ' +str(startFrame)+'     '+
                                             str(endFrame)+'  ' + str(endAnim)),
                                    button=['Cancel', 'Change'],
                                    defaultButton='Cancel',
                                    dismissString='Cancel',
                                    ma='left',
                                    icon='warning')
        if Vconfirm == 'Change':
            pm.playbackOptions(e=1, min=startAnim)
            pm.playbackOptions(e=1, max=endAnim)
        else:
            raise Exception('Canceled by user.')




