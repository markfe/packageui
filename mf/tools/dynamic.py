import pymel.core as pm
import mf.tools.lsSl as lsSl
import mf.tools.parent as tp
import mf.tools.curve as tc


class hairCurve():
    """
    Dynamic hair setup for a curve.

    ============== =============================================================
    Arguments      Description
    ============== =============================================================
    Vlist[0]       Curve
    Vlist[1]       Controller
    ============== =============================================================

    To connect the setup to the rig, skin the goal curve.
    """
    def __init__(self, Vlist=[], edit=0, **kwdArgs):
        if edit==0:
            self.create(Vlist=Vlist, **kwdArgs)
        else:
            self.edit(Vlist=Vlist)

    def create(self, Vlist=[], Vsuffix=''):
        """Creates the dynamic setup."""
        Vlist = lsSl.lsSl(Vlist, 2, 2)
        self.suffix = Vsuffix
        self.curveTrans = Vlist[0]
        self.ctrl = Vlist[1]
        self.ctrl.rename('ctrl' + self.suffix)

        self.curve = self.curveTrans.getShape()
        self.createCurves()

        self.hair = pm.createNode('hairSystem')
        self.hair.getParent().rename('hair' + self.suffix)
        self.fol = pm.createNode('follicle')

        time = pm.ls('time1')[0]

        self.goalCurve.worldSpace[0] >> self.fol.startPosition
        self.folTrans = self.fol.getParent()
        self.rbCurve.local >> self.fol.startPosition
        self.folTrans.rename('fol' + self.suffix)
        self.fol.outHair >> self.hair.inputHair[0]
        self.hair.outputHair[0] >> self.fol.currentPosition
        self.fol.outCurve >> self.dynCurve.create
        time.outTime >> self.hair.currentTime

        self.setDefaults()
        self.control()
        self.hierarchy()
        self.blendshapes()
        self.addNucleus()

    def edit(self, Vlist=[]):
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.curveTrans = None
        self.curve = None
        self.hair = None
        self.fol = None

    def createCurves(self):
        """
        Create all curves.
        """

        rebuildCurve = pm.createNode('rebuildCurve')

        rebuildCurve.rebuildType.set(0)
        rebuildCurve.spans.set(1)
        rebuildCurve.degree.set(1)
        rebuildCurve.tolerance.set(0.1)
        rebuildCurve.endKnots.set(1)
        rebuildCurve.keepRange.set(0)
        rebuildCurve.keepEndPoints.set(1)
        rebuildCurve.keepTangents.set(0)
        rebuildCurve.keepControlPoints.set(1)

        self.goalCurveTrans = pm.duplicate(self.curve)[0]
        self.goalCurveTrans.rename('goalCurve' + self.suffix)
        self.goalCurve = self.goalCurveTrans.getShape()

        self.ctrlCurveTrans = pm.duplicate(self.curve)[0]
        self.ctrlCurveTrans.rename('ctrlCurve' + self.suffix)
        self.ctrlCurve = self.ctrlCurveTrans.getShape()

        self.dynCurveTrans = pm.duplicate(self.curve)[0]
        self.dynCurveTrans.rename('dynCurve' + self.suffix)
        self.dynCurve = self.dynCurveTrans.getShape()

        self.rbCurveTrans = pm.duplicate(self.curve)[0]
        self.rbCurveTrans.rename('bsCurve' + self.suffix)
        self.rbCurve = self.rbCurveTrans.getShape()

        self.goalCurve.worldSpace[0] >> rebuildCurve.inputCurve
        print 'connecting'
        rebuildCurve.outputCurve >> self.rbCurve.create
        #raise Exception()

    def addNucleus(self):
        if not pm.objExists('nucleus_main'):
            nucleus = pm.createNode('nucleus', name='nucleus_main')
        else:
            nucleus = pm.ls('nucleus_main')[0]

        nucleus.startFrame >> self.hair.startFrame
        nucleus.outputObjects[0] >> self.hair.nextState
        self.hair.startState  >> nucleus.inputActiveStart[0]
        self.hair.currentState >> nucleus.inputActive[0]
        pm.ls('time1')[0].outTime >> nucleus.currentTime

    def control(self):
        """
        Make controller and connect some useful attributes to follicles
        and hair system.
        """
        attrVal= [['stiffness', 0.5],
                 ['damping', 0.5],
                 ['drag', 0.1],
                 ['curveAttract', 0],
                 ['lengthFlex', 0],
                 ['friction', 0.5],
                 ['gravity', 10],
                 ['strength', 0],
                 ['frequency', 1],
                 ['speed', 1],
                 ['startCurveAttract', 0.02]]

        for a in attrVal:
            self.ctrl.addAttr(a[0], defaultValue=a[1], keyable=1)

        self.ctrl.stiffness >> self.fol.stiffness
        self.ctrl.damping >> self.fol.damp
        self.ctrl.drag >> self.hair.drag
        self.ctrl.curveAttract >> self.hair.startCurveAttract
        self.ctrl.lengthFlex >> self.fol.lengthFlex
        self.ctrl.friction >> self.hair.friction
        self.ctrl.gravity >> self.hair.gravity
        self.ctrl.strength >> self.hair.turbulenceStrength
        self.ctrl.frequency >> self.hair.turbulenceFrequency
        self.ctrl.speed >> self.hair.turbulenceSpeed
        self.ctrl.startCurveAttract >>  self.fol.startCurveAttract

    def blendshapes(self):
        """
        Blendshapes are::

            dynCurve  >> curve
            ctrlCurve >> curve
            ctrlCurve >> goalCurve
        """
        blendSh = pm.blendShape (self.curve, o='world')
        pm.blendShape(blendSh[0], e=1, t=[self.curve, 0, self.ctrlCurveTrans, 1])
        pm.blendShape(blendSh[0], e=1, t=[self.curve, 1, self.dynCurveTrans, 1])
        pm.blendShape(blendSh, e=1, weight=[0,1])
        pm.blendShape(blendSh, e=1, weight=[1,1])

        blendShGoal = pm.blendShape (self.goalCurve)
        pm.blendShape(blendShGoal[0], e=1, t=[self.goalCurve, 0, self.ctrlCurveTrans, 1])
        pm.blendShape(blendShGoal, e=1, weight=[0,1])

        self.ctrl.addAttr('dynamic_blend', defaultValue=1, min=0, max=1, keyable=1)

        blend = pm.createNode('blendColors', name='blend'+self.suffix)
        blend.color1R.set (0)
        blend.color1G.set (1)
        blend.color2R.set (1)
        blend.color2G.set (0)
        self.ctrl.dynamic_blend >> blend.blender
        blend.outputR >> blendSh[0].weight[0]
        #blend.outputR >> blendShGoal[0].weight[0]
        blendShGoal[0].weight[0].set(0)
        blend.outputG >> blendSh[0].weight[1]

    def hierarchy(self):
        """
        Create the DG hierarchie.
        """
        self.gT = tp.trans(em=1, w=1, n='trans' + self.suffix).group
        self.gNT = tp.noTrans(em=1, w=1, n='no_trans' + self.suffix).group

        self.gT|self.folTrans
        self.gT|self.ctrl
        self.gNT|self.goalCurveTrans
        self.gNT|self.dynCurveTrans
        self.gNT|self.ctrlCurveTrans
        self.gNT | self.rbCurve
        self.gNT|self.hair.getParent()

    def setDefaults(self):
        """
        Overwrite mayas defaults for some attributes.
        """
        defFol= {'restPose': 1,
                 'startDirection': 1,
                 'overrideDynamics': 1,
                 'damp': 0.5,
                 'stiffness': 0.5,
                 'degree': 1,
                 'clumpWidth': 0.3}

        defHair= {'simulationMethod': 3}

        for k in defFol.iterkeys():
            folAttr = getattr(self.fol, k)
            # getattr returns the 'shortName' so it needs to be converted back.
            folAttr.set(defFol[folAttr.attrName(longName=1)])

        for k in defHair.iterkeys():
            hairAttr = getattr(self.hair, k)
            hairAttr.set(defHair[hairAttr.attrName(longName=1)])#


