"""
ribbon Setup  
"""

import pymel.core as pm
import mf.builder.nodes
from mf.tools import lsSl 
from mf.tools import snap
from mf.tools import parent as tp
from mf.tools import proxyGeo as pg
from mf.tools import distance as td
from mf.tools import key as tk
from mf.tools import cons as tc

class ribbon():
    """
    Ribbon setup. One ribbon between two objects.
    """
    def __init__(self, Vlist=[], Vsuffix='', numBindJoints=8, limbSection=1,
                 side=1):
        self.sl = lsSl.sl(Vlist, 3, 3,
                  slDict={
                          'start':'0',
                          'mid':'1',
                          'end':'2',
                          },
                          Vrename=1, 
                          Vsuffix=Vsuffix)
        
        self.side = side
        self.suffix = Vsuffix
        self.gT = tp.trans(em=1, w=1, n='trans' + self.suffix).group
        self.gNT = tp.noTrans(em=1, w=1, n='no_trans' + self.suffix).group 
        self.scaleAll = pm.createNode('multiplyDivide', 
                                  name='scale_all' + self.suffix)
        self.scaleAll.input2.set([1, 1, 1])
        self.scaleAll.input1.set([1, 1, 1])
        
        self.measure()
        self.startEndRot()
        self.createPlane()
        self.skinPlane()
        self.follicles(numBindJoints)
        self.rot()
        self.scale()
        self.cleanup()
        
        pm.select(self.sl.mid)
        
    
    def measure(self):
        """
        Get distances start to end.
        """
        self.dist = td.dist([self.sl.end, self.sl.start])
        self.len = self.dist.distance()
        self.dist.calibrate(1)
        
    def startEndRot(self):
        """
        Aims the start and end to the mid ctrl.
        Creates the rotation setup for the x axis.
        """ 
        self.null_start = tp.nullGroup([self.sl.start], Vtype=1, 
                                      Vname='null_start'+self.suffix,
                                      Vhierarchy=3)[0] 
        self.null_null_start = tp.nullGroup([self.null_start], Vtype=2, 
                                 Vname='null_null_start'+self.suffix,
                                 Vhierarchy=3)[0] 

        self.par_start = tp.nullGroup([self.sl.start], Vtype=1, 
                                      Vname='par_start'+self.suffix,
                                      Vhierarchy=3)[0]                               
        
        self.null_end = tp.nullGroup([self.sl.end], Vtype=1, 
                                      Vname='null_end'+self.suffix,
                                      Vhierarchy=3)[0] 
        self.null_null_end = tp.nullGroup([self.null_end], Vtype=2, 
                                 Vname='null_null_end'+self.suffix,
                                 Vhierarchy=3)[0] 
                                      
        self.par_end = tp.nullGroup([self.sl.end], Vtype=1, 
                                      Vname='par_end'+self.suffix,
                                      Vhierarchy=3)[0]
        
        self.aim_start = tp.nullGroup([self.sl.start], Vtype=2, 
                                      Vname='start_aim'+self.suffix)[0] 
        self.aim_end = tp.nullGroup([self.sl.end], Vtype=2, 
                                      Vname='end_aim'+self.suffix)[0]    
        self.sl.end|self.aim_end
        self.sl.start|self.aim_start
        
        null_aim_start = tp.nullGroup([self.aim_start], Vtype=2, 
                                      Vname='null_start_aim'+self.suffix,
                                      Vhierarchy=3)[0] 
        null_aim_end = tp.nullGroup([self.aim_end], Vtype=2, 
                                      Vname='null_end_aim'+self.suffix,
                                      Vhierarchy=3)[0] 
        self.null_start|null_aim_start
        self.null_end|null_aim_end
        pm.pointConstraint(self.sl.start, null_aim_start)
        pm.pointConstraint(self.sl.end, null_aim_end)

        vMult = 1
        if self.side == 2:
            vMult = -1
        
        # mid---------------------------------------
        pm.delete(pm.parentConstraint(self.sl.start, self.sl.end, self.sl.mid))
        null_mid = tp.nullGroup([self.sl.mid], Vtype=1, 
                                 Vname='null_mid'+self.suffix,
                                 Vhierarchy=3)[0]
        self.null_null_mid = tp.nullGroup([null_mid], Vtype=2, 
                                 Vname='null_null_mid'+self.suffix,
                                 Vhierarchy=3)[0] 
    
        if self.side==2:
            null_mid.rx.set(180)
            null_mid.rz.set(180)   
                                  
        pm.pointConstraint(self.aim_start, self.aim_end, null_mid)
        pm.orientConstraint(self.aim_start, self.aim_end, self.sl.mid)
        
         
        pm.aimConstraint(self.sl.mid, self.aim_end, aimVector=[-1*vMult,0,0], 
                         worldUpType='objectrotation',
                         worldUpObject=self.null_end)
        pm.aimConstraint(self.sl.mid, self.aim_start, aimVector=[1*vMult,0,0], 
                         worldUpType='objectrotation',
                         worldUpObject=self.null_start)
        
        self.start_bind = tp.nullGroup([self.aim_start], Vtype=2, Vhierarchy=4,
                                      Vname='start_bind'+self.suffix)[0] 
        self.end_bind = tp.nullGroup([self.aim_end], Vtype=2, Vhierarchy=4,
                                      Vname='end_bind'+self.suffix)[0] 
        

        
        
    def scale(self):
        """
        scale interpolation for the "thickness" to start and end point 
        of the setup. Connect hand or shoulder scale to color1R and color1G
        """
        self.scaleBlend = pm.createNode('blendColors', 
                                    name='scaleBlend' + self.suffix)
        
        self.scaleBlend.color1R.set(1)
        self.scaleBlend.color1G.set(1)
        
        self.sl.mid.thickness >> self.scaleBlend.color2R
        self.sl.mid.thickness >> self.scaleBlend.color2G
        
        self.scaleBlend.outputR >> self.iRotClsS.shift.sy
        self.scaleBlend.outputR >> self.iRotClsS.shift.sz
        
        self.scaleBlend.outputG >> self.iRotClsE.shift.sy
        self.scaleBlend.outputG >> self.iRotClsE.shift.sz
    
    def rot(self):
        """
        Interpoaltes the rotation from e.g. shoulder to elbow, elbow to wrist.
        """
        
        self.start_roll_goal = tp.nullGroup([self.null_start], Vtype=3, 
                                      Vname='start_rollg'+self.suffix,
                                      Vhierarchy=4)[0]
        self.end_roll_goal = tp.nullGroup([self.null_end], Vtype=3, 
                                      Vname='end_rollg'+self.suffix,
                                      Vhierarchy=4)[0]
        self.start_roll_goalB = tp.nullGroup([self.sl.start], Vtype=3, 
                                      Vname='start_rollgB'+self.suffix,
                                      Vhierarchy=4)[0]
        self.end_roll_goalB = tp.nullGroup([self.sl.end], Vtype=3, 
                                      Vname='end_rollgB'+self.suffix,
                                      Vhierarchy=4)[0] 
        self.sl.mid.addAttr('twist_orient_x', k=1, dv=0) 
        self.sl.mid.addAttr('twist_orient_y', k=1, dv=0)  
        self.sl.mid.addAttr('twist_orient_z', k=1, dv=0)
        self.sl.mid.twist_orient_x >> self.start_roll_goal.rx   
        self.sl.mid.twist_orient_y >> self.start_roll_goal.ry 
        self.sl.mid.twist_orient_z >> self.start_roll_goal.rz 
        
        self.sl.mid.addAttr('twist_orient_end_x', k=1, dv=0) 
        self.sl.mid.addAttr('twist_orient_end_y', k=1, dv=0)  
        self.sl.mid.addAttr('twist_orient_end_z', k=1, dv=0)
        self.sl.mid.twist_orient_end_x >> self.end_roll_goal.rx   
        self.sl.mid.twist_orient_end_y >> self.end_roll_goal.ry 
        self.sl.mid.twist_orient_end_z >> self.end_roll_goal.rz                           
                                      
        
        
        startRot = tc.endRot([self.start_roll_goal, self.start_roll_goalB, self.sl.mid], 
                             Vsuffix='_rot', attrName='auto_rot', weights=[0.5,1])[0]
        endRot = tc.endRot([self.end_roll_goal, self.end_roll_goalB], 
                           Vsuffix='_rot', weights=[0.5,1])[0]
            
        tc.distribute(Vlist=self.folRot+[endRot, startRot], 
                      Vtype=6, Voffset=0, permanent=1, skipRotate=['none','y','z'])    
        
        overrotMultStart = pm.createNode('multiplyDivide', 
                                    name='overrotMultStart' + self.suffix)
        overrotMultStart.operation.set (1)
        startRot.rotate >> overrotMultStart.input1
        overrotMultStart.input2.set([1.5, 1.5, 1.5])
        
        overrotMultEnd = pm.createNode('multiplyDivide', 
                                    name='overrotMultEnd' + self.suffix)
        overrotMultEnd.operation.set (1)
        endRot.rotate >> overrotMultEnd.input1
        overrotMultEnd.input2.set([1.5, 1.5, 1.5])
        
        tc.distribute(Vlist=self.folRot+[overrotMultEnd, overrotMultStart], 
                      Vtype=6, Voffset=0, permanent=1, skipRotate=['none','y','z'])
        
        xRotStart = pm.spaceLocator(n='xRot' + self.suffix)
        xRotEnd = pm.spaceLocator(n='xRot' + self.suffix)
        
        snap.snap([xRotStart, startRot])
        snap.snap([xRotEnd, endRot])
        self.aim_start|xRotStart
        self.aim_end|xRotEnd
        
        overrotMultEnd.outputX >> xRotEnd.rotateX
        overrotMultStart.outputX >> xRotStart.rotateX
        
        shiftMult = 1
        if self.side == 2:
            shiftMult = -1
        shiftVal = self.len*0.2    
        self.iRotClsS = tc.inbRot(Vlist=[xRotStart, self.sl.start], Vproxy=1,
                                  autoShift=shiftVal, shiftDirection=-1*shiftMult,
                                  Vsuffix=self.suffix, 
                                  proxyScale=self.len/10, 
                                  label='inbRot_A',
                                  side=self.side)
        
        self.iRotClsE = tc.inbRot(Vlist=[xRotEnd, self.sl.end], Vproxy=1, 
                                  autoShift=shiftVal, shiftDirection=1*shiftMult,
                                  Vsuffix=self.suffix, 
                                  proxyScale=self.len/10, 
                                  label='inbRot_B',
                                  side=self.side)
        
        
        
    def createPlane(self):
        """
        Creates and positions the nurbs plane.
        """
        self.nurbs = pm.nurbsPlane(ax=[0, 1, 0], 
                                   w=self.len, 
                                   lr=0.1, d=2, u=3, v=1, ch=1, 
                                   n='nurbs' + self.suffix)[0]
        
        self.dist.align([self.nurbs], 50)
        self.gNT|self.nurbs
        
    def skinPlane(self):
        """
        Skins the nurbs plane.
        """
        #snap.snap([self.sl.mid, self.arcObj])
        midBind = tp.nullGroup(Vlist=[self.sl.mid], Vhierarchy=4, Vtype=2, 
                               Vsuffix='_bind')[0]
        
        bindJoints = [self.start_bind, self.end_bind, midBind]
        skin = pm.skinCluster(bindJoints, self.nurbs, 
                                   dr=1.5, toSelectedBones=True, 
                                   ignoreHierarchy=True, mi=2)
         
        nurbsCV =  self.nurbs.getShape().cv  
  
        # set all weights to zero
        pm.skinPercent([skin, self.nurbs.getShape()], pruneWeights=100, normalize=0)
        # set weights
        pm.skinPercent (skin, nurbsCV[0], transformValue=[self.end_bind, 1], normalize=0)
        pm.skinPercent (skin, nurbsCV[1], transformValue=[self.end_bind, 0.5], normalize=0)
        pm.skinPercent (skin, nurbsCV[1], transformValue=[midBind, 0.5], normalize=0)
        pm.skinPercent (skin, nurbsCV[2], transformValue=[midBind, 1], normalize=0)
        
        pm.skinPercent (skin, nurbsCV[3], transformValue=[self.start_bind, 0.5], normalize=0)
        pm.skinPercent (skin, nurbsCV[3], transformValue=[midBind, 0.5], normalize=0)
        
        pm.skinPercent (skin, nurbsCV[4], transformValue=[self.start_bind, 1], normalize=0)
        
        #self.dist.connect([self.start_bind.sx, self.end_bind.sx, midBind.sx])
        scale_mid = pm.createNode('multiplyDivide', 
                                  name='scale_all_mid' + self.suffix)
        scale_mid.input2.set([1, 1, 1])
        scale_mid.input1.set([1, 1, 1])
        
        #self.scaleAll.outputX >> scale_mid.input1X
        self.dist.connect([scale_mid.input2X])
        self.dist.connect([self.start_bind.sx])
        self.dist.connect([self.end_bind.sx])
        self.end_bind.radius.set(2)
        self.start_bind.radius.set(2)
        
        scale_mid.outputX >> midBind.sx
        

    def follicles(self, num=8):
        """
        Creates the follicles.
        """
        folGrp = pm.group(w=1, em =1, n='fol_grp'+self.suffix)
        
        self.bindFol = []
        self.folRot = []
        
        for i in range(num):
            U = i * (1.0/num) + (0.5/num)
            folShape = pm.createNode( 'follicle' )
            fol = folShape.getParent()
            pm.rename(fol, 'fol' + str(i) + self.suffix)
            self.nurbs.local >> folShape.inputSurface
            self.nurbs.worldMatrix >> folShape.inputWorldMatrix
            folShape.outTranslate >> fol.translate
            folShape.outRotate >> fol.rotate
            folShape.parameterU.set(U)
            folShape.parameterV.set(.5)
            
            null_rot = pm.group(em=1, w=1, n='fol' + str(i) + self.suffix + '_rt')
            rot = pm.group(em=1, w=1, n='fol' + str(i) + self.suffix + '_rt')
            snap.snapTrans([rot, null_rot, fol])
            snap.snapRot([rot, null_rot, self.sl.start])
            
            folGrp|fol|null_rot|rot
                
            cubeBnd = pg.proxyGeo([rot], VcenterPivot=1, chain=0, 
                        xScale=2, yzScaleGlobal=self.len*0.04, 
                        side=self.side,
                        label=str(rot))
            
            self.sl.start.sx >> rot.sx
            
            
            self.folRot.append(rot)
            self.bindFol.append(cubeBnd.pxNodeLs[0].bj)
        
        self.gNT|folGrp
        
        self.sl.mid.addAttr('thickness', k=1, dv=1, min=0.2, max=2)
        scale_thick = pm.createNode('multiplyDivide', 
                                  name='scale_thick' + self.suffix)
        scale_fol = pm.createNode('multiplyDivide', 
                                  name='scale_fol' + self.suffix)
        
        scale_thick.input2.set([1, 1, 1])
        scale_thick.input1.set([1, 1, 1])
        self.scaleAll.output >> scale_thick.input2
        
        self.gT.s >> self.scaleAll.input2
        self.sl.mid.thickness >> scale_thick.input1Y
        self.sl.mid.thickness >> scale_thick.input1Z
        
        self.scaleAll.output >> scale_fol.input1
        self.dist.connect([scale_fol.input2X])
        
        for e in self.folRot:    
            scale_fol.outputX >> e.sx
            scale_thick.outputY >> e.sy
            scale_thick.outputY >> e.sz      
        
    def cleanup(self):
        """
        Cleanup channels and hierarchie. 
        """
        for jnt in [self.sl.mid]:
            attrList = [jnt.rx, jnt.ry, jnt.rz,
                        jnt.sy, jnt.sz, 
                        jnt.v]
            for attr in attrList:
                attr.set (lock=1, keyable=0, channelBox=0)
        
        self.gT|self.null_null_start
        self.gT|self.null_null_end
        self.gT|self.dist.start
        self.gT|self.null_null_mid

        
        
        
        
        
        