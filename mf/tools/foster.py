import pymel.core as pm
import mf.builder.nodes as bn
from mf.tools import lsSl
import mf.tools.snap as tsnap
import mf.tools.curve as tc


class foster():
    """
    Foster objects are used to connect a rig parts to objects on a referenced 
    rig. This is used to e.g. connect parts of the face to a referenced body 
    while the body rig is still in work. After finishing the face it can be 
    disconnected and then be imported or referenced into the body rig.
    With the foster objects it can be reconnected.
    
    ========== ================================================================
    Arguments  Description
    ========== ================================================================
    direction  direction of connection from or to the foster object
                   0 = original >> foster object,
                   1 = original << foster object
    ========== ================================================================
    
    Examples::
    
        import mf.builder.nodes as bn
        reload(bn)
        
        # switch off
        d = bn.get(pm.ls(transforms=1), 'transform_foster')
        for o in d:
            o.disconnectAttr()
            o.unParent()
            
        # switch on
        d = bn.get(pm.ls(transforms=1), 'transform_foster')
        for o in d:
            o.connect()
            
        # add shape
        d = bn.get(pm.ls(transforms=1), 'transform_foster')
        for o in d:
            o.shape(radius=2)
        
        # remove shape
        d = bn.get(pm.ls(transforms=1), 'transform_foster')
        for o in d:
            o.delShape()
    """
    def __init__(self, Vlist=[], Vsuffix='', edit=0, direction=1, **kwdArgs):        
        """"""
        if edit: 
            self.edit(Vlist=Vlist)   
        else: 
            self.create(Vlist=Vlist, direction=direction, **kwdArgs)
            
    def create(self, Vlist=[], direction=1, **kwdArgs):
        """creates the objects"""
        Vlist = lsSl.lsSl(Vlist, 1, 0)
        
        for o in Vlist:
            fosterName = str(o).split(':')[-1] + '_fp'
            self.foster = pm.duplicate(o, parentOnly=1, n=fosterName)[0]
            
            self.__removeCustomAttr()
            self.foster.addAttr('fostered_parent', dt='string')
            
            self.setFosteredName(o)
            
            self.foster.addAttr('fostered_direction', at='long')
            self.setDirection(direction)
            
            self.foster.addAttr(bn.register('transform_foster')[0]) 
            
            self.__connectFoster(o)
            self.annotation()
            
    def edit(self, Vlist=[]):
        """Finds all objects."""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.foster = Vlist[0]
    
    def setFosteredName(self, name):
        """Sets the name of the original object."""
        self.foster.fostered_parent.set(str(name))
    
    def setDirection(self, val):
        """Sets the direction for the connections.
        0 = original >> foster
        1 = original << foster"""
        if not self.getDirection() == val:
            self.disconnectAttr()
        return self.foster.fostered_direction.set(val)
        
    def getDirection(self):
        return self.foster.fostered_direction.get()
    
    def __removeCustomAttr(self):
        """Removes all custom atrributes that are not in the cannel box."""
        y = self.foster.listAttr(ud=1, k=1)
        print y
        z = self.foster.listAttr(ud=1)
        for a in y:
            try:
                z.remove(a)
            except:
                pass
        for a in z:
            a.setLocked(0)
            a.setKeyable(1)
            pm.deleteAttr(a)
        
    def __connectFoster(self, orig):
        """Connects foster object to the original object."""
        origPar = orig.getParent()
        direction = self.getDirection()
        self.disconnectAttr()
        
        if not origPar:
            pass
        else:
            if not self.foster.getParent() == origPar:
                ret = pm.parent(self.foster, origPar, r=1)
        
        attstr = pm.listAttr(orig, keyable=1)
        
        for a in attstr:    
            if pm.hasAttr(self.foster, a):
                scAttr = pm.ls(str(self.foster) + '.' + a)[0]
                
                if not scAttr.isLocked():
                    goalAttr = pm.ls(str(orig) + '.' + a)[0] 
                    
                    if direction: 
                        scAttr >> goalAttr
                    else:
                        goalAttr >> scAttr
    
    def disconnectAttr(self):
        """Disconnects all attributes"""
        direction = self.getDirection()
        attstr = pm.listAttr(self.foster, keyable=1)
        
        for a in attstr:
            fosterAttr = pm.ls(str(self.foster) + '.' + a)[0]
            inp = fosterAttr.inputs(plugs=1)
            outp = fosterAttr.outputs(plugs=1)
            
            if not direction:
                if not inp == []:
                    pm.disconnectAttr(inp[0], fosterAttr)    
            else:
                if not outp == []:
                    pm.disconnectAttr(fosterAttr, outp[0])
                
    def getOrig(self):
        """Returns the original object of the foster object."""
        origName =  self.foster.fostered_parent.get()
        orig = None
        try:
            orig = pm.ls(origName)[0]
        except:
            try:
                orig = pm.ls(':'.join(origName.split(':')[1:]))[0]
                print orig
            except:
                pass
        return orig
                                
    def unParent(self, snapPos=1):
        """Unparents all foster objects to the world.
        Disconnect all attributes before unparenting."""
        try:
            self.disconnectAttr()
            orig = self.getOrig()
            
            if self.foster.getParent():
                pm.parent(self.foster, w=1, r=1)
            
            if snapPos and orig and not self.getDirection():
                tsnap.snap([self.foster, orig])
        except:
            pass
        
    def connect(self):
        """Reconnects all foster objects to their originals."""
        orig = self.getOrig()
        if orig:
            self.__connectFoster(orig)
        
    def shape(self, normal=[1, 0, 0], radius=1.2, 
                 Vhistory=0, center=[0, 0, 0]):
        """Creates a ctrl shape if there is none."""
        if not self.foster.getShape():
            if self.getDirection():
                radius = radius*1.1
            
            tc.controlShape(Vlist=[self.foster], normalX=normal[0], 
                            normalY=normal[1], 
                            normalZ=normal[2], radius=radius, 
                            Vhistory=Vhistory, center=center)
    def delShape(self):
        """Deletes the shape if there is one."""
        try:
            pm.delete(self.foster.getShape())
        except:
            pass
        
    def annotation(self):
        """Creates an annotation shape if there is none."""
        self.Vannot = pm.createNode('annotationShape', 
                                        n='not_'  + str(self.foster))
        VannotTrans = self.Vannot.getParent()
        Vloc = pm.spaceLocator (name='loc' + str(self.foster))
        self.VlocShape = Vloc.getShape()
        #self.VlocShape.worldMatrix[0] >> self.Vannot.dagObjectMatrix[0]
        pm.parent(self.VlocShape, self.foster, shape=1, r=1)
        pm.parent(self.Vannot, self.foster, shape=1, r=1)
        pm.delete(VannotTrans, Vloc, self.VlocShape)
        self.Vannot.text.set(str(self.foster)) 
        self.Vannot.displayArrow.set(0)
        
        self.Vannot.overrideEnabled.set(1)
        if self.getDirection():
            self.Vannot.overrideColor.set(16)
        else:
            self.Vannot.overrideColor.set(17)
        return [self.Vannot] 


def foster_create_from():
    foster(Vlist=[], Vsuffix='',edit=0, direction=0)
    
def foster_create_to():
    foster(Vlist=[], Vsuffix='',edit=0, direction=1)

def foster_select_to():
    sl = pm.ls(sl=1)
    x = pm.ls(sl=1)[-1]
    c = bn.get(x.getParent().getChildren(), 'transform_foster')
    pm.select(sl[:-1])
    for x in c:
        if x.getDirection() == 0:
            pm.select(x.foster, add=1)

def foster_select_from():            
    sl = pm.ls(sl=1)
    x = pm.ls(sl=1)[-1]
    c = bn.get(x.getParent().getChildren(), 'transform_foster')
    pm.select(sl[:-1])
    for x in c:
        if x.getDirection() == 1:
            pm.select(x.foster, add=1)
            
def foster_on():
    d = bn.get(pm.ls(transforms=1), 'transform_foster')
    for o in d:
        print o.foster
        o.connect()

def foster_off():
    d = bn.get(pm.ls(transforms=1), 'transform_foster')
    for o in d:
        print o.foster
        o.disconnectAttr() 
        o.unParent()

def foster_shape_create():
    d = bn.get(pm.ls(transforms=1), 'transform_foster')
    for o in d:
        o.shape(radius=2)
        
def foster_shape_create_anot():
    d = bn.get(pm.ls(transforms=1), 'transform_foster')
    for o in d:
        o.annotation()

def foster_shape_delete():
    d = bn.get(pm.ls(transforms=1), 'transform_foster')
    for o in d:
        o.delShape()