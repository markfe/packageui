from __future__ import with_statement

import os
import subprocess

import pymel.core as pm
import maya.mel as mel
import mf.builder.main as bm
import mf.tools.file as ft
import mf.tools.skin as skin
import mf.tools.blendshape as bs
import mf.tools.mesh as tm


def UI(path=''):
    """
    UI to manage skin weights meshes and influences.
    """
    relPath = bm.getRelativePath()
    for i in range(2):
        relPath = os.path.split(relPath)[0]
    if path == '':
        try:
            path = os.path.abspath(relPath + '\\templates\\skin')
        except:
            pass
    if not os.path.exists(path):
        path = ''   
            
    UIname = 'skinUI'
    try:
        pm.deleteUI(UIname)
    except: 
        pass
       
    with pm.window(UIname, t=UIname, h=150, w=300) as win:
        
        
        with pm.formLayout('skinWeightsMainFl'):
            pm.textField('mainPathSkinTF', vis=1, text=path,
                         ec='import mf.tools.skinUI; mf.tools.skinUI.UIlistSkins()',
                         ann='Path')
            
            with pm.columnLayout('skinWriteMainFl', adj=1):
                pm.button(l='Import', c='import mf.tools.skinUI; mf.tools.skinUI.smartLoad()')
                pm.textField('meshExportNameTF', w=150, text='mesh_name', ann='Mesh Name')
                pm.textField('weightsExportNameTF', text='weight_name', ann='Weight Name')
                pm.button(l='Export', c='import mf.tools.skinUI; mf.tools.skinUI.smartWrite()')
            
            with pm.menuBarLayout('skinMainMenuBar') as skinMain:
                
                with pm.menu('File',  tearOff=True):
                    pm.menuItem(label='Browse', 
                                c='import mf.tools.skinUI; mf.tools.skinUI.UItempFileBrowser()')
                    pm.menuItem(label='Open in Explorer', 
                                c='import mf.tools.skinUI; mf.tools.skinUI.browse()')
                    pm.menuItem( divider=True )
                    pm.menuItem(label='Refresh', 
                                c='import mf.tools.skinUI; mf.tools.skinUI.UIrefresh()')
                    
                    
                with pm.menu('Edit',  tearOff=True):
                    pm.menuItem(label='Get names', 
                                c='import mf.tools.skinUI; mf.tools.skinUI.smartWriteUI()')
                
                with pm.menu('Tools',  tearOff=True):    
                    pm.menuItem(label='Paste Weights', 
                                c='import mf.tools.skin; mf.tools.skin.pasteWeights()')
            
            with pm.paneLayout('skinSkinPL', configuration="vertical4"): 
                
                with pm.formLayout('skinMeshesFl', parent='skinSkinPL'):
                    pm.textScrollList('skinMeshesSL', 
                                      allowMultiSelection=False, 
                                      ann='Sub Folders',
                                      font='boldLabelFont',
                                      selectCommand = 'import mf.tools.skinUI; mf.tools.skinUI.UIlistWeights()')
                    
                with pm.formLayout('meshesFl', parent='skinSkinPL'):
                    pm.textScrollList('meshesSL', 
                                      allowMultiSelection=False, 
                                      ann='Meshes',
                                      dcc='import mf.tools.skinUI; mf.tools.skinUI.UIdeselectWeights()')
                    
                with pm.formLayout('skinWeightsFl', parent='skinSkinPL'):
                    pm.textScrollList('skinWeightsSL', 
                                      allowMultiSelection=False, 
                                      ann='Weights',
                                      sc='import mf.tools.skinUI; mf.tools.skinUI.UIweightsSelect()',
                                      dcc='import mf.tools.skinUI; mf.tools.skinUI.UIdeselectMesh()')
                    
                with pm.formLayout('skinClusterFl', parent='skinSkinPL'):
                    pm.textScrollList('skinClusterSL', 
                                      allowMultiSelection=False, 
                                      en=1,
                                      ann='Skin Cluster',
                                      sc='import mf.tools.skinUI; mf.tools.skinUI.UIclusterSelect()',
                                      dcc='import mf.tools.skinUI; mf.tools.skinUI.selectSkinCluster()')
                     
    with pm.popupMenu('meshesPopup', p='meshesFl'):
        pm.menuItem(label='Delete Meshes', c='import mf.tools.skinUI; mf.tools.skinUI.deleteMeshFile()')
    
    
    with pm.popupMenu('skinWeightsPopup', p='skinWeightsFl'):
        pm.menuItem(label='Select Influences', c='import mf.tools.skinUI; mf.tools.skinUI.selectInfluences()')
        pm.menuItem(label='Delete Weights', c='import mf.tools.skinUI; mf.tools.skinUI.deleteSkinFile()')
    
    with pm.popupMenu('skinWeightsPopup', p='skinClusterFl'):
        pm.menuItem(label='Select Influences', c='import mf.tools.skinUI; mf.tools.skinUI.selectSkinClusterInfluences()')
        pm.menuItem(label='Select Meshes', c='import mf.tools.skinUI; mf.tools.skinUI.selectSkinClusterMeshes()')
        
    pm.formLayout(
        'skinWeightsMainFl', e=True,
        af=[('skinSkinPL', 'left', 0), ('skinSkinPL', 'bottom', 0),  
            ('skinSkinPL', 'right', 0), ('skinSkinPL', 'top', 40), 
            ('mainPathSkinTF', 'left', 0), ('mainPathSkinTF', 'bottom', 0), 
            ('mainPathSkinTF', 'right', 0),
            (skinMain, 'left', 0), (skinMain, 'bottom', 0), 
            (skinMain, 'right', 0), (skinMain, 'top', 0),
            ('skinWriteMainFl', 'left', 0), ('skinWriteMainFl', 'bottom', 0),
            ('skinWriteMainFl', 'right', 0)
            ],
        ac=[(skinMain, 'bottom', 0, 'mainPathSkinTF'),
            ('mainPathSkinTF', 'bottom', 0, 'skinSkinPL'),
            ('skinSkinPL', 'bottom', 0, 'skinWriteMainFl') 
            ]
            ) 
    
    pm.formLayout(
        'skinMeshesFl', e=True,
        af=[('skinMeshesSL', 'left', 0), ('skinMeshesSL', 'bottom', 0), 
            ('skinMeshesSL', 'right', 0), ('skinMeshesSL', 'top', 0),
            ]
            ) 
    pm.formLayout(
        'skinWeightsFl', e=True,
        af=[('skinWeightsSL', 'left', 0), ('skinWeightsSL', 'bottom', 0), 
            ('skinWeightsSL', 'right', 0), ('skinWeightsSL', 'top', 0),
            ]
            ) 
    
    pm.formLayout(
        'meshesFl', e=True,
        af=[('meshesSL', 'left', 0), ('meshesSL', 'bottom', 0), 
            ('meshesSL', 'right', 0), ('meshesSL', 'top', 0),
            ]
            )
    
    pm.formLayout(
        'skinClusterFl', e=True,
        af=[('skinClusterSL', 'left', 0), ('skinClusterSL', 'bottom', 0), 
            ('skinClusterSL', 'right', 0), ('skinClusterSL', 'top', 0),
            ]
            )
        
    pm.showWindow(win)
    
    if not path == '':
        meshes = ft.getSubdirs(path)
        pm.textScrollList ('skinMeshesSL', e=1, removeAll=1)
        pm.textScrollList('skinMeshesSL', append=meshes, e=1)
    
    listCluster()



def UIrefresh():
    path = pm.textField('mainPathSkinTF', q=1, text=1)
    UI(path)

def browse():
    """
    Opens the pose folder in the Windows explorer.
    """
    path = os.path.abspath(pm.textField('mainPathSkinTF', q=1, text=1))
    mesh = pm.textScrollList('skinMeshesSL', q=1, selectItem=1)
    print mesh
    print path
    if mesh == []:
        subprocess.Popen('explorer ' + path)
    else:
        mesh = mesh[0]
    
    subprocess.Popen('explorer ' + os.path.join(path, mesh))

def UIweightsSelect():
    pass
    #pm.textScrollList ('skinClusterSL', e=1, da=1)

def UIclusterSelect():
    pass
    #pm.textScrollList ('skinWeightsSL', e=1, da=1)
    

def UIdeselectWeights():
    pm.textScrollList ('skinWeightsSL', e=1, da=1)


def UIdeselectMesh():
    pm.textScrollList ('meshesSL', e=1, da=1)

def selectInfluences():
    skinPath = getSkinPath()
    weightsPath = getWeightPath(skinPath) 
    influence = skin.readInfluences(weightsPath)
    info = ['Influences not found:']
    pm.select(cl=1)
    for i in influence:
        try:
            pm.select(i, add=1)
        except:
            info.append(str(i)+'')
    if len(info) > 1:
        for i in info:
            print i

def selectSkinCluster():
    cluster = pm.textScrollList('skinClusterSL', q=1, selectItem=1)
    pm.select(cluster)
            
def selectSkinClusterInfluences():
    cluster = pm.textScrollList('skinClusterSL', q=1, selectItem=1) 
    if not cluster == []:
        cluster = pm.ls(cluster[0]) 
    else:
        cluster = None
        
    if cluster:
        pm.select(pm.skinCluster(cluster, q=1, inf=1))
    

def selectSkinClusterMeshes():
    cluster = pm.textScrollList('skinClusterSL', q=1, selectItem=1) 
    if not cluster == []:
        cluster = pm.ls(cluster[0]) 
    else:
        cluster = None
        
    if cluster:
        pm.select(pm.skinCluster(cluster, q=1, geometry=1))


def exportMeshes(path, force=0):
    type = ft.getFileFormat(path)
    
    if os.path.exists(path):
        raise Exception('File exists!')
    else:
        sel = pm.ls(sl=1)[0]
        dub = tm.duplicate([sel], rr=1)[0]
        
        try:
            pm.parent(dub, w=1)
        except:
            pass
        
        pm.select(dub)
        pm.exportSelected(path, typ=type, es=1, force=force,
               constructionHistory=0, constraints=0,
               channels=0, expressions=0,uiConfiguration=0, shader=1)
        pm.delete(dub)
        pm.select(sel)
           
def UIsetPath(Vfilename, VfileType):
    """
    Sets the project Path.
    """
    path=Vfilename
    pm.textField('mainPathSkinTF', e=1, text=path)
    UI(path)

def UItempFileBrowser():
    pm.fileBrowserDialog(m=4, fc=UIsetPath, an="Set Path")

def UIlistSkins():
    
    
    path = pm.textField('mainPathSkinTF', q=1, text=1)
    
   
    meshes = ft.getSubdirs(path)
    pm.textScrollList ('skinMeshesSL', e=1, removeAll=1)
    pm.textScrollList('skinMeshesSL', append=meshes, e=1)
    pm.textScrollList ('skinWeightsSL', e=1, removeAll=1)
    pm.textScrollList ('meshesSL', e=1, removeAll=1)
    listCluster()
    

def UIlistWeights():
    path = pm.textField('mainPathSkinTF', q=1, text=1)
    mesh = pm.textScrollList('skinMeshesSL', q=1, selectItem=1)[0]
    
    files = os.listdir(os.path.join(path, mesh))
    
    weights = ft.sortByExtension(files, '.txt')
    meshes = ft.sortByExtension(files, '.ma') 
    meshes = meshes + ft.sortByExtension(files, '.mb')
    
    pm.textScrollList ('skinWeightsSL', e=1, removeAll=1)
    pm.textScrollList('skinWeightsSL', append=weights, e=1)
    
    pm.textScrollList ('meshesSL', e=1, removeAll=1)
    pm.textScrollList('meshesSL', append=meshes, e=1)
    
    listCluster()

def listCluster():
    cluster =pm.ls(type='skinCluster')
    pm.textScrollList ('skinClusterSL', e=1, removeAll=1)
    pm.textScrollList('skinClusterSL', append=cluster, e=1)

def smartWriteUI():
    UIname = 'skinWriteUI'
    try:
        pm.deleteUI(UIname)
    except: 
        pass
    UImesh = pm.textScrollList('meshesSL', q=1, selectItem=1)
    UIweights = pm.textScrollList('skinWeightsSL', q=1, selectItem=1)
    if UImesh == []:
        UImesh = ''
    else:
        UImesh = os.path.splitext(UImesh[0])[0]
        
    if UIweights == []:
        UIweights = ''
    else:
        UIweights = os.path.splitext(UIweights[0])[0]
    skinPath = getSkinPath()
    meshPath = getMeshPath(skinPath)
    weightsPath = getWeightPath(skinPath)
    
    pm.textField('meshExportNameTF', e=1, text=UImesh)
    pm.textField('weightsExportNameTF', e=1, text=UIweights)

def smartWrite():
    skinPath = getSkinPath()
    UImesh = pm.textField('meshExportNameTF', q=1, text=1)
    UIweights =pm.textField('weightsExportNameTF', q=1, text=1)
    
    weightsPath = None
    
    if not UImesh == '':
        meshPath = os.path.join(skinPath, UImesh + '.ma')
    else:
        meshPath = None
    
    if not UIweights == '':    
        weightsPath = os.path.join(skinPath, UIweights + '.txt')
    else:
        weightsPath = None
    
    if meshPath:
        bs.performSettings(1)
        try:
            exportMeshes(meshPath)
        except  Exception, detail:
            print 'exception during mesh export:'
            print str(type(detail))
            print str(detail)
        bs.performSettings(0)
            
    if weightsPath:
        try:
            skin.writeWeights([], weightsPath, 0)
        except  Exception, detail:
            print 'exception during weights export:'
            print str(type(detail))
            print str(detail)
        
    UIlistWeights()

def smartLoad():
    """
    Load UI selected:
    Mesh
    Mesh >> Weights
    If nothing in scene selected.
    Else if selection in scene:
    Weights >> sceneMesh
    """
    selection = pm.ls(sl=1)
    mesh = selection
    all=[]
    loadComp = 0
   
    if not selection == []:
        if not selection[0].nodeType() == 'transform':
            loadComp = 1
        
            for l in selection:
                for i, e in enumerate(l):
                    all.append(e)
                    
            x = pm.ls(sl=1, s=1)
            
            shape = pm.listRelatives(all[-1], p=1)[0]
            oldMesh = [shape.getParent()]
            mesh = pm.duplicate(oldMesh, n='TEMP_PASTE_MESH')
            
            dup = []
            for e in all:
                dup.append(mesh[0].getShape().name())
                
        
    
    elif mesh == []:
        mesh = None
    
    elif mesh[0].getShape():
        mesh = mesh[0]
#        if selection[0].getShape().nodeType() == 'mesh':
#            selection = selection[0]
#        else:
#            selection = None
    else:
        mesh = None 
            
    skinPath = getSkinPath()
    meshPath = getMeshPath(skinPath)
    weightsPath = getWeightPath(skinPath) 


    cluster = None
    editCluster = 1
        
    if meshPath and weightsPath:
        skin.skinMesh([], '', weightsPath, meshPath)
    
    elif not meshPath and weightsPath:
        if mesh:
            print editCluster
            skin.applyWeights(mesh, 'smartLoad', weightsPath, 
                              editCluster=editCluster)
        
        else:
            #pass
            raise Exception('please select a mesh in the UI or in the scene.')
        
    elif meshPath and not weightsPath and not cluster:
        if mesh:
            #Get weights from selection and paste on the imported mesh
            pm.importFile(meshPath, renameAll=1, renamingPrefix=('SKIN_meshIMPORT_'))
            meshes = pm.ls('SKIN_meshIMPORT_' + '*')
            print meshes
            meshObj = None
            
            for obj in meshes:
                pm.rename(obj, str(obj).replace('SKIN_meshIMPORT_', 'mesh_'))
                if obj.nodeType() == 'transform':
                    meshObj = obj
                    print obj
            
            skin.pasteWeights([meshObj, mesh[0]])
               
        else:
            pm.importFile(meshPath, renameAll=1, renamingPrefix=('mesh_'))
    
    
    elif not weightsPath and cluster:
        if meshPath:
            print meshPath
            print cluster  
        elif mesh:
            print mesh
            print cluster
        else:
            pass 
    
    if loadComp:
        
        #get joints from old mesh and add to new one
        oldCluster = skin.getSkinfromGeo([oldMesh[0]])
        newCluster = skin.getSkinfromGeo(mesh)
        joints = pm.skinCluster(newCluster, q=1, inf=1)
        
        for e in joints:
            try:
                pm.skinCluster(oldCluster, e=1, ug=1, dr=1.6, ps=0, ns=10, lw=True, wt=0, 
                       ai=e)
                #skinCluster -e -ug -dr 1.6 -ps 0 -ns 10 -lw true -wt 0 -ai joint3 skinCluster1;
            except:
                pass
        
        pm.select(dup)
        pm.select(all, add=1)
        mel.eval('copySkinWeights -noMirror -surfaceAssociation closestPoint -influenceAssociation name -influenceAssociation label -influenceAssociation label;')
        
        pm.delete(mesh)
        
    listCluster()
        

def getSkinPath():
    path = pm.textField('mainPathSkinTF', q=1, text=1)
    UIskin = pm.textScrollList('skinMeshesSL', q=1, selectItem=1)
    if UIskin == []:
        skinPath = None
        raise Exception('No skin folder in UI selected')
    else:
        skinPath = os.path.join(path, UIskin[0])
    return skinPath
            
def getMeshPath(skinPath):
    UImesh = pm.textScrollList('meshesSL', q=1, selectItem=1)
    if UImesh == []:
        meshPath = None
    else:
        meshPath = os.path.join(skinPath, UImesh[0])
    return meshPath

def getWeightPath(skinPath):
    UIweights = pm.textScrollList('skinWeightsSL', q=1, selectItem=1)
    if UIweights == []:
        weightsPath = None
    else:
        weightsPath = os.path.join(skinPath, UIweights[0])
    return weightsPath

def deleteSkinFile():
    skinPath = getSkinPath()
    weightsPath = getWeightPath(skinPath)
    UIweights = pm.textScrollList('skinWeightsSL', q=1, selectItem=1)
    if UIweights == []:
        weightsPath = None
    else:
        weightsPath = os.path.join(skinPath, UIweights[0])
    print weightsPath
    os.remove(weightsPath)  
    UIlistWeights()
    
    
def deleteMeshFile():
    skinPath = getSkinPath()
    meshPath = getMeshPath(skinPath)
    UImesh = pm.textScrollList('meshesSL', q=1, selectItem=1)
    if UImesh == []:
        meshPath = None
    else:
        meshPath = os.path.join(skinPath, UImesh[0])
    print meshPath
    os.remove(meshPath)   
    UIlistWeights()

class sceneWeights():
    def __init__(self, path):
        """
        Read and write all weights of a scene
        path - path to the weights folder
        """
        self.path = path

    def exportWeights(self):
        self.meshes = pm.ls(type='mesh')
        for e in self.meshes:
            print e.getParent()
            if not e.isIntermediate():
                weightsPath = os.path.join(self.path, e.name().replace('|', '+').replace(':', ';')+'.txt')
                try:
                    skin.writeWeights([e.getParent()], weightsPath, 0)
                except skin.noClusterError:
                    pass


    def importWeights(self):
        dirs = os.listdir(self.path)
        for e in dirs:
            wightPath = os.path.join(self.path, e)
            meshName = e.replace('+', '|').replace(';', ':')[:-4]
            if pm.objExists(meshName):
                mesh = pm.ls(meshName)[0]

                try:
                    skin.applyWeights(Vlist=[mesh.getParent()], Vpath=wightPath)
                except:
                    try:
                        skin.applyWeights(Vlist=[mesh.getParent()], Vpath=wightPath, editCluster=1)
                    except:
                        print '#### Could not set Weight', e
                        pass
                #skin.readWeights([mesh.getParent()], wightPath)


        










