"""
general tools for organizing hierarchies
contains an UI: nullWin() 
"""

from __future__ import with_statement
import os
import pymel.core as pm
import lsSl
import snap
import mf.tools.file as ft
import mf.builder.nodes as bn

class packageRig():
    def __init__(self, Vlist=[], Vsuffix='', edit=1, **kwdArgs):
        """To organize package rig components"""        
        if edit: self.edit(Vlist=Vlist) 
        else: 
            raise bn.customNodeError('this class is for editing only')  
    
    def edit(self, Vlist=[]):
        """finds all objects"""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.group = Vlist[0]

class main():
    def __init__(self, Vlist=[], Vsuffix='', edit=0, add=0, **kwdArgs):
                
        if edit: self.edit(Vlist=Vlist)   
        else: self.create(Vlist=Vlist, add=add, **kwdArgs)
            
    def create(self, Vlist=[], add=0, **kwdArgs):
        """Creates the objects or adds setup to Vlist."""
        print add
        print Vlist
        if add:
            self.group = Vlist[0] 
        else:
            self.group = pm.group(w=1, em=1, **kwdArgs) 
        self.group.addAttr(bn.register('transform_main')[0])
        self.group.addAttr(bn.register('transform_ctrl')[0])
        
        if len(Vlist) >= 2:
            if add:
                self.groupB = Vlist[1] 
            else:
                self.groupB = pm.group(w=1, em=1, **kwdArgs)
            self.groupB.rename(self.group.name()+ 'B')
            self.group|self.groupB
            self.group.addAttr('gimbal_ctrl', defaultValue=0, min=0, max=1, keyable=1, at='long')
            self.group.gimbal_ctrl >> self.groupB.getShape().v
            self.groupB.addAttr(bn.register('transform_ctrl')[0])
            
            for e in [self.groupB.s]:
                e.setLocked(1)
                e.setKeyable(0)
                e.showInChannelBox(0)   
            
    def edit(self, Vlist=[]):
        """finds all objects"""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.group = Vlist[0]
        if self.group.hasAttr('gimbal_ctrl'):
            self.groupB = self.group.gimbal_ctrl.connections()[0]
        else:
            self.groupB = Vlist[0]   
    
    def setGimball(self, Vlist=[]):
        self.gimball= Vlist[0]
        
    def getGimball(self, Vlist=[]):
        self.group.getParent()

class meshGrp():
    def __init__(self, Vlist=[], Vsuffix='', edit=0, **kwdArgs):
                
        if edit: self.edit(Vlist=Vlist)   
        else: self.create(**kwdArgs)
            
    def create(self, Vlist=[], **kwdArgs):
        """creates the objects"""
        self.group = pm.group(**kwdArgs) 
        self.group.addAttr(bn.register('transform_mesh_grp')[0])
    
    def edit(self, Vlist=[]):
        """finds all objects"""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.group = Vlist[0]

class character():
    """
    Main group for an asset.
    """
    def __init__(self, Vlist=[], Vsuffix='', edit=0, assetName='', add=0, 
                 **kwdArgs):
        self.assetName = assetName        
        if edit: self.edit(Vlist=Vlist)   
        else: self.create(**kwdArgs)
            
    def create(self, Vlist=[], add=0, **kwdArgs):
        """creates the objects"""
        Vlist = lsSl.lsSl(Vlist)
        if add:
            self.group = Vlist[0]
        else:
            self.group = pm.group(n=self.assetName, w=1, em=1, **kwdArgs) 
        self.group.addAttr(bn.register('transform_character')[0])
        self.group.addAttr('controllers_vis', defaultValue=1, min=0, max=1,
                             keyable=1, at='long')
        self.group.addAttr('meshes_vis', defaultValue=1, min=0, max=1,
                           keyable=1, at='long')
        self.group.addAttr("meshes_override", at="enum", dv=2, 
                           en="normal:template:reference", keyable=1)
        self.group.addAttr('proxy_vis', defaultValue=1, min=0, max=1,
                            keyable=1, at='long', dv=0)
        self.group.addAttr("proxy_override", at="enum", dv=2, 
                           en="normal:template:reference", keyable=1)
        for e in [self.group.t, self.group.r, self.group.s]:
            e.setLocked(1)
            e.setKeyable(0)
            e.showInChannelBox(0)    
            
    def edit(self, Vlist=[]):
        """finds all objects"""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.group = Vlist[0]

class mainTransGrp():
    def __init__(self, Vlist=[], Vsuffix='', edit=0, **kwdArgs):
                
        if edit: self.edit(Vlist=Vlist)   
        else: self.create(**kwdArgs)
            
    def create(self, Vlist=[], **kwdArgs):
        """creates the objects"""
        self.group = pm.group(**kwdArgs) 
        self.group.addAttr(bn.register('transform_main_trans_grp')[0])
    
    def edit(self, Vlist=[]):
        """finds all objects"""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.group = Vlist[0]
        
class noTransGrp():
    def __init__(self, Vlist=[], Vsuffix='', edit=0, **kwdArgs):
                
        if edit: self.edit(Vlist=Vlist)   
        else: self.create(**kwdArgs)
            
    def create(self, Vlist=[], **kwdArgs):
        """creates the objects"""
        self.group = pm.group(**kwdArgs) 
        self.group.addAttr(bn.register('transform_no_trans_grp')[0])
    
    def edit(self, Vlist=[]):
        """finds all objects"""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.group = Vlist[0]
        
        

class noTrans():
    def __init__(self, Vlist=[], Vsuffix='', edit=0, **kwdArgs):
        
        if edit: self.edit(Vlist=Vlist)   
        else: self.create(**kwdArgs)
            
    def create(self, Vlist=[], **kwdArgs):
        """creates the objects"""
        self.group = pm.group(**kwdArgs) 
        self.group.addAttr(bn.register('transform_no_trans')[0])
    
    def edit(self, Vlist=[]):
        """finds all objects"""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.group = Vlist[0]
        
    def lock(self, val):
        """locks and unlocks"""
        self.group.t.setLocked(val)
        self.group.r.setLocked(val)
        self.group.s.setLocked(val)
        
    def content(self):
        """returns direct children"""
        return self.group.getChildren(type='transform')
    
    
class mainTrans():
    def __init__(self, Vlist=[], Vsuffix='', edit=0, **kwdArgs):
                
        if edit: self.edit(Vlist=Vlist)   
        else: self.create(**kwdArgs)
            
    def create(self, Vlist=[], **kwdArgs):
        """creates the objects"""
        self.group = pm.group(**kwdArgs) 
        self.group.addAttr(bn.register('transform_main_trans')[0])
    
    def edit(self, Vlist=[]):
        """finds all objects"""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.group = Vlist[0]
        
    
class trans():
    def __init__(self, Vlist=[], Vsuffix='', edit=0, **kwdArgs):
                
        if edit: self.edit(Vlist=Vlist)   
        else: self.create(**kwdArgs)
            
    def create(self, Vlist=[], **kwdArgs):
        """creates the objects"""
        self.group = pm.group(**kwdArgs) 
        self.group.addAttr(bn.register('transform_trans')[0])
    
    def edit(self, Vlist=[]):
        """finds all objects"""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.group = Vlist[0]
        
class drv():
    def __init__(self, Vlist=[], Vsuffix='', edit=0, **kwdArgs):
                
        if edit: self.edit(Vlist=Vlist)   
        else: self.create(**kwdArgs)
            
    def create(self, Vlist=[], **kwdArgs):
        """creates the objects"""
        self.group = pm.group(**kwdArgs) 
        self.group.addAttr(bn.register('transform_drv')[0])
    
    def edit(self, Vlist=[]):
        """finds all objects"""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.group = Vlist[0]
        
def parentShapeFreez(Vlist=[]):
    """
    Parents the shape of the first object to the second.
    """
    Vlist = lsSl.lsSl(Vlist, 2, 2)
    dub = pm.duplicate(Vlist[0], po=0, n='Ctl')[0]
    for e in [dub.t, dub.r, dub.s,
              dub.tx,
              dub.ty,
              dub.tz,
                
              dub.rx,
              dub.ry,
              dub.rz,
                
              dub.sx,
              dub.sy,
              dub.sz]:
        e.setLocked(0)
    Vlist[1]|dub
    #snap.snap([dub, Vlist[1]])
    for e in pm.listRelatives(dub, children=1, shapes=0):
        if not e in pm.listRelatives(dub, shapes=1):
            pm.delete(e)
    pm.makeIdentity(dub, apply=1, t=1, r=1, s=1, n=0, jo=1)
    
    
    
    shape = dub.getShapes()[0]
    pm.parent(shape, Vlist[1], shape=1, r=1)
    pm.delete(dub)
    return shape 

def parentShape(Vlist=[]):
    """
    Parents the shape of the first object to the second.
    """
    Vlist = lsSl.lsSl(Vlist, 2, 2)
    
    shape = Vlist[0].getShapes()[0]
    pm.parent(shape, Vlist[1], shape=1, r=1)
    return shape 

def rotAxis(Vlist=[]):
    """
    setup to control the rotation axis
    one group over and one under the selected object rotate counter wise
    """
    
# TODO: make rot and trans optional 

    Vlist = lsSl.lsSl(Vlist, 1, 0)
    
    for i in range(len(Vlist)):
        Vnull = pm.group(w=1, empty=1, n='null_%s' % (Vlist[i]))
        Vrot = pm.group(w=1, empty=1, n='rot_%s' % (Vlist[i]))
        Vunrot = pm.group(w=1, empty=1, n='unrot_%s' % (Vlist[i]))
        
        VrotInvert = pm.createNode('multiplyDivide', name='rotInv%s' % (Vlist[i]))
        VtransInvert = pm.createNode('multiplyDivide', name='transInv%s' % (Vlist[i]))
                
        snap.snap([Vnull, Vrot, Vunrot, Vlist[i]], 1)
                        
        Vnull|Vrot|Vlist[i]|Vunrot
        
        Vlist[i].addAttr( "axisX", defaultValue=0, keyable=1)
        Vlist[i].addAttr( "axisY", defaultValue=0, keyable=1)
        Vlist[i].addAttr( "axisZ", defaultValue=0, keyable=1)
        
        Vlist[i].addAttr( "pivX", defaultValue=0, keyable=1)
        Vlist[i].addAttr( "pivY", defaultValue=0, keyable=1)
        Vlist[i].addAttr( "pivZ", defaultValue=0, keyable=1)
        
        Vlist[i].axisX >> VrotInvert.input1X
        Vlist[i].axisY >> VrotInvert.input1Y
        Vlist[i].axisZ >> VrotInvert.input1Z
        
        Vlist[i].pivX >> VtransInvert.input1X
        Vlist[i].pivY >> VtransInvert.input1Y
        Vlist[i].pivZ >> VtransInvert.input1Z
        
        VrotInvert.input2X.set(-1) 
        VrotInvert.input2Y.set(-1) 
        VrotInvert.input2Z.set(-1) 
        
        VtransInvert.input2X.set(-1) 
        VtransInvert.input2Y.set(-1) 
        VtransInvert.input2Z.set(-1) 
        
        VrotInvert.outputX >> Vunrot.rx  
        VrotInvert.outputY >> Vunrot.ry
        VrotInvert.outputZ >> Vunrot.rz
        
        VtransInvert.outputX >> Vunrot.tx  
        VtransInvert.outputY >> Vunrot.ty
        VtransInvert.outputZ >> Vunrot.tz

        Vlist[i].axisX >> Vrot.rx  
        Vlist[i].axisY >> Vrot.ry
        Vlist[i].axisZ >> Vrot.rz
        
        Vlist[i].pivX >> Vrot.tx  
        Vlist[i].pivY >> Vrot.ty
        Vlist[i].pivZ >> Vrot.tz


def nullGroup(Vlist=[], Vhierarchy=1, Vtype=1, Vname='', Vprefix='', Vsuffix='', 
              Vnum=1, jointRadius=1.1, pivot=1, rot=1, connectInverseScale=0,
              freez=1):
    """
    creates:
    an object with the same location and rotation as the source object.
    The new object can be inserted into the hierarchy of source object.
   
    returns: 
    the new zero-objects
      
    =========== ==============================================================
    Arguments   Description
    =========== ==============================================================
    Vlist[]     | list of objects    
    ----------- --------------------------------------------------------------            
    Vhierarchy  | 1 = no parenting
                | 2 = null|obj
                | 3 = parent|null|obj
                | 4 = obj|null
                | 5 = obj|null|children    
    ----------- --------------------------------------------------------------            
    Vtype       | 1 = group
                | 2 = joint
                | 3 = locator
    ----------- --------------------------------------------------------------            
    Vnum        | number of copies of each new object 
                | (works only with hierarchy 1 and 4) 
    ----------- --------------------------------------------------------------            
    pivot       | 0 = world origin
                | 1 = parent 
    ----------- --------------------------------------------------------------            
    rot         | 0 = keep
                | 1 = match
    =========== ==============================================================
    """
    
    Vlist = lsSl.lsSl(Vlist, 1, 0)
     
    Vresult = []
    
    if Vhierarchy != 1 and Vhierarchy != 4:
        Vnum = 1
    
    for i in range(len(Vlist)):
        
        for j in range(Vnum):
                
            if Vnum == 1:
                numExt = ''
            else: 
                numExt = '_%s' % (j+1)
            
            if Vname == '':
                VbaseName = Vlist[i]
            else:
                VbaseName = Vname
            
            VnewName = '%s%s%s%s' % (Vprefix, VbaseName, Vsuffix, numExt)
                
            if Vtype == 1:
                Vnull = pm.group(w=1, empty=1, n=VnewName)
                Vresult.append(Vnull)
                
            elif Vtype == 2:
                pm.select(cl=1)     
                Vnull = pm.joint(n=VnewName)
                
                if jointRadius != 0:
                    try: 
                        Vradius = (Vlist[i].radius.get()*jointRadius)
                        Vnull.radius.set(Vradius)
                    except:
                        pass
                
                pm.select(cl=1)
                Vresult.append(Vnull)
                
            elif Vtype == 3:
                Vnull = pm.spaceLocator(n=VnewName)
                Vresult.append(Vnull)
            
            if pivot:    
                snap.snapTrans([Vnull, Vlist[i]], 1)
                
            if rot:
                snap.snapRot([Vnull, Vlist[i]], 1)
            
            if Vtype == 2:
                
                pm.makeIdentity(Vnull, apply=1, t=1, r=1, s=1, n=0, jo=0)
                if not freez:
                    try:
                        Vlist[i].jointOrient.set(Vnull.jointOrient.get())
                        Vnull.jointOrient.set(0,0,0)
                    except:
                        pass
            if Vhierarchy == 2:
                Vnull|Vlist[i]
                
            elif Vhierarchy == 3:
                Vparent = pm.listRelatives(Vlist[i], parent=1)
                if Vparent != []:
                    Vparent[0]|Vnull|Vlist[i]
                    
                    if Vtype == 2 and Vlist[i].nodeType() == 'joint' \
                                  and connectInverseScale:
                        Vnull.s >> Vlist[i].inverseScale
                else: 
                    Vnull|Vlist[i]
                        
            if Vhierarchy == 4:
                Vlist[i]|Vnull
                
            if Vhierarchy == 5:
                Vchildren = pm.listRelatives(Vlist[i], children=1)
                Vshapes = pm.listRelatives(Vlist[i], shapes=1)
                
                for j in range(len(Vshapes)):
                    Vchildren.remove(Vshapes[j])
                
                Vlist[i]|Vnull
                for j in range(len(Vchildren)):
                    
                    pm.parent(Vchildren[j], Vnull)
                    
                    if Vtype == 2 and Vchildren[j].nodeType() == 'joint' \
                                  and connectInverseScale:
                        Vnull.s >> Vchildren[j].inverseScale
                    
            
    pm.select (Vresult)

    return Vresult               
        
def unParent(Vlist=[]):
    """
    unparents a list and returns a list of the parents
  
    returns: list of successful unparented objects
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    Vresult = []
        
    for i in range(len(Vlist)):
        pm.select (cl=1)
        if Vlist[i].getParent() != None:    
            pm.parent (Vlist[i], w=1, absolute=1)
            Vresult.append(Vlist[i])
        
        
    pm.select (Vlist)        
    return Vresult
    
def getParent(Vlist=[]):
    """
    gets the parents from a list of objects
  
    returns: dictionary with child/parent pairs
    
    ========= ======================
    Arguments Description
    ========= ======================
    Vlist     list of objects
    ========= ======================
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    Vresult = dict([])
    
    for i in range(len(Vlist)):
        Vresult[Vlist[i]] = Vlist[i].getParent()
        
    pm.select (Vlist)
    return Vresult
    
def reParent(Vdict=dict([])): 
    """
    given a dictionary parents the values to the items
  
    returns: dictionary of successful parented child/parent pairs
    
    ========= ===================================
    Arguments Description
    ========= ===================================
    Vdict     dictionary with child/parent pairs
    ========= ===================================
    """
    Vselection = pm.ls(sl=1) 
    Vresult = dict([])
    
    for c, p in Vdict.iteritems():
        
        if c: 
            pm.select (cl=1)
            
            try:
                par =  c.getParent()
            except:
                par = None
                
            if not p and par:
                pm.parent(c, w=1)
                Vresult[c] = p
                        
            elif p:
                if not par == p:
                    pm.parent(c, p)
                    Vresult[c] = p 
            else:
                pass    

    pm.select (Vselection)
    return Vresult
        
def nullWin():

    try:
        pm.deleteUI('nullWin')
    except: pass
    
    with pm.window('nullWin', resizeToFitChildren=1, toolbox=1, s=0) as win:
        
        with pm.columnLayout():
            pm.text(' prefix, name, suffix')
            pm.textField('nullWinPrefix', text='null_', w=114)
            pm.textField('nullWinName', text='', w=114)
            pm.textField('nullWinSuffix', text='', w=114)
            pm.checkBox ('nullWinPivot', v=1, al='left', l='match pivot')
            pm.checkBox ('nullWinFreez', v=1, al='left', l='freez')
            pm.separator(w=114, h=5)
            pm.radioButtonGrp('parWinHierarchy', numberOfRadioButtons=4, 
                              vertical=1, select=3, 
                              labelArray4=['world', 'new \ obj', 'par \ new \ obj', 'obj \ new'], 
                              h=78, w=104);

            pm.radioButtonGrp('parWinHierarchyB', numberOfRadioButtons=1, 
                              vertical=1, shareCollection='parWinHierarchy', 
                              label1='obj \ new \ children', 
                              h=20, w=114);
            pm.separator(w=114, h=5)

            pm.radioButtonGrp('parWinType', numberOfRadioButtons=3, vertical=1, 
                              select=1, 
                              labelArray3=['group', 'joint', 'locator'], 
                              h=57, w=114);
            pm.button( l='create', w=114, c= 'import mf.tools.parent; mf.tools.parent.nullGrpCB()')
                      
    win.show()
    
def nullGrpCB():
    
    VprefixVal = pm.textField('nullWinPrefix', q=1, text=1)
    VnameVal = pm.textField('nullWinName', q=1, text=1)
    VsuffixVal = pm.textField('nullWinSuffix', q=1, text=1)       
    VhierarchyVal = pm.radioButtonGrp('parWinHierarchy', q=1, select=1)
    VtypeVal = pm.radioButtonGrp('parWinType', q=1, select=1)
    VpivVal = pm.checkBox('nullWinPivot', q=1, v=1)
    freez = pm.checkBox('nullWinFreez', q=1, v=1)
     
    if VhierarchyVal == 0:
        VhierarchyVal = 5
    #print VhierarchyVal          
    nullGroup([], Vhierarchy=VhierarchyVal , Vtype=VtypeVal, 
              Vprefix=VprefixVal, Vname=VnameVal, Vsuffix=VsuffixVal, 
              pivot=VpivVal, freez=freez)
        
def writeParents(Vlist=[], 
                 Vpath=r'P:\\GC\\06_RIGGING_SHADING\\01_CHARACTER\\05_GRUFFALO\\scenes\\mesh\\addOn\\parents.txt', 
                 force=0):
    """
    Exports a [child, parent] list to a file. 
    """ 
    
    fullPath = os.path.abspath(Vpath)
    path = os.path.split(fullPath)[0]
    
    
    ft.checkPath(path)
    if force != 1:
        ft.fileExists(fullPath)
        
    f = open (fullPath, 'w')
    f.writelines ('# The first line is reserved for comments.')
    
    parents = getParent(Vlist)
    
    parentList = []
    for c, p in parents.iteritems():
        parentList.append([c, p])
    parentList.sort()      
    for o in parentList:    
        if o[1]:
            f.writelines (["\n['" + o[0] + "', '" + o[1] + "']"])
        else:
            f.writelines (["\n['" + o[0] + "']"])
            
        
def readParents(Vlist=[], 
                 Vpath=r'P:\\GC\\06_RIGGING_SHADING\\01_CHARACTER\\05_GRUFFALO\\scenes\\mesh\\addOn\\parents.txt'):
    
    fullPath = os.path.abspath(Vpath)
    path = os.path.split(fullPath)[0]
    
    ft.checkPath(path)
        
    f = open (fullPath, 'r')
    
    lines = f.readlines()
    comment = lines[0].split('\n')[0]
    Vdict = dict([])
    Vresult = []

    for line in lines[1:]:
        par = eval(line.split('\n')[0])

        try:
            c = pm.ls(par[0])[0]
            Vresult.append(c)
        except:
            c = None
        try:    
            p = pm.ls(par[1])[0]
        except:
            p = None
        
        Vdict[c] = p
            
    reParent(Vdict)
    return Vresult 

def copyShape(Vlist=[], Vsuffix='', size=0.9):
    """
    Creates a copy under the selected controller.
    Works only with objects that have a shape.
    """
    Vlist = lsSl.lsSl(Vlist, 2, 0)
    
    shape = Vlist[0].getShape()  
    tempTrans = nullGroup([Vlist[0]], Vprefix='null_')[0]
    
    pm.parent(shape, tempTrans, shape=1, r=1)
    newTrans = pm.duplicate(tempTrans)[0]
    
    pm.makeIdentity(newTrans, s=1, t=0 ,r=0, n=0, a=1)
    newTrans.s.set([size, size, size])
    pm.makeIdentity(newTrans, s=1, t=0 ,r=0, n=0, a=1)    
    
    newShape = newTrans.getShape()
    
    pm.parent(shape, Vlist[0], shape=1, r=1)
    pm.parent(newShape, Vlist[1], shape=1, r=1)
    
    pm.delete(newTrans)
    pm.delete(tempTrans)
        
    pm.select(Vlist)

def gimbalCtrl(Vlist=[], Vsuffix='', under=1, lockTranslate=1, 
               attrName='gimbal_ctrl'):
    """
    Creates a copy under the selected controller.
    Works only with objects that have a shape.
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    for o in Vlist:
        shape = o.getShape()
        
        if under:
            newCtrl = nullGroup([o], Vhierarchy=5, Vprefix='extra_')[0]
        else:
            world = nullGroup([o.getParent()], Vhierarchy=1, pivot=1, Vprefix='world_')[0]
            newCtrl = nullGroup([o], Vhierarchy=3, Vprefix='extra_')[0]
            newCtrl|world|o
            
        tempTrans = nullGroup([o], Vprefix='null_')[0]
        
        pm.parent(shape, tempTrans, shape=1, r=1)
        newTrans = pm.duplicate(tempTrans)[0]
        
        pm.makeIdentity(newTrans, s=1, t=0 ,r=0, n=0, a=1)
        newTrans.s.set([0.9, 0.9, 0.9])
        pm.makeIdentity(newTrans, s=1, t=0 ,r=0, n=0, a=1)    
        
        newShape = newTrans.getShape()
        
        pm.parent(shape, o, shape=1, r=1)
        pm.parent(newShape, newCtrl, shape=1, r=1)
        
        pm.delete(newTrans)
        pm.delete(tempTrans)
        
        o.addAttr(attrName, defaultValue=0, min=0, max=1, keyable=1, at='long')
        getattr(o,attrName) >> newShape.v
        
        attrList = [newCtrl.sx, newCtrl.sy, newCtrl.sz]
                    
        if lockTranslate:
            attrList += [newCtrl.tx, newCtrl.ty, newCtrl.tz, newCtrl.v]
            
        for attr in attrList:
            attr.set (lock=1, channelBox=0, keyable=0)
        
    pm.select(Vlist)
    
def pivotCtrl(Vlist=[], Vsuffix=''):
    """
    Creates a pivot ctl.
    """
    Vlist = lsSl.lsSl(Vlist, 2, 2)
    Vlist[0].t >> Vlist[1].scalePivot
    Vlist[0].t >> Vlist[1].rotatePivot


def findCommonParents(Vlist=[]):
    """
    Finds common parents of all selected objects.
    nodes = ['a|b',
             'a|b|c|d',
             'X|b|X|d',
             'a|b|c|d|e|f',
             'b|b|c',
             'b|b|c|d']
    # result = ['a|b', 'b|b|c']
    """
    Vlist = lsSl.lsSl(Vlist, 2, 0)
    branches = []
    for e in Vlist:
        branches.append(e.longName())
    branches.sort()
    result = []
    duplicates = []
    for e in branches:
        removed = []
        for x in branches:
            if x.startswith(e) and not x == e:
                removed.append(x)

        duplicates += removed

    for e in branches:
        if not e in duplicates:
            result.append(e)

    return pm.ls(result)
    
    
    
    
        