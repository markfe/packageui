"""
general tools for constraints
contains an UI: consWin()
"""
from __future__ import with_statement
import pymel.core as pm
import mf.builder.nodes as bn
import lsSl
import parent as tp
import snap as snp
import proxyGeo as pg
import general as gt
import mf.tools.key as tk


class spaceSwitch():
    """
    Creates: 
        a space-switch for each object in the list 
    
    ============= =============================================================
    Arguments     Description   
    ============= =============================================================
    Vlist[]       list of objects for the constraints, last is the object to be 
                  constraint            
    VattrName[]   list of attribute names. if empty the object names 
                  will be used       
    Vtype         1=point, 2=orient, 3=parent   
    VlastObj      1=the object before the last will get the constraint and the 
                  last object the attribute    
    Voffset       constraint offset 
    headlineAttr  if true a locked attribute with the name 'spaces' will be
                  created to visually better organize the channel box
    edit          For editing select the constrained source object.
    ============= =============================================================
    """
    
    def __init__(self, Vlist=[], edit=0, VattrName=[], Vtype=3, 
                 Vsuffix='', VlastObj=0, Voffset=1, headlineAttr=0,
                 skipTranslate=['none','none','none'], **kwdArgs):
        
        if edit: 
            self.edit(Vlist=Vlist)   
        else: 
            self.create(Vlist=Vlist, VattrName=VattrName, Vtype=Vtype, 
                 Vsuffix=Vsuffix, VlastObj=VlastObj, Voffset=Voffset, 
                 headlineAttr=headlineAttr, **kwdArgs)
            
    def create(self, Vlist=[], VattrName=[], Vtype=3, 
                 Vsuffix='', VlastObj=0, Voffset=1, headlineAttr=0,
                 skipTranslate=['none','none','none'], **kwdArgs):
        """creates the setup"""           
        Vlist = lsSl.lsSl(Vlist, 2, 0)
        
        lastObIndex = -1-(VlastObj)
        source = Vlist[lastObIndex]
        Vresult = []
        Vcons = []
        Vweights = []
        
        bn.set([source], 'transform_space_switch')
        
        if headlineAttr:
            try:
                Vlist[-1].addAttr('spaces', defaultValue=0, min=0, keyable=1)
                Vlist[-1].spaces.setLocked(1)
            except: pass
        
        Vcons = getCons(source)
     
        if Vcons != []: 
            Vweights = filterConsWeights(Vcons, Vtype)
        
        for i in range(len(Vlist)+(lastObIndex)):
    
            # If the VattrName string is empty take objects name for attribute        
            if len(VattrName) == 0:
                VattrName.append('%s' % (Vlist[i]))
                
            Vcons = cons (Vlist=[Vlist[i], source], Vtype=Vtype, 
                          Voffset=Voffset, permanent=1, Vsuffix=Vsuffix,
                          skipTranslate=skipTranslate)
            Vresult.append(Vcons)
            
            Vlist[-1].addAttr( VattrName[0], defaultValue=0, 
                               min=0 ,max=1, keyable=1)          
            VattrA = pm.ls('%s.%s' % (Vlist[-1], VattrName[0]))
            
            # pop the first name from the name list        
            if len(VattrName) != 0:
                VattrName.pop(0)
            
            # find the latest constraint weight and connect to new attribute        
            Vweights = filterConsWeights(Vcons, Vtype)        
            VattrA[0] >> Vweights[-1]
            
        #use the edit function to get all nodes
        self.edit([source])
        
    def edit(self, Vlist=[]):
        """finds all objects"""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        
        self.source = Vlist[0]
        
        con = getCons([self.source])
        if con == []:
            raise Exception('No constraints found on selected object.')
        
        self.cons = con[0]
        
        Ntype = self.cons.nodeType()
        
        if Ntype == 'pointConstraint':
            self.goals = pm.pointConstraint(self.cons, q=1, targetList=1)
            self.weights = pm.pointConstraint(self.cons, q=1, weightAliasList=1)
            self.consType = 1 
        
        elif Ntype == 'orientConstraint':
            self.goals = pm.orientConstraint(self.cons, q=1, targetList=1)
            self.weights = pm.orientConstraint(self.cons, q=1, weightAliasList=1)
            self.consType = 2
            
        elif Ntype == 'parentConstraint':
            self.goals = pm.parentConstraint(self.cons, q=1, targetList=1)
            self.weights = pm.parentConstraint(self.cons, q=1, weightAliasList=1)
            self.consType = 3
        self.attrObj = None 
           
        for w in self.weights:
            attrObj = w.inputs()
    
            if attrObj == []:
                pass
            else:
                self.attrObj = attrObj[0]
                break        
        if w == []:
            raise Exception('No connections to the constraint weights found.')
        
    def add(self, Vlist=[], attrName='to_obj', offset=1):
        """Adds a goal object and an attribute to the existing setup."""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        
        if self.attrObj.hasAttr('spaces'):
            ro = gt.reorderAttr([self.attrObj]) 
            attrList = ro.attrList
            i = attrList.index('spaces')
            attrList.insert(i+1, attrName)
          
        self.create(Vlist=[Vlist[0], self.source, self.attrObj], 
                    VattrName=[attrName], Vtype=self.consType, 
                    Vsuffix='', VlastObj=1, 
                    Voffset=offset, headlineAttr=0)
        
        if self.attrObj.hasAttr('spaces'):
            ro.reorder(attrList)
    
    def getAttr(self, namesOnly=0):
        attr =[]
        attrNames = []
        for w in self.weights:
            plug = w.inputs(plugs=1)
            if not plug == []:
                attr.append(plug[0])
        if namesOnly:
            for a in attr:
                attrNames.append(str(a).split('.')[-1])
            return attrNames
        return attr
    
    def headline(self, name='spaces'):
        if not self.attrObj:
                print 'No attribute object found!'
                return
                
        if not self.attrObj.hasAttr(name):
            ro = gt.reorderAttr([self.attrObj]) 
            attrList = ro.attrList
            ownAttr = self.getAttr(1)
            
            for a in ownAttr:
                attrList.remove(a)
            
            self.attrObj.addAttr(name, defaultValue=0, min=0, keyable=1)
            headline = pm.ls(str(self.attrObj) + '.' + name)[0]
            headline.setLocked(1)
            
            ro.reorder(attrList + [name] + ownAttr)


class inbRot():
    """
    interpolation for the limb end. one object in-between,
    one under this group to interpolate x rotation 
    
    ========= ========================
    Arguments Description
    ========= ========================
    Vlist[0]  fix
    Vlist[1]  mobil
    Vlist[2]  rot joint(optional)
    ========= ========================    
    """
    def __init__(self, Vlist=[], Vproxy=0, proxyScale=1, Vsuffix='', 
                 side=3, label='', autoShift=0, shiftDirection=1):
    
        Vlist = lsSl.lsSl(Vlist, Vmin=2, Vmax=3)
        
        self.rotObj = tp.nullGroup(Vlist=[Vlist[1]], Vhierarchy=4, 
                                   Vtype=2, Vsuffix='_inb')
        self.nullRotObj = tp.nullGroup(Vlist=[self.rotObj[0]], Vhierarchy=3, 
                                       Vtype=2, Vprefix='null_')[0]
     
        # parent constraint on translate to prevent flipping
        cons(Vlist=[Vlist[1], self.rotObj[0]], Vtype=1, Voffset=1, weight=1, 
             skipRotate=['x','y','z'])   
        cons(Vlist=[Vlist[0], Vlist[1], self.rotObj[0]], Vtype=2, 
             skipTranslate=['x','y','z']) 
        proxPar = self.rotObj[0]
        
        # interpolate x rotation ----------------------------------------------
        if len(Vlist) == 3:
            self.xRotObj = tp.nullGroup(Vlist=[self.rotObj[0]], Vhierarchy=4, 
                                        Vtype=2, Vsuffix='_xrot')
            cons(Vlist=[Vlist[2], self.xRotObj[0]], Vtype=2, Voffset=0, 
                 Vsuffix=Vsuffix, skipRotate=['none','y','z'])
            cons(Vlist=[Vlist[1], self.xRotObj[0]], Vtype=2, Voffset=0,  
                 Vsuffix=Vsuffix, skipRotate=['none','y','z'])
            proxPar = self.xRotObj[0]
            
        
        if autoShift:
            self.rotObj[0].addAttr('junction_volume', defaultValue=0, 
                                    min=0, max=1, keyable=1)
            self.shift = tp.nullGroup(Vlist=[self.rotObj[0]], Vhierarchy=4, 
                                        Vtype=2, Vsuffix='_shift')[0]
            
            blendScaleMid = pm.createNode('blendColors', 
                                          name='blendScaleMid' + Vsuffix)
            
            blendScaleMid.output >>  self.shift.t
            self.rotObj[0].junction_volume >> blendScaleMid.blender
            self.rotObj[0].junction_volume.set(1)
            blendScaleMid.color2.set (0,0,0)
            blendScaleMid.color1.set (0,0,0)
            
            tk.drvKeySet([[0,0], 
                          [-40, 0.15*shiftDirection*autoShift], 
                          [40, -0.15*shiftDirection*autoShift]], 
                        self.rotObj[0].rz, blendScaleMid.color1G)
            
            tk.drvKeySet([[0,0], 
                          [-40, -0.15*shiftDirection*autoShift], 
                          [40, 0.15*shiftDirection*autoShift]], 
                        self.rotObj[0].ry, blendScaleMid.color1B)
            
            proxPar = self.shift
        
        if Vproxy:
            px = pg.proxyGeo(Vlist=[proxPar], VcenterPivot=1, chain=0, 
                             shader=1, Vcolor=[0.6, 0.3, 0.3], xScale=0.1, 
                             shaderName='shader', yzScaleGlobal=proxyScale,
                             side=side, label=label)

        
def cons(Vlist=[], Vtype=3, Voffset=0, weight=1.0, interpType=2, permanent=1, 
         Vname='', Vsuffix='',
        skipTranslate=['none','none','none'], skipRotate=['none','none','none'], 
        skipScale=[0, 0, 0], skipConsScale=['none','none','none']):
    """
    Creates: 
        a constraint or scale blender
    
    Returns:
        the constraint or blend node
    
    ============== ===============================================
    Arguments      Description
    ============== ===============================================
    Vlist[0]       source object
    Vlist[1]       goal object                
    Vtype          1=point, 2=orient, 3=parent, 4=scale(blend nodes), 
                   5=scale, 6=orient(blend nodes)
    Voffset        constraint offset 
    permanent      if 0 constraints will be deleted  
    weight         constraint weight
    interpType     interpolation type 1=average, 2=shortest
    Vname          constraint name if '' it will be 'cons_'+Vlist[1]    
    Vsuffix        suffix
    skipTranslate  translate axes to be skipped
    skipRotate     rotate axes to be skipped
    skipScale      scale axes to be skipped  
    ============== ===============================================
    
    .. note::
        Vtype 5, the scale constraint, does not behave in a linear, 
        predictable way. this option is only for testing. 
        For scaling, Vtype 4 should be used.
    """ 

    Vlist = lsSl.lsSl(Vlist, 2, 0)
    
    if Vname == '':
        Vname = 'cons_%s' % (Vlist[-1])
    VnameSuffix = '%s%s' % (Vname, Vsuffix)
    
    if Vtype ==1:
        Vresult = pm.ls(pm.pointConstraint (Vlist, w=weight, mo=Voffset, 
                                            sk=skipTranslate, n=VnameSuffix))
    
    elif Vtype ==2:
        Vresult = pm.ls(pm.orientConstraint (Vlist, w=weight, mo=Voffset, 
                                             sk=skipRotate, n=VnameSuffix))
        Vresult[0].interpType.set (interpType)
        
    elif Vtype == 3:
        Vresult = pm.ls(pm.parentConstraint(Vlist, w=weight, mo=Voffset, 
                                            st=skipTranslate, sr=skipRotate, 
                                            n=VnameSuffix))
        Vresult[0].interpType.set (interpType)
        
    elif Vtype == 4:
        Vblend = pm.createNode('blendColors', name='blend_%s' % (VnameSuffix))
        Vblend.blender.set (weight)
        
        # this has to be set, so that when the blender will be deleted inside
        # of the same script (permanent=0), the scaling will not be wrong.
        
        Vblend.color1R.set (Vlist[0].sx.get())
        Vblend.color1G.set (Vlist[0].sy.get())
        Vblend.color1B.set (Vlist[0].sz.get())
        Vblend.color2R.set (Vlist[0].sx.get())
        Vblend.color2G.set (Vlist[0].sy.get())
        Vblend.color2B.set (Vlist[0].sz.get())
        
        if skipScale[0] ==0:
            Vlist[0].sx >> Vblend.color1R
        if skipScale[1] ==0:
            Vlist[0].sy >> Vblend.color1G
        if skipScale[2] ==0:
            Vlist[0].sz >> Vblend.color1B
        
        Vblend.outputR >> Vlist[1].sx
        Vblend.outputG >> Vlist[1].sy
        Vblend.outputB >> Vlist[1].sz
        
        Vresult = [Vblend]
    
    elif Vtype == 5:
        Vresult = pm.ls(pm.scaleConstraint(Vlist, w=weight, mo=Voffset, 
                                           sk=skipConsScale, n=VnameSuffix))
    
    elif Vtype == 6:
        Vblend = pm.createNode('blendColors', name='blend_%s' % (VnameSuffix))
        Vblend.blender.set (weight)
        
        # this has to be set, so that when the blender will be deleted inside
        # of the same script (permanent=0), the scaling will not be wrong.
        
        if Vlist[0].type() == 'multiplyDivide':
            scRX = Vlist[0].outputX
            scRY = Vlist[0].outputY
            scRZ = Vlist[0].outputZ
        else:
            scRX = Vlist[0].rx
            scRY = Vlist[0].ry
            scRZ = Vlist[0].rz
        
        Vblend.color1R.set (scRX.get())
        Vblend.color1G.set (scRY.get())
        Vblend.color1B.set (scRZ.get())
        Vblend.color2R.set (scRX.get())
        Vblend.color2G.set (scRY.get())
        Vblend.color2B.set (scRZ.get())
        
        if skipRotate[0] =='none':
            scRX >> Vblend.color1R
            Vblend.outputR >> Vlist[1].rx
        if skipRotate[1] =='none':
            scRY >> Vblend.color1G
            Vblend.outputG >> Vlist[1].ry
        if skipRotate[2] =='none':
            scRZ >> Vblend.color1B
            Vblend.outputB >> Vlist[1].rz
        
        Vresult = [Vblend]
              
    if permanent == 0:
        pm.delete(Vresult[0])
        
    pm.select(Vlist)
    return Vresult  

def constraintSwitch(Vlist=[], VattrName='space', Vname='', Vsuffix='', 
                     Vtype=3, Voffset=1, interpType=2):
    """
    Constraints an object between two others and creates a switch attribute.
    if four objects are selected the third receives the constraint and the 
    fourth the attribute.
 
    Returns:
        the blender
  
    =========== ===============================================
    Arguments   Description
    =========== ===============================================
    Vlist       [source, source, goal, ctrl(optional)]
    VattrName   name of the new attribute    
    Vtype       1=point, 2=orient, 3=parent
    =========== =============================================== 
    """
    Vlist = lsSl.lsSl(Vlist, 3, 4)
    
    if Vname == '':
        Vname='cons_%s' % (Vlist[2])
    VnameSuffix = '%s%s' % (Vname, Vsuffix)
    
    if Vtype ==1:
        Vcons = pm.ls(pm.pointConstraint(Vlist[0], Vlist[1], Vlist[2], 
                                         mo=Voffset, n=VnameSuffix))
    
    elif Vtype ==2:
        Vcons = pm.ls(pm.orientConstraint(Vlist[0], Vlist[1], Vlist[2], 
                                          mo=Voffset, n=VnameSuffix))
        Vcons[0].interpType.set (interpType)
        
    elif Vtype == 3:
        Vcons = pm.ls(pm.parentConstraint(Vlist[0], Vlist[1], Vlist[2], 
                                          mo=Voffset, n=VnameSuffix))
        Vcons[0].interpType.set (interpType)
    
    Vweights = filterConsWeights(Vcons, Vtype)
    Vblender = blendWeights(Vweights, Vsuffix)
        
    Vctrl = Vlist[-1]     
    connectBlenderToAttr([Vctrl, Vblender], VattrName)
    pm.select (Vlist[-1]) 
    
    return Vblender

def getCons(Vobject):   
    """
    Returns:
        the constraint of an object. returns [] if there's none
    """
    Vchildren = []
    Vresult = []
    Vchildren = pm.listRelatives (Vobject, children=1)
    
    for i in range(len(Vchildren)): 
        Vtype = Vchildren[i].nodeType()
        if Vtype == 'pointConstraint' or Vtype == 'orientConstraint' or Vtype == 'parentConstraint':
            Vresult.append (Vchildren[i])
            
    return Vresult
    
def filterCons(Vlist=[]):
    """
    Filters all constraints from a list of objects.
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    Vresult = []

    for i in range(len(Vlist)):
        Vtype = Vlist[i].nodeType()
        if Vtype == 'pointConstraint' or Vtype == 'orientConstraint' or Vtype == 'parentConstraint' or Vtype == 'scaleConstraint':
            Vresult.append (Vlist[i])
    
    return Vresult
    


def filterConsWeights(Vlist=[], Vtype=3):   
    """   
    Returns a list of all the weights of a constraint.

    ========= ===============================================
    Arguments Description
    ========= ===============================================
    Vlist     [constraint]
    Vtype     1=point, 2=orient, 3=parent
    ========= ===============================================
    """
    Vlist = lsSl.lsSl(Vlist, 1, 1)
    
    if  Vlist == []:
        return []
       
    Vweights = []
    Vresult = []
    
    if Vtype == 1:
        Vweights = pm.pointConstraint (Vlist[0], q=1, weightAliasList=1)
    
    elif Vtype == 2:
        Vweights = pm.orientConstraint (Vlist[0], q=1, weightAliasList=1)
        
    elif Vtype == 3:
        Vweights = pm.parentConstraint (Vlist[0], q=1, weightAliasList=1)
    
    if Vweights != None:   
        for i in range(len(Vweights)):
            Vresult.append (Vweights[i])
                       
    return Vresult
       
def blendWeights (Vlist, Vsuffix):
    """ 
    Creates: 
        a blender and connects the two given weights of a constraint
    Returns: 
        blender
        
    ========= ===================================
    Arguments Description
    ========= ===================================
    Vlist     [weightW0, weightW1]
    ========= ===================================
    """
    Vblend = pm.createNode('blendColors', name='blend%s' % (Vsuffix))
    Vblend.blender.set (1)
    Vblend.color1R.set (1)
    Vblend.color2R.set (0)
    Vblend.color1G.set (0)
    Vblend.color2G.set (1)
    
    Vblend.outputR >> Vlist[0]
    Vblend.outputG >> Vlist[1]
    
    return Vblend  

def connectBlenderToAttr(Vlist=[], Vname='blend'):   
    """ 
    Adds an attribute on an object and connects it to a blender.
    if the attribute already exists connect to existing.
    
    ========= ==================================
    Arguments Description
    ========= ==================================
    Vlist     [ctrl, blender]  
    ========= ==================================  
    """
    Vlist = lsSl.lsSl(Vlist, 2, 2)
    
    try:
        pm.connectAttr (('%s.%s' % (Vlist[0], Vname)), Vlist[1].blender)
    except:     
        Vattr = Vlist[0].addAttr( Vname, defaultValue=0, min=0 ,max=1, keyable=1)
        pm.connectAttr (('%s.%s' % (Vlist[0], Vname)), Vlist[1].blender)

def zeroOutConstraint(Vlist=[], VlastObj=0):
    """
    Zeroes out objects for constraining so the constraint will have no offset 
    and zero values on rotate  and translate to prevent it from flipping.
    returns a list of the new objects for constraining.
    
    creates a zero group for the object to be constrained,
    creates child objects for each constraining object and snaps it to the 
    constrained object.
    selects all new objects in the order of the originals.
   
    Returns: 
        selection list
  
    ========= =================================================
    Arguments Description
    ========= =================================================
    Vlist     constraining objects, object to be constraint
    VlastObj  1=the object before the last will be the last
    ========= =================================================    
    """   
    Vlist = lsSl.lsSl(Vlist, 2, 0)
    
    Vresult = []
    
    if VlastObj == 1:
        Vlast = Vlist[-1]
        Vlist.pop()
       
    for i in range(len(Vlist)-1):
        Vnull = pm.group(w=1, empty=1, n='goal_%s' % (Vlist[i]))
        Vlist[i]|Vnull
        
        Vresult.append(Vnull)
#        print Vlist[i]      
               
    tp.nullGroup([Vlist[-1]], Vhierarchy=3, Vtype=1, Vsuffix='_source')
     
    Vresult.append(Vlist[-1])
    snp.snap(Vresult)
    
    if VlastObj == 1:
        Vresult.append(Vlast)     
    pm.select (Vresult)
#    print Vresult
    
    return Vresult      

def distribute(Vlist=[], Vtype=3, Voffset=0, permanent=1, interpType=2, Vname='', Vsuffix='', 
               skipTranslate=['none', 'none', 'none'], skipRotate=['none','none','none'], 
               skipScale=[0, 0, 0], skipConsScale=['none','none','none']):
    """
    Creates: 
        constraints for a list of objects between two objects 
    
    Returns:
        constraints or blend nodes
    
    ============== =================================================
    Arguments      Description
    ============== =================================================   
    Vlist[0:-2]    list of constrained goal objects
    Vlist[-1]      start source object
    Vlist[-2]      end source object                
    Vtype          1=point, 2=orient, 3=parent,4=scale with blend nodes
    Voffset        constraint offset
    permanent      if 0 constraints will be deleted 
    interpType     interpolation type 1=average, 2=shortest      
    Vsuffix        suffix
    skipTranslate  translate axes to be skipped
    skipRotate     rotate axes to be skipped 
    skipScale      scale axes to be skipped  
    ============== =================================================
    
    .. note:: 
        Vtype=5 is only for testing
  
    example::
        # this distributes trans, rot and scale
        import cons as tc
        tc.distribute(Vtype=4, Voffset=0, permanent=1, skipScale=[0, 0, 0])
        tc.distribute(Vtype=3, Voffset=0, permanent=1)
    """    
    Vlist = lsSl.lsSl(Vlist, 3, 0)
    
    goalList = Vlist[0:-2]
    startSource = Vlist[-1]
    endSource = Vlist[-2]  
    goalLen = (len(goalList))
    weightPart = ((1.0/(goalLen+1)))
    Vresult=[]
    
    pm.rename(Vlist[-1], '%s%s'%(Vlist[-1], Vsuffix))
    pm.rename(Vlist[-2], '%s%s'%(Vlist[-2], Vsuffix))
    
    for i in range(len(goalList)):

        startWeight = (weightPart*(i+1))
        endWeight = 1-startWeight  

        startCons = cons (
                          Vlist=[startSource, goalList[i]], 
                          Vtype=Vtype, 
                          Voffset=Voffset, 
                          weight=startWeight, 
                          permanent=1,
                          interpType=interpType,
                          Vname=Vname, 
                          Vsuffix=Vsuffix, 
                          skipTranslate=skipTranslate, 
                          skipRotate=skipRotate,
                          skipScale=skipScale,
                          skipConsScale=skipConsScale)
        
        if Vtype == 4:
            if endSource.type() == 'multiplyDivide':
                scSX = endSource.outputX
                scSY = endSource.outputY
                scSZ = endSource.outputZ
            else:
                scSX = endSource.sx
                scSY = endSource.sy
                scSZ = endSource.sz
            
            if skipScale[0] == 0:
                scSX >> startCons[0].color2R
            if skipScale[1] == 0:
                scSY >> startCons[0].color2G
            if skipScale[2] == 0:
                scSZ >> startCons[0].color2B
                
        elif Vtype == 6:
            if endSource.type() == 'multiplyDivide':
                scRX = endSource.outputX
                scRY = endSource.outputY
                scRZ = endSource.outputZ
            else:
                scRX = endSource.rx
                scRY = endSource.ry
                scRZ = endSource.rz
            
            if skipRotate[0] =='none':
                scRX >> startCons[0].color2R
            if skipRotate[1] =='none':
                scRY >> startCons[0].color2G
            if skipRotate[2] =='none':
                scRZ >> startCons[0].color2B
      
        else:
            endCons = cons (
                            Vlist=[endSource, goalList[i]], 
                            Vtype=Vtype, 
                            Voffset=Voffset, 
                            weight=endWeight, 
                            permanent=1,
                            interpType=interpType,
                            Vname=Vname, 
                            Vsuffix=Vsuffix, 
                            skipTranslate=skipTranslate, 
                            skipRotate=skipRotate,
                            skipScale=skipScale,
                            skipConsScale=skipConsScale)
        
        Vresult.append(startCons)
        
    if permanent == 0:
        pm.delete(Vresult)
        return[]
        
    pm.select(Vlist)   
    return Vresult

def endRot(Vlist=[], Vsuffix='', attrName='wide_rot', weights=[1,2]):
    """
    ============== =================================================
    Arguments      Description
    ============== ================================================= 
    Vlist[0]       fix
    Vlist[1]       mobil
    Vlist[2]       If given a attribute to control the constraint 
                   weight will be added on this object.
    attrname       name of the attribute
    ============== =================================================   
    """
    Vlist = lsSl.lsSl(Vlist, Vmin=2, Vmax=3)

    rotObj = tp.nullGroup(Vlist=[Vlist[0]], Vhierarchy=4, Vtype=2, Vsuffix='rot_ob'+Vsuffix)
    snp.snap([rotObj[0], Vlist[1]])
    
    Vcons = cons(Vlist=[Vlist[0], rotObj[0]], Vtype=2, Voffset=0, weight=weights[0], interpType=2, permanent=1, Vname='', Vsuffix='', 
               skipTranslate=['none','none','none'], skipRotate=['none','none','none'], 
               skipScale=[0, 0, 0], skipConsScale=['none','none','none'])
    
    cons(Vlist=[Vlist[1], rotObj[0]], Vtype=2, Voffset=0, weight=weights[1], interpType=2, permanent=1, Vname='', Vsuffix='', 
               skipTranslate=['none','none','none'], skipRotate=['none','none','none'], 
               skipScale=[0, 0, 0], skipConsScale=['none','none','none'])
    
    if len(Vlist) == 3:
        Vlist[2].addAttr(attrName, defaultValue=1, min=0, max=1, keyable=1)
        Vweight = filterConsWeights(Vlist=[Vcons[0]], Vtype=2)
        pm.ls(str(Vlist[2]) + '.' + attrName)[0] >> Vweight[1]
        #pm.ls(str(Vlist[2]) + '.' + attrName)[0].setLocked(1)
    return rotObj


def constraintIkFk(VikList, VfkList, VdrvList, Vattr, Vsuffix, Voffset=0, 
                   Vctrl='', VskippLast=1, blendScale=1, useConstraints=0):
                
    """
    Constraints a driven setup to a IK and FK setup
    if a ctrl object is given it will receive the attribute to blend.
  
    Returns: 
        the blend node for the constraints list of the blend nodes for scale x 
    """ 
    VblendScaleList = []
    VblendRotList =[]
    
    Vblend = pm.createNode('blendColors', name='blend%s' % (Vsuffix))
    Vblend.blender.set (1)
    Vblend.color1R.set (1)
    Vblend.color2R.set (0)
    Vblend.color1G.set (0)
    Vblend.color2G.set (1)
    
    for i in range(len(VikList)-VskippLast):
        
        VnameSuffix = 'cons_%s%s' % (VikList[i], Vsuffix)
        
        if useConstraints == 1:  
            Vcons = pm.ls(pm.parentConstraint(VfkList[i], VikList[i], 
                                              VdrvList[i], mo=Voffset, 
                                              n=VnameSuffix))
            Vcons[0].interpType.set (2)
        
            Vweights = filterConsWeights(Vcons, Vtype=3)
    #        print Vweights
            Vblend.outputR >> Vweights[0]
            Vblend.outputG >> Vweights[1]
        # use blender only on rotation (works only for jointchains)    
        else:
            VblendRot= pm.createNode('blendColors', name='blendRot%s' % (VikList[i]))
            VblendRot.color1.set(1,1,1)
            VblendRot.color2.set(1,1,1)
            
            VikList[i].rx >> VblendRot.color2R
            VikList[i].ry >> VblendRot.color2G
            VikList[i].rz >> VblendRot.color2B
            
            VfkList[i].rx >> VblendRot.color1R
            VfkList[i].ry >> VblendRot.color1G
            VfkList[i].rz >> VblendRot.color1B
            
            VblendRot.outputR >> VdrvList[i].rx
            VblendRot.outputG >> VdrvList[i].ry
            VblendRot.outputB >> VdrvList[i].rz
            
            VblendRotList.append (VblendRot)
            if Vctrl != '':
                #connectAttr (('%s.%s' % (Vctrl, VattrName)), VblendRot.blender)  
                Vattr >> VblendRot.blender                
        if blendScale==1:

            VblendScale = pm.createNode('blendColors', name='blend%s' % (VikList[i]))
            VblendScale.color1.set(1,1,1)
            VblendScale.color2.set(1,1,1)
            
            VblendScale.outputR >> VdrvList[i].sx
            VikList[i].sx >> VblendScale.color2R
            VfkList[i].sx >> VblendScale.color1R
            
            VblendScale.outputG >> VdrvList[i].sy
            VikList[i].sy >> VblendScale.color2G
            VfkList[i].sy >> VblendScale.color1G
            
            VblendScale.outputB >> VdrvList[i].sz
            VikList[i].sz >> VblendScale.color2B
            VfkList[i].sz >> VblendScale.color1B
            
            
            VblendScaleList.append (VblendScale)
            if Vctrl != '':
                #connectAttr (('%s.%s' % (Vctrl, VattrName)), VblendScale.blender)
                Vattr >> VblendScale.blender
                                  
    if Vctrl != '':   
        #connectAttr (('%s.%s' % (Vctrl, VattrName)), Vblend.blender)
        Vattr >> Vblend.blender
    
    return Vblend, VblendScaleList

def freeTransIk(Vlist=[], Vsuffix=''):
    """
    Translation of the parent has no effect on the child.
    Rotation works normally.
   
    ========== =====================
    Arguments  Description
    ========== =====================
    Vlist[0]   source
    Vlist[1]   goal
    ========== =====================
    
    Returns:
        Object to parent the child to.
    """
    Vlist = lsSl.lsSl(Vlist, 1, 1)
    obj = Vlist[0]
    parent = obj.getParent()
    
    if obj.type() == 'joint':
        objType = 2
    else:
        objType = 3
    
    goal = tp.nullGroup(Vlist=[obj], Vname='con_goal'+Vsuffix, Vtype=3)[0]                  
    source = tp.nullGroup(Vlist=[obj], Vname='con_source'+Vsuffix, Vtype=3)[0]
    child = tp.nullGroup(Vlist=[obj], Vname='child'+Vsuffix, Vtype=objType)[0]
    par = tp.nullGroup(Vlist=[obj], Vname='child_parent'+Vsuffix, Vtype=3)[0]                            
    snap = tp.nullGroup(Vlist=[obj], Vname='par_to'+Vsuffix, Vtype=objType)[0]
    
    if parent:
        parent|source
    obj|goal
    obj|par|child|snap
                                
    cons(Vlist=[source, goal], Vtype=1)
    cons(Vlist=[source, goal], Vtype=2)
    
    goal.t >> child.t
    if  obj.type() == 'joint':
        obj.s >> snap.inverseScale
    
    return snap
       
def consWin():   
    try:
        pm.deleteUI('consWin')
    except: pass

    with pm.window('consWin', resizeToFitChildren=1, s=0, toolbox=1) as win:
        
        with pm.columnLayout():
            pm.separator(st='none', w=150, h=5)
            pm.button( l='zero out', w=150, c='import mf.tools.cons; mf.tools.cons.zeroCB()')
            pm.separator(w=150, h=10)
            
            pm.textField('consWinAttrNames', text="['parent_to_a', 'parent_to_b', 'parent_to_c']", w=150)
            pm.textField('consWinSuffix', text='suffix', w=150)
            
            pm.radioButtonGrp('consWinType', numberOfRadioButtons=3, vertical=1, select=3, labelArray3=["point", "orient", "parent"], w=90, h=54)
            pm.checkBox ('consWinOffset', v=1, al='left', l='offset')
            pm.separator(w=150, h=10)
            
            pm.checkBox ('consWinLastObj', v=0, al='left', l='attr on next')
            pm.button( l='one for each', w=150, c= 'import mf.tools.cons; mf.tools.cons.consCB()')
            pm.separator(w=150, h=10)
            
            pm.textField('consWinAttrName', text='attr_name', w=150)
            pm.button( l='two with blend', w=150, c= 'import mf.tools.cons; mf.tools.cons.switchCB()')
           
    win.show()
    
def zeroCB():
    
    Vlist = []
    Vlist = lsSl.lsSl(Vlist, 2, 0)
    
    VlastObjVal = pm.checkBox ('consWinLastObj', q=1, v=1) 

    zeroOutConstraint(Vlist, VlastObjVal)  
        
def consCB():
       
    VtypeVal = pm.radioButtonGrp('consWinType', q=1, select=1)
    VattrStr = pm.textField('consWinAttrNames', q=1, text=1)
    
    VattrVal = []
    if VattrStr != '':
        VattrVal = eval(VattrStr) 

#    print len(VattrVal) 
    
    VsuffixVal = pm.textField('consWinSuffix', q=1, text=1) 
    VoffsetVal = pm.checkBox('consWinOffset', q=1, v=1)
    VlastObjVal = pm.checkBox ('consWinLastObj', q=1, v=1) 
    spaceSwitch([], Vtype=VtypeVal, VattrName=VattrVal, Vsuffix=VsuffixVal, VlastObj=VlastObjVal, Voffset=VoffsetVal)
        
def switchCB ():
        
    VtypeVal = pm.radioButtonGrp('consWinType', q=1, select=1)
    VsuffixVal = pm.textField('consWinSuffix', q=1, text=1)
    VoffsetVal = pm.checkBox ('consWinOffset', q=1, v=1)
    VattrNameVal = pm.textField('consWinAttrName', q=1, text=1)
    constraintSwitch([], Vtype=VtypeVal, Vsuffix=VsuffixVal, Voffset=VoffsetVal, VattrName=VattrNameVal)    
    
def decompMatrix(Vlist=[], Vsuffix=''):
    """
    Constraints an object to another one.
    The constraining object can be in any hierarchy.
    """
    Vlist = lsSl.lsSl(Vlist, Vmin=1, Vmax=0)
    if not pm.pluginInfo('decomposeMatrix', loaded=1, q=1):
        try:
            pm.loadPlugin('decomposeMatrix')
        except:
            pass
    
    for e in Vlist:
       
        dcm = pm.createNode('decomposeMatrix')
        loc = pm.spaceLocator()
       
        e.worldMatrix >> dcm.inputMatrix
        dcm.outputRotate >> loc.r
        dcm.outputTranslate >> loc.t
        dcm.outputScale >> loc.s
        dcm.outputShear >> loc.shear
        loc.shearXY.setKeyable(1)
        loc.shearXZ.setKeyable(1)
        loc.shearYZ.setKeyable(1)


class aimInb():
    def __init__(self, Vsuffix='', Vlist=[]):
        Vlist = lsSl.lsSl(Vlist, Vmin=1, Vmax=0)
        start = Vlist[0]
        end = Vlist[1]
        inb = Vlist[2]

        null = tp.nullGroup(Vlist=[inb],  Vsuffix='_null', Vtype=1,
                            Vhierarchy=3)[0]

        aim1 = tp.nullGroup(Vlist=[null], Vname='aim1'+Vsuffix, Vtype=1,
                            Vhierarchy=4)[0]

        aim2 = tp.nullGroup(Vlist=[null], Vname='aim1' + Vsuffix, Vtype=1,
                            Vhierarchy=4)[0]


        ac1 = pm.aimConstraint(start, aim1, aimVector=[-1,0,0],
                         worldUpType='objectrotation',
                         worldUpObject=start)

        ac2 = pm.aimConstraint(end, aim2, aimVector=[1, 0, 0],
                         worldUpType='objectrotation',
                         worldUpObject=end)

        blend = pm.createNode('blendColors', name='blend')
        blend.blender.set()
        aim1.r >> blend.color1
        aim2.r >> blend.color2
        blend.output >> inb.r


    def create(self):
        pass
    
    
    