"""
Main UI scripts like display toggle etc.
"""
import pymel.core as pm
from pymel.core.language import mel

def toggleIsolateSelect():
    
    panel = pm.getPanel(underPointer=1)
    
    if pm.isolateSelect(panel, state=1, q=1):
        pm.isolateSelect(panel, state=0)
    else:
        pm.isolateSelect(panel, state=1)
        
    print '###'
    
def toggleIsolateSelectRefresh():
    panel = pm.getPanel(underPointer=1)
    mel.enableIsolateSelect(str(panel), 1) 
        