"""
General tools for handling meshes.
"""
import pymel.core as pm
import mf.builder.nodes as bn
import mf.tools.general as gt
import mf.tools.parent as tp
from mf.tools import lsSl
from mf.tools.snap import snap

class corrective():
    """Corrective shape"""
    def __init__(self, Vlist=[], Vsuffix='', edit=0, **kwdArgs):        
        if edit: self.edit(Vlist=Vlist)   
        else: self.create(**kwdArgs)
    
    def create(self, Vlist=[], **kwdArgs):
        """Creates transform (called 'mesh') and the shape."""
        self.mesh = pm.polyCube(**kwdArgs)[0]
        self.shape = self.mesh.getShape() 
        self.shape.addAttr(bn.register('mesh_corrective')[0])
    
    def edit(self, Vlist=[]):
        """Finds transform and shape."""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.shape = Vlist[0]
        self.mesh = self.shape.getParent()
        
    def blendshape(self):
        """Finds the blendshape node if there is one."""
        bs = self.shape.worldMesh[0].outputs()
        if not bs == []:
            try:
                if bs[0].nodeType() == 'blendShape':
                    return bs[0]
            except:
                pass
        return None

class mesh():
    """All meshes used for rendering or for cache export."""
    def __init__(self, Vlist=[], Vsuffix='', edit=0, **kwdArgs):        
        if edit: self.edit(Vlist=Vlist)   
        else: self.create(**kwdArgs)
            
    def create(self, Vlist=[], **kwdArgs):
        """Creates transform (called 'mesh') and the shape."""
        self.mesh = pm.polyCube(**kwdArgs)[0]
        #print self.mesh
        self.shape = self.mesh.getShape() 
        self.shape.addAttr(bn.register('mesh_rendermesh')[0])
    
    def edit(self, Vlist=[]):
        """Finds transform and shape."""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.shape = Vlist[0]
        self.mesh = self.shape.getParent()
        
    def getVis(self):
        """Returns 1 if the shape is visible in the hierarchy 
        (for cache export)."""
        return gt.isVisible([self.shape])
    
def getVis(namespace='', framerange=[1, 24]):
    """Gets the visibility of all rendermeshes."""
    obj = pm.ls(namespace +'*')
    meshes = bn.get(obj, 'mesh_rendermesh')
    length = framerange[1] - framerange[0] + 1
    result = []
   
    for mesh in meshes:
            result.append([str(mesh.shape), []])
    
    for i in range(length):
        pm.currentTime(framerange[0] + i) 
        
        for i in range(len(meshes)):
            #print meshes[i].shape + ' -- ' + str(meshes[i].getVis())
            result[i][1].append(meshes[i].getVis())
    
    result.sort()
    return result


class folicleCtrl():
    """
    ========= =========================================
    Arguments Description
    ========= =========================================
    Vlist[0]  Ctl (on the Nurbs surface)
    Vlist[1]  Nurbs
    Vlist[2]  Nurbs Copy
    Vlist[3]  Goal objevt (on the Nurbs Copy surface)
    ========= =========================================
    
    .. Warning::
        Set liear settings to ``cm``! Otherwise there seems to be a problem 
        with the unit conversion nodes.
    """   
    def __init__(self, Vlist=[], Vsuffix=''):
        Vlist = lsSl.lsSl(Vlist, 4, 4)
        
        ctrl = Vlist[0]
        nurbs = Vlist[1]
        nurbsCopy = Vlist[2]
        goal = Vlist[3]
        
        pm.rebuildSurface(nurbs,ch=False,rpo=True,rt=False,end=True ,
                          kr=False,kcp=False,kc=False,su=False,du= 3,
                          sv=False,dv=3 ,tol=0.01,fr=False,dir=2)  
        pm.rebuildSurface(nurbsCopy,ch=False,rpo=True,rt=False,end=True ,
                          kr=False,kcp=False,kc=False,su=False,du= 3,
                          sv=False,dv=3 ,tol=0.01,fr=False,dir=2)  
        
        #goal = pm.spaceLocator(n='goal' + Vsuffix)
        vProd = pm.createNode('vectorProduct', n='vp' + Vsuffix)
        vProd.operation.set(4)
        cpos = pm.createNode('closestPointOnSurface', n='cPOS' + Vsuffix)
        foll = pm.createNode('follicle', n='foll' + Vsuffix)
        mult = pm.createNode('multiplyDivide', n='mult' + Vsuffix)
        
        nullCtrl = tp.nullGroup(
                                Vlist=[ctrl], Vhierarchy=3, pivot=1,
                                Vname='null' + Vsuffix)[0]
        nullGoal = tp.nullGroup(Vlist=[goal], Vhierarchy=3, pivot=1,
                                Vname='null' + Vsuffix)[0]
        
        
        ctrl.t >> goal.t
        pm.geometryConstraint(nurbsCopy, goal)
        
        nurbsCopy.getShape().worldSpace[0] >> cpos.inputSurface
        
        cpos.parameterU >> foll.parameterU
        cpos.parameterV >> foll.parameterV
    
        nurbs.getShape().worldMatrix[0] >> foll.inputWorldMatrix
        nurbs.getShape().local >> foll.inputSurface
        
        foll.outTranslate >> foll.getParent().t
        
        goal.worldMatrix[0] >> vProd.matrix
        vProd.output >> cpos.inPosition
        
        
                                
        ctrl.t >> mult.input1
        
        mult.input2.set([-1,-1,-1])         
        mult.output >> ctrl.rotatePivotTranslate  
        
        foll.getParent()|nullCtrl

class follicleOnPoly():
    """
    ========= =========================================
    Arguments Description
    ========= =========================================
    Vlist     [transform, ..., ..., mesh]
    ========= =========================================
    """
    def __init__(self, Vlist=[], Vsuffix='', editable=1):
        Vlist = lsSl.lsSl(Vlist, 2, 0)
        self.editable =editable
        self.suffix = Vsuffix
        self.mesh = Vlist[-1]
        for e in Vlist[:-1]:
            self.connect(e)
    
    def connect(self, trans):
        
        self.foll = pm.createNode('follicle', n='follShape' + self.suffix)
        self.foll.outTranslate >> self.foll.getParent().t
        self.foll.outRotate >> self.foll.getParent().r
        self.mesh.getShape().worldMatrix[0] >> self.foll.inputWorldMatrix
        self.mesh.getShape().outMesh >> self.foll.inputMesh
        self.cpom = pm.createNode('closestPointOnMesh')
        self.mesh.getShape().outMesh >> self.cpom.inMesh
        
        self.vector_product = pm.createNode("vectorProduct")
        self.vector_product.operation.set(4)
        trans.worldMatrix >> self.vector_product.matrix
        trans.rotatePivot >> self.vector_product.input1
            
        self.vector_product.output >> self.cpom.inPosition
        
        if self.editable:    
            self.cpom.parameterU >> self.foll.parameterU
            self.cpom.parameterV >> self.foll.parameterV
        
        else:
            self.foll.parameterU.set(self.cpom.parameterU.get())
            self.foll.parameterV.set(self.cpom.parameterV.get())
            pm.delete(self.vector_product, self.cpom)
        
        
class closestPointCtrl():
    """
    ========= =========================================
    Arguments Description
    ========= =========================================
    Vlist[0]  controller
    Vlist[1]  nurbs
    ========= =========================================
    """
    def __init__(self, Vlist=[], Vsuffix='', aimConstraint=0):
        Vlist = lsSl.lsSl(Vlist, 2, 2)
        self.suffix = Vsuffix
        
        self.ctrl = Vlist[0]
        self.nurbs = Vlist[1]
        
        null = tp.nullGroup(Vlist=[self.ctrl], Vhierarchy=3, 
                            Vname='null' + Vsuffix, 
                            pivot=1)[0]
        
        noTrans = pm.group(em=1, n='no_trans' + Vsuffix)                    
        self.goal = pm.spaceLocator(n='goal' + Vsuffix)
        self.source = pm.spaceLocator(n='source' + Vsuffix)
        
        snap([self.source, self.ctrl])
        
        self.cpos = pm.createNode('closestPointOnSurface', 
                                  n='closestPointOnSurface1' + self.suffix)
        
        self.nurbs.getShape().ws >> self.cpos.inputSurface
        self.source.t >> self.cpos.inPosition
        self.cpos.position >> self.goal.t
        
        pm.pointConstraint(self.ctrl, self.source)

        if not aimConstraint:
            pm.orientConstraint(self.ctrl, self.goal)
        else:
            pm.aimConstraint([self.source, self.goal], worldUpObject=self.source,
                         worldUpType=2,
                         u=[0, 0, 1])
        
        noTrans|self.source
        noTrans|self.goal


class closestPointCtrlPolygon():
    """
    ========= =========================================
    Arguments Description
    ========= =========================================
    Vlist[0]  controller
    Vlist[1]  nurbs
    ========= =========================================
    """

    def __init__(self, Vlist=[], Vsuffix=''):
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.suffix = Vsuffix

        self.goal = pm.spaceLocator(n='goal' + Vsuffix)
        self.source = pm.spaceLocator(n='source' + Vsuffix)
        self.cpos = pm.createNode('closestPointOnMesh',
                                  n='closestPointOnSurface1' + self.suffix)

        Vlist[0].getShape().outMesh >> self.cpos.inMesh
        Vlist[0].getShape().worldMatrix >> self.cpos.inputMatrix
        self.cpos.result.position >> self.goal.t

        dcm = pm.createNode('decomposeMatrix')
        self.source.worldMatrix >> dcm.inputMatrix
        dcm.outputTranslate >> self.cpos.inPosition

        pm.aimConstraint([self.source, self.goal], worldUpObject=self.source, worldUpType=2,
                         u=[0, 0, 1])


class rivet():
    """
    Rivet like the one from Michael Bazhutkins mel script. 
    Select two polygon mesh edges or a surface point on a nurbs surface.
    """
    def __init__(self, Vlist=[], Vsuffix='', edit=0, constraint=1, switch=0, 
                 **kwdArgs):
        
        if edit: self.edit(Vlist=Vlist)   
        else: self.create(Vlist=Vlist, Vsuffix=Vsuffix, constraint=constraint, 
                          switch=switch, **kwdArgs)
        
    def create(self, Vlist=[], Vsuffix='', constraint=0, switch=0, **kwdArgs):
        
        Vlist = lsSl.lsSl(Vlist, 1, 2)
        self.suffix = Vsuffix 
        
        if Vlist[0].nodeType() == 'mesh':
            # if polygon
            if Vlist == []:
                Vlist = pm.ls(pm.filterExpand(sm=32))
            else:
                Vlist = pm.ls(pm.filterExpand(Vlist, sm=32))

            obj = pm.ls(Vlist[0].split('.')[0])[0]
            
            self.cfmeA = pm.createNode('curveFromMeshEdge', 
                                       n='curveFromMeshEdgeA1' + self.suffix)
            self.cfmeA.ihi.set(1)
            self.cfmeA.edgeIndex[0].set(int(Vlist[0].split('[')[1].split(']')[0]))
            
            self.cfmeB = pm.createNode('curveFromMeshEdge', 
                                       n='curveFromMeshEdgeB1' + self.suffix)
            self.cfmeB.ihi.set(1)
            self.cfmeB.edgeIndex[0].set(int(Vlist[1].split('[')[1].split(']')[0]))
            
            self.loft = pm.createNode('loft', n='loft1' + self.suffix)
            self.loft.inputCurve.set(s=2)
            self.loft.uniform .set(1)
            self.loft.reverseSurfaceNormals.set(1)
            
            self.posi = pm.createNode('pointOnSurfaceInfo', 
                                      n='pointOnSurfaceInfo1' + self.suffix)
            self.posi.turnOnPercentage.set(1)
            self.posi.parameterU.set(0.5)
            self.posi.parameterV.set(0.5)
            
            self.loft.outputSurface >> self.posi.inputSurface
            self.cfmeA.outputCurve >> self.loft.ic[0]
            self.cfmeB.outputCurve >> self.loft.ic[1]
            obj.worldMesh >> self.cfmeA.im
            obj.worldMesh >> self.cfmeB.im
        
        else:
            # if nurbs
            self.cfmeA = None
            self.cfmeB = None
            self.loft = None
            
            nurbsShape = pm.ls(Vlist[0], o=1)[0]
            
            u = float(Vlist[0].split('[')[1].split(']')[0])
            v = float(Vlist[0].split('[')[2].split(']')[0])
            
            self.posi = pm.createNode('pointOnSurfaceInfo', 
                                      n='pointOnSurfaceInfo1' + self.suffix)
            self.posi.turnOnPercentage.set(0)
            self.posi.parameterU.set(u)
            self.posi.parameterV.set(v)
            
            nurbsShape.ws >> self.posi.inputSurface
        
        
        self.loc = pm.spaceLocator()
        pm.rename(self.loc, 'rivetLoc' + self.suffix + '1')
       
        self.loc.addAttr(bn.register('transform_rivet')[0])
        
        self.posi.position >> self.loc.translate
        self.cons = None
        
        if switch:
            self.switch()
        
       
        self.aimCons()
        if not constraint:
            self.delCons()
    
    def edit(self, Vlist=[], Vsuffix=''):
        """Finds all nodes."""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.suffix = Vsuffix
        self.loc = Vlist[0]
        self.posi = self.loc.t.inputs()[0]
        try: 
            self.cons = self.loc.r.inputs()[0]
        except: 
            self.cons = None
            
        self.loft = self.posi.inputSurface.inputs()[0]
        if self.loft.nodeType() == 'transform':
            self.loft = None
            self.cfmeA = None
            self.cfmeB = None
            
        else:
            self.cfmeA = self.loft.inputCurve[0].inputs()[0]
            self.cfmeB = self.loft.inputCurve[1].inputs()[0]
        
        if  self.loc.hasAttr('enable'):
            self.setRange = self.loc.enable.outputs()[0]
        
    def aimCons(self):
        """Aim constraint the rivet to the mesh edges."""
        self.delCons()
        self.cons = pm.createNode('aimConstraint', p=self.loc, 
                                 n=str(self.loc) + '_aimConstraint1')
        self.cons.target[0].targetWeight.set(1)
        self.cons.aimVector.set([0, 1, 0], type='double3')
        self.cons.upVector.set([0, 0, 1], type='double3')
        
        self.posi.normal >> self.cons.target[0].targetTranslate 
        self.posi.tangentV >> self.cons.worldUpVector
        self.cons.constraintRotate >> self.loc.rotate
        
    def orientCons(self, Vlist=[]):
        """Orient constraints the rivet to the selected object."""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.delCons()
        self.cons = pm.orientConstraint(Vlist[0], self.loc, mo=1)   
        
    def delCons(self):
        """Deletes the aim constraint"""
        if self.cons:
            r = self.loc.r.get()
            pm.delete(self.cons)
            self.cons = None
            self.loc.r.set(r)
        
    def switch(self):
        """Creates an 'enable' attribute on the locator 
        to switch the rivet off."""
        self.loc.addAttr('enable', at='long', defaultValue=1, 
                         min=0, max=1, keyable=1)
        
        self.setRange = pm.createNode('setRange', 
                                      name='setRange1' + self.suffix)
        self.setRange.oldMinX.set(0)
        self.setRange.oldMaxX.set(1)
        self.setRange.minX.set(2)
        self.setRange.maxX.set(0)
        self.loc.enable >> self.setRange.valueX
        
        if self.cfmeA:
            self.setRange.outValueX >> self.cfmeA.nodeState
        if self.cfmeB:
            self.setRange.outValueX >> self.cfmeB.nodeState
        if self.loft:
            self.setRange.outValueX >> self.loft.nodeState
        self.setRange.outValueX >> self.posi.nodeState
        
    def connectScale(self, attr=''):
        attr >> self.loc.sx
        attr >> self.loc.sy
        attr >> self.loc.sz
    
    def connectEnable(self, attr, connectVis=0):
        attr >> self.loc.enable
        if connectVis:
            attr >> self.loc.v
            
    
    def delete(self):
        """Deletes all nodes from the rivet setup."""
        pm.delete(self.loc, self.cfmeA, self.cfmeB, self.loft, self.posi,
                  self.setRange)
        if self.cons:
            pm.delete(self.cons)
            
def duplicate(Vlist=[], Vsuffix='', **args):
    Vlist = lsSl.lsSl(Vlist, 1, 1)
    """Duplicates a mes and deletes all unused shape found under the transform."""
    dup = pm.duplicate(Vlist[0], **args)
    gt.deleteUnusedShapes(dup[0].getShapes())
    
    dup[0].tx.setLocked(0)
    dup[0].ty.setLocked(0)
    dup[0].tz.setLocked(0)
    
    dup[0].rx.setLocked(0)
    dup[0].ry.setLocked(0)
    dup[0].rz.setLocked(0)
    
    dup[0].sx.setLocked(0)
    dup[0].sy.setLocked(0)
    dup[0].sz.setLocked(0)
    
    dup[0].t.setLocked(0)
    dup[0].r.setLocked(0)
    dup[0].s.setLocked(0)
    
    dup[0].shearXY.setLocked(0)
    dup[0].shearXZ.setLocked(0)
    dup[0].shearYZ.setLocked(0)
    
    return dup


    

    
        