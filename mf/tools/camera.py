"""
general camera tools
"""
import os
import pymel.core as pm
from mf.tools import lsSl


class bakeCam():
    """
    TODO:
    File browser.
    Plane and cam separate.
    """
    def __init__(self, Vlist=[], deleteOriginal=0):
        self.sl = lsSl.sl(Vlist, 1, 0, Vtype=['transform'])
        for o in self.sl.Vlist:
            self.duplicate(o)
            if deleteOriginal:
                pm.delete(o)
                
    def duplicate(self, object):
        """Duplicates the object and constraints it to the original."""
        self.dup = pm.duplicate(object, rr=1, renameChildren=1, ic=1)[0]
        fileName= os.path.splitext(os.path.split(pm.system.sceneName())[-1])[0]
        if not fileName == '':
            pm.rename(self.dup, str(object)+'__'+fileName)
        else:
            pm.rename(self.dup, str(object)+'__BAKED')
        
        self.disconnectAttr(object, self.dup)
        
        try:
            pm.parent(self.dup, w=1)
        except:
            pass
        pm.parentConstraint(object, self.dup, mo=0)
        
        shape = self.dup.getShape()
        
        isCam = 0
        if shape:
            if shape.nodeType() == 'camera':
                isCam = 1
            
        if isCam:
            try:
                self.dup.s.set(1, 1, 1)
            except: 
                pass 
        else:    
            pm.scaleConstraint(object, self.dup, mo=0)
        
        self.bake(self.dup)
        
        children = pm.listRelatives(self.dup, c=1, shapes=0)
        children.remove(self.dup.getShape())
        pm.delete(children)
        return self.dup
        
    def disconnectAttr(self, object, dup):
        """Disconnects all attributes in the attrList"""
        attList = [dup.tx, dup.ty, dup.tz,
                  dup.rx, dup.ry, dup.rz,
                  dup.sx, dup.sy, dup.sz,
                  dup.t, dup.r, dup.s]
        
        for a in attList:
            a.setLocked(0)
            inp = a.inputs(plugs=1)
            if not inp == []:
                 
                pm.disconnectAttr(inp[0], a) 

    def bake(self, object):
        """Bakes the duplicate."""
        start = pm.playbackOptions (min=1, q=1)
        end = pm.playbackOptions (max=1, q=1)
        pm.bakeResults(simulation=1, t=[start, end], sampleBy=1,
                       disableImplicitControl=1, preserveOutsideKeys=1,
                       sparseAnimCurveBake=0, removeBakedAttributeFromLayer=0,
                       bakeOnOverrideLayer=0, controlPoints=0, shape=1)


class muteCam():
    """
    Mutes or unmutes the camera
    """

    def __init__(self, camName="CAMRIG:ShotCam"):
        cam = pm.ls(camName)[0]
        camShape = pm.ls(camName+"Shape")[0]

        attrList = pm.listAttr(cam, k=1)
        attrListShape = pm.listAttr(camShape, k=1)

        self.attrList = []
        for e in attrList:
            #print e
            self.attrList.append(cam.attr(e))

        self.attrListShape = []
        for e in attrListShape:
            #print e
            self.attrListShape.append(camShape.attr(e))

    def mute(self):
        for e in self.attrList + self.attrListShape:
            e.setLocked(0)
            e.mute()
            e.setLocked(1)

    def unMute(self):
        for e in self.attrList + self.attrListShape:
            e.setLocked(0)
            e.unmute()
            e.setLocked(1)

    def tgl(self):
        muted = False
        for e in self.attrList:
            if e.isMuted():
                muted = True
        if muted:
            self.unMute()
        else:
            self.mute()



