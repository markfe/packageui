import pymel.core as pm
import mf.builder.main as bm
import mf.tools.lsSl as lsSl
import mf.tools.parent as tp
import mf.builder.nodes as bn
import mf.tools.snap as snap
import mf.tools.key as tk

class mix():
    def __init__(self):
        """
        Find KAG. If not found, create one.
        Find all tweakers.
        """
        sl = pm.ls(sl=1)
        if not pm.objExists('KAG'):
            self.KAG = pm.spaceLocator(n='KAG')
        else:
            self.KAG = pm.ls('KAG')[0]
        
        pm.select(sl)
        #self.KAG.addAttr('temp_pos', defaultValue=0, min=0, max=1, keyable=1)

    def ctls_setup(self, Vlist=[]):
        """
        Select a list of loose objects that will become the tweak ctls::
        
            null
               driven
                  tweak
        """
        Vlist = lsSl.lsSl(Vlist, 1, 0)
        
        tweaker = self.tweaker_get()
        tweakerI = 0
        
        try:
            num = []
            for e in tweaker:
                num.append(int(str(e).split('_')[-1]))
                
            num.sort()
            tweakerI = num[-1]+1
            
        except:
            pass
        
        bn.set(Vlist, 'transform_ctrl_face')
        
        for i in range(len(Vlist)):
            pm.rename(Vlist[i], 'tweaker_' + str(i + tweakerI))
            
            null_key = tp.nullGroup([Vlist[i]], 
                                      Vname='null_driven_' + str(i + tweakerI))[0]             
            driven = tp.nullGroup([Vlist[i]],
                               Vname='driven_' + str(i + tweakerI))[0]
    
            null_key|driven|Vlist[i]
            Vlist[i].addAttr('mirror', at='message')
            #Vlist[i].addAttr('side', dt='string')
            if not Vlist[i].hasAttr('side'):
                Vlist[i].addAttr("side" ,at="enum", en="Center:Left:Right:None:")
    
    def grab_values(self, Vlist=[]):
        """
        Grabs the values from the keyed objects and sets them to the tweakers.
        """
        Vlist = lsSl.lsSl(Vlist, 1, 0)
        
        for e in Vlist:
            dvn = e.getParent()
            tmp = pm.spaceLocator()
            snap.snap([tmp, e])
            pm.scaleConstraint(e, tmp, mo=0)
            
            dvn.s.set(tmp.s.get())
            
            dvn.t.set([0,0,0])
            dvn.r.set([0,0,0])
            dvn.s.set([1,1,1])
            snap.snap([e, tmp])    
            pm.delete(tmp)
        
    def ctls_match(self, Vlist=[]):
        """
        Makes a connection between two ctls to mark them as matching for 
        mirroring.
        """
        Vlist = lsSl.lsSl(Vlist, 2, 2)
        Vlist[0].mirror >> Vlist[1].mirror
        
    def ctls_side_set(self, Vlist=[], side=1):
        """
        Sets the side of a ctl (1=left, 2=right).
        """
        Vlist = lsSl.lsSl(Vlist, 1, 0)
        for e in Vlist:
            e.side.set(side)
    
    def pose_push(self, Vlist=[], val=1):
        for e in Vlist:
            if e.hasAttr('transform_ctrl_face'):
                dvn = e.getParent()
                
                dvn.tx.set(dvn.tx.get()*val)
                dvn.ty.set(dvn.ty.get()*val)
                dvn.tz.set(dvn.tz.get()*val)
                
                dvn.rx.set(dvn.rx.get()*val)
                dvn.ry.set(dvn.ry.get()*val)
                dvn.rz.set(dvn.rz.get()*val)
                
                dvn.sx.set(1+((dvn.sx.get()-1)*val))
                dvn.sy.set(1+((dvn.sy.get()-1)*val))
                dvn.sz.set(1+((dvn.sz.get()-1)*val))
            
    def push_tweakers(self, Vlist=[], val=1):
        for e in Vlist:
            if e.hasAttr('transform_ctrl_face'):
                e.tx.set(e.tx.get()*val)
                e.ty.set(e.ty.get()*val)
                e.tz.set(e.tz.get()*val)
                
                e.rx.set(e.rx.get()*val)
                e.ry.set(e.ry.get()*val)
                e.rz.set(e.rz.get()*val)
                
                e.sx.set(1+((e.sx.get()-1)*val))
                e.sy.set(1+((e.sy.get()-1)*val))
                e.sz.set(1+((e.sz.get()-1)*val))
            
    def pose_set(self, Vlist=[], attr=None):
        """
        Sets a pose.
        """
        Vlist = lsSl.lsSl(Vlist, 1, 0)
        
        for e in Vlist:
            dvn = e.getParent()
            tmp = pm.spaceLocator()
            snap.snap([tmp, e])
            snap.snap([dvn, tmp])
            
            pm.scaleConstraint(e, tmp, mo=0)
            
            dvn.s.set(tmp.s.get())
            pm.delete(tmp)
            
            e.t.set([0,0,0])
            e.r.set([0,0,0])
            e.s.set([1,1,1])
            
            for a in [dvn.tx, dvn.ty, dvn.tz,
                      dvn.rx, dvn.ry, dvn.rz,
                      dvn.sx, dvn.sy, dvn.sz]:
                
                pm.setDrivenKeyframe(a, currentDriver=attr)
                
        pm.select(Vlist)
        
    def pose_set_driven(self, Vlist=[], attr=None):
        """
        Sets a pose.
        """
        Vlist = lsSl.lsSl(Vlist, 1, 0)
        
        for e in Vlist:
            dvn = e.getParent()
            
            for a in [dvn.tx, dvn.ty, dvn.tz,
                      dvn.rx, dvn.ry, dvn.rz,
                      dvn.sx, dvn.sy, dvn.sz]:
                
                pm.setDrivenKeyframe(a, currentDriver=attr)
                
        pm.select(Vlist)
        
    def tweaker_get(self):
        c =  bn.getAll('transform_ctrl_face')
        ctls = []
        for e in c:
            ctls.append(e.ctrl)
        return ctls
    
    def poses_get(self):
        return pm.listAttr(self.KAG, userDefined = 1)
    
    def pose_tweaker_get(self, attr):
        curves = attr.outputs(plugs=0)
        driven = []
        tweakers = []
        for c in curves:
            if c.hasAttr('output'):
                output = c.output.outputs(plugs=0, scn=1)
                if not output == []:
                    if output[0].hasAttr('output'):
                        key = output[0].output.outputs(plugs=0, scn=1)
                        if not key == []:
                            if not key[0] in driven:
                                driven.append(key[0])
                    #no blendWeighted nodes yet
                    else:
                        if not output[0] in driven:
                            driven.append(output[0])
                
        for e in driven:
            
            children = e.getChildren()
            if not children == []:
                if children[0].hasAttr('transform_ctrl_face'):
                    tweakers.append(children[0])

        return tweakers
    
    def mirror(self, Vlist=[], fromSide=1, factor=[[-1,-1,-1],[1,1,1],[1,1,1]]):
        Vlist = lsSl.lsSl(Vlist, 1, 0)
        for e in Vlist:
            side = ''
            #print e
            if e.hasAttr('side'):
                side = e.side.get()
                #print side
            if side == fromSide:
                con = e.mirror.connections(plugs=0)
                #print con
                t = e.getParent().t.get()
                r = e.getParent().r.get()
                s = e.getParent().s.get()
    
                con[0].getParent().t.set([t[0]*factor[0][0], 
                                          t[1]*factor[0][1], 
                                          t[2]*factor[0][2]])
                con[0].getParent().r.set([r[0]*factor[1][0], 
                                          r[1]*factor[1][1], 
                                          r[2]*factor[1][2]])
                con[0].getParent().s.set([s[0]*factor[2][0], 
                                          s[1]*factor[2][1], 
                                          s[2]*factor[2][2]])
                
    def flip(self, Vlist=[], factor=[[-1,-1,-1],[1,1,1],[1,1,1]]):
        Vlist = lsSl.lsSl(Vlist, 1, 0)
        valList = []
        for e in Vlist:
            side = 0
            
            
            #print e
            if e.hasAttr('side'):
                side = e.side.get()
                #print side
            
            print '----------------'
            print side
            if side == 1 or side == 2:    
                con = e.mirror.connections(plugs=0)
                #print con
                t = e.getParent().t.get()
                r = e.getParent().r.get()
                s = e.getParent().s.get()
                
                print con
                print '----------------'
                
                valList.append([con[0].getParent(),
                [t[0]*factor[0][0], t[1]*factor[0][1], t[2]*factor[0][2]],
                [r[0]*factor[1][0], r[1]*factor[1][1], r[2]*factor[1][2]],
                [s[0]*factor[2][0], s[1]*factor[2][1], s[2]*factor[2][2]]])
                
            else:
                pass
                
                key = e.getParent()
                
                t = key.t.get()
                r = key.r.get()
                s = key.s.get()
    
                key.t.set([t[0]*-1, 
                           t[1]*1, 
                           t[2]*1])
                key.r.set([r[0]*1, 
                           r[1]*-1, 
                           r[2]*1])      
        for o in valList:
            o[0].t.set(o[1])
            o[0].r.set(o[2])
            o[0].s.set(o[3])
        
                
    def corrective(self, sourceA='', sourceB=''):
        
        newAttr = sourceA + '_' + sourceB
        self.KAG.addAttr(newAttr, defaultValue=0, min=0, max=1, keyable=1)
        goal = pm.ls(str(self.KAG)+ '.' + newAttr)
        
        blend = pm.createNode('blendColors', name='blend_pose')
        blend.color1R.set(0)
        pm.ls(str(self.KAG)+ '.' + sourceA).pop() >> blend.blender
        pm.ls(str(self.KAG)+ '.' + sourceB).pop() >> blend.color1R
        tk.drvKeySet([[0,0], [1,1]], blend.outputR, goal)
        
    def addPose(self, name=''):
        self.KAG.addAttr(name, defaultValue=0, min=0, max=1, keyable=1)

def docking(dock):
    if dock:
        try:
            pm.deleteUI('mixUI_doc')
            pass
        except:
            pass   
        allowedAreas = ['right', 'left']
        pm.dockControl('mixUI_doc', l='mixUI  ',
                       area='left', content='keyMixUI', allowedArea=allowedAreas, 
                       fl=0, w=240)
        
def UI(dock=1):
    """
    See attributes and tweakers.
    Select all tweakers for a pose.
    
    """
    #mix = mix()
    relPath = bm.getRelativePath()    
    UIname = 'keyMixUI'
    try:
        pm.deleteUI(UIname)
    except: 
        pass
    
    try:
        pm.deleteUI('mixUI_doc')
        pass
    except:
        pass
    
    pm.window(UIname, t=UIname, h=150, w=200, s=1)
            
    pm.formLayout('keyMixMainFl')
            
    with pm.rowLayout('mixButtonsBar', nc=5) as mixButtons:
        pm.symbolButton( image=relPath+'\\UI_icons\\refresh.png', 
                 c='import mf.tools.keyMix; mf.tools.keyMix.UI('+str(dock)+')',
                 ann='Refresh UI') 
        pm.symbolButton( image=relPath+'\\UI_icons\\shake.png', 
                 c='import mf.tools.keyMix; mf.tools.keyMix.shake()',
                 ann='Refresh KAG') 
        
        pm.symbolButton( image=relPath+'\\UI_icons\\eyeB.png', 
                 c='import mf.tools.keyMix; mf.tools.keyMix.tgl_shapes_tweaker()',
                 ann='Refresh KAG')
        
        pm.symbolButton( image=relPath+'\\UI_icons\\eyeA.png', 
                 c='import mf.tools.keyMix; mf.tools.keyMix.tgl_shapes_driven()',
                 ann='Refresh KAG')
        
        pm.button('KAG', c='import mf.tools.keyMix; mf.tools.keyMix.select_KAG()')
        
    
    with pm.rowLayout('mixMainMenuBar', adj=1) as mixAttr:
        pass
    
    with pm.rowLayout('pushPoseBarTweaker', adj=1) as pushAttr:
        pm.floatSliderGrp('pushFloatSliderTweaker', label='tweaker', field=True, minValue=0, maxValue=2.0, 
                          fieldMinValue=0, fieldMaxValue=10, value=1, pre=4, 
                          changeCommand='import mf.tools.keyMix; mf.tools.keyMix.push_tweaker()')
    
    with pm.rowLayout('pushPoseBar', adj=1) as pushAttrKey:
        pm.floatSliderGrp('pushFloatSlider', label='driven', field=True, minValue=0, maxValue=2.0, 
                          fieldMinValue=0, fieldMaxValue=10, value=1, pre=4, 
                          #bgc=[.9,.9,.6],
                          changeCommand='import mf.tools.keyMix; mf.tools.keyMix.push()')
    
    with pm.paneLayout('keyMixPL', configuration="vertical3") as mixForm: 
        
        with pm.formLayout('posesFl', parent='keyMixPL'):
            pm.textScrollList('posesSL', 
                              allowMultiSelection=True, 
                              ann='Sub Folders',
                              font='boldLabelFont',
                              selectCommand = 'import mf.tools.keyMix; mf.tools.keyMix.pose_tweaker_get()')
            with pm.popupMenu():
                pm.menuItem(label='set pose', c='import mf.tools.keyMix; mf.tools.keyMix.pose_set()')   
                pm.menuItem(label='set pose to driven', c='import mf.tools.keyMix; mf.tools.keyMix.pose_set_driven()')  
                pm.menuItem(label='set pose to tweakers selected in scene', c='import mf.tools.keyMix; mf.tools.keyMix.pose_selected_set()')
                pm.menuItem(label='set all poses to 0', c='import mf.tools.keyMix; mf.tools.keyMix.posesReset()')
                pm.menuItem(divider=1) 
                pm.menuItem(label='mirror', c='import mf.tools.keyMix; mf.tools.keyMix.mirror()') 
                pm.menuItem(label='flip', c='import mf.tools.keyMix; mf.tools.keyMix.flip()') 
                pm.menuItem(divider=1)
                pm.menuItem(label='add pose', c='import mf.tools.keyMix; mf.tools.keyMix.addPose()')
                pm.menuItem(label='add corrective', c='import mf.tools.keyMix; mf.tools.keyMix.corrective()')
                pm.menuItem(divider=1)
                #pm.menuItem(label='select  KAG', c='import mf.tools.keyMix; mf.tools.keyMix.select_KAG()') 
                pm.menuItem(label='connect ctl', c='import mf.tools.keyMix; mf.tools.keyMix.connect_ctl()') 
                
    
        with pm.formLayout('tweakerAllFl', parent='keyMixPL'):
            pm.textScrollList('tweakerAllSL', 
                              allowMultiSelection=True, 
                              ann='Meshes',
                              selectCommand = 'import mf.tools.keyMix; mf.tools.keyMix.tweaker_select_pose()',
                              dcc='')
            
            with pm.popupMenu():
                pm.menuItem(label='select all', c='import mf.tools.keyMix; mf.tools.keyMix.pose_tweakers_select()')   
            
        with pm.formLayout('tweakerListFl', parent='keyMixPL'):
            pm.textScrollList('tweakerListsSL', 
                              allowMultiSelection=True, 
                              ann='Weights',
                              sc='import mf.tools.keyMix; mf.tools.keyMix.tweaker_select()',
                              dcc='')
            
            with pm.popupMenu():
                pm.menuItem(label='make tweaker', c='import mf.tools.keyMix; mf.tools.keyMix.tweaker_set()') 
                pm.menuItem(label='set side', c='import mf.tools.keyMix; mf.tools.keyMix.side_set()') 
                pm.menuItem(label='match sides', c='import mf.tools.keyMix; mf.tools.keyMix.side_match()')
                pm.menuItem(divider=1)
                pm.menuItem(label='grab values', c='import mf.tools.keyMix; mf.tools.keyMix.grab_values()')
    
    
    with pm.columnLayout('keyMixMainFl', adj=1) as mixMirror:
        #pm.textField()
        with pm.rowLayout(numberOfColumns=5, 
                          columnWidth5=(75, 75, 75, 20, 90),
                          columnAlign=(5, 'right')):
            pm.intFieldGrp('mirror_t_KM', numberOfFields=3,  
                           columnWidth3=[20,20,20], 
                           value1=-1, value2=-1, value3=-1)
            pm.intFieldGrp('mirror_r_KM', numberOfFields=3,  
                           columnWidth3=[20,20,20], 
                           value1=1, value2=1, value3=1)
            pm.intFieldGrp('mirror_s_KM', numberOfFields=3,
                           columnWidth3=[20,20,20], 
                           value1=1, value2=1, value3=1)
            pm.intField('mirror_side_KM', v=1)
      
            pm.text('t, r, s, side')
            pass
                
    docking(dock)
            
    pm.formLayout(
        'keyMixMainFl', e=True,
        af=[(mixButtons, 'left', 0),
            (mixButtons, 'right', 0), (mixButtons, 'top', 0),
            
            (mixAttr, 'left', 0), 
            (mixAttr, 'right', 0), (mixAttr, 'top', 25),
            
            (pushAttr, 'left', 0), 
            (pushAttr, 'right', 0), (pushAttr, 'top', 50),
            
            (pushAttrKey, 'left', 0), 
            (pushAttrKey, 'right', 0), (pushAttrKey, 'top', 75),
            
            (mixForm, 'left', 0), (mixForm, 'bottom', 0),  
            (mixForm, 'right', 0), (mixForm, 'top', 100), 
            
            (mixMirror, 'left', 0), (mixMirror, 'bottom', 0),
            (mixMirror, 'right', 0)
            ],
        ac=[
            (mixForm, 'bottom', 0,  mixMirror)]
            ) 
    
    pm.formLayout(
        'posesFl', e=True,
        af=[('posesSL', 'left', 0), ('posesSL', 'bottom', 0), 
            ('posesSL', 'right', 0), ('posesSL', 'top', 0),
            ]
            ) 
    pm.formLayout(
        'tweakerListFl', e=True,
        af=[('tweakerListsSL', 'left', 0), ('tweakerListsSL', 'bottom', 0), 
            ('tweakerListsSL', 'right', 0), ('tweakerListsSL', 'top', 0),
            ]
            ) 
    
    pm.formLayout(
        'tweakerAllFl', e=True,
        af=[('tweakerAllSL', 'left', 0), ('tweakerAllSL', 'bottom', 0), 
            ('tweakerAllSL', 'right', 0), ('tweakerAllSL', 'top', 0),
            ]
            )
    tweaker_get()
    poses_get()
    if not dock:
        pm.showWindow(UIname)

def select_KAG():
    pm.select('KAG')

def push():
    val =  pm.floatSliderGrp('pushFloatSlider', q=1, v=1)
    m = mix()
    m.pose_push(Vlist=pm.ls(sl=1), val=val)
    
def push_tweaker():
    val =  pm.floatSliderGrp('pushFloatSliderTweaker', q=1, v=1)
    m = mix()
    m.push_tweakers(Vlist=pm.ls(sl=1), val=val)

def shake():
    m = mix()
    poses = m.poses_get()
    
    current = pm.currentTime(q=1)
    pm.currentTime(current, update=1)

    for p in poses:
        attr = pm.ls('KAG.' + p)[0] 
        val = attr.get()
        attr.set(0)
        attr.set(val)
    
    for p in poses:
        attr = pm.ls('KAG.' + p)[0] 
        
        if attr.inputs() == []:
            val = attr.get()
            attr.set(0)
            attr.set(val)
            
def posesReset():
    m = mix()
    poses = m.poses_get()
    for p in poses:
        pm.ls('KAG.' + p)[0].set(0)
    
def setCurrentAttr():
    attrName = pm.textScrollList('posesSL', q=1, selectItem=1)[0]
    try:
        pm.deleteUI('kexMix_KAGslider')
    except:
        pass

    pm.setParent('mixMainMenuBar')
    pm.attrControlGrp('kexMix_KAGslider', attribute='KAG.' + attrName)
    
    try:
        pm.textField('connCtlDriver', e=1, text='KAG.'+attrName)
    except:
        pass

def grab_values():
    m = mix()
    m.grab_values()
    
def tweaker_get():
    m = mix()
    tweaker = m.tweaker_get()
    tweaker = sorted(tweaker, key=lambda pl: str.lower(str(pl)))
    pm.textScrollList ('tweakerListsSL', e=1, removeAll=1)
    pm.textScrollList('tweakerListsSL', append=tweaker, e=1)
    
def poses_get():
    m = mix()
    poses = m.poses_get()
    poses = sorted(poses, key=lambda pl: str.lower(str(pl)))
    pm.textScrollList ('posesSL', e=1, removeAll=1)
    pm.textScrollList('posesSL', append=poses, e=1)
    
def pose_tweaker_get():
    m = mix()
    attrName = pm.textScrollList('posesSL', q=1, selectItem=1)[0]
    attr = pm.ls('KAG.' + attrName)[0]
    tweakers = m.pose_tweaker_get(attr)
    tweakers = sorted(tweakers, key=lambda pl: str.lower(str(pl)))
    pm.textScrollList ('tweakerAllSL', e=1, removeAll=1)
    pm.textScrollList('tweakerAllSL', append=tweakers, e=1)
    setCurrentAttr()
    
def pose_tweakers_select():
    m = mix()
    attrName = pm.textScrollList('posesSL', q=1, selectItem=1)[0]
    attr = pm.ls('KAG.' + attrName)[0]
    tweakers = m.pose_tweaker_get(attr)
    pm.select(tweakers)
    
def pose_set():
    m = mix()
    tweakerNames = pm.textScrollList('tweakerAllSL', q=1, allItems=1)
    tweaker = pm.ls(tweakerNames)
    attrName = pm.textScrollList('posesSL', q=1, selectItem=1)[0]
    attr = pm.ls('KAG.' + attrName)[0]
    m.pose_set(tweaker, attr)
    pose_tweaker_get()
    
def pose_set_driven():
    m = mix()
    tweakerNames = pm.textScrollList('tweakerAllSL', q=1, allItems=1)
    tweaker = pm.ls(tweakerNames)
    attrName = pm.textScrollList('posesSL', q=1, selectItem=1)[0]
    attr = pm.ls('KAG.' + attrName)[0]
    m.pose_set_driven(tweaker, attr)
    
def pose_set_scene():
    m = mix()
    tweaker = pm.ls(sl=1)
    attrName = pm.textScrollList('posesSL', q=1, selectItem=1)[0]
    attr = pm.ls('KAG.' + attrName)[0]
    m.pose_set(tweaker, attr)
    pose_tweaker_get()
    
def tweaker_set():
    m = mix()
    m.ctls_setup()
    UI()

def corrective():
    m = mix()
    attrNames = pm.textScrollList('posesSL', q=1, selectItem=1)
    if not len(attrNames) == 2:
        raise Exception('Select two attributes in the list')
    m.corrective(attrNames[0], attrNames[1])
    poses_get()
    
def pose_selected_set():
    m = mix()
    #tweakerNames = pm.textScrollList('tweakerListsSL', q=1, selectItem=1)
    tweakerScene = pm.ls(sl=1)
    tweaker = []
    for t in tweakerScene:
        if t.hasAttr('transform_ctrl_face'):
            tweaker.append(t)
    
    attrName = pm.textScrollList('posesSL', q=1, selectItem=1)[0]
    attr = pm.ls('KAG.' + attrName)[0]
    m.pose_set(tweaker, attr)
    pose_tweaker_get()
     
    
def tweaker_select():
    tweaker = pm.textScrollList('tweakerListsSL', q=1, selectItem=1)
    pm.select(tweaker)
    
def tweaker_select_pose():
    tweaker = pm.textScrollList('tweakerAllSL', q=1, selectItem=1)
    pm.select(tweaker)
    
def mirror():
    t = pm.intFieldGrp('mirror_t_KM', q=1, v=1)
    r = pm.intFieldGrp('mirror_r_KM', q=1, v=1)
    s = pm.intFieldGrp('mirror_s_KM', q=1, v=1)
    side = pm.intField('mirror_side_KM', q=1, v=1)
    print t, r, s
    m = mix()
    m.mirror(factor=[t,r,s], fromSide=side)
    
def flip():
    t = pm.intFieldGrp('mirror_t_KM', q=1, v=1)
    r = pm.intFieldGrp('mirror_r_KM', q=1, v=1)
    s = pm.intFieldGrp('mirror_s_KM', q=1, v=1)
    side = pm.intField('mirror_side_KM', q=1, v=1)
    print t, r, s
    m = mix()
    m.flip(factor=[t,r,s])
    
def side_set():
    side = pm.intField('mirror_side_KM', q=1, v=1)
    m = mix()
    m.ctls_side_set(side=side)
    
def side_match():
    m = mix()
    m.ctls_match()
    
def addPose():

    result = pm.promptDialog(
            style = 'text',
            title='Add pose',
            message='Attribute name',
            text='',
            button=['OK', 'Cancel'],
            defaultButton='OK',
            cancelButton='Cancel',
            dismissString='Cancel')
    if result == 'OK':
        output = pm.promptDialog(query=True, text=True)
    else:
        output = 'Cancel'
 
    m = mix()
    m.addPose(output)
    
    poses_get()
    
def tgl_shapes_tweaker():
    c =  bn.getAll('transform_ctrl_face')
    
    vis = 1
    if c[0].ctrl.getShape().v.get() == 1:
        vis = 0
    
    for e in c:
        try:
            e.ctrl.getShape().v.set(vis)
        except:
            pass
        
def tgl_shapes_driven():
    c =  bn.getAll('transform_ctrl_face')
    
    vis = 1
    if c[0].ctrl.getParent().getShape().v.get() == 1:
        vis = 0
    
    for e in c:
        try:
            e.ctrl.getParent().getShape().v.set(vis)
        except:
            pass
        
def connect_ctl(UIname='connectCtlUI'):
    try:
        pm.deleteUI(UIname)
    except: 
        pass
    
    pm.window(UIname, t=UIname, h=150, w=200, s=1)
            
    pm.formLayout('connCtlMainFl')
    with pm.columnLayout('connCtlMenuBar', adj=1) as mainColumn:
        pm.floatField('connCtlMin')
        pm.floatField('connCtlMax')
        pm.textField('connCtlDriver')
        pm.button(label='connect', c='import mf.tools.keyMix; mf.tools.keyMix.connect_ctl_cb()')
        
    pm.formLayout(
        'connCtlMainFl', e=True,
        af=[(mainColumn, 'left', 0), (mainColumn, 'bottom', 0),
            (mainColumn, 'right', 0), (mainColumn, 'top', 0)])
    pm.showWindow(UIname)
    
def connect_ctl_cb():
    attrNames = pm.channelBox('mainChannelBox', q=1, selectedMainAttributes=1)
    if not attrNames:
        raise Exception('Nothing selected in Channel Box')

    minV = pm.floatField('connCtlMin', q=1, v=1)
    maxV = pm.floatField('connCtlMax', q=1, v=1)
    driver = pm.textField('connCtlDriver', q=1, text=1)

    tk.drvKeySet(valList=[[minV, 0],[maxV, 1]], 
                 VattrDriven = pm.ls(driver)[0], 
                 VattrDriver= pm.ls(str(pm.ls(sl=1)[-1])+'.'+ attrNames[0])[0])


    
    
    
    


    
    

    
