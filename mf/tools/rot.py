"""
---------------------------------------------------------------------------
tools for rotation interpolation
---------------------------------------------------------------------------
"""
import pymel.core as pm
import lsSl
import snap
import parent as tp
import cons as tc
import proxyGeo as pg
import mf.tools.distance as td
import mf.tools.joint as tj
import mf.tools.key as tk

class startEndB():
    """
    Setup to interpolate the rotation on a limb. Whole limb.

    =========== =====================================================
    Arguments   Description
    =========== =====================================================
    Vlist[0:-3] list of parents for rot joints
    Vlist[-3]   start object
    Vlist[-2]   end object
    Vlist[-1]   attribute object
    Vsuffix     suffix
    midPosition index in Vlist for the position of e.g. elbow or knee
    attrName    name for the rot compensate attribute
    =========== =====================================================
    """
    def __init__(self, Vlist=[], Vsuffix='', midPosition=2, attrName='', 
                 side=2, rollJunction=1):
        # TODO: option to distribute yz scaling start-mid/ mid-end.
        self.sl = lsSl.sl(Vlist, 4, 0,
                          slDict={
                                  'parList':'0:-3',
                                  'startObj':'-3',
                                  'endObj':'-2',
                                  'attrObj':'-1'
                                  })
        
        # extra groups on top of start and end object
        startObjChild = tp.nullGroup(Vlist=[self.sl.parList[0]], Vhierarchy=1,
                                     Vtype=2, Vsuffix='_goal')[0]
        self.sl.startObj|startObjChild
        endObjChild = tp.nullGroup(Vlist=[self.sl.parList[-1]], Vhierarchy=1,
                                     Vtype=2, Vsuffix='_goal')[0]
        self.sl.endObj|endObjChild
        
        # This uses one extra joint for each in the chain to rotate 
        if rollJunction:
            parListB = tp.nullGroup(Vlist=self.sl.parList, Vhierarchy=4, Vtype=2, 
                                  Vsuffix='_ax')
        
            self.sl.attrObj.addAttr('rot', defaultValue=0, min=0, keyable=1)
            self.sl.attrObj.rot.setLocked(1)

            self.sl.attrObj.addAttr('roll_junction', defaultValue=0, keyable=1)
            
            for o in parListB:
                o.segmentScaleCompensate.set(0)
                self.sl.attrObj.roll_junction >> o.rx
            
        else:
            parListB = self.sl.parList
        
        
        self.sl.attrObj.addAttr('rot_extras', defaultValue=0, min=0, keyable=1)
        self.sl.attrObj.rot_extras.setLocked(1)
        
        midObj = parListB[midPosition-1]
     
        startMidList = parListB[0:midPosition-1]
        midEndList =parListB[midPosition-1:]

        startMidList.extend([midObj, startObjChild, self.sl.attrObj])
        midEndList.extend([endObjChild, self.sl.attrObj])
        
        self.sA = startMid (Vlist=startMidList, Vsuffix='_start%s'%(Vsuffix), 
                       limbSegment=1, attrName='soft_start%s'%(attrName),
                       rollJunction=rollJunction, side=side)
        self.sB = startMid (Vlist=midEndList, Vsuffix='_end%s'%(Vsuffix), 
                       limbSegment=2, attrName='soft_end%s'%(attrName),
                       rollJunction=rollJunction, side=side)
        
        proxyScale = td.get([Vlist[0], Vlist[-4]])*0.08
        
        # this is the elbow or knee inb.
        midInb =  tj.jointInbCons([self.sA.elbowCons, self.sB.elbowCons],
                                  proxyScale=proxyScale, side=side,
                                  label='midInb')
        scaleMidInb=1
        if scaleMidInb:
            self.sl.attrObj.addAttr('junction_volume', defaultValue=0, 
                                    min=0, max=1, keyable=1)
            blendScaleMid = pm.createNode('blendColors', 
                                          name='blendScaleMid' + Vsuffix)
            
            blendScaleMid.outputR >> midInb[0].sy
            self.sl.attrObj.junction_volume >> blendScaleMid.blender
            blendScaleMid.color2R.set (1)
            
            tk.drvKeySet([[0,1], 
                          [-22.5, 1.06], 
                          [22.5, 1.06], 
                          [-45, 1.2], 
                          [45, 1.2], 
                          [-70, 1.25], 
                          [70, 1.25]], 
                          midInb[0].rz, blendScaleMid.color1R)
        
            pm.keyTangent (midInb[0].sy, itt='spline', ott='spline')
        
        #pm.mute(midInb[0].sy)
        
        # This has to be connected since there is the parListB-chain
        # in-between the original ad the startMid setup.
        if rollJunction:
            startMidList[-2].getParent().s >> midInb[1].inverseScale 
        else:
            startMidList[-2].s >> midInb[1].inverseScale
        return


class startMid():
    """
    setup to interpolate the rotation on a limb.
    
    =========== =====================================================
    Arguments   Description
    =========== =====================================================
    Vlist[0:-1] list of parents for the rotating goal objects.
                (e.g. from hand to elbow for segment 1, or from elbow to shoulder for
                segmen 2.)
    Vlist[-1]   end or start source object (e.g. rotating wrist or chest)
    Vsuffix     suffix
    limbSegment 1=first segment, 2=seconds segment
    attrName    (optional) if given, an attribute will be created on Vlist[-1]
                this controls the interpolation to the rotating start source
    =========== =====================================================
    """
    def __init__(self, Vlist=[], Vsuffix='', limbSegment=1, attrName='', 
                 rollJunction=0, side=3):
    
        Vlist = lsSl.lsSl(Vlist, 3, 0)
    
        attrObj = Vlist.pop()
    
        pm.rename(attrObj, 'attrObj'+Vsuffix)
        attrObj.addAttr('overrot'+attrName, defaultValue=1.4, min=0.5, max=2, keyable=1)
        overrotAttr = attrObj.attr('overrot'+attrName)
    
        # first segment -----------------------------------------------------------
        if limbSegment == 1:
            
            rotLoc = Vlist[0:-2]
            for i in range(len(rotLoc)):
                pm.rename(rotLoc[i], 'rl'+str(i+1)+Vsuffix)
            endPoint = Vlist[-2]
            pm.rename(endPoint, 'ep'+Vsuffix)
            mobileObject = Vlist[-1]
            pm.rename(mobileObject, 'mo'+Vsuffix)
            
            # XXX
            tp.nullGroup(Vlist=[mobileObject], Vhierarchy=3, Vtype=1,
                         Vsuffix='_null')
            
            
            if attrName != '':
                xRot = tc.endRot(Vlist=[rotLoc[0], mobileObject, attrObj], Vsuffix='', attrName=attrName)
            else:
                xRot = tc.endRot(Vlist=[rotLoc[0], mobileObject], Vsuffix='')
            
            endPointOrient = tp.nullGroup(Vlist=[rotLoc[-1]], Vhierarchy=4, 
                                          Vtype=2, Vsuffix='_Orient', jointRadius=2)[0]
            snap.snapTrans([endPointOrient, endPoint])
            
            
            rotJntList = tp.nullGroup(Vlist=rotLoc, Vhierarchy=4, Vtype=2, 
                                      jointRadius=3, Vsuffix='_rot')
            
            for obj in rotJntList:
                obj.segmentScaleCompensate.set(0)
             
            tc.distribute(Vlist=rotJntList[1:]+[rotJntList[0],endPointOrient], 
                          Vtype=6, Voffset=0, permanent=1, skipRotate=['none','y','z'])    
    
            # overRot setup
            overRot = tp.nullGroup(Vlist=[rotJntList[0]], Vhierarchy=1, Vtype=2,
                                      jointRadius=3, Vsuffix='_over')
                                      
            overrotMult = pm.createNode('multiplyDivide', name='overrotMult%s' % (Vsuffix))
            overrotMult.operation.set (1)
            
            overRot[0].rotate >> overrotMult.input1
            overrotMult.input2.set([1.5, 1.5, 1.5])
            
            overrotMult.output >> rotJntList[0].rotate
    
            tc.cons(Vlist=[xRot[0], overRot[0]], Vtype=2, Voffset=0,
                    skipRotate=['none','y','z'])
            #
            overRot[0].setParent(Vlist[0])
            # END overRot setup
            dist = td.get([Vlist[0], Vlist[-2]])*0.15
            self.iRotCls = tc.inbRot(Vlist=[mobileObject, rotJntList[0]], Vproxy=1, 
                                   Vsuffix=Vsuffix, proxyScale=dist, 
                                   side=side, label='inbRot_first')
            
            # joint for the start that does not scale but only rotate
            sleeve = tp.nullGroup(Vlist=[rotJntList[0]], Vhierarchy=4, 
                                          Vtype=2, Vsuffix='_nonScale', 
                                          jointRadius=2, connectInverseScale=0)[0]
            
            pg.proxyGeo(Vlist=[sleeve], Vprefix='', Vname='', 
                                Vsuffix='_bnd', Vorient=1, xScale=1.1, 
                                yzScale=0, yzScaleGlobal=dist, customGeo=0, Vsides=6, VcenterPivot=1,
                                chain=0, shader=1, Vcolor=[0.2, 0.4, 0.4], shaderName='shoulder_shader',
                                side=side, label='non_scale')
            
            rotJntList[0].getParent().getParent().s >> sleeve.inverseScale
            
            
            # quick fix
            if rollJunction:
                rotLoc[0].getParent().scale >> self.iRotCls.nullRotObj.inverseScale
            else:
                rotLoc[0].scale >> self.iRotCls.nullRotObj.inverseScale

            
            self.elbowCons =  rotJntList[-1]
            self.px = pg.proxyGeo(Vlist=rotJntList+[endPointOrient], Vprefix='', Vname='', 
                                  Vsuffix='_bnd', Vorient=1, xScale=1, 
                                  yzScale=0, yzScaleGlobal=0.1, customGeo=0, Vsides=6, VcenterPivot=0,
                                  chain=1, shader=0, Vcolor=[0.4, 0.4, 0.6], shaderName='shader',
                                  side=side, label='rot_first')
            
        # second segment ----------------------------------------------------------
        if limbSegment == 2:
            
            rotLoc = Vlist[0:-2]
            for i in range(len(rotLoc)):
                pm.rename(rotLoc[i], 'rl'+str(i+1)+Vsuffix)
            endPoint = Vlist[-2]
            pm.rename(endPoint, 'ep'+Vsuffix)
            mobileObject = Vlist[-1]
            pm.rename(mobileObject, 'mo'+Vsuffix)
            
            
            tp.nullGroup(Vlist=[mobileObject], Vhierarchy=3, Vtype=1,
                         Vsuffix='_null')
            
            
            if attrName != '':
                xRot = tc.endRot(Vlist=[rotLoc[0], mobileObject, attrObj], Vsuffix='', attrName=attrName)
            else:
                xRot = tc.endRot(Vlist=[rotLoc[0], mobileObject], Vsuffix='')
            
            rotJntList = tp.nullGroup(Vlist=rotLoc, Vhierarchy=4, Vtype=2, jointRadius=3, Vsuffix='_rot')
            
            for obj in rotJntList:
                obj.segmentScaleCompensate.set(0)
                   
            tc.distribute(Vlist=rotJntList[:-1]+[Vlist[0].getParent(), rotJntList[-1]], Vtype=6, 
                          Voffset=0, permanent=1, skipRotate=['none','y','z'])
            
 
    
            # overRot setup
            overRot = tp.nullGroup(Vlist=[mobileObject], Vhierarchy=1, Vtype=2,
                                      jointRadius=3, Vsuffix='_over')
                                      
            overrotMult = pm.createNode('multiplyDivide', name='overrotMult%s' % (Vsuffix))
            overrotMult.operation.set (1)
            
            overRot[0].rotate >> overrotMult.input1
            overrotMult.input2.set([1.2, 1.2, 1.2])
            
            overrotMult.output >> rotJntList[-1].rotate
    
            tc.cons(Vlist=[xRot[0], overRot[0]], Vtype=2, Voffset=0,
                    skipRotate=['none','y','z'])
            #
            overRot[0].setParent(Vlist[-2])
            # END overRot setup
            dist = td.get([Vlist[0], Vlist[-2]])*0.15
            self.iRotCls = tc.inbRot(Vlist=[rotJntList[-1] ,mobileObject], 
                                     Vproxy=1, Vsuffix=Vsuffix, proxyScale=dist,
                                     side=side, label='inbRot_second')
    
            self.px = pg.proxyGeo(Vlist=rotJntList+[endPoint], Vprefix='', Vname='', Vsuffix='_bnd', Vorient=1, xScale=1, 
                                  yzScale=0, yzScaleGlobal=0.1, customGeo=0, Vsides=6, VcenterPivot=0,
                                  chain=1, shader=0, Vcolor=[0.4, 0.4, 0.6], shaderName='shader'
                                  ,side=side, label='rot_second')   
            self.elbowCons = rotJntList[0]
            
        if attrName != '':
            overrotAttr >> overrotMult.input2X
            overrotAttr >> overrotMult.input2Y
            overrotAttr >> overrotMult.input2Z
        
        overrotAttr.setLocked(1)
        

def straightChildJoint(Vlist=[], parentEndTo=[], Vsuffix=''):
    """
    =========== =====================================================
    Arguments   Description
    =========== =====================================================
    Vlist[0]    start
    Vlist[1]    end
    parentEndTo parent for the new joint (optional)
    =========== ===================================================== 
    """
    Vlist = lsSl.lsSl(Vlist, Vmin=2, Vmax=2)
    
    straightJoint = tp.nullGroup(Vlist=[Vlist[0]], Vhierarchy=1, Vtype=2, Vsuffix='_strt'+Vsuffix)
    snap.snapTrans(Vlist=[straightJoint[0], Vlist[1]])
    
    if len(parentEndTo) == 0:
        pm.parent(straightJoint, Vlist[1])
    else:
        pm.parent(straightJoint, parentEndTo[0])
    
    return straightJoint[0]
