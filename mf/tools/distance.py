"""
distance tools
"""
import pymel.core as pm 
import pymel.core.datatypes as dt
import mf.builder.nodes
import mf.tools.parent as tp
import lsSl
import snap


def get(Vlist=[]):
    """returns distance between two objects""" 
    sel = pm.ls(sl=1)
    result = dist(Vlist).delete()
    pm.select(sel)
    return result

class dist:
    """
    Class for distance object.
 
    ============== ==================================================================
    Arguments      Description
    ============== ==================================================================
    Vlist[0]       start object
    Vlist[1]       end object
    name           basename - if not given, Vlist[0] will be used
    Vsuffix        suffix      
    VdimensionLine line for the distance
    VdisplayInfo   if there is a line, the end objects name is displayed
    VlockAttr      locks translation, rotation and scale of start and end object
    edit           for editing select a distance tool in the scene (the child group).
                   editing also works for distances created with distance.distance
    ============== ==================================================================
    
    example::
     
        import mf.tools.distance as td
        help(td)
            
        d = td.dist() # select two objects
        d.text('customText')
        d.line(0)
        d.goals()
        d.distance()
        d.initial()     
        d.parent() # parent to e.g. main_ctrl (select one object)
        d.getParent()   
        d.calibrate(1) # calibrate current distance to 1
        d.distance()
        d.unCalibrate()
        d.lock(1)
        d.connect([pm.ls(sl=1)[0].tx])
        d.enableMax(1)
        
        d.enableLimits(0, 0)
        d.calibrate(1)
        d.setMax(o.distance())
        d.enableLimits(0, 1)
        
        d()
        d.delete()
        
        e = td.dist(edit=1) #new instance
    """
    def __init__(self, Vlist=[], edit=0, **kwdArgs):
        if edit==0:
            self.create(Vlist=Vlist, **kwdArgs)
        
        else:
            self.edit(Vlist=Vlist)
    
    def create(self, Vlist=[], Vsuffix='', name='', VdimensionLine=1, 
                 infoText='', VlockAttr=1):
        """creates the distance tool"""
        
        Vlist = lsSl.lsSl(Vlist, 2, 2)
        # naming
        if name != '':
            self.basename = name+Vsuffix
        else:
            self.basename = Vlist[0]+Vsuffix
        
        self.goalObj = Vlist
        self.start = pm.group(w=1, empty=1, n='distStart_%s' % self.basename)
        self.end = pm.group(w=1, empty=1, n='dist_%s' % self.basename)
        
        self.end.addAttr ('initial_dist', defaultValue=0, keyable=0)
        self.end.initial_dist.set(channelBox=1)
        
        snap.snap([self.start, self.end, self.goalObj[0]])
        self.end.setParent (self.start)
        
        pm.pointConstraint(self.goalObj[0], self.start)
        self.aim = pm.aimConstraint(self.goalObj[1], self.start,
            skip='x',
            offset=[0, 0, 0,],
            weight=1,
            aimVector=[1, 0, 0],
            upVector=[0, 1, 0],
            worldUpType='vector',
            worldUpVector=[0, 1, 0])
        pm.pointConstraint (self.goalObj[1], self.end)
        
        self.start.rx.set(0)
        
        self.end.initial_dist.set(pm.getAttr(self.end.tx))
        self.end.initial_dist.set(lock=1)
        self.line(VdimensionLine)
        self.text(infoText)           
        self.lock(VlockAttr) 
        self.setMax(self.distance())
        
        self.end.addAttr(mf.builder.nodes.register('transform_distance')[0])
        
    def edit(self, Vlist=[]):
        """gathers object for editing"""
        Vlist = lsSl.lsSl(Vlist, 1, 1)
        self.end = Vlist[0]
        self.start = self.end.getParent()
        
        self.aim = pm.ls(pm.aimConstraint(self.start, q=1))[0]
        consSt = pm.pointConstraint(self.start, q=1)
        consEn = pm.pointConstraint(self.end, q=1)
        self.goalObj = [pm.pointConstraint(consSt, q=1, targetList=1)[0],
                        pm.pointConstraint(consEn, q=1, targetList=1)[0]]
        self.basename = str(self.goalObj[0])
        try:
            self.VlocShape = self.start.getShape()
            self.Vannot = self.end.getShape()
        except: pass
           
    def __call__(self):
        """returns the objects of the distance tool"""
        return [self.start, self.end] 
    
    def line(self, state=1):
        """creates and deletes the visual reference line"""
        if state == 1 and self.start.getShape() == None:
            self.Vannot = pm.createNode('annotationShape', 
                                        n='not_%s' % self.basename)
            VannotTrans = self.Vannot.getParent()
            Vloc = pm.spaceLocator (name='loc%s' % self.basename)
            self.VlocShape = Vloc.getShape()
            self.VlocShape.worldMatrix[0] >> self.Vannot.dagObjectMatrix[0]
            pm.parent(self.VlocShape, self.start, shape=1, r=1)
            pm.parent(self.Vannot, self.end, shape=1, r=1)
            pm.delete(VannotTrans, Vloc)
            return [self.Vannot, self.VlocShape] 
         
        elif state == 0 and self.start.getShape() != None:
            try:
                pm.delete(self.Vannot)
                pm.delete(self.VlocShape)
            except:
                pass
    
    def setTemplate(self, val=1):
        "sets the loc and line to template"
        self.start.template.set(val) 
        
    def setLocScale(self, val=0):
        """sets the loc scale (0 is invisible)"""
        self.VlocShape.localScale.set([val, val, val])
        
    def text(self, text='distance'):
        """sets the annotation text if there is an annotation shape"""
        try: 
            self.Vannot.text.set(text) 
            return text
        except: pass
    
    def goals(self):
        """returns the goal objects"""
        return self.goalObj
        
    def distance(self):
        """returns the distance"""
        return self.end.tx.get()
    
    def initial(self):
        """returns the initial distance"""
        return self.end.initial_dist.get()
    
    def parent(self, Vlist=[], relative=1):
        """sets the parent for scaling"""
        Vlist = lsSl.lsSl(Vlist, 1, 1, VerrorStr='- select one transform node')
        return self.start.setParent(Vlist[0], relative=relative)  
        
    def getParent(self):
        """returns the parent, returns None if the parent is the world"""
        return self.start.getParent()
        
    def calibrate(self, value=1):
        """calibrates the current distance to the given value"""
        if value == 0: raise Exception('value must be greater than 0')
        self.lock(0)
        oldScale = self.start.scale.get()
        newScale = oldScale*self.end.tx.get()/value
        self.start.scale.set(newScale)
        if self.start.getShape() != None:
            try:
                locScale = self.VlocShape.localScale.get()*(oldScale[0]/
                                                            newScale[0])
                self.VlocShape.localScale.set(locScale)
            except: pass
        return newScale 
    
    def unCalibrate(self):
        """resets calibration"""
        self.lock(0)
        oldScale = self.start.sx.get()
        self.start.scale.set([1, 1, 1])
        if self.start.getShape() != None:
            try: 
                self.VlocShape.localScale.set(self.VlocShape.localScale.get()*
                                              oldScale)
            except: pass
        # return this for consistent testing
        return self.end.tx.get() 
    
    def setScale(self, val=1):
        self.lock(0)
        self.start.s.set([val, val, val])
    
    def lock(self, state=1):
        """sets lock state for the distance tool"""
        self.end.translate.setLocked(state)
        self.end.rotate.setLocked(state)
        self.end.scale.setLocked(state)
        self.end.v.setLocked(state)
    
        self.start.translate.setLocked(state)
        self.start.rotate.setLocked(state)
        self.start.scale.setLocked(state)
        
    def connect(self, attrList):
        """connects the distance to a given list of attributes"""
        for attr in attrList:
            self.end.tx >> attr
        return
    
    def align(self, Vlist=[], percent=0, parent=0):
        """
        Aligns objects to the distance line with a given percentage (0-100) of 
        the distance. 
        If parent is 1 the distance tool will be the new parent.
        """
        Vlist = lsSl.lsSl(Vlist, 1, 0)
        
        loc = tp.nullGroup(Vlist=[self.start], Vhierarchy=4, Vtype=1)
        position = (self.distance()/100)*percent
        loc[0].tx.set(position)
        
        for obj in Vlist:
            par = obj.getParent()
            if parent == 1:
                if par != self.start:
                    obj.setParent(self.start)   
            snap.snap([obj, loc[0]])
        pm.delete(loc) 
        return position
    
    def setRot(self, val=0):
        """
        Sets the x rotation of the distance tool.
        """
        self.start.r.setLocked(0)
        self.start.rx.set(val)
        self.start.r.setLocked(1)
    
    def setMax(self, value=None):
        """
        Clamps the distance to a value if no value is given current distance
        is used.
        """
        if value:
            pm.transformLimits(self.end, tx=(-1, value))
        
    def setMin(self, value=None):
        """
        Clamps the distance to a value if no value is given current distance
        is used.
        """
        if value:
            pm.transformLimits(self.end, tx=(value, 1))
        
    def enableLimits(self, min, max):
        pm.transformLimits(self.end, etx=(min, max))
        
    def delete(self):
        """ deletes the distance tool, and returns the distance"""
        d = self.distance()
        pm.delete(self.start)
        return d 
   
def distance (Vlist=[], Vsuffix='', Vpermanent=0, VdimensionLine=0, 
              VdisplayInfo=0, VlockAttr=1):   
    """
    creates:
        a distance tool.
        The tool can be parented to any hierarchy and will scale with 
        its parent. So unlike the maya distance tool it will measures 
        in object space and not in worldspace.
 
    returns:
        distance (float)
        distance attribute (string)
        distance group (string)
        distance dimension (string)
    
    ============== ==============================================================       
    Arguments      Description
    ============== ============================================================== 
    Vlist[0]       start object
    Vlist[1]       end object
    Vsuffix        placeholder - has no effect yet (name will be taken from Vlist[0])      
    Vpermanent     0 = distance tool is not permanent, only returns the distance
    VdimensionLine line for the distance
    VdisplayInfo   if there is a line, the end objects name is displayed
    VlockAttr      locks translation, rotation and scale of start and end object
    ============== ============================================================== 
    """
    
    Vlist = lsSl.lsSl(Vlist, 2, 2)

    VdistanceStart = pm.group(w=1, empty=1, n='distStart_%s' % Vlist[0])
    Vdistance = pm.group(w=1, empty=1, n='dist_%s' % Vlist[0])

    Vdistance.addAttr ('initial_dist', defaultValue=0, keyable=0)
    Vdistance.initial_dist.set(channelBox=1)

    if VdimensionLine == 1:
        Vannot = pm.createNode('annotationShape', n='not_%s' % Vlist[0])
        VannotTrans = Vannot.getParent()
        Vloc = pm.spaceLocator (name='loc%s' % Vlist[0])
        VlocShape = Vloc.getShape()

        VlocShape.worldMatrix[0] >> Vannot.dagObjectMatrix[0]
        pm.parent(VlocShape, VdistanceStart, shape=1, r=1)
        pm.parent(Vannot, Vdistance, shape=1, r=1)
        pm.delete(VannotTrans, Vloc)

    snap.snap([VdistanceStart, Vdistance, Vlist[0]])
    Vdistance.setParent (VdistanceStart)
    
    pm.pointConstraint(Vlist[0], VdistanceStart)
       
    Vaim = pm.aimConstraint(Vlist[1], VdistanceStart,
        offset=[0, 0, 0,],
        weight=1,
        aimVector=[1, 0, 0],
        upVector=[0, 1, 0],
        worldUpType='vector',
        worldUpVector=[0, 1, 0])
    pm.pointConstraint (Vlist[1], Vdistance)

    Vdist = pm.getAttr(Vdistance.tx)
    Vdistance.initial_dist.set(Vdist)
    Vdistance.initial_dist.set(lock=1)

    if VdisplayInfo == 1 and VdimensionLine == 1:
        Vannot.text.set('%s' % (Vlist[1]))
        
    if VlockAttr == 1:
        Vdistance.translate.setLocked(1)
        Vdistance.rotate.setLocked(1)
        Vdistance.scale.setLocked(1)
        Vdistance.v.setLocked(1)
    
        VdistanceStart.translate.setLocked(1)
        VdistanceStart.rotate.setLocked(1)
        VdistanceStart.scale.setLocked(1)
        
    if Vpermanent == 0:
        pm.delete (VdistanceStart)
        return [Vdist]

    return [Vdist, Vdistance.tx, VdistanceStart, Vdistance]    


class findClosest():
    def __init__(self, Vlist=[], searchRange=0.01, mirror=[1,1,1], difRange=0.01):
        """
        ============== =========================================================      
        Arguments      Description
        ============== ========================================================= 
        Vlist
        searchRange    Range for the search of matches.
        mirror         Mirror the center of the searchRange.
        difRange       Search range inside the sortedDist with the best match 
                       as center.
        ============== ========================================================= 
        
        result
        sortedDist
        """
        Vlist = lsSl.lsSl(Vlist, 2, 0)
        goals = Vlist[:-1]
        
        xform = pm.xform (Vlist[-1], q=1, ws=1, rp=1)
        xformM = [xform[0]*mirror[0], xform[1]*mirror[1], xform[2]*mirror[2]]
    
        transOrig = dt.Vector(xformM)
        distances = []
        for e in goals:
            xformG = pm.xform (e, q=1, ws=1, rp=1)
            trans = dt.Vector(xformG)
            d = transOrig.distanceTo(trans)
            if d < searchRange:
                distances.append([d, e])
        
        self.sortedDist = sorted(distances)
        #print sortedDist
        self.result = []
        if len(self.sortedDist) > 1:
            clampVal =  self.sortedDist[0][0] + difRange
            self.result.append(self.sortedDist[0])
            for e in self.sortedDist[1:]:
                if e[0] < clampVal:
                    #print e
                    self.result.append(e)
        else:
            self.result = self.sortedDist
            
        
class findMatchingPolygon():
    """
    Copares the last polygon in the list to the others and trys to find the 
    matching one. First by the pivot position then by polycount.
    Vlist -- [hay, hay, hay, needle]
    """
    def __init__(self, Vlist=[], searchRange=0.0001):
        
        Vlist = lsSl.lsSl(Vlist, 3, 0)
        self.haystack = Vlist[:-1]
        self.needle = Vlist[-1]
        self.searchRange = searchRange
        self.compare()
        
    
    def compare(self):
        self.match = None
        closest = findClosest(Vlist=self.haystack+[self.needle], 
                              searchRange=self.searchRange)
        if len(closest.result) > 0:
            matches = []
            for e in closest.result:
                matches.append(e[-1])
            counts = []
            for e in matches:
                print self.needle
                print '  ' + e.nodeName()
                counts.append([len(e.getShape().vtx), e])
                if len(e.getShape().vtx) == len(self.needle.getShape().vtx):
                    self.match = e
                    print '    ' + e.nodeName()

            
           
        
        
       
        
        

    
    
    
    
    

