import pymel.core as pm
import maya.cmds as cm
import mf.tools.general as gt
import mf.tools.lsSl as lsSl
import mf.builder.naming as bn


"""
TODO:
Shading:
-delete all non geo

-freez selective

-namespace?
-delete unused nodes
-shape name check

Rigging:
-ctrl all set
-check mirroring
-delete unused nodes after render layers
-delete icon cam
"""

class OrigShapes_remove():
    """Removes unused shapes from the scene"""
    def check(self):
        return printFoul(self.find(), 'Unused shapes found:')
    
    def find(self):
        result = []
        for e in pm.ls(type='mesh'):
            if e.isIntermediate():
                connections =  e.connections()
                if not connections:
                    result.append(e)
        return result
    
    def fix(self):
        for e in self.find():
            try:
                meshName = e.name()
                pm.delete(e)
                print 'Deleted:', meshName
            except Exception, detail:
                print detail
                
        
class POreader_remove():
    """Removes all Pointoven reader from the scene"""
    def check(self):
        return printFoul(self.find(), 'PO reader found:')
    
    def find(self):
        result = []
        for e in pm.ls(type='PO_Reader'):
                result.append(e)
        return result
    
    def fix(self):
        for e in pm.ls(type='mesh'):
            try:
                pm.delete(e, ch=1)
            except Exception, detail:
                print detail
    
    def fixInHistory(self):
        po = self.find()
        for e in po:
            outp = e.outputGeometry[0].outputs(p=1)
            inp = e.input[0].inputGeometry.inputs(p=1) 
            print '-----'
            print outp
            print inp
            if outp and inp:
                for o in outp:
                    inp[0] >> o
                    
                    pm.delete(e) 
                         
class Expressions_remove():
    """Removes all expressions from the scene"""
    def check(self):
        return printFoul(self.find(), 'Expression found:')
    
    def find(self):
        result = []
        for e in pm.ls(type='expression'):
            if not e.outputs():
                result.append(e)
        return result
    
    def fix(self):
        for e in self.find():
            try:
                pm.delete(e)
            except Exception, detail:
                print detail
                
                
class History_delete():
    """Deletes the history of all meshes."""
    def check(self):
        return printFoul(self.find(), 'History found on:')

    def find(self):
        foul = []
        for e in pm.ls(type='mesh'):
            hist = e.history()
            if len(hist) > 1:
                foul.append(e)
        return foul

    def cleanHistory(self):
        foul = self.find()
        pm.delete(foul, ch=1)

    def disconnectHistoryNodes(self):
        """
        Sometimes when deleting the construction history some nodes remain
        (groupId nodes). This function is used to disconnect those
        remaining nodes.
        """
        foul = self.find()
        for e in foul:
            hist = e.history()
            for h in hist:
                connections = h.connections(sourceFirst=1, connections=1)
                for c in connections:
                    if c[1].nodeName() == e:
                        pm.disconnectAttr(c)

    def fix(self):
        self.cleanHistory()
        self.disconnectHistoryNodes()

        
class DoubleNaming():
    """Fixes all non unique naming in scene."""
    def check(self):
        return printFoul(self.find(), 'Non unique names found:')
    
    def find(self):
        return bn.uniqueNames(pm.ls())
        
    def fix(self):
        bn.makeUnique(self.find())
        

class RenderLayers_remove():
    """Deletes all renderLayers."""
    def check(self):
        return printFoul(self.find(), 'Render Layers found:')
    
    def find(self):
        result = []
        for e in  pm.ls(type='renderLayer'):
            if not e.nodeName() == 'defaultRenderLayer':
                result.append(e)
        return result
        
    def fix(self):
        foul = self.find()
        if foul:
            pm.editRenderLayerGlobals(currentRenderLayer='defaultRenderLayer')
        for e in foul:
            try:
                pm.delete(e)
            except:
                print 'Could not delete', e

class DisplayLayers_remove():
    """Deletes all displayLayers."""
    def check(self):
        return printFoul(self.find(), 'Display Layers found:')
    
    def find(self):
        result = []
        for e in  pm.ls(type='displayLayer'):
            if not e.nodeName() == 'defaultLayer':
                result.append(e)
        return result
        
    def fix(self):
        for e in self.find():
            try:
                pm.delete(e)
            except:
                print 'Could not delete', e  
                
class Namespaces():
    """Checks for nested namespaces in the scene."""
    def check(self):
        return printFoul(self.find()[0], 'Namespaces found:')
    
    def fix(self):
        ns = self.find()[0]
        sort = sorted(ns, key=lambda variable: len(variable.split(':')))
        sort.reverse()
        for e in sort:
            try:
                pm.namespace(removeNamespace = e, mergeNamespaceWithRoot = True, force=1)
            except:
                pass
    def find(self):
        nested = []
        all = []
        allNs = pm.namespaceInfo(lon=1, r=1)
        
        for e in allNs:
            if e == 'UI' or e == 'shared':
                pass
            else:
                all.append(e)
                splits = e.split(':')
                if len(splits) > 1:
                    nested.append(e)
        
        return all, nested

class Meshes_unlock():
    """Unlocks all transform attribute on meshes and visibility for the shape
       (for shading scenes)."""
    tranformAttributes = ['t', 'r', 's', 'sh',
                          'tx', 'ty', 'tz',
                          'rx', 'ry', 'rz',
                          'sx', 'sy', 'sz',
                          'shxy', 'shxz', 'shyz',
                          'v']   
    def check(self):
        return printFoul(self.find(), 'Locked meshes found:')
    
    def fix(self):
        foul = []
        meshes = self.find()
        for m in meshes:
            for e in ['v']:
                attr = getattr(m, e)
                try:
                    attr.setLocked(0)
                except:
                    foul.append(attr)
                
            for e in self.tranformAttributes:
                
                attr = getattr(m.getParent(), e)
                try:
                    attr.setLocked(0)
                except:
                    foul.append(attr)
        return printFoul(foul, 'Could not unlock:')
        
    
    def find(self):
        foul = []
        meshes = pm.ls(type='mesh')
        meshTrans = []
        for e in meshes:
            if not e.isIntermediate():
                if self.checkLocked(e):
                    foul.append(e)
        return foul
    
    def checkLocked(self, obj):
        for e in ['v']:
            attr = getattr(obj, e)
            if attr.isLocked():
                return True
            
        for e in self.tranformAttributes:
            
            attr = getattr(obj.getParent(), e)
            if attr.isLocked():
                return True
        return False 

class NonRenderObjects():
    """Finds all objects in scene that ae no poly meshes"""
    def check(self):
        return printFoul(self.find(), 'Non render objects found:')
    
    def findMeshes(self):
        pass
        
    def find(self):
        foul = []
        trans = pm.ls(type='transform')
        for e in trans:
            shapes = e.getShapes()
            for s in shapes:
                if not s.nodeType() == 'mesh':
                    if not e.isDefaultNode():
                        if not e.nodeName() in ['persp','front','top','side',
                                                'ANIMATED_SHADER_PLUG']:
                            foul.append(e)
            if not shapes:
                foul.append(e)
        
        return foul


class controllerSet():
    """Checks controller sets"""
    def check(self):
        pass

class mirrorSetup():
    """Checks all controllers for a mirror setup"""
    def check(self):
        pass 

class Keyframes():
    """Removes all keyframes from scene."""
    def check(self):
        printFoul(self.find(), 'Keyframes found:')

    def fix(self):
        keys = self.find()
        pm.delete(keys)

    def find(self):
        return pm.ls(type='animCurve')

class ComponentShading():
    """Removes all component shading."""
    def check(self):
        return printFoul(self.hfHighlightBadShaded()[1], 'Component shading:')
    
    def fix(self):
        self.hfRepairShadingConnections(self.hfHighlightBadShaded()[1])
    
    def hfHighlightBadShaded(self):
        meshes = pm.ls(type='mesh')
        badMeshes = []
        badEngines = []
        warn = []
        instances = []
        for i in meshes:
            parent = pm.listRelatives(i,ap=1)
            if len(parent) > 1:
                instances.append(i)
        meshes = [f for f in meshes if f not in instances]
        for mesh in meshes:
            engines = pm.listConnections(mesh, type='shadingEngine')
           
            if engines != None:
                if len(engines) > 1:
                    badMeshes.append(mesh)
                    badEngines.extend(engines)
        if len(badMeshes) > 0:
            pass
        else:
            pass
        badEngines = list(set(badEngines))
        return badEngines, badMeshes
    
    def hfRepairShadingConnections(self, meshes):
        for mesh in meshes:
            selection = pm.ls(sl=1)
            shaders = pm.listConnections(mesh, t='shadingEngine')
 
            for shader in shaders:
                if shader == "initialShadingGroup":
                    shaders.remove(shader)
            if len(shaders) < 1:
                shaders.append("initialShadingGroup")
   
            conns = pm.listConnections(mesh, c=1, p=1, t='shadingEngine')
            for conn in conns:
                pm.delete(conn, icn=1)
            pm.select(mesh)    
            pm.hyperShade(assign=shaders[0])
            pm.select(selection)
                       
def printFoul(foul, message):
    if foul:
        print '----------------------------'
        print message, '('+str(len(foul))+')'
        for e in foul:
            print '   ', e
        print '----------------------------'        
    return foul

class Instances():
    """Convertes instances to objects."""
    def check(self):
        return printFoul(self.find(), 'Instances found:')
    
    def fix(self):
        nodeNames=[]
        for e in self.find():
            nodeNames.append(e.name())
             
        for e in nodeNames: 
            if cm.objExists(e): 
                dub = cm.duplicate(e, renameChildren=True)      
                cm.delete(e)
                cm.rename(dub[0], e)

    def find(self):        
        badMeshes = []
        transforms = pm.ls(type='transform')
        for t in transforms:
            shapes = t.getShapes()
            for s in shapes:
                if s.nodeType() == 'mesh':
                    if s.isInstanced():
                        badMeshes.append(t)
        
        badMeshes = sorted(badMeshes, key=lambda pl: self.sortByDag(pl))    
        badMeshes.reverse()
        return badMeshes
                                
    def sortByDag(self, obj):
        print obj.longName()
        return obj.longName()


class DeadBranches():
    """
    Finds all dead branches in a list of objects, that are NOT meshes.
    1. Find all shapes of searchType in nodes.
    2. Find all objects that are not parents of any of the searchType objects.
    Performance is better, If you only select the root node.
    """
    def check(self):
        return printFoul(self.find(), 'Dead objects in selection that are not '+
                                      'of the type: "' + self.searchType + '".')

    def fix(self):
        pm.delete(self.find())

    def find(self, Vlist=[], searchType='mesh'):

        self.searchType = searchType
        Vlist = lsSl.lsSl(Vlist, Vmin=1, Vmax=0)

        allMeshes = []
        allChildren = []

        for e in Vlist:
            children = e.getChildren(allDescendents=1)
            for c in children:
                name = c.longName()
                if not name in allChildren:

                    if c.nodeType() == self.searchType:
                        allMeshes.append(c.getParent().longName())
                    else:
                        allChildren.append(name)

        parentList = []
        for e in allChildren:
            for x in allMeshes:
                if x.startswith(e) or x == e:
                    parentList.append(e)
                    break

        result = []
        for e in allChildren:
            if not e in parentList:
                if  not e in allMeshes:
                    result.append(e)

        return result
        
class customAttributes():
    """Deletes all custom attributes on all transform nodes."""
    def fix(self):
        for e in self.find():
            try:
                e.setLocked(0)
                pm.deleteAttr(e)
            except:
                try:
                    # try to unlock connected attributes.
                    e.setLocked(0)
                    connections = e.connections(plugs=1)
                    for c in connections:
                        c.setLocked(0)
                    pm.deleteAttr(e)
                except:
                    pass

    def check(self):
        return printFoul(self.find(), 'Custom attributes found')

    def find(self):
        Vlist = pm.ls(dagObjects=1)
        Vresult=[]
        for i in range(len(Vlist)):
            VcustomAttr = pm.listAttr(Vlist[i], userDefined = 1)
            for j in range(len(VcustomAttr)):
                Vresult.append(pm.ls('%s.%s' % (Vlist[i], VcustomAttr[j]))[0])

        return Vresult
        
    

    
    
    