"""
utilities for package editing
"""
import maya.cmds as cm
import pymel.core as pm 
from mf.builder import component as cp
import mf.tools.parent as tp
from mf.tools import lsSl
from mf.tools import snap

class groupPackageUI():
    """
    Groups the components of a package.
    """	
    def __init__(self, group=1):
        
        self.packAttrName = cp.UIreturnPackageAttribute(returnAll=1)
        self.suffix = pm.textScrollList ('packListTSL', q=1, selectItem=1)
        
        self.compLists = []
        
        for p in self.packAttrName:
            try:
                self.compLists += cp.packageRead(Vsuffix=self.suffix, Vbuilt=0, 
                                                 packAttrName=[p])[1]
            except:
                pass
           
        if group:
            pm.select(cl=1)
            groups = []
            for pack in self.compLists:
                groups.append(self.group(pack))
            pm.select(groups)
                
        else:
            self.ungroup([])
            pass
   
    def hasPackageParent(self, o, pack):
        
        par = o.getParent()
        hasPar = 0
        packPar = 0
        if par:
            hasPar = 1
                
        for p in pack:
            if par == p:
                packPar = 1
                
        return [hasPar, packPar]
        
    def group(self, Vlist=[]):
        packageNames = cp.returnPackageAttribute() 
        
        grpName = 'PLACE'
        for p in packageNames:
            
            try:
                grpName = 'PLACE' + pm.ls(Vlist[0] + '.' + p)[0].get()
            except:
                pass
                
        group = pm.group(n=grpName, em=1, w=1)
        
        for o in Vlist:
            packPar = self.hasPackageParent(o, Vlist)
                          
            if packPar[0] and packPar[1]:
                #print o + ' Has package parent'
                pass
            
            if not packPar[0]:
                group|o
        return group
            
            
    def ungroup(self, Vlist=[]):
        Vlist = lsSl.lsSl(Vlist, 1, 0, ['transform'])
        
        for l in Vlist:
            shapeJointPairs = []
            parent = l.getParent()
            children = l.getChildren()
        
            for c in children:
                if c.nodeType() != 'joint':
                    
                    if parent:
                        parent|c
                    else:
                        pm.parent(c, w=1)
                        
                    pm.makeIdentity(c, apply=1, t=0, r=0, s=1, n=0)
                    
            for c in l.getChildren(allDescendents=1):  
                if c.nodeType() == 'joint':
                    shape = c.getShapes()
               
                    if shape:    
                        temp = pm.group(w=1, em=1)
                        snap.snap([temp, c])
                        l|temp
                        tp.parentShape([c, temp])
                        temp.s.set([1,1,1])
                        shapeJointPairs.append([temp,c])
                    
            pm.makeIdentity(l, apply=1, t=0, r=0, s=1, n=0)        
            
            for e in shapeJointPairs:
                
                e[1]|e[0]
                pm.makeIdentity(e[0], apply=1, t=1, r=1, s=1, n=0)
                e[0].setParent(w=1)
                tp.parentShape([e[0], e[1]])
                pm.delete(e[0])
            
            for c in children:
                if c.nodeType() == 'joint':
                    if parent:
                        parent|c
                    else:
                        pm.parent(c, w=1)
            
            pm.delete(l)
            


class mirrorPackageUI():
    """
    Mirrors the components of a package.
    """
    def __init__(self):
    
        self.packAttrName = cp.UIreturnPackageAttribute()
        
        Vsuffix = pm.textScrollList ('packListTSL', q=1, selectItem=1)
        
        self.compLists = cp.packageRead(Vsuffix=Vsuffix, Vbuilt=0, 
                                    packAttrName=self.packAttrName)[1]
        
        for o in self.compLists:
            
            t = self.mirrorTransforms(o)
            j = self.mirrorJoints(o)
            
            for k in t.keys():
                j[k] = t[k]
            
            newPack = []
            for c in o:
                newPack.append(j[c])
            pm.select(newPack)
            
            suff = pm.getAttr(str(o[0]) + '.' + str(self.packAttrName[0]))
            Vbuilder = pm.getAttr(str(o[0]) + '.builder')
            Voptions = pm.getAttr(str(o[0]) + '.options')
            if suff[-1] == 'L':
                suff = suff[:-1]+'R'
            elif suff[-1] == 'R':
                suff = suff[:-1]+'L'
            else:
                suff = suff+'_copy'
            
            Voptions = Voptions.replace(' ', '')
            print 'side=1' in Voptions
            if 'side=1' in Voptions:
                print Voptions
                Voptions = Voptions.replace('side=1', 'side=2')
                print Voptions
            
            elif 'side=2' in Voptions:
                Voptions = Voptions.replace('side=2', 'side=1')
            
                
            
            cp.packageMake(Vlist=[], Vbuilder=Vbuilder, Voptions=Voptions, 
                           Vsuffix=suff, 
                           packAttrName=self.packAttrName)
            
            cp.UIpopulatePackageList()
            
    def mirrorTransforms(self, Vlist=[], mirrorShape=1):
        """mirrors all transforms"""
        
        newOldTrans = {}
        for o in Vlist:
            if o.nodeType() == 'transform':
                
                new = pm.duplicate(o, renameChildren=1)[0]
                newOldTrans[o] = new
                
                grp = tp.nullGroup([new],  Vhierarchy=3, rot=0)[0]
                
                #  TODO: make it work for all axis 
                grp.tx.set(grp.tx.get()*(-1))
        
                new.setParent(None)
                pm.delete(grp)
                
                pm.makeIdentity(new, apply=1, t=0, r=0, s=1, n=0, jo=0)
                new.rx.set(new.rx.get()*(-1))
                new.ry.set(new.ry.get()*(-1))
                new.rz.set(new.rz.get()*(-1))
                
                if pm.menuItem('mirrorShapesCheckCB', cb=1, q=1):
                    if o.getShape():
                        tempCopy = pm.duplicate(o, renameChildren=1)[0]
                        temp = pm.group(w=1, em=1)
                        temp|tempCopy
                        pm.makeIdentity(tempCopy ,apply=1, t=1, r=1, s=1, n=0)
                        temp.sx.set(-1)
                        
                        new|tempCopy
                        pm.makeIdentity(tempCopy ,apply=1, t=1, r=1, s=1, n=0)
                        
                        pm.delete(new.getShape())
                        tp.parentShape([tempCopy, new])
                        pm.delete(temp, tempCopy)
                           
        return newOldTrans
    
    def mirrorJoints(self, Vlist=[]):
        """mirror all root joints"""
        
        joints = []
        for o in Vlist:
            if o.nodeType() == 'joint':
                joints.append(o)
        newJoints = []
        
        jointParents = tp.getParent(joints)
        tp.unParent(joints)
        
        newOldJoints = {None:None}
        
        for o in joints:
            # TODO: make it work for all axis 
            newStr = pm.mirrorJoint(o, mirrorYZ=1, mirrorBehavior=1)[0]
            new = pm.ls(newStr)[0]
            newJoints.append(new)
            newOldJoints[o] = new
            
            
            if o.getShape():
              
                temp = pm.group(w=1, em=1)
                snap.snapTrans([temp, new])    
                pm.scale(new.cv, -1, -1, -1, pivot=temp.t.get(), a=1)
                pm.delete(temp)
                
           
        newJointParents = {}
        
        for i in range(len(newJoints)):
            newJointParents[newJoints[i]] = newOldJoints[jointParents[joints[i]]]
        
        tp.reParent(jointParents)
        tp.reParent(newJointParents)
        
        return newOldJoints
        
    def rebuild(self):
        pass
        

class renameB_built():
    """
    Renames all components with the final name they will have when the 
    package is built. 
    File has to be saved before running.
    Rename action is undoable, but undo queue before that will be flushed.
    """
    def __init__(self):
        
        scene = pm.system.sceneName()
        if not scene:
            raise Exception('File not saved. Please save file. It needs to be reopened for renaming.')
        
        if cm.file(q=1, modified=1):
            raise Exception('Changes not saved. Please save file.')
        
        #Store all component names.
        Vsuffix = pm.textScrollList ('packListTSL', q=1, selectItem=1)
        packAttrNameList = cp.UIreturnPackageAttribute(returnAll=1)
        
        oldNames = []
        for packAttrName in packAttrNameList:
            packageList = cp.packageRead(Vsuffix=Vsuffix, Vbuilt=0, 
                                         packAttrName=[packAttrName])
        
            if packageList == []:
                pass
            else:
                for l in packageList[1]:
                    oldNames += l
        if oldNames == []:
            raise Exception('Found no package to rename.')
        
        oldNamesStr = []
        for o in oldNames:
            oldNamesStr.append(str(o))
        oldNames = []
       
        #Build all packages.
        cp.UIpackageEval()
        
        #Get all new names.
        newNames = []
        for packAttrName in packAttrNameList:
            packageList = cp.packageRead(Vsuffix=Vsuffix, Vbuilt=1, 
                                         packAttrName=[packAttrName])
            
            if packageList == []:
                pass
            else:
                for l in packageList[1]:
                    newNames += l
        
        newNamesStr = []
        for o in newNames:
            newNamesStr.append(str(o))
        newNames = []
        
        #Reload scene.
        pm.openFile(scene, f=1)
        
        #Rename components.
        for i in range(len(oldNamesStr)):
            pm.rename(pm.ls(oldNamesStr[i])[0], newNamesStr[i])
            
        cp.UIpopulatePackageList()  
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        





        