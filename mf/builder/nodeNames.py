import pymel.core as pm
from mf.tools import lsSl
import maya.OpenMaya as OpenMaya


class getDagOrder():
    def __init__(self):
        """
        Gets all nodes in scene order (like in the outliner)
        """
        self.pathList = []
        dagIterator = OpenMaya.MItDag(OpenMaya.MItDag.kDepthFirst,
                                      OpenMaya.MFn.kInvalid)

        dagNodeFn = OpenMaya.MFnDagNode()
        while (not dagIterator.isDone()):
            dagObject = dagIterator.currentItem()
            depth = dagIterator.depth()
            dagNodeFn.setObject(dagObject)
            name = dagNodeFn.fullPathName()
            # print '',dagNodeFn.hasUniqueName()
            # print name + ' (' + dagObject.apiTypeStr() + ') depth: ' + str(depth)
            self.pathList.append(name)
            dagIterator.next()


class renameAll():
    """
    Rename all objects in a scene by a simple naming convention.
    All transforms of a shape node will automatically be renamed.
    Also all shape nodes of a selected transform will automatically be renamed.
    """
    def __init__(self, Vlist=[], prefix='', suffix='', nodeTypes=[]):
        self.allSelNodes = lsSl.lsSl(Vlist, 0, 0)

        if not self.allSelNodes:
            self.allSelNodes = pm.ls()

        print len(self.allSelNodes)
        self.allNodes = []

        for e in self.allSelNodes:
            if self.isRenamable(e):
                self.allNodes.append(e)
        print len(self.allNodes)
        self._sortObjects()
        self.prefix = prefix
        self.suffix = suffix
        self.nodeTypes = nodeTypes

        self.shapeDict = {}
        self.transformDict = {}
        self.transformDict['group'] = []
        self.transformDict['transform'] = []

        for e in self.allNodes:
            nt = e.nodeType()
            if not nt == 'transform':
                self._addShape(e, nt)
            else:
                self._addTransform(e)

    def _sortObjects(self):
        """
        Sorts all objects by their DAG path.
        This will overwrite the selection order.
        """
        dagList = getDagOrder().pathList

        def _sortByDag(item):
            dag = len(item.longName().split('|'))
            nm = '|'.join(item.longName().split('|')[:-1])
            ind = 0
            if item.longName() in dagList:
                ind = dagList.index(item.longName())
            #print dag, nm
            return dag, ind

        # Sort py DAG path.
        self.allNodes = sorted(self.allNodes, key=_sortByDag)

    def _addParent(self, node):

        parent = node.getParent()
        if not parent in self.transformDict['transform']:
            self.transformDict['transform'].append(parent)

    def _addShape(self, node, tp):
        """
        add a shape node
        """

        if not self.nodeTypes or tp in self.nodeTypes:

            if not self.shapeDict.has_key(tp):
                self.shapeDict[tp] = [node]
            else:
                if node not in self.shapeDict[tp]:
                    self.shapeDict[tp].append(node)

            if pm.ls(node, shapes=1):
                self._addParent(node)

    def _addTransform(self, node):
        """
        add transform node.
        """
        if not self.nodeTypes or 'transform' in self.nodeTypes:

            shapes = node.getShapes()
            if shapes:
                if node not in self.transformDict['transform']:
                    if not self.nodeTypes or 'transform' in self.nodeTypes:
                        pass
                        #self.transformDict['transform'].append(node)
                for s in shapes:
                    nt = s.nodeType()
                    self._addShape(s, nt)
            else:
                if node not in self.transformDict['group']:
                    if not self.nodeTypes or 'transform' in self.nodeTypes:
                        self.transformDict['group'].append(node)

    def isRenamable(self, obj):
        try:
            pm.undo(obj.rename('LONG_TEMP_NAME'))
            return True
        except:
            return False

    def rename(self):
        """
        Rename all nodes.
        Transforms with no shapes will be named "group".
        Transforms with shapes will be names according to their shape name.
        """
        self.preexistingNames = []
        for k, v in self.transformDict.iteritems():
            for i in range(len(v)):
                self._tryRename(v[i], 'TEMP_' + str(i), 0)

        for k, v in self.shapeDict.iteritems():
            for i in range(len(v)):
                self._tryRename(v[i], 'TEMP_' + str(i), 0)

        for k, v in self.shapeDict.iteritems():
            for i in range(len(v)):
                if pm.ls(v[i], shapes=1):
                    self._tryRename(v[i], self.prefix + k + self.suffix +'Shape' + str(i))
                else:
                    self._tryRename(v[i], self.prefix + k + self.suffix + str(i))


        for k, v in self.transformDict.iteritems():
            for i in range(len(v)):
                if k == 'group':
                    self._tryRename(v[i], self.prefix + 'group' + self.suffix + str(i))

                else:
                    self._tryRename(v[i], v[i].getShape().replace('Shape', ''), 0)

        if self.preexistingNames:
            print "Preexisting names found:"
            for e in self.preexistingNames:
                print e

    def _tryRename(self, object, name, checkExisting=1):
        """
        Make sure default nodes and non deletable nodes do not raise exceptions.
        """
        if checkExisting:
            # TODO: check if name exists only amongst the "not to be renamed" objects.
            if pm.objExists(name):
                self.preexistingNames.append([object, name])

        try:
            object.rename(name)
        except:
            print 'Could not rename', object, name


class findCopyNumber():
    """
    Handles information about copy numbers for a specific name.
    """

    def __init__(self, name=''):
        self.allCopies = pm.ls(regex=name + '\d+')
        self.allNumbers = []
        for e in self.allCopies:
            name, num, count = getNameNum(e.nodeName())
            self.allNumbers.append(num)

    def getMax(self):
        if self.allNumbers:
            return max(self.allNumbers)
        else:
            return 0

    def getMissing(self):
        an = self.allNumbers
        if not an:
            return []
        result = []
        ma = max(an)
        ran = range(1, ma)
        for e in ran:
            if not e in an:
                result.append(e)
        return result

    def get(self):
        """
        Returns the next available copy number.
        """
        missing = self.getMissing()
        if missing:
            return missing[0]
        else:
            return self.getMax() + 1

        # newName = 'NAME' + '{num:0{width}}'.format(num=333, width=5)


def getNameNum(line=''):
    """
    Takes a string: "w8ww123"
    and returns a string, int: ('w8ww', 124)
    """
    resultName = None
    resultNum = None
    resultLen = 0
    xl = []
    if line:
        for e in line:
            xl.append(e)
        xlr = xl[:]
        xlr.reverse()
        nm = []
        for i in range(len(xlr)):
            try:
                int(xlr[i])
                nm.append(xlr[i])
            except:
                resultName = ''.join(xl[:len(xl)-i])
                nm.reverse()
                break
        if nm:
            resultNum = int(''.join(nm))
            resultLen = len(nm)
        else:
            resultNum = None
        return resultName, resultNum, resultLen

class UI():
    def __init__(self):
        UIname = 'rename_UI'
        print UIname
        try:
            pm.deleteUI(UIname)
        except:
            pass
        pm.window(UIname, t=UIname, h=10, w=100, s=1)
        self.mainFormA = pm.formLayout()
        self.editUI()

        self.mainFormA.redistribute(0, 0, 0, 0, 0, 1, 0.15)
        pm.showWindow(UIname)

        self.setNodeTypes()

    def editUI(self):

        pm.text(l='prefix:')
        self.preTf = pm.textField()
        pm.text(l='suffix:')
        self.suffTf =  pm.textField()

        pm.button('Refresh node types', c=pm.Callback(self.setNodeTypes))
        self.ntTf = pm.textScrollList(ams=1, dcc=pm.Callback(self.dcc))
        pm.button('Rename', c=pm.Callback(self.rename))

    def dcc(self):
        pm.textScrollList(self.ntTf, da=1, e=1)

    def getNodeTypes(self):
        #print pm.textScrollList(self.ntTf, q=1, si=1)
        return pm.textScrollList(self.ntTf, q=1, si=1)

    def setNodeTypes(self):
        sel = pm.ls(sl=1)
        sl = pm.ls()
        pm.select(sel)
        self.nodeTypes = []
        for e in sl:
            tp = e.nodeType()

            if not tp in self.nodeTypes:
                self.nodeTypes.append(tp)
        self.nodeTypes.sort()
        pm.textScrollList(self.ntTf , e=1, removeAll=1)
        pm.textScrollList(self.ntTf , append=self.nodeTypes, e=1)

    def rename(self):
        pref = pm.textField(self.preTf, q=1, text=1)
        suff = pm.textField(self.suffTf, q=1, text=1)
        x = renameAll(Vlist=[], prefix=pref, suffix=suff, nodeTypes=self.getNodeTypes())
        x.rename()