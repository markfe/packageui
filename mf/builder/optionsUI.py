from __future__ import with_statement

import inspect

import pymel.core as pm


def optionsUI():
    """
    Takes the options from the text field an displays them in a separate window.
    """
    try: 
        pm.deleteUI('optionsUI')
    except:
        pass

    try: 
        Vargs = optionsToDict()
    except: 
        return
    
    if Vargs == {}: return
    
    sortArgs = [ (k,Vargs[k]) for k in sorted(Vargs.keys())]

    strLena = 80
    with pm.window('optionsUI', h=10, w=10, resizeToFitChildren=1, s=0, toolbox=1) as win:
        
        with pm.columnLayout('op_main', adjustableColumn=1, h=1):
            pm.window('optionsUI', e=1, h=10, w=10)
            
            for i in range(len(sortArgs)):
                a = sortArgs[i][0]
                newstrLena = (int(len(str(a))*7.5))
                if newstrLena < 80: newstrLena = 80
                
                if newstrLena > strLena:
                    strLena = newstrLena
                #print strLena

            for i in range(len(sortArgs)):
                a = sortArgs[i][0]
                v = sortArgs[i][1]
                if sortArgs[i][1] == '':
                    v = "''"
                elif type(sortArgs[i][1]).__name__ == 'str':
                    
                    v = "'%s'" % (sortArgs[i][1])                  
                    
                strLen = (len(str(v))*7.7)
                if strLen < 100: strLen = 100

                with pm.rowLayout('op_argVal'+a,adjustableColumn=2, columnAttach2=('right', 'right'), nc=2,cw=[(1, strLena), (2, 100)]):
                    #pm.textField('op_arg'+str(i), text=str(a), font='boldLabelFont', ed=1, en=0, w=strLena)
                    pm.text('op_arg'+str(i), label=str(a), align='left', font='boldLabelFont')
                    pm.textField('op_val'+str(i), text=str(v), w=strLen, font='fixedWidthFont',
                                 ec='import mf.builder.optionsUI as oui; oui.setOptions()')
                    
            with pm.rowLayout('op_ buttons'+a,adjustableColumn=2, columnAttach2=('left', 'right'), nc=2,cw=[(1, strLena), (2, 100)]):
                pm.button('op_apply', l='get', w=strLena, c='import mf.builder.optionsUI as oui; oui.optionsUI()')
                pm.button('op_get', l='set', w=20, c='import mf.builder.optionsUI as oui; oui.setOptions()')

    win.show()
    return

def setOptions():
    """
    Sets the options field in the componentUI.
    """
    optionsStr = []
    
    x = 0
    i = 0
    while x == 0:
        
        try:
            pa = []
            pa.append(pm.text('op_arg'+str(i), q=1, label=1))
            pa.append(pm.textField('op_val'+str(i), q=1, text=1))
            optionsStr.append(pa)
            i = i+1
        except:
            x = 1
            pass
   
    optionsOutStr = argListToString(optionsStr)
            
    try: pm.textField ('optionsTF', e=1, text=str(optionsOutStr))
    except: pass

    return

def argListToString(optionsStr):
    """
    Converts a list of arg value lists into a string.
    """
    
    optionsOut = []
    optionsOutStr = ''
    
    for i in range(len(optionsStr)):
        optionsOut.append(optionsStr[i][0] + '=' + optionsStr[i][1])
        
    for i in range(len(optionsOut)):
        optionsOutStr = optionsOutStr + optionsOut[i]
    
        if i != len(optionsOut)-1:
            optionsOutStr = optionsOutStr  + ', '
    return optionsOutStr

def optionsToDict():
    """
    Returns a dictionary with the arguments from the options field.
    """    
    Voptions = pm.textField ('optionsTF', q=1, text=1)
    result = dict([])
    
    g = eval("lambda " + Voptions + ":dummy")
    args = inspect.getargspec(g)

    try:
        for i in range(len(args[3])):
            result[args[0][i-len(args[3])]] = args[3][i]     
    except:
        pass

    try:
        if args[3] != None:
            for i in range((len(args[0])-(len(args[3])))):
                result[args[0][i]] = 'SET VALUE'
        else:
            for i in range((len(args[0]))):
                result[args[0][i]] = 'SET VALUE'
    except:
        pass
    
    return result

  
