"""
tools for creating and reading rig packages

====================== ================================================================
naming convention:
=======================================================================================
component              a transform node that is part of package.
package                a set of components that will build some rig part (e.g. an eye).
package Attribute Name works like a namespace for packages
template               a file containing one or more packages.
template info          a info text file in the same folder as template.
rig                    packages connected by a final module to make a complete rig.
====================== ================================================================

for the use of suffixes see mf.builder.namingExample  

file structure:
there must be a 'templates' folder under the current path.
"""
from __future__ import with_statement

import os.path, pkgutil
import subprocess
import inspect
import re

import maya.cmds as cm
import pymel.core as pm
import pymel.core.nodetypes as nt
from pymel import versions

import mf
import mf.builder.main as bm
import mf.builder.nodes as bn
import mf.builder.functionPicker
import mf.tools.lsSl as lsSl
import mf.tools.file as tf
import mf.tools.cons as tc
import mf.tools.general as gt
import mf.tests.all as ta
import naming

class componentError(Exception): pass

class wrongArgumentsError(componentError): pass
    
class noSuffixError(componentError): pass

class nonUniqueSuffixError(componentError): pass
"""
multiple suffixes on objects
"""
class nonUniqueNameError(componentError): pass
"""
objects with the same name
"""
class nonUniquePackageError(componentError): pass
"""
multiple packages with the same name
"""
class UIinputError(componentError): pass

class quickEvalError(componentError): pass
"""
quick eval fails
"""

def returnDefaultPath():
    relPath = bm.getRelativePath()
    for i in range(2):
        relPath = os.path.split(relPath)[0]
    
    pathlist = []
    try:
        pathlist = tf.readInfo(os.path.abspath(relPath + '/settings_private.txt'), 'default paths:')
    except:
        try:
            pathlist = tf.readInfo(os.path.abspath(relPath + '/settings.txt'), 'default paths:')
        except:
            pass
        pass
    
    for p in pathlist:
        try:
            tf.checkPath(p)
            return p
        except: 
            pass
    
    noPathInfo = 'No default local path found - Please set in: '+relPath+ '\\settings.txt'
    return noPathInfo

def UIsetDefPath():
    defaultPath = returnDefaultPath()
    pm.textField('pathTF', e=1, text=defaultPath)
    UItemplateReadFiles()
    
def UIsetTestPath():
    pm.textField('pathTF', e=1, text=os.path.abspath(ta.returnTestpath()))
    UItemplateReadFiles()

def UI_on_startup(dock=1, default_function='mf'):
    """
    Use this in your userSetup to have the UI opened on startup:
    
    >>> import mf.builder.component; mf.builder.component.UI_on_startup(1)
    """
    cm.scriptJob(runOnce=1,
                 event=("NewSceneOpened","import mf.builder.component; mf.builder.component.UI("+
                        str(dock)+", '"+default_function+"')"))
    
    
def UI(dock=1, default_function='mf'):
    """
    main UI
    """
    bm.sourceAllMel()
    #mf.builder.functionPicker.UI()    
    exec ('import %s' % ('mf'))
    func = eval('mf')
    
    mf.builder.functionPicker.getListerItems(func)
    
    
    try:
        cm.deleteUI('packageUI_doc')
        pass
    except:
        pass
    
    try:
        pm.deleteUI('packageUI')
    except: 
        pass

    pm.window('packageUI', h=10, w=10, resizeToFitChildren=1, s=1, toolbox=0, t='packageUI  ' + mf.__version__,
                restoreCommand='import mf.builder.component as tc; tc.UIpopulatePackageList()')
            
    pm.formLayout('big', h=220, w=10)
    with pm.paneLayout('MainPane', configuration="vertical2", paneSize=(2, 100, 0)):
        mf.builder.functionPicker.UI(default_function)
        pm.formLayout('main', h=10, w=10)
        mainLayout(default_function)
    
    docking(dock)
    
    pm.formLayout(
        'big', e=True,
        af=[('MainPane', 'left', 0), ('MainPane', 'bottom', 0), ('MainPane', 'top', 0), ('MainPane', 'right', 0)])
    
    if not dock:
        pm.showWindow('packageUI')
    
    pm.namespace(set=':')

def docking(doc):
    
    if versions.current() >= versions.v2011 and doc:
        try:
            cm.deleteUI('packageUI_doc')
            pass
        except:
            pass   
        allowedAreas = ['right', 'left']
        cm.dockControl('packageUI_doc', l='packageUI  ' + mf.__version__,
                       area='right', content='packageUI', allowedArea=allowedAreas, 
                       fl=0, w=340)
    
def mainLayout(default_function):
    relPath = bm.getRelativePath()
    defaultPath = returnDefaultPath()
    
    with pm.rowLayout('mainCLO',adjustableColumn=2, columnAttach2=('left', 'right'), 
                      nc=2, cw=[2, 1]):
        
        with pm.rowColumnLayout('compUIicons', numberOfColumns=6):
            pm.symbolButton( image=relPath+'\\UI_icons\\tglFP.png', 
                             c='import mf.builder.component as tc; tc.UItglExpand()',
                             ann='Toggle Function Picker')
            pm.symbolButton( image=relPath+'\\UI_icons\\refresh.png', 
                             c='import mf.builder.component as tc; tc.UIpopulatePackageList(); tc.UItemplateReadFiles()',
                            ann='Refresh UI')
            pm.symbolButton( image=relPath+'\\UI_icons\\FPclearComp.png', 
                             c='import mf.builder.component as tc; tc.UIclearSelection()',
                             ann='Clear Component List')
            pm.symbolButton( image=relPath+'\\UI_icons\\FPclearBuilder.png', 
                             c='import mf.builder.component as tc; tc.UIclearBuilder()',
                             ann='Clear Suffix, Builder and Options')
            pm.symbolButton( image=relPath+'\\UI_icons\\FPclearAll.png', 
                             c='import mf.builder.component as tc; tc.UIclear()',
                             ann='Clear all Inputs')
            
        pm.textField('pathTF', text=defaultPath, w=1, 
                     enterCommand='import mf.builder.component as tc; tc.UIpopulatePackageList(); tc.UItemplateReadFiles()')
        

        
    with pm.menuBarLayout('mainMenuBar', p='main'):
        
        with pm.menu( label='File',  tearOff=True):
            pm.menuItem( label='browse', 
                      c='import mf.builder.component as tc; tc.UItempFileBrowser()')
            pm.menuItem( label='set to default path', 
                      c='import mf.builder.component as tc; tc.UIsetDefPath()')
            pm.menuItem( label='set to test path',
                      c='import mf.builder.component as tc; tc.UIsetTestPath()')
            
            pm.menuItem( divider=True )
            pm.menuItem( label='open in explorer', 
                      c='import mf.builder.component as tc; tc.UIopenExplorer()')
        
        with pm.menu( label='Tools',  tearOff=True):
            pm.menuItem( label='snap...', 
                      c='import mf.tools.snap as ts; ts.snapUI()')
            pm.menuItem( label='constraint...', 
                      c='import mf.tools.cons as tcs; tcs.consWin()')
            pm.menuItem( label='parent...', 
                      c='import mf.tools.parent as tp; tp.nullWin()')
            pm.menuItem( label='corrective...', 
                      c='import mf.tools.corrective as tc; tc.UI()')
            pm.menuItem( label='skin...', 
                      c='import mf.tools.skinUI as sui; sui.UI()')
            pm.menuItem( label='key mix...', 
                      c='import mf.tools.keyMix as km; km.UI()')
            pm.menuItem(label='rename...',
                        c='import mf.builder.nodeNames; mf.builder.nodeNames.UI()')
            pm.menuItem(label='costume...',
                        c='import mf.rigs.character.gumball.costume; mf.rigs.character.gumball.costume.UI()')
            
    
        with pm.menu( label='Help',  tearOff=True):
            pm.menuItem( label='general', 
                      c="import pymel.core as pm; pm.showHelp('" +
                      os.path.split(os.path.split(relPath)[0])[0].replace("\\", "/")+ 
                      "\\docs\\html\\index.html', a=1)")
            pm.menuItem( label='mirror pose', 
                      c="import pymel.core as pm; pm.showHelp('" +
                      os.path.split(os.path.split(relPath)[0])[0].replace("\\", "/")+ 
                      "\\docs\\html\\mirrorPose.html', a=1)")
            pm.menuItem( label='playblast', 
                      c="import pymel.core as pm; pm.showHelp('" +
                      os.path.split(os.path.split(relPath)[0])[0].replace("\\", "/")+ 
                      "\\docs\\html\\playblast.html', a=1)")
    

    with pm.tabLayout('mainTLO', 
            changeCommand='import mf.builder.component as tc; tc.UIpopulatePackageList(); tc.UItemplateReadFiles()',
            p='main'): 
         
        UIpackagesFL(default_function)
        UItemplatesFL()
        
        #UIdeformerFL()
                        
    pm.formLayout(
        'main', e=True,
        af=[('mainTLO', 'left', 0), ('mainTLO', 'bottom', 0), ('mainTLO', 'top', 0), ('mainTLO', 'right', 0),
            ('mainCLO', 'left', 0), ('mainCLO', 'bottom', 0), ('mainCLO', 'top', 0), ('mainCLO', 'right', 0),
            ('mainMenuBar', 'left', 0), ('mainMenuBar', 'bottom', 0), ('mainMenuBar', 'top', 0), ('mainMenuBar', 'right', 0)
            ],
             
        # if controls are not attached they can not accessed                 
        ac=[('mainCLO', 'bottom', 0, 'mainTLO'),
            ('mainMenuBar', 'bottom', 0, 'mainCLO')
            ], 
                                    
        ap=[('mainCLO', 'right', 0, 100),
            ('mainCLO', 'top', 22, 0),
            ('mainTLO', 'top', 49,0),
            ('mainMenuBar', 'top', 0,0)
            ])
    
    UIpopulatePackageList()
    UItemplateReadFiles()
    pm.setFocus('builderTF')
    
    
    return


def UIpackagesFL(default_function):
    
    with pm.formLayout('packages', h=10, w=10):
        
        with pm.menuBarLayout('menuA'):
            
            with pm.menu( label='Packages', tearOff=True ):
                
                pm.menuItem(label='create', 
                          ann='creates a package from the objects in the list',
                          c='import mf.builder.component as tc; tc.UIpackageMake()')
                pm.menuItem( divider=True )
                pm.menuItem( label='delete', 
                                 c='import mf.builder.component as tc; tc.UIpackageDelete()')
                pm.menuItem('deleteObjectsCheckCB', label='keep objects', checkBox=False)
                pm.menuItem( divider=True )
                pm.menuItem( label='mirror',
                          c='import mf.builder.util as tu; reload(tu); tu.mirrorPackageUI()')
                pm.menuItem('mirrorShapesCheckCB', label='mirror Shapes', checkBox=True)
                pm.menuItem( divider=True )
                
                with pm.menuItem( label='group', subMenu=True):
                    pm.menuItem( label='placement group',
                              c='import mf.builder.util as tu; tu.groupPackageUI(1)')
                    pm.menuItem( label='ungroup/freeze scale',
                              c='import mf.builder.util as tu; tu.groupPackageUI(0)')
                
                with pm.menuItem( label='rename', subMenu=True):
                    pm.menuItem( label='final names',
                                c='import mf.builder.util as tu; tu.renameB_built()')
                
                pm.menuItem( divider=True)
                pm.menuItem( label='export as template', 
                                 c='import mf.builder.component as tc; tc.UIpackageExport()')

                pm.menuItem( divider=True )
                pm.menuItem(label='build', 
                          ann='build selected packages. (builds all if nothing selected)',
                          c='import mf.builder.component as tc; tc.UIpackageEval()')
                pm.menuItem('suffixCheckCB', label='suppress naming errors', checkBox=True)
            
            with pm.menu( label='Show', tearOff=True ):
                pm.menuItem('lsFinalCB', label='"rig" packages', checkBox=False, 
                                 c='import mf.builder.component as tc; tc.UIbuiltCBchange()')
                
                pm.menuItem('lsBiltCB', label='ready built', checkBox=False, 
                                 c='import mf.builder.component as tc; tc.UIbuiltCBchange()')
                
            with pm.menu( label='Components', tearOff=True ):
                pm.menuItem(label='load selection', 
                         c='import mf.builder.component as tc; tc.UIpopulatefromSelection()')
                pm.menuItem(label='clear list', 
                         c='import mf.builder.component as tc; tc.UIclearSelection()')
                pm.menuItem(label='insert selection', 
                         c='import mf.builder.component as tc; tc.UIpopulateInsert()')
                pm.menuItem(label='replace with selection', 
                         c='import mf.builder.component as tc; tc.UIpopulateReplace()')
                pm.menuItem( divider=True)
                pm.menuItem(label='move up', 
                         c='import mf.builder.component as tc; tc.UIcomponentsMove(1)')
                pm.menuItem(label='move down', 
                         c='import mf.builder.component as tc; tc.UIcomponentsMove(0)')
                pm.menuItem(label='remove', 
                         c='import mf.builder.component as tc; tc.UIcomponentsRemove()')
                pm.menuItem( divider=True)
                pm.menuItem(label='clean objects', 
                         c='import mf.builder.component as tc; tc.UIcustomAttrDelete()')
                pm.menuItem( divider=True)
                pm.menuItem('selectComponentsCB', label='auto select', checkBox=True)
            
            with pm.menu( label='Builder', tearOff=True ):
                pm.menuItem(label='execute', 
                          ann='build selected packages. (no suffix checking)',
                          c='import mf.builder.component as tc; tc.UIpackageEvalQuick()')
                pm.menuItem(label='print',    
                    c='import mf.builder.component as tc; tc.UIprintFunction()')
                pm.menuItem(label='find...',    
                    c='import mf.builder.component as tc; tc.UIFpRebuild()')
                
            with pm.menu( label='Options', tearOff=True ):
                pm.menuItem(label='load',
                        c='import mf.builder.component as tc; tc.UIoptionsGet()')
                pm.menuItem(label='edit...',
                        c='import mf.builder.optionsUI as oui; oui.optionsUI()')
                
        pm.textScrollList('packListTSL',ann='package list',allowMultiSelection=1,height=10, w=102,
                       doubleClickCommand='import mf.builder.component as tc; tc.UIpopulateComponentList()',
                       dkc='import mf.builder.component as tc; tc.UIpackageDelete()')
                        
        pm.textScrollList('compListTSL',ann='component list',allowMultiSelection=1,height=10, w=102,
                       doubleClickCommand='import mf.builder.component as tc; tc.UIselectComponentsDoubbleClick()',
                       dkc='import mf.builder.component as tc; tc.UIcomponentsRemove()',
                       selectCommand='import mf.builder.component as tc; tc.UIselectComponents()')
        
        with pm.columnLayout('ColumnA', adjustableColumn=1):
            pm.text (l='suffix', w=40, align='left')
            pm.textField('suffixTF', text='', w=40, font='fixedWidthFont')
    
        with pm.columnLayout('ColumnB', adjustableColumn=1):
            pm.text (l='builder', w=60, align='left')
            pm.textField('builderTF', text=default_function, w=60, font='fixedWidthFont',
                      ann='press enter to list contents',
                      enterCommand='import mf.builder.component as tc; tc.UIFpRebuild()')

        with pm.columnLayout('ColumnC', adjustableColumn=1): 
            pm.text (l='options', w=60, align='left')
            pm.textField('optionsTF', text='', w=1, font='fixedWidthFont',
                          enterCommand='import mf.builder.optionsUI as oui; oui.optionsUI()')
                 
        with pm.columnLayout('ColumnD', adjustableColumn=1):
            pm.button (l='build', w=40,
                    c='import mf.builder.component as tc; tc.UIpackageEval()')
        
        with pm.columnLayout('ColumnE', adjustableColumn=1): 
            pm.button (l='execute', w=40,
                    c='import mf.builder.component as tc; tc.UIpackageEvalQuick()')

    offset = 21
    #offset = 0              
    pm.formLayout(
        'packages', e=True,
        af=[('compListTSL', 'left', 0), ('compListTSL', 'bottom', 0), ('compListTSL', 'top', 27), ('compListTSL', 'right', 0),
            ('packListTSL', 'left', 0), ('packListTSL', 'bottom', 0), ('packListTSL', 'top', 27), ('packListTSL', 'right', 0),
            ('menuA', 'left', 0), ('menuA', 'top', 7), ('menuA', 'right', 0),
            ('ColumnC', 'left', 0), ('ColumnC', 'bottom', 0), ('ColumnC', 'right', 0),
            ('ColumnA', 'left', 0), ('ColumnA', 'bottom', 0), ('ColumnA', 'right', 0),
            ('ColumnB', 'left', 0), ('ColumnB', 'bottom', 0), ('ColumnB', 'right', 0),
            ('ColumnD', 'left', 0), ('ColumnD', 'bottom', 0), ('ColumnD', 'right', 0),
            ('ColumnE', 'left', 0), ('ColumnE', 'bottom', 0), ('ColumnE', 'right', 0)
            ],
        
                   
        ac=[('compListTSL', 'left', 8, 'packListTSL'),
            ('ColumnB', 'left', 8, 'ColumnA'),
            ('ColumnE', 'left', 8, 'ColumnD')
            ],
                       
        ap=[('packListTSL', 'right', 1, 33),
            ('packListTSL', 'bottom', 90+offset, 100),
            ('compListTSL', 'bottom', 90+offset, 100),

            ('ColumnA', 'right', 1, 33),
            ('ColumnA', 'bottom', 48+offset, 100),
            ('ColumnB', 'bottom', 48+offset, 100),
            
            ('ColumnC', 'bottom', 10+offset, 100),
            
            ('ColumnD', 'right', 1, 33),
            ('ColumnD', 'bottom', 0, 100),
            ('ColumnE', 'right', 1, 100),
            ('ColumnE', 'bottom', 0, 100)
            ])
    return

def UItemplatesFL():
    
    with pm.formLayout('templates', h=10, w=10):
        
        
        with pm.menuBarLayout('menuB'):
            
            with pm.menu( label='Edit', tearOff=True, allowOptionBoxes=True):

                pm.menuItem( label='delete selected', 
                                 c='import mf.builder.component as tc; tc.UItemplateDelete()')
                pm.menuItem( label='import selected', 
                                 c='import mf.builder.component as tc; tc.UIpackageImport()')
                #menuItem( divider=True )
                #menuItem( label='check templates', 
                #                 c='import mf.tests.package_check as pc; pc.UI(); pc.UIrunCheck()')
                #menuItem( optionBox=True,
                #          c='import mf.tests.package_check as pc; pc.UI()')
                
            with pm.menu( label='Check', tearOff=True ):
                
                pm.menuItem( label='check selected', 
                                 c='import mf.tests.package_check as pc; pc.UIrunCheck()')
                pm.menuItem('incrCB', label='incremental save', checkBox=True)
                pm.menuItem('meshOnlyCB', label='show mesh only', checkBox=False)
                
                pm.menuItem( divider=True )
                with pm.menuItem( label='import', subMenu=True ):
                    pm.menuItem( label='mesh', 
                                 c='import mf.tests.package_check as pc; pc.UIimportRun(part="mesh")')
                    pm.menuItem( label='weights', 
                                 c='import mf.tests.package_check as pc; pc.UIimportRun(part="weights")')
                    pm.menuItem( label='anim', 
                                 c='import mf.tests.package_check as pc; pc.UIimportRun(part="anim")')
                    pm.menuItem( label='new cam', 
                                 c='import mf.tests.package_check as pc; pc.importTempCam()')
            
                with pm.menuItem( label='export', subMenu=True ):
                    pm.menuItem( label='mesh', 
                                 c='import mf.tests.package_check as pc; pc.UIexportRun(part="mesh")')
                    pm.menuItem( label='weights',
                                 c='import mf.tests.package_check as pc; pc.UIexportRun(part="weights")')
                    pm.menuItem( label='anim',
                                 c='import mf.tests.package_check as pc; pc.UIexportRun(part="anim")')
                    #pm.menuItem( divider=True )
                    pm.menuItem( label='cam',
                                 c='import mf.tests.package_check as pc; pc.UIexportRun(part="cam")') 
                pm.menuItem( divider=True )
                pm.menuItem( label='show blast', c='import mf.tests.package_check as pc; pc.UIshowBlast()') 
                
                
            
            with pm.menu( label='Notes', tearOff=True):
                pm.menuItem( label='delete', 
                                 c='import mf.builder.component as tc; tc.UTemplateIinfoDelete()')
                pm.menuItem( label='save', 
                                 c='import mf.builder.component as tc; tc.UItemplateInfoWrite()')

                
        with pm.paneLayout('templateTSL', configuration="vertical2", paneSize=(2, 40, 60)):
            pm.textScrollList('templateTSL', allowMultiSelection=1, height=20, w=100,
                           ann='template list -- double click to import',
                           doubleClickCommand='import mf.builder.component as tc; tc.UIpackageImport()',
                           selectCommand='import mf.builder.component as tc; tc.UItemplateInfoRead()')    
            pm.scrollField('infoSCF', ann='Notes -- press Enter to check for online help', wordWrap=1,
                        enterCommand='import mf.builder.component as tc; tc.infoOpenUrl()')

        with pm.columnLayout('ColumnTempA', adjustableColumn=1):
            pm.button (l='import selected', w=60, 
                    c='import mf.builder.component as tc; tc.UIpackageImport()')
                    #c='import mf.builder.component as tc; tc.UItemplateReadFiles()')
        
        with pm.columnLayout('ColumnTempB', adjustableColumn=1):
            pm.separator(h=20, st='none')
                 
    pm.formLayout(
        'templates', e=True,
        af=[('templateTSL', 'left', 0), ('templateTSL', 'bottom', 0), ('templateTSL', 'top', 27), ('templateTSL', 'right', 0),
            ('ColumnTempA', 'left', 0), ('ColumnTempA', 'bottom', 0), ('ColumnTempA', 'right', 0),
            ('ColumnTempB', 'left', 0), ('ColumnTempB', 'bottom', 0), ('ColumnTempB', 'right', 0),
            ('menuB', 'left', 0), ('menuB', 'top', 7), ('menuB', 'right', 0),
            ],
                    
        ac=[('ColumnTempB', 'left', 8, 'ColumnTempA'),
           ],
                       
        ap=[('templateTSL', 'right', 0, 100),
            ('templateTSL', 'bottom', 24, 100),
            
            ('ColumnTempA', 'right', 0, 40),
            ('ColumnTempA', 'bottom', 0, 100),
            ('ColumnTempB', 'bottom', 0, 100),
            ])
    return

def UIdeformerFL():
    """
    TODO: deformer UI
    export mesh to characters folder
    export weight to characters folder
    
    export under meshname for each mesh
    make versions
    
    show list for selected character
    show list for all characters
    
    import for character
        import for mesh
            import versions
    """
    pm.formLayout('deformer')


def UIFpRebuild():
    inpStr = pm.textField ('builderTF', q=1, text=1)
    mf.builder.functionPicker.UI(inpStr)
   
    paneW = pm.paneLayout('MainPane', q=1, paneSize=1)
    if paneW[2] >= 99:
        UItglExpand()


def UItglExpand():
    dock = 1
    try:
        dockW = cm.dockControl('packageUI_doc', q=1, w=1)
    except:
        dockW = cm.window('packageUI', q=1, w=1)
        dock = 0
        
    paneW = pm.paneLayout('MainPane', q=1, paneSize=1)
    result = dockW*(paneW[2]*0.01)
    if dock:
        if not paneW[2] >= 99:
            cm.dockControl('packageUI_doc', e=1, w=result)
            pm.paneLayout('MainPane', e=1, paneSize=(2, 100, 0))
           
        else:
            dockW = dockW +2
            newFpW = 280.0
            x=(newFpW+dockW)
            pa = int((dockW/x)*100.0)
            pb = int((newFpW/x)*100.0)
            
            cm.dockControl('packageUI_doc', e=1, w=x)
            pm.paneLayout('MainPane', e=1, paneSize=(2, pa, pb+1))
    else:
        if not paneW[2] >= 99:
            cm.window('packageUI', e=1, w=result)
            pm.paneLayout('MainPane', e=1, paneSize=(2, 100, 0))
           
        else:
            dockW = dockW +2
            newFpW = 280.0
            x=(newFpW+dockW)
            pa = int((dockW/x)*100.0)
            pb = int((newFpW/x)*100.0)
            
            cm.window('packageUI', e=1, w=x)
            pm.paneLayout('MainPane', e=1, paneSize=(2, pa, pb+1))
     
    
def UIopenExplorer():
    path = pm.textField('pathTF', q=1, text=1)    
    subprocess.Popen('explorer ' + os.path.abspath(path))


def UIoptionsGet(Vhelp=1):
    """
    Gets a list of the arguments of the given function and sets the 
    options field. 
    The arguments 'Vlist' and 'Vsuffix' are filtered out.
    """
    Vbuilder = pm.textField ('builderTF', q=1, text=1)
    if Vbuilder == '':
        raise UIinputError ('no builder defined')
        
    defArgs = []
    defArgsDef = []
    nonDefArgs = []
    
    # import and reload the function
    # TODO: needs cleaning
    try:
        exec ('import %s' % (Vbuilder))
        exec  ('reload (%s)' % (Vbuilder))
    except:
        pass

    Vsplit = Vbuilder.split('.')

    Vsplit.pop()
    try:
        Vmod=Vsplit[0]
    except:
        print inspect.getdoc(eval(Vbuilder))
        return

    for j in range(len(Vsplit)-1):
        Vmod=Vmod+'.'+Vsplit[j+1]
        
    exec ('import %s' % (Vmod))
    exec  ('reload (%s)' % (Vmod))
    
    Vfunc = eval(Vbuilder)
    
    try:    
        agsp = inspect.getargspec(Vfunc)
    except:
        try:
            # If its a class look for the __init__ function.
            Vfunc = eval(Vbuilder + '.__init__')
            agsp = inspect.getargspec(Vfunc)
        except:
            exec ('import %s' % (Vbuilder))
            exec  ('reload (%s)' % (Vbuilder))
            print inspect.getdoc(eval(Vbuilder))
            return

    if Vhelp ==1:
        print inspect.getdoc(Vfunc)
    
    # skip if there are no default values
    if agsp[3] != None:
        for i in range(len(agsp[3])):
            # put string attributes into quotes
            if type(agsp[3][i]).__name__ == 'str':
                defArgs.append("'%s'" % (agsp[3][i]))
            else:
                defArgs.append(agsp[3][i])
    
    # get all args            
    for i in range(len(agsp[0])-len(defArgs)):
        if agsp[0][0] == 'Vlist' or agsp[0][0] == 'Vsuffix' or agsp[0][0] == 'self':
            agsp[0].pop(0)
        else:
            nonDefArgs.append(agsp[0].pop(0))
        
    for i in range(len(agsp[0])):
        if agsp[0][i] == 'Vlist' or agsp[0][i] == 'Vsuffix' or agsp[0][i] == 'self':
            pass
        else:
            defArgsDef.append(str(agsp[0][i]) + '=' + str(defArgs[i]))
    
    optionsOut = nonDefArgs + defArgsDef
    optionsOutStr = ''
    
    for i in range(len(optionsOut)):
        optionsOutStr = optionsOutStr + optionsOut[i]
    
        if i != len(optionsOut)-1:
            optionsOutStr = optionsOutStr  + ', '

    pm.textField ('optionsTF', e=1, text=str(optionsOutStr))
    return

def returnPackageAttribute(): 
    return ['package', 'package_rig']

def UIreturnPackageAttribute(returnAll=0):
    
    packAttrName = []
    packAttrNameList = returnPackageAttribute()
    
    if returnAll == 1:
        return packAttrNameList
    
    cb = pm.menuItem('lsFinalCB', cb=1, q=1)
    if cb == 1:
        packAttrName.append(packAttrNameList[1])
    else: 
        packAttrName.append(packAttrNameList[0])
    
    return packAttrName
    
def UIbuiltCBchange():
    UIpopulatePackageList()
    #Vbuilt = checkBox('lsBiltCB', q=1, v=1)
    Vbuilt = pm.menuItem('lsBiltCB', cb=1, q=1)
    if Vbuilt == 1:
        
        pm.textField('suffixTF', e=1, editable=0)
        pm.textField('builderTF', e=1, editable=0)    
        pm.textField('optionsTF', e=1, editable=0)

    else:
        pm.textField('suffixTF', e=1, editable=1)
        pm.textField('builderTF', e=1, editable=1)    
        pm.textField('optionsTF', e=1, editable=1)
        
    return

def UIcustomAttrDelete():
    Vlist = UIobjectsFromTSL('compListTSL')
    customAttributesDel(Vlist)
    UIpopulatePackageList() 
    return

def UIpackageEval():
    packAttrName = UIreturnPackageAttribute() 
    Vsuffix = pm.textScrollList ('packListTSL', q=1, selectItem=1)
    suppress = pm.menuItem('suffixCheckCB', cb=1, q=1)
      
    if Vsuffix == []:
        packAttrName = UIreturnPackageAttribute(returnAll=1)
    
        packageEvalList(packAttrName=packAttrName, suppressSuffixError=suppress)
    
    else:
        packAttrName = UIreturnPackageAttribute(returnAll=0)
        packageEval(Vsuffix=Vsuffix, packAttrName=packAttrName[0], 
                    suppressSuffixError=suppress, delCons=0)
    # suppressSuffixError -----------------------------------------------------   
    UIpopulatePackageList()
 
    return Vsuffix
    
def UIobjectsFromTSL(TSLname):
    Vlist = []
    VlistStr = pm.textScrollList (TSLname, q=1, allItems=1)
    for i in range(len(VlistStr)):
        Vlist.append(pm.ls(VlistStr[i])[0])
    return Vlist

def UIpackageDelete():
    Vsuffix = pm.textScrollList ('packListTSL', q=1, selectItem=1)
    Vbuilt = pm.menuItem('lsBiltCB', cb=1, q=1)
    keepObj = pm.menuItem('deleteObjectsCheckCB', cb=1, q=1)
    packAttrName = UIreturnPackageAttribute()
    
    
    for i in range(len(Vsuffix)):
        compList = packageRead(Vsuffix=[Vsuffix[i]], Vbuilt=Vbuilt, 
                                packAttrName=packAttrName)[1]
        firstComp = compList[0]
        
        if keepObj:
            componentsDelete(firstComp)
              
            if firstComp[0].hasAttr(packAttrName[0]):
                pm.deleteAttr(str(firstComp[0]) + '.' + packAttrName[0])
            if firstComp[0].hasAttr('builder'):
                firstComp[0].builder.delete()
            if firstComp[0].hasAttr('options'):
                firstComp[0].options.delete()
        
        else:
            pm.delete(compList)
        
    UIpopulatePackageList()
    return

def UIpackageMake():
    Vlist = UIobjectsFromTSL('compListTSL') 
    packAttrName = UIreturnPackageAttribute()    
    Vsuffix = pm.textField ('suffixTF', q=1, text=1)
    Vbuilder = pm.textField ('builderTF', q=1, text=1)
    Voptions = pm.textField ('optionsTF', q=1, text=1)
    packageMake(Vlist=Vlist, Vbuilder=Vbuilder, Voptions=Voptions, 
                Vsuffix=Vsuffix, packAttrName=packAttrName)
    UIpopulatePackageList()
    return

def UIselectComponentsDoubbleClick():
    if not pm.menuItem('selectComponentsCB', cb=1, q=1):
        Vlist = pm.textScrollList ('compListTSL', q=1, selectItem=1)
        pm.select (Vlist)

def UIselectComponents():
    if pm.menuItem('selectComponentsCB', cb=1, q=1):
        Vlist = pm.textScrollList ('compListTSL', q=1, selectItem=1)
        pm.select(Vlist)
    #UIpopulatePackageList()
    return

def UIpopulatefromSelection():
    Vlist = pm.ls(sl=1)
    pm.textScrollList ('compListTSL', e=1, removeAll=1)
    pm.textScrollList('compListTSL', e=1, append=Vlist)
    #UIpopulatePackageList()
    return

def UIclear():
    UIclearSelection()
    UIclearBuilder()

def UIclearSelection():
    pm.textScrollList ('compListTSL', e=1, removeAll=1)
    
def UIclearBuilder():
    pm.textField('suffixTF', text='', e=1)
    pm.textField('builderTF', text='', e=1)
    pm.textField('optionsTF', text='', e=1)

def UIcomponentsMove(moveUp=1):
    l = pm.textScrollList ('compListTSL', q=1, allItems=1)
    r = pm.textScrollList ('compListTSL', q=1, selectItem=1)
    
    if moveUp:
        l.reverse()
    else:
        r.reverse()
        
    for o in r:
        i = l.index(o)
        if i+1 == len(l):
            return 
        l.insert(i+2, o)
        l.remove(o)
    
    if moveUp:
        l.reverse()
        
    pm.textScrollList ('compListTSL', e=1, removeAll=1)
    pm.textScrollList('compListTSL', e=1, append=l)
    o = pm.textScrollList ('compListTSL', e=1, selectItem=r)

def UIcomponentsRemove():
    r = pm.textScrollList ('compListTSL', q=1, selectItem=1)
    pm.textScrollList('compListTSL', e=1, removeItem=r)
    
def UIpopulateReplace():
    """
    replaces a selection in the list with selection in the scene
    """
    Vlist = lsSl.lsSl(Vmin=1, Vmax=0)
    Vlist.reverse()
    textlist = pm.textScrollList('compListTSL', q=1, allItems=1)
    removeList = pm.textScrollList('compListTSL', q=1, selectItem=1)
    pos = textlist.index(removeList[0])
    
    for ob in removeList:
        textlist.remove(ob)
    
    for i in range(len(Vlist)):
        textlist.insert(pos, Vlist[i])
    pass
     
    pm.textScrollList ('compListTSL', e=1, removeAll=1)
    pm.textScrollList('compListTSL', e=1, append=textlist)
    pm.textScrollList('compListTSL', e=1, selectItem=Vlist)
    
def UIpopulateInsert():
    """
    replaces a selection in the list with selection in the scene
    """
    Vlist = lsSl.lsSl(Vmin=1, Vmax=0)
    Vlist.reverse()
    textlist = pm.textScrollList('compListTSL', q=1, allItems=1)
    selectionList = pm.textScrollList('compListTSL', q=1, selectItem=1)
    textlist.reverse()
    
    pos = textlist.index(selectionList[-1])

    result = textlist[:pos] + Vlist + textlist[pos:]
    result.reverse()
     
    pm.textScrollList ('compListTSL', e=1, removeAll=1)
    pm.textScrollList('compListTSL', e=1, append=result)
    pm.textScrollList('compListTSL', e=1, selectItem=Vlist)
    
def UIpopulatePackageList():
    packAttrName=UIreturnPackageAttribute()
    Vbuilt = pm.menuItem('lsBiltCB', cb=1, q=1)
    x = packNameRead(Vbuilt=Vbuilt, packAttrName=packAttrName)
    pm.textScrollList ('packListTSL', e=1, removeAll=1)
    if x != []:
        pm.textScrollList('packListTSL', e=1, append=x[0])
        return

def UIpopulateComponentList():
    Vsuffix = pm.textScrollList ('packListTSL', q=1, selectItem=1)
    Vbuilt = pm.menuItem('lsBiltCB', cb=1, q=1)
    packAttrName = UIreturnPackageAttribute()
    
    packageList = packageRead(Vsuffix, Vbuilt=Vbuilt, packAttrName=packAttrName)
    pm.textScrollList ('compListTSL', e=1, removeAll=1)
    pm.textScrollList('compListTSL', e=1, append=packageList[1][0])
    Vsuffix = packageList[1][0][0].getAttr(packAttrName[0])
    Vbuilder = packageList[1][0][0].getAttr('builder')
    Voptions = packageList[1][0][0].getAttr('options')
    pm.textField ('suffixTF', e=1, text=Vsuffix)
    pm.textField ('builderTF', e=1, text=Vbuilder)
    pm.textField ('optionsTF', e=1, text=Voptions)
    if pm.menuItem('selectComponentsCB', cb=1, q=1):
        pm.select(packageList[1][0])
        #pm.textScrollList ('compListTSL', e=1, selectItem=packageList[1][0])
    
    return

def UItemplateReadFiles (Vpath=''):
    Vpath = pm.textField('pathTF', q=1, text=1)
    VfullPath = Vpath + '/templates'
    if os.path.exists(VfullPath) == 0:
        pm.textScrollList('templateTSL', e=1, removeAll=1)
        return
    Vsubdir = os.listdir(VfullPath)
    
    VpathlistMB = tf.sortByExtension(Vsubdir, '.mb')
    VpathlistMB = sorted(VpathlistMB, key=lambda pl: str.lower(str(pl)))
    VpathlistMA = tf.sortByExtension(Vsubdir, '.ma')
    VpathlistMA = sorted(VpathlistMA, key=lambda pl: str.lower(str(pl)))
    Vpathlist = VpathlistMB + VpathlistMA
    pm.textScrollList('templateTSL', e=1, removeAll=1)
    pm.textScrollList('templateTSL', e=1, append=Vpathlist)
    
    pm.scrollField ('infoSCF', e=1, text='')
    
    return

def UItemplateInfoRead():
    Vpath = pm.textField('pathTF', q=1, text=1)
    VfullPath = Vpath + '/templates'
    fileName = pm.textScrollList ('templateTSL', q=1, selectItem=1)
    Vname = os.path.splitext(fileName[0])
    try:
        infoText = templateInfoRead(Vpath=VfullPath, Vfile=Vname[0]) 
        pm.scrollField ('infoSCF', e=1, text=infoText)
    except:
        pm.scrollField ('infoSCF', e=1, text='')
        pass
    return

def templateInfoRead (Vpath='', Vfile=''):
    """
    reads the info file of a given template file and returns the info text.
    """
    VfullPath = (Vpath + '/' + Vfile + '.txt')
    tf.checkPath(VfullPath)
    infoText = tf.readFile(VfullPath)
    return infoText

def infoOpenUrl():
    """parses the text for urls and trys to open them in the default browser"""
    
    try:
        Vpath = pm.textField('pathTF', q=1, text=1)
        VfullPath = Vpath + '/templates'
        fileName = pm.textScrollList ('templateTSL', q=1, selectItem=1)
        Vname = os.path.splitext(fileName[0])
        
        f = open (VfullPath+'/'+Vname[0]+'.txt', 'r')
        read_data = f.readlines()
        #print read_data
        f.close()
        r = re.compile(r"(http://[^ ]+)")
        for line in read_data:    
            try:
                m = re.search(r, line)
                pm.showHelp(m.groups(), a=1)
            except:
                pass
    except:
        pass
    return
    
def UItemplateInfoWrite():
    
    Vpath = pm.textField('pathTF', q=1, text=1)
    VfullPath = Vpath + '/templates'
    fileName = pm.textScrollList ('templateTSL', q=1, selectItem=1)
    Vname = os.path.splitext(fileName[0])
    
    VfullTxtPath = (VfullPath + '/' + Vname[0] + '.txt')
    infoText = pm.scrollField ('infoSCF', q=1, text=1)
    
    if infoText != '' and os.path.exists(VfullPath) == 1:
        
        f = open (VfullTxtPath, 'w')
        f.write(infoText)
        f.close()
        
    elif infoText == '' and os.path.exists(VfullTxtPath) == 1:
        os.remove(VfullTxtPath)  
    return

def UIsetPath(Vfilename, VfileType):
    pm.textField('pathTF', edit=1, text=Vfilename)
    return

def UItempFileBrowser():
    pm.fileBrowserDialog(m=4, fc=UIsetPath, an="set template path")
    UItemplateReadFiles()
    return
    
def packageMake(Vlist=[], Vbuilder='default', Voptions='optionA=1, optionB=2', 
                Vsuffix='', packAttrName=['package']):
    """
    creates a package
    
    ========= ==================================================
    Arguments Description
    ========= ==================================================
    Vlist[]   list of package objects
    Vbuilder  name of the module that builds the package
    Voptions  string of arguments for the builder  
    Vsuffix   package suffix
    ========= ==================================================
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    
    # import and reload the module ----------------------------------------
    # to check for Vlist and Vsuffix arguments
    Vsplit = Vbuilder.split('.')
    Vsplit.pop()
    Vmod=Vsplit[0]
    for j in range(len(Vsplit)-1):
        Vmod=Vmod+'.'+Vsplit[j+1]
    exec ('import %s' % (Vmod))
    exec  ('reload (%s)' % (Vmod))
    
    builderArgs = checkBuilderArgs(eval(Vbuilder))
    if builderArgs != []:
        raise wrongArgumentsError ('missing arguments: '+str(builderArgs))
    
    packageSuffixCheck(Vlist=[Vlist[0]], Vsuffix=Vsuffix)
    componentsMake(Vlist=Vlist, VattributeName='Vlist')
    if Vbuilder == '':
        raise UIinputError('no builder defined')
    if Vsuffix == '':
        raise UIinputError('no package name defined')
    
    if not Vlist[0].hasAttr(packAttrName[0]):
        Vlist[0].addAttr(packAttrName[0], dt='string')
    if not Vlist[0].hasAttr('builder'):
        Vlist[0].addAttr('builder', dt='string')
    if not Vlist[0].hasAttr('options'):
        Vlist[0].addAttr('options', dt='string')
    
    packAttr = pm.ls(Vlist[0] + '.' + packAttrName[0])
    packAttr[0].set(Vsuffix)
    
    Vlist[0].builder.set(Vbuilder)
    Vlist[0].options.set(Voptions)
    return

def packNameRead(Vsuffix=[], Vbuilt=0, packAttrName=[]):
    
    packList = bn.findObjectsByAttr(attr=packAttrName[0], nodeType='transform')
    
    VcompFirstList = []
    VsuffixList = []
        
    for i in range(len(packList)):
        messageAttrDelUnused([packList[i]])
        # get all components with the options ---------------------------------
        ValredyBuilt = packList[i].hasAttr('build_with_1')
        
        if Vbuilt == 0:
            if ValredyBuilt == 0:
                VcompFirstList.append(packList[i])
                
                if Vsuffix==[]:
                    VsuffixList.append(packList[i].getAttr(packAttrName[0]))
                else:
                    VsuffixList = Vsuffix
        else:
            if ValredyBuilt == 1:
                VcompFirstList.append(packList[i])
                
                if Vsuffix==[]:
                    VsuffixList.append(packList[i].getAttr(packAttrName[0]))
                else:
                    VsuffixList = Vsuffix
             
    if VcompFirstList == []:
        return[]
    
    if Vsuffix == []:
        VsuffixList = sorted(VsuffixList, key=lambda sl: str.lower(str(sl)))
        #VsuffixList.sort()
        
    return [VsuffixList, VcompFirstList]

def packageRead(Vsuffix=[], Vbuilt=0, packAttrName=[]):
    """
    looks for components and returns a list of sorted component packages.

    returns:
    a list of all packages within the suffix list 
    e.g. [[suffixA, suffixB], [[1_AAA, 2_AAA], [1_BBB, 2_BBB]]]
    
    ========= =======================================================
    Arguments Description
    ========= =======================================================
    Vsuffix   list of suffixes for the components to be retuned
    ========= =======================================================
    """

    names = packNameRead(Vsuffix, Vbuilt, packAttrName)
    if names == []:
        return []
    VsuffixList, VcompFirstList = names

    VcompListAll = []
    # sort by suffix ----------------------------------------------------------    
    for i in range(len(VsuffixList)):
        for j in range(len(VcompFirstList)):
            if VcompFirstList[j].getAttr(packAttrName[0]) == VsuffixList[i]:
                # read packages that are connected ----------------------------
                compList = componentsRead(Vlist=[VcompFirstList[j]], 
                                          VattributeName='Vlist', returnOnlyObj=1)
               
                VcompListAll.append(compList)
    
    return [VsuffixList, VcompListAll]

def UIpackageImport():
    Vpath = pm.textField('pathTF', query=1, text=1)
    Vfolder = Vpath + '/templates'
    Vfilenames = pm.textScrollList('templateTSL', q=1, selectItem=1)
    packageImport(Vfolder=Vfolder, Vfilenames=Vfilenames)

def packageImport(Vfolder='', Vfilenames=[]):
    """
    imports a file from the given folder
    """
    for i in range(len(Vfilenames)):
        VfullPath = Vfolder + '/' + Vfilenames[i]
        Vname = os.path.splitext(Vfilenames[i])[0]
        pm.importFile(VfullPath, rpr=Vname, loadReferenceDepth="all")
    return

def UItemplateDelete():
    Vpath = pm.textField('pathTF', query=1, text=1)
    Vfolder = Vpath + '/templates'
    Vfilenames = pm.textScrollList('templateTSL', q=1, selectItem=1)
    templateDelete(Vfolder=Vfolder, Vfilenames=Vfilenames)
   
def templateDelete(Vfolder='', Vfilenames=[]):
    """
    Deletes a file from the given folder.
    Also deletes the info file if there is one.
    """
    VdelFile = 'are you sure to delete those templates?\n\n'
    VdelPath = []
    for i in range(len(Vfilenames)):
        VfullPath = Vfolder + '/' + Vfilenames[i]
        
        if os.path.exists(VfullPath) == 1:

            VdelPath.append(VfullPath)
            VdelFile=VdelFile+('%s\n' % str(Vfilenames[i]))
            
    Vconfirm = pm.confirmDialog( 
            title='Delete Templates', 
            message=(VdelFile), 
            button=['delete','cancel'], 
            defaultButton='cancel', 
            dismissString='cancel' )
    
    if Vconfirm == 'cancel':
        return
    
    for i in range(len(VdelPath)):
        if os.path.exists(VdelPath[i]) == 1:
            os.remove(VdelPath[i]) 
            Vname = os.path.splitext(VdelPath[i])[0]
            try:
                os.remove(Vname + '.txt')
            except:
                pass
            
    UItemplateReadFiles()
    return

def cleanupTemplates(remove=0):
    """
    Deletes all files that do not have a template asossiated to it.
    """   
    Vpath = pm.textField('pathTF', query=1, text=1)
    Vfolder = Vpath + '/check'
    folderList = ['animation', 'built', 'cameras', 
                  'meshes', 'playblasts', 'weights']
    
    templates = os.listdir(os.path.join(Vpath, 'templates'))
    template_names = []
    for t in templates:
        name_ext = os.path.splitext(t)
        if name_ext[1] == '.ma' or name_ext[1] == '.mb':
            template_names.append(name_ext[0])
    
    
    result = []
    print templates
    print template_names
    
    for f in folderList:
        VfullPath = os.path.join(Vfolder, f)
        content = os.listdir(VfullPath)
        print VfullPath
        for c in content:
            name_ext = os.path.splitext(c)
            
            if not name_ext[0] in template_names:
                if name_ext[1] in ['.ma', '.mb', '.txt']:
                    print name_ext[0]
                     
                    result.append(os.path.join(VfullPath, c))
    if remove:
        for f in result:
            os.remove(f)
            print f
        
    return result

def UTemplateIinfoDelete():
    VdelPath = []
    VdelFile = 'are you sure to delete those info files?\n\n'
    Vpath = pm.textField('pathTF', query=1, text=1)
    Vfolder = Vpath + '/templates/'
    Vfilenames = pm.textScrollList('templateTSL', q=1, selectItem=1)
    
    for i in range(len(Vfilenames)):
        Vname = os.path.splitext(Vfilenames[i])[0]
        VfullPath = (Vfolder + Vname + '.txt')
        if os.path.exists(VfullPath) == 1:
            
            VdelPath.append(VfullPath)
            VdelFile=VdelFile+('%s.txt\n' % (Vname))
            
    Vconfirm = pm.confirmDialog( 
            title='Delete Info Files', 
            message=(VdelFile), 
            button=['delete','cancel'], 
            defaultButton='cancel', 
            dismissString='cancel' )
    
    if Vconfirm == 'cancel':
        return
    
    for i in range(len(VdelPath)):         
        try:
            os.remove(VdelPath[i])
            pm.scrollField ('infoSCF', e=1, text='')
        except:
            pass
    
    UItemplateReadFiles()
    return

def UIpackageExport():
    packAttrName=UIreturnPackageAttribute()
    Vpath = pm.textField('pathTF', query=1, text=1)
    Vsuffix = pm.textScrollList ('packListTSL', q=1, selectItem=1)
    
    if Vsuffix == []:
        packAttrName = UIreturnPackageAttribute(returnAll=1)

    packageExport(Vpath=Vpath, Vsuffix=Vsuffix, packAttrName=packAttrName)
    return
    
def packageExport(Vpath='', Vsuffix=[], packAttrName=[''], exportConstraints=1):
    """
    exports a package to the 'templates' folder under the given path

    ============ ==================================================
    Arguments    Description
    ============ ==================================================
    Vpath        path containing the 'templates' folder
    Vsuffix      suffix
    packAttrName list of package namespaces
    ============ ==================================================
    """ 
    Vresult = []
    defaultFileName='template'
    try:
        defaultFileName = Vsuffix[0]    
    except:
        pass
    
    for a in range(len(packAttrName)):
        compLists = packageRead(Vsuffix=Vsuffix, Vbuilt=0, 
                                packAttrName=[packAttrName[a]])

        try:
            for i in range(len(compLists[1])):
                Vresult = Vresult + compLists[1][i]
        except:
            pass
    
    if Vresult == []:
        raise Exception('no package to export')
            
    Vfolder = os.path.join(Vpath, 'templates')
    tf.checkPath(Vfolder) 
    defaultPath = os.path.abspath(os.path.join(Vfolder, defaultFileName))
    
    VfullPath = pm.fileDialog(mode=1,
                           directoryMask=Vfolder+'/*.ma', 
                           defaultFileName=Vfolder+'/*.ma',
                           title='export package')
    if VfullPath == None:
        return
    pm.select (cl=1)
    pm.select(Vresult)

    # make sure constraints will be exported too
    # uiConfiguration=0
    try:
        pm.exportSelected(VfullPath, typ="mayaAscii", es=1, force=1,
                       constructionHistory=0, constraints=exportConstraints,
                       channels=0, expressions=0,uiConfiguration=0, shader=0)
    except:
        pm.exportSelected(VfullPath, typ="mayaBinary", es=1, force=1,
                       constructionHistory=0, constraints=exportConstraints,
                       channels=0, expressions=0,uiConfiguration=0, shader=0)

    return Vresult

def packageEvalList(packAttrName=[], suppressSuffixError=0, delCons=0):
    """
    evaluates a list of packages namespaces
    all namespaces will be evaluated
    
    ============ ========================================================
    Arguments    Description
    ============ ========================================================
    packAttrName list of the package attribute names to be evaluated
    ============ ========================================================
    """
    
    for p in range(len(packAttrName)):
        packageEval(Vsuffix=[], packAttrName=packAttrName[p], 
                    suppressSuffixError=suppressSuffixError, delCons=delCons)
        
def packageEvalDelCons(VcompList):
    """
    deletes all constraints from components and their children
    """
    Vchildren = gt.getChildren(VcompList)
    Vcons = tc.filterCons(Vchildren)

    for i in range(len(Vcons)):
        try:
            pm.delete (Vcons[i])
        except:
            pass

def UIpackageEvalQuick():
    Vlist = UIobjectsFromTSL('compListTSL')
    Vsuffix = pm.textField ('suffixTF', q=1, text=1)
    Vbuilder = pm.textField ('builderTF', q=1, text=1)
    Voptions = pm.textField ('optionsTF', q=1, text=1)
    
    packageEvalQuick(Vlist=Vlist, Vbuilder=Vbuilder, Voptions=Voptions, 
                     Vsuffix=Vsuffix)
    return

def UIprintFunction():
    Vlist = UIobjectsFromTSL('compListTSL')
    Vsuffix = pm.textField ('suffixTF', q=1, text=1)
    Vbuilder = pm.textField ('builderTF', q=1, text=1)
    Voptions = pm.textField ('optionsTF', q=1, text=1)
    
    packageEvalQuick(Vlist=Vlist, Vbuilder=Vbuilder, Voptions=Voptions, 
                     Vsuffix=Vsuffix, printInfo=1)
    return

def checkBuilderArgs(Vfunc):
    """
    trys a builder and raises exceptions if there is no Vlist argument. 
    """ 
    result = []      
    try:
        if inspect.isclass(Vfunc):
            agsp = inspect.getargspec(Vfunc.__init__)
            agsp[0].remove('self')    
        else:
            agsp = inspect.getargspec(Vfunc)
    except:
        return result
    
    # if there are no * or ** args look for Vlist and Vsuffix
    if agsp[2] == None:
        try: agsp[0].index('Vlist')
        except: result.append('Vlist')
        try: agsp[0].index('Vsuffix')
        except: result.append('Vsuffix')
    #print result
    return result
 
def packageEvalQuick(Vlist=[], Vbuilder='mf', Voptions='', Vsuffix='', printInfo=0):
    """
    evaluates a function without creating components and without deleting 
    constraints or checking naming. 
    """
    # import and reload the module ----------------------------------------
    Vsplit = Vbuilder.split('.')
    Vsplit.pop()
    Vmod=Vsplit[0]
    for j in range(len(Vsplit)-1):
        Vmod=Vmod+'.'+Vsplit[j+1]
    exec ('import %s' % (Vmod))
    exec  ('reload (%s)' % (Vmod))
    
    if Voptions != '':
        Voptions = Voptions+','
   
    builderType = checkBuilderArgs(eval(Vbuilder))
    #print builderType
        
    if builderType == []:
        commandStr = '%s(Vlist=%s, Vsuffix=\'%s\',%s)' % (Vbuilder, Vlist, Vsuffix, Voptions)
    
    elif builderType == ['Vlist']:
        commandStr = '%s(Vsuffix=\'%s\',%s)' % (Vbuilder, Vsuffix, Voptions)
        
    elif builderType == ['Vsuffix']:
        commandStr = '%s(Vlist=%s, %s)' % (Vbuilder, Vlist, Voptions)
    
    elif builderType == ['Vlist', 'Vsuffix']:
        commandStr = '%s(%s)' % (Vbuilder, Voptions)
    
    if printInfo:
        print '\n# Shelf Button:'
        print '#--------------------------------------------------------'
        if not Vlist == [] or Vlist == None or Vlist == '':
            print 'import pymel.core.nodetypes as nt'
        print ('import %s' % (Vmod))
        print ('reload (%s)' % (Vmod))            
        print commandStr
        print '#--------------------------------------------------------'
        
    else: 
        print '---------------------------------------------------------'            
        result = eval (commandStr)
        
        print 'evaluated: '+commandStr
        print 'result:'
        print result
        print '---------------------------------------------------------'
    
    return

def packageEval(Vsuffix=[], packAttrName='', suppressSuffixError=0, delCons=0):
    """
    evaluates (builds) a list of packages()
    
    =================== =======================================================  
    Arguments           Description
    =================== =======================================================
    Vsuffix             suffix list for the packages to evaluate
                        if the list is empty everything with the packAttrName 
                        will be evaluated
    packAttrName        name of the package attribute to be evaluated
    suppressSuffixError passes even if not all dag objects have the package 
                        suffix.
    =================== =======================================================
    """
    outputPread = packageRead(Vsuffix=Vsuffix, Vbuilt=0, packAttrName=[packAttrName])

    # returns nothing if packagheRead does not contain any package
    try:
        VcompList = outputPread[1]
    except: return

    if VcompList == '':
        return

    for i in range(len(VcompList)):
        

        # delete all constraints from components and their children------------
        if delCons == 1:
            packageEvalDelCons(VcompList[i])


        # list for suffixCheck
        if suppressSuffixError == 0:
            listA = cm.ls(dagObjects=1)

        Vbuilder = VcompList[i][0].getAttr('builder')
        Voptions = VcompList[i][0].getAttr('options')
        Vsuffix = VcompList[i][0].getAttr(packAttrName)

        # import and reload the module ----------------------------------------
        Vsplit = Vbuilder.split('.')
        Vsplit.pop()
        Vmod=Vsplit[0]
        for j in range(len(Vsplit)-1):
            Vmod=Vmod+'.'+Vsplit[j+1]
        exec ('import %s' % (Vmod))
        exec  ('reload (%s)' % (Vmod))

        # delete attributes ---------------------------------------------------
        pm.deleteAttr(str(VcompList[i][0]) + '.' + packAttrName)
        VcompList[i][0].builder.delete()
        VcompList[i][0].options.delete()
        componentsDelete([VcompList[i][0]])

        # this is the list for the command string, works better than using ls()
        Vlist = VcompList[i]

        if Voptions == '':
            commandStr = '%s(Vlist=Vlist, Vsuffix=\'%s\')' % (Vbuilder, Vsuffix)
        else:
            commandStr = '%s(Vlist=Vlist, %s, Vsuffix=\'%s\')' % (Vbuilder, Voptions, Vsuffix)
        #print commandStr
        print '---------------------------------------------------------------'
        print commandStr
        eval (commandStr)
        
        for j in range(len(VcompList[i])):
            ind = 1
            while VcompList[i][j].hasAttr('build_with_%s'%(ind)):
                ind = ind + 1
       
            # add info attributes     
            VcompList[i][j].addAttr('build_with_%s'%(ind), dt='string')
            
            Vattr = pm.ls(VcompList[i][j] + '.' + 'build_with_' + str(ind))
            Vattr[0].set(commandStr)
        
        # recreate attributes -------------------------------------------------
        packageMake(Vlist=Vlist, Vbuilder=Vbuilder, Voptions=Voptions, 
                    Vsuffix=Vsuffix, packAttrName=[packAttrName])
        
        # check suffix --------------------------------------------------------
        if suppressSuffixError == 0:
            listB = cm.ls(dagObjects=1)
            listC = naming.compareSceneObj(listA, listB)
            listC = listC + Vlist
            
            componentSuffixCheck(listC, Vsuffix=Vsuffix, 
                                 suppressSuffixError=suppressSuffixError)

        print '---------------------------------------------------------------'
        print 'building successful:'
        print commandStr
        if suppressSuffixError:
            print '- suffix check OFF'
    print '---------------------------------------------------------------'
    print 'all building for "%s" successful' % (packAttrName)
    print '==============================================================='

    #return commandStr
    
def componentSuffixCheck(listC=[], Vsuffix='', suppressSuffixError=0):
    """
    checks the naming for newly created objects after building a package
    """ 
    ob = naming.readSuffix(listC, Vsuffix)
    if ob[0] != []:
        print '---------------------------------------------------------------' 
        print 'These DAG objects are without the suffix (' + str(len(ob[0])) + '):'
        for obj in ob[0]:
            print obj
        print '---------------------------------------------------------------' 
         
    # unique suffix ---------------------------------------------------
    if ob[2] != []:
        print '---------------------------------------------------------------' 
        print 'these objects have multiple suffixes:'
        for obj in ob[2]:
            print obj
        print '---------------------------------------------------------------'
        
    # unique names----------------------------------------------------
    nonUnique = naming.uniqueNames(listC)
    if nonUnique != []:
        print '---------------------------------------------------------------' 
        print 'these objects have non unique names:'
        for obj in nonUnique:
            print obj
        print '---------------------------------------------------------------'
    
    if ob[0] != []:
        if suppressSuffixError == 0:
            raise noSuffixError (str(len(ob[0])) + ' DAG objects without suffix "' + Vsuffix + '": ' + str(ob[0]))
          
    if ob[2] != []:   
        if suppressSuffixError == 0:
            raise nonUniqueSuffixError (str(len(ob[2])) + ' objects with multiple suffix "' + Vsuffix + '": ' + str(ob[2]))
    
    if nonUnique != []:
        if suppressSuffixError == 0:
            raise nonUniqueNameError ('objects with non unique names: '+ str(nonUnique))

        
def packageSuffixCheck(Vlist=[], Vsuffix='', packAttrName=['package']):
    """
    raises an exception if the given suffix already exists on any other than
    the given object.
  
    ========= ======================================  
    Arguments Description
    ========= ======================================  
    Vlist[0]  first object for the new package
    Vsuffix   the suffix to be checked
    ========= ======================================  
    """
    Vlist = lsSl.lsSl(Vlist, 1, 1) 
    VobjList = pm.ls(dagObjects=1, transforms=1)

    VcompFirstList = []
    
    for i in range(len(VobjList)):
        # get all components with suffix --------------------------------------
        if VobjList[i].hasAttr(packAttrName[0]) and VobjList[i].hasAttr('build_with_1') == 0 and Vlist[0] != VobjList[i]:
            VcompFirstList.append(VobjList[i])
            
            if VobjList[i].getAttr(packAttrName[0]) == Vsuffix:
                raise nonUniquePackageError ('suffix "%s" already exists on "%s"' % (Vsuffix, VobjList[i]))

def componentsMake(Vlist=[], VattributeName='Vlist'):
    """
    connects a list of objects to message attributes on the first object

    ============== ============================================================ 
    Arguments      Description
    ============== ============================================================
    Vlist[0]       goal object
    Vlist[1:]      source objects
    VattributeName name for the message attributes
    ============== ============================================================
    """
    Vlist = lsSl.lsSl(Vlist, 2, 0)
    Vresult =[]
    
    # delete existing connections ---------------------------------------------
    #if Vlist[0].hasAttr('%s_%s'%(VattributeName, 1)):
    messageAttrDelUnused(Vlist=Vlist)
    componentsDelete([Vlist[0]], VattributeName)
        
    #print ('!existing attribute: "%s.%s" will be overwritten!' % (Vlist[0], VattributeName))
    
    for i in range(len(Vlist)-1):
        i=i+1 
          
    # source attribute ----------------------------------------------------
        ind = 1
        while Vlist[0].hasAttr('%s_%s'%(VattributeName, ind)):
            ind = ind + 1   
        Vlist[0].addAttr('%s_%s'%(VattributeName, ind), at='message')
        VattrA = pm.ls('%s.%s_%s'%(Vlist[0], VattributeName, ind)) 
        
    # goal attribute ------------------------------------------------------
        ind = 0
        while Vlist[i].hasAttr('%s_goal_%s'%(VattributeName, ind)):
            ind = ind + 1
        Vlist[i].addAttr('%s_goal_%s'%(VattributeName, ind), at='message')     
        VattrB = pm.ls('%s.%s_goal_%s'%(Vlist[i], VattributeName, ind))

        Vresult.append([VattrA[0], VattrB[0]])
        VattrA[0] >> VattrB[0]

    return Vresult

def componentsRead(Vlist=[], VattributeName='Vlist', returnOnlyObj=0):
    """
    reads connections of message attributes
   
    returns:
    lists of [connected object, source attribute, goal attribute]
    or:
    a list of the package objects
  
    ============== ============================================================
    Arguments      Description
    ============== ============================================================
    Vlist[0]       first package object that contains the connected atributes
    VattributeName name of the message attributes
    returnOnlyObj  if 1 a list of the package objects will be returned
    ============== ============================================================
    """
    Vlist = lsSl.lsSl(Vlist, 1, 1)
    Vresult = []
    if returnOnlyObj:
            Vresult = [Vlist[0]]
    
    ind = 1
    while Vlist[0].hasAttr('%s_%s'%(VattributeName, ind)):
        VconObj = pm.listConnections('%s.%s_%s'%(Vlist[0], VattributeName, ind))
        VsourceAttr = pm.ls('%s.%s_%s'%(Vlist[0], VattributeName, ind))
        VgoalAttr = pm.ls(pm.connectionInfo('%s.%s_%s'%(Vlist[0], VattributeName, ind), 
                                      destinationFromSource=1)[0])
        ind = ind + 1
        
        if returnOnlyObj:
            Vresult.append(VconObj[0])
        else:
            Vresult.append([VconObj[0], VsourceAttr[0], VgoalAttr[0]])
         
    return Vresult

def componentsDelete(Vlist=[], VattributeName='Vlist'):
    """
    deletes a packages message attributes

    arguments:
    Vlist[0] -- first component
    VattributeName -- name for the attribute linked to the components 
    """
    Vresult = []
    attrList = componentsRead(Vlist, VattributeName)
    
    for i in range(len(attrList)):
        pm.deleteAttr(attrList[i][1])
        pm.deleteAttr(attrList[i][2])
        Vresult.append([attrList[i][1], attrList[i][2]])

    return Vresult

def messageAttrDelUnused(Vlist=[]):
    """
    deletes all message attributes without connections from the listed objects
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0) 
       
    for i in range(len(Vlist)):
        VcustomAttr = pm.listAttr(Vlist[i], userDefined = 1)
        
        for j in range(len(VcustomAttr)):
            
            if pm.attributeQuery(VcustomAttr[j], node=Vlist[i], message=1):
                VmessageAttr = pm.ls('%s.%s' % (Vlist[i], VcustomAttr[j]))
                Vconnections = VmessageAttr[0].connections() 
                
                if Vconnections == []:
                    pm.deleteAttr(VmessageAttr)  
    return

def customAttributesDel(Vlist=[]):
    """
    deletes blindly all custom attributes
    """   
    Vlist = lsSl.lsSl(Vlist, 1, 0) 
    Vresult=[]   
    for i in range(len(Vlist)):
        VcustomAttr = pm.listAttr(Vlist[i], userDefined = 1)
        for j in range(len(VcustomAttr)):
            Vresult.append(pm.ls('%s.%s' % (Vlist[i], VcustomAttr[j]))[0])
            try:
                pm.deleteAttr(pm.ls('%s.%s' % (Vlist[i], VcustomAttr[j]))[0])
            except:
                pass
    return Vresult

def getModuleNames(inp):
    """
    returns the module names as strings
    """
    result = []
    pkgpath = os.path.dirname(inp.__file__)
    result = [name for _, name, _ in pkgutil.iter_modules([pkgpath])]
    
    return result

def getFunctionNames(inp):
    """
    returns the function names as strings
    """
    result = []
    for name in dir(inp):
        obj = getattr(inp, name)
        if inspect.isfunction(obj):

            if inp == inspect.getmodule(obj, _filename=None):
                result.append(name)
                #print inspect.getdoc(obj)
        
        elif inspect.isclass(obj):
            #print 'class: ' + str(obj)
            if inp == inspect.getmodule(obj, _filename=None):
                result.append(name)
            

    return result

def populateFPreturn(inpStr):
    """
    Trys to import the module or function and passe it to gt module or function 
    names.
    """
    result = []
    try:
        inp = __import__(inpStr, fromlist=['dummy'])
        reload(inp)
        setBackButton(inpStr)
    except ImportError:
        split = inpStr.split('.')
        ext = split[-1]
        x = split[0]
        for o in split[1:-1]:
            x=x+'.'+o
        try:
            inp = __import__(x, fromlist=['dummy'])
            reload(inp)
        except ImportError:
            
            return result
        
        f = getFunctionNames(inp)
        y = f
        
        if y == []:
            y = getModuleNames(inp)
            
        for o in y:
            if o == ext:       
                setBackButton(ext)
                return []
            
            # Search function
            if str.lower(str(ext)) in str.lower(str(o)):
                result.append(o)

        if not result == []:        
            pm.textField ('builderTF', e=1, text=x)
            setBackButton(x)
        
        # Autocomplete
        if len(result) == 1:
            pm.textField ('builderTF', e=1, text=x + '.' + result[0])     
            populateFP(x + '.' + result[0])
            return None
        return result

    result = getFunctionNames(inp)
    if result == []:
        result = getModuleNames(inp)
    
    #If there is a point at the end like mf. take the split before that.
    if inpStr.split('.')[-1] == '':
        try:
            setBackButton(inpStr.split('.')[-2])
        except:
            pass
                         
    return result

def populateFP(inpStr, maxItems=100):
    """
    Populates the functionPicker UI, if there is nothing to populate it with,
    deletes the UI.
    """
    pm.textScrollList ('fpTSL', e=1, removeAll=1)
    result = populateFPreturn(inpStr)
    if result:
        if result == []:
            try:
                pm.setFocus('builderTF')
            except: 
                pass
        
        pm.textScrollList ('fpTSL', e=1, removeAll=1)
        pm.textScrollList('fpTSL', e=1, append=result[0:maxItems])
        try:
            pm.setFocus('builderTF')
        except:
            pass
        
    
def setBackButton(inpStr):
    """
    """
    split = inpStr.split('.')
    if len(split) > 1:
        current = split[-1]
    else:
        current = inpStr
    pm.button ('upBTN', l='<  '+current, w=30, ann='', e=1)
    
    
def functionPicker():
    """
    function picker window
    """
    try:
        pm.deleteUI('functionPickerUI')
    except: 
        pass
    
    with pm.window('functionPickerUI', h=10, w=10, resizeToFitChildren=1, s=1, toolbox=1,
                restoreCommand='import mf.builder.component as tc; tc.UIpopulatePackageList()') as win:
        
        with pm.formLayout('mainFP'):
            pm.button ('upBTN', l='<', w=30, ann='',
                    c='import mf.builder.component as tc; tc.UIfunctionPickerUp()')
            pm.textScrollList('fpTSL',ann='component list',allowMultiSelection=0,height=100, w=102,
                           doubleClickCommand='import mf.builder.component as tc; tc.UIsetBuilder()',
                           sc='import mf.builder.component as tc; tc.helpOnSelect()')

        pm.formLayout('mainFP', e=True,
                   af=[('upBTN', 'left', 0), ('upBTN', 'bottom', 0), ('upBTN', 'top', 3), ('upBTN', 'right', 0),
                       ('fpTSL', 'left', 0), ('fpTSL', 'bottom', 0), ('fpTSL', 'top', 3), ('fpTSL', 'right', 0)
                       
                         ], 
                    ac=[('upBTN', 'bottom', 0, 'fpTSL')],               
                    ap=[('upBTN', 'top', 0, 0),
                        ('fpTSL', 'top', 23, 0)]) 
                    
    win.show()
    return

def helpOnSelect():
    """
    prints the docstring for selected object in the functinPickerUI 
    """
    try:
        txt = pm.textField ('builderTF', q=1, text=1)
        obj = pm.textScrollList ('fpTSL', q=1, selectItem=1)[0]
    
        Vbuilder=txt+'.'+obj
    
        if Vbuilder == '':
            raise UIinputError ('no builder defined')
    
        try:
            exec ('import %s' % (Vbuilder))
            exec  ('reload (%s)' % (Vbuilder))
            Vfunc = eval(Vbuilder)
            print '\n'
            print inspect.getdoc(Vfunc)
        except:
            exec ('import %s' % (txt))
            exec  ('reload (%s)' % (txt))
            Vfunc = eval(Vbuilder)
            print '\n'
            print inspect.getdoc(Vfunc)
    except:
        return



def UIsetBuilder():
    """
    Sets the builder field from the function picker.
    """    
    inpStr = pm.textField ('builderTF', q=1, text=1)
    obj = pm.textScrollList ('fpTSL', q=1, selectItem=1)[0]

    if inpStr[-1] == '.':
        inpStr = inpStr[:-1]
        
    pm.textField ('builderTF', e=1, text=inpStr + '.' + obj)

    populateFP(inpStr + '.' + obj)
    return

def UIfunctionPickerUp():
    inpStr = pm.textField ('builderTF', q=1, text=1)
    if inpStr[-1] == '.':
        inpStr = inpStr[:-1]
    
    up = os.path.splitext(inpStr)
    pm.textField ('builderTF', e=1, text=up[0])
    populateFP(up[0])
    
    return

