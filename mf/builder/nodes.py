""" 
Custom node classes.
"""
import maya.OpenMaya as OpenMaya
import pymel.core as pm


class customNodeError(Exception): pass

def register(id=''):
    """
    Registers a 'custom node' name.
    The name is used as attribute name to mark the node.
    Raises an error if the name is not valid.
    Returns the name, the nodeType and the function for the custom node.

    Custom nodes need an edit argument e.g. mf.tools.distance(edit=1) so they
    can be recreated from objects in a maya scene.  
    
    A Custom nodes is defined as:
        attribute name, nodeType, class that creates the node.  
    """
    # imports inside the function
    import mf.tools.proxyGeo
    import mf.tools.distance
    import mf.tools.parent
    import mf.tools.curve
    import mf.tools.mesh
    import mf.tools.cons
    import mf.tools.foster
    import mf.tools.general
    
    # Define: attribute name, nodeType, class that creates the node.
    nodes = {
             'joint_bind' : ['joint', mf.tools.proxyGeo.bindJoint],
             'transform_distance' : ['transform', mf.tools.distance.dist],
             
             'transform_character' : ['transform', mf.tools.parent.character],
             'transform_main' : ['transform', mf.tools.parent.main],
             'transform_main_trans_grp' : ['transform', mf.tools.parent.mainTransGrp],
             'transform_no_trans_grp' : ['transform', mf.tools.parent.noTransGrp],
             'transform_mesh_grp' : ['transform', mf.tools.parent.meshGrp],
             
             'transform_no_trans' : ['transform', mf.tools.parent.noTrans],
             'transform_main_trans' : ['transform', mf.tools.parent.mainTrans],
             'transform_trans' : ['transform', mf.tools.parent.trans],
             
             'transform_drv' : ['transform', mf.tools.parent.drv],
             
             'transform_ctrl' : ['', mf.tools.curve.ctrl],
             'transform_ctrl_extra' : ['', mf.tools.curve.ctrl],
             'transform_ctrl_face' : ['', mf.tools.curve.ctrl],
             
             'package_rig' : ['', mf.tools.parent.packageRig],
             
             'mesh_rendermesh' : ['mesh', mf.tools.mesh.mesh],
             'mesh_corrective' : ['mesh', mf.tools.mesh.corrective],
             
             'transform_rivet' : ['transform', mf.tools.mesh.rivet],
             
             'transform_space_switch' : ['', mf.tools.cons.spaceSwitch],
             
             'transform_foster' : ['', mf.tools.foster.foster]
             }
    
    ids = [] 
    for c, p in nodes.iteritems():
        ids.append(c)
        
    if id not in ids:
        raise customNodeError ('"' + id + '" is not in node id list.')
      
    if hasattr(pm, id):
        raise customNodeError ('"' + id + '" is a pymel.core class.')
    
    return [id] + nodes[id]


def findObjectsByAttr(attr='', nodeType=''):
    """
    Returns all nodes in the scene that have a given attribute.
    
    ============= =============================================
    Arguments     Description
    ============= =============================================
    attr          Name of the attribute to look for.
    nodeType      This can be 'transform', mesh' or 'joint'
                  If no node type is given all dependency nodes
                  will be searched.
    ============= =============================================
    """
    node = None
    
    if nodeType=='transform':
        node = OpenMaya.MFn.kTransform
    elif nodeType=='mesh':
        node = OpenMaya.MFn.kMesh
    elif nodeType=='joint':
        node = OpenMaya.MFn.kJoint
    else:
        node = OpenMaya.MFn.kDependencyNode
        
    dagIt = OpenMaya.MItDag(OpenMaya.MItDag.kDepthFirst, node)
    
    withAttr = []
    while not dagIt.isDone():
        try:
            object = OpenMaya.MObject
            object = dagIt.currentItem()
            depNode = OpenMaya.MFnDependencyNode(object)
            depNodeAttr = depNode.hasAttribute(attr)
            if depNodeAttr:
                
                if not depNode.hasUniqueName():
                    name = depNode.name()
                    # workaround to get the dag name
                    depNode.setName('OBJECT__WITH__DOUBLENAME')
                    
                    obj = pm.ls(depNode.name())[0]
                    withAttr.append(obj)
                    depNode.setName(name)
                else:
                    withAttr.append(pm.ls(depNode.name())[0])
                    
            dagIt.next()
        except:
            dagIt.next()
   
    return withAttr

def get(Vlist=[], id=''):
    """
    Returns a custom node class instance for all custom nodes in the list.
    """    
    import mf.tools.lsSl
    
    Vlist = mf.tools.lsSl.lsSl(Vlist, 1, 0)
    node = register(id)
    result = []    
    
    for o in findObjectsByAttr(id, node[1]):
        if o in Vlist:
            result.append(node[2](Vlist=[o], edit=1))
    
    return result

def getAll(id='', **kwrdArgs):
    """
    Returns a custom node class instance for all custom nodes in the SCENE.
    """
    node = register(id)
    result = []    
    
    for o in findObjectsByAttr(id, node[1]):
        result.append(node[2](Vlist=[o], edit=1))
    
    return result

def set(Vlist=[], id=''):
    """
    Converts a node to a custom node by adding a custom attribute as marker.
    """
    import mf.tools.lsSl
    
    Vlist = mf.tools.lsSl.lsSl(Vlist, 1, 0)
    attr = register(id)[0]
    
    for o in Vlist:
        if o.hasAttr(attr):
            pass
        else:    
            o.addAttr(attr)
            
def remove(Vlist=[], id=''):
    """
    Remove a custom node attr.
    """
    import mf.tools.lsSl
    
    Vlist = mf.tools.lsSl.lsSl(Vlist, 1, 0)
    attr = register(id)[0]
    
    for o in Vlist:
        if o.hasAttr(attr):
            o.deleteAttr(attr)
        else:    
            pass
    
    
                
            
    