"""
example for the use of suffixes

A builder can create a new object, pass it to another function or do both 
things.

All packages must rename the original objects.
That must happen at the beginning of the building so all tools that use object 
names get the suffix passed on 

Here are some examples of how to build and pass names.
"""
import pymel.core as pm
from mf.tools import lsSl


def makeGroups(Vlist=[], Vsuffix=''):
    """
    Groups each object and names it with a base name 'j' and a number starting 
    with 1 e.g. 'g_1'
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    result = []
    
    for i in range(len(Vlist)):
        result.append(pm.group(Vlist[i], w=1,  n='g_%s%s'%(i+1, Vsuffix)))
    
    return result
              
def makeJoints(Vlist=[], Vsuffix=''):
    """
    Creates a joint for each object and names it with a base name 'j' and a 
    number starting with 1 e.g. 'j_1'
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    result = []
    
    for i in range(len(Vlist)):
        pm.select(cl=1)
        result.append(pm.joint(n='j_%s%s'%(i+1, Vsuffix)))
        pm.select(cl=1)
        
    return result
      
def makeGroupedJoints(Vlist=[], Vsuffix=''):
    """
    runs .makeJoints and than .makeGroups on the new joints
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    result = []
        
    joints = makeJoints(Vlist=Vlist, Vsuffix=Vsuffix)
    groups = makeGroups(Vlist=joints, Vsuffix=Vsuffix)
    pm.group(groups, n='main'+Vsuffix)
        
    return result

def groupThreeTimes(Vlist=[], Vsuffix=''):
    """
    runs .makeGroups three times with right naming
    """
    Vlist = lsSl.lsSl(Vlist, 3, 3)
    
    makeGroups(Vlist=[Vlist[0]], Vsuffix='_A'+Vsuffix)
    makeGroups(Vlist=[Vlist[1]], Vsuffix='_B'+Vsuffix)
    makeGroups(Vlist=[Vlist[2]], Vsuffix='_C'+Vsuffix)
    pass

        

    