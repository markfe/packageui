'''
functionPickerUI

SUMMARY
    FunctionPicker lists a tree of functions for easier managable use of scripts. It will list all Scripts downwards (recursive) from the input module

WORKFLOW
  
    The favorites are saved in the OS userfolder under maya/functionPicker/favorites.txt
    The assignFunction at the end is the function that is called when LMB clicked on an item.
    Icons defines the iconPaths for class and method. The script also looks for individual icons (32x32) in the path of the module. A function named
    test.mypackage.mymodule.myfunction() would have its icon in the folder .../test/mypackage/icons/mymodule.myfunction.png. Only PNG
    files are read. Transparency is possible!
    
    Call UI for Interface! All UI-functions are in the UI-Class

PARAMETER

    inpStr -   Expects a string of an module that is reachable through the sys.path
    
UPDATES:

    VERSION 1.1:
        
        -speed improvements:
            populateLister now gets modules as parameters to avoid double importing

SPEED TEST

.. code-block:: python

    t1 =  Timer( 'ef.builder.functionPicker.populateLister("mf.tools")', 'import ef.builder.functionPicker')
    t1.timeit(number = 100)
    
    t2 = Timer('ef.builder.functionPicker1_1.getModuleNames( eval("mf.tools") )' , 'import ef.builder.functionPicker1_1 ; import mf')
    t2.timeit(number = 100)
'''

#DOCS
__author__ = "Elias Fauser"
__version__ = '1.1'

#IMPORTS
import pymel.core as pm

import os
import inspect
import pkgutil

def getModuleNames(inp):
    
    """
    returns the module names as strings
    """

    pkgpath = os.path.dirname(inp.__file__)
    result = [name for _, name, _ in pkgutil.iter_modules([pkgpath])]
    
    return result

def getFunctionNames(inp):
    
    """
    returns the function names as strings
    """
    result = []
    
    for name in dir(inp):
        obj = getattr(inp, name)
        if inspect.isfunction(obj):

            if inp == inspect.getmodule(obj, _filename=None):
                result.append([inp.__name__ + '.' + name , 1 ])
                #print inspect.getdoc(obj)
        
        elif inspect.isclass(obj):
            #print 'class: ' + str(obj)
            if inp == inspect.getmodule(obj, _filename=None):
                result.append([inp.__name__ + '.' + name , 0 ])
            

    return result

def checkModule ( inpStr ):

    focusFlag = None
        
    try:
        #Try to import the name like it was given from the user
        inp = __import__(inpStr, fromlist=['dummy'])
        reload(inp)
        
    except ImportError:

        #Get the parent module and search under it for the function/module
        parentStr = inpStr.split('.')
        searchStr = parentStr[-1]
        parentStr = '.'.join(parentStr[:-1])
        
        try:
            #try to import that
            inp = __import__(parentStr, fromlist=['dummy'])
            
        except ImportError:
            
            raise Exception('Import failed')
        
        found = getFunctionNames(inp)[:]
        
        if found == []:
            found = getModuleNames(inp)[:]
            
        for obj in found:
            if obj.startswith(searchStr):
                inpStr = parentStr + '.' + obj
                try:
                    inp = __import__(inpStr, fromlist=['dummy'])
                    
                except ImportError:
                    
                    raise Exception('Import failed')
                
                focusFlag = inpStr
                break
    
    return inp , focusFlag

def getListerItems(inpModule):
    
    functions = []
    
    try:
        allPkgs = pkgutil.walk_packages(inpModule.__path__, inpModule.__name__+'.')
        for _, name, _  in allPkgs:
            try:
                name = __import__(name, fromlist= ['dummy'])
                functions.extend(getFunctionNames(name))
            except:
                pass
    except:
        functions.extend(getFunctionNames(inpModule))
    
    return functions

class UI:   
    
    relPath = os.path.dirname( os.path.realpath( __file__ ) )
    icons = [relPath+'\\UI_icons\\class.png',relPath+'\\UI_icons\\method.png']
    thisScript = __name__
    
    def __init__(self,inpStr):
        
        """
        Created the UI for the functionPicker
        """
        
        wndName = 'fpLister'
        wndWidth = 100
        wndHeight = 100
        
        try:
            pm.deleteUI('formLyt')
        except:
            pass
        
        #with pm.window(wndName , title = 'Function Picker '+__version__,w = wndWidth, vis = 0) as wnd:
        with pm.formLayout('formLyt', p='MainPane') as formL:
            self.functionLister = pm.nodeTreeLister('fLister',
                                               ruc = True,
                                               h = wndHeight,
                                               fcb = self.refreshFavorites)
            pm.formLayout(formL, e=True, af=((self.functionLister, 'top', 0),
                                                (self.functionLister, 'left', 0),
                                                (self.functionLister, 'bottom', 0),
                                                (self.functionLister, 'right', 0)))
    

        #Fill Lister
        focusStr = None
        if not inpStr == '':
            inp , focusStr  = checkModule(inpStr)
            functionList = getListerItems(inp)
            self.populateLister(self.functionLister, functionList , inp )        
        
        #Load favorites
        self.loadFavorites(self.functionLister)
        
        #Expand
        pm.nodeTreeLister(self.functionLister ,e = 1,etd = 0)
        
#        #Get focus on substitute
        if focusStr:
            pm.textField ('builderTF', e=1, text=focusStr)
#            pm.setFocus('builderTF')
        
        #Get focus of Item to avoid display Errors
        try:
            pm.nodeTreeLister(self.functionLister ,e = 1,sp = '')
        except:
            pass
        
        #wnd.show()
    
    def refreshFavorites(self, *args):
    
        """
        Is called for every change on the favorites and saves these changes into a local file.
        Filepath is: User/Documents/maya/functionPicker
        """
        userPath = os.path.abspath(os.path.expanduser("~"))
        dirPath = os.path.join(userPath,'maya','functionPicker')
        filePath = os.path.join(dirPath,'favorites.txt')
        
        
        if not os.path.exists(dirPath):
            try:
                os.chdir(os.path.join(userPath,'maya'))
                os.mkdir('functionPicker')
            except:
                print 'Saving the favourites failed'
                return
        
        try:
            os.remove(filePath)
        except:
            pass
        
        currentFavs = pm.nodeTreeLister(self.functionLister, q = 1, fl = True)
        
        if not args[1]:
            currentFavs.remove(args[0])

        f = open(filePath, 'w')
        
        try:         

            for each in currentFavs:
                f.write(each+'\n')
        finally:
            f.close()
            
    def loadFavorites(self,functionLister):
        
        """
        imports the favorites into the lister
        """
        
        userPath = os.path.abspath(os.path.expanduser("~"))
        filePath = os.path.join(userPath,'maya','functionPicker','favorites.txt')

        if not os.path.exists(filePath):
            return
        
        f = open(filePath, 'r')
    
        try:      
            line = ' '   
            while line:
                line = f.readline()
    
                clearLine = line.replace('\n','').strip()
                if len(clearLine) > 1:
                    pm.nodeTreeLister(self.functionLister, e = 1, addFavorite = clearLine)
    
        finally:
            f.close()
    
    def populateLister(self , functionLister , functionList , inpModule):
        
        """
        takes the functionlist and adds them to the nodeTreeLister. Also checks for icons in the files.
        """
        
        #get the global directory of the modules
        direct = '\\'.join(inpModule.__file__.split('\\')[:-1])
        
        for item in functionList:
            itemStr = item[0].replace('.', '/')
            split = item[0].split('.')
            
            if len(split)>1:
                fileName = '.'.join(split[-2:])+'.png'
            else:
                fileName = split[-1]+'.png'
            
            filePath = '\\'.join(split[len(inpModule.__name__.split('.')):-2])

            iconPath = os.path.join(direct,filePath,'icons',fileName)

            iconPathGeneral = '.'.join(iconPath.split('.')[0:-2])+'.png'
            #TODO: Improvement: Script still searches in every module for every function for the globalicon
            
            if os.path.exists(iconPath):
                icon = iconPath
                
            elif os.path.exists(iconPathGeneral): 
                icon = iconPathGeneral  
                 
            else:
                icon = self.icons[item[1]]
    
            pm.nodeTreeLister(functionLister ,e = 1,
                              add = [itemStr,
                                     icon,
                                     'import '+self.thisScript+'; '+self.thisScript+'.assignFunction("'+item[0]+'")'])   

    def reloadUI(self,inp):
        
        self.UI(inp)
    
def assignFunction(name):
    
    """
    function that is called when a node in the nodeTreeLister is clicked.
    !!!This function can be changed to fit individual purposes!!!
    
    Parameter:
        name - functionName (f.e. os.path.append)
    """
    
    parent = ('.'.join(name.split('.')[:-1]))
    Vbuilder = name
    funcDocStr = None
    
    try:
        exec ('import %s' % (Vbuilder))
        exec  ('reload (%s)' % (Vbuilder))
        Vfunc = eval(Vbuilder)
        
        funcDocStr = inspect.getdoc(Vfunc)
        
        if not funcDocStr:
            try:

                funcDocStr =+ inspect.getdoc(eval(parent + '.__init__'))
            except:
                pass
    except:
        try:
            exec ('import %s' % (parent))
            exec  ('reload (%s)' % (parent))
            Vfunc = eval(Vbuilder)
            funcDocStr = inspect.getdoc(Vfunc)
            #Prints docs - looks for function and class docs
            if 1: #not funcDocStr:
                try:
                    funcDocStr =+ inspect.getdoc(eval(Vbuilder + '.__init__'))                    
                except:
                    pass
                funcDocStr =+ inspect.getdoc(eval(parent))
            
        except:
            pass

    try:
        pm.textField ('builderTF', e=1, text=name)
        pm.setFocus('builderTF')
    except:
        pass
    
    if funcDocStr:
        print '\n'
        print funcDocStr
        
    try:        
        Vfunc = eval(Vbuilder + '.__init__')
        if inspect.ismethod(Vfunc):
            funcDocStr = True
    except:
        pass
    
    if not funcDocStr:
        print 'No docstring found.'
    
    
    
