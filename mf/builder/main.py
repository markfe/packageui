"""
All startup variables and sourcing.
"""
import os
import pymel.core as pm
import mf.tools.file as tf


def getRelativePath():
    """
    Gets the relative path this file is in.
    """
    Vpath = os.path.dirname( os.path.realpath( __file__ ) )
    return Vpath

def sourceAllMel():
    path = getRelativePath()
    for i in range(2):
        path = os.path.split(path)[0]
    melPath = os.path.join(path, 'mel')
    
    try:
        files = os.listdir(melPath)
    except:
        return
    
    for f in files:
        if os.path.splitext(f)[-1] == '.mel':
            print f
            scriptPath = os.path.abspath(os.path.join(melPath, f))
            pathMaya = scriptPath.replace('\\', '/')
            try:
                pm.mel.source(pathMaya)
            except Exception, detail:
                print detail