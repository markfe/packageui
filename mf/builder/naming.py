"""
tools to test correct naming
"""
import pymel.core as pm
from mf.tools import lsSl

def makeUnique(Vlist):
    dict = {}
    
    for o in Vlist:
        baseName = o.split('|')[-1]
        if not dict.has_key(baseName):
            dict[baseName] = [o]
        else:
            dict[baseName].append(o)
    
    print 'Objects with same names:' 
    for c, p in dict.iteritems():
        print c
        i=1
        for o in p:
            oldName = str(o)
            try:
                clash = pm.ls(c + str(i))
                if not clash == []:
                    print '## Name clash with new name! '
                    pm.rename(o, c + str(i) + '_UNIQUE', ignoreShape=1)
                else: 
                    pm.rename(o, c + str(i), ignoreShape=1)
                print oldName + ' >> ' + str(o)
            except:
                print str(o) + ' ## renaming failed!'
            i += 1
        print '------------------------------------------'
    return dict
            
        
    

def uniqueNames(Vlist=[]):
    """
    returns: list of objects that don't have unique names
    """
    def getSort(item):
        return item.split('|')[-1]

    Vlist = lsSl.lsSl(Vlist, 1, 0)
    Vresult = []
    
    for i in range(len(Vlist)): 
        try:
            nm = str(Vlist[i])
            splits = nm.split('|')
        except:
            print 'depreciated characters in:'
            print Vlist[i].name()   

        try:
            splits[1]
            Vresult.append(Vlist[i])
        except:
            pass

        Vresult = sorted(Vresult, key=getSort)
    return Vresult

    
def compareSceneObj(listA=[], listB=[]):
    """
    Takes 2 lists and returns the content of the first that's not in the second::

        listA = pm.ls()
        listB = pm.ls()

    returns: content of listB thats not in listA
    """
    listC = [item for item in listB if item not in listA]
    
    return listC

def readSuffix(Vlist=[], Vsuffix=''):
    """
    verifies a suffix for a list of objects

    returns:
    list of objects that don't have the suffix,
    list of filtered objects that don't have the suffix,
    list of objects that have the suffix more than once

    arguments:
    Vsuffix -- suffix to be verified
    """
    Vlist = lsSl.lsSl(Vlist, 1, 0)
    allObj = []
    multObj = []
    
    for i in range(len(Vlist)):
        Vsplit = Vlist[i].split(Vsuffix) 
        if len(Vsplit) == 1:
            allObj.append(Vlist[i]) 
            
        if len(Vsplit) > 2:
            multObj.append(Vlist[i])   
    
    filteredObj=[]
    for obj in allObj:
        if pm.nodeType(obj) == 'transform':
            filteredObj.append(obj)
    
    return [allObj, filteredObj, multObj]


