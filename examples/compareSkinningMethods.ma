//Maya ASCII 2012 scene
//Name: compareSkinningMethods.ma
//Last modified: Mon, Jun 24, 2013 09:26:34 PM
//Codeset: 1252
requires maya "2012";
requires "Mayatomr" "2012.0m - 3.9.1.43 ";
requires "stereoCamera" "10.0";
currentUnit -l meter -a degree -t pal;
fileInfo "application" "maya";
fileInfo "product" "Maya 2012";
fileInfo "version" "2012 x64";
fileInfo "cutIdentifier" "201109261240-811691";
fileInfo "osv" "Microsoft Windows 7 Business Edition, 64-bit Windows 7 Service Pack 1 (Build 7601)\n";
createNode transform -s -n "persp";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -2.2367157953596104 26.912440063815783 -2.8078999182868514 ;
	setAttr ".r" -type "double3" -455.13835272957556 92.199999999999164 -1.526666247102488e-013 ;
createNode camera -s -n "perspShape" -p "persp";
	setAttr -k off ".v" no;
	setAttr ".fl" 34.999999999999993;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".coi" 27.96697503981779;
	setAttr ".imn" -type "string" "persp";
	setAttr ".den" -type "string" "persp_depth";
	setAttr ".man" -type "string" "persp_mask";
	setAttr ".hc" -type "string" "viewSet -p %camera";
createNode transform -s -n "top";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 4.3850119748811158 -8.152717579212311 -2.3421142954245053 ;
	setAttr ".r" -type "double3" 89.999999999999986 0 0 ;
	setAttr ".rp" -type "double3" -1.7763568394002505e-017 0 -1.1368683772161603e-015 ;
	setAttr ".rpt" -type "double3" 1.8827878441876413e-016 8.9238027292565403e-016 1.1797130334425502e-015 ;
createNode camera -s -n "topShape" -p "top";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".coi" 8.1527175792123128;
	setAttr ".ow" 19.974626078116042;
	setAttr ".imn" -type "string" "top";
	setAttr ".den" -type "string" "top_depth";
	setAttr ".man" -type "string" "top_mask";
	setAttr ".tp" -type "double3" -10.787486555735597 2.2737367544323206e-013 -252.93179137761103 ;
	setAttr ".hc" -type "string" "viewSet -t %camera";
	setAttr ".o" yes;
createNode transform -s -n "front";
	setAttr ".v" no;
	setAttr ".t" -type "double3" 0 0 100.10000000000001 ;
createNode camera -s -n "frontShape" -p "front";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".coi" 100.10000000000001;
	setAttr ".ow" 30;
	setAttr ".imn" -type "string" "front";
	setAttr ".den" -type "string" "front_depth";
	setAttr ".man" -type "string" "front_mask";
	setAttr ".hc" -type "string" "viewSet -f %camera";
	setAttr ".o" yes;
createNode transform -s -n "side";
	setAttr ".v" no;
	setAttr ".t" -type "double3" -0.1078748655573564 8.1527175792123092 -2.5293179137761084 ;
	setAttr ".r" -type "double3" -89.999999999999986 89.999999999999986 0 ;
	setAttr ".rpt" -type "double3" -1.3900296498798412e-015 1.222907124375245e-015 -1.5149859325502568e-016 ;
createNode camera -s -n "sideShape" -p "side";
	setAttr -k off ".v" no;
	setAttr ".rnd" no;
	setAttr ".ncp" 0.001;
	setAttr ".fcp" 100;
	setAttr ".coi" 8.152717579212311;
	setAttr ".ow" 19.974626078116039;
	setAttr ".imn" -type "string" "side";
	setAttr ".den" -type "string" "side_depth";
	setAttr ".man" -type "string" "side_mask";
	setAttr ".tp" -type "double3" -10.78748655573596 0 -252.93179137761086 ;
	setAttr ".hc" -type "string" "viewSet -s %camera";
	setAttr ".o" yes;
createNode transform -n "all";
createNode joint -n "n0" -p "all";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
createNode joint -n "n1" -p "n0";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 100 0 0 1;
createNode joint -n "n2" -p "n1";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 200 0 0 1;
createNode joint -n "n3" -p "n2";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 300 0 0 1;
createNode joint -n "n4" -p "n3";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 400 0 0 1;
createNode joint -n "n5" -p "n4";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 500 0 0 1;
createNode joint -n "n6" -p "n5";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 600 0 0 1;
createNode transform -n "n7" -p "all";
	setAttr ".t" -type "double3" 3 0 0 ;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr ".s" -type "double3" 0.5 1 0.5 ;
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "nShape7" -p "n7";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".lev" 1;
	setAttr ".dr" 1;
createNode mesh -n "nShape1Orig7" -p "n7";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 56 ".uvst[0].uvsp[0:55]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998 0.58333331
		 0.61325186 0.54166663 0.61325186 0.49999997 0.61325186 0.45833331 0.61325186 0.41666669
		 0.61325186 0.625 0.61325186 0.375 0.61325186 0.58333331 0.53806388 0.54166663 0.53806388
		 0.49999997 0.53806388 0.45833331 0.53806388 0.41666666 0.53806388 0.625 0.53806388
		 0.375 0.53806388 0.58333331 0.46287593 0.54166663 0.46287593 0.49999994 0.46287593
		 0.45833331 0.46287593 0.41666666 0.46287593 0.625 0.46287593 0.375 0.46287593 0.58333331
		 0.38768798 0.54166663 0.38768798 0.49999994 0.38768798 0.45833331 0.38768798 0.41666666
		 0.38768798 0.625 0.38768798 0.375 0.38768798;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 38 ".vt[0:37]"  0.50000024 -2.5 -0.86602533 -0.49999985 -2.5 -0.86602557
		 -1 -2.5 -1.4901161e-007 -0.50000012 -2.5 0.86602539 0.49999997 -2.5 0.86602545 1 -2.5 0
		 0.50000024 2.5 -0.86602533 -0.49999985 2.5 -0.86602557 -1 2.5 -1.4901161e-007 -0.50000012 2.5 0.86602539
		 0.49999997 2.5 0.86602545 1 2.5 0 0 -2.5 0 0 2.5 0 1 1.5 0 0.49999997 1.5 0.86602545
		 -0.50000018 1.5 0.86602539 -1 1.5 -1.4901161e-007 -0.49999985 1.5 -0.86602557 0.50000024 1.5 -0.86602533
		 1 0.5 0 0.49999997 0.5 0.86602545 -0.50000018 0.5 0.86602539 -1 0.5 -1.4901161e-007
		 -0.49999985 0.5 -0.86602557 0.50000024 0.5 -0.86602533 1 -0.50000012 0 0.49999997 -0.50000012 0.86602545
		 -0.50000012 -0.50000012 0.86602539 -1 -0.50000012 -1.4901161e-007 -0.49999985 -0.50000012 -0.86602557
		 0.50000024 -0.50000012 -0.86602533 1 -1.5 0 0.49999997 -1.5 0.86602545 -0.50000012 -1.5 0.86602539
		 -1 -1.5 -1.4901161e-007 -0.49999985 -1.5 -0.86602557 0.50000024 -1.5 -0.86602533;
	setAttr -s 78 ".ed[0:77]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 37 0 1 36 0 2 35 0 3 34 0 4 33 0 5 32 0 12 0 1
		 12 1 1 12 2 1 12 3 1 12 4 1 12 5 1 6 13 1 7 13 1 8 13 1 9 13 1 10 13 1 11 13 1 14 11 0
		 15 10 0 14 15 1 16 9 0 15 16 1 17 8 0 16 17 1 18 7 0 17 18 1 19 6 0 18 19 1 19 14 1
		 20 14 0 21 15 0 20 21 1 22 16 0 21 22 1 23 17 0 22 23 1 24 18 0 23 24 1 25 19 0 24 25 1
		 25 20 1 26 20 0 27 21 0 26 27 1 28 22 0 27 28 1 29 23 0 28 29 1 30 24 0 29 30 1 31 25 0
		 30 31 1 31 26 1 32 26 0 33 27 0 32 33 1 34 28 0 33 34 1 35 29 0 34 35 1 36 30 0 35 36 1
		 37 31 0 36 37 1 37 32 1;
	setAttr -s 42 ".fc[0:41]" -type "polyFaces" 
		f 4 0 13 76 -13
		mu 0 4 6 7 53 55
		f 4 1 14 74 -14
		mu 0 4 7 8 52 53
		f 4 2 15 72 -15
		mu 0 4 8 9 51 52
		f 4 3 16 70 -16
		mu 0 4 9 10 50 51
		f 4 4 17 68 -17
		mu 0 4 10 11 49 50
		f 4 5 12 77 -18
		mu 0 4 11 12 54 49
		f 3 -1 -19 19
		mu 0 3 1 0 26
		f 3 -2 -20 20
		mu 0 3 2 1 26
		f 3 -3 -21 21
		mu 0 3 3 2 26
		f 3 -4 -22 22
		mu 0 3 4 3 26
		f 3 -5 -23 23
		mu 0 3 5 4 26
		f 3 -6 -24 18
		mu 0 3 0 5 26
		f 3 6 25 -25
		mu 0 3 24 23 27
		f 3 7 26 -26
		mu 0 3 23 22 27
		f 3 8 27 -27
		mu 0 3 22 21 27
		f 3 9 28 -28
		mu 0 3 21 20 27
		f 3 10 29 -29
		mu 0 3 20 25 27
		f 3 11 24 -30
		mu 0 3 25 24 27
		f 4 -33 30 -11 -32
		mu 0 4 29 28 18 17
		f 4 -35 31 -10 -34
		mu 0 4 30 29 17 16
		f 4 -37 33 -9 -36
		mu 0 4 31 30 16 15
		f 4 -39 35 -8 -38
		mu 0 4 32 31 15 14
		f 4 -41 37 -7 -40
		mu 0 4 34 32 14 13
		f 4 -42 39 -12 -31
		mu 0 4 28 33 19 18
		f 4 -45 42 32 -44
		mu 0 4 36 35 28 29
		f 4 -47 43 34 -46
		mu 0 4 37 36 29 30
		f 4 -49 45 36 -48
		mu 0 4 38 37 30 31
		f 4 -51 47 38 -50
		mu 0 4 39 38 31 32
		f 4 -53 49 40 -52
		mu 0 4 41 39 32 34
		f 4 -54 51 41 -43
		mu 0 4 35 40 33 28
		f 4 -57 54 44 -56
		mu 0 4 43 42 35 36
		f 4 -59 55 46 -58
		mu 0 4 44 43 36 37
		f 4 -61 57 48 -60
		mu 0 4 45 44 37 38
		f 4 -63 59 50 -62
		mu 0 4 46 45 38 39
		f 4 -65 61 52 -64
		mu 0 4 48 46 39 41
		f 4 -66 63 53 -55
		mu 0 4 42 47 40 35
		f 4 -69 66 56 -68
		mu 0 4 50 49 42 43
		f 4 -71 67 58 -70
		mu 0 4 51 50 43 44
		f 4 -73 69 60 -72
		mu 0 4 52 51 44 45
		f 4 -75 71 62 -74
		mu 0 4 53 52 45 46
		f 4 -77 73 64 -76
		mu 0 4 55 53 46 48
		f 4 -78 75 65 -67
		mu 0 4 49 54 47 42;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode joint -n "n8" -p "all";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 -200 1;
createNode joint -n "n9" -p "n8";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 100 0 -200 1;
createNode joint -n "n10" -p "n9";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 200 0 -200 1;
createNode joint -n "n11" -p "n10";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 300 0 -200 1;
createNode joint -n "n12" -p "n11";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 400 0 -200 1;
createNode joint -n "n13" -p "n12";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 500 0 -200 1;
createNode joint -n "n14" -p "n13";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 600 0 -200 1;
createNode transform -n "n15" -p "all";
	setAttr ".t" -type "double3" 3.4246179861501491 1.2776047194625891e-016 -2 ;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr ".s" -type "double3" 0.5 1 0.5 ;
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "nShape15" -p "n15";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".lev" 1;
	setAttr ".dr" 1;
createNode mesh -n "nShape2Orig15" -p "n15";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 56 ".uvst[0].uvsp[0:55]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998 0.58333331
		 0.61325186 0.54166663 0.61325186 0.49999997 0.61325186 0.45833331 0.61325186 0.41666669
		 0.61325186 0.625 0.61325186 0.375 0.61325186 0.58333331 0.53806388 0.54166663 0.53806388
		 0.49999997 0.53806388 0.45833331 0.53806388 0.41666666 0.53806388 0.625 0.53806388
		 0.375 0.53806388 0.58333331 0.46287593 0.54166663 0.46287593 0.49999994 0.46287593
		 0.45833331 0.46287593 0.41666666 0.46287593 0.625 0.46287593 0.375 0.46287593 0.58333331
		 0.38768798 0.54166663 0.38768798 0.49999994 0.38768798 0.45833331 0.38768798 0.41666666
		 0.38768798 0.625 0.38768798 0.375 0.38768798;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 38 ".vt[0:37]"  0.50000024 -2.5 -0.86602533 -0.49999985 -2.5 -0.86602557
		 -1 -2.5 -1.4901161e-007 -0.50000012 -2.5 0.86602539 0.49999997 -2.5 0.86602545 1 -2.5 0
		 0.50000024 2.5 -0.86602533 -0.49999985 2.5 -0.86602557 -1 2.5 -1.4901161e-007 -0.50000012 2.5 0.86602539
		 0.49999997 2.5 0.86602545 1 2.5 0 -2.5243548e-031 -2.5 0 -2.5243548e-031 2.5 0 1 1.5 0
		 0.49999997 1.5 0.86602545 -0.50000018 1.5 0.86602539 -1 1.5 -1.4901161e-007 -0.49999985 1.5 -0.86602557
		 0.50000024 1.5 -0.86602533 1 0.5 0 0.49999997 0.5 0.86602545 -0.50000018 0.5 0.86602539
		 -1 0.5 -1.4901161e-007 -0.49999985 0.5 -0.86602557 0.50000024 0.5 -0.86602533 1 -0.50000012 0
		 0.49999997 -0.50000012 0.86602545 -0.50000012 -0.50000012 0.86602539 -1 -0.50000012 -1.4901161e-007
		 -0.49999985 -0.50000012 -0.86602557 0.50000024 -0.50000012 -0.86602533 1 -1.5 0 0.49999997 -1.5 0.86602545
		 -0.50000012 -1.5 0.86602539 -1 -1.5 -1.4901161e-007 -0.49999985 -1.5 -0.86602557
		 0.50000024 -1.5 -0.86602533;
	setAttr -s 78 ".ed[0:77]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 37 0 1 36 0 2 35 0 3 34 0 4 33 0 5 32 0 12 0 1
		 12 1 1 12 2 1 12 3 1 12 4 1 12 5 1 6 13 1 7 13 1 8 13 1 9 13 1 10 13 1 11 13 1 14 11 0
		 15 10 0 14 15 1 16 9 0 15 16 1 17 8 0 16 17 1 18 7 0 17 18 1 19 6 0 18 19 1 19 14 1
		 20 14 0 21 15 0 20 21 1 22 16 0 21 22 1 23 17 0 22 23 1 24 18 0 23 24 1 25 19 0 24 25 1
		 25 20 1 26 20 0 27 21 0 26 27 1 28 22 0 27 28 1 29 23 0 28 29 1 30 24 0 29 30 1 31 25 0
		 30 31 1 31 26 1 32 26 0 33 27 0 32 33 1 34 28 0 33 34 1 35 29 0 34 35 1 36 30 0 35 36 1
		 37 31 0 36 37 1 37 32 1;
	setAttr -s 42 ".fc[0:41]" -type "polyFaces" 
		f 4 0 13 76 -13
		mu 0 4 6 7 53 55
		f 4 1 14 74 -14
		mu 0 4 7 8 52 53
		f 4 2 15 72 -15
		mu 0 4 8 9 51 52
		f 4 3 16 70 -16
		mu 0 4 9 10 50 51
		f 4 4 17 68 -17
		mu 0 4 10 11 49 50
		f 4 5 12 77 -18
		mu 0 4 11 12 54 49
		f 3 -1 -19 19
		mu 0 3 1 0 26
		f 3 -2 -20 20
		mu 0 3 2 1 26
		f 3 -3 -21 21
		mu 0 3 3 2 26
		f 3 -4 -22 22
		mu 0 3 4 3 26
		f 3 -5 -23 23
		mu 0 3 5 4 26
		f 3 -6 -24 18
		mu 0 3 0 5 26
		f 3 6 25 -25
		mu 0 3 24 23 27
		f 3 7 26 -26
		mu 0 3 23 22 27
		f 3 8 27 -27
		mu 0 3 22 21 27
		f 3 9 28 -28
		mu 0 3 21 20 27
		f 3 10 29 -29
		mu 0 3 20 25 27
		f 3 11 24 -30
		mu 0 3 25 24 27
		f 4 -33 30 -11 -32
		mu 0 4 29 28 18 17
		f 4 -35 31 -10 -34
		mu 0 4 30 29 17 16
		f 4 -37 33 -9 -36
		mu 0 4 31 30 16 15
		f 4 -39 35 -8 -38
		mu 0 4 32 31 15 14
		f 4 -41 37 -7 -40
		mu 0 4 34 32 14 13
		f 4 -42 39 -12 -31
		mu 0 4 28 33 19 18
		f 4 -45 42 32 -44
		mu 0 4 36 35 28 29
		f 4 -47 43 34 -46
		mu 0 4 37 36 29 30
		f 4 -49 45 36 -48
		mu 0 4 38 37 30 31
		f 4 -51 47 38 -50
		mu 0 4 39 38 31 32
		f 4 -53 49 40 -52
		mu 0 4 41 39 32 34
		f 4 -54 51 41 -43
		mu 0 4 35 40 33 28
		f 4 -57 54 44 -56
		mu 0 4 43 42 35 36
		f 4 -59 55 46 -58
		mu 0 4 44 43 36 37
		f 4 -61 57 48 -60
		mu 0 4 45 44 37 38
		f 4 -63 59 50 -62
		mu 0 4 46 45 38 39
		f 4 -65 61 52 -64
		mu 0 4 48 46 39 41
		f 4 -66 63 53 -55
		mu 0 4 42 47 40 35
		f 4 -69 66 56 -68
		mu 0 4 50 49 42 43
		f 4 -71 67 58 -70
		mu 0 4 51 50 43 44
		f 4 -73 69 60 -72
		mu 0 4 52 51 44 45
		f 4 -75 71 62 -74
		mu 0 4 53 52 45 46
		f 4 -77 73 64 -76
		mu 0 4 55 53 46 48
		f 4 -78 75 65 -67
		mu 0 4 49 54 47 42;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode joint -n "n16" -p "all";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 -600 1;
createNode joint -n "n17" -p "n16";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 100 0 -600 1;
createNode joint -n "n18" -p "n17";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 200 0 -600 1;
createNode joint -n "n19" -p "n18";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 300 0 -600 1;
createNode joint -n "n20" -p "n19";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 400 0 -600 1;
createNode joint -n "n21" -p "n20";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 500 0 -600 1;
createNode joint -n "n22" -p "n21";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 600 0 -600 1;
createNode joint -n "n23" -p "n22";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 600 0 -200 1;
createNode joint -n "n24" -p "n22";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 600 0 -600 1;
	setAttr ".radi" 1.2100000000000002;
createNode joint -n "n25" -p "n24";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 600 0 -600 1;
	setAttr ".radi" 1.1;
createNode orientConstraint -n "n26" -p "n25";
	addAttr -ci true -k true -sn "w0" -ln "joint7W0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "joint6W1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 0 -20.436424647876429 0 ;
	setAttr ".int" 2;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "n27" -p "n25";
	addAttr -ci true -sn "joint_bind" -ln "joint_bind" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr -l on ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 600 0 -600 1;
	setAttr ".sd" 3;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "0";
createNode transform -n "n28" -p "n27";
	addAttr -ci true -sn "center_pivot" -ln "center_pivot" -at "double";
	setAttr ".s" -type "double3" 0.1 1 1 ;
	setAttr -cb on ".center_pivot" 1;
createNode mesh -n "n28Shape" -p "n28";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".lev" 0;
	setAttr -s 12 ".pt[0:11]" -type "float3"  -2.2888183e-007 1.0000002 
		0 0.99999982 1.5258789e-007 0 1.5 -0.5 0 1.0000001 -1.1444092e-007 0 3.8146972e-008 
		0.99999994 0 -0.5 1.5 0 -1.0000002 2.2888183e-007 0 -1.5258789e-007 -0.99999982 0 
		0.5 -1.5 0 1.1444092e-007 -1.0000001 0 -0.99999994 -3.8146972e-008 0 -1.5 0.5 0;
	setAttr -s 12 ".vt[0:11]"  0.50000024 -0.5 -0.86602533 -0.49999985 -0.5 -0.86602557
		 -1 -0.5 -1.4901161e-007 -0.50000012 -0.5 0.86602539 0.49999997 -0.5 0.86602545 1 -0.5 0
		 0.50000024 0.5 -0.86602533 -0.49999985 0.5 -0.86602557 -1 0.5 -1.4901161e-007 -0.50000012 0.5 0.86602539
		 0.49999997 0.5 0.86602545 1 0.5 0;
	setAttr -s 18 ".ed[0:17]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 6 0 1 7 0 2 8 0 3 9 0 4 10 0 5 11 0;
	setAttr -s 8 ".fc[0:7]" -type "polyFaces" 
		f 4 0 13 -7 -13
		mu 0 4 6 7 14 13
		f 4 1 14 -8 -14
		mu 0 4 7 8 15 14
		f 4 2 15 -9 -15
		mu 0 4 8 9 16 15
		f 4 3 16 -10 -16
		mu 0 4 9 10 17 16
		f 4 4 17 -11 -17
		mu 0 4 10 11 18 17
		f 4 5 12 -12 -18
		mu 0 4 11 12 19 18
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 6 6 7 8 9 10 11
		mu 0 6 24 23 22 21 20 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode joint -n "n29" -p "n21";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 500 0 -600 1;
	setAttr ".radi" 1.2100000000000002;
createNode joint -n "n30" -p "n29";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 500 0 -600 1;
	setAttr ".radi" 1.1;
createNode orientConstraint -n "n31" -p "n30";
	addAttr -ci true -k true -sn "w0" -ln "joint6W0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "joint5W1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 0 -20.436424647876432 0 ;
	setAttr ".int" 2;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "n32" -p "n30";
	addAttr -ci true -sn "joint_bind" -ln "joint_bind" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr -l on ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 500 0 -600 1;
	setAttr ".sd" 3;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "0";
createNode transform -n "n33" -p "n32";
	addAttr -ci true -sn "center_pivot" -ln "center_pivot" -at "double";
	setAttr ".s" -type "double3" 0.1 1 1 ;
	setAttr -cb on ".center_pivot" 1;
createNode mesh -n "n33Shape" -p "n33";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".lev" 0;
	setAttr -s 12 ".pt[0:11]" -type "float3"  -2.2888183e-007 1.0000002 
		0 0.99999982 1.5258789e-007 0 1.5 -0.5 0 1.0000001 -1.1444092e-007 0 3.8146972e-008 
		0.99999994 0 -0.5 1.5 0 -1.0000002 2.2888183e-007 0 -1.5258789e-007 -0.99999982 0 
		0.5 -1.5 0 1.1444092e-007 -1.0000001 0 -0.99999994 -3.8146972e-008 0 -1.5 0.5 0;
	setAttr -s 12 ".vt[0:11]"  0.50000024 -0.5 -0.86602533 -0.49999985 -0.5 -0.86602557
		 -1 -0.5 -1.4901161e-007 -0.50000012 -0.5 0.86602539 0.49999997 -0.5 0.86602545 1 -0.5 0
		 0.50000024 0.5 -0.86602533 -0.49999985 0.5 -0.86602557 -1 0.5 -1.4901161e-007 -0.50000012 0.5 0.86602539
		 0.49999997 0.5 0.86602545 1 0.5 0;
	setAttr -s 18 ".ed[0:17]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 6 0 1 7 0 2 8 0 3 9 0 4 10 0 5 11 0;
	setAttr -s 8 ".fc[0:7]" -type "polyFaces" 
		f 4 0 13 -7 -13
		mu 0 4 6 7 14 13
		f 4 1 14 -8 -14
		mu 0 4 7 8 15 14
		f 4 2 15 -9 -15
		mu 0 4 8 9 16 15
		f 4 3 16 -10 -16
		mu 0 4 9 10 17 16
		f 4 4 17 -11 -17
		mu 0 4 10 11 18 17
		f 4 5 12 -12 -18
		mu 0 4 11 12 19 18
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 6 6 7 8 9 10 11
		mu 0 6 24 23 22 21 20 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode joint -n "n34" -p "n20";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 400 0 -600 1;
	setAttr ".radi" 1.2100000000000002;
createNode joint -n "n35" -p "n34";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 400 0 -600 1;
	setAttr ".radi" 1.1;
createNode orientConstraint -n "n36" -p "n35";
	addAttr -ci true -k true -sn "w0" -ln "joint5W0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "joint4W1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 0 -20.436424647876439 0 ;
	setAttr ".int" 2;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "n37" -p "n35";
	addAttr -ci true -sn "joint_bind" -ln "joint_bind" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr -l on ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 400 0 -600 1;
	setAttr ".sd" 3;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "0";
createNode transform -n "n38" -p "n37";
	addAttr -ci true -sn "center_pivot" -ln "center_pivot" -at "double";
	setAttr ".s" -type "double3" 0.1 1 1 ;
	setAttr -cb on ".center_pivot" 1;
createNode mesh -n "n38Shape" -p "n38";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".lev" 0;
	setAttr -s 12 ".pt[0:11]" -type "float3"  -2.2888183e-007 1.0000002 
		0 0.99999982 1.5258789e-007 0 1.5 -0.5 0 1.0000001 -1.1444092e-007 0 3.8146972e-008 
		0.99999994 0 -0.5 1.5 0 -1.0000002 2.2888183e-007 0 -1.5258789e-007 -0.99999982 0 
		0.5 -1.5 0 1.1444092e-007 -1.0000001 0 -0.99999994 -3.8146972e-008 0 -1.5 0.5 0;
	setAttr -s 12 ".vt[0:11]"  0.50000024 -0.5 -0.86602533 -0.49999985 -0.5 -0.86602557
		 -1 -0.5 -1.4901161e-007 -0.50000012 -0.5 0.86602539 0.49999997 -0.5 0.86602545 1 -0.5 0
		 0.50000024 0.5 -0.86602533 -0.49999985 0.5 -0.86602557 -1 0.5 -1.4901161e-007 -0.50000012 0.5 0.86602539
		 0.49999997 0.5 0.86602545 1 0.5 0;
	setAttr -s 18 ".ed[0:17]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 6 0 1 7 0 2 8 0 3 9 0 4 10 0 5 11 0;
	setAttr -s 8 ".fc[0:7]" -type "polyFaces" 
		f 4 0 13 -7 -13
		mu 0 4 6 7 14 13
		f 4 1 14 -8 -14
		mu 0 4 7 8 15 14
		f 4 2 15 -9 -15
		mu 0 4 8 9 16 15
		f 4 3 16 -10 -16
		mu 0 4 9 10 17 16
		f 4 4 17 -11 -17
		mu 0 4 10 11 18 17
		f 4 5 12 -12 -18
		mu 0 4 11 12 19 18
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 6 6 7 8 9 10 11
		mu 0 6 24 23 22 21 20 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode joint -n "n39" -p "n19";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 300 0 -600 1;
	setAttr ".radi" 1.2100000000000002;
createNode joint -n "n40" -p "n39";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 300 0 -600 1;
	setAttr ".radi" 1.1;
createNode orientConstraint -n "n41" -p "n40";
	addAttr -ci true -k true -sn "w0" -ln "joint4W0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "joint3W1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 0 -20.436424647876432 0 ;
	setAttr ".int" 2;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "n42" -p "n40";
	addAttr -ci true -sn "joint_bind" -ln "joint_bind" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr -l on ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 300 0 -600 1;
	setAttr ".sd" 3;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "0";
createNode transform -n "n43" -p "n42";
	addAttr -ci true -sn "center_pivot" -ln "center_pivot" -at "double";
	setAttr ".s" -type "double3" 0.1 1 1 ;
	setAttr -cb on ".center_pivot" 1;
createNode mesh -n "n43Shape" -p "n43";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".lev" 0;
	setAttr -s 12 ".pt[0:11]" -type "float3"  -2.2888183e-007 1.0000002 
		0 0.99999982 1.5258789e-007 0 1.5 -0.5 0 1.0000001 -1.1444092e-007 0 3.8146972e-008 
		0.99999994 0 -0.5 1.5 0 -1.0000002 2.2888183e-007 0 -1.5258789e-007 -0.99999982 0 
		0.5 -1.5 0 1.1444092e-007 -1.0000001 0 -0.99999994 -3.8146972e-008 0 -1.5 0.5 0;
	setAttr -s 12 ".vt[0:11]"  0.50000024 -0.5 -0.86602533 -0.49999985 -0.5 -0.86602557
		 -1 -0.5 -1.4901161e-007 -0.50000012 -0.5 0.86602539 0.49999997 -0.5 0.86602545 1 -0.5 0
		 0.50000024 0.5 -0.86602533 -0.49999985 0.5 -0.86602557 -1 0.5 -1.4901161e-007 -0.50000012 0.5 0.86602539
		 0.49999997 0.5 0.86602545 1 0.5 0;
	setAttr -s 18 ".ed[0:17]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 6 0 1 7 0 2 8 0 3 9 0 4 10 0 5 11 0;
	setAttr -s 8 ".fc[0:7]" -type "polyFaces" 
		f 4 0 13 -7 -13
		mu 0 4 6 7 14 13
		f 4 1 14 -8 -14
		mu 0 4 7 8 15 14
		f 4 2 15 -9 -15
		mu 0 4 8 9 16 15
		f 4 3 16 -10 -16
		mu 0 4 9 10 17 16
		f 4 4 17 -11 -17
		mu 0 4 10 11 18 17
		f 4 5 12 -12 -18
		mu 0 4 11 12 19 18
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 6 6 7 8 9 10 11
		mu 0 6 24 23 22 21 20 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode joint -n "n44" -p "n18";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 200 0 -600 1;
	setAttr ".radi" 1.2100000000000002;
createNode joint -n "n45" -p "n44";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 200 0 -600 1;
	setAttr ".radi" 1.1;
createNode orientConstraint -n "n46" -p "n45";
	addAttr -ci true -k true -sn "w0" -ln "joint3W0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "joint2W1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 0 -20.436424647876429 0 ;
	setAttr ".int" 2;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "n47" -p "n45";
	addAttr -ci true -sn "joint_bind" -ln "joint_bind" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr -l on ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 200 0 -600 1;
	setAttr ".sd" 3;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "0";
createNode transform -n "n48" -p "n47";
	addAttr -ci true -sn "center_pivot" -ln "center_pivot" -at "double";
	setAttr ".s" -type "double3" 0.1 1 1 ;
	setAttr -cb on ".center_pivot" 1;
createNode mesh -n "n48Shape" -p "n48";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".lev" 0;
	setAttr -s 12 ".pt[0:11]" -type "float3"  -2.2888183e-007 1.0000002 
		0 0.99999982 1.5258789e-007 0 1.5 -0.5 0 1.0000001 -1.1444092e-007 0 3.8146972e-008 
		0.99999994 0 -0.5 1.5 0 -1.0000002 2.2888183e-007 0 -1.5258789e-007 -0.99999982 0 
		0.5 -1.5 0 1.1444092e-007 -1.0000001 0 -0.99999994 -3.8146972e-008 0 -1.5 0.5 0;
	setAttr -s 12 ".vt[0:11]"  0.50000024 -0.5 -0.86602533 -0.49999985 -0.5 -0.86602557
		 -1 -0.5 -1.4901161e-007 -0.50000012 -0.5 0.86602539 0.49999997 -0.5 0.86602545 1 -0.5 0
		 0.50000024 0.5 -0.86602533 -0.49999985 0.5 -0.86602557 -1 0.5 -1.4901161e-007 -0.50000012 0.5 0.86602539
		 0.49999997 0.5 0.86602545 1 0.5 0;
	setAttr -s 18 ".ed[0:17]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 6 0 1 7 0 2 8 0 3 9 0 4 10 0 5 11 0;
	setAttr -s 8 ".fc[0:7]" -type "polyFaces" 
		f 4 0 13 -7 -13
		mu 0 4 6 7 14 13
		f 4 1 14 -8 -14
		mu 0 4 7 8 15 14
		f 4 2 15 -9 -15
		mu 0 4 8 9 16 15
		f 4 3 16 -10 -16
		mu 0 4 9 10 17 16
		f 4 4 17 -11 -17
		mu 0 4 10 11 18 17
		f 4 5 12 -12 -18
		mu 0 4 11 12 19 18
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 6 6 7 8 9 10 11
		mu 0 6 24 23 22 21 20 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode joint -n "n49" -p "n17";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 100 0 -600 1;
	setAttr ".radi" 1.2100000000000002;
createNode joint -n "n50" -p "n49";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 100 0 -600 1;
	setAttr ".radi" 1.1;
createNode orientConstraint -n "n51" -p "n50";
	addAttr -ci true -k true -sn "w0" -ln "joint2W0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "joint9W1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 0 -20.436424647876429 0 ;
	setAttr ".int" 2;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "n52" -p "n50";
	addAttr -ci true -sn "joint_bind" -ln "joint_bind" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr -l on ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 100 0 -600 1;
	setAttr ".sd" 3;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "0";
createNode transform -n "n53" -p "n52";
	addAttr -ci true -sn "center_pivot" -ln "center_pivot" -at "double";
	setAttr ".s" -type "double3" 0.1 1 1 ;
	setAttr -cb on ".center_pivot" 1;
createNode mesh -n "n53Shape" -p "n53";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".lev" 0;
	setAttr -s 12 ".pt[0:11]" -type "float3"  -2.2888183e-007 1.0000002 
		0 0.99999982 1.5258789e-007 0 1.5 -0.5 0 1.0000001 -1.1444092e-007 0 3.8146972e-008 
		0.99999994 0 -0.5 1.5 0 -1.0000002 2.2888183e-007 0 -1.5258789e-007 -0.99999982 0 
		0.5 -1.5 0 1.1444092e-007 -1.0000001 0 -0.99999994 -3.8146972e-008 0 -1.5 0.5 0;
	setAttr -s 12 ".vt[0:11]"  0.50000024 -0.5 -0.86602533 -0.49999985 -0.5 -0.86602557
		 -1 -0.5 -1.4901161e-007 -0.50000012 -0.5 0.86602539 0.49999997 -0.5 0.86602545 1 -0.5 0
		 0.50000024 0.5 -0.86602533 -0.49999985 0.5 -0.86602557 -1 0.5 -1.4901161e-007 -0.50000012 0.5 0.86602539
		 0.49999997 0.5 0.86602545 1 0.5 0;
	setAttr -s 18 ".ed[0:17]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 6 0 1 7 0 2 8 0 3 9 0 4 10 0 5 11 0;
	setAttr -s 8 ".fc[0:7]" -type "polyFaces" 
		f 4 0 13 -7 -13
		mu 0 4 6 7 14 13
		f 4 1 14 -8 -14
		mu 0 4 7 8 15 14
		f 4 2 15 -9 -15
		mu 0 4 8 9 16 15
		f 4 3 16 -10 -16
		mu 0 4 9 10 17 16
		f 4 4 17 -11 -17
		mu 0 4 10 11 18 17
		f 4 5 12 -12 -18
		mu 0 4 11 12 19 18
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 6 6 7 8 9 10 11
		mu 0 6 24 23 22 21 20 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "n54" -p "all";
	setAttr ".t" -type "double3" 3.5 -2.4428756802111855e-017 -6 ;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr ".s" -type "double3" 0.5 1 0.5 ;
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "nShape54" -p "n54";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".lev" 1;
	setAttr ".dr" 1;
createNode mesh -n "nShape1Orig54" -p "n54";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 56 ".uvst[0].uvsp[0:55]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998 0.58333331
		 0.61325186 0.54166663 0.61325186 0.49999997 0.61325186 0.45833331 0.61325186 0.41666669
		 0.61325186 0.625 0.61325186 0.375 0.61325186 0.58333331 0.53806388 0.54166663 0.53806388
		 0.49999997 0.53806388 0.45833331 0.53806388 0.41666666 0.53806388 0.625 0.53806388
		 0.375 0.53806388 0.58333331 0.46287593 0.54166663 0.46287593 0.49999994 0.46287593
		 0.45833331 0.46287593 0.41666666 0.46287593 0.625 0.46287593 0.375 0.46287593 0.58333331
		 0.38768798 0.54166663 0.38768798 0.49999994 0.38768798 0.45833331 0.38768798 0.41666666
		 0.38768798 0.625 0.38768798 0.375 0.38768798;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 38 ".vt[0:37]"  0.50000024 -2.5 -0.86602533 -0.49999985 -2.5 -0.86602557
		 -1 -2.5 -1.4901161e-007 -0.50000012 -2.5 0.86602539 0.49999997 -2.5 0.86602545 1 -2.5 0
		 0.50000024 2.5 -0.86602533 -0.49999985 2.5 -0.86602557 -1 2.5 -1.4901161e-007 -0.50000012 2.5 0.86602539
		 0.49999997 2.5 0.86602545 1 2.5 0 0 -2.5 0 0 2.5 0 1 1.5 0 0.49999997 1.5 0.86602545
		 -0.50000018 1.5 0.86602539 -1 1.5 -1.4901161e-007 -0.49999985 1.5 -0.86602557 0.50000024 1.5 -0.86602533
		 1 0.5 0 0.49999997 0.5 0.86602545 -0.50000018 0.5 0.86602539 -1 0.5 -1.4901161e-007
		 -0.49999985 0.5 -0.86602557 0.50000024 0.5 -0.86602533 1 -0.50000012 0 0.49999997 -0.50000012 0.86602545
		 -0.50000012 -0.50000012 0.86602539 -1 -0.50000012 -1.4901161e-007 -0.49999985 -0.50000012 -0.86602557
		 0.50000024 -0.50000012 -0.86602533 1 -1.5 0 0.49999997 -1.5 0.86602545 -0.50000012 -1.5 0.86602539
		 -1 -1.5 -1.4901161e-007 -0.49999985 -1.5 -0.86602557 0.50000024 -1.5 -0.86602533;
	setAttr -s 78 ".ed[0:77]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 37 0 1 36 0 2 35 0 3 34 0 4 33 0 5 32 0 12 0 1
		 12 1 1 12 2 1 12 3 1 12 4 1 12 5 1 6 13 1 7 13 1 8 13 1 9 13 1 10 13 1 11 13 1 14 11 0
		 15 10 0 14 15 1 16 9 0 15 16 1 17 8 0 16 17 1 18 7 0 17 18 1 19 6 0 18 19 1 19 14 1
		 20 14 0 21 15 0 20 21 1 22 16 0 21 22 1 23 17 0 22 23 1 24 18 0 23 24 1 25 19 0 24 25 1
		 25 20 1 26 20 0 27 21 0 26 27 1 28 22 0 27 28 1 29 23 0 28 29 1 30 24 0 29 30 1 31 25 0
		 30 31 1 31 26 1 32 26 0 33 27 0 32 33 1 34 28 0 33 34 1 35 29 0 34 35 1 36 30 0 35 36 1
		 37 31 0 36 37 1 37 32 1;
	setAttr -s 42 ".fc[0:41]" -type "polyFaces" 
		f 4 0 13 76 -13
		mu 0 4 6 7 53 55
		f 4 1 14 74 -14
		mu 0 4 7 8 52 53
		f 4 2 15 72 -15
		mu 0 4 8 9 51 52
		f 4 3 16 70 -16
		mu 0 4 9 10 50 51
		f 4 4 17 68 -17
		mu 0 4 10 11 49 50
		f 4 5 12 77 -18
		mu 0 4 11 12 54 49
		f 3 -1 -19 19
		mu 0 3 1 0 26
		f 3 -2 -20 20
		mu 0 3 2 1 26
		f 3 -3 -21 21
		mu 0 3 3 2 26
		f 3 -4 -22 22
		mu 0 3 4 3 26
		f 3 -5 -23 23
		mu 0 3 5 4 26
		f 3 -6 -24 18
		mu 0 3 0 5 26
		f 3 6 25 -25
		mu 0 3 24 23 27
		f 3 7 26 -26
		mu 0 3 23 22 27
		f 3 8 27 -27
		mu 0 3 22 21 27
		f 3 9 28 -28
		mu 0 3 21 20 27
		f 3 10 29 -29
		mu 0 3 20 25 27
		f 3 11 24 -30
		mu 0 3 25 24 27
		f 4 -33 30 -11 -32
		mu 0 4 29 28 18 17
		f 4 -35 31 -10 -34
		mu 0 4 30 29 17 16
		f 4 -37 33 -9 -36
		mu 0 4 31 30 16 15
		f 4 -39 35 -8 -38
		mu 0 4 32 31 15 14
		f 4 -41 37 -7 -40
		mu 0 4 34 32 14 13
		f 4 -42 39 -12 -31
		mu 0 4 28 33 19 18
		f 4 -45 42 32 -44
		mu 0 4 36 35 28 29
		f 4 -47 43 34 -46
		mu 0 4 37 36 29 30
		f 4 -49 45 36 -48
		mu 0 4 38 37 30 31
		f 4 -51 47 38 -50
		mu 0 4 39 38 31 32
		f 4 -53 49 40 -52
		mu 0 4 41 39 32 34
		f 4 -54 51 41 -43
		mu 0 4 35 40 33 28
		f 4 -57 54 44 -56
		mu 0 4 43 42 35 36
		f 4 -59 55 46 -58
		mu 0 4 44 43 36 37
		f 4 -61 57 48 -60
		mu 0 4 45 44 37 38
		f 4 -63 59 50 -62
		mu 0 4 46 45 38 39
		f 4 -65 61 52 -64
		mu 0 4 48 46 39 41
		f 4 -66 63 53 -55
		mu 0 4 42 47 40 35
		f 4 -69 66 56 -68
		mu 0 4 50 49 42 43
		f 4 -71 67 58 -70
		mu 0 4 51 50 43 44
		f 4 -73 69 60 -72
		mu 0 4 52 51 44 45
		f 4 -75 71 62 -74
		mu 0 4 53 52 45 46
		f 4 -77 73 64 -76
		mu 0 4 55 53 46 48
		f 4 -78 75 65 -67
		mu 0 4 49 54 47 42;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "nShape3Orig54" -p "n54";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 56 ".uvst[0].uvsp[0:55]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998 0.58333331
		 0.61325186 0.54166663 0.61325186 0.49999997 0.61325186 0.45833331 0.61325186 0.41666669
		 0.61325186 0.625 0.61325186 0.375 0.61325186 0.58333331 0.53806388 0.54166663 0.53806388
		 0.49999997 0.53806388 0.45833331 0.53806388 0.41666666 0.53806388 0.625 0.53806388
		 0.375 0.53806388 0.58333331 0.46287593 0.54166663 0.46287593 0.49999994 0.46287593
		 0.45833331 0.46287593 0.41666666 0.46287593 0.625 0.46287593 0.375 0.46287593 0.58333331
		 0.38768798 0.54166663 0.38768798 0.49999994 0.38768798 0.45833331 0.38768798 0.41666666
		 0.38768798 0.625 0.38768798 0.375 0.38768798;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 38 ".vt[0:37]"  0.50000024 -2.5 -0.86602533 -0.49999985 -2.5 -0.86602557
		 -1 -2.5 -1.4901161e-007 -0.50000012 -2.5 0.86602539 0.49999997 -2.5 0.86602545 1 -2.5 0
		 0.50000024 2.5 -0.86602533 -0.49999985 2.5 -0.86602557 -1 2.5 -1.4901161e-007 -0.50000012 2.5 0.86602539
		 0.49999997 2.5 0.86602545 1 2.5 0 0 -2.5 0 0 2.5 0 1 1.5 0 0.49999997 1.5 0.86602545
		 -0.50000018 1.5 0.86602539 -1 1.5 -1.4901161e-007 -0.49999985 1.5 -0.86602557 0.50000024 1.5 -0.86602533
		 1 0.5 0 0.49999997 0.5 0.86602545 -0.50000018 0.5 0.86602539 -1 0.5 -1.4901161e-007
		 -0.49999985 0.5 -0.86602557 0.50000024 0.5 -0.86602533 1 -0.50000012 0 0.49999997 -0.50000012 0.86602545
		 -0.50000012 -0.50000012 0.86602539 -1 -0.50000012 -1.4901161e-007 -0.49999985 -0.50000012 -0.86602557
		 0.50000024 -0.50000012 -0.86602533 1 -1.5 0 0.49999997 -1.5 0.86602545 -0.50000012 -1.5 0.86602539
		 -1 -1.5 -1.4901161e-007 -0.49999985 -1.5 -0.86602557 0.50000024 -1.5 -0.86602533;
	setAttr -s 78 ".ed[0:77]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 37 0 1 36 0 2 35 0 3 34 0 4 33 0 5 32 0 12 0 1
		 12 1 1 12 2 1 12 3 1 12 4 1 12 5 1 6 13 1 7 13 1 8 13 1 9 13 1 10 13 1 11 13 1 14 11 0
		 15 10 0 14 15 1 16 9 0 15 16 1 17 8 0 16 17 1 18 7 0 17 18 1 19 6 0 18 19 1 19 14 1
		 20 14 0 21 15 0 20 21 1 22 16 0 21 22 1 23 17 0 22 23 1 24 18 0 23 24 1 25 19 0 24 25 1
		 25 20 1 26 20 0 27 21 0 26 27 1 28 22 0 27 28 1 29 23 0 28 29 1 30 24 0 29 30 1 31 25 0
		 30 31 1 31 26 1 32 26 0 33 27 0 32 33 1 34 28 0 33 34 1 35 29 0 34 35 1 36 30 0 35 36 1
		 37 31 0 36 37 1 37 32 1;
	setAttr -s 42 ".fc[0:41]" -type "polyFaces" 
		f 4 0 13 76 -13
		mu 0 4 6 7 53 55
		f 4 1 14 74 -14
		mu 0 4 7 8 52 53
		f 4 2 15 72 -15
		mu 0 4 8 9 51 52
		f 4 3 16 70 -16
		mu 0 4 9 10 50 51
		f 4 4 17 68 -17
		mu 0 4 10 11 49 50
		f 4 5 12 77 -18
		mu 0 4 11 12 54 49
		f 3 -1 -19 19
		mu 0 3 1 0 26
		f 3 -2 -20 20
		mu 0 3 2 1 26
		f 3 -3 -21 21
		mu 0 3 3 2 26
		f 3 -4 -22 22
		mu 0 3 4 3 26
		f 3 -5 -23 23
		mu 0 3 5 4 26
		f 3 -6 -24 18
		mu 0 3 0 5 26
		f 3 6 25 -25
		mu 0 3 24 23 27
		f 3 7 26 -26
		mu 0 3 23 22 27
		f 3 8 27 -27
		mu 0 3 22 21 27
		f 3 9 28 -28
		mu 0 3 21 20 27
		f 3 10 29 -29
		mu 0 3 20 25 27
		f 3 11 24 -30
		mu 0 3 25 24 27
		f 4 -33 30 -11 -32
		mu 0 4 29 28 18 17
		f 4 -35 31 -10 -34
		mu 0 4 30 29 17 16
		f 4 -37 33 -9 -36
		mu 0 4 31 30 16 15
		f 4 -39 35 -8 -38
		mu 0 4 32 31 15 14
		f 4 -41 37 -7 -40
		mu 0 4 34 32 14 13
		f 4 -42 39 -12 -31
		mu 0 4 28 33 19 18
		f 4 -45 42 32 -44
		mu 0 4 36 35 28 29
		f 4 -47 43 34 -46
		mu 0 4 37 36 29 30
		f 4 -49 45 36 -48
		mu 0 4 38 37 30 31
		f 4 -51 47 38 -50
		mu 0 4 39 38 31 32
		f 4 -53 49 40 -52
		mu 0 4 41 39 32 34
		f 4 -54 51 41 -43
		mu 0 4 35 40 33 28
		f 4 -57 54 44 -56
		mu 0 4 43 42 35 36
		f 4 -59 55 46 -58
		mu 0 4 44 43 36 37
		f 4 -61 57 48 -60
		mu 0 4 45 44 37 38
		f 4 -63 59 50 -62
		mu 0 4 46 45 38 39
		f 4 -65 61 52 -64
		mu 0 4 48 46 39 41
		f 4 -66 63 53 -55
		mu 0 4 42 47 40 35
		f 4 -69 66 56 -68
		mu 0 4 50 49 42 43
		f 4 -71 67 58 -70
		mu 0 4 51 50 43 44
		f 4 -73 69 60 -72
		mu 0 4 52 51 44 45
		f 4 -75 71 62 -74
		mu 0 4 53 52 45 46
		f 4 -77 73 64 -76
		mu 0 4 55 53 46 48
		f 4 -78 75 65 -67
		mu 0 4 49 54 47 42;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode joint -n "n55" -p "all";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 300 1;
createNode joint -n "n56" -p "n55";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 100 0 300 1;
createNode joint -n "n57" -p "n56";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 200 0 300 1;
createNode joint -n "n58" -p "n57";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 300 0 300 1;
createNode joint -n "n59" -p "n58";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 400 0 300 1;
createNode joint -n "n60" -p "n59";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 500 0 300 1;
createNode joint -n "n61" -p "n60";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".t" -type "double3" 1 0 0 ;
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 600 0 0 1;
createNode joint -n "n62" -p "n60";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 500 0 300 1;
	setAttr ".radi" 1.2100000000000002;
createNode joint -n "n63" -p "n62";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 500 0 300 1;
	setAttr ".radi" 1.1;
createNode orientConstraint -n "n64" -p "n63";
	addAttr -ci true -k true -sn "w0" -ln "joint6W0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "joint5W1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 0 -20.436424647876432 0 ;
	setAttr ".int" 2;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "n65" -p "n63";
	addAttr -ci true -sn "joint_bind" -ln "joint_bind" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr -l on ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 500 0 300 1;
	setAttr ".sd" 3;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "0";
createNode transform -n "n66" -p "n65";
	addAttr -ci true -sn "center_pivot" -ln "center_pivot" -at "double";
	setAttr ".s" -type "double3" 0.1 1 1 ;
	setAttr -cb on ".center_pivot" 1;
createNode mesh -n "n66Shape" -p "n66";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".lev" 0;
	setAttr -s 12 ".pt[0:11]" -type "float3"  -2.2888183e-007 1.0000002 
		0 0.99999982 1.5258789e-007 0 1.5 -0.5 0 1.0000001 -1.1444092e-007 0 3.8146972e-008 
		0.99999994 0 -0.5 1.5 0 -1.0000002 2.2888183e-007 0 -1.5258789e-007 -0.99999982 0 
		0.5 -1.5 0 1.1444092e-007 -1.0000001 0 -0.99999994 -3.8146972e-008 0 -1.5 0.5 0;
	setAttr -s 12 ".vt[0:11]"  0.50000024 -0.5 -0.86602533 -0.49999985 -0.5 -0.86602557
		 -1 -0.5 -1.4901161e-007 -0.50000012 -0.5 0.86602539 0.49999997 -0.5 0.86602545 1 -0.5 0
		 0.50000024 0.5 -0.86602533 -0.49999985 0.5 -0.86602557 -1 0.5 -1.4901161e-007 -0.50000012 0.5 0.86602539
		 0.49999997 0.5 0.86602545 1 0.5 0;
	setAttr -s 18 ".ed[0:17]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 6 0 1 7 0 2 8 0 3 9 0 4 10 0 5 11 0;
	setAttr -s 8 ".fc[0:7]" -type "polyFaces" 
		f 4 0 13 -7 -13
		mu 0 4 6 7 14 13
		f 4 1 14 -8 -14
		mu 0 4 7 8 15 14
		f 4 2 15 -9 -15
		mu 0 4 8 9 16 15
		f 4 3 16 -10 -16
		mu 0 4 9 10 17 16
		f 4 4 17 -11 -17
		mu 0 4 10 11 18 17
		f 4 5 12 -12 -18
		mu 0 4 11 12 19 18
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 6 6 7 8 9 10 11
		mu 0 6 24 23 22 21 20 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode joint -n "n67" -p "n59";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 400 0 300 1;
	setAttr ".radi" 1.2100000000000002;
createNode joint -n "n68" -p "n67";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 400 0 300 1;
	setAttr ".radi" 1.1;
createNode orientConstraint -n "n69" -p "n68";
	addAttr -ci true -k true -sn "w0" -ln "joint5W0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "joint4W1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 0 -20.436424647876439 0 ;
	setAttr ".int" 2;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "n70" -p "n68";
	addAttr -ci true -sn "joint_bind" -ln "joint_bind" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr -l on ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 400 0 300 1;
	setAttr ".sd" 3;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "0";
	setAttr ".liw" yes;
createNode transform -n "n71" -p "n70";
	addAttr -ci true -sn "center_pivot" -ln "center_pivot" -at "double";
	setAttr ".s" -type "double3" 0.1 1 1 ;
	setAttr -cb on ".center_pivot" 1;
createNode mesh -n "n71Shape" -p "n71";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".lev" 0;
	setAttr -s 12 ".pt[0:11]" -type "float3"  -2.2888183e-007 1.0000002 
		0 0.99999982 1.5258789e-007 0 1.5 -0.5 0 1.0000001 -1.1444092e-007 0 3.8146972e-008 
		0.99999994 0 -0.5 1.5 0 -1.0000002 2.2888183e-007 0 -1.5258789e-007 -0.99999982 0 
		0.5 -1.5 0 1.1444092e-007 -1.0000001 0 -0.99999994 -3.8146972e-008 0 -1.5 0.5 0;
	setAttr -s 12 ".vt[0:11]"  0.50000024 -0.5 -0.86602533 -0.49999985 -0.5 -0.86602557
		 -1 -0.5 -1.4901161e-007 -0.50000012 -0.5 0.86602539 0.49999997 -0.5 0.86602545 1 -0.5 0
		 0.50000024 0.5 -0.86602533 -0.49999985 0.5 -0.86602557 -1 0.5 -1.4901161e-007 -0.50000012 0.5 0.86602539
		 0.49999997 0.5 0.86602545 1 0.5 0;
	setAttr -s 18 ".ed[0:17]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 6 0 1 7 0 2 8 0 3 9 0 4 10 0 5 11 0;
	setAttr -s 8 ".fc[0:7]" -type "polyFaces" 
		f 4 0 13 -7 -13
		mu 0 4 6 7 14 13
		f 4 1 14 -8 -14
		mu 0 4 7 8 15 14
		f 4 2 15 -9 -15
		mu 0 4 8 9 16 15
		f 4 3 16 -10 -16
		mu 0 4 9 10 17 16
		f 4 4 17 -11 -17
		mu 0 4 10 11 18 17
		f 4 5 12 -12 -18
		mu 0 4 11 12 19 18
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 6 6 7 8 9 10 11
		mu 0 6 24 23 22 21 20 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode joint -n "n72" -p "n58";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 300 0 300 1;
	setAttr ".radi" 1.2100000000000002;
createNode joint -n "n73" -p "n72";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 300 0 300 1;
	setAttr ".radi" 1.1;
createNode orientConstraint -n "n74" -p "n73";
	addAttr -ci true -k true -sn "w0" -ln "joint4W0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "joint3W1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 0 -20.436424647876432 0 ;
	setAttr ".int" 2;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "n75" -p "n73";
	addAttr -ci true -sn "joint_bind" -ln "joint_bind" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr -l on ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 300 0 300 1;
	setAttr ".sd" 3;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "0";
	setAttr ".liw" yes;
createNode transform -n "n76" -p "n75";
	addAttr -ci true -sn "center_pivot" -ln "center_pivot" -at "double";
	setAttr ".s" -type "double3" 0.1 1 1 ;
	setAttr -cb on ".center_pivot" 1;
createNode mesh -n "n76Shape" -p "n76";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".lev" 0;
	setAttr -s 12 ".pt[0:11]" -type "float3"  -2.2888183e-007 1.0000002 
		0 0.99999982 1.5258789e-007 0 1.5 -0.5 0 1.0000001 -1.1444092e-007 0 3.8146972e-008 
		0.99999994 0 -0.5 1.5 0 -1.0000002 2.2888183e-007 0 -1.5258789e-007 -0.99999982 0 
		0.5 -1.5 0 1.1444092e-007 -1.0000001 0 -0.99999994 -3.8146972e-008 0 -1.5 0.5 0;
	setAttr -s 12 ".vt[0:11]"  0.50000024 -0.5 -0.86602533 -0.49999985 -0.5 -0.86602557
		 -1 -0.5 -1.4901161e-007 -0.50000012 -0.5 0.86602539 0.49999997 -0.5 0.86602545 1 -0.5 0
		 0.50000024 0.5 -0.86602533 -0.49999985 0.5 -0.86602557 -1 0.5 -1.4901161e-007 -0.50000012 0.5 0.86602539
		 0.49999997 0.5 0.86602545 1 0.5 0;
	setAttr -s 18 ".ed[0:17]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 6 0 1 7 0 2 8 0 3 9 0 4 10 0 5 11 0;
	setAttr -s 8 ".fc[0:7]" -type "polyFaces" 
		f 4 0 13 -7 -13
		mu 0 4 6 7 14 13
		f 4 1 14 -8 -14
		mu 0 4 7 8 15 14
		f 4 2 15 -9 -15
		mu 0 4 8 9 16 15
		f 4 3 16 -10 -16
		mu 0 4 9 10 17 16
		f 4 4 17 -11 -17
		mu 0 4 10 11 18 17
		f 4 5 12 -12 -18
		mu 0 4 11 12 19 18
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 6 6 7 8 9 10 11
		mu 0 6 24 23 22 21 20 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode joint -n "n77" -p "n57";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 200 0 300 1;
	setAttr ".radi" 1.2100000000000002;
createNode joint -n "n78" -p "n77";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 200 0 300 1;
	setAttr ".radi" 1.1;
createNode orientConstraint -n "n79" -p "n78";
	addAttr -ci true -k true -sn "w0" -ln "joint3W0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "joint2W1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 0 -20.436424647876429 0 ;
	setAttr ".int" 2;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "n80" -p "n78";
	addAttr -ci true -sn "joint_bind" -ln "joint_bind" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr -l on ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 200 0 300 1;
	setAttr ".sd" 3;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "0";
createNode transform -n "n81" -p "n80";
	addAttr -ci true -sn "center_pivot" -ln "center_pivot" -at "double";
	setAttr ".s" -type "double3" 0.1 1 1 ;
	setAttr -cb on ".center_pivot" 1;
createNode mesh -n "n81Shape" -p "n81";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".lev" 0;
	setAttr -s 12 ".pt[0:11]" -type "float3"  -2.2888183e-007 1.0000002 
		0 0.99999982 1.5258789e-007 0 1.5 -0.5 0 1.0000001 -1.1444092e-007 0 3.8146972e-008 
		0.99999994 0 -0.5 1.5 0 -1.0000002 2.2888183e-007 0 -1.5258789e-007 -0.99999982 0 
		0.5 -1.5 0 1.1444092e-007 -1.0000001 0 -0.99999994 -3.8146972e-008 0 -1.5 0.5 0;
	setAttr -s 12 ".vt[0:11]"  0.50000024 -0.5 -0.86602533 -0.49999985 -0.5 -0.86602557
		 -1 -0.5 -1.4901161e-007 -0.50000012 -0.5 0.86602539 0.49999997 -0.5 0.86602545 1 -0.5 0
		 0.50000024 0.5 -0.86602533 -0.49999985 0.5 -0.86602557 -1 0.5 -1.4901161e-007 -0.50000012 0.5 0.86602539
		 0.49999997 0.5 0.86602545 1 0.5 0;
	setAttr -s 18 ".ed[0:17]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 6 0 1 7 0 2 8 0 3 9 0 4 10 0 5 11 0;
	setAttr -s 8 ".fc[0:7]" -type "polyFaces" 
		f 4 0 13 -7 -13
		mu 0 4 6 7 14 13
		f 4 1 14 -8 -14
		mu 0 4 7 8 15 14
		f 4 2 15 -9 -15
		mu 0 4 8 9 16 15
		f 4 3 16 -10 -16
		mu 0 4 9 10 17 16
		f 4 4 17 -11 -17
		mu 0 4 10 11 18 17
		f 4 5 12 -12 -18
		mu 0 4 11 12 19 18
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 6 6 7 8 9 10 11
		mu 0 6 24 23 22 21 20 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode joint -n "n82" -p "n56";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 100 0 300 1;
	setAttr ".radi" 1.2100000000000002;
createNode joint -n "n83" -p "n82";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 100 0 300 1;
	setAttr ".radi" 1.1;
createNode orientConstraint -n "n84" -p "n83";
	addAttr -ci true -k true -sn "w0" -ln "joint2W0" -dv 1 -min 0 -at "double";
	addAttr -ci true -k true -sn "w1" -ln "joint11W1" -dv 1 -min 0 -at "double";
	setAttr -k on ".nds";
	setAttr -k off ".v";
	setAttr -k off ".tx";
	setAttr -k off ".ty";
	setAttr -k off ".tz";
	setAttr -k off ".rx";
	setAttr -k off ".ry";
	setAttr -k off ".rz";
	setAttr -k off ".sx";
	setAttr -k off ".sy";
	setAttr -k off ".sz";
	setAttr ".erp" yes;
	setAttr -s 2 ".tg";
	setAttr ".lr" -type "double3" 0 -20.436424647876429 0 ;
	setAttr ".int" 2;
	setAttr -k on ".w0";
	setAttr -k on ".w1";
createNode joint -n "n85" -p "n83";
	addAttr -ci true -sn "joint_bind" -ln "joint_bind" -at "double";
	addAttr -ci true -sn "liw" -ln "lockInfluenceWeights" -min 0 -max 1 -at "bool";
	setAttr ".mnrl" -type "double3" -360 -360 -360 ;
	setAttr ".mxrl" -type "double3" 360 360 360 ;
	setAttr -l on ".ssc" no;
	setAttr ".bps" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 100 0 300 1;
	setAttr ".sd" 3;
	setAttr ".typ" 18;
	setAttr ".otp" -type "string" "0";
createNode transform -n "n86" -p "n85";
	addAttr -ci true -sn "center_pivot" -ln "center_pivot" -at "double";
	setAttr ".s" -type "double3" 0.1 1 1 ;
	setAttr -cb on ".center_pivot" 1;
createNode mesh -n "n86Shape" -p "n86";
	setAttr -k off ".v";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 26 ".uvst[0].uvsp[0:25]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".lev" 0;
	setAttr -s 12 ".pt[0:11]" -type "float3"  -2.2888183e-007 1.0000002 
		0 0.99999982 1.5258789e-007 0 1.5 -0.5 0 1.0000001 -1.1444092e-007 0 3.8146972e-008 
		0.99999994 0 -0.5 1.5 0 -1.0000002 2.2888183e-007 0 -1.5258789e-007 -0.99999982 0 
		0.5 -1.5 0 1.1444092e-007 -1.0000001 0 -0.99999994 -3.8146972e-008 0 -1.5 0.5 0;
	setAttr -s 12 ".vt[0:11]"  0.50000024 -0.5 -0.86602533 -0.49999985 -0.5 -0.86602557
		 -1 -0.5 -1.4901161e-007 -0.50000012 -0.5 0.86602539 0.49999997 -0.5 0.86602545 1 -0.5 0
		 0.50000024 0.5 -0.86602533 -0.49999985 0.5 -0.86602557 -1 0.5 -1.4901161e-007 -0.50000012 0.5 0.86602539
		 0.49999997 0.5 0.86602545 1 0.5 0;
	setAttr -s 18 ".ed[0:17]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 6 0 1 7 0 2 8 0 3 9 0 4 10 0 5 11 0;
	setAttr -s 8 ".fc[0:7]" -type "polyFaces" 
		f 4 0 13 -7 -13
		mu 0 4 6 7 14 13
		f 4 1 14 -8 -14
		mu 0 4 7 8 15 14
		f 4 2 15 -9 -15
		mu 0 4 8 9 16 15
		f 4 3 16 -10 -16
		mu 0 4 9 10 17 16
		f 4 4 17 -11 -17
		mu 0 4 10 11 18 17
		f 4 5 12 -12 -18
		mu 0 4 11 12 19 18
		f 6 -6 -5 -4 -3 -2 -1
		mu 0 6 0 5 4 3 2 1
		f 6 6 7 8 9 10 11
		mu 0 6 24 23 22 21 20 25;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode transform -n "n87" -p "all";
	setAttr ".t" -type "double3" 3 0 3 ;
	setAttr -l on ".tx";
	setAttr -l on ".ty";
	setAttr -l on ".tz";
	setAttr ".r" -type "double3" 0 0 90 ;
	setAttr -l on ".rx";
	setAttr -l on ".ry";
	setAttr -l on ".rz";
	setAttr ".s" -type "double3" 0.5 1 0.5 ;
	setAttr -l on ".sx";
	setAttr -l on ".sy";
	setAttr -l on ".sz";
createNode mesh -n "nShape87" -p "n87";
	setAttr -k off ".v";
	setAttr -s 4 ".iog[0].og";
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr ".lev" 1;
	setAttr ".dr" 1;
createNode mesh -n "nShape1Orig87" -p "n87";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 56 ".uvst[0].uvsp[0:55]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998 0.58333331
		 0.61325186 0.54166663 0.61325186 0.49999997 0.61325186 0.45833331 0.61325186 0.41666669
		 0.61325186 0.625 0.61325186 0.375 0.61325186 0.58333331 0.53806388 0.54166663 0.53806388
		 0.49999997 0.53806388 0.45833331 0.53806388 0.41666666 0.53806388 0.625 0.53806388
		 0.375 0.53806388 0.58333331 0.46287593 0.54166663 0.46287593 0.49999994 0.46287593
		 0.45833331 0.46287593 0.41666666 0.46287593 0.625 0.46287593 0.375 0.46287593 0.58333331
		 0.38768798 0.54166663 0.38768798 0.49999994 0.38768798 0.45833331 0.38768798 0.41666666
		 0.38768798 0.625 0.38768798 0.375 0.38768798;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 38 ".vt[0:37]"  0.50000024 -2.5 -0.86602533 -0.49999985 -2.5 -0.86602557
		 -1 -2.5 -1.4901161e-007 -0.50000012 -2.5 0.86602539 0.49999997 -2.5 0.86602545 1 -2.5 0
		 0.50000024 2.5 -0.86602533 -0.49999985 2.5 -0.86602557 -1 2.5 -1.4901161e-007 -0.50000012 2.5 0.86602539
		 0.49999997 2.5 0.86602545 1 2.5 0 0 -2.5 0 0 2.5 0 1 1.5 0 0.49999997 1.5 0.86602545
		 -0.50000018 1.5 0.86602539 -1 1.5 -1.4901161e-007 -0.49999985 1.5 -0.86602557 0.50000024 1.5 -0.86602533
		 1 0.5 0 0.49999997 0.5 0.86602545 -0.50000018 0.5 0.86602539 -1 0.5 -1.4901161e-007
		 -0.49999985 0.5 -0.86602557 0.50000024 0.5 -0.86602533 1 -0.50000012 0 0.49999997 -0.50000012 0.86602545
		 -0.50000012 -0.50000012 0.86602539 -1 -0.50000012 -1.4901161e-007 -0.49999985 -0.50000012 -0.86602557
		 0.50000024 -0.50000012 -0.86602533 1 -1.5 0 0.49999997 -1.5 0.86602545 -0.50000012 -1.5 0.86602539
		 -1 -1.5 -1.4901161e-007 -0.49999985 -1.5 -0.86602557 0.50000024 -1.5 -0.86602533;
	setAttr -s 78 ".ed[0:77]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 37 0 1 36 0 2 35 0 3 34 0 4 33 0 5 32 0 12 0 1
		 12 1 1 12 2 1 12 3 1 12 4 1 12 5 1 6 13 1 7 13 1 8 13 1 9 13 1 10 13 1 11 13 1 14 11 0
		 15 10 0 14 15 1 16 9 0 15 16 1 17 8 0 16 17 1 18 7 0 17 18 1 19 6 0 18 19 1 19 14 1
		 20 14 0 21 15 0 20 21 1 22 16 0 21 22 1 23 17 0 22 23 1 24 18 0 23 24 1 25 19 0 24 25 1
		 25 20 1 26 20 0 27 21 0 26 27 1 28 22 0 27 28 1 29 23 0 28 29 1 30 24 0 29 30 1 31 25 0
		 30 31 1 31 26 1 32 26 0 33 27 0 32 33 1 34 28 0 33 34 1 35 29 0 34 35 1 36 30 0 35 36 1
		 37 31 0 36 37 1 37 32 1;
	setAttr -s 42 ".fc[0:41]" -type "polyFaces" 
		f 4 0 13 76 -13
		mu 0 4 6 7 53 55
		f 4 1 14 74 -14
		mu 0 4 7 8 52 53
		f 4 2 15 72 -15
		mu 0 4 8 9 51 52
		f 4 3 16 70 -16
		mu 0 4 9 10 50 51
		f 4 4 17 68 -17
		mu 0 4 10 11 49 50
		f 4 5 12 77 -18
		mu 0 4 11 12 54 49
		f 3 -1 -19 19
		mu 0 3 1 0 26
		f 3 -2 -20 20
		mu 0 3 2 1 26
		f 3 -3 -21 21
		mu 0 3 3 2 26
		f 3 -4 -22 22
		mu 0 3 4 3 26
		f 3 -5 -23 23
		mu 0 3 5 4 26
		f 3 -6 -24 18
		mu 0 3 0 5 26
		f 3 6 25 -25
		mu 0 3 24 23 27
		f 3 7 26 -26
		mu 0 3 23 22 27
		f 3 8 27 -27
		mu 0 3 22 21 27
		f 3 9 28 -28
		mu 0 3 21 20 27
		f 3 10 29 -29
		mu 0 3 20 25 27
		f 3 11 24 -30
		mu 0 3 25 24 27
		f 4 -33 30 -11 -32
		mu 0 4 29 28 18 17
		f 4 -35 31 -10 -34
		mu 0 4 30 29 17 16
		f 4 -37 33 -9 -36
		mu 0 4 31 30 16 15
		f 4 -39 35 -8 -38
		mu 0 4 32 31 15 14
		f 4 -41 37 -7 -40
		mu 0 4 34 32 14 13
		f 4 -42 39 -12 -31
		mu 0 4 28 33 19 18
		f 4 -45 42 32 -44
		mu 0 4 36 35 28 29
		f 4 -47 43 34 -46
		mu 0 4 37 36 29 30
		f 4 -49 45 36 -48
		mu 0 4 38 37 30 31
		f 4 -51 47 38 -50
		mu 0 4 39 38 31 32
		f 4 -53 49 40 -52
		mu 0 4 41 39 32 34
		f 4 -54 51 41 -43
		mu 0 4 35 40 33 28
		f 4 -57 54 44 -56
		mu 0 4 43 42 35 36
		f 4 -59 55 46 -58
		mu 0 4 44 43 36 37
		f 4 -61 57 48 -60
		mu 0 4 45 44 37 38
		f 4 -63 59 50 -62
		mu 0 4 46 45 38 39
		f 4 -65 61 52 -64
		mu 0 4 48 46 39 41
		f 4 -66 63 53 -55
		mu 0 4 42 47 40 35
		f 4 -69 66 56 -68
		mu 0 4 50 49 42 43
		f 4 -71 67 58 -70
		mu 0 4 51 50 43 44
		f 4 -73 69 60 -72
		mu 0 4 52 51 44 45
		f 4 -75 71 62 -74
		mu 0 4 53 52 45 46
		f 4 -77 73 64 -76
		mu 0 4 55 53 46 48
		f 4 -78 75 65 -67
		mu 0 4 49 54 47 42;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode mesh -n "nShape4Orig87" -p "n87";
	setAttr -k off ".v";
	setAttr ".io" yes;
	setAttr ".vir" yes;
	setAttr ".vif" yes;
	setAttr ".uvst[0].uvsn" -type "string" "map1";
	setAttr -s 91 ".uvst[0].uvsp[0:90]" -type "float2" 0.57812506 0.020933539
		 0.42187503 0.020933509 0.34375 0.15624997 0.421875 0.29156646 0.578125 0.29156649
		 0.65625 0.15625 0.375 0.3125 0.41666666 0.3125 0.45833331 0.3125 0.49999997 0.3125
		 0.54166663 0.3125 0.58333331 0.3125 0.625 0.3125 0.375 0.68843985 0.41666666 0.68843985
		 0.45833331 0.68843985 0.49999997 0.68843985 0.54166663 0.68843985 0.58333331 0.68843985
		 0.625 0.68843985 0.57812506 0.70843351 0.42187503 0.70843351 0.34375 0.84375 0.421875
		 0.97906649 0.578125 0.97906649 0.65625 0.84375 0.5 0.15000001 0.5 0.83749998 0.58333331
		 0.61325186 0.54166663 0.61325186 0.49999997 0.61325186 0.45833331 0.61325186 0.41666669
		 0.61325186 0.625 0.61325186 0.375 0.61325186 0.58333331 0.53806388 0.54166663 0.53806388
		 0.49999997 0.53806388 0.45833331 0.53806388 0.41666666 0.53806388 0.625 0.53806388
		 0.375 0.53806388 0.58333331 0.46287593 0.54166663 0.46287593 0.49999994 0.46287593
		 0.45833331 0.46287593 0.41666666 0.46287593 0.625 0.46287593 0.375 0.46287593 0.58333331
		 0.38768798 0.54166663 0.38768798 0.49999994 0.38768798 0.45833331 0.38768798 0.41666666
		 0.38768798 0.625 0.38768798 0.375 0.38768798 0.625 0.65084589 0.375 0.65084589 0.58333331
		 0.65084589 0.54166663 0.65084589 0.49999997 0.65084589 0.45833331 0.65084589 0.41666669
		 0.65084589 0.625 0.57565784 0.375 0.57565784 0.58333331 0.57565784 0.54166663 0.57565784
		 0.49999997 0.57565784 0.45833331 0.57565784 0.41666669 0.57565784 0.625 0.50046992
		 0.375 0.50046992 0.58333331 0.50046992 0.54166663 0.50046992 0.49999994 0.50046992
		 0.45833331 0.50046992 0.41666666 0.50046992 0.625 0.42528194 0.375 0.42528194 0.58333331
		 0.42528194 0.54166663 0.42528194 0.49999994 0.42528194 0.45833331 0.42528194 0.41666666
		 0.42528194 0.625 0.35009399 0.375 0.35009399 0.58333331 0.35009399 0.54166663 0.35009399
		 0.49999994 0.35009399 0.45833331 0.35009399 0.41666666 0.35009399;
	setAttr ".cuvs" -type "string" "map1";
	setAttr ".dcc" -type "string" "Ambient+Diffuse";
	setAttr ".covm[0]"  0 1 1;
	setAttr ".cdvm[0]"  0 1 1;
	setAttr -s 68 ".vt[0:67]"  0.50000024 -2.5 -0.86602533 -0.49999985 -2.5 -0.86602557
		 -1 -2.5 -1.4901161e-007 -0.50000012 -2.5 0.86602539 0.49999997 -2.5 0.86602545 1 -2.5 0
		 0.50000024 2.5 -0.86602533 -0.49999985 2.5 -0.86602557 -1 2.5 -1.4901161e-007 -0.50000012 2.5 0.86602539
		 0.49999997 2.5 0.86602545 1 2.5 0 0 -2.5 0 0 2.5 0 1 1.5 0 0.49999997 1.5 0.86602545
		 -0.50000018 1.5 0.86602539 -1 1.5 -1.4901161e-007 -0.49999985 1.5 -0.86602557 0.50000024 1.5 -0.86602533
		 1 0.5 0 0.49999997 0.5 0.86602545 -0.50000018 0.5 0.86602539 -1 0.5 -1.4901161e-007
		 -0.49999985 0.5 -0.86602557 0.50000024 0.5 -0.86602533 1 -0.50000012 0 0.49999997 -0.50000012 0.86602545
		 -0.50000012 -0.50000012 0.86602539 -1 -0.50000012 -1.4901161e-007 -0.49999985 -0.50000012 -0.86602557
		 0.50000024 -0.50000012 -0.86602533 1 -1.5 0 0.49999997 -1.5 0.86602545 -0.50000012 -1.5 0.86602539
		 -1 -1.5 -1.4901161e-007 -0.49999985 -1.5 -0.86602557 0.50000024 -1.5 -0.86602533
		 0.50000024 2 -0.86602533 1 2 0 0.49999997 2 0.86602545 -0.50000018 2 0.86602539 -1 2 -1.4901161e-007
		 -0.49999985 2 -0.86602557 0.50000024 1 -0.86602533 1 1 0 0.49999997 1 0.86602545
		 -0.50000018 1 0.86602539 -1 1 -1.4901161e-007 -0.49999985 1 -0.86602557 0.50000024 -5.7220458e-008 -0.86602533
		 1 -5.7220458e-008 0 0.49999997 -5.7220458e-008 0.86602545 -0.50000018 -5.7220458e-008 0.86602539
		 -1 -5.7220458e-008 -1.4901161e-007 -0.49999985 -5.7220458e-008 -0.86602557 0.50000024 -1.000000119209 -0.86602533
		 1 -1.000000119209 0 0.49999997 -1.000000119209 0.86602545 -0.50000012 -1.000000119209 0.86602539
		 -1 -1.000000119209 -1.4901161e-007 -0.49999985 -1.000000119209 -0.86602557 0.50000024 -2 -0.86602533
		 1 -2 0 0.49999997 -2 0.86602545 -0.50000012 -2 0.86602539 -1 -2 -1.4901161e-007 -0.49999985 -2 -0.86602557;
	setAttr -s 138 ".ed[0:137]"  0 1 0 1 2 0 2 3 0 3 4 0 4 5 0 5 0 0 6 7 0
		 7 8 0 8 9 0 9 10 0 10 11 0 11 6 0 0 62 0 1 67 0 2 66 0 3 65 0 4 64 0 5 63 0 12 0 1
		 12 1 1 12 2 1 12 3 1 12 4 1 12 5 1 6 13 1 7 13 1 8 13 1 9 13 1 10 13 1 11 13 1 14 39 0
		 15 40 0 14 15 1 16 41 0 15 16 1 17 42 0 16 17 1 18 43 0 17 18 1 19 38 0 18 19 1 19 14 1
		 20 45 0 21 46 0 20 21 1 22 47 0 21 22 1 23 48 0 22 23 1 24 49 0 23 24 1 25 44 0 24 25 1
		 25 20 1 26 51 0 27 52 0 26 27 1 28 53 0 27 28 1 29 54 0 28 29 1 30 55 0 29 30 1 31 50 0
		 30 31 1 31 26 1 32 57 0 33 58 0 32 33 1 34 59 0 33 34 1 35 60 0 34 35 1 36 61 0 35 36 1
		 37 56 0 36 37 1 37 32 1 38 6 0 39 11 0 38 39 1 40 10 0 39 40 1 41 9 0 40 41 1 42 8 0
		 41 42 1 43 7 0 42 43 1 43 38 1 44 19 0 45 14 0 44 45 1 46 15 0 45 46 1 47 16 0 46 47 1
		 48 17 0 47 48 1 49 18 0 48 49 1 49 44 1 50 25 0 51 20 0 50 51 1 52 21 0 51 52 1 53 22 0
		 52 53 1 54 23 0 53 54 1 55 24 0 54 55 1 55 50 1 56 31 0 57 26 0 56 57 1 58 27 0 57 58 1
		 59 28 0 58 59 1 60 29 0 59 60 1 61 30 0 60 61 1 61 56 1 62 37 0 63 32 0 62 63 1 64 33 0
		 63 64 1 65 34 0 64 65 1 66 35 0 65 66 1 67 36 0 66 67 1 67 62 1;
	setAttr -s 72 ".fc[0:71]" -type "polyFaces" 
		f 4 0 13 137 -13
		mu 0 4 6 7 90 85
		f 4 1 14 136 -14
		mu 0 4 7 8 89 90
		f 4 2 15 134 -15
		mu 0 4 8 9 88 89
		f 4 3 16 132 -16
		mu 0 4 9 10 87 88
		f 4 4 17 130 -17
		mu 0 4 10 11 86 87
		f 4 5 12 128 -18
		mu 0 4 11 12 84 86
		f 3 -1 -19 19
		mu 0 3 1 0 26
		f 3 -2 -20 20
		mu 0 3 2 1 26
		f 3 -3 -21 21
		mu 0 3 3 2 26
		f 3 -4 -22 22
		mu 0 3 4 3 26
		f 3 -5 -23 23
		mu 0 3 5 4 26
		f 3 -6 -24 18
		mu 0 3 0 5 26
		f 3 6 25 -25
		mu 0 3 24 23 27
		f 3 7 26 -26
		mu 0 3 23 22 27
		f 3 8 27 -27
		mu 0 3 22 21 27
		f 3 9 28 -28
		mu 0 3 21 20 27
		f 3 10 29 -29
		mu 0 3 20 25 27
		f 3 11 24 -30
		mu 0 3 25 24 27
		f 4 -33 30 82 -32
		mu 0 4 29 28 58 59
		f 4 -35 31 84 -34
		mu 0 4 30 29 59 60
		f 4 -37 33 86 -36
		mu 0 4 31 30 60 61
		f 4 -39 35 88 -38
		mu 0 4 32 31 61 62
		f 4 -41 37 89 -40
		mu 0 4 34 32 62 57
		f 4 -42 39 80 -31
		mu 0 4 28 33 56 58
		f 4 -45 42 94 -44
		mu 0 4 36 35 65 66
		f 4 -47 43 96 -46
		mu 0 4 37 36 66 67
		f 4 -49 45 98 -48
		mu 0 4 38 37 67 68
		f 4 -51 47 100 -50
		mu 0 4 39 38 68 69
		f 4 -53 49 101 -52
		mu 0 4 41 39 69 64
		f 4 -54 51 92 -43
		mu 0 4 35 40 63 65
		f 4 -57 54 106 -56
		mu 0 4 43 42 72 73
		f 4 -59 55 108 -58
		mu 0 4 44 43 73 74
		f 4 -61 57 110 -60
		mu 0 4 45 44 74 75
		f 4 -63 59 112 -62
		mu 0 4 46 45 75 76
		f 4 -65 61 113 -64
		mu 0 4 48 46 76 71
		f 4 -66 63 104 -55
		mu 0 4 42 47 70 72
		f 4 -69 66 118 -68
		mu 0 4 50 49 79 80
		f 4 -71 67 120 -70
		mu 0 4 51 50 80 81
		f 4 -73 69 122 -72
		mu 0 4 52 51 81 82
		f 4 -75 71 124 -74
		mu 0 4 53 52 82 83
		f 4 -77 73 125 -76
		mu 0 4 55 53 83 78
		f 4 -78 75 116 -67
		mu 0 4 49 54 77 79
		f 4 -81 78 -12 -80
		mu 0 4 58 56 19 18
		f 4 -83 79 -11 -82
		mu 0 4 59 58 18 17
		f 4 -85 81 -10 -84
		mu 0 4 60 59 17 16
		f 4 -87 83 -9 -86
		mu 0 4 61 60 16 15
		f 4 -89 85 -8 -88
		mu 0 4 62 61 15 14
		f 4 -90 87 -7 -79
		mu 0 4 57 62 14 13
		f 4 -93 90 41 -92
		mu 0 4 65 63 33 28
		f 4 -95 91 32 -94
		mu 0 4 66 65 28 29
		f 4 -97 93 34 -96
		mu 0 4 67 66 29 30
		f 4 -99 95 36 -98
		mu 0 4 68 67 30 31
		f 4 -101 97 38 -100
		mu 0 4 69 68 31 32
		f 4 -102 99 40 -91
		mu 0 4 64 69 32 34
		f 4 -105 102 53 -104
		mu 0 4 72 70 40 35
		f 4 -107 103 44 -106
		mu 0 4 73 72 35 36
		f 4 -109 105 46 -108
		mu 0 4 74 73 36 37
		f 4 -111 107 48 -110
		mu 0 4 75 74 37 38
		f 4 -113 109 50 -112
		mu 0 4 76 75 38 39
		f 4 -114 111 52 -103
		mu 0 4 71 76 39 41
		f 4 -117 114 65 -116
		mu 0 4 79 77 47 42
		f 4 -119 115 56 -118
		mu 0 4 80 79 42 43
		f 4 -121 117 58 -120
		mu 0 4 81 80 43 44
		f 4 -123 119 60 -122
		mu 0 4 82 81 44 45
		f 4 -125 121 62 -124
		mu 0 4 83 82 45 46
		f 4 -126 123 64 -115
		mu 0 4 78 83 46 48
		f 4 -129 126 77 -128
		mu 0 4 86 84 54 49
		f 4 -131 127 68 -130
		mu 0 4 87 86 49 50
		f 4 -133 129 70 -132
		mu 0 4 88 87 50 51
		f 4 -135 131 72 -134
		mu 0 4 89 88 51 52
		f 4 -137 133 74 -136
		mu 0 4 90 89 52 53
		f 4 -138 135 76 -127
		mu 0 4 85 90 53 55;
	setAttr ".cd" -type "dataPolyComponent" Index_Data Edge 0 ;
	setAttr ".cvd" -type "dataPolyComponent" Index_Data Vertex 0 ;
	setAttr ".hfd" -type "dataPolyComponent" Index_Data Face 0 ;
createNode lightLinker -s -n "lightLinker1";
	setAttr -s 3 ".lnk";
	setAttr -s 3 ".slnk";
createNode displayLayerManager -n "layerManager";
	setAttr ".cdl" 1;
	setAttr -s 2 ".dli[1]"  1;
	setAttr -s 2 ".dli";
createNode displayLayer -n "defaultLayer";
createNode renderLayerManager -n "renderLayerManager";
createNode renderLayer -n "defaultRenderLayer";
	setAttr ".g" yes;
createNode script -n "sceneConfigurationScriptNode";
	setAttr ".b" -type "string" "playbackOptions -min 1 -max 24 -ast 1 -aet 48 ";
	setAttr ".st" 6;
createNode skinCluster -n "skinCluster1";
	setAttr -s 38 ".wl";
	setAttr ".wl[0].w[5]"  1;
	setAttr ".wl[1].w[5]"  1;
	setAttr ".wl[2].w[5]"  1;
	setAttr ".wl[3].w[5]"  1;
	setAttr ".wl[4].w[5]"  1;
	setAttr ".wl[5].w[5]"  1;
	setAttr ".wl[6].w[0]"  1;
	setAttr ".wl[7].w[0]"  1;
	setAttr ".wl[8].w[0]"  1;
	setAttr ".wl[9].w[0]"  1;
	setAttr ".wl[10].w[0]"  1;
	setAttr ".wl[11].w[0]"  1;
	setAttr ".wl[12].w[5]"  1;
	setAttr ".wl[13].w[0]"  1;
	setAttr ".wl[14].w[1]"  1;
	setAttr ".wl[15].w[1]"  1;
	setAttr ".wl[16].w[1]"  1;
	setAttr ".wl[17].w[1]"  1;
	setAttr ".wl[18].w[1]"  1;
	setAttr ".wl[19].w[1]"  1;
	setAttr ".wl[20].w[2]"  1;
	setAttr ".wl[21].w[2]"  1;
	setAttr ".wl[22].w[2]"  1;
	setAttr ".wl[23].w[2]"  1;
	setAttr ".wl[24].w[2]"  1;
	setAttr ".wl[25].w[2]"  1;
	setAttr ".wl[26].w[3]"  1;
	setAttr ".wl[27].w[3]"  1;
	setAttr ".wl[28].w[3]"  1;
	setAttr ".wl[29].w[3]"  1;
	setAttr ".wl[30].w[3]"  1;
	setAttr ".wl[31].w[3]"  1;
	setAttr ".wl[32].w[4]"  1;
	setAttr ".wl[33].w[4]"  1;
	setAttr ".wl[34].w[4]"  1;
	setAttr ".wl[35].w[4]"  1;
	setAttr ".wl[36].w[4]"  1;
	setAttr ".wl[37].w[4]"  1;
	setAttr -s 7 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 200 1;
	setAttr ".pm[1]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -100 0 200 1;
	setAttr ".pm[2]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -200 0 200 1;
	setAttr ".pm[3]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -300 0 200 1;
	setAttr ".pm[4]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -400 0 200 1;
	setAttr ".pm[5]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -500 0 200 1;
	setAttr ".pm[6]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -600 0 200 1;
	setAttr ".gm" -type "matrix" 1.1102230246251565e-016 0.5 0 0 -1 2.2204460492503131e-016 0 0
		 0 0 0.5 0 342.46179861501491 1.277604719462589e-014 -200 1;
	setAttr -s 6 ".ma";
	setAttr -s 7 ".dpf[0:6]"  6 6 6 6 6 6 6;
	setAttr -s 6 ".lw";
	setAttr -s 6 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 1;
	setAttr ".bm" 0;
	setAttr ".ptw" -type "doubleArray" 38 1 1 1 1 1 1 0 0 0 0 0 0 1 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ;
	setAttr ".ucm" yes;
createNode tweak -n "tweak1";
createNode objectSet -n "skinCluster1Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster1GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster1GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet1";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId2";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts2";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode dagPose -n "bindPose3";
	setAttr -s 6 ".wm";
	setAttr -s 7 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 -200 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr -s 6 ".m";
	setAttr -s 6 ".p";
	setAttr ".bp" yes;
createNode skinCluster -n "skinCluster2";
	setAttr -s 38 ".wl";
	setAttr ".wl[0].w[5]"  1;
	setAttr ".wl[1].w[5]"  1;
	setAttr ".wl[2].w[5]"  1;
	setAttr ".wl[3].w[5]"  1;
	setAttr ".wl[4].w[5]"  1;
	setAttr ".wl[5].w[5]"  1;
	setAttr ".wl[6].w[0]"  1;
	setAttr ".wl[7].w[0]"  1;
	setAttr ".wl[8].w[0]"  1;
	setAttr ".wl[9].w[0]"  1;
	setAttr ".wl[10].w[0]"  1;
	setAttr ".wl[11].w[0]"  1;
	setAttr ".wl[12].w[5]"  1;
	setAttr ".wl[13].w[0]"  1;
	setAttr ".wl[14].w[1]"  1;
	setAttr ".wl[15].w[1]"  1;
	setAttr ".wl[16].w[1]"  1;
	setAttr ".wl[17].w[1]"  1;
	setAttr ".wl[18].w[1]"  1;
	setAttr ".wl[19].w[1]"  1;
	setAttr ".wl[20].w[2]"  1;
	setAttr ".wl[21].w[2]"  1;
	setAttr ".wl[22].w[2]"  1;
	setAttr ".wl[23].w[2]"  1;
	setAttr ".wl[24].w[2]"  1;
	setAttr ".wl[25].w[2]"  1;
	setAttr ".wl[26].w[3]"  1;
	setAttr ".wl[27].w[3]"  1;
	setAttr ".wl[28].w[3]"  1;
	setAttr ".wl[29].w[3]"  1;
	setAttr ".wl[30].w[3]"  1;
	setAttr ".wl[31].w[3]"  1;
	setAttr ".wl[32].w[4]"  1;
	setAttr ".wl[33].w[4]"  1;
	setAttr ".wl[34].w[4]"  1;
	setAttr ".wl[35].w[4]"  1;
	setAttr ".wl[36].w[4]"  1;
	setAttr ".wl[37].w[4]"  1;
	setAttr -s 7 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1;
	setAttr ".pm[1]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -100 0 0 1;
	setAttr ".pm[2]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -200 0 0 1;
	setAttr ".pm[3]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -300 0 0 1;
	setAttr ".pm[4]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -400 0 0 1;
	setAttr ".pm[5]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -500 0 0 1;
	setAttr ".pm[6]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -600 0 0 1;
	setAttr ".gm" -type "matrix" 1.1102230246251565e-016 0.5 0 0 -1 2.2204460492503131e-016 0 0
		 0 0 0.5 0 300 0 0 1;
	setAttr -s 6 ".ma";
	setAttr -s 7 ".dpf[0:6]"  6 6 6 6 6 6 6;
	setAttr -s 6 ".lw";
	setAttr -s 6 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 1;
	setAttr ".bm" 0;
	setAttr ".ptw" -type "doubleArray" 38 1 1 1 1 1 1 0 0 0 0 0 0 1 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 ;
	setAttr ".ucm" yes;
createNode tweak -n "tweak2";
createNode objectSet -n "skinCluster2Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster2GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster2GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet2";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId4";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts4";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode dagPose -n "bindPose4";
	setAttr -s 6 ".wm";
	setAttr -s 7 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr -s 6 ".m";
	setAttr -s 6 ".p";
	setAttr ".bp" yes;
createNode animCurveTU -n "joint8_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "joint8_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint8_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.96;
createNode animCurveTA -n "joint8_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint8_scaleX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint8_scaleY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint8_scaleZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint2_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint2_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint2_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint2_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint2_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint2_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.959572214283028;
createNode animCurveTA -n "joint2_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint2_scaleX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint2_scaleY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint2_scaleZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint3_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint3_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint3_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint3_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint3_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint3_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.959572214283028;
createNode animCurveTA -n "joint3_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint3_scaleX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint3_scaleY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint3_scaleZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint4_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint4_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint4_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint4_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint4_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint4_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.959572214283028;
createNode animCurveTA -n "joint4_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint4_scaleX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint4_scaleY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint4_scaleZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint5_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint5_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint5_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint5_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint5_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint5_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.959572214283028;
createNode animCurveTA -n "joint5_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint5_scaleX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint5_scaleY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint5_scaleZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint6_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint6_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint6_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint6_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint6_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint6_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.959572214283028;
createNode animCurveTA -n "joint6_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint6_scaleX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint6_scaleY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint6_scaleZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint1_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTA -n "joint1_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint1_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.96;
createNode animCurveTA -n "joint1_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint1_scaleX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint1_scaleY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint1_scaleZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint2_visibility1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint2_translateX1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint2_translateY1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint2_translateZ1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint2_rotateX1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint2_rotateY1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.959572214283028;
createNode animCurveTA -n "joint2_rotateZ1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint2_scaleX1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint2_scaleY1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint2_scaleZ1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint3_visibility1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint3_translateX1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint3_translateY1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint3_translateZ1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint3_rotateX1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint3_rotateY1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.959572214283028;
createNode animCurveTA -n "joint3_rotateZ1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint3_scaleX1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint3_scaleY1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint3_scaleZ1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint4_visibility1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint4_translateX1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint4_translateY1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint4_translateZ1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint4_rotateX1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint4_rotateY1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.959572214283028;
createNode animCurveTA -n "joint4_rotateZ1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint4_scaleX1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint4_scaleY1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint4_scaleZ1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint5_visibility1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint5_translateX1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint5_translateY1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint5_translateZ1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint5_rotateX1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint5_rotateY1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.959572214283028;
createNode animCurveTA -n "joint5_rotateZ1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint5_scaleX1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint5_scaleY1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint5_scaleZ1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint6_visibility1";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint6_translateX1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint6_translateY1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint6_translateZ1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint6_rotateX1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint6_rotateY1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.959572214283028;
createNode animCurveTA -n "joint6_rotateZ1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint6_scaleX1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint6_scaleY1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint6_scaleZ1";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode lambert -n "shader";
	setAttr ".c" -type "float3" 0.60000002 0.30000001 0.30000001 ;
createNode shadingEngine -n "shaderSG";
	setAttr ".ihi" 0;
	setAttr -s 11 ".dsm";
	setAttr ".ro" yes;
createNode materialInfo -n "materialInfo1";
createNode skinCluster -n "skinCluster3";
	setAttr -s 38 ".wl";
	setAttr ".wl[0].w[0]"  1;
	setAttr ".wl[1].w[0]"  1;
	setAttr ".wl[2].w[0]"  1;
	setAttr ".wl[3].w[0]"  1;
	setAttr ".wl[4].w[0]"  1;
	setAttr ".wl[5].w[0]"  1;
	setAttr ".wl[6].w[5]"  1;
	setAttr ".wl[7].w[5]"  1;
	setAttr ".wl[8].w[5]"  1;
	setAttr ".wl[9].w[5]"  1;
	setAttr ".wl[10].w[5]"  1;
	setAttr ".wl[11].w[5]"  1;
	setAttr ".wl[12].w[0]"  1;
	setAttr ".wl[13].w[5]"  1;
	setAttr ".wl[14].w[4]"  1;
	setAttr ".wl[15].w[4]"  1;
	setAttr ".wl[16].w[4]"  1;
	setAttr ".wl[17].w[4]"  1;
	setAttr ".wl[18].w[4]"  1;
	setAttr ".wl[19].w[4]"  1;
	setAttr ".wl[20].w[3]"  1;
	setAttr ".wl[21].w[3]"  1;
	setAttr ".wl[22].w[3]"  1;
	setAttr ".wl[23].w[3]"  1;
	setAttr ".wl[24].w[3]"  1;
	setAttr ".wl[25].w[3]"  1;
	setAttr ".wl[26].w[2]"  1;
	setAttr ".wl[27].w[2]"  1;
	setAttr ".wl[28].w[2]"  1;
	setAttr ".wl[29].w[2]"  1;
	setAttr ".wl[30].w[2]"  1;
	setAttr ".wl[31].w[2]"  1;
	setAttr ".wl[32].w[1]"  1;
	setAttr ".wl[33].w[1]"  1;
	setAttr ".wl[34].w[1]"  1;
	setAttr ".wl[35].w[1]"  1;
	setAttr ".wl[36].w[1]"  1;
	setAttr ".wl[37].w[1]"  1;
	setAttr -s 6 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -600 0 600 1;
	setAttr ".pm[1]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -500 0 600 1;
	setAttr ".pm[2]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -400 0 600 1;
	setAttr ".pm[3]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -300 0 600 1;
	setAttr ".pm[4]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -200 0 600 1;
	setAttr ".pm[5]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -100 0 600 1;
	setAttr ".gm" -type "matrix" 1.1102230246251565e-016 0.5 0 0 -1 2.2204460492503131e-016 0 0
		 0 0 0.5 0 350 -2.4428756802111855e-015 -600 1;
	setAttr -s 6 ".ma";
	setAttr -s 6 ".dpf[0:5]"  6 6 6 6 6 6;
	setAttr -s 6 ".lw";
	setAttr -s 6 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 1;
	setAttr ".bm" 0;
	setAttr ".ucm" yes;
createNode tweak -n "tweak3";
createNode objectSet -n "skinCluster3Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster3GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster3GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet3";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId6";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts6";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode dagPose -n "bindPose5";
	setAttr -s 25 ".wm";
	setAttr -s 25 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 -600 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[9]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[10]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[11]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[12]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[13]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[14]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[15]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[16]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[17]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[18]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[19]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[20]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[21]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[22]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[23]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[24]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr -s 25 ".m";
	setAttr -s 25 ".p";
	setAttr -s 25 ".g[0:24]" yes yes yes yes yes yes yes yes yes no yes 
		yes no yes yes no yes yes no yes yes no yes yes no;
	setAttr ".bp" yes;
createNode displayLayer -n "layer1";
	setAttr ".dt" 2;
	setAttr ".do" 1;
createNode animCurveTU -n "joint9_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint9_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint9_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint9_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 -6 12 -6;
createNode animCurveTA -n "joint9_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint9_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.96;
createNode animCurveTA -n "joint9_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint9_scaleX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint9_scaleY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint9_scaleZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint2_visibility2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint2_translateX2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint2_translateY2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint2_translateZ2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint2_rotateX2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint2_rotateY2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.96;
createNode animCurveTA -n "joint2_rotateZ2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint2_scaleX2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint2_scaleY2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint2_scaleZ2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint3_visibility2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint3_translateX2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint3_translateY2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint3_translateZ2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint3_rotateX2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint3_rotateY2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.96;
createNode animCurveTA -n "joint3_rotateZ2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint3_scaleX2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint3_scaleY2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint3_scaleZ2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint4_visibility2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint4_translateX2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint4_translateY2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint4_translateZ2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint4_rotateX2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint4_rotateY2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.96;
createNode animCurveTA -n "joint4_rotateZ2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint4_scaleX2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint4_scaleY2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint4_scaleZ2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint5_visibility2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint5_translateX2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint5_translateY2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint5_translateZ2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint5_rotateX2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint5_rotateY2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.96;
createNode animCurveTA -n "joint5_rotateZ2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint5_scaleX2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint5_scaleY2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint5_scaleZ2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint6_visibility2";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint6_translateX2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint6_translateY2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint6_translateZ2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint6_rotateX2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint6_rotateY2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.96;
createNode animCurveTA -n "joint6_rotateZ2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint6_scaleX2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint6_scaleY2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint6_scaleZ2";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint7_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint7_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint7_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint7_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint7_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint7_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.96;
createNode animCurveTA -n "joint7_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint7_scaleX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint7_scaleY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint7_scaleZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint8_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  12 0;
createNode animCurveTL -n "joint8_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  12 0;
createNode animCurveTL -n "joint8_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  12 -3;
createNode skinCluster -n "skinCluster4";
	setAttr -s 68 ".wl";
	setAttr ".wl[0].w[10]"  1;
	setAttr ".wl[1].w[10]"  1;
	setAttr ".wl[2].w[10]"  1;
	setAttr ".wl[3].w[10]"  1;
	setAttr ".wl[4].w[10]"  1;
	setAttr ".wl[5].w[10]"  1;
	setAttr ".wl[6].w[5]"  1;
	setAttr ".wl[7].w[5]"  1;
	setAttr ".wl[8].w[5]"  1;
	setAttr ".wl[9].w[5]"  1;
	setAttr ".wl[10].w[5]"  1;
	setAttr ".wl[11].w[5]"  1;
	setAttr ".wl[12].w[10]"  1;
	setAttr ".wl[13].w[5]"  1;
	setAttr ".wl[14].w[6]"  1;
	setAttr ".wl[15].w[6]"  1;
	setAttr ".wl[16].w[6]"  1;
	setAttr ".wl[17].w[6]"  1;
	setAttr ".wl[18].w[6]"  1;
	setAttr ".wl[19].w[6]"  1;
	setAttr ".wl[20].w[7]"  1;
	setAttr ".wl[21].w[7]"  1;
	setAttr ".wl[22].w[7]"  1;
	setAttr ".wl[23].w[7]"  1;
	setAttr ".wl[24].w[7]"  1;
	setAttr ".wl[25].w[7]"  1;
	setAttr ".wl[26].w[8]"  1;
	setAttr ".wl[27].w[8]"  1;
	setAttr ".wl[28].w[8]"  1;
	setAttr ".wl[29].w[8]"  1;
	setAttr ".wl[30].w[8]"  1;
	setAttr ".wl[31].w[8]"  1;
	setAttr ".wl[32].w[9]"  1;
	setAttr ".wl[33].w[9]"  1;
	setAttr ".wl[34].w[9]"  1;
	setAttr ".wl[35].w[9]"  1;
	setAttr ".wl[36].w[9]"  1;
	setAttr ".wl[37].w[9]"  1;
	setAttr ".wl[38].w[0]"  1;
	setAttr ".wl[39].w[0]"  1;
	setAttr ".wl[40].w[0]"  1;
	setAttr ".wl[41].w[0]"  1;
	setAttr ".wl[42].w[0]"  1;
	setAttr ".wl[43].w[0]"  1;
	setAttr ".wl[44].w[1]"  1;
	setAttr ".wl[45].w[1]"  1;
	setAttr ".wl[46].w[1]"  1;
	setAttr ".wl[47].w[1]"  1;
	setAttr ".wl[48].w[1]"  1;
	setAttr ".wl[49].w[1]"  1;
	setAttr ".wl[50].w[12]"  1;
	setAttr ".wl[51].w[12]"  1;
	setAttr ".wl[52].w[12]"  1;
	setAttr ".wl[53].w[12]"  1;
	setAttr ".wl[54].w[12]"  1;
	setAttr ".wl[55].w[12]"  1;
	setAttr ".wl[56].w[11]"  1;
	setAttr ".wl[57].w[11]"  1;
	setAttr ".wl[58].w[11]"  1;
	setAttr ".wl[59].w[11]"  1;
	setAttr ".wl[60].w[11]"  1;
	setAttr ".wl[61].w[11]"  1;
	setAttr ".wl[62].w[4]"  1;
	setAttr ".wl[63].w[4]"  1;
	setAttr ".wl[64].w[4]"  1;
	setAttr ".wl[65].w[4]"  1;
	setAttr ".wl[66].w[4]"  1;
	setAttr ".wl[67].w[4]"  1;
	setAttr -s 13 ".pm";
	setAttr ".pm[0]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -100 0 -300 1;
	setAttr ".pm[1]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -200 0 -300 1;
	setAttr ".pm[2]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -300 0 -300 1;
	setAttr ".pm[3]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -400 0 -300 1;
	setAttr ".pm[4]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -500 0 -300 1;
	setAttr ".pm[5]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 0 0 -300 1;
	setAttr ".pm[6]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -100 0 -300 1;
	setAttr ".pm[7]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -200 0 -300 1;
	setAttr ".pm[8]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -300 0 -300 1;
	setAttr ".pm[9]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -400 0 -300 1;
	setAttr ".pm[10]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -500 0 -300 1;
	setAttr ".pm[11]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -400 0 -300 1;
	setAttr ".pm[12]" -type "matrix" 1 0 0 0 0 1 0 0 0 0 1 0 -300 0 -300 1;
	setAttr ".gm" -type "matrix" 1.1102230246251565e-016 0.5 0 0 -1 2.2204460492503131e-016 0 0
		 0 0 0.5 0 300 0 300 1;
	setAttr -s 11 ".ma";
	setAttr -s 13 ".dpf[0:12]"  6 6 6 6 6 6 6 6 6 6 6 6 6;
	setAttr -s 11 ".lw";
	setAttr ".mmi" yes;
	setAttr ".mi" 1;
	setAttr ".bm" 0;
	setAttr ".ptw" -type "doubleArray" 68 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 1 1 1 1 1 1 ;
	setAttr ".ucm" yes;
createNode tweak -n "tweak4";
createNode objectSet -n "skinCluster4Set";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "skinCluster4GroupId";
	setAttr ".ihi" 0;
createNode groupParts -n "skinCluster4GroupParts";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode objectSet -n "tweakSet4";
	setAttr ".ihi" 0;
	setAttr ".vo" yes;
createNode groupId -n "groupId8";
	setAttr ".ihi" 0;
createNode groupParts -n "groupParts8";
	setAttr ".ihi" 0;
	setAttr ".ic" -type "componentList" 1 "vtx[*]";
createNode dagPose -n "bindPose6";
	setAttr -s 21 ".wm";
	setAttr -s 23 ".xm";
	setAttr ".xm[0]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 300 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[1]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[2]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[3]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[4]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[5]" -type "matrix" "xform" 1 1 1 0 0 0 0 100.00000000000006 0 5.6843418860808015e-014 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[6]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[7]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[8]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[9]" -type "matrix" "xform" 1 1 1 0 0 0 0 100.00000000000006 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[10]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[11]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[12]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[13]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[14]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[15]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[16]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[17]" -type "matrix" "xform" 1 1 1 0 0 0 0 100 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[18]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[19]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 yes;
	setAttr ".xm[20]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[21]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr ".xm[22]" -type "matrix" "xform" 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
		 0 0 0 0 0 0 0 0 0 0 1 0 0 0 1 1 1 1 no;
	setAttr -s 21 ".m";
	setAttr -s 21 ".p";
	setAttr -s 21 ".g[2:22]" yes yes no no yes yes no no yes yes no no 
		yes yes no no yes yes no no no;
	setAttr ".bp" yes;
createNode animCurveTU -n "joint11_visibility";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint11_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint11_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint11_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 3 12 3;
createNode animCurveTA -n "joint11_rotateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint11_rotateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.96;
createNode animCurveTA -n "joint11_rotateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint11_scaleX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint11_scaleY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint11_scaleZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint2_visibility3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint2_translateX3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint2_translateY3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint2_translateZ3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint2_rotateX3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint2_rotateY3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.96;
createNode animCurveTA -n "joint2_rotateZ3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint2_scaleX3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint2_scaleY3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint2_scaleZ3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint3_visibility3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint3_translateX3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint3_translateY3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint3_translateZ3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint3_rotateX3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint3_rotateY3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.96;
createNode animCurveTA -n "joint3_rotateZ3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint3_scaleX3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint3_scaleY3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint3_scaleZ3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint4_visibility3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint4_translateX3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint4_translateY3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint4_translateZ3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint4_rotateX3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint4_rotateY3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.96;
createNode animCurveTA -n "joint4_rotateZ3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint4_scaleX3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint4_scaleY3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint4_scaleZ3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint5_visibility3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint5_translateX3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint5_translateY3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint5_translateZ3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint5_rotateX3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint5_rotateY3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.96;
createNode animCurveTA -n "joint5_rotateZ3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint5_scaleX3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint5_scaleY3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint5_scaleZ3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint6_visibility3";
	setAttr ".tan" 9;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
	setAttr -s 2 ".kot[0:1]"  5 5;
createNode animCurveTL -n "joint6_translateX3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint6_translateY3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTL -n "joint6_translateZ3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint6_rotateX3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTA -n "joint6_rotateY3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 71.96;
createNode animCurveTA -n "joint6_rotateZ3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 0 12 0;
createNode animCurveTU -n "joint6_scaleX3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint6_scaleY3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTU -n "joint6_scaleZ3";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr -s 2 ".ktv[0:1]"  1 1 12 1;
createNode animCurveTL -n "joint1_translateX";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  12 0;
createNode animCurveTL -n "joint1_translateY";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  12 0;
createNode animCurveTL -n "joint1_translateZ";
	setAttr ".tan" 18;
	setAttr ".wgt" no;
	setAttr ".ktv[0]"  12 0;
createNode hyperGraphInfo -n "nodeEditorPanel1Info";
createNode hyperView -n "hyperView1";
	setAttr ".vl" -type "double2" -114.28571428571429 -92.857142857142861 ;
	setAttr ".vh" -type "double2" 530.95238095238096 30.952380952380956 ;
	setAttr ".dag" no;
createNode hyperLayout -n "hyperLayout1";
	setAttr ".ihi" 0;
	setAttr ".anf" yes;
select -ne :time1;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".o" 7;
	setAttr ".unw" 7;
select -ne :renderPartition;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 3 ".st";
	setAttr -k on ".an";
	setAttr -k on ".pt";
select -ne :initialShadingGroup;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 4 ".dsm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -av -cb on ".micc";
	setAttr -cb on ".mica";
	setAttr -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :initialParticleSE;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr ".ro" yes;
	setAttr -cb on ".mimt";
	setAttr -cb on ".miop";
	setAttr -cb on ".mise";
	setAttr -cb on ".mism";
	setAttr -cb on ".mice";
	setAttr -cb on ".micc";
	setAttr -cb on ".mica";
	setAttr -av -cb on ".micw";
	setAttr -cb on ".mirw";
select -ne :defaultShaderList1;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 3 ".s";
select -ne :postProcessList1;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -s 2 ".p";
select -ne :defaultRenderingList1;
select -ne :renderGlobalsList1;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
select -ne :defaultRenderGlobals;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".ifg";
	setAttr -k on ".clip";
	setAttr -k on ".edm";
	setAttr -k on ".edl";
	setAttr -av -k on ".esr";
	setAttr -k on ".ors";
	setAttr -k on ".sdf";
	setAttr -k on ".gama";
	setAttr -k on ".ar";
	setAttr ".fs" 1;
	setAttr ".ef" 10;
	setAttr -k on ".bfs";
	setAttr -k on ".me";
	setAttr -k on ".se";
	setAttr -k on ".be";
	setAttr -k on ".ep";
	setAttr -k on ".fec";
	setAttr -k on ".ofc";
	setAttr -k on ".ofe";
	setAttr -k on ".efe";
	setAttr -k on ".umfn";
	setAttr -k on ".ufe";
	setAttr -k on ".peie";
	setAttr -k on ".ifp";
	setAttr -k on ".comp";
	setAttr -k on ".cth";
	setAttr -k on ".soll";
	setAttr -k on ".rd";
	setAttr -k on ".lp";
	setAttr -av -k on ".sp";
	setAttr -k on ".shs";
	setAttr -k on ".lpr";
	setAttr -k on ".gv";
	setAttr -k on ".sv";
	setAttr -k on ".mm";
	setAttr -k on ".npu";
	setAttr -k on ".itf";
	setAttr -k on ".shp";
	setAttr -k on ".isp";
	setAttr -k on ".uf";
	setAttr -k on ".oi";
	setAttr -k on ".rut";
	setAttr -k on ".mb";
	setAttr -av -k on ".mbf";
	setAttr -k on ".afp";
	setAttr -k on ".pfb";
	setAttr -k on ".pfrm";
	setAttr -k on ".pfom";
	setAttr -av -k on ".bll";
	setAttr -k on ".bls";
	setAttr -k on ".smv";
	setAttr -k on ".ubc";
	setAttr -k on ".mbc";
	setAttr -k on ".mbt";
	setAttr -k on ".udbx";
	setAttr -k on ".smc";
	setAttr -k on ".kmv";
	setAttr -k on ".isl";
	setAttr -k on ".ism";
	setAttr -k on ".imb";
	setAttr -k on ".rlen";
	setAttr -av -k on ".frts";
	setAttr -k on ".tlwd";
	setAttr -k on ".tlht";
	setAttr -k on ".jfc";
select -ne :defaultResolution;
	setAttr -av -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -av -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -av ".w";
	setAttr -av ".h";
	setAttr -av -k on ".pa" 1;
	setAttr -av -k on ".al";
	setAttr -av ".dar";
	setAttr -av -k on ".ldar";
	setAttr -k on ".dpi";
	setAttr -av -k on ".off";
	setAttr -av -k on ".fld";
	setAttr -av -k on ".zsl";
	setAttr -k on ".isu";
	setAttr -k on ".pdu";
select -ne :defaultLightSet;
	setAttr -k on ".cch";
	setAttr -k on ".ihi";
	setAttr -k on ".nds";
	setAttr -k on ".bnm";
	setAttr -k on ".mwc";
	setAttr -k on ".an";
	setAttr -k on ".il";
	setAttr -k on ".vo";
	setAttr -k on ".eo";
	setAttr -k on ".fo";
	setAttr -k on ".epo";
	setAttr -k on ".ro" yes;
select -ne :defaultObjectSet;
	setAttr ".ro" yes;
select -ne :hardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr ".ctrs" 256;
	setAttr ".btrs" 512;
	setAttr -k off ".fbfm";
	setAttr -k off -cb on ".ehql";
	setAttr -k off -cb on ".eams";
	setAttr -k off -cb on ".eeaa";
	setAttr -k off -cb on ".engm";
	setAttr -k off -cb on ".mes";
	setAttr -k off -cb on ".emb";
	setAttr -av -k off -cb on ".mbbf";
	setAttr -k off -cb on ".mbs";
	setAttr -k off -cb on ".trm";
	setAttr -k off -cb on ".tshc";
	setAttr -k off ".enpt";
	setAttr -k off -cb on ".clmt";
	setAttr -k off -cb on ".tcov";
	setAttr -k off -cb on ".lith";
	setAttr -k off -cb on ".sobc";
	setAttr -k off -cb on ".cuth";
	setAttr -k off -cb on ".hgcd";
	setAttr -k off -cb on ".hgci";
	setAttr -k off -cb on ".mgcs";
	setAttr -k off -cb on ".twa";
	setAttr -k off -cb on ".twz";
	setAttr -k on ".hwcc";
	setAttr -k on ".hwdp";
	setAttr -k on ".hwql";
	setAttr -k on ".hwfr";
select -ne :defaultHardwareRenderGlobals;
	setAttr -k on ".cch";
	setAttr -cb on ".ihi";
	setAttr -k on ".nds";
	setAttr -cb on ".bnm";
	setAttr -k on ".rp";
	setAttr -k on ".cai";
	setAttr -k on ".coi";
	setAttr -cb on ".bc";
	setAttr -av -k on ".bcb";
	setAttr -av -k on ".bcg";
	setAttr -av -k on ".bcr";
	setAttr -k on ".ei";
	setAttr -k on ".ex";
	setAttr -av -k on ".es";
	setAttr -av -k on ".ef";
	setAttr -av -k on ".bf";
	setAttr -k on ".fii";
	setAttr -av -k on ".sf";
	setAttr -k on ".gr";
	setAttr -k on ".li";
	setAttr -k on ".ls";
	setAttr -k on ".mb";
	setAttr -k on ".ti";
	setAttr -k on ".txt";
	setAttr -k on ".mpr";
	setAttr -k on ".wzd";
	setAttr ".fn" -type "string" "im";
	setAttr -k on ".if";
	setAttr ".res" -type "string" "ntsc_4d 646 485 1.333";
	setAttr -k on ".as";
	setAttr -k on ".ds";
	setAttr -k on ".lm";
	setAttr -k on ".fir";
	setAttr -k on ".aap";
	setAttr -k on ".gh";
	setAttr -cb on ".sd";
connectAttr "joint1_scaleX.o" "n0.sx";
connectAttr "joint1_scaleY.o" "n0.sy";
connectAttr "joint1_scaleZ.o" "n0.sz";
connectAttr "joint1_visibility.o" "n0.v";
connectAttr "joint1_translateX.o" "n0.tx";
connectAttr "joint1_translateY.o" "n0.ty";
connectAttr "joint1_translateZ.o" "n0.tz";
connectAttr "joint1_rotateX.o" "n0.rx";
connectAttr "joint1_rotateY.o" "n0.ry";
connectAttr "joint1_rotateZ.o" "n0.rz";
connectAttr "n0.s" "n1.is";
connectAttr "joint2_scaleX1.o" "n1.sx";
connectAttr "joint2_scaleY1.o" "n1.sy";
connectAttr "joint2_scaleZ1.o" "n1.sz";
connectAttr "joint2_visibility1.o" "n1.v";
connectAttr "joint2_translateX1.o" "n1.tx";
connectAttr "joint2_translateY1.o" "n1.ty";
connectAttr "joint2_translateZ1.o" "n1.tz";
connectAttr "joint2_rotateX1.o" "n1.rx";
connectAttr "joint2_rotateY1.o" "n1.ry";
connectAttr "joint2_rotateZ1.o" "n1.rz";
connectAttr "n1.s" "n2.is";
connectAttr "joint3_scaleX1.o" "n2.sx";
connectAttr "joint3_scaleY1.o" "n2.sy";
connectAttr "joint3_scaleZ1.o" "n2.sz";
connectAttr "joint3_visibility1.o" "n2.v";
connectAttr "joint3_translateX1.o" "n2.tx";
connectAttr "joint3_translateY1.o" "n2.ty";
connectAttr "joint3_translateZ1.o" "n2.tz";
connectAttr "joint3_rotateX1.o" "n2.rx";
connectAttr "joint3_rotateY1.o" "n2.ry";
connectAttr "joint3_rotateZ1.o" "n2.rz";
connectAttr "n2.s" "n3.is";
connectAttr "joint4_scaleX1.o" "n3.sx";
connectAttr "joint4_scaleY1.o" "n3.sy";
connectAttr "joint4_scaleZ1.o" "n3.sz";
connectAttr "joint4_visibility1.o" "n3.v";
connectAttr "joint4_translateX1.o" "n3.tx";
connectAttr "joint4_translateY1.o" "n3.ty";
connectAttr "joint4_translateZ1.o" "n3.tz";
connectAttr "joint4_rotateX1.o" "n3.rx";
connectAttr "joint4_rotateY1.o" "n3.ry";
connectAttr "joint4_rotateZ1.o" "n3.rz";
connectAttr "n3.s" "n4.is";
connectAttr "joint5_scaleX1.o" "n4.sx";
connectAttr "joint5_scaleY1.o" "n4.sy";
connectAttr "joint5_scaleZ1.o" "n4.sz";
connectAttr "joint5_visibility1.o" "n4.v";
connectAttr "joint5_translateX1.o" "n4.tx";
connectAttr "joint5_translateY1.o" "n4.ty";
connectAttr "joint5_translateZ1.o" "n4.tz";
connectAttr "joint5_rotateX1.o" "n4.rx";
connectAttr "joint5_rotateY1.o" "n4.ry";
connectAttr "joint5_rotateZ1.o" "n4.rz";
connectAttr "n4.s" "n5.is";
connectAttr "joint6_scaleX1.o" "n5.sx";
connectAttr "joint6_scaleY1.o" "n5.sy";
connectAttr "joint6_scaleZ1.o" "n5.sz";
connectAttr "joint6_visibility1.o" "n5.v";
connectAttr "joint6_translateX1.o" "n5.tx";
connectAttr "joint6_translateY1.o" "n5.ty";
connectAttr "joint6_translateZ1.o" "n5.tz";
connectAttr "joint6_rotateX1.o" "n5.rx";
connectAttr "joint6_rotateY1.o" "n5.ry";
connectAttr "joint6_rotateZ1.o" "n5.rz";
connectAttr "n5.s" "n6.is";
connectAttr "skinCluster2GroupId.id" "nShape7.iog.og[2].gid";
connectAttr "skinCluster2Set.mwc" "nShape7.iog.og[2].gco";
connectAttr "groupId4.id" "nShape7.iog.og[3].gid";
connectAttr "tweakSet2.mwc" "nShape7.iog.og[3].gco";
connectAttr "skinCluster2.og[0]" "nShape7.i";
connectAttr "tweak2.vl[0].vt[0]" "nShape7.twl";
connectAttr "joint8_scaleX.o" "n8.sx";
connectAttr "joint8_scaleY.o" "n8.sy";
connectAttr "joint8_scaleZ.o" "n8.sz";
connectAttr "joint8_visibility.o" "n8.v";
connectAttr "joint8_translateX.o" "n8.tx";
connectAttr "joint8_translateY.o" "n8.ty";
connectAttr "joint8_translateZ.o" "n8.tz";
connectAttr "joint8_rotateX.o" "n8.rx";
connectAttr "joint8_rotateY.o" "n8.ry";
connectAttr "joint8_rotateZ.o" "n8.rz";
connectAttr "n8.s" "n9.is";
connectAttr "joint2_scaleX.o" "n9.sx";
connectAttr "joint2_scaleY.o" "n9.sy";
connectAttr "joint2_scaleZ.o" "n9.sz";
connectAttr "joint2_visibility.o" "n9.v";
connectAttr "joint2_translateX.o" "n9.tx";
connectAttr "joint2_translateY.o" "n9.ty";
connectAttr "joint2_translateZ.o" "n9.tz";
connectAttr "joint2_rotateX.o" "n9.rx";
connectAttr "joint2_rotateY.o" "n9.ry";
connectAttr "joint2_rotateZ.o" "n9.rz";
connectAttr "n9.s" "n10.is";
connectAttr "joint3_scaleX.o" "n10.sx";
connectAttr "joint3_scaleY.o" "n10.sy";
connectAttr "joint3_scaleZ.o" "n10.sz";
connectAttr "joint3_visibility.o" "n10.v";
connectAttr "joint3_translateX.o" "n10.tx";
connectAttr "joint3_translateY.o" "n10.ty";
connectAttr "joint3_translateZ.o" "n10.tz";
connectAttr "joint3_rotateX.o" "n10.rx";
connectAttr "joint3_rotateY.o" "n10.ry";
connectAttr "joint3_rotateZ.o" "n10.rz";
connectAttr "n10.s" "n11.is";
connectAttr "joint4_scaleX.o" "n11.sx";
connectAttr "joint4_scaleY.o" "n11.sy";
connectAttr "joint4_scaleZ.o" "n11.sz";
connectAttr "joint4_visibility.o" "n11.v";
connectAttr "joint4_translateX.o" "n11.tx";
connectAttr "joint4_translateY.o" "n11.ty";
connectAttr "joint4_translateZ.o" "n11.tz";
connectAttr "joint4_rotateX.o" "n11.rx";
connectAttr "joint4_rotateY.o" "n11.ry";
connectAttr "joint4_rotateZ.o" "n11.rz";
connectAttr "n11.s" "n12.is";
connectAttr "joint5_scaleX.o" "n12.sx";
connectAttr "joint5_scaleY.o" "n12.sy";
connectAttr "joint5_scaleZ.o" "n12.sz";
connectAttr "joint5_visibility.o" "n12.v";
connectAttr "joint5_translateX.o" "n12.tx";
connectAttr "joint5_translateY.o" "n12.ty";
connectAttr "joint5_translateZ.o" "n12.tz";
connectAttr "joint5_rotateX.o" "n12.rx";
connectAttr "joint5_rotateY.o" "n12.ry";
connectAttr "joint5_rotateZ.o" "n12.rz";
connectAttr "n12.s" "n13.is";
connectAttr "joint6_scaleX.o" "n13.sx";
connectAttr "joint6_scaleY.o" "n13.sy";
connectAttr "joint6_scaleZ.o" "n13.sz";
connectAttr "joint6_visibility.o" "n13.v";
connectAttr "joint6_translateX.o" "n13.tx";
connectAttr "joint6_translateY.o" "n13.ty";
connectAttr "joint6_translateZ.o" "n13.tz";
connectAttr "joint6_rotateX.o" "n13.rx";
connectAttr "joint6_rotateY.o" "n13.ry";
connectAttr "joint6_rotateZ.o" "n13.rz";
connectAttr "n13.s" "n14.is";
connectAttr "skinCluster1GroupId.id" "nShape15.iog.og[2].gid";
connectAttr "skinCluster1Set.mwc" "nShape15.iog.og[2].gco";
connectAttr "groupId2.id" "nShape15.iog.og[3].gid";
connectAttr "tweakSet1.mwc" "nShape15.iog.og[3].gco";
connectAttr "skinCluster1.og[0]" "nShape15.i";
connectAttr "tweak1.vl[0].vt[0]" "nShape15.twl";
connectAttr "joint9_scaleX.o" "n16.sx";
connectAttr "joint9_scaleY.o" "n16.sy";
connectAttr "joint9_scaleZ.o" "n16.sz";
connectAttr "joint9_visibility.o" "n16.v";
connectAttr "joint9_translateX.o" "n16.tx";
connectAttr "joint9_translateY.o" "n16.ty";
connectAttr "joint9_translateZ.o" "n16.tz";
connectAttr "joint9_rotateX.o" "n16.rx";
connectAttr "joint9_rotateY.o" "n16.ry";
connectAttr "joint9_rotateZ.o" "n16.rz";
connectAttr "n16.s" "n17.is";
connectAttr "joint2_scaleX2.o" "n17.sx";
connectAttr "joint2_scaleY2.o" "n17.sy";
connectAttr "joint2_scaleZ2.o" "n17.sz";
connectAttr "joint2_visibility2.o" "n17.v";
connectAttr "joint2_translateX2.o" "n17.tx";
connectAttr "joint2_translateY2.o" "n17.ty";
connectAttr "joint2_translateZ2.o" "n17.tz";
connectAttr "joint2_rotateX2.o" "n17.rx";
connectAttr "joint2_rotateY2.o" "n17.ry";
connectAttr "joint2_rotateZ2.o" "n17.rz";
connectAttr "n17.s" "n18.is";
connectAttr "joint3_scaleX2.o" "n18.sx";
connectAttr "joint3_scaleY2.o" "n18.sy";
connectAttr "joint3_scaleZ2.o" "n18.sz";
connectAttr "joint3_visibility2.o" "n18.v";
connectAttr "joint3_translateX2.o" "n18.tx";
connectAttr "joint3_translateY2.o" "n18.ty";
connectAttr "joint3_translateZ2.o" "n18.tz";
connectAttr "joint3_rotateX2.o" "n18.rx";
connectAttr "joint3_rotateY2.o" "n18.ry";
connectAttr "joint3_rotateZ2.o" "n18.rz";
connectAttr "n18.s" "n19.is";
connectAttr "joint4_scaleX2.o" "n19.sx";
connectAttr "joint4_scaleY2.o" "n19.sy";
connectAttr "joint4_scaleZ2.o" "n19.sz";
connectAttr "joint4_visibility2.o" "n19.v";
connectAttr "joint4_translateX2.o" "n19.tx";
connectAttr "joint4_translateY2.o" "n19.ty";
connectAttr "joint4_translateZ2.o" "n19.tz";
connectAttr "joint4_rotateX2.o" "n19.rx";
connectAttr "joint4_rotateY2.o" "n19.ry";
connectAttr "joint4_rotateZ2.o" "n19.rz";
connectAttr "n19.s" "n20.is";
connectAttr "joint5_scaleX2.o" "n20.sx";
connectAttr "joint5_scaleY2.o" "n20.sy";
connectAttr "joint5_scaleZ2.o" "n20.sz";
connectAttr "joint5_visibility2.o" "n20.v";
connectAttr "joint5_translateX2.o" "n20.tx";
connectAttr "joint5_translateY2.o" "n20.ty";
connectAttr "joint5_translateZ2.o" "n20.tz";
connectAttr "joint5_rotateX2.o" "n20.rx";
connectAttr "joint5_rotateY2.o" "n20.ry";
connectAttr "joint5_rotateZ2.o" "n20.rz";
connectAttr "n20.s" "n21.is";
connectAttr "joint6_scaleX2.o" "n21.sx";
connectAttr "joint6_scaleY2.o" "n21.sy";
connectAttr "joint6_scaleZ2.o" "n21.sz";
connectAttr "joint6_visibility2.o" "n21.v";
connectAttr "joint6_translateX2.o" "n21.tx";
connectAttr "joint6_translateY2.o" "n21.ty";
connectAttr "joint6_translateZ2.o" "n21.tz";
connectAttr "joint6_rotateX2.o" "n21.rx";
connectAttr "joint6_rotateY2.o" "n21.ry";
connectAttr "joint6_rotateZ2.o" "n21.rz";
connectAttr "n21.s" "n22.is";
connectAttr "joint7_rotateX.o" "n22.rx";
connectAttr "joint7_rotateY.o" "n22.ry";
connectAttr "joint7_rotateZ.o" "n22.rz";
connectAttr "joint7_scaleX.o" "n22.sx";
connectAttr "joint7_scaleY.o" "n22.sy";
connectAttr "joint7_scaleZ.o" "n22.sz";
connectAttr "joint7_visibility.o" "n22.v";
connectAttr "joint7_translateX.o" "n22.tx";
connectAttr "joint7_translateY.o" "n22.ty";
connectAttr "joint7_translateZ.o" "n22.tz";
connectAttr "n22.s" "n24.is";
connectAttr "n26.crx" "n25.rx";
connectAttr "n26.cry" "n25.ry";
connectAttr "n26.crz" "n25.rz";
connectAttr "n25.ro" "n26.cro";
connectAttr "n25.pim" "n26.cpim";
connectAttr "n25.jo" "n26.cjo";
connectAttr "n22.r" "n26.tg[0].tr";
connectAttr "n22.ro" "n26.tg[0].tro";
connectAttr "n22.pm" "n26.tg[0].tpm";
connectAttr "n22.jo" "n26.tg[0].tjo";
connectAttr "n26.w0" "n26.tg[0].tw";
connectAttr "n21.r" "n26.tg[1].tr";
connectAttr "n21.ro" "n26.tg[1].tro";
connectAttr "n21.pm" "n26.tg[1].tpm";
connectAttr "n21.jo" "n26.tg[1].tjo";
connectAttr "n26.w1" "n26.tg[1].tw";
connectAttr "n25.s" "n27.is";
connectAttr "layer1.di" "n28.do";
connectAttr "n21.s" "n29.is";
connectAttr "n31.crx" "n30.rx";
connectAttr "n31.cry" "n30.ry";
connectAttr "n31.crz" "n30.rz";
connectAttr "n30.ro" "n31.cro";
connectAttr "n30.pim" "n31.cpim";
connectAttr "n30.jo" "n31.cjo";
connectAttr "n21.r" "n31.tg[0].tr";
connectAttr "n21.ro" "n31.tg[0].tro";
connectAttr "n21.pm" "n31.tg[0].tpm";
connectAttr "n21.jo" "n31.tg[0].tjo";
connectAttr "n31.w0" "n31.tg[0].tw";
connectAttr "n20.r" "n31.tg[1].tr";
connectAttr "n20.ro" "n31.tg[1].tro";
connectAttr "n20.pm" "n31.tg[1].tpm";
connectAttr "n20.jo" "n31.tg[1].tjo";
connectAttr "n31.w1" "n31.tg[1].tw";
connectAttr "n30.s" "n32.is";
connectAttr "layer1.di" "n33.do";
connectAttr "n20.s" "n34.is";
connectAttr "n36.crx" "n35.rx";
connectAttr "n36.cry" "n35.ry";
connectAttr "n36.crz" "n35.rz";
connectAttr "n35.ro" "n36.cro";
connectAttr "n35.pim" "n36.cpim";
connectAttr "n35.jo" "n36.cjo";
connectAttr "n20.r" "n36.tg[0].tr";
connectAttr "n20.ro" "n36.tg[0].tro";
connectAttr "n20.pm" "n36.tg[0].tpm";
connectAttr "n20.jo" "n36.tg[0].tjo";
connectAttr "n36.w0" "n36.tg[0].tw";
connectAttr "n19.r" "n36.tg[1].tr";
connectAttr "n19.ro" "n36.tg[1].tro";
connectAttr "n19.pm" "n36.tg[1].tpm";
connectAttr "n19.jo" "n36.tg[1].tjo";
connectAttr "n36.w1" "n36.tg[1].tw";
connectAttr "n35.s" "n37.is";
connectAttr "layer1.di" "n38.do";
connectAttr "n19.s" "n39.is";
connectAttr "n41.crx" "n40.rx";
connectAttr "n41.cry" "n40.ry";
connectAttr "n41.crz" "n40.rz";
connectAttr "n40.ro" "n41.cro";
connectAttr "n40.pim" "n41.cpim";
connectAttr "n40.jo" "n41.cjo";
connectAttr "n19.r" "n41.tg[0].tr";
connectAttr "n19.ro" "n41.tg[0].tro";
connectAttr "n19.pm" "n41.tg[0].tpm";
connectAttr "n19.jo" "n41.tg[0].tjo";
connectAttr "n41.w0" "n41.tg[0].tw";
connectAttr "n18.r" "n41.tg[1].tr";
connectAttr "n18.ro" "n41.tg[1].tro";
connectAttr "n18.pm" "n41.tg[1].tpm";
connectAttr "n18.jo" "n41.tg[1].tjo";
connectAttr "n41.w1" "n41.tg[1].tw";
connectAttr "n40.s" "n42.is";
connectAttr "layer1.di" "n43.do";
connectAttr "n18.s" "n44.is";
connectAttr "n46.crx" "n45.rx";
connectAttr "n46.cry" "n45.ry";
connectAttr "n46.crz" "n45.rz";
connectAttr "n45.ro" "n46.cro";
connectAttr "n45.pim" "n46.cpim";
connectAttr "n45.jo" "n46.cjo";
connectAttr "n18.r" "n46.tg[0].tr";
connectAttr "n18.ro" "n46.tg[0].tro";
connectAttr "n18.pm" "n46.tg[0].tpm";
connectAttr "n18.jo" "n46.tg[0].tjo";
connectAttr "n46.w0" "n46.tg[0].tw";
connectAttr "n17.r" "n46.tg[1].tr";
connectAttr "n17.ro" "n46.tg[1].tro";
connectAttr "n17.pm" "n46.tg[1].tpm";
connectAttr "n17.jo" "n46.tg[1].tjo";
connectAttr "n46.w1" "n46.tg[1].tw";
connectAttr "n45.s" "n47.is";
connectAttr "layer1.di" "n48.do";
connectAttr "n17.s" "n49.is";
connectAttr "n51.crx" "n50.rx";
connectAttr "n51.cry" "n50.ry";
connectAttr "n51.crz" "n50.rz";
connectAttr "n50.ro" "n51.cro";
connectAttr "n50.pim" "n51.cpim";
connectAttr "n50.jo" "n51.cjo";
connectAttr "n17.r" "n51.tg[0].tr";
connectAttr "n17.ro" "n51.tg[0].tro";
connectAttr "n17.pm" "n51.tg[0].tpm";
connectAttr "n17.jo" "n51.tg[0].tjo";
connectAttr "n51.w0" "n51.tg[0].tw";
connectAttr "n16.r" "n51.tg[1].tr";
connectAttr "n16.ro" "n51.tg[1].tro";
connectAttr "n16.pm" "n51.tg[1].tpm";
connectAttr "n16.jo" "n51.tg[1].tjo";
connectAttr "n51.w1" "n51.tg[1].tw";
connectAttr "n50.s" "n52.is";
connectAttr "layer1.di" "n53.do";
connectAttr "skinCluster3GroupId.id" "nShape54.iog.og[4].gid";
connectAttr "skinCluster3Set.mwc" "nShape54.iog.og[4].gco";
connectAttr "groupId6.id" "nShape54.iog.og[5].gid";
connectAttr "tweakSet3.mwc" "nShape54.iog.og[5].gco";
connectAttr "skinCluster3.og[0]" "nShape54.i";
connectAttr "tweak3.vl[0].vt[0]" "nShape54.twl";
connectAttr "joint11_scaleX.o" "n55.sx";
connectAttr "joint11_scaleY.o" "n55.sy";
connectAttr "joint11_scaleZ.o" "n55.sz";
connectAttr "joint11_visibility.o" "n55.v";
connectAttr "joint11_translateX.o" "n55.tx";
connectAttr "joint11_translateY.o" "n55.ty";
connectAttr "joint11_translateZ.o" "n55.tz";
connectAttr "joint11_rotateX.o" "n55.rx";
connectAttr "joint11_rotateY.o" "n55.ry";
connectAttr "joint11_rotateZ.o" "n55.rz";
connectAttr "n55.s" "n56.is";
connectAttr "joint2_scaleX3.o" "n56.sx";
connectAttr "joint2_scaleY3.o" "n56.sy";
connectAttr "joint2_scaleZ3.o" "n56.sz";
connectAttr "joint2_visibility3.o" "n56.v";
connectAttr "joint2_translateX3.o" "n56.tx";
connectAttr "joint2_translateY3.o" "n56.ty";
connectAttr "joint2_translateZ3.o" "n56.tz";
connectAttr "joint2_rotateX3.o" "n56.rx";
connectAttr "joint2_rotateY3.o" "n56.ry";
connectAttr "joint2_rotateZ3.o" "n56.rz";
connectAttr "n56.s" "n57.is";
connectAttr "joint3_scaleX3.o" "n57.sx";
connectAttr "joint3_scaleY3.o" "n57.sy";
connectAttr "joint3_scaleZ3.o" "n57.sz";
connectAttr "joint3_visibility3.o" "n57.v";
connectAttr "joint3_translateX3.o" "n57.tx";
connectAttr "joint3_translateY3.o" "n57.ty";
connectAttr "joint3_translateZ3.o" "n57.tz";
connectAttr "joint3_rotateX3.o" "n57.rx";
connectAttr "joint3_rotateY3.o" "n57.ry";
connectAttr "joint3_rotateZ3.o" "n57.rz";
connectAttr "n57.s" "n58.is";
connectAttr "joint4_scaleX3.o" "n58.sx";
connectAttr "joint4_scaleY3.o" "n58.sy";
connectAttr "joint4_scaleZ3.o" "n58.sz";
connectAttr "joint4_visibility3.o" "n58.v";
connectAttr "joint4_translateX3.o" "n58.tx";
connectAttr "joint4_translateY3.o" "n58.ty";
connectAttr "joint4_translateZ3.o" "n58.tz";
connectAttr "joint4_rotateX3.o" "n58.rx";
connectAttr "joint4_rotateY3.o" "n58.ry";
connectAttr "joint4_rotateZ3.o" "n58.rz";
connectAttr "n58.s" "n59.is";
connectAttr "joint5_scaleX3.o" "n59.sx";
connectAttr "joint5_scaleY3.o" "n59.sy";
connectAttr "joint5_scaleZ3.o" "n59.sz";
connectAttr "joint5_visibility3.o" "n59.v";
connectAttr "joint5_translateX3.o" "n59.tx";
connectAttr "joint5_translateY3.o" "n59.ty";
connectAttr "joint5_translateZ3.o" "n59.tz";
connectAttr "joint5_rotateX3.o" "n59.rx";
connectAttr "joint5_rotateY3.o" "n59.ry";
connectAttr "joint5_rotateZ3.o" "n59.rz";
connectAttr "n59.s" "n60.is";
connectAttr "joint6_scaleX3.o" "n60.sx";
connectAttr "joint6_scaleY3.o" "n60.sy";
connectAttr "joint6_scaleZ3.o" "n60.sz";
connectAttr "joint6_visibility3.o" "n60.v";
connectAttr "joint6_translateX3.o" "n60.tx";
connectAttr "joint6_translateY3.o" "n60.ty";
connectAttr "joint6_translateZ3.o" "n60.tz";
connectAttr "joint6_rotateX3.o" "n60.rx";
connectAttr "joint6_rotateY3.o" "n60.ry";
connectAttr "joint6_rotateZ3.o" "n60.rz";
connectAttr "n60.s" "n61.is";
connectAttr "n60.s" "n62.is";
connectAttr "n64.crx" "n63.rx";
connectAttr "n64.cry" "n63.ry";
connectAttr "n64.crz" "n63.rz";
connectAttr "n63.ro" "n64.cro";
connectAttr "n63.pim" "n64.cpim";
connectAttr "n63.jo" "n64.cjo";
connectAttr "n60.r" "n64.tg[0].tr";
connectAttr "n60.ro" "n64.tg[0].tro";
connectAttr "n60.pm" "n64.tg[0].tpm";
connectAttr "n60.jo" "n64.tg[0].tjo";
connectAttr "n64.w0" "n64.tg[0].tw";
connectAttr "n59.r" "n64.tg[1].tr";
connectAttr "n59.ro" "n64.tg[1].tro";
connectAttr "n59.pm" "n64.tg[1].tpm";
connectAttr "n59.jo" "n64.tg[1].tjo";
connectAttr "n64.w1" "n64.tg[1].tw";
connectAttr "n63.s" "n65.is";
connectAttr "layer1.di" "n66.do";
connectAttr "n59.s" "n67.is";
connectAttr "n69.crx" "n68.rx";
connectAttr "n69.cry" "n68.ry";
connectAttr "n69.crz" "n68.rz";
connectAttr "n68.ro" "n69.cro";
connectAttr "n68.pim" "n69.cpim";
connectAttr "n68.jo" "n69.cjo";
connectAttr "n59.r" "n69.tg[0].tr";
connectAttr "n59.ro" "n69.tg[0].tro";
connectAttr "n59.pm" "n69.tg[0].tpm";
connectAttr "n59.jo" "n69.tg[0].tjo";
connectAttr "n69.w0" "n69.tg[0].tw";
connectAttr "n58.r" "n69.tg[1].tr";
connectAttr "n58.ro" "n69.tg[1].tro";
connectAttr "n58.pm" "n69.tg[1].tpm";
connectAttr "n58.jo" "n69.tg[1].tjo";
connectAttr "n69.w1" "n69.tg[1].tw";
connectAttr "n68.s" "n70.is";
connectAttr "layer1.di" "n71.do";
connectAttr "n58.s" "n72.is";
connectAttr "n74.crx" "n73.rx";
connectAttr "n74.cry" "n73.ry";
connectAttr "n74.crz" "n73.rz";
connectAttr "n73.ro" "n74.cro";
connectAttr "n73.pim" "n74.cpim";
connectAttr "n73.jo" "n74.cjo";
connectAttr "n58.r" "n74.tg[0].tr";
connectAttr "n58.ro" "n74.tg[0].tro";
connectAttr "n58.pm" "n74.tg[0].tpm";
connectAttr "n58.jo" "n74.tg[0].tjo";
connectAttr "n74.w0" "n74.tg[0].tw";
connectAttr "n57.r" "n74.tg[1].tr";
connectAttr "n57.ro" "n74.tg[1].tro";
connectAttr "n57.pm" "n74.tg[1].tpm";
connectAttr "n57.jo" "n74.tg[1].tjo";
connectAttr "n74.w1" "n74.tg[1].tw";
connectAttr "n73.s" "n75.is";
connectAttr "layer1.di" "n76.do";
connectAttr "n57.s" "n77.is";
connectAttr "n79.crx" "n78.rx";
connectAttr "n79.cry" "n78.ry";
connectAttr "n79.crz" "n78.rz";
connectAttr "n78.ro" "n79.cro";
connectAttr "n78.pim" "n79.cpim";
connectAttr "n78.jo" "n79.cjo";
connectAttr "n57.r" "n79.tg[0].tr";
connectAttr "n57.ro" "n79.tg[0].tro";
connectAttr "n57.pm" "n79.tg[0].tpm";
connectAttr "n57.jo" "n79.tg[0].tjo";
connectAttr "n79.w0" "n79.tg[0].tw";
connectAttr "n56.r" "n79.tg[1].tr";
connectAttr "n56.ro" "n79.tg[1].tro";
connectAttr "n56.pm" "n79.tg[1].tpm";
connectAttr "n56.jo" "n79.tg[1].tjo";
connectAttr "n79.w1" "n79.tg[1].tw";
connectAttr "n78.s" "n80.is";
connectAttr "layer1.di" "n81.do";
connectAttr "n56.s" "n82.is";
connectAttr "n84.crx" "n83.rx";
connectAttr "n84.cry" "n83.ry";
connectAttr "n84.crz" "n83.rz";
connectAttr "n83.ro" "n84.cro";
connectAttr "n83.pim" "n84.cpim";
connectAttr "n83.jo" "n84.cjo";
connectAttr "n56.r" "n84.tg[0].tr";
connectAttr "n56.ro" "n84.tg[0].tro";
connectAttr "n56.pm" "n84.tg[0].tpm";
connectAttr "n56.jo" "n84.tg[0].tjo";
connectAttr "n84.w0" "n84.tg[0].tw";
connectAttr "n55.r" "n84.tg[1].tr";
connectAttr "n55.ro" "n84.tg[1].tro";
connectAttr "n55.pm" "n84.tg[1].tpm";
connectAttr "n55.jo" "n84.tg[1].tjo";
connectAttr "n84.w1" "n84.tg[1].tw";
connectAttr "n83.s" "n85.is";
connectAttr "layer1.di" "n86.do";
connectAttr "skinCluster4GroupId.id" "nShape87.iog.og[4].gid";
connectAttr "skinCluster4Set.mwc" "nShape87.iog.og[4].gco";
connectAttr "groupId8.id" "nShape87.iog.og[5].gid";
connectAttr "tweakSet4.mwc" "nShape87.iog.og[5].gco";
connectAttr "skinCluster4.og[0]" "nShape87.i";
connectAttr "tweak4.vl[0].vt[0]" "nShape87.twl";
relationship "link" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "link" ":lightLinker1" "shaderSG.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialShadingGroup.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" ":initialParticleSE.message" ":defaultLightSet.message";
relationship "shadowLink" ":lightLinker1" "shaderSG.message" ":defaultLightSet.message";
connectAttr "layerManager.dli[0]" "defaultLayer.id";
connectAttr "renderLayerManager.rlmi[0]" "defaultRenderLayer.rlid";
connectAttr "skinCluster1GroupParts.og" "skinCluster1.ip[0].ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1.ip[0].gi";
connectAttr "bindPose3.msg" "skinCluster1.bp";
connectAttr "n8.wm" "skinCluster1.ma[0]";
connectAttr "n9.wm" "skinCluster1.ma[1]";
connectAttr "n10.wm" "skinCluster1.ma[2]";
connectAttr "n11.wm" "skinCluster1.ma[3]";
connectAttr "n12.wm" "skinCluster1.ma[4]";
connectAttr "n13.wm" "skinCluster1.ma[5]";
connectAttr "n8.liw" "skinCluster1.lw[0]";
connectAttr "n9.liw" "skinCluster1.lw[1]";
connectAttr "n10.liw" "skinCluster1.lw[2]";
connectAttr "n11.liw" "skinCluster1.lw[3]";
connectAttr "n12.liw" "skinCluster1.lw[4]";
connectAttr "n13.liw" "skinCluster1.lw[5]";
connectAttr "n13.msg" "skinCluster1.ptt";
connectAttr "groupParts2.og" "tweak1.ip[0].ig";
connectAttr "groupId2.id" "tweak1.ip[0].gi";
connectAttr "skinCluster1GroupId.msg" "skinCluster1Set.gn" -na;
connectAttr "nShape15.iog.og[2]" "skinCluster1Set.dsm" -na;
connectAttr "skinCluster1.msg" "skinCluster1Set.ub[0]";
connectAttr "tweak1.og[0]" "skinCluster1GroupParts.ig";
connectAttr "skinCluster1GroupId.id" "skinCluster1GroupParts.gi";
connectAttr "groupId2.msg" "tweakSet1.gn" -na;
connectAttr "nShape15.iog.og[3]" "tweakSet1.dsm" -na;
connectAttr "tweak1.msg" "tweakSet1.ub[0]";
connectAttr "nShape2Orig15.w" "groupParts2.ig";
connectAttr "groupId2.id" "groupParts2.gi";
connectAttr "n8.msg" "bindPose3.m[0]";
connectAttr "n9.msg" "bindPose3.m[1]";
connectAttr "n10.msg" "bindPose3.m[2]";
connectAttr "n11.msg" "bindPose3.m[3]";
connectAttr "n12.msg" "bindPose3.m[4]";
connectAttr "n13.msg" "bindPose3.m[5]";
connectAttr "bindPose3.w" "bindPose3.p[0]";
connectAttr "bindPose3.m[0]" "bindPose3.p[1]";
connectAttr "bindPose3.m[1]" "bindPose3.p[2]";
connectAttr "bindPose3.m[2]" "bindPose3.p[3]";
connectAttr "bindPose3.m[3]" "bindPose3.p[4]";
connectAttr "bindPose3.m[4]" "bindPose3.p[5]";
connectAttr "n8.bps" "bindPose3.wm[0]";
connectAttr "n9.bps" "bindPose3.wm[1]";
connectAttr "n10.bps" "bindPose3.wm[2]";
connectAttr "n11.bps" "bindPose3.wm[3]";
connectAttr "n12.bps" "bindPose3.wm[4]";
connectAttr "n13.bps" "bindPose3.wm[5]";
connectAttr "skinCluster2GroupParts.og" "skinCluster2.ip[0].ig";
connectAttr "skinCluster2GroupId.id" "skinCluster2.ip[0].gi";
connectAttr "bindPose4.msg" "skinCluster2.bp";
connectAttr "n0.wm" "skinCluster2.ma[0]";
connectAttr "n1.wm" "skinCluster2.ma[1]";
connectAttr "n2.wm" "skinCluster2.ma[2]";
connectAttr "n3.wm" "skinCluster2.ma[3]";
connectAttr "n4.wm" "skinCluster2.ma[4]";
connectAttr "n5.wm" "skinCluster2.ma[5]";
connectAttr "n0.liw" "skinCluster2.lw[0]";
connectAttr "n1.liw" "skinCluster2.lw[1]";
connectAttr "n2.liw" "skinCluster2.lw[2]";
connectAttr "n3.liw" "skinCluster2.lw[3]";
connectAttr "n4.liw" "skinCluster2.lw[4]";
connectAttr "n5.liw" "skinCluster2.lw[5]";
connectAttr "n5.msg" "skinCluster2.ptt";
connectAttr "groupParts4.og" "tweak2.ip[0].ig";
connectAttr "groupId4.id" "tweak2.ip[0].gi";
connectAttr "skinCluster2GroupId.msg" "skinCluster2Set.gn" -na;
connectAttr "nShape7.iog.og[2]" "skinCluster2Set.dsm" -na;
connectAttr "skinCluster2.msg" "skinCluster2Set.ub[0]";
connectAttr "tweak2.og[0]" "skinCluster2GroupParts.ig";
connectAttr "skinCluster2GroupId.id" "skinCluster2GroupParts.gi";
connectAttr "groupId4.msg" "tweakSet2.gn" -na;
connectAttr "nShape7.iog.og[3]" "tweakSet2.dsm" -na;
connectAttr "tweak2.msg" "tweakSet2.ub[0]";
connectAttr "nShape1Orig7.w" "groupParts4.ig";
connectAttr "groupId4.id" "groupParts4.gi";
connectAttr "n0.msg" "bindPose4.m[0]";
connectAttr "n1.msg" "bindPose4.m[1]";
connectAttr "n2.msg" "bindPose4.m[2]";
connectAttr "n3.msg" "bindPose4.m[3]";
connectAttr "n4.msg" "bindPose4.m[4]";
connectAttr "n5.msg" "bindPose4.m[5]";
connectAttr "bindPose4.w" "bindPose4.p[0]";
connectAttr "bindPose4.m[0]" "bindPose4.p[1]";
connectAttr "bindPose4.m[1]" "bindPose4.p[2]";
connectAttr "bindPose4.m[2]" "bindPose4.p[3]";
connectAttr "bindPose4.m[3]" "bindPose4.p[4]";
connectAttr "bindPose4.m[4]" "bindPose4.p[5]";
connectAttr "n0.bps" "bindPose4.wm[0]";
connectAttr "n1.bps" "bindPose4.wm[1]";
connectAttr "n2.bps" "bindPose4.wm[2]";
connectAttr "n3.bps" "bindPose4.wm[3]";
connectAttr "n4.bps" "bindPose4.wm[4]";
connectAttr "n5.bps" "bindPose4.wm[5]";
connectAttr "shader.oc" "shaderSG.ss";
connectAttr "n53Shape.iog" "shaderSG.dsm" -na;
connectAttr "n48Shape.iog" "shaderSG.dsm" -na;
connectAttr "n43Shape.iog" "shaderSG.dsm" -na;
connectAttr "n38Shape.iog" "shaderSG.dsm" -na;
connectAttr "n33Shape.iog" "shaderSG.dsm" -na;
connectAttr "n28Shape.iog" "shaderSG.dsm" -na;
connectAttr "n86Shape.iog" "shaderSG.dsm" -na;
connectAttr "n81Shape.iog" "shaderSG.dsm" -na;
connectAttr "n76Shape.iog" "shaderSG.dsm" -na;
connectAttr "n71Shape.iog" "shaderSG.dsm" -na;
connectAttr "n66Shape.iog" "shaderSG.dsm" -na;
connectAttr "shaderSG.msg" "materialInfo1.sg";
connectAttr "shader.msg" "materialInfo1.m";
connectAttr "skinCluster3GroupParts.og" "skinCluster3.ip[0].ig";
connectAttr "skinCluster3GroupId.id" "skinCluster3.ip[0].gi";
connectAttr "bindPose5.msg" "skinCluster3.bp";
connectAttr "n27.wm" "skinCluster3.ma[0]";
connectAttr "n32.wm" "skinCluster3.ma[1]";
connectAttr "n37.wm" "skinCluster3.ma[2]";
connectAttr "n42.wm" "skinCluster3.ma[3]";
connectAttr "n47.wm" "skinCluster3.ma[4]";
connectAttr "n52.wm" "skinCluster3.ma[5]";
connectAttr "n27.liw" "skinCluster3.lw[0]";
connectAttr "n32.liw" "skinCluster3.lw[1]";
connectAttr "n37.liw" "skinCluster3.lw[2]";
connectAttr "n42.liw" "skinCluster3.lw[3]";
connectAttr "n47.liw" "skinCluster3.lw[4]";
connectAttr "n52.liw" "skinCluster3.lw[5]";
connectAttr "groupParts6.og" "tweak3.ip[0].ig";
connectAttr "groupId6.id" "tweak3.ip[0].gi";
connectAttr "skinCluster3GroupId.msg" "skinCluster3Set.gn" -na;
connectAttr "nShape54.iog.og[4]" "skinCluster3Set.dsm" -na;
connectAttr "skinCluster3.msg" "skinCluster3Set.ub[0]";
connectAttr "tweak3.og[0]" "skinCluster3GroupParts.ig";
connectAttr "skinCluster3GroupId.id" "skinCluster3GroupParts.gi";
connectAttr "groupId6.msg" "tweakSet3.gn" -na;
connectAttr "nShape54.iog.og[5]" "tweakSet3.dsm" -na;
connectAttr "tweak3.msg" "tweakSet3.ub[0]";
connectAttr "nShape3Orig54.w" "groupParts6.ig";
connectAttr "groupId6.id" "groupParts6.gi";
connectAttr "n16.msg" "bindPose5.m[0]";
connectAttr "n17.msg" "bindPose5.m[1]";
connectAttr "n18.msg" "bindPose5.m[2]";
connectAttr "n19.msg" "bindPose5.m[3]";
connectAttr "n20.msg" "bindPose5.m[4]";
connectAttr "n21.msg" "bindPose5.m[5]";
connectAttr "n22.msg" "bindPose5.m[6]";
connectAttr "n24.msg" "bindPose5.m[7]";
connectAttr "n25.msg" "bindPose5.m[8]";
connectAttr "n27.msg" "bindPose5.m[9]";
connectAttr "n29.msg" "bindPose5.m[10]";
connectAttr "n30.msg" "bindPose5.m[11]";
connectAttr "n32.msg" "bindPose5.m[12]";
connectAttr "n34.msg" "bindPose5.m[13]";
connectAttr "n35.msg" "bindPose5.m[14]";
connectAttr "n37.msg" "bindPose5.m[15]";
connectAttr "n39.msg" "bindPose5.m[16]";
connectAttr "n40.msg" "bindPose5.m[17]";
connectAttr "n42.msg" "bindPose5.m[18]";
connectAttr "n44.msg" "bindPose5.m[19]";
connectAttr "n45.msg" "bindPose5.m[20]";
connectAttr "n47.msg" "bindPose5.m[21]";
connectAttr "n49.msg" "bindPose5.m[22]";
connectAttr "n50.msg" "bindPose5.m[23]";
connectAttr "n52.msg" "bindPose5.m[24]";
connectAttr "bindPose5.w" "bindPose5.p[0]";
connectAttr "bindPose5.m[0]" "bindPose5.p[1]";
connectAttr "bindPose5.m[1]" "bindPose5.p[2]";
connectAttr "bindPose5.m[2]" "bindPose5.p[3]";
connectAttr "bindPose5.m[3]" "bindPose5.p[4]";
connectAttr "bindPose5.m[4]" "bindPose5.p[5]";
connectAttr "bindPose5.m[5]" "bindPose5.p[6]";
connectAttr "bindPose5.m[6]" "bindPose5.p[7]";
connectAttr "bindPose5.m[7]" "bindPose5.p[8]";
connectAttr "bindPose5.m[8]" "bindPose5.p[9]";
connectAttr "bindPose5.m[5]" "bindPose5.p[10]";
connectAttr "bindPose5.m[10]" "bindPose5.p[11]";
connectAttr "bindPose5.m[11]" "bindPose5.p[12]";
connectAttr "bindPose5.m[4]" "bindPose5.p[13]";
connectAttr "bindPose5.m[13]" "bindPose5.p[14]";
connectAttr "bindPose5.m[14]" "bindPose5.p[15]";
connectAttr "bindPose5.m[3]" "bindPose5.p[16]";
connectAttr "bindPose5.m[16]" "bindPose5.p[17]";
connectAttr "bindPose5.m[17]" "bindPose5.p[18]";
connectAttr "bindPose5.m[2]" "bindPose5.p[19]";
connectAttr "bindPose5.m[19]" "bindPose5.p[20]";
connectAttr "bindPose5.m[20]" "bindPose5.p[21]";
connectAttr "bindPose5.m[1]" "bindPose5.p[22]";
connectAttr "bindPose5.m[22]" "bindPose5.p[23]";
connectAttr "bindPose5.m[23]" "bindPose5.p[24]";
connectAttr "n16.bps" "bindPose5.wm[0]";
connectAttr "n17.bps" "bindPose5.wm[1]";
connectAttr "n18.bps" "bindPose5.wm[2]";
connectAttr "n19.bps" "bindPose5.wm[3]";
connectAttr "n20.bps" "bindPose5.wm[4]";
connectAttr "n21.bps" "bindPose5.wm[5]";
connectAttr "n22.bps" "bindPose5.wm[6]";
connectAttr "n24.bps" "bindPose5.wm[7]";
connectAttr "n25.bps" "bindPose5.wm[8]";
connectAttr "n27.bps" "bindPose5.wm[9]";
connectAttr "n29.bps" "bindPose5.wm[10]";
connectAttr "n30.bps" "bindPose5.wm[11]";
connectAttr "n32.bps" "bindPose5.wm[12]";
connectAttr "n34.bps" "bindPose5.wm[13]";
connectAttr "n35.bps" "bindPose5.wm[14]";
connectAttr "n37.bps" "bindPose5.wm[15]";
connectAttr "n39.bps" "bindPose5.wm[16]";
connectAttr "n40.bps" "bindPose5.wm[17]";
connectAttr "n42.bps" "bindPose5.wm[18]";
connectAttr "n44.bps" "bindPose5.wm[19]";
connectAttr "n45.bps" "bindPose5.wm[20]";
connectAttr "n47.bps" "bindPose5.wm[21]";
connectAttr "n49.bps" "bindPose5.wm[22]";
connectAttr "n50.bps" "bindPose5.wm[23]";
connectAttr "n52.bps" "bindPose5.wm[24]";
connectAttr "layerManager.dli[1]" "layer1.id";
connectAttr "skinCluster4GroupParts.og" "skinCluster4.ip[0].ig";
connectAttr "skinCluster4GroupId.id" "skinCluster4.ip[0].gi";
connectAttr "bindPose6.msg" "skinCluster4.bp";
connectAttr "n85.wm" "skinCluster4.ma[0]";
connectAttr "n80.wm" "skinCluster4.ma[1]";
connectAttr "n65.wm" "skinCluster4.ma[4]";
connectAttr "n55.wm" "skinCluster4.ma[5]";
connectAttr "n56.wm" "skinCluster4.ma[6]";
connectAttr "n57.wm" "skinCluster4.ma[7]";
connectAttr "n58.wm" "skinCluster4.ma[8]";
connectAttr "n59.wm" "skinCluster4.ma[9]";
connectAttr "n60.wm" "skinCluster4.ma[10]";
connectAttr "n70.wm" "skinCluster4.ma[11]";
connectAttr "n75.wm" "skinCluster4.ma[12]";
connectAttr "n85.liw" "skinCluster4.lw[0]";
connectAttr "n80.liw" "skinCluster4.lw[1]";
connectAttr "n65.liw" "skinCluster4.lw[4]";
connectAttr "n55.liw" "skinCluster4.lw[5]";
connectAttr "n56.liw" "skinCluster4.lw[6]";
connectAttr "n57.liw" "skinCluster4.lw[7]";
connectAttr "n58.liw" "skinCluster4.lw[8]";
connectAttr "n59.liw" "skinCluster4.lw[9]";
connectAttr "n60.liw" "skinCluster4.lw[10]";
connectAttr "n70.liw" "skinCluster4.lw[11]";
connectAttr "n75.liw" "skinCluster4.lw[12]";
connectAttr "n65.msg" "skinCluster4.ptt";
connectAttr "groupParts8.og" "tweak4.ip[0].ig";
connectAttr "groupId8.id" "tweak4.ip[0].gi";
connectAttr "skinCluster4GroupId.msg" "skinCluster4Set.gn" -na;
connectAttr "nShape87.iog.og[4]" "skinCluster4Set.dsm" -na;
connectAttr "skinCluster4.msg" "skinCluster4Set.ub[0]";
connectAttr "tweak4.og[0]" "skinCluster4GroupParts.ig";
connectAttr "skinCluster4GroupId.id" "skinCluster4GroupParts.gi";
connectAttr "groupId8.msg" "tweakSet4.gn" -na;
connectAttr "nShape87.iog.og[5]" "tweakSet4.dsm" -na;
connectAttr "tweak4.msg" "tweakSet4.ub[0]";
connectAttr "nShape4Orig87.w" "groupParts8.ig";
connectAttr "groupId8.id" "groupParts8.gi";
connectAttr "n55.msg" "bindPose6.m[0]";
connectAttr "n56.msg" "bindPose6.m[1]";
connectAttr "n82.msg" "bindPose6.m[2]";
connectAttr "n83.msg" "bindPose6.m[3]";
connectAttr "n85.msg" "bindPose6.m[4]";
connectAttr "n57.msg" "bindPose6.m[5]";
connectAttr "n77.msg" "bindPose6.m[6]";
connectAttr "n78.msg" "bindPose6.m[7]";
connectAttr "n80.msg" "bindPose6.m[8]";
connectAttr "n58.msg" "bindPose6.m[9]";
connectAttr "n72.msg" "bindPose6.m[10]";
connectAttr "n73.msg" "bindPose6.m[11]";
connectAttr "n59.msg" "bindPose6.m[13]";
connectAttr "n67.msg" "bindPose6.m[14]";
connectAttr "n68.msg" "bindPose6.m[15]";
connectAttr "n60.msg" "bindPose6.m[17]";
connectAttr "n62.msg" "bindPose6.m[18]";
connectAttr "n63.msg" "bindPose6.m[19]";
connectAttr "n65.msg" "bindPose6.m[20]";
connectAttr "n70.msg" "bindPose6.m[21]";
connectAttr "n75.msg" "bindPose6.m[22]";
connectAttr "bindPose6.w" "bindPose6.p[0]";
connectAttr "bindPose6.m[0]" "bindPose6.p[1]";
connectAttr "bindPose6.m[1]" "bindPose6.p[2]";
connectAttr "bindPose6.m[2]" "bindPose6.p[3]";
connectAttr "bindPose6.m[3]" "bindPose6.p[4]";
connectAttr "bindPose6.m[1]" "bindPose6.p[5]";
connectAttr "bindPose6.m[5]" "bindPose6.p[6]";
connectAttr "bindPose6.m[6]" "bindPose6.p[7]";
connectAttr "bindPose6.m[7]" "bindPose6.p[8]";
connectAttr "bindPose6.m[5]" "bindPose6.p[9]";
connectAttr "bindPose6.m[9]" "bindPose6.p[10]";
connectAttr "bindPose6.m[10]" "bindPose6.p[11]";
connectAttr "bindPose6.m[9]" "bindPose6.p[13]";
connectAttr "bindPose6.m[13]" "bindPose6.p[14]";
connectAttr "bindPose6.m[14]" "bindPose6.p[15]";
connectAttr "bindPose6.m[13]" "bindPose6.p[17]";
connectAttr "bindPose6.m[17]" "bindPose6.p[18]";
connectAttr "bindPose6.m[18]" "bindPose6.p[19]";
connectAttr "bindPose6.m[19]" "bindPose6.p[20]";
connectAttr "bindPose6.m[15]" "bindPose6.p[21]";
connectAttr "bindPose6.m[11]" "bindPose6.p[22]";
connectAttr "n55.bps" "bindPose6.wm[0]";
connectAttr "n56.bps" "bindPose6.wm[1]";
connectAttr "n82.bps" "bindPose6.wm[2]";
connectAttr "n83.bps" "bindPose6.wm[3]";
connectAttr "n85.bps" "bindPose6.wm[4]";
connectAttr "n57.bps" "bindPose6.wm[5]";
connectAttr "n77.bps" "bindPose6.wm[6]";
connectAttr "n78.bps" "bindPose6.wm[7]";
connectAttr "n80.bps" "bindPose6.wm[8]";
connectAttr "n58.bps" "bindPose6.wm[9]";
connectAttr "n72.bps" "bindPose6.wm[10]";
connectAttr "n73.bps" "bindPose6.wm[11]";
connectAttr "n59.bps" "bindPose6.wm[13]";
connectAttr "n67.bps" "bindPose6.wm[14]";
connectAttr "n68.bps" "bindPose6.wm[15]";
connectAttr "n60.bps" "bindPose6.wm[17]";
connectAttr "n62.bps" "bindPose6.wm[18]";
connectAttr "n63.bps" "bindPose6.wm[19]";
connectAttr "n65.bps" "bindPose6.wm[20]";
connectAttr "n70.bps" "bindPose6.wm[21]";
connectAttr "n75.bps" "bindPose6.wm[22]";
connectAttr "hyperView1.msg" "nodeEditorPanel1Info.b[0]";
connectAttr "hyperLayout1.msg" "hyperView1.hl";
connectAttr "shaderSG.pa" ":renderPartition.st" -na;
connectAttr "nShape7.iog" ":initialShadingGroup.dsm" -na;
connectAttr "nShape15.iog" ":initialShadingGroup.dsm" -na;
connectAttr "nShape54.iog" ":initialShadingGroup.dsm" -na;
connectAttr "nShape87.iog" ":initialShadingGroup.dsm" -na;
connectAttr "shader.msg" ":defaultShaderList1.s" -na;
connectAttr "defaultRenderLayer.msg" ":defaultRenderingList1.r" -na;
// End of compareSkinningMethods.ma
